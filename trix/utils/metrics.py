from typing import Any, Dict

import jax.numpy as jnp


def compute_accuracy(y_true: jnp.array, y_pred: jnp.array) -> jnp.array:
    """
    Compute the accuracy.

    Args:
        y_true: Tensor of numbers between 0 and num_cls-1 of shape (**, num_samples,).
        y_pred: Tensor of numbers between 0 and num_cls-1 of shape (**, num_samples,).

    Returns:
        The accuracies as a tensor of shape (**,).
    """
    eqs = jnp.equal(y_true, y_pred)
    return jnp.mean(eqs, axis=-1)


def get_confusion_metrics(confusion_matrix: jnp.ndarray) -> Dict[str, Any]:
    """
    Get precision, recall, f1-score and MCC.

    Args:
        confusion_matrix: A jax array representing the confusion matrix
            of shape (num_classes, num_classes)
    Returns:
        Dictionary containing the following metrics:
            MCC, macro and micro precision, recall and f1-score.
    """
    assert len(confusion_matrix.shape) == 2
    assert confusion_matrix.shape[0] == confusion_matrix.shape[1]

    tp = jnp.diag(confusion_matrix)
    fp = jnp.sum(confusion_matrix, axis=0) - tp
    fn = jnp.sum(confusion_matrix, axis=1) - tp

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1_score = 2 * (precision * recall) / (precision + recall)

    # Calculate macro precision, recall, and F1-score
    macro_precision = jnp.mean(jnp.nan_to_num(precision))
    macro_recall = jnp.mean(jnp.nan_to_num(recall))
    macro_f1_score = jnp.mean(jnp.nan_to_num(f1_score))

    # Calculate micro precision and recall, and micro F1-score
    micro_precision = jnp.nan_to_num(jnp.sum(tp) / jnp.sum(tp + fp))
    micro_recall = jnp.nan_to_num(jnp.sum(tp) / jnp.sum(tp + fn))
    micro_f1_score = jnp.nan_to_num(
        2 * (micro_precision * micro_recall) / (micro_precision + micro_recall)
    )

    # MCC Code from sklearn
    t_sum = confusion_matrix.sum(axis=1, dtype=jnp.float64)
    p_sum = confusion_matrix.sum(axis=0, dtype=jnp.float64)
    n_correct = jnp.trace(confusion_matrix, dtype=jnp.float64)
    n_samples = p_sum.sum()
    cov_ytyp = n_correct * n_samples - jnp.dot(t_sum, p_sum)
    cov_ypyp = n_samples**2 - jnp.dot(p_sum, p_sum)
    cov_ytyt = n_samples**2 - jnp.dot(t_sum, t_sum)

    if cov_ypyp * cov_ytyt == 0:
        mcc = 0.0
    else:
        mcc = cov_ytyp / jnp.sqrt(cov_ytyt * cov_ypyp)

    # Replace NaNs with zeros (occurs when a denominator in the equation is zero)
    mcc = jnp.nan_to_num(mcc)

    return {
        "micro_precision": micro_precision,
        "micro_recall": micro_recall,
        "micro_f1": micro_f1_score,
        "mcc": mcc,
        "macro_precision": macro_precision,
        "macro_recall": macro_recall,
        "macro_f1": macro_f1_score,
    }
