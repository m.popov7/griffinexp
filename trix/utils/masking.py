from typing import Callable, Tuple, Union

import jax
import jax.numpy as jnp

from trix.types import AttentionMask, Tokens


def build_causal_attention_mask(batch_size: int, seq_len: int) -> AttentionMask:
    """
    Builds a batch of causal masks of shape (batch_size, 1, seq_len, seq_len) to feed
    to an attention layer.

    Args:
        batch_size: Batch size.
        seq_len: Length of the sequences.

    Returns:
        Batch of causal masks.
    """
    mask = jnp.ones((batch_size, 1, seq_len, seq_len))
    causal_mask = jnp.tril(mask)
    return causal_mask


def create_stacked_causal_mask_func(max_examples: int) -> Callable:
    """
    Creates a function which can mask a training sequence with multiple concatenated
    examples, up to max_examples.

    Args:
        max_examples: Maximum number of stacked examples that can appear in the
            training sequences.  Upper limit must be set to compile in JAX.

    Returns:
        Function to build the causal masks.
    """

    def build_stacked_mask(token_ids: jnp.ndarray, eos_token_id: int) -> jnp.ndarray:
        """
        Builds the causal mask which uses EOS tokens to ensure different training
        examples do not interact.

        Args:
            token_ids: The input sequence of token ids, which can be separated into
                max_examples (or fewer) training examples.
            eos_token_id: The token signifying the end of an example.

        Returns:
            The causal, stacked mask
        """
        token_ids = jnp.where(token_ids == eos_token_id, 1, 0)
        seq_len = token_ids.shape[-1]

        separator_positions = jnp.argwhere(
            token_ids, size=max_examples, fill_value=seq_len
        ).reshape(-1)

        causal_mask = jnp.tril(jnp.ones((seq_len, seq_len), dtype=int))
        stacked_mask = jnp.ones((2 * seq_len, 2 * seq_len), dtype=int)

        def scan_func(carry: jnp.ndarray, x: int) -> Tuple[jnp.ndarray, int]:
            carry = jax.lax.dynamic_update_slice(
                carry, jnp.zeros((seq_len, seq_len), dtype=int), (x + 1, 0)
            )
            carry = jax.lax.dynamic_update_slice(
                carry, jnp.ones((seq_len, seq_len), dtype=int), (x + 1, x + 1)
            )
            return carry, 0

        stacked_mask, _ = jax.lax.scan(scan_func, stacked_mask, separator_positions)
        stacked_mask = stacked_mask[:seq_len, :seq_len] & causal_mask
        return stacked_mask[None, ...]  # Add head dimension

    return jax.vmap(build_stacked_mask, (0, None))  # type: ignore # Add batch dimension


def _causal_with_prefix_mask(seq_len: int, prefix: int) -> AttentionMask:
    """
    Builds a causal mask with prefix of shape (1, seq_len, seq_len). Function to be
    vmapped.

    Args:
        seq_len: Length of the sequences.
        prefix: Prefix up to which the mask is fully connected.

    Returns:
        A causal mask with prefix.
    """
    mask = jnp.ones((1, seq_len, seq_len))
    causal_mask = jnp.tril(mask)
    keep = jnp.tile(jnp.arange(seq_len), (1, seq_len, 1))
    prefix_mask = (keep < prefix) * mask + (keep >= prefix) * causal_mask
    return prefix_mask


def build_prefix_causal_attention_mask(
    batch_size: int, seq_len: int, prefix: Union[int, jnp.ndarray]
) -> AttentionMask:
    """
    Builds a batch of causal mask with prefix of shape (batch_size, 1, seq_len, seq_len)
    to feed to an attention layer.

    Args:
        batch_size: Batch size.
        seq_len: Length of the sequences.
        prefix: Prefix up to which the mask is fully connected.

    Returns:
        Batch of causal masks with prefix.
    """
    if type(prefix) is int:
        prefix = jnp.array([prefix] * batch_size)
    prefix_mask = jax.vmap(_causal_with_prefix_mask, in_axes=(None, 0))(seq_len, prefix)
    return prefix_mask


def build_padding_attention_mask(tokens: Tokens, pad_token_id: int) -> AttentionMask:
    """
    Builds a padding mask from a sequence of tokens by masking <pad> in the attention.

    Args:
        tokens: Batch of sequences of shape (batch_size, seq_len).
        pad_token_id: Int corresponding to the <pad> token to mask.

    Returns:
        Batch of attention masks, masking out <pad> tokens.
    """
    padding_mask = tokens != pad_token_id
    padding_mask = padding_mask[:, None, :]
    padding_mask = jnp.einsum("bhT, bht->bhtT", padding_mask, padding_mask)
    return padding_mask


def build_perceiver_padding_attention_mask(
    tokens: Tokens, resampled_length: int, pad_token_id: int
) -> AttentionMask:
    """
    Builds a padding mask from a sequence of tokens by masking <pad> in the attention,
    specific to perceiver resampler.

    Args:
        tokens: Batch of sequences of shape (batch_size, seq_len).
        resampled_length: New length after a PerceiverResampler module. Will be used to
            define the shape of the mask.
        pad_token_id: Int corresponding to the <pad> token to mask.

    Returns:
        Batch of attention masks, masking out <pad> tokens, of shape (batch_size, 1,
            resampled_length, seq_len+resampled_length)
    """
    batch_size, _ = tokens.shape
    padding_mask = tokens != pad_token_id  # (batch_size, seq_len)

    # add 1 for resampled_length
    # (in perceiver we concat [token_embeddings, latent query_embeddings])
    padding_mask = jnp.concatenate(
        (padding_mask, jnp.ones((batch_size, resampled_length))), axis=1
    )  # (batch_size, seq_len + resampled_length)

    padding_mask = padding_mask[
        :, None, None, :
    ]  # (batch_size, 1, seq_len + resampled_length)
    # repeat the mask resampled_length times for latent_query attention
    padding_mask = jnp.tile(padding_mask, (1, 1, resampled_length, 1))
    return padding_mask
