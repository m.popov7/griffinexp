from typing import Optional

import haiku as hk
import numpy as np


def shard_linear_and_psum(
    params: hk.Params,
    num_shards: int,
    sharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Shards a set of parameters of a Linear to a LinearAndPSum layer.

    Args:
        params: Unsharded parameters, must have a key layer_name
            and a key layer_name/~/linear.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}

    layer_name = np.sort(list(params.keys()))[0]
    if "b" in params[layer_name].keys():
        sharded_params[layer_name] = {
            "bias": np.tile(params[layer_name]["b"], (num_shards, 1))
        }
    weight = params[layer_name]["w"]
    sharded_params[layer_name + "/~/linear"] = {
        "w": np.reshape(
            weight, (num_shards, weight.shape[0] // num_shards, *weight.shape[1:])
        )
    }
    return sharded_params


def shard_parallel_linear(
    params: hk.Params,
    num_shards: int,
    sharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Shards a set of parameters of a parallel Linear layer.

    Args:
        params: Unsharded parameters, must have one key with two subkeys 'w' and 'b'.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.
    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    weight = params[layer_name]["w"]
    sharded_params[layer_name] = {
        "w": weight.reshape(
            weight.shape[0], num_shards, weight.shape[1] // num_shards
        ).transpose(1, 0, 2),
    }

    if "b" in params[layer_name].keys():
        bias = params[layer_name]["b"]
        sharded_params[layer_name]["b"] = bias.reshape(
            num_shards, bias.shape[0] // num_shards, *bias.shape[1:]
        )

    return sharded_params


def shard_parallel_linear_glu(
    params: hk.Params,
    num_shards: int,
    sharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Shards a set of parameters of a parallel Linear layer prior when there is a gated
     linear unit activation (GLU).
     such a layer corresponds to fc1 in the following code :
         x1, x2 = jnp.split(self.fc1(x), indices_or_sections=2, axis=-1)
         x = self._ffn_activation_fn(x1) * x2
     GLU is implemented in trix/models/gpt/layers.py and in
     trix/layers/attention/self_attention.py

    Args:
        params: Unsharded parameters, must have one key with two subkeys 'w' and 'b'.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.
    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    weight = params[layer_name]["w"]
    weight_left, weight_right = np.split(weight, indices_or_sections=2, axis=1)
    sharded_weight_left = weight_left.reshape(
        weight_left.shape[0], num_shards, weight_left.shape[1] // num_shards
    ).transpose(1, 0, 2)
    sharded_weight_right = weight_right.reshape(
        weight_right.shape[0], num_shards, weight_right.shape[1] // num_shards
    ).transpose(1, 0, 2)
    sharded_params[layer_name] = {
        "w": np.concatenate((sharded_weight_left, sharded_weight_right), axis=2)
    }

    if "b" in params[layer_name].keys():
        bias = params[layer_name]["b"]
        bias_left, bias_right = np.split(bias, indices_or_sections=2, axis=1)
        sharded_bias_left = bias_left.reshape(
            num_shards, bias_left.shape[0] // num_shards, *bias_left.shape[1:]
        )
        sharded_bias_right = bias_left.reshape(
            num_shards, bias_right.shape[0] // num_shards, *bias_right.shape[1:]
        )
        sharded_params[layer_name]["b"] = np.concatenate(
            (sharded_bias_left, sharded_bias_right), axis=1
        )

    return sharded_params


def shard_replicated_linear(
    params: hk.Params,
    num_shards: int,
    sharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Shards a set of parameters of a replicated Linear layer.

    Args:
        params: Unsharded parameters, must have one key with two subkeys 'w' and 'b'.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    layer_name = np.sort(list(params.keys()))[0]
    sharded_params[layer_name] = {
        "w": np.tile(params[layer_name]["w"], reps=(num_shards, 1, 1)),
    }
    if "b" in params[layer_name].keys():
        sharded_params[layer_name]["b"] = np.tile(
            params[layer_name]["b"], reps=(num_shards, 1)
        )
    return sharded_params


def shard_replicated_norm(
    params: hk.Params,
    num_shards: int,
    sharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Shards a set of parameters of a LayerNorm to of parameters of a ReplicatedLayerNorm.

    Args:
        params: Unsharded parameters, must have a key with two subkeys 'scale' and
            'offset'.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    layer_name = np.sort(list(params.keys()))[0]
    sharded_params[layer_name] = {
        "scale": np.tile(params[layer_name]["scale"], (num_shards, 1)),
    }

    if "offset" in params[layer_name].keys():
        sharded_params[layer_name]["offset"] = np.tile(
            params[layer_name]["offset"], (num_shards, 1)
        )

    return sharded_params


def shard_replicated_embedding(
    params: hk.Params, num_shards: int, sharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Shards a set of parameters of an embedding layer (Embed or
    LearnedPositionalEmbeddings) to of parameters of a ReplicatedLayerNorm.

    Args:
        params: Unsharded parameters, must have a key with one subkey 'embeddings'.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    layer_name = np.sort(list(params.keys()))[0]
    sharded_params[layer_name] = {
        "embeddings": np.tile(params[layer_name]["embeddings"], (num_shards, 1, 1)),
    }
    return sharded_params


def shard_attention_block(
    params: hk.Params, num_shards: int, sharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Shards the parameters of a SelfAttentionBlock or CrossAttentionBlock.

    Args:
        params: Unsharded parameters of a SelfAttentionBlock or CrossAttentionBlock.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    for layer_name in params:
        if "fc2" in layer_name or "mha_output" in layer_name:
            sharded_params = shard_linear_and_psum(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif (
            ("fc1" in layer_name)
            or ("value" in layer_name)
            or ("key" in layer_name)
            or ("query" in layer_name)
        ):
            sharded_params = shard_parallel_linear(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )

        elif "layer_norm" in layer_name:
            sharded_params = shard_replicated_norm(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        else:
            raise ValueError(
                "your parameter dictionary does not correspond to a "
                "SelfAttentionBlock/CrossAttentionBlock."
                " This can also happen if the names of the layers in "
                "SelfAttentionBlock/CrossAttentionBlock"
                " have been modified."
            )
    return sharded_params
