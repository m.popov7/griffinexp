from typing import Optional

import haiku as hk
import numpy as np


def unshard_linear_and_psum(
    params: hk.Params,
    unsharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Unshards a set of parameters of a LinearAndPSum layer.

    Args:
        params: Sharded parameters, must have a key layer_name
            and a key layer_name/~/linear.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    if len(params.keys()) > 2:
        raise ValueError(
            "params should only have two keys (see docstring). "
            f"found {len(params.keys())}"
        )
    layer_name = np.sort(list(params.keys()))[0]
    if "bias" in params[layer_name].keys():
        unsharded_params[layer_name] = {
            "w": np.concatenate(params[layer_name + "/~/linear"]["w"], axis=0),
        }
        unsharded_params[layer_name]["b"] = params[layer_name]["bias"][0]

    else:
        unsharded_params[layer_name.replace("/~/linear", "")] = {
            "w": np.concatenate(params[layer_name]["w"], axis=0),
        }
    return unsharded_params


def unshard_replicated_linear(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Unshards a set of parameters of a replicated Linear layer (lm head).

    Args:
        params: Sharded parameters, must have a single key.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    unsharded_params[layer_name] = {
        "w": params[layer_name]["w"][0],
    }
    if "b" in params[layer_name].keys():
        unsharded_params[layer_name]["b"] = params[layer_name]["b"][0]
    return unsharded_params


def unshard_parallel_linear(
    params: hk.Params,
    unsharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Unshards a set of parameters of a parallel Linear layer (attention heads).

    Args:
        params: Sharded parameters, must have a single key.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    unsharded_params[layer_name] = {
        "w": np.concatenate(params[layer_name]["w"].transpose(0, 2, 1)).transpose(1, 0),
    }
    if "b" in params[layer_name].keys():
        unsharded_params[layer_name]["b"] = np.concatenate(params[layer_name]["b"])

    return unsharded_params


def unshard_parallel_linear_glu(
    params: hk.Params,
    unsharded_params: Optional[hk.Params] = None,
) -> hk.Params:
    """
    Unshards a set of parameters of a parallel Linear layer prior when there is a gated
     linear unit activation (GLU).
     such a layer corresponds to fc1 in the following code :
         x1, x2 = jnp.split(self.fc1(x), indices_or_sections=2, axis=-1)
         x = self._ffn_activation_fn(x1) * x2
     GLU is implemented in trix/models/gpt/layers.py and in
     trix/layers/attention/self_attention.py

    Args:
        params: Sharded parameters, must have a single key.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    weight = params[layer_name]["w"]
    weight_left, weight_right = np.split(weight, indices_or_sections=2, axis=2)
    weight_left = np.concatenate(weight_left.transpose(0, 2, 1)).transpose(1, 0)
    weight_right = np.concatenate(weight_right.transpose(0, 2, 1)).transpose(1, 0)
    unsharded_params[layer_name] = {
        "w": np.concatenate((weight_left, weight_right), axis=1)
    }

    if "b" in params[layer_name].keys():
        bias = params[layer_name]["b"]
        bias_left, bias_right = np.split(bias, indices_or_sections=2, axis=1)
        bias_left = np.concatenate(bias_left)
        bias_right = np.concatenate(bias_right)
        unsharded_params[layer_name]["b"] = np.concatenate((bias_left, bias_right))

    return unsharded_params


def unshard_replicated_norm(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Unshards a set of parameters of a ReplicatedLayerNorm.

    Args:
        params: Sharded parameters, must have a single key.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    unsharded_params[layer_name] = {
        "scale": params[layer_name]["scale"][0],
    }
    if "offset" in params[layer_name].keys():
        unsharded_params[layer_name]["offset"] = params[layer_name]["offset"][0]
    return unsharded_params


def unshard_attention_block(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Unshards the parameters of a SelfAttentionBlock/CrossAttentionBlock.

    Args:
        params: Sharded parameters of a SelfAttentionBlock/CrossAttentionBlock.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    for layer_name in params:
        if "fc2" in layer_name:
            if "~/linear" not in layer_name:
                # TODO: pass second layer as it is done twice
                fc2_params = {
                    layer: params[layer] for layer in params if "fc2" in layer
                }
                unsharded_params = unshard_linear_and_psum(fc2_params, unsharded_params)

        elif "mha_output" in layer_name:
            if "~/linear" not in layer_name:
                # TODO: pass second layer as it is done twice
                mha_output_params = {
                    layer: params[layer] for layer in params if "mha_output" in layer
                }
                unsharded_params = unshard_linear_and_psum(
                    mha_output_params, unsharded_params
                )
        elif (
            ("fc1" in layer_name)
            or ("value" in layer_name)
            or ("key" in layer_name)
            or ("query" in layer_name)
        ):
            unsharded_params = unshard_parallel_linear(
                {layer_name: params[layer_name]}, unsharded_params
            )

        elif "layer_norm" in layer_name:

            unsharded_params = unshard_replicated_norm(
                {layer_name: params[layer_name]}, unsharded_params
            )
        else:
            raise ValueError(
                f"the key {layer_name} caused troubles : \n"
                "your parameter dictionary does not correspond to a SelfAttentionBlock."
                " This can also happen if the names of the layers in SelfAttentionBlock"
                " have been modified."
            )
    return unsharded_params


def unshard_replicated_embedding(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Unshards a set of parameters of a ReplicatedEmbed or
    ReplicatedLearnedPositionalEmbedding.

    Args:
        params: Sharded parameters, must have a single key.
        unsharded_params: Optional unsharded parameters to
            which new weights will be appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    if len(params.keys()) != 1:
        raise ValueError(
            "params should only have one key" "corresponding to the layer name"
        )
    layer_name = np.sort(list(params.keys()))[0]
    unsharded_params[layer_name] = {
        "embeddings": params[layer_name]["embeddings"][0],
    }
    return unsharded_params
