import re

RNA_NUCLEOTIDE_TYPES = ["U", "C", "A", "G"]
NON_STANDARD_NUCLEOTIDE_TYPES = [
    "N",
    "Y",
    "R",
    "S",
    "M",
    "K",
    "W",
    "D",
    "H",
    "B",
    "V",
    "T",
    "X",
    "I",
]  # ref: https://www.hgmd.cf.ac.uk/docs/nuc_lett.html
# and http://www.geneinfinity.org/sp/sp_nucsymbols.html,
# https://en.wikipedia.org/wiki/Nucleotide#Abbreviation_codes_for_degenerate_bases
RNA_REGEX = re.compile(f"^[{''.join(RNA_NUCLEOTIDE_TYPES)}]+$")
RNA_REGEX_ALL = re.compile(
    f"^[{''.join(RNA_NUCLEOTIDE_TYPES + NON_STANDARD_NUCLEOTIDE_TYPES)}]+$"
)

NON_STANDARD_NUCLEOTIDE_MAP = {  # ref: https://www.hgmd.cf.ac.uk/docs/nuc_lett.html
    "N": ["A", "G", "C", "U"],
    "Y": ["C", "U"],
    "R": ["G", "A"],
    "S": ["G", "C"],
    "M": ["A", "C"],
    "K": ["G", "U"],
    "W": ["A", "U"],
    "D": ["G", "A", "U"],
    "H": ["A", "C", "U"],
    "B": ["G", "U", "C"],
    "V": ["G", "C", "A"],
    "T": ["U"],
    "X": ["A", "G", "C", "U"],  # ref: http://www.geneinfinity.org/sp/sp_nucsymbols.html
    "I": [
        "A",
        "G",
        "C",
        "U",
    ],
}
# ref: https://en.wikipedia.org/wiki/Nucleotide#Abbreviation_codes_for_degenerate_bases
