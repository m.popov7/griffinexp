import os
from typing import Tuple

import boto3
import joblib
import numpy as np
import tqdm

from trix.training.base import TrainingState

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> str:
    """
    Get directory to save files on user machine.
    """
    return os.path.expanduser(
        os.path.join(
            os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR),
            "trix",
        )
    )


def init_s3_trix_bucket() -> Tuple[boto3.session.Session, str]:
    """Initialize the s3_client to the kao endpoint.
    Assumes that the key and secret have already been loaded as environment variables

    Returns:
        Tuple[boto3.session.Session, str]: _description_
    """
    s3_endpoint = "https://storage.googleapis.com"
    bucket = "trix-internal"

    # This part assumes the user has already exported required env variables
    session = boto3.Session()
    s3_client = session.client(
        service_name="s3",
        aws_access_key_id=os.environ.get("AWS_ACCESS_KEY_ID"),
        aws_secret_access_key=os.environ.get("AWS_SECRET_ACCESS_KEY"),
        endpoint_url=s3_endpoint,
    )

    return s3_client, bucket


def load_checkpoint_from_s3(
    s3_client: boto3.session.Session,
    bucket: str,
    bucket_checkpoint_directory: str,
) -> TrainingState:
    """
    Downloads a checkpoint located on a s3 bucket in the cache and load it in RAM. This
    probably also works with Google Cloud Storage.

    Args:
        s3_client: S3 which is going to query the path to the checkpoint that we want
            to load.
        bucket: Bucket in which the path is located
        bucket_checkpoint_directory: Path to the checkpoint on the bucket.

    Returns:
        The loaded training state.
    """
    # Get directories
    name_local_folder = bucket_checkpoint_directory.split(os.path.sep)[-1]
    save_dir = os.path.join(_get_dir(), name_local_folder)

    os.makedirs(save_dir, exist_ok=True)

    # Download the checkpoint
    # Download key
    download_from_s3_bucket(
        s3_client=s3_client,
        bucket=bucket,
        key=os.path.join(bucket_checkpoint_directory, "key.npy"),
        filename=os.path.join(save_dir, "key.npy"),
    )

    # Download params
    download_from_s3_bucket(
        s3_client=s3_client,
        bucket=bucket,
        key=os.path.join(bucket_checkpoint_directory, "params.joblib"),
        filename=os.path.join(save_dir, "params.joblib"),
    )

    # Download optimizer state
    download_from_s3_bucket(
        s3_client=s3_client,
        bucket=bucket,
        key=os.path.join(bucket_checkpoint_directory, "opt_state.joblib"),
        filename=os.path.join(save_dir, "opt_state.joblib"),
    )

    # Download step
    download_from_s3_bucket(
        s3_client=s3_client,
        bucket=bucket,
        key=os.path.join(bucket_checkpoint_directory, "step.npy"),
        filename=os.path.join(save_dir, "step.npy"),
    )

    # Load the different components of the training state
    key = np.load(os.path.join(save_dir, "key.npy"))
    params = joblib.load(os.path.join(save_dir, "params.joblib"))
    opt_state = joblib.load(os.path.join(save_dir, "opt_state.joblib"))
    step = np.load(os.path.join(save_dir, "step.npy"))

    return TrainingState(
        step=step,
        params=params,
        optimizer_state=opt_state,
        random_key=key,
    )


def download_from_s3_bucket(
    s3_client: boto3.session.Session,
    bucket: str,
    key: str,
    filename: str,
    verbose: bool = True,
) -> None:
    """
    Download data from the s3 bucket and display downloading progression bar.

    Args:
        s3_client: Boto3 s3 client
        bucket: Bucket name.
        key: Path towards file in the bucket.
        filename: Path to save file locally.
        verbose: Whether or not to print the progress bar during the download.
    """
    kwargs = {
        "Bucket": bucket,
        "Key": key,
    }
    object_size = s3_client.head_object(**kwargs)["ContentLength"]

    if verbose:
        with tqdm.tqdm(
            total=object_size, unit="B", unit_scale=True, desc=filename
        ) as pbar:
            with open(filename, "wb") as f:
                s3_client.download_fileobj(
                    Bucket=bucket,
                    Key=key,
                    ExtraArgs=None,
                    Fileobj=f,
                    Callback=lambda bytes_transferred: pbar.update(bytes_transferred),
                )
    else:
        with open(filename, "wb") as f:
            s3_client.download_fileobj(
                Bucket=bucket,
                Key=key,
                ExtraArgs=None,
                Fileobj=f,
            )
