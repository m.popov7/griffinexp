import dataclasses

import numpy as np

from trix.utils.constants.bio import NUCLEOTIDES
from trix.utils.constants.rna import (
    NON_STANDARD_NUCLEOTIDE_MAP,
    RNA_NUCLEOTIDE_TYPES,
    RNA_REGEX_ALL,
)


class SequenceLengthError(Exception):
    """Exception raised for errors in the input sequence length.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message: str = "Invalid sequence length.") -> None:
        self.message = message
        super().__init__(self.message)


class RNABaseError(Exception):
    """Exception raised for errors in the RNA sequence composition.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message: str = "RNA must only have bases `ACGU`") -> None:
        self.message = message
        super().__init__(self.message)


@dataclasses.dataclass(init=False, frozen=True)
class RNASequence:
    """Dataclass to ensure RNA syntax and semantics, notably in the case of mRNA
    sequences (with a coding sequence)."""

    rna_sequence: str
    is_coding: bool
    coding_sequence_start_index: int | None
    coding_sequence_end_index: int | None
    taxonomy: str = ""

    def __init__(
        self,
        is_coding: bool = True,
        rna_sequence: str | None = None,
        coding_sequence_start_index: int | None = None,
        coding_sequence_end_index: int | None = None,
        five_prime_utr: str | None = None,
        coding_sequence: str | None = None,
        three_prime_utr: str | None = None,
        taxonomy: str = "",
    ):
        """
        Initialize an RNASequence.

        It must be initialized with either the `rna_sequence` parameter; or the
        `coding_sequence` parameter. And additionally, when coding, with either
        `coding_sequence_start_index` and `coding_sequence_end_index` parameters;
        or `five_prime_utr` and `three_prime_utr` parameters.

        Args:
            is_coding: A boolean indicating whether the sequence is coding (mRNA) or
                not (ncRNA).
            rna_sequence: The complete string (including UTRs, if coding) of the RNA
                sequence. If coding, this parameter should be used with
                `coding_sequence_start_index` and `coding_sequence_end_index`. Cannot
                be used with `coding_sequence` parameter.
            coding_sequence_start_index: Only for a coding sequence. The index
                (0-based) where the coding sequence begins. Should be used with
                `rna_sequence` parameter.
            coding_sequence_end_index: Only for a coding sequence. The index
                (0-based) where the coding sequence ends. Should be used with
                `rna_sequence` parameter.
            five_prime_utr: Only for a coding sequence. The string corresponding to the
             5'-UTR of the coding sequence. Should be used with `coding_sequence`
             parameter.
            coding_sequence: The string of the coding sequence (CDS) of the mRNA
                sequence. If coding, this parameter should be used with `
                five_prime_utr` and `three_prime_utr`. If not coding, this
                parameter should not be used. Cannot be used with
                `rna_sequence` parameter.
            three_prime_utr: Only for a coding sequence. The string
                corresponding to the 3'-UTR of the coding sequence. Should
                be used with `coding_sequence` parameter.
            taxonomy: The string corresponding to the taxonomy of the
                RNA sequence.
        """
        # Parameter checks
        if not is_coding and not all(
            (
                coding_sequence_start_index is None,
                coding_sequence_end_index is None,
                five_prime_utr is None,
                three_prime_utr is None,
            )
        ):
            raise ValueError(
                "RNASequence cannot be initialised with UTRs when the sequence is not "
                "coding."
            )

        utr_parameters_consistent = (
            (rna_sequence is None)
            == (coding_sequence_start_index is None)
            == (coding_sequence_end_index is None)
        ) and (
            (five_prime_utr is None)
            == (coding_sequence is None)
            == (three_prime_utr is None)
        )

        if (rna_sequence is None) == (coding_sequence is None) or (
            is_coding and not utr_parameters_consistent
        ):
            raise ValueError(
                "RNASequence must be initialized with either the `rna_sequence` "
                "parameter; or the `coding_sequence` parameter. And additionally, "
                "when coding, with either `coding_sequence_start_index` and "
                "`coding_sequence_end_index` parameters; or `five_prime_utr` "
                "and `three_prime_utr` parameters."
            )

        # Convert strings to indexes, if needed
        if coding_sequence is not None:
            if is_coding:
                assert five_prime_utr is not None
                assert three_prime_utr is not None

                rna_sequence = five_prime_utr + coding_sequence + three_prime_utr
                coding_sequence_start_index = len(five_prime_utr)
                coding_sequence_end_index = len(five_prime_utr) + len(coding_sequence)
            else:
                raise NotImplementedError(
                    "Non-coding RNASequence has been initialized with "
                    "`coding_sequence` parameter. `rna_sequence` parameter should be "
                    "used instead."
                )

        object.__setattr__(self, "is_coding", is_coding)
        object.__setattr__(self, "rna_sequence", rna_sequence)
        object.__setattr__(
            self, "coding_sequence_start_index", coding_sequence_start_index
        )
        object.__setattr__(self, "coding_sequence_end_index", coding_sequence_end_index)
        object.__setattr__(self, "taxonomy", taxonomy)

        # Assertions
        if is_coding:
            assert len(self.coding_sequence) % 3 == 0

        assert RNA_REGEX_ALL.match(self.rna_sequence) is not None, self.rna_sequence

    def __str__(self) -> str:
        return f"{self.rna_sequence}"

    @property
    def coding_sequence(self) -> str:
        if not self.is_coding:
            raise ValueError(
                "Cannot access the coding sequence of a non-coding sequence."
            )

        return self.rna_sequence[
            self.coding_sequence_start_index : self.coding_sequence_end_index
        ]

    @property
    def five_prime_utr(self) -> str:
        if not self.is_coding:
            raise RuntimeError(
                "Attempt to access an untranslated region for a non-coding RNASequence."
            )

        return self.rna_sequence[: self.coding_sequence_start_index]

    @property
    def three_prime_utr(self) -> str:
        if not self.is_coding:
            raise RuntimeError(
                "Attempt to access an untranslated region for a non-coding RNASequence."
            )

        return self.rna_sequence[self.coding_sequence_end_index :]

    def map_non_standard_nucleotides(
        self, rng: None | np.random.Generator = None
    ) -> "RNASequence":
        """
        Convert all non-standard nucleotides in the 5' UTR, coding region, and 3'UTR
        regions to standard nucleotides following the rule defined in
        `map_non_standard_nucleotide`.

        Args:
            rng: a random generator or None forwarded to `map_non_standard_nucleotide`

        Returns:
            a new RNASequence of same length with only standard nucleotides.
        """
        return RNASequence(
            is_coding=self.is_coding,
            rna_sequence=map_non_standard_nucleotide_sequence(self.rna_sequence, rng),
            coding_sequence_start_index=self.coding_sequence_start_index,
            coding_sequence_end_index=self.coding_sequence_end_index,
            taxonomy=self.taxonomy,
        )

    def get_codons(self) -> tuple[str, ...]:
        """
        Compute and returns the codons for the coding region of this mRNA sequence as a
         tuple of 3-character strings.

        Returns:
            tuple of 3-character strings (one per codon for the coding region of this
            mRNA sequence).

        Raises:
            ValueError: if the coding sequence is of a length that is not a multiple of
             three.
        """
        if not self.is_coding:
            raise RuntimeError("Attempt to access codons for a non-coding RNASequence.")

        return get_codons(self.coding_sequence)


def map_non_standard_nucleotide(
    nucleotide: str, rng: None | np.random.Generator = None
) -> str:
    """
    Map a single (potentially non-standard) nucleotide to a standard nucleotide among
        the ones it corresponds to (either a randomly-chosen one if a random generator
        is provided or else the first one in the list of NON_STANDARD_NUCLEOTIDE_MAP).

    Args:
        nucleotide: a letter that is in NON_STANDARD_NUCLEOTIDE_TYPES.
        rng: a random generator or None

    Returns:
        a standard nucleotide as a str with one character.
    """
    assert len(nucleotide) == 1, nucleotide
    assert RNA_REGEX_ALL.match(nucleotide) is not None, nucleotide
    return (  # type: ignore
        rng.choice(NON_STANDARD_NUCLEOTIDE_MAP[nucleotide])
        if rng is not None
        else NON_STANDARD_NUCLEOTIDE_MAP[nucleotide][0]
    )


def map_non_standard_nucleotide_sequence(
    nucleotide_sequence: str, rng: None | np.random.Generator = None
) -> str:
    """
    Map a sequence of (potentially non-standard) nucleotides to a sequence of standard
        nucleotides using `map_non_standard_nucleotide`.

    Args:
        nucleotide_sequence: a sequence of (potentially non-standard) nucleotides
            matching the RNA_REGEX_ALL regex.
        rng: a random generator or None forwarded to map_non_standard_nucleotide

    Returns:
        a sequence of standard nucleotides of same length.
    """
    if not nucleotide_sequence:
        return nucleotide_sequence

    assert RNA_REGEX_ALL.match(nucleotide_sequence) is not None, nucleotide_sequence

    return "".join(
        [
            map_non_standard_nucleotide(nucleotide, rng)
            if not is_standard_nucleotide(nucleotide)
            else nucleotide
            for nucleotide in nucleotide_sequence
        ]
    )


def get_codons(
    sequence: str,
    allow_non_ternary: bool = False,
) -> tuple[str, ...]:
    """Return all the codons in a coding sequence.

    Args:
        sequence: RNA coding sequence.
        allow_non_ternary: whether all "codons" must be nucleotide triplets

    Returns:
        tuple of codons present in the input sequence
    """

    if allow_non_ternary:
        return tuple(
            sequence[i * 3 : min((i + 1) * 3, len(sequence))]
            for i in range(len(sequence) // 3)
        )
    else:
        return tuple(sequence[i * 3 : (i + 1) * 3] for i in range(len(sequence) // 3))


def is_standard_nucleotide(nucleotide: str) -> bool:
    """Test if a nucleotide is standard or not.

    Args:
        nucleotide: the query nucleotide.

    Returns:
        True if standard, False if not.
    """
    return nucleotide in set(RNA_NUCLEOTIDE_TYPES).union(set(NUCLEOTIDES))
