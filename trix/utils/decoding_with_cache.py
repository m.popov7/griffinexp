from typing import Tuple

import haiku as hk
import jax
import jax.numpy as jnp

from trix.types import RNGKey, Tokens


def update_tokens_ids_greedy_with_cache(
    tokens_ids: Tokens,
    time_step: jnp.ndarray,
    random_key: RNGKey,
    params: hk.Params,
    cache_state: hk.State,
    apply_fn: hk.Transformed.apply,
    pad_token_id: int,
) -> Tuple[Tokens, RNGKey, hk.State]:
    """
    Update the input sequence of tokens using greedy decoding with a decoder model.
    Typical inputs could be a prompt with end of tokens appended. This function can
    then be called recursively after the prompt to generate the rest of the sentence.
    It should be called from time_step = 0 to (tokens_ids.shape[1] - 1) ( while
     update_tokens_ids_greedy should be only called from  time_step = prompt_length to
      (tokens_ids.shape[1] - 1)

    Args:
        tokens_ids: Input tokens ids, shape = (batch_size, sequence_length).
        time_step: Time step at which to decode, shape = (,).
        random_key: Random key.
        params: Decoder parameters.
        cache_state: The cache state containing previous keys and values
        apply_fn: Decoder apply fn.
        pad_token_id: The token id used to pad a batch of sequences.
        Where tokens_ids[:,time_step] == pad_token_id, returned tokens are being updated

    Returns:
        Tokens ids with decoded token at position time_step + 1 and updated random key.
        Cache state for the next step
    """

    current_token_id = tokens_ids[:, time_step]
    outs, cache_state = apply_fn(
        params,
        cache_state,
        random_key,
        jnp.expand_dims(current_token_id, 1),
        jnp.tile(time_step, (current_token_id.shape[0], 1)),
    )
    logits = outs["logits"]
    logits = logits[:, 0, :]
    new_tokens_id = jnp.argmax(logits, axis=-1)
    new_tokens_id = jnp.where(
        tokens_ids[:, time_step + 1] == pad_token_id,
        new_tokens_id,
        tokens_ids[:, time_step + 1],
    )
    tokens_ids = jax.lax.dynamic_update_slice(
        tokens_ids, jnp.expand_dims(new_tokens_id, 1), (0, time_step + 1)
    )

    return tokens_ids, random_key, cache_state


def update_tokens_ids_temperature_sampling_with_cache(
    tokens_ids: Tokens,
    time_step: jnp.ndarray,
    random_key: RNGKey,
    params: hk.Params,
    cache_state: hk.State,
    apply_fn: hk.Transformed.apply,
    pad_token_id: int,
    temperature: float = 1.0,
) -> Tuple[Tokens, RNGKey, hk.State]:
    """
    Update the input sequence of tokens using greedy decoding with a decoder model.
    Typical inputs could be a prompt with end of tokens appended. This function can
    then be called recursively after the prompt to generate the rest of the sentence.
    It should be called from time_step = 0 to (tokens_ids.shape[1] - 1) ( while
     update_tokens_ids_greedy should be only called from  time_step = prompt_length to
      (tokens_ids.shape[1] - 1)

    Args:
        tokens_ids: Input tokens ids, shape = (batch_size, sequence_length).
        time_step: Time step at which to decode, shape = (,).
        random_key: Random key.
        params: Decoder parameters.
        cache_state: The cache state containing previous keys and values
        apply_fn: Decoder apply fn.
        pad_token_id: The token id used to pad a batch of sequences.
        Where tokens_ids[:,time_step] == pad_token_id, returned tokens are being updated
        temperature: temperature coefficient for sampling.

    Returns:
        Tokens ids with decoded token at position time_step + 1 and updated random key.
        Cache state for the next step
    """

    current_token_id = tokens_ids[:, time_step]
    outs, cache_state = apply_fn(
        params,
        cache_state,
        random_key,
        jnp.expand_dims(current_token_id, 1),
        jnp.tile(time_step, (current_token_id.shape[0], 1)),
    )
    logits = outs["logits"]
    logits = logits[:, 0, :]
    logits = logits / temperature
    random_key, sub_key = jax.random.split(random_key)
    new_tokens_id = jax.random.categorical(sub_key, logits, axis=-1)
    new_tokens_id = jnp.where(
        tokens_ids[:, time_step + 1] == pad_token_id,
        new_tokens_id,
        tokens_ids[:, time_step + 1],
    )
    tokens_ids = jax.lax.dynamic_update_slice(
        tokens_ids, jnp.expand_dims(new_tokens_id, 1), (0, time_step + 1)
    )

    return tokens_ids, random_key, cache_state
