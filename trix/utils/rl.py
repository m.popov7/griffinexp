import os
from typing import List, Tuple, Union

import jax
import jax.numpy as jnp
import numpy as np
from numpy.random import RandomState
from sklearn.cluster import KMeans

from trix.types import RNGKey, Tokens


def get_mask_from_dones(dones: jnp.ndarray) -> jnp.ndarray:
    """
    Generate a mask for a RL trajectory given the `dones` indicators.

    Args:
        dones: Indicators that the trajectory is terminated, shape (episode_length, *).

    Returns:
        Mask for the trajectory, shape (episode_length, *).
    """
    mask = jnp.cumsum(jnp.cumsum(dones, axis=0), axis=0)
    mask = jnp.where(mask > 1, 0, 1)
    return mask


def compute_mu_law(x: jnp.ndarray, mu: int = 100, m: int = 256) -> jnp.ndarray:
    """
    Compand each element of x using mu-law.
    In GATO (https://arxiv.org/pdf/2205.06175.pdf) they use this technique to deal with
    continuous observations that do not have given max and min values.

    Note: A processed vector is typically close to [-1,1], in GATO they further clip it
    to this interval.

    Args:
        x: Array to be companded.
        mu: Mu parameter of the mu-law.
        m: Parameter of the mu-law.

    Returns:
        Companded signal.
    """
    return jnp.sign(x) * (jnp.log(jnp.abs(x) * mu + 1.0) / jnp.log(m * mu + 1.0))


def compute_inverse_mu_law(x: jnp.ndarray, mu: int = 100, m: int = 256) -> jnp.ndarray:
    """
    Computes inverse of mu_law, see `compute_mu_law`.

    Args:
        x: Array to be companded.
        mu: Mu parameter of the mu-law.
        m: Parameter of the mu-law.

    Returns:
        Companded signal.
    """
    return jnp.sign(x) * ((1 + m * mu) ** jnp.abs(x) - 1) / mu


def chunk_trajectory_tokens(
    tokens: Tokens,
    random_key: RNGKey,
    horizon_size: int,
    concat_size: int,
    num_chunks_per_trajectory: int,
) -> Tokens:
    """
    Splits tokens into multiple fixed-horizon-size chunks. Chunks are sampled
    uniformly in the trajectories.

    Args:
        tokens: Tokens. Shape: (num_sequences, episode_length*concat_size)
        random_key: Random key.
        horizon_size: Horizon size.
        concat_size: Length of the vector standing for a timestep in RL, chunks have
            size (horizon_size*concat_size).
        num_chunks_per_trajectory: Number of chunks to sample per trajectory.

    Returns:
        Chunks of shape
            (num_sequences*num_chunks_per_trajectory, horizon_size*concat_size).
    """
    sequence_length = jax.tree_util.tree_leaves(tokens)[0].shape[1]
    num_sequences = jax.tree_util.tree_leaves(tokens)[0].shape[0]

    assert sequence_length % concat_size == 0

    episode_length = sequence_length // concat_size
    start_indices = (
        jax.random.randint(
            random_key,
            minval=0,
            maxval=episode_length - horizon_size + 1,
            shape=(num_sequences, num_chunks_per_trajectory),
        )
        * concat_size
    )

    @jax.jit
    def _get_chunks(data: jnp.ndarray) -> jnp.ndarray:
        return jax.vmap(
            jax.vmap(
                lambda x, y: jax.lax.dynamic_slice_in_dim(
                    x, y, horizon_size * concat_size, axis=0
                )
            ),
            in_axes=(None, 1),
        )(data, start_indices)

    chunks = jax.tree_map(_get_chunks, tokens)
    chunks = jax.tree_map(lambda x: x.reshape(-1, *x.shape[2:]), chunks)
    return chunks


def compute_cvt_centroids(
    num_descriptors: int,
    num_init_cvt_samples: int,
    num_centroids: int,
    minval: Union[float, List[float]],
    maxval: Union[float, List[float]],
    random_key: RNGKey,
    cache_centroids: bool = False,
) -> Tuple[jnp.ndarray, RNGKey]:
    """
    Computes centroids for Centroidal Voronoi Tessellation (CVT).

    Args:
        num_descriptors: Number of scalar descriptors.
        num_init_cvt_samples: Number of sampled points to be used for clustering to
            determine the centroids. The larger the number of centroids and the
            number of descriptors, the higher this value must be (e.g. 100000 for
            1024 centroids and 100 descriptors).
        num_centroids: Number of centroids.
        minval: Minimum descriptors value.
        maxval: Maximum descriptors value.
        random_key: Jax random key.
        cache_centroids: Whether to use cached centroids when possible.

    Returns:
        The centroids with shape (num_centroids, num_descriptors).
        A Jax random key.
    """

    centroids_filepath = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        f"centroids_{num_descriptors}_{num_init_cvt_samples}_{num_centroids}.npy",
    )

    if os.path.isfile(centroids_filepath) and cache_centroids:
        return jnp.array(np.load(centroids_filepath)), random_key

    minval = jnp.array(minval)
    maxval = jnp.array(maxval)

    # assume here all values are in [0, 1] and rescale later
    random_key, subkey = jax.random.split(random_key)
    x = jax.random.uniform(key=subkey, shape=(num_init_cvt_samples, num_descriptors))

    # compute k means
    random_key, subkey = jax.random.split(random_key)
    k_means = KMeans(
        init="k-means++",
        n_clusters=num_centroids,
        n_init=1,
        random_state=RandomState(subkey),
    )
    k_means.fit(x)
    centroids = k_means.cluster_centers_
    # rescale now
    centroids = jnp.asarray(centroids) * (maxval - minval) + minval

    if cache_centroids:
        np.save(centroids_filepath, np.array(centroids), allow_pickle=False)
    return centroids, random_key


def get_cells_indices(
    batch_of_descriptors: jnp.ndarray, centroids: jnp.ndarray
) -> jnp.ndarray:
    """
    Returns the array of cells indices for a batch of descriptors
    given the centroids of the repertoire.

    Args:
        batch_of_descriptors: A batch of descriptors
            of shape (batch_size, num_descriptors).
        centroids: Centroids array of shape (num_centroids, num_descriptors).

    Returns:
        The indices of the centroids corresponding to each vector of descriptors
            in the batch with shape (batch_size,).
    """
    num_centroids = centroids.shape[0]
    num_descriptors = batch_of_descriptors.shape[0]

    gram_matrix = jnp.einsum("ik,jk->ij", batch_of_descriptors, centroids)
    norm_descriptors = jnp.einsum(
        "ik,ik->i", batch_of_descriptors, batch_of_descriptors
    )
    norm_centroids = jnp.einsum("ik,ik->i", centroids, centroids)

    distance_matrix = (
        jnp.dot(
            norm_descriptors.reshape((num_descriptors, 1)), jnp.ones((1, num_centroids))
        )
        + jnp.dot(
            jnp.ones((num_descriptors, 1)), norm_centroids.reshape((1, num_centroids))
        )
        - 2 * gram_matrix
    )
    return jnp.argmin(distance_matrix, axis=-1)
