import logging

import jax.numpy as jnp


def debug_log_tensor(
    tensor_name: str, tensor: jnp.ndarray, logger: logging.Logger
) -> None:
    """
    Logging with debugging level a tensor's name, shape and dtype.

    Args:
        tensor_name: tensor's name.
        tensor: jnp tensor.
        logger: logger to be used.
    """
    shape = tensor.shape
    dtype = tensor.dtype
    debug_msg = f"Tensor, Name = {tensor_name}, Shape = {shape}, Dtype = {dtype}"
    logger.debug(debug_msg, stacklevel=2)
