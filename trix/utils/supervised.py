import numpy as np


def compute_binned_labels(
    labels: np.ndarray, n_bins: int, eps: float = 1
) -> np.ndarray:
    """
    Take an array of continuous labels, a number of bins to use to discretize,
    and output the binned version of the labels. If the bins are [0,1,2,3], then
    the label value 2.25 will be represented by the vector [0,0,0.75, 0.25].

    Args:
        labels : Labels array. [n_sequence, n_labels]
        n_bins : Number of bins to use.
        eps : Margin taken at the extremities to build the bins.

    Returns:
        labels_binned: Binned labels [n_sequences, n_labels, n_bins + 1]. To get the
            continuous labels taken as an input, it is only needed to do a dot product
            between the support (bins array) and the binned labels.
        bins: Bins used for each label [n_labels, n_bins + 1]. A dot product with the
            classification probabilities will return the continuous value.
    """

    # Create the array that will store the binned labels
    labels_binned = np.zeros((labels.shape[0], labels.shape[1], n_bins + 1))

    # Create list of bins
    bins_list = []

    for idx_label in range(labels.shape[1]):
        label_vector = labels[:, idx_label]
        # Compute the discretized values that will serve to represent the labels
        min_value = np.min(label_vector)
        max_value = np.max(label_vector)
        bins = np.linspace(min_value - eps, max_value + eps, n_bins + 1)
        bins_list.append(bins)

        # Loop over label
        for idx, label in enumerate(label_vector):
            # Find index of the values
            idx_x2 = np.argmax(label < bins)
            idx_x1 = idx_x2 - 1

            # Find values
            x2 = bins[idx_x2]
            x1 = bins[idx_x1]

            # Compute alpha
            alpha = (label - x1) / (x2 - x1)
            labels_binned[idx, idx_label, idx_x1] = 1 - alpha
            labels_binned[idx, idx_label, idx_x2] = alpha
    bins = np.stack(bins_list, axis=0)

    return labels_binned, bins
