from typing import Any, List

import sentencepiece as spm
from transformers import PreTrainedTokenizer

from trix.types import Tokens


class SentencePieceTokenizer(PreTrainedTokenizer):
    """
    Sentence piece based tokenizer.
    Requires a pretrained SentencePiece model.
    """

    model_input_names = ["input_ids", "attention_mask"]

    def __init__(
        self,
        spm_model_path: str,
        eos_token: str = "</s>",
        unk_token: str = "<unk>",
        pad_token: str = "<pad>",
        bos_token: str = "<s>",
        add_eos_token: bool = False,
        **kwargs: Any,
    ):
        """
        Args:
            spm_model_path: The path to the pretrained SentencePiece model.
        """

        self._spm_model_path = spm_model_path
        self._spm_model = spm.SentencePieceProcessor(self._spm_model_path)
        self._vocabulary_size: int = self._spm_model.get_piece_size()
        self._bos_token_id = self._spm_model.bos_id()
        self._eos_token_id = self._spm_model.eos_id()
        self._pad_token_id = self._spm_model.pad_id()
        self._unk_token_id = self._spm_model.unk_id()
        if self._bos_token_id == -1:
            self._bos_token_id = self._pad_token_id
            bos_token = pad_token

        self.vocabulary = {
            i: self._spm_model.decode(i) for i in range(self._vocabulary_size)
        }
        self.standard_tokens = list(self.vocabulary.values())
        self._add_eos = add_eos_token
        super().__init__(
            eos_token=eos_token,
            unk_token=unk_token,
            pad_token=pad_token,
            bos_token=bos_token,
            **kwargs,
        )

    def vocab_size(self) -> int:
        return self._vocabulary_size

    def tokenize(self, text: str, **kwargs: Any) -> Any:
        # Replace the SPIECE_UNDERLINE with a space to
        # make sure SPIECE_UNDERLINE is only used at
        # the beginning of the text
        return super().tokenize(text, **kwargs)

    def convert_tokens_to_string(self, tokens: Tokens) -> Any:
        """Converts a sequence of tokens (string) in a single string."""
        current_sub_tokens: List = []
        out_string = ""
        prev_is_special = False
        for token in tokens:
            # make sure that special tokens are not decoded using sentencepiece model
            if token in self.all_special_tokens:
                if not prev_is_special:
                    out_string += " "
                out_string += self._spm_model.decode(current_sub_tokens) + token
                prev_is_special = True
                current_sub_tokens = []
            else:
                current_sub_tokens.append(token)
                prev_is_special = False
        out_string += self._spm_model.decode(current_sub_tokens)
        return out_string.strip()

    def _convert_token_to_id(self, token: str) -> Any:
        """Converts a token (str) in an id using the vocab."""
        return self._spm_model.piece_to_id(token)

    def _convert_id_to_token(self, index: int) -> Any:
        """Converts an index (integer) in a token (str) using the vocab."""
        token = self._spm_model.IdToPiece(index)
        return token

    def _tokenize(self, text: str, **kwargs: Any) -> Any:
        """ """
        if self._add_eos:
            text += self.eos_token
        tokens = self._spm_model.encode(text, out_type=str)
        return tokens
