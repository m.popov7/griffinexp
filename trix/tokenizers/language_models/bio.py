from itertools import product
from typing import Dict, List, Optional, Tuple

import numpy as np

from trix.tokenizers.language_models.standard import StandardTokenizer
from trix.utils.constants.bio import EXTRA_NUCLEOTIDES, NUCLEOTIDES


def _compute_k_mers(k: int) -> List[str]:
    """
    Generates all the different k-mers for nucleotides given a value of k.

    Args:
        k: The k parameter for k-mers.

    Returns:
        All the different k-mers.
    """
    return ["".join(elt) for elt in product(NUCLEOTIDES, repeat=k)]


def compute_dcnuc_tokens_to_ids(k_mers: int) -> Tuple[Dict[str, int], List[str]]:
    """Compute the tokens to ids mapping that correspond to the tokenizer used to train
    the DCNUC pretrained models.

    Args:
        k_mers (int): k used for the kmers.

    Returns:
        Dict[str, int]: Corresponding tokens to ids mapping.
    """
    # Get tokenizer
    kmers_tokens = _compute_k_mers(k=k_mers)
    standard_tokens = kmers_tokens + NUCLEOTIDES + EXTRA_NUCLEOTIDES

    unk_token = "<unk>"
    pad_token = "<pad>"
    mask_token = "<mask>"
    class_token = "<cls>"
    eos_token = "<eos>"
    bos_token = "<bos>"

    special_tokens_1 = [unk_token, pad_token, mask_token, class_token]
    special_tokens_2 = [eos_token, bos_token]
    all_tokens = special_tokens_1 + standard_tokens + special_tokens_2
    tokens_to_ids = {tok: i for i, tok in enumerate(all_tokens)}

    return tokens_to_ids, standard_tokens


class NucleotidesKmersTokenizer(StandardTokenizer):
    """
    This is a tokenizer specific for nucleotide sequences.
    It only considers sequence containing the tokens A, T, C, G and N.
    N is always considered as a special token and tokenized alone.
    """

    def __init__(
        self,
        k_mers: int,
        unk_token: str = "<unk>",
        pad_token: str = "<pad>",
        mask_token: str = "<mask>",
        class_token: str = "<cls>",
        eos_token: str = "<eos>",
        bos_token: str = "<bos>",
        prepend_bos_token: bool = False,
        prepend_cls_token: bool = False,
        append_eos_token: bool = False,
        tokens_to_ids: Optional[Dict[str, int]] = None,
    ):
        """
        Instantiates a FixedSizeNucleotideKmersTokenizer.

        Args:
            k_mers: How many nucleotides to consider for generating vocabulary.
            unk_token: Unknown token.
            pad_token: Pad token.
            mask_token: Mask token.
            class_token: Class token.
            eos_token: End of speech tokens.
            bos_token: Beginning of sentence token.
            prepend_bos_token: Prepend beginning of sentence token.
            prepend_cls_token: Prepend class token.
            append_eos_token: Append end of speech token.
            tokens_to_ids: (Optional) Enable the user to optionally choose ids for
                the tokens. If you provide this argument the dictionary must include
                the following special tokens
                ["<unk>","<pad>","<mask>","<cls>","<eos>","<bos>"]
                or instantiation will fail. Additionally, if the ids in your dictionary
                do not start at 0 then an error will also be raised. If this argument is
                not specified, then ids are attributed automatically by the tokenizer
                during initialization.
        """
        kmers_tokens = _compute_k_mers(k_mers)
        standard_tokens = kmers_tokens + NUCLEOTIDES + EXTRA_NUCLEOTIDES

        StandardTokenizer.__init__(
            self,
            standard_tokens=standard_tokens,
            unk_token=unk_token,
            pad_token=pad_token,
            mask_token=mask_token,
            class_token=class_token,
            eos_token=eos_token,
            bos_token=bos_token,
            prepend_bos_token=prepend_bos_token,
            prepend_cls_token=prepend_cls_token,
            append_eos_token=append_eos_token,
            tokens_to_ids=tokens_to_ids,
        )

        self._k_mers = k_mers

    def tokenize(self, sequence: str) -> Tuple[List[str], List[int]]:
        """
        Tokenizes a sequence and returns the list of tokens as well
        as the list of their IDs. The tokenization algorithm first splits up the
        substrings of the input sequence in-between N characters.
        Then these substrings are split into pieces of length k, and if it
        is possible (edge cases) it adds up pieces of length 1.

        If a single character that does not correspond
        to any token is found, an error is raised.

        Args:
            sequence: Sequence to be tokenized.

        Returns:
            List of tokens.
            List of token ids.

        Example:
            Find below two tokenization examples when k_mers=5.

            ATCGAATGGCGATGCAC --> ATCGA ATGGC GATGC A C

            ATCGAATNGGCGATGCAC -> ATCGA A T N GGCGA TGCAC
        """
        splitted_seq = sequence.split("N")
        len_splitted = len(splitted_seq)
        tokens: List[str] = []

        for i, split in enumerate(splitted_seq):
            chunks = [
                split[i * self._k_mers : (i + 1) * self._k_mers]
                for i in range(len(split) // self._k_mers)
            ]
            if len(split) % self._k_mers != 0:
                chunks.append(split[(len(split) // self._k_mers) * self._k_mers :])

            for chunk in chunks:
                if len(chunk) == self._k_mers:
                    tokens.append(chunk)
                else:
                    for nucl in chunk:
                        tokens.append(nucl)
            if i < len_splitted - 1:
                tokens.append("N")

        if self._prepend_cls_token:
            tokens = [self._class_token] + tokens

        if self._prepend_bos_token:
            tokens = [self._bos_token] + tokens

        if self._append_eos_token:
            tokens.append(self._eos_token)

        tokens_ids = [self.token_to_id(tok) for tok in tokens]

        return tokens, tokens_ids


class FixedSizeNucleotidesKmersTokenizer(NucleotidesKmersTokenizer):
    """
    Simple tokenizer that naively extracts tokens. Used for amino-acids
    and nucleotides. This tokenizer also tokenizes batches to a
    fixed maximum length. If one of the sequences provided exceeds the maximum
    length, an exception is raised.
    """

    def __init__(
        self,
        k_mers: int,
        fixed_length: int,
        unk_token: str = "<unk>",
        pad_token: str = "<pad>",
        mask_token: str = "<mask>",
        class_token: str = "<cls>",
        eos_token: str = "<eos>",
        bos_token: str = "<bos>",
        prepend_bos_token: bool = False,
        prepend_cls_token: bool = False,
        append_eos_token: bool = False,
        tokens_to_ids: Optional[Dict[str, int]] = None,
    ):
        """
        Instantiates a FixedSizeNucleotideKmersTokenizer.

        Args:
            k_mers: How many nucleotides to consider for generating vocabulary.
            unk_token: Unknown token.
            pad_token: Pad token.
            mask_token: Mask token.
            class_token: Class token.
            eos_token: End of speech tokens.
            bos_token: Beginning of sentence token.
            prepend_bos_token: Prepend beginning of sentence token.
            prepend_cls_token: Prepend class token.
            append_eos_token: Append end of speech token.
            fixed_length: Fixed length to pad all sequences in batches.
        """
        NucleotidesKmersTokenizer.__init__(
            self,
            unk_token=unk_token,
            pad_token=pad_token,
            mask_token=mask_token,
            class_token=class_token,
            eos_token=eos_token,
            bos_token=bos_token,
            prepend_bos_token=prepend_bos_token,
            prepend_cls_token=prepend_cls_token,
            append_eos_token=append_eos_token,
            k_mers=k_mers,
            tokens_to_ids=tokens_to_ids,
        )
        self._fixed_length = fixed_length

    @property
    def fixed_length(self) -> int:
        """
        Property that returns the pre-defined fixed sequence length.

        Returns:
            The pre-defined fixed sequence length.
        """
        return self._fixed_length

    def pad_tokens_batch(
        self, batch: List[Tuple[List[str], List[int]]]
    ) -> List[Tuple[List[str], List[int]]]:
        """
        Takes tokens and tokens ids of a batch of sequences, and returns a batch of
        padded sequences.

        Args:
            batch: List of tuples, each composed of a sequence's tokens and token ids.

        Returns:
            The padded list, where every sequence is padded to the fixed maximum length.
        """
        lengths = [len(t[0]) for t in batch]
        maximum_length = max(lengths)
        if maximum_length > self._fixed_length:
            raise ValueError(
                f"Found a sequence with length {maximum_length} that "
                f"exceeds the fixed length to tokenize ({self._fixed_length})."
            )
        deltas = [self._fixed_length - length for length in lengths]
        padded_tokens = [
            t[0] + ([self.pad_token] * delta) for t, delta in zip(batch, deltas)
        ]
        padded_tokens_ids = [
            t[1] + ([self.pad_token_id] * delta) for t, delta in zip(batch, deltas)
        ]
        return [
            (toks, toks_ids) for toks, toks_ids in zip(padded_tokens, padded_tokens_ids)
        ]


class NucleotideOneHotEncoder:
    """
    Not a tokenizer per se but also in a similar fashion to turn a sequence of
    Nucleotides into an input that can be fed to a deep learning model. This encoder
    is the one used for the Enformer model.

    The mapping used is
    A: [1, 0, 0, 0]
    C: [0, 1, 0, 0]
    G: [0, 0, 1, 0]
    T: [0, 0, 0, 1]
    N: [0, 0, 0, 0]

    In the source implementation, padding is defined as [0.25, 0.25, 0.25, 0.25]. Here,
    as we have doubts about this design we prefer to not support padding.
    """

    def __init__(self) -> None:
        self._mapping = {
            "A": np.array([1, 0, 0, 0]),
            "C": np.array([0, 1, 0, 0]),
            "G": np.array([0, 0, 1, 0]),
            "T": np.array([0, 0, 0, 1]),
            "N": np.array([0, 0, 0, 0]),
        }

    def tokenize(self, sequence: str) -> np.ndarray:
        """
        Transform a sequence into the corresponding one hot vector.

        Args:
            sequence: e.g. ATTTCGCGCGCNNCGCGCG

        Returns:
            corresponding one-hot array of shape (seq_length, 4)
        """
        # quick processing to have sequences of ATCGN in capital letters only
        sequence = sequence.upper()
        letters = set(sequence)
        for letter in letters:
            if letter not in {"A", "C", "G", "T", "N"}:
                sequence.replace(letter, "N")
        one_hots = [self._mapping[letter] for letter in sequence]
        return one_hots

    def batch_tokenize(self, sequences: List[str]) -> np.ndarray:
        """
        Transform a batch of sequences into the corresponding batch of one hot vectors.

        Args:
            sequences: list of sequences. All sequences should have the same length.

        Returns:
            corresponding array of shape (batch_size, seq_length, 4)
        """
        lengths = {len(seq) for seq in sequences}
        assert len(lengths) == 1, "all sequences should have the same length"
        batch = [self.tokenize(seq) for seq in sequences]
        return np.array(batch).astype(np.float32)
