import dataclasses

import jax.numpy as jnp

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.utils.constants.rna import RNA_NUCLEOTIDE_TYPES, RNA_REGEX
from trix.utils.rna_sequence import RNASequence


class InvalidRNASequenceError(Exception):
    pass


@dataclasses.dataclass(frozen=True)
class CodonTokenizerParams:
    cds_only: bool = False  # if True, discard the 5' and 3' UTR regions when tokenizing
    use_taxonomy_token: bool = False  # whether to use a taxonomy token
    use_non_standard_nucleotide_tokens: bool = (
        False  # if True, creates non-standard nucleotide tokens when
    )
    # non-standard nucleotides are met instead of discarding sequences that
    # have non-standard nucleotides.
    convert_non_standard_nucleotides: bool = (
        False  # If True, non-standard nucleotides found in the input mRNA
    )
    # sequences are converted to one of the corresponding "standard" nucleotides
    # before tokenizing the sequence.
    use_codon_tokens: bool = True  # whether to use specific tokens for the codons
    # in the coding region or to simply encode individual nucleotides just like in
    # the UTR regions
    prepend_bos_token: bool = True
    append_eos_token: bool = True
    prepend_cds_bos_token: bool = (
        True  # If True, add a token signaling the beginning of the coding region
    )
    append_cds_eos_token: bool = (
        True  # If True, add a token signaling the end of the coding region
    )
    bos_token: str = "<bos>"
    eos_token: str = "<eos>"
    cds_bos_token: str = "<cds_bos>"
    cds_eos_token: str = "<cds_eos>"
    cds_non_standard_nucleotide_token: str = "<cds_nn>"
    utr_non_standard_nucleotide_token: str = "<utr_nn>"
    taxonomy_tokens: list[str] = dataclasses.field(
        default_factory=list
    )  # list of tokens used in training representing the taxonomy
    unknown_taxonomy_token: str = "<unk_tax>"


class CodonTokenizer(BaseTokenizer):
    """
    This tokenizer treats the coding region differently to the UTR region:

    If `use_codon_tokens` is set to True in params, codons (triplets of nucleotides)
    are encoded using a single token in the coding region.
    """

    def __init__(self, params: CodonTokenizerParams, fixed_length: int) -> None:
        """Initialize a CodonTokenizer using the given parameters.

        Args:
            params: an instance of CodonTokenizerParams.
            fixed_length: the length to pad/truncate sequences to.
        """
        self._params = params

        special_position_tokens: list[str] = []
        if params.prepend_bos_token:
            special_position_tokens.append(params.bos_token)
        if params.append_eos_token:
            special_position_tokens.append(params.eos_token)
        if params.prepend_cds_bos_token:
            special_position_tokens.append(params.cds_bos_token)
        if params.append_cds_eos_token:
            special_position_tokens.append(params.cds_eos_token)
        if params.use_non_standard_nucleotide_tokens:
            special_position_tokens.append(params.cds_non_standard_nucleotide_token)
            special_position_tokens.append(params.utr_non_standard_nucleotide_token)
        if params.use_taxonomy_token:
            for t in params.taxonomy_tokens:
                special_position_tokens.append(t)
            special_position_tokens.append(params.unknown_taxonomy_token)

        all_codons = [
            f"{x}{y}{z}"
            for x in RNA_NUCLEOTIDE_TYPES
            for y in RNA_NUCLEOTIDE_TYPES
            for z in RNA_NUCLEOTIDE_TYPES
        ]
        self._all_standard_tokens: list[str] = list(RNA_NUCLEOTIDE_TYPES) + (
            all_codons if params.use_codon_tokens else []
        )
        self._all_tokens: list[str] = (
            special_position_tokens + self._all_standard_tokens
        )
        self._token_to_id_map: dict[str, int] = {
            token: token_id for token_id, token in enumerate(self._all_tokens)
        }
        self._vocabulary_size = len(self._all_tokens)
        self._mask_token_id = self._vocabulary_size
        self._pad_token_id = self._vocabulary_size + 1
        self._fixed_length = fixed_length

    @property
    def vocabulary(self) -> list[str]:
        return list(self._all_tokens) + [
            self.pad_token,
            self.mask_token,
        ]

    @property
    def standard_tokens(self) -> list[str]:
        return list(self._all_standard_tokens)

    @property
    def special_tokens(self) -> list[str]:
        return [self.pad_token, self.mask_token]

    @property
    def unk_token(self) -> str:
        raise NotImplementedError()

    @property
    def pad_token(self) -> str:
        return "<pad>"

    @property
    def mask_token(self) -> str:
        return "<mask>"

    @property
    def class_token(self) -> str:
        raise NotImplementedError()

    @property
    def eos_token(self) -> str:
        raise NotImplementedError()

    @property
    def bos_token(self) -> str:
        raise NotImplementedError()

    def id_to_token(self, token_id: int) -> str:
        if token_id == self._pad_token_id:
            return self.pad_token
        if token_id == self._mask_token_id:
            return self.mask_token
        assert token_id < self._vocabulary_size
        return self._all_tokens[token_id]

    def token_to_id(self, token: str) -> int:
        if token == self.pad_token:
            return self._pad_token_id
        if token == self.mask_token:
            return self._mask_token_id
        assert token in self._all_tokens
        return self._token_to_id_map[token]

    def tokenize(  # type: ignore[override]
        self, sequence: RNASequence
    ) -> tuple[list[str], list[int]]:
        """Tokenize only one sequence using the private RNABaseTokenizer.

        Args:
            sequence: an RNASequence object (cannot be a string) that can be coding
                (in which case it has a 5'UTR, a coding sequence, and a 3'UTR) or
                non-coding (in which case it only has a `rna_sequence` attribute).

        Returns:
            a tuple with the list of tokens, and the list of token ids.
        """
        if isinstance(sequence, str):
            raise NotImplementedError(
                "This tokenizer only tokenizes RNASequence objects, not strings."
                "Use `trix.utils.rna_sequence` to cast your sequences correctly."
            )
        all_token_ids: list[int] = []

        if (
            not self._params.convert_non_standard_nucleotides
            and not self._params.use_non_standard_nucleotide_tokens
            and RNA_REGEX.match(str(sequence)) is None
        ):
            raise InvalidRNASequenceError(
                f"Non-standard nucleotide found in RNA sequence even though "
                f"the use_non_standard_nucleotide_tokens and "
                f"convert_non_standard_nucleotides options are set to False "
                f"(exact sequence {str(sequence)})."
            )

        if self._params.convert_non_standard_nucleotides:
            sequence = sequence.map_non_standard_nucleotides()

        if self._params.cds_only:
            if not self._params.use_codon_tokens:
                # coding is False because a cds only tokeniser may be used against
                # non-coding RNA sequences
                sequence = RNASequence(
                    is_coding=False,
                    rna_sequence=sequence.coding_sequence,
                    taxonomy=sequence.taxonomy,
                )
            else:
                sequence = RNASequence(
                    coding_sequence=sequence.coding_sequence,
                    five_prime_utr="",
                    three_prime_utr="",
                )

        if self._params.use_taxonomy_token:
            all_token_ids.append(
                self._token_to_id_map[sequence.taxonomy]
                if sequence.taxonomy in self._token_to_id_map
                and sequence.taxonomy != ""
                else self._token_to_id_map[self._params.unknown_taxonomy_token]
            )

        if self._params.prepend_bos_token and (
            not sequence.is_coding
            or sequence.five_prime_utr != ""
            or not self._params.prepend_cds_bos_token
        ):
            all_token_ids.append(self._token_to_id_map[self._params.bos_token])

        # 5'UTR
        if sequence.is_coding and not self._params.cds_only:
            if self._params.use_non_standard_nucleotide_tokens:
                all_token_ids.extend(
                    (
                        self._token_to_id_map[nucleotide]
                        if nucleotide in RNA_NUCLEOTIDE_TYPES
                        else self._token_to_id_map[
                            self._params.utr_non_standard_nucleotide_token
                        ]
                    )
                    for nucleotide in sequence.five_prime_utr
                )
            else:
                all_token_ids.extend(
                    self._token_to_id_map[nucleotide]
                    for nucleotide in sequence.five_prime_utr
                )

        if self._params.prepend_cds_bos_token:
            all_token_ids.append(self._token_to_id_map[self._params.cds_bos_token])

        if self._params.use_codon_tokens:
            if self._params.use_non_standard_nucleotide_tokens:
                all_token_ids.extend(
                    (
                        self._token_to_id_map[codon]
                        if (RNA_REGEX.match(codon) is not None)
                        else self._token_to_id_map[
                            self._params.cds_non_standard_nucleotide_token
                        ]
                    )
                    for codon in sequence.get_codons()
                )
            else:
                all_token_ids.extend(
                    self._token_to_id_map[codon] for codon in sequence.get_codons()
                )
        else:
            if self._params.use_non_standard_nucleotide_tokens:
                all_token_ids.extend(
                    (
                        self._token_to_id_map[nucleotide]
                        if nucleotide in RNA_NUCLEOTIDE_TYPES
                        else self._token_to_id_map[
                            self._params.utr_non_standard_nucleotide_token
                        ]
                    )
                    for nucleotide in (
                        sequence.coding_sequence
                        if sequence.is_coding
                        else sequence.rna_sequence
                    )
                )
            else:
                all_token_ids.extend(
                    self._token_to_id_map[nucleotide]
                    for nucleotide in (
                        sequence.coding_sequence
                        if sequence.is_coding
                        else sequence.rna_sequence
                    )
                )

        if self._params.append_cds_eos_token:
            all_token_ids.append(self._token_to_id_map[self._params.cds_eos_token])

        # 3'UTR
        if sequence.is_coding and not self._params.cds_only:
            if self._params.use_non_standard_nucleotide_tokens:
                all_token_ids.extend(
                    (
                        self._token_to_id_map[nucleotide]
                        if nucleotide in RNA_NUCLEOTIDE_TYPES
                        else self._token_to_id_map[
                            self._params.utr_non_standard_nucleotide_token
                        ]
                    )
                    for nucleotide in sequence.three_prime_utr
                )
            else:
                all_token_ids.extend(
                    self._token_to_id_map[nucleotide]
                    for nucleotide in sequence.three_prime_utr
                )

        if self._params.append_eos_token and (
            not sequence.is_coding
            or sequence.three_prime_utr != ""
            or not self._params.append_cds_eos_token
        ):
            all_token_ids.append(self._token_to_id_map[self._params.eos_token])

        return (
            [self.id_to_token(token_id) for token_id in all_token_ids],
            all_token_ids,
        )

    def batch_tokenize(  # type: ignore[override]
        self, sequences: list[RNASequence]
    ) -> list[tuple[list[str], list[int]]]:
        """Tokenize a batch of sequences, using the tokenize method, and then padding
            each sequence accordingly.

        Args:
            sequences: a list of RNASequence objects.

        Returns:
            a list of tuples containing the list of tokens, and the list of token ids
            for each sequence.
        """
        tokenized_sequences = [
            list(self.tokenize(sequence)[1]) for sequence in sequences
        ]
        return self.pad_tokens_batch(  # type: ignore
            [
                (
                    [self.id_to_token(token_id) for token_id in tokenized_sequence],
                    tokenized_sequence,
                )
                for tokenized_sequence in tokenized_sequences
            ]
        )

    def pad_tokens_batch(
        self, batch: list[tuple[list[str], list[int]]]
    ) -> list[tuple[list[str], list[int]]]:
        """Pad a batch of tokenized sequences to the same fixed length.

        Args:
            batch: A list of tokenized sequences i.e. a list of tuples
                containing the list of tokens, and the list of token ids
                for each sequence.

        Returns:
            A list of padded tokenized sequences.
        """
        lengths = [len(t[0]) for t in batch]
        maximum_length = max(lengths)
        if maximum_length > self._fixed_length:
            raise ValueError(
                "Found a sequence with length that exceeds the permitted fixed length."
            )
        deltas = [self._fixed_length - length for length in lengths]
        padded_tokens = [
            t[0] + ([self.pad_token] * delta) for t, delta in zip(batch, deltas)
        ]
        padded_tokens_ids = [
            t[1] + ([self.pad_token_id] * delta) for t, delta in zip(batch, deltas)
        ]
        return [
            (toks, toks_ids) for toks, toks_ids in zip(padded_tokens, padded_tokens_ids)
        ]

    def padding_mask(self, all_token_ids: jnp.ndarray) -> jnp.ndarray:
        return 1 * (all_token_ids != self.pad_token_id)  # type: ignore
