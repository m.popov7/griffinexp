from typing import Dict, List, Optional, Tuple

from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer


class HyenaDNATokenizer(FixedSizeStandardTokenizer):

    """
    Defines a tokenizer for the Hyena DNA model. The only difference in behavior
    compared to the FixedSizeStandardTokenizer is that there is optionality to append a
    <sep> token and dictate if to pad on the right or left of the original sequence.
    """

    def __init__(
        self,
        standard_tokens: List[str],
        fixed_length: int,
        unk_token: str = "<unk>",
        pad_token: str = "<pad>",
        mask_token: str = "<mask>",
        class_token: str = "<cls>",
        eos_token: str = "<eos>",
        bos_token: str = "<bos>",
        sep_token: str = "<sep>",
        prepend_bos_token: bool = False,
        prepend_cls_token: bool = False,
        append_eos_token: bool = False,
        append_sep_token: bool = False,
        extra_special_tokens: Optional[List[str]] = None,
        tokens_to_ids: Optional[Dict[str, int]] = None,
        padding_side: str = "left",
    ):

        """
        Initializes a hyena tokenizer instance.

        Args:
            standard_tokens: Standard tokens, where special tokens are omitted.
            fixed_length: Fixed length to pad all sequences in the batches
            unk_token: Unknown token.
            pad_token: Pad token.
            mask_token: Mask token.
            class_token: Class token.
            eos_token: End of speech tokens.
            bos_token: Beginning of sentence token.
            sep_token: Separation token.
            prepend_bos_token: Prepend beginning of sentence token.
            prepend_cls_token: Prepend class token.
            append_eos_token: Append end of speech token.
            append_sep_token: Append separation token.
            extra_special_tokens: (Optional) Enable the user to define optionally
                additional special tokens. Since regex is used for tokenization, any
                special tokens that are also special tokens in regex must include
                a "\" escape seq. For instance "$" -> "\\$"
            tokens_to_ids: (Optional) Enable the user to optionally choose ids for
                the tokens. If you provide this argument the dictionary must include
                the following special tokens
                ["<unk>","<pad>","<mask>","<cls>","<eos>","<bos>"]
                or instantiation will fail. Additionally, if the ids in your dictionary
                do not start at 0 then an error will also be raised. If this argument is
                not specified, then ids are attributed automatically by the tokenizer
                during initialization.
            padding_side: Whether to pad on the left or right of a sequence. Defaults to
                left side padding

        """

        # automatically add sep token to special tokens
        if extra_special_tokens:
            extra_special_tokens.append(sep_token)
        else:
            extra_special_tokens = [sep_token]

        super().__init__(
            standard_tokens=standard_tokens,
            fixed_length=fixed_length,
            unk_token=unk_token,
            pad_token=pad_token,
            mask_token=mask_token,
            class_token=class_token,
            eos_token=eos_token,
            bos_token=bos_token,
            prepend_bos_token=prepend_bos_token,
            prepend_cls_token=prepend_cls_token,
            append_eos_token=append_eos_token,
            extra_special_tokens=extra_special_tokens,
            tokens_to_ids=tokens_to_ids,
        )

        self._sep_token = sep_token
        self._padding_side = padding_side
        self._append_sep_token = append_sep_token

        if padding_side not in ["right", "left"]:
            raise ValueError(
                f"Padding side argument must be either right or left. "
                f"You passed {padding_side}."
            )

        if append_sep_token and append_eos_token:
            raise ValueError(
                "Cannot append both eos token and sep token. Must only " "choose one."
            )

    @property
    def sep_token(self) -> str:
        return self._sep_token

    def tokenize(self, sequence: str) -> Tuple[List[str], List[int]]:
        """
        Tokenizes a sequence and returns the list of tokens as well
        as the list of their IDs. Any character found in the sequence that does not
        correspond to any token in the vocabulary is replaced by the unk token. ***This
        differs from the StandardTokenizer implementation by optionally appending the
        sep token.

        Args:
            sequence: Sequence to be tokenized.

        Returns:
            List of tokens.
            List of token ids.
        """
        tokens: List[str] = self._compiled_regex.findall(sequence)
        tokens = [
            tok if tok in self._tokens_to_ids.keys() else self._unk_token
            for tok in tokens
        ]
        if self._prepend_cls_token:
            tokens = [self._class_token] + tokens

        if self._prepend_bos_token:
            tokens = [self._bos_token] + tokens

        if self._append_eos_token:
            tokens.append(self._eos_token)

        if self._append_sep_token:
            tokens.append(self._sep_token)

        tokens_ids = [self.token_to_id(tok) for tok in tokens]

        return tokens, tokens_ids

    def pad_tokens_batch(
        self, batch: List[Tuple[List[str], List[int]]]
    ) -> List[Tuple[List[str], List[int]]]:
        """
        Takes tokens and tokens ids of a batch of sequences and returns a batch of
        padded sequences. ***The difference between this implementation and the
        FixedSizeStandardTokenizer is that it can pad on either side of the tokenized
        sequence.

        Args:
            batch: List of tuples, each composed of a sequence's tokens and token ids.

        Returns:
            The padded list, where every sequence is padded to the pre-defined
            max length.
        """
        lengths = [len(t[0]) for t in batch]
        maximum_length = max(lengths)
        if maximum_length > self._fixed_length:
            raise ValueError(
                f"Found a sequence with length {maximum_length}  that "
                f"exceeds the fixed length to tokenize ({self._fixed_length} )"
            )
        deltas = [self._fixed_length - length for length in lengths]

        # pads on the right
        if self._padding_side == "right":
            padded_tokens = [
                t[0] + ([self.pad_token] * delta) for t, delta in zip(batch, deltas)
            ]
            padded_tokens_ids = [
                t[1] + ([self.pad_token_id] * delta) for t, delta in zip(batch, deltas)
            ]

        # pads on the left
        elif self._padding_side == "left":
            padded_tokens = [
                ([self.pad_token] * delta) + t[0] for t, delta in zip(batch, deltas)
            ]
            padded_tokens_ids = [
                ([self.pad_token_id] * delta) + t[1] for t, delta in zip(batch, deltas)
            ]

        return [
            (toks, toks_ids) for toks, toks_ids in zip(padded_tokens, padded_tokens_ids)
        ]
