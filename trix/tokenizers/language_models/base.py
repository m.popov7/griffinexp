from abc import ABC, abstractmethod
from typing import List, Tuple


class BaseTokenizer(ABC):
    """
    This class represents a possible general abstraction for tokenizers in Trix.
    All the tokenizers used in examples will inherit from this class. However, users may
    themselves define other kinds of tokenizers as they see fit. In this abstraction, we
    distinguish standard tokens (e.g. words in english, or amino-acids in proteomics)
    from special tokens (e.g. pad token, mask token, etc ...). This abstraction
    introduces a set of special tokens that we found to be useful as defaults. However,
    classes that inherit from this abstraction can add other special tokens or not use
    any of the ones defined by default.
    """

    @property
    @abstractmethod
    def vocabulary(self) -> List[str]:
        """
        Property that returns the list of all tokens (in str representation)
        used by that tokenizer.

        Returns:
            The list of all tokens (in str representation) used by that tokenizer.
        """
        pass

    @property
    def vocabulary_size(self) -> int:
        """
        Property that returns the total number of tokens.

        Returns:
            Total number of tokens.
        """
        return len(self.vocabulary)

    @property
    @abstractmethod
    def standard_tokens(self) -> List[str]:
        """
        Property that returns the list of standards tokens (in str representation)
        used by that tokenizer.

        Returns:
            The list of standards tokens (in str representation) used by that tokenizer.
        """
        pass

    @property
    @abstractmethod
    def special_tokens(self) -> List[str]:
        """
        Property that returns the list of special tokens (in str representation)
        used by that tokenizer.

        Returns:
            The list of special tokens (in str representation) used by that tokenizer.
        """
        pass

    @property
    @abstractmethod
    def unk_token(self) -> str:
        """
        Property that returns the str representation of the unknown token.

        Returns:
            Str representation of the unknown token.
        """
        pass

    @property
    @abstractmethod
    def pad_token(self) -> str:
        """
        Property that returns the str representation of the pad token.

        Returns:
            Str representation of the pad token.
        """
        pass

    @property
    @abstractmethod
    def mask_token(self) -> str:
        """
        Property that returns the str representation of the mask token.

        Returns:
            Str representation of the mask token.
        """
        pass

    @property
    @abstractmethod
    def class_token(self) -> str:
        """
        Property that returns the str representation of the class token. Note that
        class and bos (begin of sequence) tokens can sometimes be confused. We introduce
        both  tokens for more granularity. For instance, one might append a class token
        at the beginning of a sequence that goes into an encoder and a bos token
        at the beginning of a sequence that goes into a decoder.

        Returns:
            Str representation of the class token.
        """
        pass

    @property
    @abstractmethod
    def eos_token(self) -> str:
        """
        Property that returns the str representation of the end of sequence token.

        Returns:
            Str representation of the end of sequence token.
        """
        pass

    @property
    @abstractmethod
    def bos_token(self) -> str:
        """
        Property that returns the str representation of the bos (beginning of sequence)
        token. Note that class and bos tokens can be sometimes be confused. We introduce
        both  tokens for more granularity. For instance, one might append a class token
        at the beginning of a sequence that goes into an encoder and a bos token
        at the beginning of a sequence that goes into a decoder.

        Returns:
            Str representation of the bos token.
        """
        pass

    @property
    def unk_token_id(self) -> int:
        """
        Property that returns id (int representation) of the unknown token.

        Returns:
            Id (int representation) of the unknown token.
        """
        return self.token_to_id(self.unk_token)

    @property
    def pad_token_id(self) -> int:
        """
        Property that returns id (int representation) of the pad token.

        Returns:
            Id (int representation) of the pad token.
        """
        return self.token_to_id(self.pad_token)

    @property
    def mask_token_id(self) -> int:
        """
        Property that returns id (int representation) of the mask token.

        Returns:
            Id (int representation) of the mask token.
        """
        return self.token_to_id(self.mask_token)

    @property
    def class_token_id(self) -> int:
        """
        Property that returns id (int representation) of the class token.

        Returns:
            Id (int representation) of the class token.
        """
        return self.token_to_id(self.class_token)

    @property
    def eos_token_id(self) -> int:
        """
        Property that returns id (int representation) of the eos token.

        Returns:
            Id (int representation) of the eos token.
        """
        return self.token_to_id(self.eos_token)

    @property
    def bos_token_id(self) -> int:
        """
        Property that returns id (int representation) of the bos token.

        Returns:
            Id (int representation) of the bos token.
        """
        return self.token_to_id(self.bos_token)

    @abstractmethod
    def id_to_token(self, token_id: int) -> str:
        """
        Retrieves the str representation of a token from its id (int representation).

        Args:
            token_id: Id of the token.

        Returns:
            The str representation of the token.
        """
        pass

    @abstractmethod
    def token_to_id(self, token: str) -> int:
        """
        Retrieves the id (int representation) of a token from its str representation.

        Args:
            token: The str representation of the token.

        Returns:
            The id of the token.
        """

    @abstractmethod
    def tokenize(self, sequence: str) -> Tuple[List[str], List[int]]:
        """
        Tokenizes a sequence and returns the list of tokens as well
        as the list of their IDs. Any character found in the sequence that does not
        correspond to any token in the vocabulary is replaced by the unk token.

        Args:
            sequence: The sequence to be tokenized.

        Returns:
            A 2-elements tuple containing 1. the list of the str representations of the
            tokens and 2. the list of the int representations of the tokens.
        """
        pass

    @abstractmethod
    def batch_tokenize(self, sequences: List[str]) -> List[Tuple[List[str], List[int]]]:
        """
        Tokenizes a batch of sequences.

        Args:
            sequences: List of sequences to be tokenized.

        Returns:
            List of 2-elements tuple for each sequence in the input where the tuple is
            containing 1. the list of the str representations of the
            tokens for that sequence and 2. the list of the int representations of
            the tokens for that sequence.
        """
        pass

    def pad_tokens_batch(
        self, batch: List[Tuple[List[str], List[int]]]
    ) -> List[Tuple[List[str], List[int]]]:
        """
        Takes a batch of sequences tokens ids and returns a batch of padded sequences.

        Args:
            batch: List of tuples, each composed of a sequence's tokens and token ids.

        Returns:
            List of 2-elements tuple for each sequence in the input where the tuple is
            containing 1. the list of the str representations of the
            tokens for that sequence and 2. the list of the int representations of
            the tokens for that sequence. Pad Tokens are added so that each sequence
            of tokens in the batch has the same length (all sequences padded to the
            length of the longest sequence in the batch).
        """
        lengths = [len(t[0]) for t in batch]
        maximum_length = max(lengths)
        deltas = [maximum_length - length for length in lengths]
        padded_tokens = [
            t[0] + ([self.pad_token] * delta) for t, delta in zip(batch, deltas)
        ]
        padded_tokens_ids = [
            t[1] + ([self.pad_token_id] * delta) for t, delta in zip(batch, deltas)
        ]
        return [
            (toks, toks_ids) for toks, toks_ids in zip(padded_tokens, padded_tokens_ids)
        ]
