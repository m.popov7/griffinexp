import functools
from dataclasses import dataclass
from typing import Dict, List

import jax
import jax.numpy as jnp

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.tokenizers.rl.base import RLTokenizer, Transition
from trix.types import RNGKey, Tokens
from trix.utils.rl import (
    compute_cvt_centroids,
    compute_inverse_mu_law,
    compute_mu_law,
    get_cells_indices,
    get_mask_from_dones,
)


@dataclass
class VoronoiTokenizerParams:
    observation_size: int
    action_size: int
    num_centroids: int = 1024
    num_init_cvt_samples: int = 50000
    mu_law_mu: int = 100
    mu_law_m: int = 256  # mu_law parameters values taken from GATO
    add_separator_token: bool = False
    cache_centroids: bool = True
    prepend_bos_token: bool = False


class VoronoiTokenizer(BaseTokenizer, RLTokenizer):
    """
    Tokenizer that tokenize inputs by directly discretizing the whole input space,
    using a Centroidal Voronoi Tessellation (CVT). Hence, each observation vector is
    encoded into one single value, as opposed to GATO-style tokenization where each
    observation component is encoded into a single value. Same holds for actions.
    """

    def __init__(self, random_key: RNGKey, params: VoronoiTokenizerParams):
        self._mu_law_mu = params.mu_law_mu
        self._mu_law_m = params.mu_law_m
        self._observation_size = params.observation_size
        self._action_size = params.action_size
        self._num_centroids = params.num_centroids
        self._add_separator_token = params.add_separator_token
        self._prepend_bos_token = params.prepend_bos_token
        self._obs_centroids, random_key = compute_cvt_centroids(
            random_key=random_key,
            num_descriptors=params.observation_size,
            num_init_cvt_samples=params.num_init_cvt_samples,
            num_centroids=params.num_centroids,
            cache_centroids=params.cache_centroids,
            minval=-1.0,
            maxval=1.0,
        )

        self._action_centroids, _ = compute_cvt_centroids(
            random_key=random_key,
            num_descriptors=params.action_size,
            num_init_cvt_samples=params.num_init_cvt_samples,
            num_centroids=params.num_centroids,
            cache_centroids=params.cache_centroids,
            minval=-1.0,
            maxval=1.0,
        )

        standard_tokens = list(map(str, range(2 * params.num_centroids)))

        self._unk_token = "<unk>"
        self._pad_token = "<pad>"
        self._mask_token = "<mask>"
        self._class_token = "<cls>"
        self._eos_token = "<eos>"
        self._bos_token = "<bos>"

        special_tokens = [
            self._unk_token,
            self._pad_token,
            self._mask_token,
            self._class_token,
            self._eos_token,
            self._bos_token,
        ]

        self._all_tokens = special_tokens + standard_tokens
        self._standard_tokens = standard_tokens
        self._special_tokens = special_tokens
        self._tokens_to_ids = {tok: i for i, tok in enumerate(self._all_tokens)}
        self._ids_to_tokens = {i: tok for tok, i in self._tokens_to_ids.items()}

    @property
    def vocabulary(self) -> List[str]:
        return self._all_tokens

    @property
    def standard_tokens(self) -> List[str]:
        return self._standard_tokens

    @property
    def special_tokens(self) -> List[str]:
        return self._special_tokens

    @property
    def unk_token(self) -> str:
        return self._unk_token

    @property
    def pad_token(self) -> str:
        return self._pad_token

    @property
    def mask_token(self) -> str:
        return self._mask_token

    @property
    def class_token(self) -> str:
        return self._class_token

    @property
    def eos_token(self) -> str:
        return self._eos_token

    @property
    def bos_token(self) -> str:
        return self._bos_token

    @property
    def observation_size(self) -> int:
        return self._observation_size

    @property
    def action_size(self) -> int:
        return self._action_size

    @property
    def centroids(self) -> Dict[str, jnp.ndarray]:
        """
        The centroids that define the discretization space for obs and actions.
        """

        return {
            "action_centroids": self._action_centroids,
            "obs_centroids": self._obs_centroids,
        }

    @property
    def concat_size(self) -> int:
        """
        Size of the concatenation of obs tokens and actions tokens, with optionally
        a separator token.
        """
        concat_size = 1 + 1
        if self._add_separator_token:
            concat_size += 1
        return concat_size

    def id_to_token(self, token_id: int) -> str:
        try:
            token = self._ids_to_tokens[token_id]
            return token
        except KeyError:
            raise KeyError("Token id not found in vocabulary")

    def token_to_id(self, token: str) -> int:
        return self._tokens_to_ids.get(token, -1)

    @functools.partial(jax.jit, static_argnames=("self",))
    def tokenize_obs(
        self,
        obs: jnp.ndarray,
    ) -> jnp.ndarray:
        """
        Component-wise tokenization of an array of observations.

        Args:
            obs: Array of observations of any shape with float values.

        Returns:
            A jnp.ndarray of type jnp.int32 with the token ids corresponding to
                the component-wise tokenization of the input
        """
        original_obs_shape = obs.shape

        # Project obs into [-1,1] interval.
        obs = compute_mu_law(x=obs, mu=self._mu_law_mu, m=self._mu_law_m)
        obs = jnp.clip(obs, a_min=-1.0, a_max=1.0)

        # Reshape to match the shape format required by get_cells_indices
        obs = jnp.reshape(obs, (-1, self.observation_size))
        tokens = get_cells_indices(obs, self._obs_centroids)
        # Reshape back to original shape (setting aside the last dimension)
        tokens = jnp.reshape(tokens, (*original_obs_shape[:-1], 1))

        tokens = tokens + len(self._special_tokens)
        return tokens.astype(jnp.int32)

    @functools.partial(jax.jit, static_argnames=("self",))
    def tokenize_actions(
        self,
        actions: jnp.ndarray,
    ) -> jnp.ndarray:
        """
        Component-wise tokenization of an array of actions.
        Assumes that actions are within [-1,1].

        Args:
            actions: Array of actions of any shape with float values.

        Returns:
            A jnp.ndarray of type jnp.int32 with the token ids corresponding to
                the component-wise tokenization of the input.
        """
        original_actions_shape = actions.shape

        # Reshape to match the shape format required by get_cells_indices
        actions = jnp.reshape(actions, (-1, self._action_size))
        tokens = get_cells_indices(actions, self._action_centroids)

        # Reshape back to original shape (setting aside the last dimension)
        tokens = jnp.reshape(tokens, (*original_actions_shape[:-1], 1))

        # shift action tokens to differentiate from obs tokens
        tokens = tokens + self._num_centroids + len(self._special_tokens)

        return tokens.astype(jnp.int32)

    def tokenize(self, transitions: Transition) -> Dict[str, Tokens]:  # type: ignore
        """
        Transforms a trajectory into a fixed-sized array using the GATO conventions.

        Args:
            transitions: Sequence of Transition object, each leaves has shape
                (episode_length, *).

        Returns:
            Dictionary containing fixed-size tokens and positions, to pass to the
                model. Leaves shape
                (concat_size*episode_length, max(3, obs_size, action_size)).
        """

        episode_length = transitions.obs.shape[0]

        # Get token ids for obs and actions
        tokenized_obs = self.tokenize_obs(transitions.obs)
        tokenized_actions = self.tokenize_actions(transitions.actions)

        sep_tokens = self.class_token_id * jnp.ones(
            shape=(episode_length, 1), dtype=jnp.int32
        )

        if self._add_separator_token:
            tokenized_trans = jnp.concatenate(
                [tokenized_obs, sep_tokens, tokenized_actions], axis=-1
            )
        else:
            tokenized_trans = jnp.concatenate(
                [tokenized_obs, tokenized_actions], axis=-1
            )

        # pad Done transitions
        mask = get_mask_from_dones(transitions.dones)
        tokenized_trans = jnp.where(
            mask[..., None],
            tokenized_trans,
            jnp.ones_like(tokenized_trans) * self.pad_token_id,
        )

        tokens = jnp.reshape(
            tokenized_trans,
            newshape=(-1),
        )

        if self._prepend_bos_token:
            start_tokens = self.bos_token_id * jnp.ones(shape=(1,), dtype=jnp.int32)
            tokens = jnp.concatenate([start_tokens, tokens], axis=-1)

        positions = self.get_positions(episode_length=episode_length)

        return {"tokens": tokens, "positions": positions}

    def batch_tokenize(  # type: ignore
        self, transitions: Transition
    ) -> Dict[str, Tokens]:
        """
        Vmapped version of `tokenize`.

        Args:
            transitions: A batch of transitions sequences. Leaves shape:
                (batch_size, episode_length, *)

        Returns:
            Dictionary containing fixed-size tokens and positions, to pass to the
                model. Leaves shape:
                (batch_size, concat_size*episode_length, max(3, obs_size, action_size)).
        """
        return jax.vmap(self.tokenize)(transitions)  # type: ignore

    def decode(
        self,
        tokens: Tokens,
    ) -> Dict[str, jnp.ndarray]:
        """
        Decodes a token to retrieve the inputtted values.

        Args:
            tokens: Tokens.

        Returns:
            Dictionary containing the decoded values.
        """
        # Encoded tokens have been shifted to include utility tokens into vocabulary,
        # hence the need to unshift them
        action_offset = len(self._special_tokens) + self._num_centroids
        obs_offset = len(self._special_tokens)

        seq_len = tokens.shape[-1]
        horizon_size = seq_len // self.concat_size

        tokens = tokens.reshape(-1, horizon_size, self.concat_size)

        actions = tokens[..., -1] - action_offset
        obs = tokens[..., 0] - obs_offset

        actions = jnp.take(self._action_centroids, actions, axis=0)
        obs = jnp.take(self._obs_centroids, obs, axis=0)

        obs = compute_inverse_mu_law(obs, mu=self._mu_law_mu, m=self._mu_law_m)

        return {"actions": actions, "obs": obs}

    def get_positions(self, episode_length: int) -> jnp.ndarray:
        """
        Returns an array that contains the timesteps positions.

        Args:
            episode_length: Length of the sequences to be tokenized.

        Returns:
            Positions.
        """
        x = jnp.arange(episode_length)

        num_repeats = self.concat_size

        x = jnp.repeat(x, num_repeats, axis=0)

        if self._prepend_bos_token:
            x = x + 1
            x = jnp.concatenate([jnp.asarray([0]), x], axis=0)

        return x
