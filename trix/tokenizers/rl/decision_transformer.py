from typing import Dict, Optional

import jax
import jax.numpy as jnp

from trix.tokenizers.rl.base import RLTokenizer, Transition
from trix.types import Tokens


class DecisionTransformerTokenizer(RLTokenizer):
    """
    WARNING: This is not tokenizer per se, because the DecisionTransformer processes
    continuous inputs, this is why this class doesn't inherit from `BaseTokenizer`.

    This object processes a sequence of trajectories into an array of
    fixed-sized values. Specifically it will output an interleaved sequence
    (r0, s0, a0, r1, s1, a1, ...)
    with
        r: a concatenation of (returns_to_go, dones, positions)
        s: the observations
        a: the actions

    Note: For the sake of having a fixed sized array, special padding tokens
    are append in the axis of the dimensionality of the objects (ie. observation
    size/action size/return-to-go size).
    """

    def __init__(
        self,
        action_size: int,
        observation_size: int,
        return_scaling: float = 1.0,
        feature_pad_value: float = jnp.nan,
    ):
        self._feature_pad_value = feature_pad_value
        self._return_scaling = return_scaling
        self._action_size = action_size
        self._observation_size = observation_size

    @property
    def concat_size(self) -> int:

        """
        Size of the concatenation of returns-to-go, obs and actions tokens. As
        observations and actions are compressed by the DecisionTransformer it's 3.
        """
        return 3

    @property
    def action_size(self) -> int:
        return self._action_size

    @property
    def observation_size(self) -> int:
        return self._observation_size

    def tokenize(
        self,
        transitions: Transition,
        target_return: Optional[jnp.ndarray] = None,
    ) -> Dict[str, Tokens]:
        """
        Transforms a trajectory into a fixed-sized array using the DecisionTransformer
            conventions.

        Args:
            transitions: Transitions (or trajectory), each leaves has shape
                (episode_length, *).
            target_return: (Optional) the target return to compute the return_to_go,
                if no value is passed then the target return is set to the value of
                the episode return.

        Returns:
            Dictionary containing fixed-size tokens and positions, to pass to the
                model. Leaves shape:
                (concat_size*episode_length, max(3, obs_size, action_size)).
        """

        actions = transitions.actions
        dones = transitions.dones
        rewards = transitions.rewards
        obs = transitions.obs

        if target_return is None:
            _target_return = jnp.sum(rewards)
        else:
            _target_return = target_return

        returns_to_go = _target_return - jnp.cumsum(rewards)
        returns_to_go *= self._return_scaling

        episode_length = obs.shape[0]

        obs_size = obs.shape[-1]
        action_size = actions.shape[-1]

        returns_to_go = returns_to_go.reshape(episode_length, 1)
        dones = dones.reshape(episode_length, 1)
        positions = jnp.arange(episode_length).reshape(episode_length, 1)
        returns_to_go = jnp.concatenate([returns_to_go, dones], axis=1)
        max_size = max(obs_size, action_size, 2)
        obs = jnp.pad(
            obs,
            ((0, 0), (0, max_size - obs_size)),
            constant_values=self._feature_pad_value,
        )
        actions = jnp.pad(
            actions,
            ((0, 0), (0, max_size - action_size)),
            constant_values=self._feature_pad_value,
        )
        returns = jnp.pad(
            returns_to_go,
            ((0, 0), (0, max_size - 2)),
            constant_values=self._feature_pad_value,
        )
        # r_t = (returns-to-go_t, dones_t, pad, pad, pad, ...)

        # Interleave tokens as (r_1, s_1, a_1, r_2, s_2, a_2, ...)
        x = jnp.stack((returns, obs, actions), axis=1)
        x = x.reshape(self.concat_size * episode_length, max_size)

        positions = self.get_positions(episode_length)
        return {"tokens": x, "positions": positions}

    def batch_tokenize(
        self,
        transitions: Transition,
        target_returns: Optional[jnp.ndarray] = None,  # type: ignore
    ) -> Dict[str, Tokens]:
        """
        Vmapped version of `tokenize`.

        Args:
            transitions: A batch of transitions sequences. Leaves shape:
                (batch_size, episode_length, *).
            target_returns: (Optional) a batch of target returns to compute the
                return_to_go, if no value is passed then the target returns are set to
                the values of the episode returns.

        Returns:
            Dictionary containing fixed-size tokens and positions, to pass to the
                model. Leaves shape:
                (batch_size, concat_size*episode_length, max(3, obs_size, action_size)).
        """
        if target_returns is None:
            return jax.vmap(self.tokenize, in_axes=(0, None))(  # type: ignore
                transitions, target_returns
            )
        else:
            return jax.vmap(self.tokenize)(transitions, target_returns)  # type: ignore

    def get_positions(self, episode_length: int) -> jnp.ndarray:

        """
        Returns an array that contains the timesteps positions (e.g. return-to-go,
        obs and actions have the same timestep position for a given timestep).

        Args:
            episode_length: Length of the sequences to be tokenized.

        Returns:
            Positions.
        """
        x = jnp.arange(episode_length)
        num_repeats = self.concat_size

        positions = jnp.repeat(x, num_repeats, axis=0)
        return positions

    def decode(
        self,
        tokens: Tokens,
    ) -> Dict[str, jnp.ndarray]:
        """
        Decodes a token to retrieve the inputted values.

        Args:
            tokens: Tokens.

        Returns:
            Dictionary containing the decoded values.
        """

        # Encoded tokens have been shifted to include utility tokens into vocabulary,
        # hence the need to unshift them
        seq_len = tokens.shape[1]
        episode_length = seq_len // self.concat_size

        # Returns to go are at index (0,3,6,...)
        returns_to_go_index = jnp.arange(episode_length) * 3
        obs_index = returns_to_go_index + 1
        actions_index = returns_to_go_index + 2

        obs = tokens[:, obs_index][..., : self.observation_size]
        actions = tokens[:, actions_index][..., : self.action_size]
        returns_to_go = tokens[:, returns_to_go_index][..., 0]
        dones = tokens[:, returns_to_go_index][..., 1].astype(jnp.int32)

        return {
            "obs": obs,
            "actions": actions,
            "returns_to_go": returns_to_go,
            "dones": dones,
        }
