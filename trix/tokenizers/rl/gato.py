import functools
from typing import Dict, List

import jax
import jax.numpy as jnp

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.tokenizers.rl.base import RLTokenizer, Transition
from trix.types import Tokens
from trix.utils.rl import compute_inverse_mu_law, compute_mu_law, get_mask_from_dones


class GATOTokenizer(BaseTokenizer, RLTokenizer):
    """
    Tokenizer that constructs GATO-like sequences.
    """

    def __init__(
        self,
        observation_size: int,
        action_size: int,
        num_quantization_tokens: int,
        mu_law_mu: int = 100,
        mu_law_m: int = 256,  # mu_law parameters values taken from GATO
        add_separator_token: bool = True,
        prepend_bos_token: bool = False,
    ):
        self._num_quantization_tokens = num_quantization_tokens
        self._bins = jnp.linspace(-1, 1.000001, self._num_quantization_tokens + 1)
        self._observation_size = observation_size
        self._action_size = action_size
        self._num_bins = num_quantization_tokens
        self._mu_law_mu = mu_law_mu
        self._mu_law_m = mu_law_m
        self._add_separator_token = add_separator_token
        self._prepend_bos_token = prepend_bos_token

        standard_tokens = list(map(str, range(num_quantization_tokens)))

        self._unk_token = "<unk>"
        self._pad_token = "<pad>"
        self._mask_token = "<mask>"
        self._class_token = "<cls>"
        self._eos_token = "<eos>"
        self._bos_token = "<bos>"

        special_tokens = [
            self._unk_token,
            self._pad_token,
            self._mask_token,
            self._class_token,
            self._eos_token,
            self._bos_token,
        ]

        self._all_tokens = special_tokens + standard_tokens
        self._standard_tokens = standard_tokens
        self._special_tokens = special_tokens

        self._tokens_to_ids = {tok: i for i, tok in enumerate(self._all_tokens)}
        self._ids_to_tokens = {i: tok for tok, i in self._tokens_to_ids.items()}

    @property
    def vocabulary(self) -> List[str]:
        return self._all_tokens

    @property
    def standard_tokens(self) -> List[str]:
        return self._standard_tokens

    @property
    def special_tokens(self) -> List[str]:
        return self._special_tokens

    @property
    def unk_token(self) -> str:
        return self._unk_token

    @property
    def pad_token(self) -> str:
        return self._pad_token

    @property
    def mask_token(self) -> str:
        return self._mask_token

    @property
    def class_token(self) -> str:
        return self._class_token

    @property
    def eos_token(self) -> str:
        return self._eos_token

    @property
    def bos_token(self) -> str:
        return self._bos_token

    @property
    def observation_size(self) -> int:
        return self._observation_size

    @property
    def action_size(self) -> int:
        return self._action_size

    @property
    def bins(self) -> jnp.ndarray:
        """
        The bins that define the discretization space for obs and actions.
        """
        bins: jnp.ndarray = self._bins
        return bins

    @property
    def concat_size(self) -> int:
        """
        Size of the concatenation of obs tokens and actions tokens, with optionally
        a separator token.
        """
        concat_size = self.action_size + self.observation_size
        if self._add_separator_token:
            concat_size += 1
        return concat_size

    @property
    def num_quantization_tokens(self) -> int:
        """
        Number of bins for quantization.
        """
        return self.num_quantization_tokens

    def id_to_token(self, token_id: int) -> str:
        try:
            token = self._ids_to_tokens[token_id]
            return token
        except KeyError:
            raise KeyError("Token id not found in vocabulary")

    def token_to_id(self, token: str) -> int:
        return self._tokens_to_ids.get(token, -1)

    @functools.partial(jax.jit, static_argnames=("self",))
    def tokenize_obs(
        self,
        obs: jnp.ndarray,
    ) -> jnp.ndarray:
        """
        Component-wise tokenization of an array of observations.

        Args:
            obs: Observations of any shape with float values.

        Returns:
            A jnp.ndarray of type jnp.int32 with the token ids corresponding to
                the component-wise tokenization of the input.
        """
        # Project obs into [-1,1] interval.
        obs = compute_mu_law(x=obs, mu=self._mu_law_mu, m=self._mu_law_m)
        obs = jnp.clip(obs, a_min=-1.0, a_max=1.0)
        tokens = jnp.digitize(obs, self.bins)
        tokens = tokens.astype(jnp.int32)
        tokens = tokens + len(self._special_tokens) - 1
        return tokens

    @functools.partial(jax.jit, static_argnames=("self",))
    def tokenize_actions(
        self,
        actions: jnp.ndarray,
    ) -> jnp.ndarray:
        """
        Component-wise tokenization of an array of actions.
        Assumes that actions are within [-1,1].

        Args:
            actions: Actions of any shape with float values.

        Returns:
            A jnp.ndarray of type jnp.int32 with the token ids corresponding to
                the component-wise tokenization of the input.
        """
        tokens = jnp.digitize(actions, self.bins)
        tokens = tokens.astype(jnp.int32)
        tokens = tokens + len(self._special_tokens) - 1
        return tokens

    @functools.partial(jax.jit, static_argnames=("self",))
    def tokenize_rewards(
        self,
        rewards: jnp.ndarray,
    ) -> jnp.ndarray:
        """
        Component-wise tokenization of an array of rewards.

        Note: in GATO rewards are not included, but the tokenization scheme
            generalizes well to any type of inputs so it could be used for rewards
            too.

        Args:
            rewards: array of rewards.

        Returns:
            A jnp.ndarray of type jnp.int32 with the token ids corresponding to
                the component-wise tokenization of the input.
        """
        rewards = compute_mu_law(x=rewards, mu=self._mu_law_mu, m=self._mu_law_m)
        rewards = jnp.clip(rewards, a_min=-1.0, a_max=1.0)
        tokens = jnp.digitize(rewards, self.bins)
        tokens = tokens.astype(jnp.int32)
        tokens = tokens + len(self._special_tokens) - 1
        return tokens

    def tokenize(self, transitions: Transition) -> Dict[str, Tokens]:  # type: ignore
        """
        Transforms a trajectory into a fixed-sized array using the GATO conventions.

        Args:
            transitions: Transitions (or trajectory), each leaves has shape
                (episode_length, *).

        Returns:
            Dictionary containing fixed-size tokens and positions,
                to pass to the model. Leaves have shape:
                (concat_size*episode_length,)
        """
        episode_length = transitions.obs.shape[0]

        obs = self.tokenize_obs(transitions.obs)
        actions = self.tokenize_actions(transitions.actions)

        tokenized_transitions = Transition(
            obs=obs,
            actions=actions,
            dones=transitions.dones,
            rewards=transitions.rewards,
        )

        if self._add_separator_token:
            sep_tokens = self.class_token_id * jnp.ones(
                shape=(episode_length, 1), dtype=jnp.int32
            )
            tokens = jnp.concatenate(
                [tokenized_transitions.obs, sep_tokens, tokenized_transitions.actions],
                axis=-1,
            )
        else:
            tokens = jnp.concatenate(
                [tokenized_transitions.obs, tokenized_transitions.actions], axis=-1
            )

        # Mask data after the end of the episode
        mask = get_mask_from_dones(transitions.dones)
        tokens = jnp.where(
            mask[..., None],
            tokens,
            self.pad_token_id,
        )

        tokens = jnp.reshape(tokens, newshape=(-1,), order="C")

        if self._prepend_bos_token:
            start_tokens = self.bos_token_id * jnp.ones(shape=(1,), dtype=jnp.int32)
            tokens = jnp.concatenate([start_tokens, tokens], axis=-1)

        positions = self.get_positions(episode_length=episode_length)

        return {"tokens": tokens, "positions": positions}

    def batch_tokenize(  # type: ignore
        self, transitions: Transition
    ) -> Dict[str, Tokens]:
        """
        Vmapped version of `tokenize`.

        Args:
            transitions: A batch of transitions sequences. Leaves shape:
                (batch_size, episode_length, *)

        Returns:
            Dictionary containing fixed-size tokens and positions,
                to pass to the model. Leaves have shape:
                (batch_size, concat_size*episode_length)
        """
        return jax.vmap(self.tokenize)(transitions)  # type: ignore

    def get_positions(
        self,
        episode_length: int,
    ) -> jnp.ndarray:
        """
        Returns an array that contains the timesteps positions (e.g. every observation
        in the same timestep in considered to have the same position).

        Args:
            episode_length: Length of the sequences to be tokenized.

        Returns:
            Positions.
        """
        x = jnp.arange(episode_length)
        num_repeats = self.concat_size

        x = jnp.repeat(x, num_repeats, axis=0)

        if self._prepend_bos_token:
            x = x + 1
            x = jnp.concatenate([jnp.asarray([0]), x], axis=0)
        return x

    def decode(
        self,
        tokens: Tokens,
    ) -> Dict[str, jnp.ndarray]:
        """
        Decodes a token to retrieve the inputted values.

        Args:
            tokens: Tokens.

        Returns:
            Dictionary containing the decoded values.
        """

        # Encoded tokens have been shifted to include utility tokens into vocabulary,
        # hence the need to unshift them
        offset = len(self.special_tokens)
        tokens = tokens - offset
        tokens = self.bins[tokens]
        seq_len = tokens.shape[-1]
        horizon_size = seq_len // self.concat_size

        tokens = tokens.reshape(-1, horizon_size, self.concat_size)
        actions = tokens[..., -self.action_size :]
        obs = tokens[..., : self.observation_size]
        obs = compute_inverse_mu_law(obs, mu=self._mu_law_mu, m=self._mu_law_m)

        return {"obs": obs, "actions": actions}
