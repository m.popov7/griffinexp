from abc import ABC, abstractmethod
from typing import Dict, NamedTuple

import jax.numpy as jnp

from trix.types import Tokens


class Transition(NamedTuple):
    obs: jnp.ndarray
    actions: jnp.ndarray
    rewards: jnp.ndarray
    dones: jnp.ndarray


class RLTokenizer(ABC):
    """
    Abstract class that enforce having specific methods for the tokenizers designed
    for RL.
    """

    @property
    @abstractmethod
    def action_size(self) -> int:
        """
        The size of the action vector.
        """
        pass

    @property
    @abstractmethod
    def observation_size(self) -> int:
        """
        The size of the observation vector.
        """
        pass

    @property
    @abstractmethod
    def concat_size(self) -> int:
        """
        The size of the concatenation of all RL elements corresponding to a given
        step in the environment. Number of tokens per timestep.
        """
        pass

    @abstractmethod
    def tokenize(self, transitions: Transition) -> Dict[str, Tokens]:
        pass

    @abstractmethod
    def batch_tokenize(self, transitions: Transition) -> Dict[str, Tokens]:
        pass

    @abstractmethod
    def get_positions(self, episode_length: int) -> jnp.ndarray:
        """
        Returns an array that contains the timesteps number along the tokens in the
        sequence in order to compute the positional encodings.

        Args:
            episode_length: Length of the sequencse to be tokenized.

        Returns:
            Positions of the tokens in the trajectory.
        """
        pass

    @abstractmethod
    def decode(self, tokens: Tokens) -> Dict[str, jnp.ndarray]:
        """Decodes a token to retrieve the inputted values.

        Args:
            tokens: Tokens.

        Returns:
            Dictionary containing the decoded values.
        """
        pass
