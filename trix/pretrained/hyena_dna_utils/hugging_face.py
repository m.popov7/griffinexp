import json
import os
import re
from typing import Any, Dict, Tuple

import torch
from huggingface_hub import snapshot_download

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> str:
    """
    Get directory to save files (checkpoints, json config,etc...) on user machine.
    """
    return os.path.expanduser(
        os.path.join(
            os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR), "trix", "hyena_dna"
        )
    )


def parse_substring(orig_str: str) -> str:
    """
    Hack to handle matching keys between models trained with and without
    gradient checkpointing.
    """

    # modify for mixer keys
    pattern = r".mixer"
    parsed = ".mixer.layer"

    modified_string = re.sub(parsed, pattern, orig_str)

    # modify for mlp keys
    pattern = r".mlp"
    parsed = ".mlp.layer"

    modified_string = re.sub(parsed, pattern, modified_string)

    return modified_string


def process_torch_weights(
    pretrained_dict: torch.nn.ParameterDict,
    checkpointing: bool = False,
) -> Dict:
    """Loads pretrained (backbone and lm head) weights while handling some dict key
    differences. These key differences also must be made so that there are no
    discrepancies with the torch standalone code used for testing.

    Args:
        pretrained_dict: torch parameter dictionary with the hyena pretrained weights
        checkpointing: whether checkpointing was used in training

    Returns:
        The pretrained torch weights with processed keys.
    """

    new_dict = {}
    pattern = "model."

    # need to do some state dict "surgery"
    for key_loaded, _ in pretrained_dict.items():
        if "backbone" in key_loaded or "lm_head" in key_loaded:
            # the state dicts differ by one prefix, '.model', so we remove that
            key = re.sub(pattern, "", key_loaded)

            # need to add an extra ".layer" in key
            if checkpointing:
                key = parse_substring(key)

            new_dict[key] = pretrained_dict[key_loaded]

    return new_dict


def get_hyena_dna_from_hf(
    model_name: str,
) -> Tuple[torch.nn.ParameterDict, Dict[str, Any]]:
    """
    Downloads the pretrained hyena dna repo from hf and returns the pretrained config
    and weights.

    Args:
        model_name: Hyena dna model to download.

    Returns:
        HyenaDNA torch pretrained weights.
        Pretrained model config.
    """

    save_path = _get_dir()

    # download the model's hugging face repo, snap download handles checking if
    # folder already present in cache.
    repo_id = f"LongSafari/{model_name}"

    repo_path = snapshot_download(
        repo_id=repo_id, repo_type="model", cache_dir=save_path
    )

    # load model config from repo
    config = json.load(open(os.path.join(repo_path, "config.json")))

    # load model checkpoints
    loaded_ckpt = torch.load(
        os.path.join(repo_path, "weights.ckpt"),
        map_location=torch.device("cpu"),
    )

    # need to load weights slightly different if using gradient checkpointing
    if config.get("checkpoint_mixer", False):
        checkpointing = (
            config["checkpoint_mixer"] is True or config["checkpoint_mixer"] is True
        )
    else:
        checkpointing = False

    # grab state dict from both and load weights
    torch_params = process_torch_weights(
        loaded_ckpt["state_dict"], checkpointing=checkpointing
    )

    return torch_params, config
