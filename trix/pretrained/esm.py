"""Implementation of utilities to load a pretrained ESM model in Jax."""
import re
from collections import defaultdict
from pathlib import Path
from typing import Callable, Dict, List, Optional, Tuple

import esm
import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import torch

from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer


def convert_model_torch_to_jax(
    model: torch.nn.ParameterDict, model_name: str
) -> hk.Params:
    """
    Converts ESM PyTorch model to Haiku parameters.

    Args:
        model: Pretrained PyTorch ESM model.
        model_name: Name of the model (see ESM Github repository).

    Returns:
       Haiku parameters.
    """
    param_dict: Dict = defaultdict(lambda: {})

    # copy weights for the initial embedding layers
    param_dict[f"{model_name}/~/embed"][
        "embeddings"
    ] = model.embed_tokens.weight.detach().numpy()
    if hasattr(model, "embed_positions"):
        if hasattr(model.embed_positions, "weight"):
            param_dict[f"{model_name}/~/esm_learned_positional_embeddings/~/embed"][
                "embeddings"
            ] = model.embed_positions.weight.detach().numpy()
        if hasattr(model.emb_layer_norm_before, "weight"):
            param_dict[f"{model_name}/~/emb_layer_norm_before"][
                "scale"
            ] = model.emb_layer_norm_before.weight.detach().numpy()
            param_dict[f"{model_name}/~/emb_layer_norm_before"][
                "offset"
            ] = model.emb_layer_norm_before.bias.detach().numpy()

    # copy weights for each of the transformer blocks
    num_layers = re.search("t(.+?)_", model_name)
    num_layers = int(num_layers.group(1))  # type: ignore
    for layer_idx in range(num_layers):
        # multiheaded self-attention weights
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention/query"][
            "w"
        ] = (model.layers[layer_idx].self_attn.q_proj.weight.detach().numpy().T)
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention/query"][
            "b"
        ] = (model.layers[layer_idx].self_attn.q_proj.bias.detach().numpy())
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention/key"][
            "w"
        ] = (model.layers[layer_idx].self_attn.k_proj.weight.detach().numpy().T)
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention/key"][
            "b"
        ] = (model.layers[layer_idx].self_attn.k_proj.bias.detach().numpy())
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention/value"][
            "w"
        ] = (model.layers[layer_idx].self_attn.v_proj.weight.detach().numpy().T)
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention/value"][
            "b"
        ] = (model.layers[layer_idx].self_attn.v_proj.bias.detach().numpy())
        param_dict[
            f"{model_name}/attention_layer_{layer_idx}/~/self_attention/mha_output"
        ]["w"] = (model.layers[layer_idx].self_attn.out_proj.weight.detach().numpy().T)
        param_dict[
            f"{model_name}/attention_layer_{layer_idx}/~/self_attention/mha_output"
        ]["b"] = (model.layers[layer_idx].self_attn.out_proj.bias.detach().numpy())
        if model.layers[layer_idx].self_attn.bias_k is not None:
            num_heads = model.layers[0].self_attn.num_heads
            key_size = model.layers[0].self_attn.embed_dim // num_heads
            param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention"][
                "bias_k"
            ] = (
                model.layers[layer_idx]
                .self_attn.bias_k.detach()
                .numpy()
                .reshape(1, 1, num_heads, key_size)
            )
            param_dict[f"{model_name}/attention_layer_{layer_idx}/~/self_attention"][
                "bias_v"
            ] = (
                model.layers[layer_idx]
                .self_attn.bias_v.detach()
                .numpy()
                .reshape(1, 1, num_heads, key_size)
            )

        # weights for the linear layers
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/fc1"]["w"] = (
            model.layers[layer_idx].fc1.weight.detach().numpy().T
        )
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/fc1"]["b"] = (
            model.layers[layer_idx].fc1.bias.detach().numpy()
        )
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/fc2"]["w"] = (
            model.layers[layer_idx].fc2.weight.detach().numpy().T
        )
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/fc2"]["b"] = (
            model.layers[layer_idx].fc2.bias.detach().numpy()
        )

        # weights for the layer norms
        param_dict[
            f"{model_name}/attention_layer_{layer_idx}/~/self_attention_layer_norm"
        ]["scale"] = (
            model.layers[layer_idx].self_attn_layer_norm.weight.detach().numpy()
        )
        param_dict[
            f"{model_name}/attention_layer_{layer_idx}/~/self_attention_layer_norm"
        ]["offset"] = (
            model.layers[layer_idx].self_attn_layer_norm.bias.detach().numpy()
        )
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/final_layer_norm"][
            "scale"
        ] = (model.layers[layer_idx].final_layer_norm.weight.detach().numpy())
        param_dict[f"{model_name}/attention_layer_{layer_idx}/~/final_layer_norm"][
            "offset"
        ] = (model.layers[layer_idx].final_layer_norm.bias.detach().numpy())

    # weights for the post-transformer blocks layer norm.
    if hasattr(model, "emb_layer_norm_after"):
        param_dict[f"{model_name}/~/roberta_lm_head/~/emb_layer_norm_after"][
            "scale"
        ] = model.emb_layer_norm_after.weight.detach().numpy()
        param_dict[f"{model_name}/~/roberta_lm_head/~/emb_layer_norm_after"][
            "offset"
        ] = model.emb_layer_norm_after.bias.detach().numpy()

    if hasattr(model, "lm_head"):
        param_dict[f"{model_name}/~/roberta_lm_head/~/lm_head_fc_1"]["w"] = (
            model.lm_head.dense.weight.detach().numpy().T
        )
        param_dict[f"{model_name}/~/roberta_lm_head/~/lm_head_fc_1"][
            "b"
        ] = model.lm_head.dense.bias.detach().numpy()
        param_dict[f"{model_name}/~/roberta_lm_head/~/lm_head_layer_norm"][
            "scale"
        ] = model.lm_head.layer_norm.weight.detach().numpy()
        param_dict[f"{model_name}/~/roberta_lm_head/~/lm_head_layer_norm"][
            "offset"
        ] = model.lm_head.layer_norm.bias.detach().numpy()

        param_dict[f"{model_name}/~/roberta_lm_head/~/lm_final_fc"]["w"] = (
            model.lm_head.weight.detach().numpy().T
        )
        param_dict[f"{model_name}/~/roberta_lm_head/~/lm_final_fc"][
            "b"
        ] = model.lm_head.bias.detach().numpy()
    else:
        param_dict[f"{model_name}/~/simple_lm_head/~/lm_final_fc"]["w"] = (
            model.embed_out.detach().numpy().T
        )
        if model.embed_out_bias is not None:
            param_dict[f"{model_name}/~/simple_lm_head/~/lm_final_fc"][
                "b"
            ] = model.embed_out_bias.detach().numpy()
        else:
            param_dict[f"{model_name}/~/simple_lm_head/~/lm_final_fc"]["b"] = np.zeros(
                model.embed_out.shape[0]
            )
    return param_dict


def get_model_hyperparameters(
    model: torch.nn.ParameterDict, model_name: str, alphabet: esm.Alphabet
) -> Dict:
    """
    Gets ESM model hyperparameters to create Haiku model.

    Args:
        model: Pretrained PyTorch ESM model.
        model_name: Name of the model (see ESM Github repository).
        alphabet: Alphabet of ESM.

    Returns:
        Dictionary of hyperparameters of the model.
    """
    hyperparam_dict: Dict = {}
    hyperparam_dict[model_name] = {}

    # copy weights for each of the 33 transformer blocks
    num_layers = re.search("t(.+?)_", model_name)
    num_layers = int(num_layers.group(1))  # type: ignore
    for layer_idx in range(num_layers):
        if model.layers[layer_idx].self_attn.bias_k is not None:
            hyperparam_dict[model_name]["add_bias_kv"] = True
        else:
            hyperparam_dict[model_name]["add_bias_kv"] = False

    hyperparam_dict[model_name]["attention_heads"] = model.layers[0].self_attn.num_heads
    hyperparam_dict[model_name]["embed_dim"] = model.layers[0].self_attn.embed_dim
    hyperparam_dict[model_name]["ffn_embed_dim"] = model.layers[
        layer_idx
    ].fc1.out_features
    hyperparam_dict[model_name]["num_layers"] = num_layers
    hyperparam_dict[model_name]["alphabet_size"] = alphabet.__len__()
    hyperparam_dict[model_name]["model_type"] = model_name.split("_")[0]
    hyperparam_dict[model_name]["tokens"] = alphabet.all_toks
    hyperparam_dict[model_name]["embed_scale"] = model.embed_scale

    # esm-1b and esm-1v share same architecture
    if model_name == "esm1b_t33_650M_UR50S" or model_name == "esm1v_t33_650M_UR90S_1":
        hyperparam_dict[model_name]["model_max_positions"] = 1024  # specified because
        # of learned positional embeddings
        hyperparam_dict[model_name]["positional_embedding"] = "learned"
        hyperparam_dict[model_name]["lm_head"] = "roberta"
        hyperparam_dict[model_name]["token_dropout"] = getattr(
            model.args, "token_dropout", False
        )
        hyperparam_dict[model_name]["emb_layer_norm_before"] = hasattr(
            model.emb_layer_norm_before, "weight"
        )
        hyperparam_dict[model_name]["use_rotary_embedding"] = False
        hyperparam_dict[model_name]["mask_before_attention"] = True
        hyperparam_dict[model_name]["append_eos"] = True
        hyperparam_dict[model_name]["prepend_cls"] = True
    # esm-2 architecture
    elif model_name[:4] == "esm2":
        hyperparam_dict[model_name]["positional_embedding"] = None
        hyperparam_dict[model_name]["lm_head"] = "roberta"
        hyperparam_dict[model_name]["token_dropout"] = model.token_dropout
        hyperparam_dict[model_name]["emb_layer_norm_before"] = False
        hyperparam_dict[model_name]["use_rotary_embedding"] = True
        hyperparam_dict[model_name]["mask_before_attention"] = True
        hyperparam_dict[model_name]["append_eos"] = True
        hyperparam_dict[model_name]["prepend_cls"] = True
    # esm-1 architecture
    else:
        hyperparam_dict[model_name]["positional_embedding"] = "sinusoidal"
        hyperparam_dict[model_name]["lm_head"] = "simple"
        hyperparam_dict[model_name]["token_dropout"] = getattr(
            model.args, "token_dropout", False
        )
        hyperparam_dict[model_name]["emb_layer_norm_before"] = hasattr(
            model.emb_layer_norm_before, "weight"
        )
        hyperparam_dict[model_name]["use_rotary_embedding"] = False
        hyperparam_dict[model_name]["mask_before_attention"] = False
        hyperparam_dict[model_name]["append_eos"] = False
        hyperparam_dict[model_name]["prepend_cls"] = True
    return hyperparam_dict


def get_config_and_tokenizer(
    hyperparam_dict: Dict,
    model_name: str,
    embeddings_layers_to_save: Tuple[int, ...],
    attention_maps_to_save: Optional[List[Tuple[int, int]]] = None,
    max_positions: int = 1024,
    use_gradient_checkpointing: bool = False,
) -> Tuple[ESMTransformerConfig, FixedSizeStandardTokenizer]:
    """
    Creates the Transformer configuration for ESM.

    Args:
        hyperparam_dict: Dictionary of hyperparameters of the model.
        model_name: Name of the model (see ESM Github repository).
        embeddings_layers_to_save: Indices of the layers to save.
        attention_maps_to_save: (Indices layer, Indices maps) to save.
        max_positions: Maximum number of tokens (for padding).
        use_gradient_checkpointing: whether to activate gradient checkpointing on all
            transformer layers.

    Returns:
        Model configuration.
        Model tokenizer.
    """
    if attention_maps_to_save is None:
        attention_maps_to_save = []

    hyperparam_dict = hyperparam_dict[model_name]
    tokens = hyperparam_dict["tokens"]

    unk_token = "<unk>"
    pad_token = "<pad>"
    mask_token = "<mask>"
    cls_token = "<cls>"
    eos_token = "<eos>"
    bos_token = "<bos>"
    extra_special_tokens = []
    if "<null_1>" in tokens:
        extra_special_tokens.append("<null_1>")
    if "<null_0>" in tokens:
        extra_special_tokens.append("<null_0>")
    if "<sep>" in tokens:
        extra_special_tokens.append("<sep>")

    special_tokens = [unk_token, pad_token, mask_token, cls_token, eos_token]
    special_tokens = special_tokens + extra_special_tokens

    standard_tokens = list(set(tokens).difference(set(special_tokens)))

    tokenizer = FixedSizeStandardTokenizer(
        fixed_length=max_positions,
        standard_tokens=standard_tokens,
        unk_token=unk_token,
        pad_token=pad_token,
        mask_token=mask_token,
        class_token=cls_token,
        eos_token=eos_token,
        prepend_cls_token=hyperparam_dict["prepend_cls"],
        append_eos_token=hyperparam_dict["append_eos"],
        extra_special_tokens=extra_special_tokens,
        tokens_to_ids={tok: i for i, tok in enumerate(tokens + [bos_token])},
    )

    config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size - 1,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        # if the model has learned positional embeddings and a max_positions less than
        # true max positions is passed then jax will break since the full positional
        # embeddings matrix is returned in the convert_model_torch_to_jax function.
        max_positions=hyperparam_dict["model_max_positions"]
        if hyperparam_dict["positional_embedding"] == "learned"
        else max_positions,
        emb_layer_norm_before=hyperparam_dict["emb_layer_norm_before"],
        lm_head=hyperparam_dict["lm_head"],
        add_bias_kv=hyperparam_dict["add_bias_kv"],
        positional_embedding=hyperparam_dict["positional_embedding"],
        attention_heads=hyperparam_dict["attention_heads"],
        embed_dim=hyperparam_dict["embed_dim"],
        ffn_embed_dim=hyperparam_dict["ffn_embed_dim"],
        num_layers=hyperparam_dict["num_layers"],
        token_dropout=hyperparam_dict["token_dropout"],
        masking_ratio=0.15,  # hard-coded but it doesn't affect inference.
        masking_prob=0.8,  # hard-coded but it doesn't affect inference.
        embed_scale=hyperparam_dict["embed_scale"],
        use_rotary_embedding=hyperparam_dict["use_rotary_embedding"],
        mask_before_attention=hyperparam_dict["mask_before_attention"],
        embeddings_layers_to_save=embeddings_layers_to_save,
        attention_maps_to_save=attention_maps_to_save,
        use_gradient_checkpointing=use_gradient_checkpointing,
    )

    return config, tokenizer


def get_pretrained_esm_model(
    model_name: str,
    model_path: Optional[str] = None,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    embeddings_layers_to_save: Tuple[int, ...] = (),
    attention_maps_to_save: Optional[List[Tuple[int, int]]] = None,
    max_positions: int = 1024,
    use_gradient_checkpointing: bool = False,
) -> Tuple[hk.Params, Callable, FixedSizeStandardTokenizer, ESMTransformerConfig]:
    """
    Create a Haiku ESM model by downloading and transforming a pretrained PyTorch model.

    Args:
        model_name: Name of the model (see ESM Github repository).
        model_path: Path to the model if you want to load it from there.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        embeddings_layers_to_save: Intermediate embeddings to return in the output.
        attention_maps_to_save: Intermediate attention maps to return in the output.
        max_positions: Maximum length of a token (for padding).
        use_gradient_checkpointing: whether to activate gradient checkpointing on all
            transformer layers.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config (hyperparameters).
    """
    if attention_maps_to_save is None:
        attention_maps_to_save = []
    supported_models = [
        "esm1_t12_85M_UR50S",
        "esm1_t34_670M_UR100",
        "esm1_t34_670M_UR50D",
        "esm1_t34_670M_UR50S",
        "esm1_t6_43M_UR50S",
        "esm1b_t33_650M_UR50S",
        "esm1v_t33_650M_UR90S_1",
        "esm2_t6_8M_UR50D",
        "esm2_t12_35M_UR50D",
        "esm2_t30_150M_UR50D",
        "esm2_t33_650M_UR50D",
        "esm2_t36_3B_UR50D",
        "esm2_t48_15B_UR50D",
    ]
    if not (model_name in supported_models):
        raise NotImplementedError(
            f"Unknown ESM {model_name} model. "
            f"Supported models are {supported_models}"
        )
    if model_path is None:
        # load the model from hub
        load_function = getattr(esm.pretrained, model_name)
        model, alphabet = load_function()
    else:
        # load the model from path
        model_path = Path(model_path)  # type: ignore
        assert model_path.is_file()  # type: ignore
        assert model_path.stem == model_name  # type: ignore
        model, alphabet = esm.pretrained.load_model_and_alphabet_local(model_path)

    hyperparam_dict = get_model_hyperparameters(model, model_name, alphabet)
    parameters = convert_model_torch_to_jax(model, model_name)

    if param_dtype != jnp.float32:
        parameters = jax.tree_util.tree_map(lambda x: x.astype(param_dtype), parameters)

    config, tokenizer = get_config_and_tokenizer(
        hyperparam_dict=hyperparam_dict,
        model_name=model_name,
        embeddings_layers_to_save=embeddings_layers_to_save,
        attention_maps_to_save=attention_maps_to_save,
        max_positions=max_positions,
        use_gradient_checkpointing=use_gradient_checkpointing,
    )

    forward_fn = build_esm_fn(
        model_config=config,
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        model_name=model_name,
    )

    return parameters, forward_fn, tokenizer, config
