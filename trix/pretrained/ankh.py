"""Implementation of utilities to load a pretrained T5 model in Trix."""

# from pathlib import Path
from typing import Callable, Optional, Tuple

import haiku as hk
from transformers import (
    AutoTokenizer,
    T5EncoderModel,
    T5ForConditionalGeneration,
    logging,
)

from trix.models.ankh.model import (
    T5Config,
    build_ankh_encoder_only_fn,
    build_ankh_transformer_fn,
)
from trix.models.t5.decoder import T5DecoderConfig
from trix.models.t5.encoder import T5EncoderConfig
from trix.pretrained.t5 import translate_encoder, translate_transformer

logging.set_verbosity_error()


def get_pretrained_ankh_model(
    base_or_large: str,
    encoder_only: bool = True,
    embeddings_layers_to_save: Optional[Tuple[int, ...]] = None,
) -> Tuple[hk.Params, Callable, AutoTokenizer, T5Config]:
    """
    Create a Haiku T5 model either by downloading the model directly or by
    downloading transforming a pretrained PyTorch model.

    Args:
        base_or_large: Ankh core model to load.  Select from ['base','large'].
        encoder_only: True if performing a protein analysis task that only requires
             the pretrained encoder, false if performing sequence generation using the
             encoder-decoder
        embeddings_layers_to_save: Tuple of integers denoting which embeddings to save.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config (hyperparameters).
    """
    if embeddings_layers_to_save is None:
        embeddings_layers_to_save = ()
    supported_list = ["base", "large"]
    if not (base_or_large in supported_list):
        raise NotImplementedError(
            f"Unknown model {base_or_large}. Supported models are {supported_list}"
        )

    # Default Ankh-base Config
    encoder_config = T5EncoderConfig(
        vocab_size=144,
        pad_token_id=0,
        eos_token_id=1,
        embed_dim=768,
        attention_embed_dim=768,
        ffn_embed_dim=3072,
        num_heads=12,
        num_layers=48,
        num_buckets=64,
        max_bucket_distance=128,
        dropout_rate=0.1,
        ffn_activation_name="gelu",
        use_glu_in_ffn=True,
        embeddings_layers_to_save=embeddings_layers_to_save,
    )
    decoder_config = T5DecoderConfig(
        vocab_size=144,
        pad_token_id=0,
        eos_token_id=1,
        embed_dim=768,
        attention_embed_dim=768,
        ffn_embed_dim=3072,
        num_heads=12,
        num_layers=24,
        num_buckets=64,
        max_bucket_distance=128,
        dropout_rate=0.1,
        logits_from_embed=False,
        ffn_activation_name="gelu",
        use_glu_in_ffn=True,
    )

    if base_or_large == "large":
        encoder_config.embed_dim = 1536
        encoder_config.ffn_embed_dim = 3840
        encoder_config.num_heads = 16
        decoder_config.embed_dim = 1536
        decoder_config.ffn_embed_dim = 3840
        decoder_config.num_heads = 16

    model_config = T5Config(
        encoder_config=encoder_config, decoder_config=decoder_config
    )

    if encoder_only is True:
        model_params = T5EncoderModel.from_pretrained(
            f"ElnaggarLab/ankh-{base_or_large}"
        ).state_dict()
        parameters = translate_encoder(
            torch_params=model_params,
            num_layers=encoder_config.num_layers,
            use_glu_in_ffn=encoder_config.use_glu_in_ffn,
            module_name=None,
        )
        for parameter_name in list(parameters.keys()):
            new_name = parameter_name.replace("t5_encoder/", "ankh_encoder/")
            parameters[new_name] = parameters.pop(parameter_name)

        model_fn = build_ankh_encoder_only_fn(encoder_config)

    else:
        model_params = T5ForConditionalGeneration.from_pretrained(
            f"ElnaggarLab/ankh-{base_or_large}"
        ).state_dict()
        parameters = translate_transformer(
            torch_params=model_params,
            encoder_num_layers=encoder_config.num_layers,
            decoder_num_layers=decoder_config.num_layers,
            use_glu_in_ffn=encoder_config.use_glu_in_ffn,
            with_lm_head=(not decoder_config.logits_from_embed),
            module_name="ankh_transformer",
        )
        model_fn = build_ankh_transformer_fn(model_config)

    tokenizer = AutoTokenizer.from_pretrained(
        f"ElnaggarLab/ankh-{base_or_large}", model_max_length=512
    )

    return parameters, model_fn, tokenizer, model_config
