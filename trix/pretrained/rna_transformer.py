import json
import os
from typing import Any, Callable

import haiku as hk
import joblib
import regex as re

from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.codon_tokenizer import (
    CodonTokenizer,
    CodonTokenizerParams,
)
from trix.utils.s3 import download_from_s3_bucket, init_s3_trix_bucket

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> str:
    """
    Get directory to save files on user machine.
    """
    return os.path.expanduser(
        os.path.join(
            os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR),
            "trix",
            "rna_transformer_v1_150M_coding_region",
        )
    )


def get_pretrained_rna_transformer_model(
    model_name: str,
    fixed_length: int = 1024,
    embeddings_layers_to_save: tuple[int, ...] = (),
    attention_maps_to_save: list[tuple[int, int]] | None = None,
    verbose: bool = True,
) -> tuple[hk.Params, Callable, CodonTokenizer, ESMTransformerConfig]:
    """
    Download pre-trained weights and hyperparameters for a DeepChain RNA Transformer.

    DeepChain RNA Transformer models have ESM-like architectures, are trained on RNA
    sequences, and use a special CodonTokenizer.

    Args:
        model_name: Name of the model, e.g.
            rna_transformer_v1_150M_coding_region.
        fixed_length: Number of tokens to get with padding/truncation.
        embeddings_layers_to_save: Intermediate embeddings to return in the output.
        attention_maps_to_save: Intermediate attention maps to return in the output.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        Haiku function to call the model.
        The tokenizer used to train the model.
        Model config (hyperparameters).

    Example:
        parameters, forward_fn, tokenizer, config = \
        get_pretrained_rna_transformer_model(
            model_name="rna_transformer_v1_150M_coding_region",
            # with 1,024 tokens.
            fixed_length=1024,
            # Get embedding at layers 5 and 20
            embeddings_layers_to_save=(5, 20,),
            # Get attention map number 4 at layer 1 and attention map number 14
            # at layer 12
            attention_maps_to_save=((1,4), (12, 14)),
        )
    """
    if attention_maps_to_save is None:
        attention_maps_to_save = []
    supported_models = [
        "rna_transformer_v1_150M_coding_region",
    ]

    if not (model_name in supported_models):
        raise NotImplementedError(
            f"Unknown {model_name} model. " f"Supported models are {supported_models}"
        )

    # Download weights and hyperparams
    parameters, hyperparams = download_ckpt_and_hyperparams(model_name, verbose=verbose)
    hyperparams["model_config"]["max_positions"] = fixed_length
    if embeddings_layers_to_save:
        hyperparams["model_config"][
            "embeddings_layers_to_save"
        ] = embeddings_layers_to_save
    if attention_maps_to_save:
        hyperparams["model_config"]["attention_maps_to_save"] = attention_maps_to_save

    # Get tokenizer
    tokenizer = CodonTokenizer(
        CodonTokenizerParams(**hyperparams["tokenizer_config"]),
        fixed_length=fixed_length,
    )

    # Get model config
    config = ESMTransformerConfig(**hyperparams["model_config"])

    # Get model
    forward_fn = build_esm_fn(
        model_config=config,
    )

    return parameters, forward_fn, tokenizer, config


def download_ckpt_and_hyperparams(
    model_name: str, verbose: bool = True
) -> tuple[hk.Params, dict[str, Any]]:
    """
    Download checkpoint and hyperparams from GCP bucket.

    Args:
        model_name: Name of the model.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        Model hyperparameters dict.
    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    checkpoint_steps = {"rna_transformer_v1_150M_coding_region": 806000}
    checkpoint_step = checkpoint_steps[model_name]
    params_save_dir = os.path.join(save_dir, f"params_step_{checkpoint_step}.joblib")
    hyperparams_save_dir = os.path.join(save_dir, "hyperparams.json")

    if os.path.exists(hyperparams_save_dir) and os.path.exists(params_save_dir):
        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = joblib.load(f)

        return params, hyperparams

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Parsing model name with regex
        prog = re.compile(r"(rna_transformer)_(\S+)")  # noqa: W605
        result = prog.match(model_name)

        model_class = result.group(1)
        model_dir = result.group(2)

        print(f"Downloading checkpoints/{model_class}/{model_dir}/" f"hyperparams.json")
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/{model_class}/{model_dir}/hyperparams.json",
            filename=hyperparams_save_dir,
            verbose=verbose,
        )
        print(
            f"Downloading checkpoints/{model_class}/{model_dir}/"
            f"params_step_{checkpoint_step}.joblib"
        )
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/{model_class}/{model_dir}/"
            f"params_step_{checkpoint_step}.joblib",
            filename=params_save_dir,
            verbose=verbose,
        )

        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = joblib.load(f)

        return params, hyperparams


def download_test_inputs_file(model_name: str) -> dict[str, Any]:
    """
    Download files to check inputs and inference outputs.
    The returned dictionary contains a sequence, the tokens corresponding
    to that sequence, the logits corresponding to that sequence as well as
    embeddings at several layers.

    Args:
        model_name: Name of the model.

    Returns:
        Dict containing inputs and outputs.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    checkpoint_steps = {"rna_transformer_v1_150M_coding_region": 806000}
    checkpoint_step = checkpoint_steps[model_name]
    test_file_save_dir = os.path.join(
        save_dir, f"tests_inputs_outputs_checkpoint_{checkpoint_step}.joblib"
    )

    if os.path.exists(test_file_save_dir):
        # Load locally
        with open(test_file_save_dir, "rb") as f:
            test_inputs = joblib.load(f)

        return test_inputs  # type: ignore

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Parsing model name with regex
        prog = re.compile(r"(rna_transformer)_(\S+)")  # noqa: W605
        result = prog.match(model_name)

        model_class = result.group(1)
        model_dir = result.group(2)

        bucket_file_path = f"checkpoints/{model_class}/{model_dir}/"
        bucket_file_path += f"tests_inputs_outputs_checkpoint_{checkpoint_step}.joblib"
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=bucket_file_path,
            filename=test_file_save_dir,
        )

        # Load locally
        with open(test_file_save_dir, "rb") as f:
            test_inputs_outputs = joblib.load(f)

        return test_inputs_outputs  # type: ignore
