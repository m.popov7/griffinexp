"""Implementation of utilities to load a pretrained scGPT model in Jax."""
import gc
import json
import os
import typing
from collections import defaultdict
from typing import Any, Dict, Tuple

import haiku as hk
import jax.numpy as jnp
import joblib
import numpy as np
import torch

from trix.models.scgpt.model import ScGptConfig, build_scgpt_fn
from trix.utils.s3 import download_from_s3_bucket, init_s3_trix_bucket

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> str:
    """
    Get directory to save files on user machine.
    """
    return os.path.expanduser(
        os.path.join(os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR), "trix", "scgpt")
    )


def translate_torch_params_scgpt(
    torch_params: torch.nn.ParameterDict,
    num_layers: int,
    model_name: str,
) -> hk.Params:
    """
    Converts the full Hugging Face PyTorch DNAbert model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix ESM
    architecture.  Other choices may cause errors.

    Args:
        torch_params: Pretrained PyTorch DNAbert model state_dict().
        num_layers: Number of decoder layers in the desired pre-trained model.
        model_name: model name in trix

    Returns:
       Dictionary of Haiku parameters.
    """
    translate_dict = {}
    prefix = f"{model_name}/"

    translate_dict["encoder.embedding.weight"] = (
        prefix + "~/encoder/~/embed",
        "embeddings",
    )

    translate_dict["encoder.enc_norm.weight"] = (
        prefix + "~/encoder/~/layer_norm",
        "scale",
    )
    translate_dict["encoder.enc_norm.bias"] = (
        prefix + "~/encoder/~/layer_norm",
        "offset",
    )

    translate_dict["encoder.embedding.weight"] = (
        prefix + "~/encoder/~/embed",
        "embeddings",
    )

    translate_dict["encoder.enc_norm.weight"] = (
        prefix + "~/encoder/~/layer_norm",
        "scale",
    )
    translate_dict["encoder.enc_norm.bias"] = (
        prefix + "~/encoder/~/layer_norm",
        "offset",
    )

    translate_dict["decoder.fc.0.weight"] = (
        prefix + "~/decoder/~/linear",
        "w",
    )

    translate_dict["decoder.fc.0.bias"] = (
        prefix + "~/decoder/~/linear",
        "b",
    )

    translate_dict["decoder.fc.2.weight"] = (
        prefix + "~/decoder/~/linear_1",
        "w",
    )

    translate_dict["decoder.fc.2.bias"] = (
        prefix + "~/decoder/~/linear_1",
        "b",
    )

    translate_dict["decoder.fc.4.weight"] = (
        prefix + "~/decoder/~/linear_2",
        "w",
    )

    translate_dict["decoder.fc.4.bias"] = (
        prefix + "~/decoder/~/linear_2",
        "b",
    )

    translate_dict["mvc_decoder.gene2query.weight"] = (
        prefix + "~/mvc_decoder/~/linear",
        "w",
    )
    translate_dict["mvc_decoder.gene2query.bias"] = (
        prefix + "~/mvc_decoder/~/linear",
        "b",
    )
    translate_dict["mvc_decoder.W.weight"] = (
        prefix + "~/mvc_decoder/~/linear_1",
        "w",
    )

    translate_dict["value_encoder.linear1.weight"] = (
        prefix + "~/value_encoder/~/linear",
        "w",
    )

    translate_dict["value_encoder.linear1.bias"] = (
        prefix + "~/value_encoder/~/linear",
        "b",
    )
    translate_dict["value_encoder.linear2.weight"] = (
        prefix + "~/value_encoder/~/linear_1",
        "w",
    )

    translate_dict["value_encoder.linear2.bias"] = (
        prefix + "~/value_encoder/~/linear_1",
        "b",
    )

    translate_dict["value_encoder.norm.weight"] = (
        prefix + "~/value_encoder/~/layer_norm",
        "scale",
    )
    translate_dict["value_encoder.norm.bias"] = (
        prefix + "~/value_encoder/~/layer_norm",
        "offset",
    )

    for k in range(num_layers):
        prefix_layer = prefix + f"~/self_attention_block_{k}/~/"
        torch_prefix = f"transformer_encoder.layers.{k}."

        if torch_prefix + "self_attn.Wqkv.weight" in torch_params:
            # attention K, Q, V projections
            translate_dict[torch_prefix + "self_attn.Wqkv.weight"] = (
                prefix_layer + "self_attention/query_key_value",
                "w",
            )
            translate_dict[torch_prefix + "self_attn.Wqkv.bias"] = (
                prefix_layer + "self_attention/query_key_value",
                "b",
            )

        else:
            # attention K, Q, V projections
            translate_dict[torch_prefix + "self_attn.in_proj_weight"] = (
                prefix_layer + "self_attention/query_key_value",
                "w",
            )
            translate_dict[torch_prefix + "self_attn.in_proj_bias"] = (
                prefix_layer + "self_attention/query_key_value",
                "b",
            )
        # Attention output
        translate_dict[torch_prefix + "self_attn.out_proj.weight"] = (
            prefix_layer + "self_attention/mha_output",
            "w",
        )
        translate_dict[torch_prefix + "self_attn.out_proj.bias"] = (
            prefix_layer + "self_attention/mha_output",
            "b",
        )

        translate_dict[torch_prefix + "linear1.weight"] = (
            prefix_layer + "fc1",
            "w",
        )
        translate_dict[torch_prefix + "linear1.bias"] = (
            prefix_layer + "fc1",
            "b",
        )
        translate_dict[torch_prefix + "linear2.weight"] = (
            prefix_layer + "fc2",
            "w",
        )
        translate_dict[torch_prefix + "linear2.bias"] = (
            prefix_layer + "fc2",
            "b",
        )

        translate_dict[torch_prefix + "norm1.weight"] = (
            prefix_layer + "self_attention_layer_norm",
            "scale",
        )

        translate_dict[torch_prefix + "norm1.bias"] = (
            prefix_layer + "self_attention_layer_norm",
            "offset",
        )

        translate_dict[torch_prefix + "norm2.weight"] = (
            prefix_layer + "final_layer_norm",
            "scale",
        )

        translate_dict[torch_prefix + "norm2.bias"] = (
            prefix_layer + "final_layer_norm",
            "offset",
        )
    # print(translate_dict)
    params: Dict[str, Dict[str, np.ndarray]] = defaultdict(dict)
    for torch_key, (trix_key, weight_key) in translate_dict.items():
        if "Wqkv.weight" in torch_key or "in_proj_weight" in torch_key:
            torch_params[torch_key]
            split_query_key_value_params = np.array(torch_params.pop(torch_key))

            split_query_key_value_params = np.split(  # type: ignore
                split_query_key_value_params,
                indices_or_sections=3,
                axis=0,
            )

            params[trix_key.replace("query_key_value", "query")][
                weight_key
            ] = split_query_key_value_params[0].transpose(1, 0)
            params[trix_key.replace("query_key_value", "key")][
                weight_key
            ] = split_query_key_value_params[1].transpose(1, 0)
            params[trix_key.replace("query_key_value", "value")][
                weight_key
            ] = split_query_key_value_params[2].transpose(1, 0)

        elif "Wqkv.bias" in torch_key or "in_proj_bias" in torch_key:
            split_query_key_value_params = np.array(torch_params.pop(torch_key))

            split_query_key_value_params = np.split(  # type: ignore
                split_query_key_value_params,
                indices_or_sections=3,
                axis=0,
            )
            params[trix_key.replace("query_key_value", "query")][
                weight_key
            ] = split_query_key_value_params[0]
            params[trix_key.replace("query_key_value", "key")][
                weight_key
            ] = split_query_key_value_params[1]
            params[trix_key.replace("query_key_value", "value")][
                weight_key
            ] = split_query_key_value_params[2]

        elif "weight" in torch_key and "embed" not in trix_key:
            # in pytorch, the weights of dense matrices indexation is transposes
            # compared to haiku, except for word-token-embedding
            params[trix_key][weight_key] = np.array(
                torch_params.pop(torch_key)
            ).transpose()
        else:
            params[trix_key][weight_key] = np.array(torch_params.pop(torch_key))

    params = dict(params)
    params = {
        k.replace("self_attention_block_0", "self_attention_block"): v
        for k, v in params.items()
    }
    return params  # type: ignore


@typing.no_type_check
def reset_dsbn_statistics(state: hk.State):
    for key in state.keys():
        if "domain_specific_batch_norm" in key:
            if "var_ema" in key:
                state[key]["average"] = jnp.ones_like(state[key]["average"])
                state[key]["hidden"] = jnp.ones_like(state[key]["hidden"])
                state[key]["counter"] = jnp.zeros_like(state[key]["counter"])

            if "mean_ema" in key:
                state[key]["average"] = jnp.zeros_like(state[key]["average"])
                state[key]["hidden"] = jnp.zeros_like(state[key]["hidden"])
                state[key]["counter"] = jnp.zeros_like(state[key]["counter"])
    return state


def download_ckpt_and_hyperparams(
    model_name: str, verbose: bool = True
) -> Tuple[torch.nn.ParameterDict, Dict[str, Any], Dict[str, Any]]:
    """
    Download checkpoint and hyperparams on kao datacenter.

    Args:
        model_name: Name of the model.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        Model hyperparameters dict.
        Vocabulary.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    params_save_dir = os.path.join(save_dir, "model.pt")
    hyperparams_save_dir = os.path.join(save_dir, "args.json")
    vocab_save_dir = os.path.join(save_dir, "vocab.json")

    if (
        os.path.exists(hyperparams_save_dir)
        and os.path.exists(params_save_dir)
        and os.path.exists(vocab_save_dir)
    ):
        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(vocab_save_dir, "rb") as f:
            vocab = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = torch.load(f, map_location="cpu")

        return params, hyperparams, vocab

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Parsing model name with regex

        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/scgpt/{model_name}/args.json",
            filename=hyperparams_save_dir,
            verbose=verbose,
        )

        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/scgpt/{model_name}/model.pt",
            filename=params_save_dir,
            verbose=verbose,
        )

        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/scgpt/{model_name}/vocab.json",
            filename=vocab_save_dir,
            verbose=verbose,
        )

        # Load locally
        with open(hyperparams_save_dir, "rb") as f1:
            hyperparams = json.load(f1)

        with open(params_save_dir, "rb") as f2:
            params = torch.load(f2, map_location="cpu")

        with open(vocab_save_dir, "rb") as f3:
            vocab = json.load(f3)

        return params, hyperparams, vocab


def download_io_data(
    model_name: str,
    test_file_name: str,
) -> Dict[str, Any]:
    """
    Download checkpoint and hyperparams on kao datacenter.

    Args:
        model_name: Name of the model.
        test_file_name: Name of the test file.

    Returns:
        Data dictionary.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    data_save_dir = os.path.join(save_dir, test_file_name)

    if os.path.exists(data_save_dir):
        # Load locally
        with open(data_save_dir, "rb") as f:
            data = dict(joblib.load(f))

        return data

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Parsing model name with regex

        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/scgpt/{model_name}/{test_file_name}",
            filename=data_save_dir,
        )

        # Load locally
        with open(data_save_dir, "rb") as f:
            data = dict(joblib.load(f))

        return data


def get_pretrained_scgpt_model(
    model_name: str,
    pad_token: str = "<pad>",
    num_batch_ids: int = 1,
    verbose: bool = True,
) -> Tuple[hk.Params, hk.TransformedWithState, Dict[str, int], ScGptConfig]:
    """
    Create a Haiku ScGpt model by downloading the pytorch weights hosted on Kao.


    Args:
        model_name: name of the model to download. It can be one of the following:
            - "blood": trained on blood data
            - "human": trained on human data
            - "pancancer": trained on pancancer data
        pad_token: the padding token to use.
        num_batch_ids: the number of batch ids to use. It is used to encode the batch
            ids in the model.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Vocabulary.
        Model config.

    """
    _torch_params, model_configs, vocab = download_ckpt_and_hyperparams(
        model_name, verbose=verbose
    )
    torch_params = {k.replace("Wqkv.", "in_proj_"): v for k, v in _torch_params.items()}

    ntokens = len(vocab)

    # Retrieve model parameters from config files
    embsize = model_configs["embsize"]
    nhead = model_configs["nheads"]
    d_hid = model_configs["d_hid"]
    nlayers = model_configs["nlayers"]

    config = ScGptConfig(
        gene_ids_vocabulary_size=ntokens,
        embed_dim=embsize,
        num_heads=nhead,
        ffn_embed_dim=d_hid,
        num_layers=nlayers,
        num_batch_labels=num_batch_ids,
        use_mvc_decoder=True,
    )

    # Translate torch params to Haiku params
    hk_params = translate_torch_params_scgpt(
        torch_params, nlayers, "sc_gpt_transformer"  # type: ignore
    )

    del torch_params
    gc.collect()
    torch.cuda.empty_cache()

    jax_transformer = build_scgpt_fn(config=config)

    forward_fn = hk.transform_with_state(jax_transformer)
    # Remove the torch parameters from the RAM.
    # Initialize Haiku model (state = running stats for batch norm)

    return hk_params, forward_fn, vocab, config
