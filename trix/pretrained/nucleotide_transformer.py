import json
import os
from typing import Any, Callable, Dict, List, Optional, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
import joblib
import regex as re

from trix.models.bigbird.model import BigbirdConfig, build_bigbird_forward_fn
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.bio import (
    FixedSizeNucleotidesKmersTokenizer,
    compute_dcnuc_tokens_to_ids,
)
from trix.utils.s3 import download_from_s3_bucket, init_s3_trix_bucket

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> str:
    """
    Get directory to save files on user machine.
    """
    return os.path.expanduser(
        os.path.join(os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR), "trix", "dc_nuc")
    )


def download_ckpt_and_hyperparams(
    model_name: str, verbose: bool = True
) -> Tuple[hk.Params, Dict[str, Any]]:
    """
    Download checkpoint and hyperparams on kao datacenter.

    Args:
        model_name: Name of the model.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        Model hyperparameters dict.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    epoch_nums = {
        "dcnuc_500M_human_ref": 99,
        "dcnuc_500M_1000G": 51,
        "dcnuc_2B5_1000G": 230,
        "dcnuc_2B5_multi_species": 68,
        "dcnuc_bigbird_1B_1000G": 999,
        "dcnuc_bigbird_1B_multi_species": 85,
        "dcnuc_1B_edible_plants": 60,
        "dcnuc_5B_edible_plants": 208,
        "dcnuc_20B_teragenome": 228,
        "dcnuc_v2_500M_multi_species": 93,
        "dcnuc_v2_250M_multi_species": 30,
        "dcnuc_v2_100M_multi_species": 30,
        "dcnuc_v2_50M_multi_species": 30,
    }
    epoch_num = epoch_nums[model_name]
    params_save_dir = os.path.join(save_dir, f"epoch{epoch_num}.joblib")
    hyperparams_save_dir = os.path.join(save_dir, "hyperparams.json")

    if os.path.exists(hyperparams_save_dir) and os.path.exists(params_save_dir):
        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = joblib.load(f)

        return params, hyperparams

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Parsing model name with regex
        prog = re.compile(r"(dcnuc_bigbird|dcnuc_v2|dcnuc)_(\S+)")  # noqa: W605
        result = prog.match(model_name)

        model_class = result.group(1)  # dcnuc_bigbird, dcnuc or dcnuc_v2
        model_dir = result.group(2)  # e.g. 2B5_1000G

        print(f"Downloading checkpoints/{model_class}/{model_dir}/hyperparams.joblib")
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/{model_class}/{model_dir}/hyperparams.json",
            filename=hyperparams_save_dir,
            verbose=verbose,
        )
        print(
            f"Downloading checkpoints/{model_class}/{model_dir}/epoch{epoch_num}.joblib"
        )
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/{model_class}/{model_dir}/epoch{epoch_num}.joblib",
            filename=params_save_dir,
            verbose=verbose,
        )

        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = joblib.load(f)

        return params, hyperparams


def download_test_inputs_file(model_name: str) -> Dict[str, Any]:
    """
    Download files to check inputs and inference outputs.
    The returned dictionary contains a sequence, the tokens corresponding
    to that sequence, the logits corresponding to that sequence as well as
    embeddings at several layers.

    Args:
        model_name: Name of the model.

    Returns:
        Dict containing inputs and outputs.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    epoch_nums = {
        "dcnuc_500M_human_ref": 99,
        "dcnuc_500M_1000G": 51,
        "dcnuc_2B5_1000G": 230,
        "dcnuc_2B5_multi_species": 68,
        "dcnuc_bigbird_1B_1000G": 999,
        "dcnuc_bigbird_1B_multi_species": 85,
        "dcnuc_1B_edible_plants": 60,
        "dcnuc_5B_edible_plants": 208,
        "dcnuc_20B_teragenome": 228,
        "dcnuc_v2_500M_multi_species": 93,
        "dcnuc_v2_250M_multi_species": 30,
        "dcnuc_v2_100M_multi_species": 30,
        "dcnuc_v2_50M_multi_species": 30,
    }
    epoch_num = epoch_nums[model_name]
    test_file_save_dir = os.path.join(save_dir, f"test_inputs_epoch{epoch_num}.joblib")

    if os.path.exists(test_file_save_dir):
        # Load locally
        with open(test_file_save_dir, "rb") as f:
            test_inputs = joblib.load(f)

        return test_inputs  # type: ignore

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Parsing model name with regex
        prog = re.compile(r"(dcnuc_bigbird|dcnuc_v2|dcnuc)_(\S+)")  # noqa: W605
        result = prog.match(model_name)

        model_class = result.group(1)  # dcnuc_bigbird or dcnuc
        model_dir = result.group(2)  # e.g. 2B5_1000G

        bucket_file_path = f"checkpoints/{model_class}/{model_dir}/"
        bucket_file_path += f"test_inputs_epoch{epoch_num}.joblib"
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=bucket_file_path,
            filename=test_file_save_dir,
        )

        # Load locally
        with open(test_file_save_dir, "rb") as f:
            test_inputs = joblib.load(f)

        return test_inputs  # type: ignore


def get_pretrained_dcnuc_model(
    model_name: str,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    embeddings_layers_to_save: Tuple[int, ...] = (),
    attention_maps_to_save: Optional[List[Tuple[int, int]]] = None,
    max_positions: int = 1024,
    verbose: bool = True,
) -> Tuple[
    hk.Params, Callable, FixedSizeNucleotidesKmersTokenizer, ESMTransformerConfig
]:
    """
    Create a Haiku DeepChain Nucleotide
    model by downloading pre-trained weights and hyperparameters.
    DeepChain Nucleotide Models have ESM-like architectures.

    Args:
        model_name: Name of the model.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        embeddings_layers_to_save: Intermediate embddings to return in the output.
        attention_maps_to_save: Intermediate attention maps to return in the output.
        max_positions: Maximum length of a token (for padding).

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config (hyperparameters).

    Example:
        parameters, forward_fn, tokenizer, config = get_pretrained_dcnuc_model(
            model_name="dcnuc_500M_1000G",
            # Get embedding at layers 5 and 20
            embeddings_layers_to_save=(5, 20,),
            # Get attention map number 4 at layer 1 and attention map number 14
            # at layer 12
            attention_maps_to_save=((1,4), (12, 14)),
            max_positions=128,
        )
    """
    if attention_maps_to_save is None:
        attention_maps_to_save = []
    supported_models = [
        "dcnuc_500M_human_ref",
        "dcnuc_500M_1000G",
        "dcnuc_2B5_1000G",
        "dcnuc_2B5_multi_species",
        "dcnuc_bigbird_1B_1000G",
        "dcnuc_bigbird_1B_multi_species",
        "dcnuc_1B_edible_plants",
        "dcnuc_5B_edible_plants",
        "dcnuc_20B_teragenome",
        "dcnuc_v2_500M_multi_species",
        "dcnuc_v2_250M_multi_species",
        "dcnuc_v2_100M_multi_species",
        "dcnuc_v2_50M_multi_species",
    ]

    if not (model_name in supported_models):
        raise NotImplementedError(
            f"Unknown {model_name} model. " f"Supported models are {supported_models}"
        )

    # Download weights and hyperparams
    parameters, hyperparams = download_ckpt_and_hyperparams(model_name, verbose=verbose)

    # Parsing model name with regex
    prog = re.compile(r"(dcnuc_bigbird|dcnuc_v2|dcnuc)_(\S+)")  # noqa: W605
    result = prog.match(model_name)
    model_class = result.group(1)  # dcnuc_bigbird, dcnuc_v2 or dcnuc

    # Get tokenizer
    num_k_mers = hyperparams["k_for_kmers"]
    if model_class == "dcnuc_v2":
        # This model is more recent so the tokenizer can be simply used as it is
        tokenizer = FixedSizeNucleotidesKmersTokenizer(
            k_mers=num_k_mers,
            fixed_length=max_positions,
            prepend_cls_token=True,
        )
    else:
        # Re-index tokens to match with old code used to train initial models
        tokens_to_ids, _ = compute_dcnuc_tokens_to_ids(k_mers=num_k_mers)
        tokenizer = FixedSizeNucleotidesKmersTokenizer(
            k_mers=hyperparams["k_for_kmers"],
            fixed_length=max_positions,
            prepend_cls_token=True,
            tokens_to_ids=tokens_to_ids,
        )

    # Get model
    if model_class == "dcnuc" or model_class == "dcnuc_v2":
        # TODO: Uniformize all configs to avoid that
        if "key_dim" in hyperparams.keys():
            key_size = hyperparams["key_dim"]
        elif "key_size" in hyperparams.keys():
            key_size = hyperparams["key_size"]
        else:
            key_size = None

        if "add_bias_ffn" in hyperparams.keys():
            add_bias_ffn = hyperparams["add_bias_ffn"]
        else:
            add_bias_ffn = True

        if "ffn_activation_name" in hyperparams.keys():
            ffn_activation_name = hyperparams["ffn_activation_name"]
        else:
            ffn_activation_name = "gelu-no-approx"

        if "use_glu_in_ffn" in hyperparams.keys():
            use_glu_in_ffn = hyperparams["use_glu_in_ffn"]
        else:
            use_glu_in_ffn = False

        if model_class == "dcnuc_v2":
            alphabet_size = len(tokenizer.vocabulary)
        else:
            alphabet_size = len(tokenizer.vocabulary) - 2

        if "add_bias_kv" in hyperparams.keys():
            add_bias_kv = hyperparams["add_bias_kv"]
        else:
            add_bias_kv = False

        if "use_rotary_embedding" in hyperparams.keys():
            use_rotary_embedding = hyperparams["use_rotary_embedding"]
        else:
            use_rotary_embedding = False

        if "mask_before_attention" in hyperparams.keys():
            mask_before_attention = hyperparams["mask_before_attention"]
        else:
            mask_before_attention = False

        if "positional_embedding" in hyperparams.keys():
            positional_embedding = hyperparams["positional_embedding"]
        else:
            positional_embedding = "learned"

        if "lm_head" in hyperparams.keys():
            lm_head = hyperparams["lm_head"]
        else:
            lm_head = "roberta"

        # Get config
        config = ESMTransformerConfig(
            alphabet_size=alphabet_size,
            pad_token_id=tokenizer.pad_token_id,
            mask_token_id=tokenizer.mask_token_id,
            max_positions=hyperparams["max_positions"],
            embed_scale=hyperparams["embed_scale"],
            # architecture
            emb_layer_norm_before=hyperparams["emb_layer_norm_before"],
            key_size=key_size,
            attention_heads=hyperparams["attention_heads"],
            embed_dim=hyperparams["embed_dim"],
            ffn_embed_dim=hyperparams["ffn_embed_dim"],
            num_layers=hyperparams["num_layers"],
            positional_embedding=positional_embedding,
            lm_head=lm_head,
            add_bias_kv=add_bias_kv,
            add_bias_ffn=add_bias_ffn,
            use_glu_in_ffn=use_glu_in_ffn,
            ffn_activation_name=ffn_activation_name,
            use_rotary_embedding=use_rotary_embedding,
            mask_before_attention=mask_before_attention,
            # bert
            token_dropout=hyperparams["token_dropout"],
            masking_ratio=hyperparams["masking_ratio"],
            masking_prob=hyperparams["masking_prob"],
            # embeddings to save
            embeddings_layers_to_save=embeddings_layers_to_save,
            attention_maps_to_save=attention_maps_to_save,
        )

        # NOTE: module names are changed here, to validate !
        parameters = rename_dcnuc_modules(parameters, model_name)

        if param_dtype != jnp.float32:
            parameters = jax.tree_util.tree_map(
                lambda x: x.astype(param_dtype), parameters
            )

        forward_fn = build_esm_fn(
            model_config=config,
            compute_dtype=compute_dtype,
            param_dtype=param_dtype,
            output_dtype=output_dtype,
            model_name=model_name,
        )

    elif model_class == "dcnuc_bigbird":
        # Get config
        config = BigbirdConfig(
            alphabet_size=tokenizer.vocabulary_size - 2,
            pad_token_id=tokenizer.pad_token_id,
            mask_token_id=tokenizer.mask_token_id,
            max_positions=hyperparams["max_positions"],
            embed_dim=hyperparams["embed_dim"],
            ffn_embed_dim=hyperparams["ffn_embed_dim"],
            attention_heads=hyperparams["attention_heads"],
            num_layers=hyperparams["num_layers"],
            token_dropout=hyperparams["token_dropout"],
            masking_ratio=hyperparams["masking_ratio"],
            masking_prob=hyperparams["masking_prob"],
            block_size=hyperparams["block_size"],
            num_rand_blocks=hyperparams["num_rand_blocks"],
            use_gradient_checkpointing=hyperparams["use_remat"]
            if hyperparams.get("use_remat") is not None
            else False,
            embeddings_layers_to_save=embeddings_layers_to_save,
            attention_maps_to_save=attention_maps_to_save,
        )

        parameters = rename_dcnuc_modules(parameters, model_name=model_name)

        if param_dtype != jnp.float32:
            parameters = jax.tree_util.tree_map(
                lambda x: x.astype(param_dtype), parameters
            )

        forward_fn = build_bigbird_forward_fn(
            config,
            compute_dtype=jnp.float32,
            param_dtype=jnp.float32,
            output_dtype=jnp.float32,
            model_name=model_name,
        )

    else:
        # Should never happen
        raise ValueError()

    return parameters, forward_fn, tokenizer, config


def rename_dcnuc_modules(parameters: hk.Params, model_name: str) -> hk.Params:
    """
    Adjusts the names of the modules from checkpoints to ESMTransformer.

    Args:
        parameters: Parameters loaded from .joblib archive.
        model_name: Name of the loaded DC-NUC model.

    Returns:
        Parameters with updated names.
    """
    for layer_name in list(parameters.keys()):
        new_name = layer_name.replace("esm_transformer", model_name)
        new_name = new_name.replace("bigbird_transformer", model_name)

        if "attention_layer" in new_name:
            if new_name.split("/")[3] == "mha":
                new_name = "/".join(
                    new_name.split("/")[:3]
                    + ["self_attention"]
                    + new_name.split("/")[4:]
                )
        if "mha_layer_norm" in new_name:
            new_name = new_name.replace("mha_layer_norm", "self_attention_layer_norm")
        if "esm_roberta_lm_head" in new_name:
            new_name = new_name.replace("esm_roberta_lm_head", "roberta_lm_head")

        parameters[new_name] = parameters.pop(layer_name)

    return parameters
