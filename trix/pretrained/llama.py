"""Implementation of utilities to load a pretrained llama model in Trix."""
import gc
import json
import logging
import os
import re
import subprocess
from collections import defaultdict
from typing import Any, Callable, Dict, List, Mapping, Tuple, Union

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import torch
from transformers import AutoTokenizer, LlamaConfig, LlamaForCausalLM, LlamaTokenizer

import trix.pretrained
from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.utils.s3 import download_from_s3_bucket, init_s3_trix_bucket

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> Any:
    """
    Get directory to save files on user machine.
    """
    return os.path.expanduser(
        os.path.join(os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR), "trix", "llama")
    )


LLAMA_MODEL_NAME = "llama_decoder"


def translate_torch_params_from_original(
    torch_params: torch.nn.ParameterDict, num_layers: int
) -> Mapping[str, Mapping[str, np.ndarray]]:
    """
    Converts the full original PyTorch llama model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix llama
    architecture.

    Args:
        torch_params: Pretrained PyTorch llama model state_dict().
        num_layers: Number of decoder layers in the desired pre-trained model.

    Returns:
       Dictionary of Haiku parameters.
    """
    translate_dict = {}
    llama_prfix = f"{LLAMA_MODEL_NAME}/"

    translate_dict["tok_embeddings.weight"] = (
        llama_prfix + "~/token_embed",
        "embeddings",
    )

    for k in range(num_layers):
        prefix_layer = llama_prfix + f"gpt_decoder_layer_{k}/~/"
        torch_prefix = f"layers.{k}."

        # RMS norm
        translate_dict[torch_prefix + "attention_norm.weight"] = (
            prefix_layer + "attn_RMS_norm",
            "scale",
        )
        translate_dict[torch_prefix + "ffn_norm.weight"] = (
            prefix_layer + "ffn_RMS_norm",
            "scale",
        )

        # attention K, Q, V projections
        translate_dict[torch_prefix + "attention.wk.weight"] = (
            prefix_layer + "self_attn/~/key_linear",
            "w",
        )
        translate_dict[torch_prefix + "attention.wv.weight"] = (
            prefix_layer + "self_attn/~/value_linear",
            "w",
        )
        translate_dict[torch_prefix + "attention.wq.weight"] = (
            prefix_layer + "self_attn/~/query_linear",
            "w",
        )
        translate_dict[torch_prefix + "attention.wo.weight"] = (
            prefix_layer + "self_attn/~/out_linear",
            "w",
        )
        # MLP dense layers
        translate_dict[torch_prefix + "feed_forward.w2.weight"] = (
            prefix_layer + "fc2_linear",
            "w",
        )
        translate_dict[torch_prefix + "feed_forward.w1.weight"] = (
            prefix_layer + "fc1_linear_glu",
            "w",
        )
        translate_dict[torch_prefix + "feed_forward.w3.weight"] = (
            prefix_layer + "fc3_linear_glu",
            "w",
        )

    translate_dict["output.weight"] = (
        llama_prfix + "~/simple_lm_head/~/lm_final_fc",
        "w",
    )
    translate_dict["norm.weight"] = (
        llama_prfix + "~/final_RMS_norm",
        "scale",
    )

    params: Dict[str, Dict[str, np.ndarray]] = defaultdict(dict)
    for torch_key, (trix_key, weight_key) in translate_dict.items():
        if "weight" in torch_key and not ("tok_embeddings" in torch_key):
            # in pytorch, the weights of dense matrices indexation is transposes
            # compared to haiku, except for word-token-embedding
            params[trix_key][weight_key] = np.array(
                torch_params[torch_key], dtype=np.float16
            ).transpose()
        else:
            params[trix_key][weight_key] = np.array(
                torch_params[torch_key], dtype=np.float16
            )

    for k in range(num_layers):
        prefix_layer = llama_prfix + f"gpt_decoder_layer_{k}/~/"
        params[prefix_layer + "fc1_linear_glu"]["w"] = np.concatenate(
            (
                params[prefix_layer + "fc1_linear_glu"]["w"],
                params[prefix_layer + "fc3_linear_glu"]["w"],
            ),
            axis=1,
        )
        params.pop(prefix_layer + "fc3_linear_glu")
    return dict(params)


def translate_torch_params_from_huggingface(
    torch_params: torch.nn.ParameterDict,
    num_layers: int,
    num_heads: int,
    embed_dim: int,
) -> Mapping[str, Mapping[str, np.ndarray]]:
    """
    Converts the full Hugging Face PyTorch llama model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix llama
    architecture.

    Args:
        torch_params: Pretrained PyTorch llama model state_dict().
        num_layers: Number of decoder layers in the desired pre-trained model.
        num_heads: Number of attention heads.
        embed_dim: Embedding dimension.

    Returns:
       Dictionary of Haiku parameters.
    """

    def hf_permut_back(x: np.array) -> np.array:
        """
        Function to revert the permutation huggingface applied to original
        Llama keys and queries projections weights.
        """
        x = np.reshape(x, (num_heads, 2, embed_dim // num_heads // 2, embed_dim))
        x = np.transpose(x, (0, 2, 1, 3))
        x = np.reshape(x, (embed_dim, embed_dim))
        return x

    translate_dict = {}
    llama_prefix = f"{LLAMA_MODEL_NAME}/"

    translate_dict["model.embed_tokens.weight"] = (
        llama_prefix + "~/token_embed",
        "embeddings",
    )

    for k in range(num_layers):
        prefix_layer = llama_prefix + f"gpt_decoder_layer_{k}/~/"
        torch_prefix = f"model.layers.{k}."

        # Attention K, Q, V projections.
        translate_dict[torch_prefix + "self_attn.k_proj.weight"] = (
            prefix_layer + "self_attn/~/key_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attn.v_proj.weight"] = (
            prefix_layer + "self_attn/~/value_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attn.q_proj.weight"] = (
            prefix_layer + "self_attn/~/query_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attn.o_proj.weight"] = (
            prefix_layer + "self_attn/~/out_linear",
            "w",
        )

        # Attention layer norm.
        translate_dict[torch_prefix + "input_layernorm.weight"] = (
            prefix_layer + "attn_RMS_norm",
            "scale",
        )

        # MLP dense layers.
        translate_dict[torch_prefix + "mlp.gate_proj.weight"] = (
            prefix_layer + "fc1_linear",
            "w",
        )
        translate_dict[torch_prefix + "mlp.up_proj.weight"] = (
            prefix_layer + "fc3_linear",
            "w",
        )
        translate_dict[torch_prefix + "mlp.down_proj.weight"] = (
            prefix_layer + "fc2_linear",
            "w",
        )

        # MLP layer norm.
        translate_dict[torch_prefix + "post_attention_layernorm.weight"] = (
            prefix_layer + "ffn_RMS_norm",
            "scale",
        )

    translate_dict["model.norm.weight"] = (
        llama_prefix + "~/final_RMS_norm",
        "scale",
    )
    translate_dict["lm_head.weight"] = (
        llama_prefix + "~/simple_lm_head/~/lm_final_fc",
        "w",
    )

    params: Dict[str, Dict[str, np.ndarray]] = defaultdict(dict)
    for torch_key, (trix_key, weight_key) in translate_dict.items():
        if "weight" in torch_key and not ("embed" in torch_key):
            # in pytorch, the weights of dense matrices indexation is transposes
            # compared to haiku, except for word-token-embedding
            if "self_attn.k_proj" in torch_key or "self_attn.q_proj" in torch_key:
                params[trix_key][weight_key] = hf_permut_back(
                    np.array(torch_params[torch_key], dtype=np.float16)
                ).transpose()
            else:
                params[trix_key][weight_key] = np.array(
                    torch_params[torch_key], dtype=np.float16
                ).transpose()
        else:
            params[trix_key][weight_key] = np.array(
                torch_params[torch_key], dtype=np.float16
            )

    for k in range(num_layers):
        prefix_layer = llama_prefix + f"gpt_decoder_layer_{k}/~/"
        params[prefix_layer + "fc1_linear_glu"]["w"] = np.concatenate(
            (
                params[prefix_layer + "fc1_linear"]["w"],
                params[prefix_layer + "fc3_linear"]["w"],
            ),
            axis=1,
        )
        params.pop(prefix_layer + "fc1_linear")
        params.pop(prefix_layer + "fc3_linear")

    return dict(params)


def download_ckpt_and_hyperparams_from_original(
    model: str,
) -> Tuple[hk.Params, Dict[str, Any]]:
    """
    Download original llama weights and config and translate them into
    Trix format.
    Args:
        model: LLAMA model size, can be one of ['7B','13B']

    Returns:
        Model parameters.
        Hyperparameters dict.
    """
    target_folder = _get_dir()

    num_parts_dict = {}
    num_parts_dict["7B"] = 1
    num_parts_dict["13B"] = 2
    num_parts_dict["30B"] = 4
    num_parts_dict["65B"] = 8

    assert model in list(num_parts_dict.keys()), (
        f"unrecognized LLAMA model {model}." f"It should be in ['7B','13B','30B','65B']"
    )
    number_of_param_billions = int(re.search(r"\d+", model).group())  # type: ignore
    logging.warning(
        f"you're using the {model} LLAMA model. The model weights are in "
        f"fp16. you'll need at least {number_of_param_billions*2}GB on "
        f"your disk and on your RAM"
    )

    here = trix.pretrained.__file__.replace("/__init__.py", "")
    cmd = f"{here}/download_llama_weights.sh", model, target_folder

    subprocess.run(cmd, capture_output=False)

    with open(f"{target_folder}/{model}/params.json") as f:
        config_json = json.loads(f.read())

    if num_parts_dict[model] >= 2:
        model_params: Dict[str, List[torch.Tensor.type]] = defaultdict(lambda: [])
        for part_index in range(num_parts_dict[model]):
            for k, v in torch.load(
                f"{target_folder}/{model}/"
                "consolidated.{:02d}.pth".format(part_index),
                map_location="cpu",
            ).items():
                model_params[k].append(v)

        for k, v in model_params.items():
            if len(v[0].shape) == 1:
                model_params[k] = v[0]
            elif len(v[0].shape) == 2:
                dim_0, dim_1 = v[0].shape
                dim_00, dim_11 = v[1].shape
                if dim_0 == config_json["dim"]:
                    model_params[k] = torch.concat(v, dim=1)
                elif dim_1 == config_json["dim"]:
                    model_params[k] = torch.concat(v, dim=0)
                else:
                    if dim_00 == dim_0:
                        model_params[k] = torch.concat(v, dim=1)
                    elif dim_11 == dim_1:
                        model_params[k] = torch.concat(v, dim=0)
    else:
        model_params = torch.load(
            f"{target_folder}/{model}/consolidated.00.pth",
            map_location="cpu",
        )
    parameters = translate_torch_params_from_original(
        model_params, config_json["n_layers"]
    )
    del model_params
    gc.collect()

    return parameters, config_json


def download_tokenizer_from_kao(
    model_size: str, model_type: str, verbose: bool = True
) -> LlamaTokenizer:
    """
    Download llama tokenizer in huggingface format from kao.

    Args:
        model_size: LLAMA model size, can be one of ['7B','13B']
        model_type: LLama, LLama 2, or Vicuna, one of ['llama', 'llama_2',
            'llama_2_chat', 'vicuna']
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Tokenizer
    """
    _, tokenizer = download_from_kao(
        model_size=model_size,
        model_type=model_type,
        include_model=False,
        verbose=verbose,
    )
    return tokenizer


def download_from_kao(
    model_size: str,
    model_type: str,
    include_model: bool = True,
    download_in_float32: bool = False,
    verbose: bool = True,
) -> Tuple[Union[LlamaForCausalLM, None], LlamaTokenizer]:
    """
    Download llama weights in huggingface format from kao.
    Args:
        model_size: LLAMA model size, can be one of ['7B','13B']
        model_type: LLama or Vicuna, one of ['llama', 'llama_2',
            'llama_2_chat', 'vicuna']
        include_model: whether to include the HF model (True) or
            only the tokenizer (False). Default: True
        download_in_float32: whether to create the torch model using
            torch.float32 as dtype (True) or torch.float16 (False).
            Necessary for some tests.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        HF config.
        Tokenizer
    """
    if model_size not in {"7B", "13B"}:
        raise NotImplementedError(
            f"Model {model_size} not supported for now. This function "
            f"supports only 7B and 13B."
        )

    if model_type not in {"llama", "llama_2", "llama_2_chat", "vicuna"}:
        raise NotImplementedError(
            f"Model type {model_type} not supported for now. This function "
            f"supports only llama, llama_2, llama_2_chat, and vicuna."
        )

    model_name = f"{model_type}_{model_size}"
    save_dir = os.path.join(_get_dir(), model_name)

    if model_name == "llama_2_7B":
        raise NotImplementedError(
            "Currently the llama_2_7B model is not supported due to"
            "unidentified computing inaccuracies (work in progress)"
        )

    num_parts_dict = {}
    num_parts_dict["7B"] = 2
    num_parts_dict["13B"] = 3

    files_to_download = [
        "generation_config.json",
        "special_tokens_map.json",
        "tokenizer.model",
        "tokenizer_config.json",
    ]

    if include_model:
        files_to_download += ["config.json"]
        files_to_download += ["pytorch_model.bin.index.json"]
        num_parts = num_parts_dict[model_size]
        files_to_download += [
            f"pytorch_model-{i:05}-of-{num_parts:05}.bin"
            for i in range(1, num_parts + 1)
        ]

    if model_type in {"llama", "llama_2", "llama_2_chat"}:
        files_to_download += ["tokenizer.json"]

    local_files_paths = [os.path.join(save_dir, file) for file in files_to_download]
    if not all([os.path.exists(path) for path in local_files_paths]):

        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        # Download files from kao
        kao_folder = (
            "llama_huggingface"
            if model_type in {"llama", "vicuna"}
            else "llama_2_huggingface"
        )
        for file in files_to_download:
            download_from_s3_bucket(
                s3_client=s3_client,
                bucket=bucket,
                key=f"checkpoints/{kao_folder}/{model_name}/{file}",
                filename=os.path.join(save_dir, file),
                verbose=verbose,
            )

    dtype = torch.float32 if download_in_float32 else torch.float16
    hf_tokenizer = LlamaTokenizer.from_pretrained(
        pretrained_model_name_or_path=save_dir,
    )
    hf_model = (
        LlamaForCausalLM.from_pretrained(
            pretrained_model_name_or_path=save_dir,
            torch_dtype=dtype,
        )
        if include_model
        else None
    )

    return hf_model, hf_tokenizer


def get_ckpt_config_and_tokenizer_from_kao(
    model_size: str, model_type: str, verbose: bool = True
) -> Tuple[hk.Params, LlamaConfig, LlamaTokenizer]:
    """
    Download llama weights in huggingface format from kao.
    Args:
        model_size: LLAMA model size, can be one of ['7B','13B']
        model_type: LLama or Vicuna, one of ['llama', 'llama_2',
            'llama_2_chat', 'vicuna']
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        HF config.
        Tokenizer
    """
    hf_model, hf_tokenizer = download_from_kao(
        model_size=model_size,
        model_type=model_type,
        include_model=True,
        verbose=verbose,
    )

    assert hf_model is not None, "The returned model can't be None"

    # Get weights and config
    torch_params = hf_model.state_dict()
    hf_config = hf_model.config
    hk_params = translate_torch_params_from_huggingface(
        torch_params,
        num_layers=hf_config.num_hidden_layers,
        num_heads=hf_config.num_attention_heads,
        embed_dim=hf_config.hidden_size,
    )

    return hk_params, hf_config, hf_tokenizer


def get_model_type_and_size(
    model_name: str = "llama_7B",
) -> Tuple[str, str]:

    allowed_model_names = {
        "llama_7B",
        "llama_13B",
        "vicuna_7B",
        "vicuna_13B",
        "llama_2_7B",
        "llama_2_13B",
        "llama_2_chat_7B",
        "llama_2_chat_13B",
    }

    if model_name not in allowed_model_names:
        raise NotImplementedError(
            f"Model {model_name} not supported for now. This function "
            f"supports only llama_7B, llama_13B, vicuna_7B, vicuna_13B, "
            f"llama_2_7B, llama_2_13B, llama_2_chat_7B, llama_2_chat_13B."
        )

    # retrieve model size and type
    model_name_split = model_name.split("_")
    model_size = model_name_split[-1]
    model_type = "_".join(model_name_split[:-1])

    return model_type, model_size


def get_pretrained_llama_model(
    compute_dtype: jnp.dtype = jnp.float16,
    param_dtype: jnp.dtype = jnp.float16,
    output_dtype: jnp.dtype = jnp.float16,
    model_name: str = "llama_7B",
    use_gradient_checkpointing: bool = False,
    verbose: bool = True,
) -> Tuple[hk.Params, Callable, AutoTokenizer, GptConfig]:
    """
    Create a Haiku llama model by downloading the pytorch weights hosted by huggin face
        and translating them.
    Args:
        compute_dtype: which type of dtype to compute activations.
            bfloat16 can represent higher number than float16, it is preferred
            when training.
            The accepted types are : jnp.float16, jnp.bfloat16, jnp.float32
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.
        model_name: LLAMA 1 model, ['llama_7B', 'llama_13B', 'vicuna_7B', 'vicuna_13B']
            or LLAMA 2 model, ['llama_2_7B', 'llama_2_13B', 'llama_2_chat_7B',
            'llama_2_chat_13B']
        use_gradient_checkpointing: whether to activate gradient checkpointing on all
            transformer layers.
        verbose: Whether or not to print the progress bar during the downloads.

    NOTE: with, since he model is released in fp16, it should rather be
            called half_precision
    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config
    """
    if not (
        param_dtype == jnp.float16
        or param_dtype == jnp.float32
        or param_dtype == jnp.bfloat16
    ):
        raise ValueError(
            f"param dtype {param_dtype} is not supported. "
            f"Only float16, float32 and bfloat 16 are supported."
        )

    model_type, model_size = get_model_type_and_size(model_name)

    # download config and parameters
    params, hf_config, tokenizer = get_ckpt_config_and_tokenizer_from_kao(
        model_size=model_size,
        model_type=model_type,
        verbose=verbose,
    )

    # get GPT config
    config = GptConfig(
        vocab_size=tokenizer.vocab_size,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=hf_config.hidden_size,
        ffn_embed_dim=hf_config.intermediate_size,
        num_heads=hf_config.num_attention_heads,
        num_layers=hf_config.num_hidden_layers,
        rope_dimensions=None,
        max_position_embeddings=hf_config.max_position_embeddings,
        add_bias_ffn=False,
        norm_type="RMS_norm",
        parallel_attention_ff=False,
        ffn_activation_name="silu",
        use_glu_in_ffn=True,
        add_bias_lm_head=False,
        use_gradient_checkpointing=use_gradient_checkpointing,
    )

    # cast model parameters if needed
    if param_dtype != jnp.float16:
        params = jax.tree_util.tree_map(lambda x: x.astype(param_dtype), params)

    # build gpt function
    llama_fn = build_gpt_fn(
        config,
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        name=LLAMA_MODEL_NAME,
    )

    return params, llama_fn, tokenizer, config


def get_pretrained_llama_tokenizer(
    model_name: str = "llama_7B", verbose: bool = True
) -> LlamaTokenizer:
    """
    Create a Haiku llama model by downloading the pytorch weights hosted by huggin face
        and translating them.
    Args:
        model_name: LLAMA or LLAMA 2 model, ['llama_7B', 'llama_13B', 'vicuna_7B',
            'vicuna_13B', 'llama_2_7B', 'llama_2_13B', 'llama_2_chat_7B',
            'llama_2_chat_13B']
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Tokenizer.
    """

    model_type, model_size = get_model_type_and_size(model_name)

    # download config and parameters
    _, tokenizer = download_from_kao(
        model_size=model_size,
        model_type=model_type,
        include_model=False,
        verbose=verbose,
    )

    return tokenizer


def get_pretrained_llama_model_deprecated(
    compute_dtype: jnp.dtype = jnp.float16,
    param_dtype: jnp.dtype = jnp.float16,
    output_dtype: jnp.dtype = jnp.float16,
    model: str = "7B",
    use_gradient_checkpointing: bool = False,
) -> Tuple[hk.Params, Callable, AutoTokenizer, GptConfig]:
    """
    Create a Haiku llama model by downloading the pytorch weights hosted by huggin face
        and translating them.
    Args:
        compute_dtype: which type of dtype to compute activations.
            bfloat16 can represent higher number than float16, it is preferred
            when training.
            The accepted types are : jnp.float16, jnp.bfloat16, jnp.float32
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.
        model: LLAMA model size, can be one of ['7B','13B']
        use_gradient_checkpointing: whether to activate gradient checkpointing on all
            transformer layers.

    NOTE: with, since he model is released in fp16, it should rather be
            called half_precision
    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config
    """
    if not (
        param_dtype == jnp.float16
        or param_dtype == jnp.float32
        or param_dtype == jnp.bfloat16
    ):
        raise ValueError(
            f"param dtype {param_dtype} is not supported. "
            f"Only float16, float32 and bfloat 16 are supported."
        )

    # download config and parameters
    params, hyperparams = download_ckpt_and_hyperparams_from_original(model=model)

    # get tokenizer
    tokenizer = AutoTokenizer.from_pretrained("Neko-Institute-of-Science/LLaMA-7B-HF")
    tokenizer.pad_token_id = tokenizer.eos_token_id
    tokenizer.padding_side = "right"

    # get GPT config
    ffn_embed_dim = hyperparams["multiple_of"] * (
        (hyperparams["dim"] * 4 * 2 / 3 + hyperparams["multiple_of"] - 1)
        // hyperparams["multiple_of"]
    )
    config = GptConfig(
        vocab_size=tokenizer.vocab_size,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=hyperparams["dim"],
        ffn_embed_dim=ffn_embed_dim,
        num_heads=hyperparams["n_heads"],
        num_layers=hyperparams["n_layers"],
        rope_dimensions=None,
        max_position_embeddings=2048,
        add_bias_ffn=False,
        norm_type="RMS_norm",
        parallel_attention_ff=False,
        ffn_activation_name="silu",
        use_glu_in_ffn=True,
        add_bias_lm_head=False,
        use_gradient_checkpointing=use_gradient_checkpointing,
    )

    # cast model parameters if needed
    if param_dtype != jnp.float16:
        params = jax.tree_util.tree_map(lambda x: x.astype(param_dtype), params)

    # build gpt function
    llama_fn = build_gpt_fn(
        config,
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        name=LLAMA_MODEL_NAME,
    )

    return params, llama_fn, tokenizer, config
