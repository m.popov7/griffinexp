"""Implementation of utilities to load a pretrained T5 model in Trix."""
from collections import defaultdict
from typing import Callable, Dict, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import torch
from transformers import (
    T5EncoderModel,
    T5ForConditionalGeneration,
    T5Tokenizer,
    logging,
)

from trix.models.t5.decoder import T5DecoderConfig
from trix.models.t5.encoder import T5EncoderConfig, build_t5_encoder_fn
from trix.models.t5.model import T5Config, build_t5_fn

logging.set_verbosity_error()


def translate_encoder_layer(
    torch_params: dict,
    layer: int,
    use_glu_in_ffn: bool = False,
    module_name: Optional[str] = "t5_transformer",
) -> hk.Params:
    """
    Converts 1 encoder layer in the Hugging Face PyTorch T5 model to Haiku parameters.
    Note that the parameters names match the defaults defined in the T5
    architecture.  Other choices may cause errors.  See:
    https://github.com/huggingface/transformers/tree/main/src/transformers/models/t5

    Args:
        torch_params: Pretrained PyTorch T5 model state_dict().
        layer: Numbered encoder layer in the desired pre-trained model.
        use_glu_in_ffn: Whether Gated Linear Unit (GLU) is used in Feed
            Forward Network (FFN) block.

    Returns:
       Dictionary of Haiku parameters.
    """
    l_str = "" if layer == 0 else f"_{layer}"
    layer_norms = ["self_attention_layer_norm", "fc_layer_norm"]
    if module_name is not None:
        module_prefix = f"{module_name}/~/"
    else:
        module_prefix = ""

    haiku_prefix = f"{module_prefix}t5_encoder/~/t5_encoder_layer"
    attention_prefix = "/~/self_attention/~/"
    params: Dict = defaultdict(dict)

    # Note that PyTorch stores the transpose of the weight matrices
    for x in ["query_linear", "key_linear", "value_linear", "out_linear"]:
        params[f"{haiku_prefix}{l_str}{attention_prefix}{x}"]["w"] = jnp.transpose(
            jnp.array(
                torch_params[
                    f"encoder.block.{layer}.layer.0.SelfAttention.{x[0]}.weight"
                ]
            )
        )

    for i in range(2):
        params[f"{haiku_prefix}{l_str}/~/{layer_norms[i]}"]["w"] = jnp.array(
            torch_params[f"encoder.block.{layer}.layer.{i}.layer_norm.weight"]
        )

    if use_glu_in_ffn:
        torch_wi_1 = jnp.array(
            torch_params[f"encoder.block.{layer}.layer.1.DenseReluDense.wi_0.weight"]
        )
        torch_wi_2 = jnp.array(
            torch_params[f"encoder.block.{layer}.layer.1.DenseReluDense.wi_1.weight"]
        )
        params[f"{haiku_prefix}{l_str}/~/fc1_linear"]["w"] = jnp.transpose(
            jnp.concatenate([torch_wi_1, torch_wi_2], axis=0)
        )
    else:
        params[f"{haiku_prefix}{l_str}/~/fc1_linear"]["w"] = jnp.transpose(
            jnp.array(
                torch_params[f"encoder.block.{layer}.layer.1.DenseReluDense.wi.weight"]
            )
        )

    params[f"{haiku_prefix}{l_str}/~/fc2_linear"]["w"] = jnp.transpose(
        jnp.array(
            torch_params[f"encoder.block.{layer}.layer.1.DenseReluDense.wo.weight"]
        )
    )

    return params


def translate_encoder(
    torch_params: torch.nn.ParameterDict,
    num_layers: int,
    use_glu_in_ffn: bool = False,
    module_name: Optional[str] = None,
) -> hk.Params:
    """
    Converts all layers from the Hugging Face PyTorch T5 model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix T5
    architecture.  Other choices may cause errors

    Args:
        torch_params: Pretrained PyTorch T5 model state_dict().
        num_layers: Total number of encoder layers.
        use_glu_in_ffn: Whether Gated Linear Unit (GLU) is used in Feed
            Forward Network (FFN) block.
        module_name: Name to give to the model.

    Returns:
       Dictionary of Haiku parameters.
    """
    params: Dict = defaultdict(dict)
    if module_name is not None:
        module_prefix = f"{module_name}/~/"
    else:
        module_prefix = ""

    for i in range(num_layers):
        params.update(
            translate_encoder_layer(
                torch_params, i, use_glu_in_ffn=use_glu_in_ffn, module_name=module_name
            )
        )
    params[f"{module_prefix}t5_encoder/~/final_layer_norm"]["w"] = jnp.array(
        torch_params["encoder.final_layer_norm.weight"]
    )
    params[f"{module_prefix}t5_encoder/~/token_embed"]["embeddings"] = jnp.array(
        torch_params["shared.weight"]
    )
    params[f"{module_prefix}t5_encoder/~/positional_bias/~/positional_embed"][
        "embeddings"
    ] = jnp.array(
        torch_params[
            "encoder.block.0.layer.0.SelfAttention.relative_attention_bias.weight"
        ]
    )
    return params


def translate_decoder_layer(
    torch_params: torch.nn.ParameterDict,
    layer: int,
    use_glu_in_ffn: bool = False,
    module_name: str = "t5_transformer",
) -> hk.Params:
    """
    Converts 1 decoder layer from the Hugging Face PyTorch T5 model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix T5
    architecture.  Other choices may cause errors

    Args:
        torch_params: Pretrained PyTorch T5 model state_dict().
        layer: Numbered encoder layer in the desired pre-trained model.
        use_glu_in_ffn: Whether Gated Linear Unit (GLU) is used in Feed
            Forward Network (FFN) block.

    Returns:
       Dictionary of Haiku parameters.
    """
    if module_name is not None:
        module_prefix = f"{module_name}/~/"
    else:
        module_prefix = ""
    l_str = "" if layer == 0 else f"_{layer}"
    layer_norms = [
        "self_attention_layer_norm",
        "cross_attention_layer_norm",
        "fc_layer_norm",
    ]
    haiku_prefix = f"{module_prefix}t5_decoder/~/t5_decoder_layer"
    self_attention_prefix = "/~/self_attention/~/"
    cross_attention_prefix = "/~/cross_attention/~/"
    params: Dict = defaultdict(dict)

    for x in ["query_linear", "key_linear", "value_linear", "out_linear"]:
        params[f"{haiku_prefix}{l_str}{self_attention_prefix}{x}"]["w"] = jnp.transpose(
            jnp.array(
                torch_params[
                    f"decoder.block.{layer}.layer.0.SelfAttention.{x[0]}.weight"
                ]
            )
        )
        params[f"{haiku_prefix}{l_str}{cross_attention_prefix}{x}"][
            "w"
        ] = jnp.transpose(
            jnp.array(
                torch_params[
                    f"decoder.block.{layer}.layer.1.EncDecAttention.{x[0]}.weight"
                ]
            )
        )

    for i in range(3):
        params[f"{haiku_prefix}{l_str}/~/{layer_norms[i]}"]["w"] = jnp.array(
            torch_params[f"decoder.block.{layer}.layer.{i}.layer_norm.weight"]
        )

    if use_glu_in_ffn:
        torch_wi_1 = jnp.array(
            torch_params[f"decoder.block.{layer}.layer.2.DenseReluDense.wi_0.weight"]
        )
        torch_wi_2 = jnp.array(
            torch_params[f"decoder.block.{layer}.layer.2.DenseReluDense.wi_1.weight"]
        )
        params[f"{haiku_prefix}{l_str}/~/fc1_linear"]["w"] = jnp.transpose(
            jnp.transpose(jnp.concatenate([torch_wi_1, torch_wi_2], axis=0)).T
        )
    else:
        params[f"{haiku_prefix}{l_str}/~/fc1_linear"]["w"] = jnp.transpose(
            jnp.array(
                torch_params[f"decoder.block.{layer}.layer.2.DenseReluDense.wi.weight"]
            )
        )

    params[f"{haiku_prefix}{l_str}/~/fc2_linear"]["w"] = jnp.transpose(
        jnp.array(
            torch_params[f"decoder.block.{layer}.layer.2.DenseReluDense.wo.weight"]
        )
    )

    return params


def translate_decoder(
    torch_params: torch.nn.ParameterDict,
    num_layers: int,
    use_glu_in_ffn: bool = False,
    with_lm_head: bool = False,
    module_name: str = "t5_transformer",
) -> hk.Params:
    """
    Converts all decoder layers from the Hugging Face PyTorch T5 model to Haiku
    parameters. Note that the parameters names match the defaults defined in the Trix T5
    architecture.  Other choices may cause errors

    Args:
        torch_params: Pretrained PyTorch T5 model state_dict().
        num_layers: Total number of encoder layers.
        use_glu_in_ffn: Whether Gated Linear Unit (GLU) is used in Feed
            Forward Network (FFN) block.
        with_lm_head: Whether model has lm head (Flan-T5) or not (T5, computed using
            token embeddings).

    Returns:
       Dictionary of Haiku parameters.
    """
    if module_name is not None:
        module_prefix = f"{module_name}/~/"
    else:
        module_prefix = ""
    params: Dict = defaultdict(dict)
    for i in range(num_layers):
        params.update(
            translate_decoder_layer(
                torch_params,
                layer=i,
                use_glu_in_ffn=use_glu_in_ffn,
                module_name=module_name,
            )
        )
    params[f"{module_prefix}t5_decoder/~/token_embed"]["embeddings"] = jnp.array(
        torch_params["shared.weight"]
    )

    params[f"{module_prefix}t5_decoder/~/positional_bias/~/positional_embed"][
        "embeddings"
    ] = jnp.array(
        torch_params[
            "decoder.block.0.layer.0.SelfAttention.relative_attention_bias.weight"
        ]
    )

    params[f"{module_prefix}t5_decoder/~/final_layer_norm"]["w"] = jnp.array(
        torch_params["decoder.final_layer_norm.weight"]
    )
    if with_lm_head is True:
        params[f"{module_prefix}t5_decoder/~/lm_head_linear"]["w"] = jnp.transpose(
            jnp.array(torch_params["lm_head.weight"])
        )

    return params


def translate_transformer(
    torch_params: torch.nn.ParameterDict,
    encoder_num_layers: int,
    decoder_num_layers: int,
    use_glu_in_ffn: bool = False,
    with_lm_head: bool = False,
    module_name: str = "t5_transformer",
) -> hk.Params:
    """
    Converts the full Hugging Face PyTorch T5 model to Haiku parameters.  Supports
    the original T5 architecture, as well as the T5 1.1 and Flan-type architectures.
    Note that the parameters names match the defaults defined in the Trix T5
    architecture.  Other choices may cause errors.  See:
    https://github.com/huggingface/transformers/tree/main/src/transformers/models/t5
    The torch_params can be retrieved using
    T5Model.from_pretrained(<model_name>).state_dict()

    Args:
        torch_params: Pretrained PyTorch Ankh model state_dict().
        encoder_num_layers: Number of encoder layers in the desired pre-trained model.
        decoder_num_layers: Number of encoder layers in the desired pre-trained model.
        use_glu_in_ffn: Whether Gated Linear Unit (GLU) is used in Feed
            Forward Network (FFN) block.
        with_lm_head: Whether the model adds a separate lm head or reuses the token
            embedding layer.

    Returns:
       Dictionary of Haiku parameters.
    """
    params: Dict = defaultdict(dict)
    params.update(
        translate_encoder(
            torch_params,
            encoder_num_layers,
            use_glu_in_ffn=use_glu_in_ffn,
            module_name=module_name,
        )
    )
    params.update(
        translate_decoder(
            torch_params,
            decoder_num_layers,
            use_glu_in_ffn=use_glu_in_ffn,
            with_lm_head=with_lm_head,
            module_name=module_name,
        )
    )

    return dict(params)


def get_pretrained_t5_model(
    model_name: str,
    encoder_only: bool = False,
    embeddings_layers_to_save: Optional[Tuple[int, ...]] = None,
    module_name: Optional[str] = None,
) -> Tuple[hk.Params, Callable, T5Tokenizer, T5Config]:
    """
    Create a Haiku T5 model either by downloading the model directly or by
    downloading transforming a pretrained PyTorch model.

    Args:
        model_name: Name of the model.  Select from ['t5-small', 't5-base', 't5-large',
            'flan-t5-small', 'flan-t5-base', 'flan-t5-large'].

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config (hyperparameters).
    """
    if embeddings_layers_to_save is None:
        embeddings_layers_to_save = ()

    if embeddings_layers_to_save is None:
        embeddings_layers_to_save = ()

    supported_list = [
        "t5-small",
        "t5-base",
        "t5-large",
        "flan-t5-small",
        "flan-t5-base",
        "flan-t5-large",
    ]
    if not (model_name in supported_list):
        raise NotImplementedError(
            f"Unknown T5 {model_name} model. " f"Supported models are {supported_list}"
        )

    if model_name == "t5-small":
        encoder_config = T5EncoderConfig(
            embeddings_layers_to_save=embeddings_layers_to_save, use_glu_in_ffn=False
        )
        decoder_config = T5DecoderConfig(use_glu_in_ffn=False)

        model_config = T5Config(
            encoder_config=encoder_config, decoder_config=decoder_config
        )

    if model_name == "t5-base":
        encoder_config = T5EncoderConfig(
            embed_dim=768,
            attention_embed_dim=768,
            ffn_embed_dim=3072,
            num_heads=12,
            num_layers=12,
            ffn_activation_name="relu",
            use_glu_in_ffn=False,
            embeddings_layers_to_save=embeddings_layers_to_save,
        )
        decoder_config = T5DecoderConfig(
            embed_dim=768,
            attention_embed_dim=768,
            ffn_embed_dim=3072,
            num_heads=12,
            num_layers=12,
            ffn_activation_name="relu",
            use_glu_in_ffn=False,
        )

        model_config = T5Config(
            encoder_config=encoder_config, decoder_config=decoder_config
        )

    if model_name == "t5-large":
        encoder_config = T5EncoderConfig(
            embed_dim=1024,
            attention_embed_dim=1024,
            ffn_embed_dim=4096,
            num_heads=16,
            num_layers=24,
            ffn_activation_name="relu",
            use_glu_in_ffn=False,
            embeddings_layers_to_save=embeddings_layers_to_save,
        )
        decoder_config = T5DecoderConfig(
            embed_dim=1024,
            attention_embed_dim=1024,
            ffn_embed_dim=4096,
            num_heads=16,
            num_layers=24,
            ffn_activation_name="relu",
            use_glu_in_ffn=False,
        )

        model_config = T5Config(
            encoder_config=encoder_config, decoder_config=decoder_config
        )

    if model_name == "flan-t5-small":
        encoder_config = T5EncoderConfig(
            embed_dim=512,
            attention_embed_dim=384,
            ffn_embed_dim=1024,
            num_heads=6,
            num_layers=8,
            ffn_activation_name="gelu",
            use_glu_in_ffn=True,
            embeddings_layers_to_save=embeddings_layers_to_save,
        )

        decoder_config = T5DecoderConfig(
            embed_dim=512,
            attention_embed_dim=384,
            ffn_embed_dim=1024,
            num_heads=6,
            num_layers=8,
            ffn_activation_name="gelu",
            use_glu_in_ffn=True,
            logits_from_embed=False,
        )
        model_config = T5Config(
            encoder_config=encoder_config, decoder_config=decoder_config
        )

    if model_name == "flan-t5-base":
        encoder_config = T5EncoderConfig(
            embed_dim=768,
            attention_embed_dim=768,
            ffn_embed_dim=2048,
            num_heads=12,
            num_layers=12,
            ffn_activation_name="gelu",
            use_glu_in_ffn=True,
            embeddings_layers_to_save=embeddings_layers_to_save,
        )

        decoder_config = T5DecoderConfig(
            embed_dim=768,
            attention_embed_dim=768,
            ffn_embed_dim=2048,
            num_heads=12,
            num_layers=12,
            ffn_activation_name="gelu",
            use_glu_in_ffn=True,
            logits_from_embed=False,
        )
        model_config = T5Config(
            encoder_config=encoder_config, decoder_config=decoder_config
        )

    if model_name == "flan-t5-large":
        encoder_config = T5EncoderConfig(
            embed_dim=1024,
            attention_embed_dim=1024,
            ffn_embed_dim=2816,
            num_heads=16,
            num_layers=24,
            ffn_activation_name="gelu",
            use_glu_in_ffn=True,
            embeddings_layers_to_save=embeddings_layers_to_save,
        )

        decoder_config = T5DecoderConfig(
            embed_dim=1024,
            attention_embed_dim=1024,
            ffn_embed_dim=2816,
            num_heads=16,
            num_layers=24,
            ffn_activation_name="gelu",
            use_glu_in_ffn=True,
            logits_from_embed=False,
        )

        model_config = T5Config(
            encoder_config=encoder_config, decoder_config=decoder_config
        )

    if model_name[0] == "f":
        model_name = "google/" + model_name
    model_params = T5ForConditionalGeneration.from_pretrained(model_name).state_dict()
    tokenizer = T5Tokenizer.from_pretrained(model_name, model_max_length=512)
    if encoder_only is True:
        model_params = T5EncoderModel.from_pretrained(model_name).state_dict()
        tokenizer = T5Tokenizer.from_pretrained(model_name, model_max_length=512)
        parameters = translate_encoder(
            torch_params=model_params,
            num_layers=encoder_config.num_layers,
            use_glu_in_ffn=encoder_config.use_glu_in_ffn,
            module_name=None,
        )
        if module_name is not None:
            for parameter_name in list(parameters.keys()):
                new_name = parameter_name.replace("t5_encoder/", f"{module_name}/")
                parameters[new_name] = parameters.pop(parameter_name)

        t5_fn = build_t5_encoder_fn(encoder_config, name=module_name)
    else:
        model_params = T5ForConditionalGeneration.from_pretrained(
            model_name
        ).state_dict()
        tokenizer = T5Tokenizer.from_pretrained(model_name, model_max_length=512)
        parameters = translate_transformer(
            torch_params=model_params,
            encoder_num_layers=encoder_config.num_layers,
            decoder_num_layers=decoder_config.num_layers,
            use_glu_in_ffn=encoder_config.use_glu_in_ffn,
            with_lm_head=(not decoder_config.logits_from_embed),
            module_name="t5_transformer" if module_name is None else module_name,
        )

        t5_fn = build_t5_fn(config=model_config, name=module_name)

    return parameters, t5_fn, tokenizer, model_config


def get_t5_decoder_config(model_name: str) -> T5DecoderConfig:
    """
    Returns decoder configs for t5 models
    Args:
        model_name: name of the t5 variant.
    Returns:
        T5 decoder config
    """
    config = T5DecoderConfig()
    if model_name == "t5-base":
        config.vocab_size = 256
        config.embed_dim = 768
        config.num_layers = 12
        config.num_heads = 12
        config.attention_embed_dim = 768  # 64 (per head dim * num_heads)
        config.ffn_embed_dim = 3072
        config.ffn_activation_name = "gelu"
        config.logits_from_embed = False
    elif model_name == "t5-large":
        config.vocab_size = 256
        config.embed_dim = 1024
        config.num_layers = 24
        config.num_heads = 16
        config.attention_embed_dim = 1024
        config.ffn_embed_dim = 4096
        config.ffn_activation_name = "gelu"
        config.logits_from_embed = False
    elif model_name == "t5-mini":
        config.vocab_size = 256
        config.embed_dim = 256
        config.num_layers = 4
        config.num_heads = 4
        config.attention_embed_dim = 1024
        config.ffn_embed_dim = 1024
        config.ffn_activation_name = "gelu"
        config.logits_from_embed = False
    elif model_name == "t5-small":
        config.vocab_size = 256
        config.embed_dim = 512
        config.num_layers = 6
        config.num_heads = 8
        config.attention_embed_dim = 512
        config.ffn_embed_dim = 2048
        config.ffn_activation_name = "gelu"
        config.logits_from_embed = False
    elif model_name == "t5-tiny":
        config.vocab_size = 256
        config.embed_dim = 128
        config.num_layers = 2
        config.num_heads = 2
        config.attention_embed_dim = 64
        config.ffn_embed_dim = 512
        config.ffn_activation_name = "gelu"
        config.logits_from_embed = False
    else:
        raise ValueError(f"Unknown model name: {model_name}")
    return config
