from collections import defaultdict
from typing import Callable, Dict, Tuple

import haiku as hk
import torch
from jax import numpy as jnp

from trix.models.hyena_dna.model import HyenaDNAConfig, build_hyena_dna_forward
from trix.pretrained.hyena_dna_utils.character_tokenizer import CharacterTokenizer
from trix.pretrained.hyena_dna_utils.hugging_face import get_hyena_dna_from_hf
from trix.tokenizers.language_models.hyena_dna_tokenizer import HyenaDNATokenizer
from trix.utils.constants.bio import EXTRA_NUCLEOTIDES, NUCLEOTIDES


def convert_torch_hyena_block_params_to_jax(
    torch_block_params: torch.nn.ParameterDict, prefix: str
) -> hk.Params:
    """
    Converts a torch parameter dictionary that defines a torch hyena block into haiku
    params that define a jax hyena block.

    Args:
        torch_block_params: Torch parameter dictionary defining a torch hyena block.
        prefix: Name to use as the parent module for the hyena block.

    Returns:
        A haiku parameter dictionary defining the jax hyena block which has
        equivalent parameters to torch parameter dictionary pass as input.
    """
    param_dict: Dict = defaultdict(lambda: {})

    # convert hyena operator mixer
    param_dict[f"{prefix}/~/hyena_operator/~/linear"]["w"] = jnp.array(
        torch_block_params["mixer.in_proj.weight"].detach().numpy().T
    )
    param_dict[f"{prefix}/~/hyena_operator/~/linear"]["b"] = jnp.array(
        torch_block_params["mixer.in_proj.bias"].detach().numpy()
    )

    param_dict[f"{prefix}/~/hyena_operator/~/linear_1"]["w"] = jnp.array(
        torch_block_params["mixer.out_proj.weight"].detach().numpy().T
    )
    param_dict[f"{prefix}/~/hyena_operator/~/linear_1"]["b"] = jnp.array(
        torch_block_params["mixer.out_proj.bias"].detach().numpy().T
    )

    param_dict[f"{prefix}/~/hyena_operator/~/conv1_d"]["w"] = jnp.array(
        torch_block_params["mixer.short_filter.weight"].detach().numpy().T
    )
    param_dict[f"{prefix}/~/hyena_operator/~/conv1_d"]["b"] = jnp.array(
        torch_block_params["mixer.short_filter.bias"].detach().numpy()
    )

    num_implicit_filters = len(
        [
            key
            for key, value in torch_block_params.items()
            if "mixer.filter_fn.implicit_filter" in key and "weight" in key
        ]
    )

    for i in range(num_implicit_filters):
        if i == 0:
            param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter/~/linear"][
                "w"
            ] = jnp.array(
                torch_block_params[
                    f"mixer.filter_fn.implicit_filter." f"{str(i)}.weight"
                ]
                .detach()
                .numpy()
                .T
            )
            param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter/~/linear"][
                "b"
            ] = jnp.array(
                torch_block_params[f"mixer.filter_fn.implicit_filter." f"{str(i)}.bias"]
                .detach()
                .numpy()
            )

        else:
            param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter/~/linear_{i}"][
                "w"
            ] = jnp.array(
                torch_block_params[
                    f"mixer.filter_fn.implicit_filter." f"{str(i * 2)}.weight"
                ]
                .detach()
                .numpy()
                .T
            )

            if (
                f"mixer.filter_fn.implicit_filter.{str(i * 2)}.bias"
                in torch_block_params
            ):
                param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter/~/linear_{i}"][
                    "b"
                ] = jnp.array(
                    torch_block_params[
                        f"mixer.filter_fn.implicit_filter." f"{str(i * 2)}.bias"
                    ]
                    .detach()
                    .numpy()
                )

        if i < num_implicit_filters - 1:
            param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter"][
                f"freq_{i}"
            ] = jnp.array(
                torch_block_params[
                    f"mixer.filter_fn.implicit_filter" f".{str(i * 2 + 1)}.freq"
                ]
                .detach()
                .numpy()
            )

    # hyena operator bias
    param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter"]["bias"] = jnp.array(
        torch_block_params["mixer.filter_fn.bias"].detach().numpy()
    )

    # hyena operator positional embedding
    param_dict[f"{prefix}/~/hyena_operator/~/hyena_filter/~/positional_embedding"][
        "z"
    ] = jnp.array(torch_block_params["mixer.filter_fn.pos_emb.z"].detach().numpy())

    # layer norms
    param_dict[f"{prefix}/~/layer_norm"]["scale"] = jnp.array(
        torch_block_params["norm1.weight"].detach().numpy()
    )
    param_dict[f"{prefix}/~/layer_norm"]["offset"] = jnp.array(
        torch_block_params["norm1.bias"].detach().numpy()
    )

    param_dict[f"{prefix}/~/layer_norm_1"]["scale"] = jnp.array(
        torch_block_params["norm2.weight"].detach().numpy()
    )
    param_dict[f"{prefix}/~/layer_norm_1"]["offset"] = jnp.array(
        torch_block_params["norm2.bias"].detach().numpy()
    )

    # mlp
    param_dict[f"{prefix}/~/mlp/~/linear"]["w"] = jnp.array(
        torch_block_params["mlp.fc1.weight"].detach().numpy().T
    )
    param_dict[f"{prefix}/~/mlp/~/linear"]["b"] = jnp.array(
        torch_block_params["mlp.fc1.bias"].detach().numpy()
    )

    param_dict[f"{prefix}/~/mlp/~/linear_1"]["w"] = jnp.array(
        torch_block_params["mlp.fc2.weight"].detach().numpy().T
    )
    param_dict[f"{prefix}/~/mlp/~/linear_1"]["b"] = jnp.array(
        torch_block_params["mlp.fc2.bias"].detach().numpy()
    )

    return param_dict


def convert_torch_hyena_dna_params_to_jax(
    torch_params: torch.nn.ParameterDict, model_name: str
) -> hk.Params:
    """
    Converts parameters for a torch implemented hyena dna model to equivalent haiku
    parameters.

    Args:
        torch_params: A torch parameter dictionary.
        model_name: Highest level module name in haiku defined hyena dna model.

    Returns:
        A haiku parameter dictionary defining a hyena dna model.

    """
    param_dict: Dict = defaultdict(lambda: {})

    # get token embedding weights
    param_dict[f"{model_name}/~/lm_backbone/~/token_embeddings/~/embed"][
        "embeddings"
    ] = jnp.array(
        torch_params["backbone.embeddings.word_embeddings.weight"].detach().numpy()
    )

    # get inner block weights
    num_blocks = len(
        {
            int(key.split(".")[2])
            for key in torch_params.keys()
            if "backbone.layers" in key
        }
    )

    for i in range(num_blocks):

        block = {
            ".".join(key.split(".")[3:]): value
            for key, value in torch_params.items()
            if f"backbone.layers.{str(i)}" in key
        }

        if i == 0:
            prefix = model_name + "/~/lm_backbone/~/block"
        else:
            prefix = model_name + f"/~/lm_backbone/~/block_{i}"

        block_params = convert_torch_hyena_block_params_to_jax(block, prefix)
        param_dict.update(block_params)

    # get final norm weights
    param_dict[f"{model_name}/~/lm_backbone/~/layer_norm"]["scale"] = jnp.array(
        torch_params["backbone.ln_f.weight"].detach().numpy()
    )
    param_dict[f"{model_name}/~/lm_backbone/~/layer_norm"]["offset"] = jnp.array(
        torch_params["backbone.ln_f.bias"].detach().numpy()
    )

    param_dict[f"{model_name}/~/linear"]["w"] = jnp.array(
        torch_params["lm_head.weight"].detach().numpy().T
    )

    return param_dict


def convert_torch_config_to_jax(config: Dict) -> HyenaDNAConfig:
    """
    Converts the config from a pretrained hyena dna model from hugging face into a
    HyenaDNAConfig dataclass.

    Args:
        config: Pretrained model config returned by HyenaDNAPretrainedModel.

    Returns:
        A HyenaDNAConfig dataclass with equivalent attributes.
    """

    jax_config = HyenaDNAConfig(
        vocab_size=config["vocab_size"],
        pad_vocab_size_multiple=config["pad_vocab_size_multiple"],
        model_embed_dim=config["d_model"],
        mlp_dim=config["d_inner"],
        num_hyena_blocks=config["n_layer"],
        embed_dropout=config["embed_dropout"],
        resid_dropout=config["resid_dropout"],
        hyena_operator_filter_positional_embed_dim=config["layer"]["emb_dim"],
        hyena_operator_max_seq_len=config["layer"]["l_max"],
        hyena_operator_implicit_filter_mlp_dim=config["layer"]["filter_order"],
    )

    return jax_config


def get_pretrained_hyena_tokenizer(max_length: int) -> HyenaDNATokenizer:

    """
    Gets a tokenizer that is equivalent to the tokenizer used in the pretraining of
    the Hyena DNA models.
    """

    # instantiates a CharacterTokenizer in the way that was documented in the hyena dna
    # repo. See : https://github.com/HazyResearch/hyena-dna/blob/
    # db7cc5fb0d063a584692136374b48fb72f077fe8/huggingface.py#L224
    character_tokenizer = CharacterTokenizer(
        characters=["A", "C", "G", "T", "N"],  # add DNA characters, N is uncertain
        model_max_length=max_length + 2,  # to account for special tokens, like EOS
        add_special_tokens=False,  # we handle special tokens elsewhere
        padding_side="left",  # since HyenaDNA is causal, we pad on the left
    )

    int_to_token_mapping = character_tokenizer._vocab_int_to_str

    # adjust the tokens to match the ones used as default in the FixStandardTokenizer
    int_to_token_mapping[0] = "<cls>"
    int_to_token_mapping[1] = "<sep>"
    int_to_token_mapping[2] = "<bos>"
    int_to_token_mapping[3] = "<mask>"
    int_to_token_mapping[4] = "<pad>"
    int_to_token_mapping[5] = "<reserved>"
    int_to_token_mapping[6] = "<unk>"

    int_to_token_mapping[12] = "<eos>"

    # reverse mapping
    token_to_int_mapping = {v: k for k, v in int_to_token_mapping.items()}

    # instantiate an HyenaDNATokenizer that is equivalent to the CharacterTokenizer
    # defined above
    trix_tokenizer = HyenaDNATokenizer(
        standard_tokens=NUCLEOTIDES + EXTRA_NUCLEOTIDES,
        fixed_length=max_length,
        prepend_cls_token=True,
        append_sep_token=True,
        padding_side="left",
        extra_special_tokens=["<reserved>"],
        tokens_to_ids=token_to_int_mapping,
    )

    return trix_tokenizer


def get_pretrained_hyena_dna(
    model_name: str,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    use_gradient_checkpointing_mixer: bool = False,
    use_gradient_checkpointing_mlp: bool = False,
) -> Tuple[hk.Params, Callable, HyenaDNATokenizer, HyenaDNAConfig]:
    """
    Gets a pretrained hyena dna model by downloading the model checkpoint/config from
    the associated hugging face repo and converting the torch implemented model to the
    jax implemented model.


    Args:
        model_name: Name of the pretrained hyena dna model to fetch.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        use_gradient_checkpointing_mixer: enable grad checkpointing for the mixer block
        use_gradient_checkpointing_mlp: enable grad checkpointing for the mlp block

    Returns:
        Dictionary of the pretrained hyena dna parameters.
        Haiku function to call the model.
        Model config.
        Tokenizer.
    """

    # supported hyena dna models
    hyena_models = [
        "hyenadna-tiny-16k-seqlen-d128",
        "hyenadna-medium-160k-seqlen",
        "hyenadna-small-32k-seqlen",
        "hyenadna-tiny-1k-seqlen-d256",
        "hyenadna-large-1m-seqlen",
        "hyenadna-medium-450k-seqlen",
        "hyenadna-tiny-1k-seqlen",
    ]

    if model_name not in hyena_models:
        raise ValueError(
            f"{model_name} not supported! Only the following hyena dna "
            f"models are supported: {hyena_models}"
        )

    torch_params, torch_config = get_hyena_dna_from_hf(model_name=model_name)

    hyena_jax_params = convert_torch_hyena_dna_params_to_jax(
        torch_params=torch_params, model_name="hyena_dna"
    )

    hyena_jax_config = convert_torch_config_to_jax(config=torch_config)

    hyena_tokenizer = get_pretrained_hyena_tokenizer(
        max_length=hyena_jax_config.hyena_operator_max_seq_len
    )

    hyena_dna_forward = build_hyena_dna_forward(
        hyena_jax_config,
        model_name="hyena_dna",
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        use_gradient_checkpointing_mixer=use_gradient_checkpointing_mixer,
        use_gradient_checkpointing_mlp=use_gradient_checkpointing_mlp,
    )

    return hyena_jax_params, hyena_dna_forward, hyena_tokenizer, hyena_jax_config
