"""Implementation of utilities to load a pretrained gptj model in Trix."""
import gc
from collections import defaultdict
from typing import Callable, Dict, Tuple

import haiku as hk
import jax.numpy as jnp
import numpy as np
import torch
from transformers import AutoConfig, AutoModelForCausalLM, AutoTokenizer, logging

from trix.models.gpt.model import GptConfig
from trix.models.rita.model import build_rita_fn

# In order to disable hugging face transformer module's warning.
logging.set_verbosity_error()

__all__ = [
    "build_rita_fn",
    "RITA_MODEL_NAME",
    "translate_torch_params",
    "get_pretrained_rita_model",
]

RITA_MODEL_NAME = "rita_decoder"


def translate_torch_params(
    torch_params: torch.nn.ParameterDict, num_layers: int
) -> Dict[str, Dict[str, np.ndarray]]:
    """
    Converts the full Hugging Face PyTorch RITA model to Haiku parameters.

    Note that the parameter names must match the defaults defined in the Trix RITA/GPT
    architecture (refer to rita_model.py). Other choices may cause errors. See here for
    parameters of the PyTorch RITA:
    https://huggingface.co/lightonai/RITA_s/blob/fced662eadd2b7099a3b92a88365dfc3c98eb3da/rita_modeling.py#L212

    Args:
        torch_params: Pretrained PyTorch RITA model state_dict().
        num_layers: Number of decoder layers in the desired pre-trained model.

    Returns:
       Dictionary of Haiku parameters.
    """
    translate_dict = {}
    rita_prefix = f"{RITA_MODEL_NAME}/"

    translate_dict["transformer.embedding.weight"] = (
        rita_prefix + "~/token_embed",
        "embeddings",
    )

    for k in range(num_layers):
        prefix_layer = rita_prefix + f"gpt_decoder_layer_{k}/~/"
        torch_prefix = f"transformer.layers.{k}."

        # Attention K, Q, V projections.
        translate_dict[torch_prefix + "self_attention.key.weight"] = (
            prefix_layer + "self_attn/~/key_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attention.key.bias"] = (
            prefix_layer + "self_attn/~/key_linear",
            "b",
        )
        translate_dict[torch_prefix + "self_attention.value.weight"] = (
            prefix_layer + "self_attn/~/value_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attention.value.bias"] = (
            prefix_layer + "self_attn/~/value_linear",
            "b",
        )
        translate_dict[torch_prefix + "self_attention.query.weight"] = (
            prefix_layer + "self_attn/~/query_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attention.query.bias"] = (
            prefix_layer + "self_attn/~/query_linear",
            "b",
        )
        translate_dict[torch_prefix + "self_attention.proj.weight"] = (
            prefix_layer + "self_attn/~/out_linear",
            "w",
        )
        translate_dict[torch_prefix + "self_attention.proj.bias"] = (
            prefix_layer + "self_attn/~/out_linear",
            "b",
        )

        # Attention layer norm.
        translate_dict[torch_prefix + "attn_norm.weight"] = (
            prefix_layer + "attn_layer_norm",
            "scale",
        )
        translate_dict[torch_prefix + "attn_norm.bias"] = (
            prefix_layer + "attn_layer_norm",
            "offset",
        )

        # Note: RITA models include a "self_attention.rotary_embedding.inv_freq" key,
        # but the weights for the inv_freq are all constant.

        # MLP dense layers.
        translate_dict[torch_prefix + "mlp.0.weight"] = (
            prefix_layer + "fc1_linear",
            "w",
        )
        translate_dict[torch_prefix + "mlp.0.bias"] = (
            prefix_layer + "fc1_linear",
            "b",
        )
        translate_dict[torch_prefix + "mlp.2.weight"] = (
            prefix_layer + "fc2_linear",
            "w",
        )
        translate_dict[torch_prefix + "mlp.2.bias"] = (
            prefix_layer + "fc2_linear",
            "b",
        )

        # MLP layer norm.
        translate_dict[torch_prefix + "mlp_norm.weight"] = (
            prefix_layer + "ffn_layer_norm",
            "scale",
        )
        translate_dict[torch_prefix + "mlp_norm.bias"] = (
            prefix_layer + "ffn_layer_norm",
            "offset",
        )

    translate_dict["transformer.final_norm.weight"] = (
        rita_prefix + "~/final_layer_norm",
        "scale",
    )
    translate_dict["transformer.final_norm.bias"] = (
        rita_prefix + "~/final_layer_norm",
        "offset",
    )
    translate_dict["lm_head.weight"] = (
        rita_prefix + "~/simple_lm_head/~/lm_final_fc",
        "w",
    )

    params: Dict[str, Dict[str, np.ndarray]] = defaultdict(dict)
    for torch_key, (trix_key, weight_key) in translate_dict.items():
        if "weight" in torch_key and not ("embedding" in torch_key):
            # in pytorch, the weights of dense matrices indexation is transposes
            # compared to haiku, except for word-token-embedding
            params[trix_key][weight_key] = np.array(torch_params[torch_key]).transpose()
        else:
            params[trix_key][weight_key] = np.array(torch_params[torch_key])

    return dict(params)


def get_pretrained_rita_model(
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: str = "RITA_s",
) -> Tuple[hk.Params, Callable, AutoTokenizer, GptConfig]:
    """
    Create a Haiku RITA model by downloading the pytorch weights hosted by HuggingFace
    and translating them.

    The model returned is RITADecoder from rita_model.py, which is identical to
    GptDecoder except for the implementation of RoPE.

    Args:
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. It determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.
        model_name: RITA model name; can be one of ["RITA_s", "RITA_m",
            "RITA_l", "RITA_xl"]

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config.
    """

    rita_models = [
        "RITA_s",
        "RITA_m",
        "RITA_l",
        "RITA_xl",
    ]
    assert (
        model_name in rita_models
    ), f"Unrecognized RITA model `{model_name}`. It must be one of {rita_models}"

    huggingface_name = f"lightonai/{model_name}"
    rita_config = AutoConfig.from_pretrained(
        huggingface_name,
        trust_remote_code=True,
    )
    rita_params = AutoModelForCausalLM.from_pretrained(
        huggingface_name,
        trust_remote_code=True,
    ).state_dict()

    tokenizer = AutoTokenizer.from_pretrained(huggingface_name)
    tokenizer.add_special_tokens({"eos_token": "<EOS>", "pad_token": "<EOS>"})
    assert tokenizer.eos_token == tokenizer.pad_token == "<EOS>"
    assert tokenizer.eos_token_id == tokenizer.pad_token_id == 2

    config = GptConfig(
        vocab_size=rita_config.vocab_size,
        eos_token_id=rita_config.eos_token_id,
        embed_dim=rita_config.d_model,
        ffn_embed_dim=rita_config.d_feedforward,
        num_heads=rita_config.num_heads,
        num_layers=rita_config.num_layers,
        # RITA sets the ROPE dim to be the size of each transformer head; see `head_dim`
        # here:
        # https://huggingface.co/lightonai/RITA_s/blob/fced662eadd2b7099a3b92a88365dfc3c98eb3da/rita_modeling.py#L48
        rope_dimensions=rita_config.d_model // rita_config.num_heads,
        max_position_embeddings=rita_config.max_seq_len,
        add_bias_ffn=True,
        # RITA uses the approximate GELU (i.e., the default in JAX and Trix):
        # https://huggingface.co/lightonai/RITA_s/blob/main/rita_modeling.py#L25
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=False,
        norm_type="layer_norm",
        # The decoder layer indeed appears to not follow the parallel attention
        # implementation from GPT-NeoX:
        # https://huggingface.co/lightonai/RITA_s/blob/fced662eadd2b7099a3b92a88365dfc3c98eb3da/rita_modeling.py#L197
        parallel_attention_ff=False,
        use_gradient_checkpointing=False,
        # Unlike the standard GPT model, RITA uses bias in its attention mechanism.
        add_bias_attn=True,
    )

    parameters = translate_torch_params(rita_params, config.num_layers)
    # Remove the torch parameters from the RAM.
    del rita_params
    gc.collect()
    torch.cuda.empty_cache()

    rita_fn = build_rita_fn(
        config=config,
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        name=RITA_MODEL_NAME,
    )
    return parameters, rita_fn, tokenizer, config
