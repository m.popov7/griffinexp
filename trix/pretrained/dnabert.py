"""Implementation of utilities to load a pretrained dnabert model in Trix."""
import gc
from collections import defaultdict
from functools import partial
from typing import Callable, Dict, List, Tuple

import haiku as hk
import jax.numpy as jnp
import numpy as np
import torch
from transformers import AutoModel, AutoTokenizer, logging

from trix.models.esm.model import ESMTransformerConfig, build_esm_fn

# in order to disable hugging face transformer module's warning
logging.set_verbosity_error()


def translate_torch_params_dnabert2(
    torch_params: torch.nn.ParameterDict,
    num_layers: int,
    model_name: str,
) -> hk.Params:
    """
    Converts the full Hugging Face PyTorch DNAbert model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix ESM
    architecture.  Other choices may cause errors.

    Args:
        torch_params: Pretrained PyTorch DNAbert model state_dict().
        num_layers: Number of decoder layers in the desired pre-trained model.
        model_name: model name in trix

    Returns:
       Dictionary of Haiku parameters.
    """
    translate_dict = {}
    prefix = f"{model_name}/"

    translate_dict["embeddings.word_embeddings.weight"] = (
        prefix + "~/embed",
        "embeddings",
    )

    translate_dict["embeddings.token_type_embeddings.weight"] = (
        prefix + "~/bias_word_embedding",
        "b",
    )

    translate_dict["embeddings.LayerNorm.weight"] = (
        prefix + "~/emb_layer_norm_before",
        "scale",
    )
    translate_dict["embeddings.LayerNorm.bias"] = (
        prefix + "~/emb_layer_norm_before",
        "offset",
    )

    for k in range(num_layers):
        prefix_layer = prefix + f"attention_layer_{k}/~/"
        torch_prefix = f"encoder.layer.{k}."

        # Layer norm
        translate_dict[torch_prefix + "attention.output.LayerNorm.weight"] = (
            prefix_layer + "self_attention_layer_norm",
            "scale",
        )
        translate_dict[torch_prefix + "attention.output.LayerNorm.bias"] = (
            prefix_layer + "self_attention_layer_norm",
            "offset",
        )
        # attention K, Q, V projections
        translate_dict[torch_prefix + "attention.self.Wqkv.weight"] = (
            prefix_layer + "self_attention/query_key_value",
            "w",
        )
        translate_dict[torch_prefix + "attention.self.Wqkv.bias"] = (
            prefix_layer + "self_attention/query_key_value",
            "b",
        )

        # Attention output
        translate_dict[torch_prefix + "attention.output.dense.weight"] = (
            prefix_layer + "self_attention/mha_output",
            "w",
        )
        translate_dict[torch_prefix + "attention.output.dense.bias"] = (
            prefix_layer + "self_attention/mha_output",
            "b",
        )

        # MLP dense layers
        translate_dict[torch_prefix + "mlp.gated_layers.weight"] = (
            prefix_layer + "fc1",
            "w",
        )

        translate_dict[torch_prefix + "mlp.wo.weight"] = (
            prefix_layer + "fc2",
            "w",
        )
        translate_dict[torch_prefix + "mlp.wo.bias"] = (
            prefix_layer + "fc2",
            "b",
        )

        translate_dict[torch_prefix + "mlp.layernorm.weight"] = (
            prefix_layer + "final_layer_norm",
            "scale",
        )
        translate_dict[torch_prefix + "mlp.layernorm.bias"] = (
            prefix_layer + "final_layer_norm",
            "offset",
        )

    params: Dict[str, Dict[str, np.ndarray]] = defaultdict(dict)
    for torch_key, (trix_key, weight_key) in translate_dict.items():
        if "Wqkv.weight" in torch_key:
            split_query_key_value_params = np.array(torch_params.pop(torch_key))

            split_query_key_value_params = np.split(
                split_query_key_value_params,
                indices_or_sections=3,
                axis=0,
            )

            params[trix_key.replace("query_key_value", "query")][
                weight_key
            ] = split_query_key_value_params[0].transpose(1, 0)
            params[trix_key.replace("query_key_value", "key")][
                weight_key
            ] = split_query_key_value_params[1].transpose(1, 0)
            params[trix_key.replace("query_key_value", "value")][
                weight_key
            ] = split_query_key_value_params[2].transpose(1, 0)

        elif "Wqkv.bias" in torch_key:
            split_query_key_value_params = np.array(torch_params.pop(torch_key))

            split_query_key_value_params = np.split(
                split_query_key_value_params,
                indices_or_sections=3,
                axis=0,
            )
            params[trix_key.replace("query_key_value", "query")][
                weight_key
            ] = split_query_key_value_params[0]
            params[trix_key.replace("query_key_value", "key")][
                weight_key
            ] = split_query_key_value_params[1]
            params[trix_key.replace("query_key_value", "value")][
                weight_key
            ] = split_query_key_value_params[2]

        elif (
            "weight" in torch_key
            and not ("word_embeddings" in torch_key)
            and not ("position_embeddings" in torch_key)
            and not ("token_type_embeddings" in torch_key)
        ):
            # in pytorch, the weights of dense matrices indexation is transposes
            # compared to haiku, except for word-token-embedding
            params[trix_key][weight_key] = np.array(
                torch_params.pop(torch_key)
            ).transpose()
        else:
            if "token_type_embeddings" in torch_key:
                params[trix_key][weight_key] = np.array(torch_params.pop(torch_key))[0]
            else:
                params[trix_key][weight_key] = np.array(torch_params.pop(torch_key))

    for k in range(num_layers):
        prefix_layer = prefix + f"attention_layer_{k}/~/"
        params[prefix_layer + "fc1"]["b"] = np.zeros(6144)

    return dict(params)


def translate_torch_params_dnabert(
    torch_params: torch.nn.ParameterDict, num_layers: int, model_name: str
) -> hk.Params:
    """
    Converts the full Hugging Face PyTorch DNAbert model to Haiku parameters.
    Note that the parameters names match the defaults defined in the Trix ESM
    architecture.  Other choices may cause errors.

    Args:
        torch_params: Pretrained PyTorch DNAbert model state_dict().
        num_layers: Number of decoder layers in the desired pre-trained model.
        model_name: model name in trix

    Returns:
       Dictionary of Haiku parameters.
    """

    translate_dict = {}
    prefix = f"{model_name}/"

    translate_dict["embeddings.word_embeddings.weight"] = (
        prefix + "~/embed",
        "embeddings",
    )

    translate_dict["embeddings.token_type_embeddings.weight"] = (
        prefix + "~/bias_word_embedding",
        "b",
    )
    translate_dict["embeddings.position_embeddings.weight"] = (
        prefix + "~/learned_positional_embeddings/~/embed",
        "embeddings",
    )

    translate_dict["embeddings.LayerNorm.weight"] = (
        prefix + "~/emb_layer_norm_before",
        "scale",
    )
    translate_dict["embeddings.LayerNorm.bias"] = (
        prefix + "~/emb_layer_norm_before",
        "offset",
    )

    for k in range(num_layers):
        prefix_layer = prefix + f"attention_layer_{k}/~/"
        torch_prefix = f"encoder.layer.{k}."

        # Layer norm
        translate_dict[torch_prefix + "attention.output.LayerNorm.weight"] = (
            prefix_layer + "self_attention_layer_norm",
            "scale",
        )
        translate_dict[torch_prefix + "attention.output.LayerNorm.bias"] = (
            prefix_layer + "self_attention_layer_norm",
            "offset",
        )
        # attention K, Q, V projections
        translate_dict[torch_prefix + "attention.self.key.weight"] = (
            prefix_layer + "self_attention/key",
            "w",
        )
        translate_dict[torch_prefix + "attention.self.key.bias"] = (
            prefix_layer + "self_attention/key",
            "b",
        )

        translate_dict[torch_prefix + "attention.self.query.weight"] = (
            prefix_layer + "self_attention/query",
            "w",
        )
        translate_dict[torch_prefix + "attention.self.query.bias"] = (
            prefix_layer + "self_attention/query",
            "b",
        )

        translate_dict[torch_prefix + "attention.self.value.weight"] = (
            prefix_layer + "self_attention/value",
            "w",
        )
        translate_dict[torch_prefix + "attention.self.value.bias"] = (
            prefix_layer + "self_attention/value",
            "b",
        )

        translate_dict[torch_prefix + "attention.output.dense.weight"] = (
            prefix_layer + "self_attention/mha_output",
            "w",
        )
        translate_dict[torch_prefix + "attention.output.dense.bias"] = (
            prefix_layer + "self_attention/mha_output",
            "b",
        )

        # MLP dense layers
        translate_dict[torch_prefix + "intermediate.dense.weight"] = (
            prefix_layer + "fc1",
            "w",
        )
        translate_dict[torch_prefix + "intermediate.dense.bias"] = (
            prefix_layer + "fc1",
            "b",
        )

        translate_dict[torch_prefix + "output.dense.weight"] = (
            prefix_layer + "fc2",
            "w",
        )
        translate_dict[torch_prefix + "output.dense.bias"] = (
            prefix_layer + "fc2",
            "b",
        )

        translate_dict[torch_prefix + "output.LayerNorm.weight"] = (
            prefix_layer + "final_layer_norm",
            "scale",
        )
        translate_dict[torch_prefix + "output.LayerNorm.bias"] = (
            prefix_layer + "final_layer_norm",
            "offset",
        )

    params: Dict[str, Dict[str, np.ndarray]] = defaultdict(dict)
    for torch_key, (trix_key, weight_key) in translate_dict.items():
        if (
            "weight" in torch_key
            and not ("word_embeddings" in torch_key)
            and not ("position_embeddings" in torch_key)
            and not ("token_type_embeddings" in torch_key)
        ):
            # in pytorch, the weights of dense matrices indexation is transposes
            # compared to haiku, except for word-token-embedding
            params[trix_key][weight_key] = np.array(
                torch_params.pop(torch_key)
            ).transpose()
        else:
            if "token_type_embeddings" in torch_key:
                params[trix_key][weight_key] = np.array(torch_params.pop(torch_key))[0]
            else:
                params[trix_key][weight_key] = np.array(torch_params.pop(torch_key))

    return dict(params)


def get_pretrained_dnabert(
    model_name: str = "dnabert_2",
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
) -> Tuple[hk.Params, Callable, AutoTokenizer, ESMTransformerConfig]:
    """
    Create a Haiku DNAbert model by downloading the pytorch weights hosted by
    huggingface and translating them.

    Args:
        model_name: dnabert model names available: {"dnabert_1", "dnabert_2"}
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config
    """

    if model_name == "dnabert_1":
        huggingfacehub_model_name = "DNA_bert_6"
        revision = "55e0c0eb7b734c8b9b77bc083bf89eb6fbda1341"

    elif model_name == "dnabert_2":
        huggingfacehub_model_name = "DNABERT-2-117M"
        revision = "25abaf0bd247444fcfa837109f12088114898d98"
    else:
        raise ValueError(f"wrong model name : {model_name}")

    huggingface_model = AutoModel.from_pretrained(
        f"zhihan1996/{huggingfacehub_model_name}",
        trust_remote_code=True,
        revision=revision,
    )
    model_params = huggingface_model.state_dict()

    tokenizer = AutoTokenizer.from_pretrained(
        f"zhihan1996/{huggingfacehub_model_name}",
        trust_remote_code=True,
        revision=revision,
    )

    if model_name == "dnabert_1":

        def tokenize(self: AutoTokenizer, sequence: str) -> Tuple[List[str], List[int]]:
            assert not ("N" in sequence)
            assert len(sequence) >= 6
            k = 6
            kmers_tokens_list = " ".join(
                [sequence[i : i + k] for i in range(len(sequence) - k + 1)]
            )
            tokens_ids = self(kmers_tokens_list, return_tensors="np")["input_ids"][0]
            tokens = self.convert_ids_to_tokens(tokens_ids)

            return tokens, tokens_ids

        tokenizer.tokenize = partial(tokenize, tokenizer)

        def batch_tokenize(
            self: AutoTokenizer, sequence_list: List[str]
        ) -> List[Tuple[List[str], List[int]]]:
            assert not ("N" in "".join(sequence_list))
            for sequence in sequence_list:
                assert len(sequence) >= 6

            k = 6
            kmers_tokens_list = [
                " ".join([seq[i : i + k] for i in range(len(seq) - k + 1)])
                for seq in sequence_list
            ]
            tokens_ids = self(kmers_tokens_list, return_tensors="np", padding=True)[
                "input_ids"
            ]

            return [
                (tokenizer.convert_ids_to_tokens(tokens_id), tokens_id)
                for tokens_id in tokens_ids
            ]

        tokenizer.batch_tokenize = partial(batch_tokenize, tokenizer)

        config = ESMTransformerConfig(
            pad_token_id=tokenizer.pad_token_id,
            mask_token_id=tokenizer.mask_token_id,
            alphabet_size=4101,
            embed_dim=768,
            ffn_embed_dim=3072,
            attention_heads=12,
            num_layers=12,
            max_positions=512,
            layer_norm_eps=1e-12,
            ffn_activation_name="gelu-no-approx",
            add_bias_ffn=True,
            add_bias_kv=False,
            positional_embedding="learned_standard",
            pre_layer_norm=False,
            emb_layer_norm_before=True,
            lm_head=None,
            embeddings_layers_to_save=(12,),
            bias_word_embedding=True,
        )
        parameters = translate_torch_params_dnabert(
            model_params, config.num_layers, model_name
        )

    elif model_name == "dnabert_2":

        def tokenize(self: AutoTokenizer, sequence: str) -> Tuple[List[str], List[int]]:
            assert not ("N" in sequence)

            tokens_ids = self(sequence, return_tensors="np")["input_ids"][0]
            tokens = self.convert_ids_to_tokens(tokens_ids)

            return tokens, tokens_ids

        tokenizer.tokenize = partial(tokenize, tokenizer)

        def batch_tokenize(
            self: AutoTokenizer, sequence_list: List[str]
        ) -> List[Tuple[List[str], List[int]]]:
            assert not ("N" in "".join(sequence_list))

            tokens_ids = self(sequence_list, return_tensors="np", padding=True)[
                "input_ids"
            ]

            return [
                (tokenizer.convert_ids_to_tokens(tokens_id), tokens_id)
                for tokens_id in tokens_ids
            ]

        tokenizer.batch_tokenize = partial(batch_tokenize, tokenizer)

        config = ESMTransformerConfig(
            pad_token_id=tokenizer.pad_token_id,
            mask_token_id=tokenizer.mask_token_id,
            alphabet_size=4096,
            embed_dim=768,
            ffn_embed_dim=3072,
            attention_heads=12,
            num_layers=12,
            max_positions=512,
            layer_norm_eps=1e-12,
            ffn_activation_name="gelu-no-approx",
            add_bias_ffn=True,
            add_bias_kv=False,
            positional_embedding="alibi_dnabert_2",
            pre_layer_norm=False,
            emb_layer_norm_before=True,
            lm_head=None,
            embeddings_layers_to_save=(12,),
            bias_word_embedding=True,
            use_glu_in_ffn=True,
        )
        parameters = translate_torch_params_dnabert2(
            model_params, config.num_layers, model_name
        )

    forward_fn = build_esm_fn(
        model_config=config,
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        model_name=model_name,
    )

    # remove the torch parameters from the RAM
    del model_params
    gc.collect()
    torch.cuda.empty_cache()

    return parameters, forward_fn, tokenizer, config


def get_fixed_size_dnabert_tokenizer(
    model_name: str, fixed_padding_len: int
) -> AutoTokenizer:
    """
    Returns the fixed len tokenizer

    Args:
        model_name: dnabert model names available: {"dnabert_1", "dnabert_2"}
        fixed_padding_len: fixed padding length

    Returns:
        Tokenizer.
    """
    if model_name == "dnabert_1":
        huggingfacehub_model_name = "DNA_bert_6"

    elif model_name == "dnabert_2":
        huggingfacehub_model_name = "DNABERT-2-117M"
    else:
        raise ValueError(f"wrong model name : {model_name}")

    tokenizer = AutoTokenizer.from_pretrained(
        f"zhihan1996/{huggingfacehub_model_name}", trust_remote_code=True
    )

    if model_name == "dnabert_1":

        def tokenize(self: AutoTokenizer, sequence: str) -> Tuple[List[str], List[int]]:
            assert not ("N" in sequence)
            assert len(sequence) >= 6
            k_for_kmers = 6
            kmers_tokens_list = " ".join(
                [
                    sequence[i : i + k_for_kmers]
                    for i in range(len(sequence) - k_for_kmers + 1)
                ]
            )
            tokens_ids = self(
                kmers_tokens_list,
                return_tensors="np",
                max_length=fixed_padding_len,
                padding="max_length",
            )["input_ids"][0]
            tokens = self.convert_ids_to_tokens(tokens_ids)

            return tokens, tokens_ids

        tokenizer.tokenize = partial(tokenize, tokenizer)

        def batch_tokenize(
            self: AutoTokenizer, sequence_list: List[str]
        ) -> List[Tuple[List[str], List[int]]]:
            assert not ("N" in "".join(sequence_list))

            for sequence in sequence_list:
                assert len(sequence) >= 6

            k = 6
            kmers_tokens_list = [
                " ".join([seq[i : i + k] for i in range(len(seq) - k + 1)])
                for seq in sequence_list
            ]
            tokens_ids = self(
                kmers_tokens_list,
                return_tensors="np",
                max_length=fixed_padding_len,
                padding="max_length",
            )["input_ids"]
            assert tokens_ids.shape[1] <= fixed_padding_len
            return [
                (tokenizer.convert_ids_to_tokens(tokens_id), tokens_id)
                for tokens_id in tokens_ids
            ]

        tokenizer.batch_tokenize = partial(batch_tokenize, tokenizer)

    elif model_name == "dnabert_2":

        def tokenize(self: AutoTokenizer, sequence: str) -> Tuple[List[str], List[int]]:
            assert not ("N" in sequence)

            tokens_ids = self(sequence, return_tensors="np")["input_ids"][0]
            tokens = self.convert_ids_to_tokens(tokens_ids)

            return tokens, tokens_ids

        tokenizer.tokenize = partial(tokenize, tokenizer)

        def batch_tokenize(
            self: AutoTokenizer, sequence_list: List[str]
        ) -> List[Tuple[List[str], List[int]]]:
            assert not ("N" in "".join(sequence_list))

            tokens_ids = self(
                sequence_list,
                return_tensors="np",
                max_length=fixed_padding_len,
                padding="max_length",
            )["input_ids"]
            assert tokens_ids.shape[1] <= fixed_padding_len

            return [
                (tokenizer.convert_ids_to_tokens(tokens_id), tokens_id)
                for tokens_id in tokens_ids
            ]

        tokenizer.batch_tokenize = partial(batch_tokenize, tokenizer)

    return tokenizer
