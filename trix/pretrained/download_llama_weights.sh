#!/bin/bash
# UPDATE from Shawn (Mar 5 @ 2:43 AM)

echo "❤️ Resume download is supported. You can ctrl-c and rerun the program to resume the downloading"

PRESIGNED_URL="https://agi.gpt4.org/llama/LLaMA/*"

MODEL_SIZE=${1:-7B}  # edit this list with the model sizes you wish to download
TARGET_FOLDER=${2:-./}           # where all files should end up

if [ $TARGET_FOLDER != "./" ]; then
    mkdir -p $TARGET_FOLDER
fi

declare -A N_SHARD_DICT

N_SHARD_DICT["7B"]="0"
N_SHARD_DICT["13B"]="1"
N_SHARD_DICT["30B"]="3"
N_SHARD_DICT["65B"]="7"

#set -x # enable debug mode
echo "Downloading tokenizer..."
wget -q --progress=bar:force ${PRESIGNED_URL/'*'/"tokenizer.model"} -O ${TARGET_FOLDER}"/tokenizer.model"
echo ✅ ${TARGET_FOLDER}"/tokenizer.model"
wget -q --progress=bar:force ${PRESIGNED_URL/'*'/"tokenizer_checklist.chk"} -O ${TARGET_FOLDER}"/tokenizer_checklist.chk"
echo ✅ ${TARGET_FOLDER}"/tokenizer_checklist.chk"

(cd ${TARGET_FOLDER} && md5sum -c tokenizer_checklist.chk)
for i in ${MODEL_SIZE//,/ }
do
    echo "Downloading ${i} LLAMA model"
    mkdir -p ${TARGET_FOLDER}"/${i}"

    # Build the list of URLs using a for loop
    urls=()
    for s in $(seq -f "0%g" 0 ${N_SHARD_DICT[$i]}); do
      urls+=${PRESIGNED_URL/'*'/"${i}/consolidated.${s}.pth "}
    done
    echo "downloading $urls"
    aria2c --console-log-level=warn --max-connection-per-server=16 \
      --summary-interval=0 --dir=${TARGET_FOLDER}"/${i}/"  \
      --file-allocation=none -j8 -x2 -c -Z \
      $urls

    for s in $(seq -f "0%g" 0 ${N_SHARD_DICT[$i]}); do
      echo ✅ ${TARGET_FOLDER}"/${i}/consolidated.${s}.pth"
    done
    wget -q --progress=bar:force ${PRESIGNED_URL/'*'/"${i}/params.json"} -O ${TARGET_FOLDER}"/${i}/params.json"
    echo ✅ ${TARGET_FOLDER}"/${i}/params.json"
    wget -q --progress=bar:force ${PRESIGNED_URL/'*'/"${i}/checklist.chk"} -O ${TARGET_FOLDER}"/${i}/checklist.chk"
    echo ✅ ${TARGET_FOLDER}"/${i}/checklist.chk"
    echo "Checking checksums"
    (cd ${TARGET_FOLDER}"/${i}" && md5sum -c checklist.chk)
done
