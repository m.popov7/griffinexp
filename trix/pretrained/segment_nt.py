import json
import os
from typing import Any, Callable, Dict, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import joblib

from trix.models.esm.finetuning import build_esm_with_head_fn
from trix.models.segment_nt.layers import UNetHead
from trix.models.segment_nt.model import SegmentNTConfig
from trix.tokenizers.language_models.bio import FixedSizeNucleotidesKmersTokenizer
from trix.utils.s3 import download_from_s3_bucket, init_s3_trix_bucket

ENV_XDG_CACHE_HOME = "XDG_CACHE_HOME"
DEFAULT_CACHE_DIR = "~/.cache"


def _get_dir() -> str:
    """
    Get directory to save files on user machine.
    """
    return os.path.expanduser(
        os.path.join(
            os.getenv(ENV_XDG_CACHE_HOME, DEFAULT_CACHE_DIR), "trix", "segment_nt"
        )
    )


def download_ckpt_and_hyperparams(
    model_name: str, verbose: bool = True
) -> Tuple[hk.Params, Dict[str, Any]]:
    """
    Download checkpoint and hyperparams on kao datacenter.

    Args:
        model_name: Name of the model.
        verbose: Whether or not to print the progress bar during the downloads.

    Returns:
        Model parameters.
        Model hyperparameters dict.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    params_save_dir = os.path.join(save_dir, "ckpt.joblib")
    hyperparams_save_dir = os.path.join(save_dir, "hyperparams.json")

    if os.path.exists(hyperparams_save_dir) and os.path.exists(params_save_dir):
        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = joblib.load(f)

        return params, hyperparams

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        print(f"Downloading checkpoints/segment_nt/{model_name}/hyperparams.json")
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/segment_nt/{model_name}/hyperparams.json",
            filename=hyperparams_save_dir,
            verbose=verbose,
        )
        print(f"Downloading checkpoints/segment_nt/{model_name}/ckpt.joblib")
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=f"checkpoints/segment_nt/{model_name}/ckpt.joblib",
            filename=params_save_dir,
            verbose=verbose,
        )

        # Load locally
        with open(hyperparams_save_dir, "rb") as f:
            hyperparams = json.load(f)

        with open(params_save_dir, "rb") as f:
            params = joblib.load(f)

        return params, hyperparams


def download_test_inputs_file(model_name: str) -> Dict[str, Any]:
    """
    Download files to check inputs and inference outputs.
    The returned dictionary contains a sequence, the tokens corresponding
    to that sequence, the logits corresponding to that sequence as well as
    embeddings at several layers.

    Args:
        model_name: Name of the model.

    Returns:
        Dict containing inputs and outputs.

    """
    # Get directories
    save_dir = os.path.join(_get_dir(), model_name)

    test_file_save_dir = os.path.join(save_dir, "test_inputs.joblib")

    if os.path.exists(test_file_save_dir):
        # Load locally
        with open(test_file_save_dir, "rb") as f:
            test_inputs = joblib.load(f)

        return test_inputs  # type: ignore

    else:
        os.makedirs(save_dir, exist_ok=True)

        # This part assumes the user has already exported required env variables
        s3_client, bucket = init_s3_trix_bucket()

        bucket_file_path = f"checkpoints/segment_nt/{model_name}/"
        bucket_file_path += "test_inputs.joblib"
        print(f"Downloading {bucket_file_path}")
        download_from_s3_bucket(
            s3_client=s3_client,
            bucket=bucket,
            key=bucket_file_path,
            filename=test_file_save_dir,
        )

        # Load locally
        with open(test_file_save_dir, "rb") as f:
            test_inputs = joblib.load(f)

        return test_inputs  # type: ignore


def rename_modules_segment_nt(parameters: hk.Params, model_name: str) -> hk.Params:
    """
    Adjusts the names of the modules from checkpoints to Segment-NT.

    Args:
        parameters: Parameters loaded from .joblib archive.
        model_name: Name of the loaded model.

    Returns:
        Parameters with updated names.
    """
    for layer_name in list(parameters.keys()):
        new_name = layer_name.replace("dcnuc_v2_500M_multi_species", model_name)
        new_name = new_name.replace("u_net_head", model_name + "_1")

        parameters[new_name] = parameters.pop(layer_name)

    return parameters


def get_pretrained_segment_nt_model(
    model_name: str,
    rescaling_factor: Optional[float] = None,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    embeddings_layers_to_save: Tuple[int, ...] = (),
    attention_maps_to_save: Optional[Tuple[Tuple[int, int], ...]] = None,
    max_positions: int = 1024,
) -> Tuple[hk.Params, Callable, FixedSizeNucleotidesKmersTokenizer, SegmentNTConfig]:
    """
    Create a Haiku Segment-NT model by downloading pre-trained weights and
    hyperparameters. Segment NT models have ESM-like architectures with a UNet
    segmentation head on top.

    Args:
        model_name: Name of the model.
        rescaling_factor: The rescaling factor that is applied to the rotary embeddings
            module as described in https://arxiv.org/abs/2309.00071. This rescaling
            factor is to be specified only if inference is done on sequences longer
            than the training sequence length, i.e 30kb, because the rescaling
            factor used to train on 30kb is already used by default. If this is the
            case, the rescaling factor s should be s=L'/L with L' the inference length
            and L=2048.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        embeddings_layers_to_save: Intermediate embeddings to return in the output.
        attention_maps_to_save: Intermediate attention maps to return in the output.
        max_positions: Maximum length of a token (for padding).

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config (hyperparameters).

    Example:
        parameters, forward_fn, tokenizer, config = get_pretrained_segment_nt_model(
            model_name="segment_nt",
            # Get embedding at layers 5 and 20
            embeddings_layers_to_save=(5, 20,),
            # Get attention map number 4 at layer 1 and attention map number 14
            # at layer 12
            attention_maps_to_save=((1,4), (12, 14)),
            max_positions=128,
        )
    """
    if attention_maps_to_save is None:
        attention_maps_to_save = ()

    supported_models = [
        "segment_nt",
        "segment_nt_multi_species",
    ]

    if not (model_name in supported_models):
        raise NotImplementedError(
            f"Unknown {model_name} model. " f"Supported models are {supported_models}"
        )

    # Download weights and hyperparams
    parameters, hyperparams = download_ckpt_and_hyperparams(model_name)

    # Get tokenizer
    num_k_mers = hyperparams["k_for_kmers"]
    # This model is more recent so the tokenizer can be simply used as it is
    tokenizer = FixedSizeNucleotidesKmersTokenizer(
        k_mers=num_k_mers,
        fixed_length=max_positions,
        prepend_cls_token=True,
    )

    # Adapat hyperparameters from config
    if "key_dim" in hyperparams.keys():
        key_size = hyperparams["key_dim"]
    elif "key_size" in hyperparams.keys():
        key_size = hyperparams["key_size"]
    else:
        key_size = None

    alphabet_size = len(tokenizer.vocabulary)

    if "add_bias_ffn" in hyperparams.keys():
        add_bias_ffn = hyperparams["add_bias_ffn"]
    else:
        add_bias_ffn = True

    if "ffn_activation_name" in hyperparams.keys():
        ffn_activation_name = hyperparams["ffn_activation_name"]
    else:
        ffn_activation_name = "gelu-no-approx"

    if "use_glu_in_ffn" in hyperparams.keys():
        use_glu_in_ffn = hyperparams["use_glu_in_ffn"]
    else:
        use_glu_in_ffn = False

    if "add_bias_kv" in hyperparams.keys():
        add_bias_kv = hyperparams["add_bias_kv"]
    else:
        add_bias_kv = False

    if "use_rotary_embedding" in hyperparams.keys():
        use_rotary_embedding = hyperparams["use_rotary_embedding"]
    else:
        use_rotary_embedding = False

    if "mask_before_attention" in hyperparams.keys():
        mask_before_attention = hyperparams["mask_before_attention"]
    else:
        mask_before_attention = False

    if "positional_embedding" in hyperparams.keys():
        positional_embedding = hyperparams["positional_embedding"]
    else:
        positional_embedding = "learned"

    if "lm_head" in hyperparams.keys():
        lm_head = hyperparams["lm_head"]
    else:
        lm_head = "roberta"

    training_rescaling_factor = hyperparams["rescaling_factor"]

    if rescaling_factor is None:
        inference_rescaling_factor = training_rescaling_factor

    # Rescaling factor is indicated and should replace the one from the training
    else:
        inference_rescaling_factor = rescaling_factor

    genomic_features = hyperparams["features"]

    # Get config
    config = SegmentNTConfig(
        alphabet_size=alphabet_size,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        max_positions=hyperparams["max_positions"],
        embed_scale=hyperparams["embed_scale"],
        # architecture
        emb_layer_norm_before=hyperparams["emb_layer_norm_before"],
        key_size=key_size,
        attention_heads=hyperparams["attention_heads"],
        embed_dim=hyperparams["embed_dim"],
        ffn_embed_dim=hyperparams["ffn_embed_dim"],
        num_layers=hyperparams["num_layers"],
        positional_embedding=positional_embedding,
        lm_head=lm_head,
        add_bias_kv=add_bias_kv,
        add_bias_ffn=add_bias_ffn,
        use_glu_in_ffn=use_glu_in_ffn,
        ffn_activation_name=ffn_activation_name,
        use_rotary_embedding=use_rotary_embedding,
        mask_before_attention=mask_before_attention,
        # bert
        token_dropout=hyperparams["token_dropout"],
        masking_ratio=hyperparams["masking_ratio"],
        masking_prob=hyperparams["masking_prob"],
        # embeddings to save
        embeddings_layers_to_save=embeddings_layers_to_save,
        attention_maps_to_save=attention_maps_to_save,  # type: ignore
        # Rotary embeddings rescaling
        rescaling_factor=inference_rescaling_factor,
        features=genomic_features,
    )

    # NOTE: module names are changed here, to validate !
    parameters = rename_modules_segment_nt(parameters, model_name)

    # get segmentation model
    def head_fn() -> hk.Module:
        return UNetHead(
            features=genomic_features,
            embed_dimension=config.embed_dim,
            name=model_name,
        )

    forward_fn = build_esm_with_head_fn(
        model_config=config,
        head_fn=head_fn,
        compute_dtype=compute_dtype,
        param_dtype=param_dtype,
        output_dtype=output_dtype,
        model_name=model_name,
    )

    return parameters, forward_fn, tokenizer, config
