"""Implementation of a standard Encoder Decoder model."""
from dataclasses import dataclass
from typing import Callable, Dict, Optional

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.models.t5.decoder import T5Decoder, T5DecoderConfig
from trix.models.t5.encoder import T5Encoder, T5EncoderConfig
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput
from trix.utils.masking import build_causal_attention_mask, build_padding_attention_mask


@dataclass
class T5Config:
    """
    Parameters to initialize a T5 model. The configurations are broad enough to be
    compatible with the standard T5 model, T5 v1.1, and Flan-T5.  Defaults to the values
    for t5-small.

    Args:
        encoder_config: Encoder configuration.
        decoder_config: Decoder configuration.
    """

    encoder_config: T5EncoderConfig
    decoder_config: T5DecoderConfig

    def __post_init__(self) -> None:
        """
        Checks that the given values are compatible.
        """
        if self.encoder_config.embed_dim != self.decoder_config.embed_dim:
            raise ValueError(
                "Attention embed dim must be the same in encoder and decoder for "
                "cross-attention."
            )
        if self.encoder_config.pad_token_id != self.decoder_config.pad_token_id:
            raise ValueError("Pad token id must be the same for encoder and decoder.")
        if self.encoder_config.eos_token_id != self.decoder_config.eos_token_id:
            raise ValueError("Stop token id must be the same for encoder and decoder.")
        if self.encoder_config.vocab_size != self.decoder_config.vocab_size:
            raise ValueError("Vocabulary must be the same for encoder and decoder.")
        if (
            self.encoder_config.max_bucket_distance
            != self.decoder_config.max_bucket_distance
        ):
            raise ValueError(
                "Max bucket distance must be the same for encoder and decoder."
            )
        if self.encoder_config.num_buckets != self.decoder_config.num_buckets:
            raise ValueError(
                "Number of buckets must be the same for encoder and decoder."
            )


class T5Transformer(hk.Module):
    """
    Creates the T5 Encoder Decoder model.
    """

    def __init__(
        self,
        config: T5Config,
        name: Optional[str] = None,
    ):
        """
        Initializes the Encoder stack and Decoder stack, including the relative
        positional embeddings.  Defaults to the hyperparameters for T5_small.

        Args:
            config: Configuration data class containing the hyperparameters for the full
                encoder-decoder
            name: Optional name for this module.
        """

        super().__init__(name=name)
        self._config = config
        self.encoder = T5Encoder(config=config.encoder_config)
        self.decoder = T5Decoder(config=config.decoder_config)

    def encode(
        self,
        encoder_token_ids: Tokens,
        encoder_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Encodes the tokens fed to the encoder to embeddings to feed to the decoder.

        Args:
            encoder_token_ids: Input tokenized sequences of shape (batch_size, seq_len).
            encoder_attention_mask: Attention mask of shape (batch_size, 1, seq_len,
                seq_len). If none is given, will default to a mask on pad tokens defined
                by self.pad_token_id.
            dropout_rate: Dropout rate, defaults to 0.0.

        Returns:
            TransformerOutput: _description_
        """

        return self.encoder(
            token_ids=encoder_token_ids,
            attention_mask=encoder_attention_mask,
            dropout_rate=dropout_rate,
        )

    def decode(
        self,
        decoder_token_ids: Tokens,
        encoder_embeddings: Embedding,
        decoder_self_attention_mask: Optional[AttentionMask] = None,
        decoder_cross_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:

        return self.decoder(
            token_ids=decoder_token_ids,
            encoder_embeddings=encoder_embeddings,
            self_attention_mask=decoder_self_attention_mask,
            cross_attention_mask=decoder_cross_attention_mask,
            dropout_rate=dropout_rate,
        )

    def __call__(
        self,
        encoder_token_ids: Tokens,
        decoder_token_ids: Tokens,
        encoder_output: Optional[Dict[str, Embedding]] = None,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_self_attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Computes the output of the decoder given an encoder sequence and either a
        decoder sequence (training) or the decoder start token (inference).

        Args:
            encoder_token_ids: Sequence of token ids delivered to the encoder of shape
                (batch_size, seq_len).
            decoder_token_ids: Sequence of token ids delivered to the decoder of shape
                (batch_size, seq_len).
            encoder_attention_mask: Mask for the self-attention in the encoder, of shape
                (batch_size, 1, seq_len, seq_len). If none is given, will mask only
                pad tokens.
            decoder_self_attention_mask: Mask for the self-attention in the decoder, of
                shape (batch_size, 1, seq_len, seq_len). If none is given, will default
                to a causal mask that also masks pad tokens.
            cross_attention_mask: Mask for the cross-attention in the decoder, of shape
                (batch_size, 1, seq_len). If none is given, will mask only pad tokens.
            dropout_rate: Fraction of components set to 0 during dropout.

        Returns:
            Logits associated with decoder_token_ids.
        """
        batch_size, seq_len = decoder_token_ids.shape
        if encoder_attention_mask is None:
            encoder_attention_mask = build_padding_attention_mask(
                tokens=encoder_token_ids,
                pad_token_id=self._config.encoder_config.pad_token_id,
            )
        if decoder_self_attention_mask is None:
            # Decoder for T5 only takes in a causal mask unless specified otherwise.
            decoder_self_attention_mask = build_causal_attention_mask(
                batch_size=batch_size, seq_len=seq_len
            )
        if cross_attention_mask is None:
            cross_attention_mask = encoder_attention_mask[:, :, :1]

        if encoder_output is None:
            encoder_output = self.encode(
                encoder_token_ids=encoder_token_ids,
                encoder_attention_mask=encoder_attention_mask,
                dropout_rate=dropout_rate,
            )
        encoder_embeddings = encoder_output["embeddings"]
        decoder_outs = self.decode(
            decoder_token_ids=decoder_token_ids,
            encoder_embeddings=encoder_embeddings,
            decoder_self_attention_mask=decoder_self_attention_mask,
            decoder_cross_attention_mask=cross_attention_mask,
            dropout_rate=dropout_rate,
        )

        outs: TransformerOutput = {}
        outs.update(encoder_output)
        outs.update(decoder_outs)

        return outs


def build_t5_fn(
    config: T5Config,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Create the model's forward pass.  Supports the creation of the original T5 model,
    as well as the newer Flan-T5 architecture.

    Args:
        config: Configuration data class containing the hyperparameters for the T5
            forward function
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        name: Optional name for this module.

    Returns:
        T5 Encoder Decoder model forward function.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(T5Transformer, policy)
    hk.mixed_precision.set_policy(T5Encoder, policy)
    hk.mixed_precision.set_policy(T5Decoder, policy)

    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32,
        param_dtype=param_dtype,
        output_dtype=compute_dtype,
    )
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)

    def t5_fn(
        encoder_token_ids: jnp.ndarray,
        decoder_token_ids: jnp.ndarray,
        encoder_output: Optional[Dict[str, Embedding]] = None,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_self_attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> jnp.ndarray:
        """
        Calls the T5Transformer.

        Args:
            encoder_token_ids: Sequence of token ids delivered to the encoder of shape
                (batch_size, seq_len).
            decoder_token_ids: Sequence of token ids delivered to the decoder of shape
                (batch_size, seq_len).
            encoder_attention_mask: Mask for the self-attention in the encoder, of shape
                (batch_size, 1, seq_len, seq_len). If none is given, will mask only
                pad tokens.
            decoder_self_attention_mask: Mask for the self-attention in the decoder, of
                shape (batch_size, 1, seq_len, seq_len). If none is given, will default
                to a causal mask that also masks pad tokens.
            cross_attention_mask: Mask for the cross-attention in the decoder, of shape
                (batch_size, 1, seq_len). If none is given, will mask only pad tokens.
            dropout_rate: Fraction of components set to 0 during dropout.

        Returns:
            jnp.ndarray: _description_
        """
        model = T5Transformer(config=config, name=name)

        return model(
            encoder_token_ids=encoder_token_ids,
            decoder_token_ids=decoder_token_ids,
            encoder_output=encoder_output,
            encoder_attention_mask=encoder_attention_mask,
            decoder_self_attention_mask=decoder_self_attention_mask,
            cross_attention_mask=cross_attention_mask,
            dropout_rate=dropout_rate,
        )

    return t5_fn
