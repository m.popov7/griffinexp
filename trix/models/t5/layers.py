from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp

from trix.types import Embedding
from trix.utils.activations import get_activation_fn


class T5LayerNorm(hk.Module):
    """
    Basic layer normalization of each individual time step and batch member.  In
    contrast to the standard implementation, T5 does not subtract off the mean or
    include a trainable bias parameter.
    """

    def __init__(self, name: Optional[str] = "layer_norm"):
        super().__init__(name=name)

    def __call__(self, embeddings: jnp.ndarray, eps: float = 1e-6) -> jnp.ndarray:
        """
        Run the T5 layer normalization on the embedding dimension of the inputs

        Args:
            embeddings: Input embeddings
            eps: Small parameter to prevent possible division by 0

        Returns:
            The normalized output
        """
        scale_params = hk.get_parameter(
            name="w",
            shape=(embeddings.shape[-1],),
            dtype=embeddings.dtype,
            init=jnp.ones,
        )
        mean_sq = jnp.mean(jax.lax.square(embeddings), axis=-1, keepdims=True)
        y = jnp.asarray(embeddings * jax.lax.rsqrt(mean_sq + eps))
        return y * scale_params


class T5MultiHeadAttention(hk.Module):
    """
    Multi-head attention with masking applied. Modified from Haiku implementation to
    be able to support relative positional embeddings.  Computes the keys, queries, and
    values from the input embeddings.  Future versions could compute and store these
    to prevent redundant calculations during autoregressive inference. The key, query,
    and value sizes are fixed to be embed_dim/num_heads in accordance with the standard
    T5 model.
    """

    def __init__(
        self,
        embed_dim: int,
        attention_embed_dim: int,
        num_heads: int,
        name: Optional[str] = "attention",
    ):
        """
        Initializes the attention layer.

        Args:
            embed_dim: Length of the token embedding at each position in the sequence.
            attention_embed_dim: Last dimension of the keys, queries, and values.
                Typically equal to embed_dim.
            num_heads: Number of independent attention heads.
            name: Optional name for this module.
        """
        super().__init__(name=name)
        self.num_heads = num_heads
        self.key_linear = hk.Linear(
            output_size=attention_embed_dim, with_bias=False, name="key_linear"
        )
        self.query_linear = hk.Linear(
            output_size=attention_embed_dim, with_bias=False, name="query_linear"
        )
        self.value_linear = hk.Linear(
            output_size=attention_embed_dim, with_bias=False, name="value_linear"
        )
        self.out_linear = hk.Linear(
            output_size=embed_dim, with_bias=False, name="out_linear"
        )

    def _apply_dropout(
        self, attention_weights: jnp.ndarray, dropout_rate: float
    ) -> jnp.ndarray:
        """
        Utility function to apply dropout on attention head.
        Note: Can be replaced with hk.dropout in dm-haiku=0.0.11
        Args:
            attention_weights: attention weights generated by multiplying
                query and key matrics
            dropout_rate: Dropout rate between 0 and 1
        Returns:
            Attention weights with dropout and appropriate scaling applied.
        """
        keep_prob = 1.0 - dropout_rate
        dropout_shape = list(attention_weights.shape)
        dropout_shape[-2] = 1
        keep = jax.random.bernoulli(hk.next_rng_key(), keep_prob, dropout_shape)
        keep = jnp.broadcast_to(keep, attention_weights.shape)
        multiplier = keep.astype(attention_weights.dtype) / jnp.asarray(
            keep_prob, dtype=attention_weights.dtype
        )
        attention_weights = attention_weights * multiplier
        return attention_weights

    def __call__(
        self,
        query_inputs: jnp.ndarray,
        key_inputs: jnp.ndarray,
        value_inputs: jnp.ndarray,
        attention_mask: Optional[jnp.ndarray] = None,
        position_bias: Optional[jnp.ndarray] = None,
        dropout_rate: float = 0.0,
    ) -> jnp.ndarray:
        """
        Computes the result of multiheaded dot-product attention, using
        pre-computed projections for the queries, keys, and values.  Accepts
        a relative positional bias argument to be added to the logits, as well
        as an attention_mask when training the decoder

        Args:
            query_inputs: Embeddings that will be projected to become the queries.
            key_inputs: Embeddings that will be projected to become the keys.
            value_inputs: Embeddings that will be projected to become the values.
            attention_mask: Mask to be applied in the attention layers.
                Triangular for autoregressive models.
            position_bias: Relative positional embeddings as defined in T5.

        Returns:
            The standard output of multi-headed attention
        """
        # Note: assertion cannot be jitted leading to jax Concretization error.
        keys = self.key_linear(key_inputs)
        queries = self.query_linear(query_inputs)
        values = self.value_linear(value_inputs)
        keys = keys.reshape(keys.shape[0], keys.shape[1], self.num_heads, -1)
        queries = queries.reshape(
            queries.shape[0], queries.shape[1], self.num_heads, -1
        )
        attention_logits = jnp.einsum("...thd,...Thd->...htT", queries, keys)
        if position_bias is not None:
            attention_logits += position_bias
        if attention_mask is not None:
            attention_logits = jnp.where(attention_mask, attention_logits, -1e30)

        attention_weights = jax.nn.softmax(attention_logits, axis=-1)
        attention_weights = hk.cond(
            dropout_rate > 0.0,
            lambda x, drate: self._apply_dropout(x, dropout_rate=drate),
            lambda x, drate: x,
            attention_weights,
            dropout_rate,
        )

        values = values.reshape(values.shape[0], values.shape[1], self.num_heads, -1)
        values = jnp.einsum("...htT,...Thd->...thd", attention_weights, values)
        values = jnp.reshape(values, (values.shape[0], values.shape[1], -1))

        return self.out_linear(values)


class T5EncoderLayer(hk.Module):
    """
    Single layer in the encoder, including self-attention and feed-forward operations.
    The feed-forward network takes one linear projection for each activation, and then
    multiplies the activations of those layers together before completing the MLP.
    """

    def __init__(
        self,
        embed_dim: int,
        attention_embed_dim: int,
        num_heads: int,
        ffn_embed_dim: int,
        ffn_activation_name: str = "relu",
        use_glu_in_ffn: bool = False,
        name: Optional[str] = None,
    ):
        """
        Initializes the encoder layer, including the projections needed for
        self-attention and the linear layers applied in the fully connected portion

        Args:
            embed_dim: Length of the token embedding at each position in the sequence.
            attention_embed_dim: Last dimension of the keys, queries, and values.
                Typically equal to embed_dim.
            num_heads: Number of independent attention heads.
            ffn_embed_dim: Size of the hidden dimension in the fully connected network.
            ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "gelu-no-approx", "relu", "swish".
            use_glu_in_ffn: whether to use Gated Linear Unit (GLU) in Feed
                Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
                to True and use swish as ffn_activation_name.
                Same principle for a gated-relu.
            name: Optional name for this module.
        """
        super().__init__(name=name)

        self.num_heads = num_heads
        self.self_attention = T5MultiHeadAttention(
            embed_dim=embed_dim,
            attention_embed_dim=attention_embed_dim,
            num_heads=num_heads,
            name="self_attention",
        )

        self.self_attention_layer_norm = T5LayerNorm("self_attention_layer_norm")
        self.fc_layer_norm = T5LayerNorm("fc_layer_norm")

        # Get ffn activation function
        self._ffn_activation_fn = get_activation_fn(activation_name=ffn_activation_name)
        self._use_glu_in_fnn = use_glu_in_ffn

        # Define layers
        if use_glu_in_ffn:
            # user should multiply ffn_embed_dim by 2/3 when using GLU
            # to keep total number of parameters equal
            # see https://arxiv.org/pdf/2002.05202.pdf. for more details
            # we multiply by 2 here as the output will be split in 2 for GLU
            ffn_embed_dim = int(2 * ffn_embed_dim)

        # T5 does not have biases anywhere in their FFN block
        self.fc1_linear = hk.Linear(
            output_size=ffn_embed_dim, with_bias=False, name="fc1_linear"
        )

        self.fc2_linear = hk.Linear(
            output_size=embed_dim, with_bias=False, name="fc2_linear"
        )

    @hk.transparent
    def mlp(self, x: jnp.ndarray, dropout_rate: float = 0.0) -> jnp.ndarray:
        """
        Applies the linear layers associated with the tuple of activations, multiplies
        the activations, then applies dropout and a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, embed_dim).
            dropout_rate: The percentage of inputs to drop

        Returns:
            The transformed sequence embedding.
        """
        if self._use_glu_in_fnn:
            x1, x2 = jnp.split(self.fc1_linear(x), indices_or_sections=2, axis=-1)
            x = self._ffn_activation_fn(x1) * x2
        else:
            x = self._ffn_activation_fn(self.fc1_linear(x))

        x = hk.dropout(hk.next_rng_key(), dropout_rate, x)
        return self.fc2_linear(x)

    def __call__(
        self,
        token_embed_seq: jnp.ndarray,
        dropout_rate: float = 0.0,
        attention_mask: Optional[jnp.ndarray] = None,
        position_bias: Optional[jnp.ndarray] = None,
    ) -> jnp.ndarray:
        """
        Computes the output embeddings of the encoder layer

        Args:
            token_embed_seq: Encoder layer input embeddings of shape
                (batch_size,seq_len,embed_dim).
            dropout_rate: Fraction of tokens randomly set to 0 in dropout layers.
            attention_mask: Tokens to set to 0 during attention.
            position_bias: Relative positional embeddings applied to the logits.

        Returns:
            The output embeddings that result from the application of the layer
        """
        x1 = self.self_attention_layer_norm(token_embed_seq)
        x1 = self.self_attention(
            x1,
            x1,
            x1,
            attention_mask=attention_mask,
            position_bias=position_bias,
            dropout_rate=dropout_rate,
        )
        x1 = hk.dropout(hk.next_rng_key(), dropout_rate, x1) + token_embed_seq

        x2 = self.fc_layer_norm(x1)
        x2 = self.mlp(x2, dropout_rate)
        return hk.dropout(hk.next_rng_key(), dropout_rate, x2) + x1


class T5DecoderLayer(hk.Module):
    """
    Single layer in the decoder, including self-attention, cross-attention,
    and feed-forward operations.
    """

    def __init__(
        self,
        embed_dim: int,
        attention_embed_dim: int,
        num_heads: int,
        ffn_embed_dim: int,
        ffn_activation_name: str = "relu",
        use_glu_in_ffn: bool = False,
        name: Optional[str] = None,
    ):
        """
        Initializes the decoder layer, including self-attention, cross-attention,
        layer norms, and the linear layers applied in the fully connected portion

        Args:
            embed_dim: Length of the token embedding at each position in the sequence.
            attention_embed_dim: Last dimension of the keys, queries, and values.
                Typically equal to embed_dim.
            num_heads: Number of independent attention heads.
            ffn_embed_dim: Size of the hidden dimension in the fully connected network.
            ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "relu", "swish".
            use_glu_in_ffn: whether to use Gated Linear Unit (GLU) in Feed
                Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
                to True and use swish as ffn_activation_name.
                Same principle for a gated-relu.
            name: Optional name for this module.
        """

        super().__init__(name=name)
        self.num_heads = num_heads

        self.self_attention = T5MultiHeadAttention(
            embed_dim=embed_dim,
            attention_embed_dim=attention_embed_dim,
            num_heads=num_heads,
            name="self_attention",
        )
        self.cross_attention = T5MultiHeadAttention(
            embed_dim=embed_dim,
            attention_embed_dim=attention_embed_dim,
            num_heads=num_heads,
            name="cross_attention",
        )

        self.self_attention_layer_norm = T5LayerNorm(name="self_attention_layer_norm")
        self.cross_attention_layer_norm = T5LayerNorm(name="cross_attention_layer_norm")
        self.fc_layer_norm = T5LayerNorm(name="fc_layer_norm")

        # Get ffn activation function
        self._ffn_activation_fn = get_activation_fn(activation_name=ffn_activation_name)
        self._use_glu_in_fnn = use_glu_in_ffn

        # Define layers
        if use_glu_in_ffn:
            # user should multiply ffn_embed_dim by 2/3 when using GLU
            # to keep total number of parameters equal
            # see https://arxiv.org/pdf/2002.05202.pdf. for more details
            # we multiply by 2 here as the output will be split in 2 for GLU
            ffn_embed_dim = int(2 * ffn_embed_dim)

        # T5 does not have biases anywhere in their FFN block
        self.fc1_linear = hk.Linear(
            output_size=ffn_embed_dim, with_bias=False, name="fc1_linear"
        )

        self.fc2_linear = hk.Linear(
            output_size=embed_dim, with_bias=False, name="fc2_linear"
        )

    def mlp(self, x: jnp.ndarray, dropout_rate: float = 0.0) -> jnp.ndarray:
        """
        Applies one linear layer, a Relu activation, dropout
        then a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, embed_dim).
            dropout_rate: The percentage of inputs to drop

        Returns:
            The transformed sequence embedding.
        """
        if self._use_glu_in_fnn:
            x1, x2 = jnp.split(self.fc1_linear(x), indices_or_sections=2, axis=-1)
            x = self._ffn_activation_fn(x1) * x2
        else:
            x = self._ffn_activation_fn(self.fc1_linear(x))

        x = hk.dropout(hk.next_rng_key(), dropout_rate, x)
        return self.fc2_linear(x)

    def __call__(
        self,
        embed_seq: Embedding,
        encoder_output: Embedding,
        dropout_rate: float = 0.0,
        self_attention_mask: Optional[jnp.ndarray] = None,
        cross_attention_mask: Optional[jnp.ndarray] = None,
        position_bias: Optional[jnp.ndarray] = None,
    ) -> jnp.ndarray:
        """
        Computes the output embeddings of the encoder layer

        Args:
            embed_seq: Decoder layer input embeddings with shape
                (batch_size,seq_len,embed_dim)
            encoder_output: Final output embeddings from the encoder
            dropout_rate: Fraction of tokens randomly set to 0 in dropout layers.
            self_attention_mask: Mask to be applied in the self-attention call.
            cross_attention_mask: Mask to be applied in the cross-attention call.
            position_bias: Relative positional embeddings applied to the logits.

        Returns:
            The output embeddings that result from the application of the layer
        """

        x1 = self.self_attention_layer_norm(embed_seq)
        x1 = self.self_attention(
            x1,
            x1,
            x1,
            attention_mask=self_attention_mask,
            position_bias=position_bias,
            dropout_rate=dropout_rate,
        )
        x1 = hk.dropout(hk.next_rng_key(), dropout_rate, x1) + embed_seq

        x2 = self.cross_attention_layer_norm(x1)
        x2 = self.cross_attention(
            x2, encoder_output, encoder_output, attention_mask=cross_attention_mask
        )
        x2 = hk.dropout(hk.next_rng_key(), dropout_rate, x2) + x1

        x3 = self.fc_layer_norm(x2)
        x3 = self.mlp(x3, dropout_rate)

        return hk.dropout(hk.next_rng_key(), dropout_rate, x3) + x2


class RelativePositionalBiases(hk.Module):
    """
    Class to generate the T5-type relative positional embeddings which are
    added into the attention logits.
    """

    def __init__(
        self,
        num_buckets: int = 64,
        num_heads: int = 12,
        name: Optional[str] = "positional_bias",
    ):
        """
        Initializes the relative positional embeddings for a single stack.
        Defaults to the hyperparameters for Ankh_base.

        Args:
            num_buckets: Number of relative positional buckets, each of which is
                assigned a common bias.
            num_heads: Number of attention heads in the encoder-decoder.
            name: Optional name for this module.
        """
        super().__init__(name=name)
        self.num_buckets = num_buckets
        self.pos_embed = hk.Embed(
            vocab_size=num_buckets, embed_dim=num_heads, name="positional_embed"
        )

    def __call__(
        self,
        query_length: int,
        key_length: int,
        bidirectional: bool = True,
        max_distance: int = 128,
    ) -> jnp.ndarray:
        """
        Computes the relative positional biases according to the method described in T5.
        Each distance from the current query is given a bucket, and each bucket is
        given a scalar bias.  Biases are different across heads but shared across all
        layers in the same stack.

        Args:
            query_length: Number of queries that will be used to calculate
                self-attention. Typically equal to the sequence length.
            key_length: Number of keys that will be used to calculate self-attention.
                Typically equal to the sequence length.
            bidirectional: When False, keys that come after the current query are
                invalid. Set to False in the decoder, but in the standard T5
                architecture the masking mechanism already invalidates the biases in
                those positions.
            max_distance: Maximum distance to start a bucket.  All greater distances
                will be clipped to this bucket.

        Returns:
            The positional bias array of shape (1,num_heads,query_length,key_length)
        """
        num_buckets = self.num_buckets
        context_position = jnp.arange(key_length - query_length, key_length, dtype=int)[
            :, None
        ]
        memory_position = jnp.arange(key_length, dtype=int)[None, :]

        relative_position = memory_position - context_position
        relative_buckets = 0
        if bidirectional:
            num_buckets //= 2
            relative_buckets += (relative_position > 0) * num_buckets
            relative_position = jnp.abs(relative_position)
        else:
            relative_position = -jnp.clip(relative_position, a_max=0)
        max_exact = num_buckets // 2
        is_small = relative_position < max_exact

        # The other half of the buckets are for logarithmically bigger bins in positions
        # up to max_distance
        relative_position_if_large = max_exact + (
            jnp.log(relative_position / max_exact)
            / jnp.log(max_distance / max_exact)
            * (num_buckets - max_exact)
        )
        relative_position_if_large = jnp.clip(
            relative_position_if_large, a_max=num_buckets - 1
        )
        relative_buckets += jnp.where(
            is_small, relative_position, relative_position_if_large
        )
        relative_buckets = jnp.array(relative_buckets, dtype=int)

        final_biases = self.pos_embed(relative_buckets)
        final_biases = final_biases.transpose((2, 0, 1))[None, :, :, :]
        return final_biases


class T5DecoderOnlyLayer(hk.Module):
    """
    Single layer in the decoder, including self-attention, cross-attention,
    and feed-forward operations.
    """

    def __init__(
        self,
        embed_dim: int,
        attention_embed_dim: int,
        num_heads: int,
        ffn_embed_dim: int,
        ffn_activation_name: str = "relu",
        use_glu_in_ffn: bool = False,
        name: Optional[str] = None,
    ):
        """
        Initializes the decoder layer, including self-attention, cross-attention,
        layer norms, and the linear layers applied in the fully connected portion

        Args:
            embed_dim: Length of the token embedding at each position in the sequence.
            attention_embed_dim: Last dimension of the keys, queries, and values.
                Typically equal to embed_dim.
            num_heads: Number of independent attention heads.
            ffn_embed_dim: Size of the hidden dimension in the fully connected network.
            ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "relu", "swish".
            use_glu_in_ffn: whether to use Gated Linear Unit (GLU) in Feed
                Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
                to True and use swish as ffn_activation_name.
                Same principle for a gated-relu.
            name: Optional name for this module.
        """

        super().__init__(name=name)
        self.num_heads = num_heads

        self.self_attention = T5MultiHeadAttention(
            embed_dim=embed_dim,
            attention_embed_dim=attention_embed_dim,
            num_heads=num_heads,
            name="self_attention",
        )

        self.self_attention_layer_norm = T5LayerNorm(name="self_attention_layer_norm")
        self.fc_layer_norm = T5LayerNorm(name="fc_layer_norm")

        # Get ffn activation function
        self._ffn_activation_fn = get_activation_fn(activation_name=ffn_activation_name)
        self._use_glu_in_fnn = use_glu_in_ffn

        # Define layers
        if use_glu_in_ffn:
            # user should multiply ffn_embed_dim by 2/3 when using GLU
            # to keep total number of parameters equal
            # see https://arxiv.org/pdf/2002.05202.pdf. for more details
            # we multiply by 2 here as the output will be split in 2 for GLU
            ffn_embed_dim = int(2 * ffn_embed_dim)

        # T5 does not have biases anywhere in their FFN block
        self.fc1_linear = hk.Linear(
            output_size=ffn_embed_dim, with_bias=False, name="fc1_linear"
        )

        self.fc2_linear = hk.Linear(
            output_size=embed_dim, with_bias=False, name="fc2_linear"
        )

    def mlp(self, x: jnp.ndarray, dropout_rate: float = 0.0) -> jnp.ndarray:
        """
        Applies one linear layer, a Relu activation, dropout
        then a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, embed_dim).
            dropout_rate: The percentage of inputs to drop

        Returns:
            The transformed sequence embedding.
        """
        if self._use_glu_in_fnn:
            x1, x2 = jnp.split(self.fc1_linear(x), indices_or_sections=2, axis=-1)
            x = self._ffn_activation_fn(x1) * x2
        else:
            x = self._ffn_activation_fn(self.fc1_linear(x))

        x = hk.dropout(hk.next_rng_key(), dropout_rate, x)
        return self.fc2_linear(x)

    def __call__(
        self,
        embed_seq: Embedding,
        dropout_rate: float = 0.0,
        self_attention_mask: Optional[jnp.ndarray] = None,
        position_bias: Optional[jnp.ndarray] = None,
    ) -> jnp.ndarray:
        """
        Computes the output embeddings of the encoder layer

        Args:
            embed_seq: Decoder layer input embeddings with shape
                (batch_size,seq_len,embed_dim)
            dropout_rate: Fraction of tokens randomly set to 0 in dropout layers.
            self_attention_mask: Mask to be applied in the self-attention call.
            position_bias: Relative positional embeddings applied to the logits.

        Returns:
            The output embeddings that result from the application of the layer
        """

        x1 = self.self_attention_layer_norm(embed_seq)
        x1 = self.self_attention(
            x1,
            x1,
            x1,
            attention_mask=self_attention_mask,
            position_bias=position_bias,
            dropout_rate=dropout_rate,
        )
        x1 = hk.dropout(hk.next_rng_key(), dropout_rate, x1) + embed_seq

        x2 = self.fc_layer_norm(x1)
        x2 = self.mlp(x2, dropout_rate)

        return hk.dropout(hk.next_rng_key(), dropout_rate, x2) + x2
