"""Implementation of a standard Encoder Decoder model."""
from dataclasses import dataclass
from typing import Callable, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.models.t5.layers import RelativePositionalBiases, T5EncoderLayer, T5LayerNorm
from trix.types import AttentionMask, Tokens, TransformerOutput
from trix.utils.activations import SUPPORTED_FFN_ACTIVATIONS
from trix.utils.masking import build_padding_attention_mask


@dataclass
class T5EncoderConfig:
    """
    Parameters to initialize a T5Encoder model.

    Args:
        vocab_size: Token vocabulary size, uniformly set to 32128 in pre-trained models.
        pad_token_id: Id for the pad token, which is 0 in the standard T5 vocabulary.
        eos_token_id: Id for the stop token, which is 1 in the standard T5 vocabulary.
        embed_dim: Embedding dimension.
        attention_embed_dim: Embedding dimension in the attention layer, equal to
            num_heads*key_size. Most architectures have attention_embed_dim == embed_dim
            but some like flan-t5-small do not.
        ffn_embed_dim: Feed forward embedding dimension.
        num_heads: Number of attention heads.
        num_layers: Number of layers in the encoder.
        ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "gelu-no-approx", "relu", "swish".
        use_glu_in_ffn: Whether to use Gated Linear Unit (GLU) in Feed
            Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
            to True and use swish as ffn_activation_name.
            Same principle for a gated-relu.
        num_buckets: Number of positional bias buckets in the positional embeddings.
        max_bucket_distance: Maximum distance from a query that will start a positional
            bias bucket.
        output_len: Length of the outputs that will be generated during inference.
        dropout_rate: Fraction of embedding components set to 0 during dropout.
        embeddings_layers_to_save: Indices of the embeddings to return from the
            encoder.
    """

    vocab_size: int = 32128
    pad_token_id: int = 0
    eos_token_id: int = 1

    # architecture
    embed_dim: int = 512
    attention_embed_dim: int = 512
    ffn_embed_dim: int = 2048
    num_heads: int = 8
    num_layers: int = 6
    ffn_activation_name: str = "relu"
    use_glu_in_ffn: bool = False

    num_buckets: int = 32
    max_bucket_distance: int = 128
    dropout_rate: float = 0.0

    # return
    embeddings_layers_to_save: Tuple[int, ...] = ()

    def __post_init__(self) -> None:
        if self.attention_embed_dim % self.num_heads != 0:
            raise ValueError(
                "The attention embedding dimension should be "
                "divisible by the number of heads, however provided embedding "
                f"dimension is {self.attention_embed_dim} and the number of heads is "
                f"{self.num_heads}."
            )
        if self.ffn_activation_name not in SUPPORTED_FFN_ACTIVATIONS:
            raise ValueError(
                f"The ffn_activation_name must be one of {SUPPORTED_FFN_ACTIVATIONS}"
            )


class T5Encoder(hk.Module):
    """
    Creates an encoder module based on the T5 model architecture. The
    key differences from a standard transformer are the simplified layer normalization
    and the use of a relative position embedding scheme.
    """

    def __init__(
        self,
        config: T5EncoderConfig,
        name: Optional[str] = None,
    ):
        """
        Initializes the Encoder stack for the T5 Encoder Decoder model.  Defaults to the
        hyperparameters used by T5_Small

        Args:
            config: Configuration data class for the encoder.
            name: Optional name for this module.
        """
        super().__init__(name=name)
        self._config = config

        self.token_embedding = hk.Embed(
            vocab_size=config.vocab_size, embed_dim=config.embed_dim, name="token_embed"
        )

        self.relative_positional_bias = RelativePositionalBiases(
            num_buckets=config.num_buckets,
            num_heads=config.num_heads,
        )

        self.embeddings_layers_to_save = config.embeddings_layers_to_save

        self.layers = [
            T5EncoderLayer(
                embed_dim=config.embed_dim,
                attention_embed_dim=config.attention_embed_dim,
                num_heads=config.num_heads,
                ffn_embed_dim=config.ffn_embed_dim,
                ffn_activation_name=config.ffn_activation_name,
                use_glu_in_ffn=config.use_glu_in_ffn,
            )
            for _ in range(config.num_layers)
        ]
        self.final_layer_norm = T5LayerNorm("final_layer_norm")

    def __call__(
        self,
        token_ids: Tokens,
        attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Compute encoder output embeddings based on input token embeddings.

        Args:
            token_ids: Input token embeddings of shape (batch_size, seq_len,
                embed_dim). Encoder takes embeddings as an input as the Embed layer is
                shared with the Decoder.
            dropout_rate: Fraction of components to be set to 0 during dropout
            attention_mask: Mask to be applied in the self-attention call.
                embed_dim). Encoder takes embeddings as an input as the Embed layer is
                shared with the Decoder.

        Returns:
            The final embeddings produced after sequential application of the encoder
            layers
        """
        outs: TransformerOutput = {}
        seq_len = token_ids.shape[1]

        token_embeddings = self.token_embedding(ids=token_ids)
        x = hk.dropout(hk.next_rng_key(), dropout_rate, token_embeddings)

        position_bias = self.relative_positional_bias(
            query_length=seq_len,
            key_length=seq_len,
            bidirectional=True,
            max_distance=self._config.max_bucket_distance,
        )

        if attention_mask is None:
            attention_mask = build_padding_attention_mask(
                tokens=token_ids, pad_token_id=self._config.pad_token_id
            )

        for i, layer in enumerate(self.layers):
            x = layer(x, dropout_rate, attention_mask, position_bias)
            if i + 1 in self.embeddings_layers_to_save:
                outs[f"embeddings_{i+1}"] = x

        x = self.final_layer_norm(x)
        x = hk.dropout(hk.next_rng_key(), dropout_rate, x)
        outs["embeddings"] = x
        return outs


def build_t5_encoder_fn(
    config: T5EncoderConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Create the T5 encoder forward pass.

    Args:
        config: Configuration data class containing the hyperparameters for the T5
            forward function
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        name: Optional name, will default to t5_encoder.

    Returns:
        T5 Encoder model forward function.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(T5Encoder, policy)

    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32, param_dtype=param_dtype, output_dtype=compute_dtype
    )
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)

    def t5_encoder_fn(
        encoder_token_ids: jnp.ndarray,
        attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> jnp.ndarray:
        model = T5Encoder(config, name=name)

        return model(
            token_ids=encoder_token_ids,
            attention_mask=attention_mask,
            dropout_rate=dropout_rate,
        )

    return t5_encoder_fn
