"""Implementation of a standard Encoder Decoder model."""
from dataclasses import dataclass
from typing import Callable, Optional, Union

import haiku as hk
import jax.numpy as jnp
import jmp
from jax.lax import rsqrt

from trix.models.t5.layers import (
    RelativePositionalBiases,
    T5DecoderLayer,
    T5DecoderOnlyLayer,
    T5LayerNorm,
)
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput
from trix.utils.activations import SUPPORTED_FFN_ACTIVATIONS
from trix.utils.masking import build_causal_attention_mask


@dataclass
class T5DecoderConfig:
    """
    Parameters to initialize a T5Decoder model.

    Args:
        vocab_size: Token vocabulary, uniformly set to 32128 in pre-trained models.
        pad_token_id: Id for the pad token, which is 0 in the standard T5 vocabulary.
        eos_token_id: Id for the end of sentence token, which is 1 in the
            standard T5 vocabulary.
        embed_dim: Embedding dimension.
        attention_embed_dim: Embedding dimension in the attention layer, equal to
            num_heads*key_size. Most architectures have attention_embed_dim == embed_dim
            but some like flan-t5-small do not.
        ffn_embed_dim: Feed forward embedding dimension.
        num_heads: Number of attention heads.
        num_layers: Number of layers in the decoder.
        ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "gelu-no-approx", "relu", "swish".
        use_glu_in_ffn: Whether to use Gated Linear Unit (GLU) in Feed
            Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
            to True and use swish as ffn_activation_name.
            Same principle for a gated-relu.
        num_buckets: Number of positional bias buckets in the positional embeddings.
        max_bucket_distance: Maximum distance from a query that will start a positional
            bias bucket.
        output_len: Length of the outputs that will be generated during inference.
        dropout_rate: Fraction of embedding components set to 0 during dropout.
        logits_from_embed: Whether to obtain logits from embeddings in the decoder
            using the transpose of the token embeddings or using a new linear layer.
    """

    vocab_size: int = 32128
    pad_token_id: int = 0
    eos_token_id: int = 1

    # architecture
    embed_dim: int = 512
    attention_embed_dim: int = 512
    ffn_embed_dim: int = 2048
    num_heads: int = 8
    num_layers: int = 6

    ffn_activation_name: str = "relu"
    use_glu_in_ffn: bool = False

    num_buckets: int = 32
    max_bucket_distance: int = 128
    dropout_rate: float = 0.0
    logits_from_embed: bool = True

    def __post_init__(self) -> None:
        if self.attention_embed_dim % self.num_heads != 0:
            raise ValueError(
                "The attention embedding dimension should be "
                "divisible by the number of heads, however provided embedding "
                f"dimension is {self.attention_embed_dim} and the number of heads is "
                f"{self.num_heads}."
            )
        if self.ffn_activation_name not in SUPPORTED_FFN_ACTIVATIONS:
            raise ValueError(
                f"The ffn_activation_name must be one of {SUPPORTED_FFN_ACTIVATIONS}"
            )


class T5Decoder(hk.Module):
    """
    Creates a decoder stack for the T5 encoder-decoder architecture.  Applies a
    sequence of decoder layers to obtain a final set of output embeddings and logits.
    """

    def __init__(
        self,
        config: T5DecoderConfig,
        name: Optional[str] = None,
    ):
        """
        Initializes the Decoder stack for the T5 Encoder Decoder model.  Defaults to the
        hyperparameters used by T5_Small

        Args:
            config: Configuration data class for the Decoder.
            name: Optional name for this module.
        """
        super().__init__(name=name)

        self._config = config

        self.token_embedding = hk.Embed(
            vocab_size=config.vocab_size, embed_dim=config.embed_dim, name="token_embed"
        )

        self.position_bias = RelativePositionalBiases(
            num_buckets=config.num_buckets,
            num_heads=config.num_heads,
        )

        self.layers = [
            T5DecoderLayer(
                embed_dim=config.embed_dim,
                attention_embed_dim=config.attention_embed_dim,
                num_heads=config.num_heads,
                ffn_embed_dim=config.ffn_embed_dim,
                ffn_activation_name=config.ffn_activation_name,
                use_glu_in_ffn=config.use_glu_in_ffn,
            )
            for _ in range(config.num_layers)
        ]
        self.final_layer_norm = T5LayerNorm("final_layer_norm")

        # lm head
        self.lm_head_linear: Union[Callable, hk.Linear]

        # flan t5
        if config.logits_from_embed is True:
            lm_head_weight = jnp.transpose(self.token_embedding.embeddings) * rsqrt(
                config.embed_dim * 1.0
            )
            self.lm_head_linear = lambda x: (x @ lm_head_weight)
        # t5
        else:
            self.lm_head_linear = hk.Linear(
                output_size=config.vocab_size, with_bias=False, name="lm_head_linear"
            )

    def __call__(
        self,
        token_ids: Embedding,
        encoder_embeddings: Embedding,
        self_attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Compute decoder outputs based on decoder token embeddings and encoder output
        embeddings.

        Args:
            token_ids: Token embeddings fed to the decoder.  Takes shape (batch_size,
                seq_len).
            encoder_embeddings: Token embeddings entered into the decoder. Takes shape
                (batch_size, seq_len, embed_dim).
            self_attention_mask: Mask to be applied during self-attention, takes shape
                (batch_size, 1, seq_len, seq_len). If none is given, will default to
                causal attention.
            cross_attention_mask: Mask to be applied during cross-attention, takes shape
                (batch_size, 1, seq_len, seq_len). If none is given, will default
                to ones (since padding information comes from encoder tokens).
            dropout_rate: Fraction of components to be set to 0 during dropout.

        Returns:
            The final embeddings produced after sequential application of the decoder
            layers.
        """

        outs: TransformerOutput = {}

        batch_size, seq_len = token_ids.shape
        token_embeddings = self.token_embedding(ids=token_ids)

        x = hk.dropout(hk.next_rng_key(), dropout_rate, token_embeddings)

        position_bias = self.position_bias(
            query_length=seq_len,
            key_length=seq_len,
            bidirectional=False,
            max_distance=self._config.max_bucket_distance,
        )

        if self_attention_mask is None:
            # T5 decoder only takes in a causal mask. <pad> tokens are used to also
            # begin decoding.
            self_attention_mask = build_causal_attention_mask(
                batch_size=batch_size, seq_len=seq_len
            )

        if cross_attention_mask is None:
            cross_attention_mask = jnp.ones(batch_size, 1, seq_len, seq_len)

        for layer in self.layers:
            x = layer(
                x,
                encoder_embeddings,
                dropout_rate=dropout_rate,
                self_attention_mask=self_attention_mask,
                cross_attention_mask=cross_attention_mask,
                position_bias=position_bias,
            )

        x = self.final_layer_norm(x)
        x = hk.dropout(hk.next_rng_key(), dropout_rate, x)
        logits = self.lm_head_linear(x)
        outs["logits"] = logits
        return outs


class T5DecoderOnly(hk.Module):
    """
    Creates a decoder stack for the T5 decoder-only architecture \
    for a text-to-text task.
    """

    def __init__(
        self,
        config: T5DecoderConfig,
        name: Optional[str] = None,
    ):
        """
        Initializes the Decoder stack for the T5 Encoder Decoder model.  Defaults to the
        hyperparameters used by T5_Small

        Args:
            config: Configuration data class for the Decoder.
            name: Optional name for this module.
        """
        super().__init__(name=name)

        self._config = config

        self.token_embedding = hk.Embed(
            vocab_size=config.vocab_size, embed_dim=config.embed_dim, name="token_embed"
        )

        self.position_bias = RelativePositionalBiases(
            num_buckets=config.num_buckets,
            num_heads=config.num_heads,
        )

        self.layers = [
            T5DecoderOnlyLayer(
                embed_dim=config.embed_dim,
                attention_embed_dim=config.attention_embed_dim,
                num_heads=config.num_heads,
                ffn_embed_dim=config.ffn_embed_dim,
                ffn_activation_name=config.ffn_activation_name,
                use_glu_in_ffn=config.use_glu_in_ffn,
                name=f"layers_{idx}",
            )
            for idx in range(config.num_layers)
        ]
        self.final_layer_norm = T5LayerNorm("decoder_norm")

        # lm head
        self.lm_head_linear: Union[Callable, hk.Linear]

        # flan t5
        if config.logits_from_embed is True:
            lm_head_weight = jnp.transpose(self.token_embedding.embeddings) * rsqrt(
                config.embed_dim * 1.0
            )
            self.lm_head_linear = lambda x: (x @ lm_head_weight)
        # t5
        else:
            self.lm_head_linear = hk.Linear(
                output_size=config.vocab_size, with_bias=False, name="lm_head_linear"
            )

    def __call__(
        self,
        decoder_token_ids: Embedding,
        self_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Compute decoder outputs based on decoder token embeddings.

        Args:
            decoder_token_ids: Token embeddings fed to the decoder.
                Takes shape (batch_size, seq_len).
            self_attention_mask: Mask to be applied during self-
                attention. Takes shape (batch_size, 1, seq_len, seq_len).
                If None, will default to causal attention.
            dropout_rate: Fraction of components to be set to 0 during dropout.

        Returns:
            The final embeddings produced after sequential application of
            the decoder layers.
        """
        outs: TransformerOutput = {}

        batch_size, seq_len = decoder_token_ids.shape
        token_embeddings = self.token_embedding(ids=decoder_token_ids)
        x = hk.dropout(hk.next_rng_key(), dropout_rate, token_embeddings)

        position_bias = self.position_bias(
            query_length=seq_len,
            key_length=seq_len,
            bidirectional=False,
            max_distance=self._config.max_bucket_distance,
        )

        if self_attention_mask is None:
            # T5 decoder only takes in a causal mask. <pad> tokens are used to also
            # begin decoding.
            self_attention_mask = build_causal_attention_mask(
                batch_size=batch_size, seq_len=seq_len
            )

        for layer in self.layers:
            x = layer(
                x,
                dropout_rate=dropout_rate,
                self_attention_mask=self_attention_mask,
                position_bias=position_bias,
            )

        x = self.final_layer_norm(x)
        x = hk.dropout(hk.next_rng_key(), dropout_rate, x)
        logits = self.lm_head_linear(x)
        outs["logits"] = logits
        return outs


def build_t5_decoder(
    config: T5DecoderConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Builds a T5 decoder from the given configuration.

    Args:
        config: Configuration data class containing
            the hyperparameters for the T5 decoder
            forward function
        name: Optional name, will default to t5_decoder.

    Returns:
        T5 Decoder forward fn
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(T5Decoder, policy)

    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32, param_dtype=param_dtype, output_dtype=compute_dtype
    )
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)

    def t5_encoder_decoder(
        encoder_embeddings: Embedding,
        decoder_token_ids: Tokens,
        self_attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> jnp.ndarray:
        """
        Compute decoder outputs based on decoder token ids
        and encoder output embeddings.
        Args:
            encoder_embeddings: Token embeddings from encoder
                (batch size, seq len, embed_dim)
            decoder_token_ids: Token embeddings fed to the decoder
                (batch size, seq len)
            self_attention_mask: Mask to be applied during self-attention,
                takes shape (batch_size, 1, seq_len, seq_len).
                If none is given, will default to causal attention.
            cross_attention_mask: Mask to be applied during cross-attention, takes shape
                (batch_size, 1, seq_len, seq_len). If none is given, will default
                to ones (since padding information comes from encoder tokens).
            dropout_rate: Fraction of components to be set to 0 during dropout.
        Returns:
            Token logits (batch size, seq len, vocab size)

        """
        decoder = T5Decoder(config=config, name=name)
        decoder_outputs = decoder(
            token_ids=decoder_token_ids,
            encoder_embeddings=encoder_embeddings,
            self_attention_mask=self_attention_mask,
            cross_attention_mask=cross_attention_mask,
            dropout_rate=dropout_rate,
        )
        return decoder_outputs

    return t5_encoder_decoder


def build_t5_decoder_only(
    config: T5DecoderConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Builds a T5 decoder only model from the given configuration.
    Args:
        config: Configuration data class containing
            the hyperparameters for the T5 decoder
            forward function
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        name: Optional name, will default to t5_decoder.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(T5DecoderOnly, policy)

    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32,
        param_dtype=param_dtype,
        output_dtype=compute_dtype,
    )
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)

    def t5_decoder_only(
        decoder_token_ids: Tokens,
        decoder_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> jnp.ndarray:
        decoder = T5DecoderOnly(config=config, name=name)
        decoder_outputs = decoder(
            decoder_token_ids=decoder_token_ids,
            self_attention_mask=decoder_attention_mask,
            dropout_rate=dropout_rate,
        )
        return decoder_outputs

    return t5_decoder_only
