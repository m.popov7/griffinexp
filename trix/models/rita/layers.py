"""GPT layers specific to RITA.

See trix.models.rita.model for more information.
"""
from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp

from trix.models.gpt.layers import GptDecoderLayer, GptMultiHeadAttention
from trix.utils.activations import get_activation_fn


class RITADecoderLayer(GptDecoderLayer):
    """
    Single layer in the encoder, including self-attention and feed-forward operations.
    The feed-forward network uses a ReLU activation and has no biases.
    """

    def __init__(
        self,
        embed_dim: int,
        ffn_embed_dim: int,
        num_heads: int,
        rotary_dim: Optional[int],
        max_position_embeddings: int,
        norm_type: str,
        parallel_attention_ff: bool,
        add_bias_ffn: bool,
        ffn_activation_name: str,
        use_glu_in_ffn: bool,
        name: Optional[str] = None,
        add_bias_attn: bool = False,
    ):
        """
        Initializes the encoder layer, including the projections needed for
        self-attention and the linear layers applied in the fully connected portion

        Args:
            embed_dim: Dimension of the embeddings
            ffn_embed_dim: Dimension of the hidden layer in the MLP
            num_heads: Number of independent attention heads.
            rotary_dim: The dimension in key space to apply the rotary positional
                embeddings
            max_position_embeddings: The maximum length to apply rotary positional
                embeddings
            norm_type: The type of norm used ( pre normalization scheme ) used. can be
                one of ["layer_norm", "RMS_norm"]
            parallel_attention_ff: Whether to do the attention and the MLP in parallel,
                and then sum up the results as it is done in GPT-NeoX :
                Black, Sid, et al. "Gpt-neox-20b: An open-source autoregressive
                language model." arXiv preprint arXiv:2204.06745 (2022).
                It is said to improve the training time of 15% when compiling with JAX
            add_bias_ffn: Add bias in feed forward network block.
            ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "gelu-no-approx", "relu", "swish", and "silu"
            use_glu_in_ffn: Whether to use Gated Linear Unit (GLU) in Feed
                Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
                to True and use swish as ffn_activation_name.
                Same principle for a gated-relu.
            name: Optional name for this module.
            add_bias_attn: Add bias to the attention mechanism (key, query, value, and
                output projections).
        """
        # RITA-SPECIFIC - we cannot call super() here since GptDecoderLayer creates its
        # own set of layers.
        hk.Module.__init__(
            self,
            name=name,
        )

        self.num_heads = num_heads
        self.parallel_attention_ff = parallel_attention_ff
        # RITA-SPECIFIC - replaced GptMultiHeadAttention
        self.sa_layer = RITAMultiHeadAttention(
            embed_dim=embed_dim,
            num_heads=num_heads,
            name="self_attn",
            rotary_dim=rotary_dim,
            max_position_embeddings=max_position_embeddings,
            add_bias_attn=add_bias_attn,
        )

        if norm_type == "layer_norm":
            self.attn_norm = hk.LayerNorm(
                axis=-1, create_scale=True, create_offset=True, name="attn_layer_norm"
            )
            if not (self.parallel_attention_ff):
                self.ffn_norm = hk.LayerNorm(
                    axis=-1,
                    create_scale=True,
                    create_offset=True,
                    name="ffn_layer_norm",
                )
        elif norm_type == "RMS_norm":
            self.attn_norm = hk.RMSNorm(
                axis=-1, create_scale=True, name="attn_RMS_norm", eps=1e-6
            )
            if not (self.parallel_attention_ff):
                self.ffn_norm = hk.RMSNorm(
                    axis=-1, create_scale=True, name="ffn_RMS_norm", eps=1e-6
                )
        else:
            raise ValueError(f"unrecognized norm_type : {norm_type}")

        # Get ffn activation function
        self._ffn_activation_fn = get_activation_fn(activation_name=ffn_activation_name)
        self._use_glu_in_fnn = use_glu_in_ffn

        # Define layers
        if use_glu_in_ffn:
            # user should multiply ffn_embed_dim by 2/3 when using GLU
            # to keep total number of parameters equal
            # see https://arxiv.org/pdf/2002.05202.pdf. for more details
            # we multiply by 2 here as the output will be split in 2 for GLU
            ffn_embed_dim = int(2 * ffn_embed_dim)

        self.fc1_linear = hk.Linear(
            output_size=ffn_embed_dim,
            with_bias=add_bias_ffn,
            name="fc1_linear_glu" if use_glu_in_ffn else "fc1_linear",
        )
        self.fc2_linear = hk.Linear(
            output_size=embed_dim, with_bias=add_bias_ffn, name="fc2_linear"
        )


# RITA-SPECIFIC - RoPE implementation
def rita_rotate_every_two(attention_tensor: jnp.ndarray) -> jnp.ndarray:
    """
    Prepare a tensor to apply the RoPE mechanism.

    Note that this is the version of `rotate_every_two` used in RITA:
    https://huggingface.co/lightonai/RITA_s/blob/fced662eadd2b7099a3b92a88365dfc3c98eb3da/rita_modeling.py#L35
    The `rotate_half` function on line 35 linked above shows that in RITA, the rotation
    is done by concatenating two halves of the sequence, so an input such as
    [1,2,3,4,5,6] becomes [-6,-5,-4,1,2,3]. However, in a proper implementation of RoPE,
    the output should actually be [-2,1,-4,3,-6,5].

    See here for more info: https://gitlab.com/instadeep/trix/-/merge_requests/235

    Args:
        attention_tensor: Tensor of shape (batch_size, seq_len, num_heads, key_dim)
            It is in fact a key of query tensor

    Returns:
        Takes the second half of the tensor, flips its sign, and moves it to the front
        of the tensor.
        tensor size : (batch_size, seq_len, num_heads, key_dim)
    """
    halfway_idx = attention_tensor.shape[-1] // 2
    first_half = attention_tensor[..., :halfway_idx]
    second_half = attention_tensor[..., halfway_idx:]
    return jnp.concatenate((-second_half, first_half), axis=-1)


# RITA-SPECIFIC - RoPE implementation
def rita_apply_rotary_pos_emb(
    attention_tensor: jnp.ndarray, sincos: jnp.ndarray
) -> jnp.ndarray:
    """
    Apply the RoPE to attention_tensor.

    This is the version of RoPE used in RITA here:
    https://huggingface.co/lightonai/RITA_s/blob/fced662eadd2b7099a3b92a88365dfc3c98eb3da/rita_modeling.py#L61
    Note that this version of RoPE is incorrect compared to the original RoPE. Line 61
    linked above shows that `emb` is created by concatenating `freqs` with `freqs`, so a
    sequence such as [1,2,3] becomes [1,2,3,1,2,3]. However, a correct RoPE
    implementation actually repeats each value (in PyTorch, the appropriate function is
    `repeat_interleave`); i.e., the output should be [1,1,2,2,3,3].

    See here for more info: https://gitlab.com/instadeep/trix/-/merge_requests/235

    Args:
        attention_tensor: Tensor of shape (batch_size, seq_len, num_heads, key_dim)
            It is in fact a key of query tensor
        sincos: the sincos generated by the function 'create_sinusoidal_positions'
            shape :

    Returns:
        the corresponding RoPE-encoded tensor, following the RITA implementation
    """
    sin_pos, cos_pos = sincos
    sin_pos = jnp.concatenate((sin_pos, sin_pos), -1)[:, :, None, :]
    cos_pos = jnp.concatenate((cos_pos, cos_pos), -1)[:, :, None, :]
    return (attention_tensor * cos_pos) + (
        rita_rotate_every_two(attention_tensor) * sin_pos
    )


class RITAMultiHeadAttention(GptMultiHeadAttention):
    """
    Multi-head attention with masking applied. Modified from Haiku implementation to
    be able to support relative positional embeddings.  Computes the keys, queries, and
    values from the input embeddings.  Future versions could compute and store these
    to prevent redundant calculations during autoregressive inference. The keys, queries
    , and value sizes are fixed to be embed_dim/num_heads in accordance with the
    standard GPT model.
    """

    def __call__(
        self,
        query_inputs: jnp.ndarray,
        key_inputs: jnp.ndarray,
        value_inputs: jnp.ndarray,
        attention_mask: Optional[jnp.ndarray],
    ) -> jnp.ndarray:
        """
        Computes the result of multiheaded dot-product attention, using
        pre-computed projections for the queries, keys, and values.

        Args:
            query_inputs: Embeddings that will be projected to become the queries.
            key_inputs: Embeddings that will be projected to become the keys.
            value_inputs: Embeddings that will be projected to become the values.
            attention_mask: Mask to be applied in the attention layers.
                Triangular for autoregressive models.
                shape : (1, 1, seq_len, seq_len)

        Returns:
            The standard output of multi-headed attention
        """
        position_ids = jnp.arange(0, key_inputs.shape[1], 1, dtype=jnp.int32)
        position_ids = jnp.expand_dims(position_ids, 0).repeat(key_inputs.shape[0], 0)

        keys = self.key_linear(key_inputs)
        queries = self.query_linear(query_inputs)
        values = self.value_linear(value_inputs)

        keys = keys.reshape(keys.shape[0], keys.shape[1], self.num_heads, -1)
        queries = queries.reshape(
            queries.shape[0], queries.shape[1], self.num_heads, -1
        )
        values = values.reshape(values.shape[0], values.shape[1], self.num_heads, -1)

        sincos = jnp.take(self.get_sincos_positions(), position_ids, axis=0)
        sincos = jnp.split(sincos, 2, axis=-1)

        if self.rotary_dim is not None:
            k_rot = keys[:, :, :, : self.rotary_dim]
            k_pass = keys[:, :, :, self.rotary_dim :]

            q_rot = queries[:, :, :, : self.rotary_dim]
            q_pass = queries[:, :, :, self.rotary_dim :]

            # RITA-SPECIFIC - calling the RoPE implementation
            k_rot = rita_apply_rotary_pos_emb(k_rot, sincos)
            q_rot = rita_apply_rotary_pos_emb(q_rot, sincos)

            keys = jnp.concatenate([k_rot, k_pass], axis=-1)
            queries = jnp.concatenate([q_rot, q_pass], axis=-1)
        else:
            # RITA-SPECIFIC - calling the RoPE implementation
            keys = rita_apply_rotary_pos_emb(keys, sincos)
            queries = rita_apply_rotary_pos_emb(queries, sincos)

        attention_logits = jnp.einsum("...thd,...Thd->...htT", queries, keys)
        sqrt_key_size = jnp.sqrt(keys.shape[-1]).astype(queries.dtype)
        attention_logits = attention_logits / sqrt_key_size

        attention_logits = jnp.where(attention_mask, attention_logits, -1e30)

        attention_weights = jax.nn.softmax(attention_logits, axis=-1)

        values = jnp.einsum("...htT,...Thd->...thd", attention_weights, values)
        values = jnp.reshape(values, (values.shape[0], values.shape[1], -1))

        return self.out_linear(values)
