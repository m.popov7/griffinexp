"""GPT model specific to RITA.

In RITA (https://huggingface.co/lightonai/RITA_s/), the authors make several bugs in
their implementation of RoPE (rotary positional embeddings); see here for details:
https://gitlab.com/instadeep/trix/-/merge_requests/235

While we could add a flag to support this behavior in the GPT models in Trix, this
behavior is highly specific to RITA and is actually incorrect, so doing so would pollute
the GPT code. We have thus decided to create a RITADecoder, RITADecoderLayer, and
RITAMultiHeadAttention that each inherit from GptDecoder, GptDecoderLayer, and
GptMultiHeadAttention, respectively. These classes thus maintain the same behavior as
the GPT model, except for the change in how RoPE is computed. We also duplicate the
build_gpt_fn into build_rita_fn.

Even though we inherit from the GPT classes, we still had to duplicate several functions
in order to change their implementation to use the RITA classes and RITA RoPE functions.
Throughout this file, we have added the comment "RITA-SPECIFIC" wherever we made a
change to the original GPT code.

Finally, we also add additional functionality for operating with RITA, such as soft
prompting. These sections of code are also marked with RITA-SPECIFIC.
"""
import logging
from typing import Callable, Optional

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.models.gpt.model import GptConfig, GptDecoder
from trix.models.rita.layers import RITADecoderLayer
from trix.types import Tokens, TransformerOutput
from trix.utils.logging import debug_log_tensor

logger = logging.getLogger(__name__)


class RITADecoder(GptDecoder):
    @hk.experimental.name_like("__call__")
    def decoder_layer(self, layer_idx: int) -> RITADecoderLayer:
        """
        Returns the GPT encoder layer.

        Args:
            layer_idx: the layer index

        Returns:
            The named RITADecoderLayer
        """
        # RITA-SPECIFIC - replaced GptDecoderLayer
        return RITADecoderLayer(
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            num_heads=self._config.num_heads,
            rotary_dim=self._config.rope_dimensions,
            max_position_embeddings=self._config.max_position_embeddings,
            norm_type=self._config.norm_type,
            parallel_attention_ff=self._config.parallel_attention_ff,
            add_bias_ffn=self._config.add_bias_ffn,
            ffn_activation_name=self._config.ffn_activation_name,
            use_glu_in_ffn=self._config.use_glu_in_ffn,
            name=f"gpt_decoder_layer_{layer_idx}",
            add_bias_attn=self._config.add_bias_attn,
        )

    # RITA-SPECIFIC - added soft prompting
    def __call__(
        self,
        token_ids: Tokens,
        attention_mask: Optional[jnp.ndarray] = None,
        soft_prompt: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        """
        Compute the logits and embeddings from a sequence of tokens

        Args:
            token_ids: Sequence of token ids delivered to the decoder of shape
                (batch_size, seq_len)
            attention_mask: Optional attention mask to allow for stacked examples or
                other non-standard behavior. Otherwise, will create default causal mask.
            soft_prompt: A (batch_size, soft_prompt_len, embed_dim) array of embeddings
                to prepend to the transformer inputs as a soft prompt.

        Returns:
             The logits over the token vocabulary at each time step.
        """
        debug_log_tensor("Tokens Ids", token_ids, logger=logger)

        # (batch_size,seq_len) -> (batch_size,seq_len,embed_dim)
        tokens_embeddings = self.token_embed(token_ids)
        debug_log_tensor("Tokens embeddings", tokens_embeddings, logger=logger)

        if soft_prompt is None:
            # (batch_size,0,embed_dim)
            soft_prompt = jnp.empty(
                (tokens_embeddings.shape[0], 0, tokens_embeddings.shape[2])
            )

        # Concatenate soft prompt.
        # (batch_size,seq_len,embed_dim) ->
        # (batch_size,soft_prompt_len+seq_len,embed_dim)
        tokens_embeddings = jnp.concatenate((soft_prompt, tokens_embeddings), axis=1)

        # (batch_size,seq_len,embed_dim) -> (batch_size,seq_len,embed_dim)
        embeddings = self.apply_transformer_layers(tokens_embeddings, attention_mask)
        debug_log_tensor("Transformer stack output", embeddings, logger=logger)
        embeddings = self.final_norm(embeddings)

        # Get outputs
        outs = {}
        outs["embeddings"] = embeddings
        debug_log_tensor("LM head input", embeddings, logger=logger)

        # Compute logits
        logits = self.lm_head(embeddings)["logits"]
        debug_log_tensor("Logits", logits, logger=logger)

        outs["logits"] = logits

        return outs  # type: ignore


def build_rita_fn(
    config: GptConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Create the model's forward pass.

    Args:
        config: Configuration data class containing the hyperparameters for the GPT
            forward function.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.
        name: the name of the model. example: gpt_j_decoder.


        # NOTE: in inference, the model could be in fp16 without too much degradation
        # NOTE: on NVIDIA accelerator, XLA inter-device operation ( psum, all_gather,
            etc ... ) are not always implemented for bf16. but on TPU hardware yes

    Returns:
        RITA Decoder model forward function.
    """

    assert {compute_dtype, param_dtype, output_dtype}.issubset(
        {
            jnp.bfloat16,
            jnp.float32,
            jnp.float16,
        }
    ), f"provide a dtype in {jnp.bfloat16, jnp.float32, jnp.float16}"

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    # RITA-SPECIFIC - replaced GptDecoder
    hk.mixed_precision.set_policy(RITADecoder, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32, param_dtype=jnp.float32, output_dtype=compute_dtype
    )
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.RMSNorm, norm_policy)

    def rita_fn(
        token_ids: jnp.ndarray, soft_prompt: jnp.ndarray = None
    ) -> TransformerOutput:
        # RITA-SPECIFIC - replaced GptDecoder and added soft-prompting
        model = RITADecoder(config, name=name)

        # mypy mistakes `model` for a GPTDecoder although it is clearly a RITADecoder.
        return model(  # type: ignore[call-arg]
            token_ids=token_ids,
            soft_prompt=soft_prompt,
        )

    return rita_fn
