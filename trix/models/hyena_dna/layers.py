from typing import Dict, Optional, Tuple, Union

import haiku as hk
import jax
import jax.numpy as jnp

from trix.utils.activations import get_activation_fn


class PositionalEmbedding(hk.Module):
    """Defines complex exponential positional embeddings used in Hyena filters."""

    def __init__(self, emb_dim: int, seq_len: int, name: Optional[str] = None):
        """
        Args:
            emb_dim: Positional embedding dimension
            seq_len: Number of positions to embed
            name: Optional name of the module
        """
        super().__init__(name=name)

        self.seq_len = seq_len
        self.emb_dim = emb_dim

        # The time embedding fed to the filters is normalized so that t_f = 1
        self.t = jnp.linspace(0, 1, self.seq_len).reshape(1, self.seq_len, 1)  # 1, L, 1
        self.z = hk.get_parameter(
            "z",
            shape=(1, self.seq_len, self.emb_dim),
            dtype=jnp.float32,
            init=self._init_z,
        )

    def _init_z(self, shape: Tuple[int], dtype: jnp.dtype) -> jnp.ndarray:
        if self.emb_dim > 1:
            bands = (self.emb_dim - 1) // 2

        # To compute the right embeddings we use the "proper" linspace
        t_rescaled = jnp.linspace(0, self.seq_len - 1, self.seq_len)[None, :, None]
        w = 2 * jnp.pi * t_rescaled / self.seq_len  # 1, L, 1

        f = jnp.linspace(1e-4, bands - 1, bands)[None, None]
        z = jnp.exp(-1j * f * w)
        z = jnp.concatenate([self.t, z.real, z.imag], axis=-1)

        return z

    def __call__(self, seq_len: jnp.ndarray) -> Tuple[jnp.ndarray, jnp.ndarray]:
        return self.z[:, :seq_len], self.t[:, :seq_len]


class ExponentialModulation(hk.Module):
    """Applies exponential decay to the implicit hyena filters of the hyena operator."""

    def __init__(
        self,
        embed_dim: int,
        fast_decay_pct: float = 0.3,
        slow_decay_pct: float = 1.5,
        target: float = 1e-2,
        shift: float = 0.05,
        name: Optional[str] = None,
    ):

        """
        Initializes the exponential modulation layer.

        Args:
            embed_dim: Embedding dimension of the filter which the modulation will be
            applied to
            fast_decay_pct: Fast decay value to determine upper bound
            slow_decay_pct: Slow decay value to determine lower bound
            target: The target decay value rate
            shift: Bias applied to the decayed vector
            name: Optional name of the module
        """
        super().__init__(name=name)

        self.shift = shift
        self.embed_dim = embed_dim

        max_decay = jnp.log(target) / fast_decay_pct
        min_decay = jnp.log(target) / slow_decay_pct

        self.deltas = jnp.linspace(min_decay, max_decay, self.embed_dim)[None, None]

    def __call__(self, t: jnp.ndarray, x: jnp.ndarray) -> jnp.ndarray:
        decay = jnp.exp(-t * jnp.abs(self.deltas))
        x = x * (decay + self.shift)
        return x


class HyenaFilter(hk.Module):
    """Defines the hyena filter which is composed of a positional embedding layer,
    a mlp, and exponential modulation.
    """

    def __init__(
        self,
        filter_embed_dim: int,
        filter_positional_embed_dim: int = 3,
        implicit_filter_mlp_dim: int = 16,
        seq_len: int = 1024,
        frequency: int = 1,
        num_inner_mlps: int = 2,
        train_freq: bool = True,
        name: Optional[str] = None,
    ):
        """
        Initializes the hyena filter.

        Args:
            filter_embed_dim: Number of channels in the filter.
            filter_positional_embed_dim: Dimension of the positional embedding layer.
            implicit_filter_mlp_dim: Dimension used as hidden state in mlp.
            seq_len: Sequence length used in positional embedding layer.
            frequency: Frequency of the periodic sin activations.
            num_inner_mlps: Number of inner mlp's.
            train_freq: Whether to treat the frequency as parameter
            name: Optional name of the module.
        """
        super().__init__(name=name)

        self.filter_embed_dim = filter_embed_dim
        self.filter_positional_embed_dim = filter_positional_embed_dim

        self.seq_len = seq_len
        self.num_inner_mlps = num_inner_mlps

        self.bias = hk.get_parameter(
            "bias",
            shape=(self.filter_embed_dim,),
            dtype=jnp.float32,
            init=hk.initializers.RandomNormal(),
        )

        self.sin_activation_fn = get_activation_fn("sin")

        # get frequencies for sin activation
        self.frequencies = []
        for i in range(num_inner_mlps + 1):
            if train_freq:
                freq = hk.get_parameter(
                    f"freq_{i}",
                    shape=(1, implicit_filter_mlp_dim),
                    init=hk.initializers.Constant(frequency),
                )
            else:
                freq = frequency * jnp.ones((1, implicit_filter_mlp_dim))

            self.frequencies.append(freq)

        if not (
            filter_positional_embed_dim % 2 != 0 and filter_positional_embed_dim >= 3
        ):
            raise ValueError(
                "positional emb_dim must be odd and greater or equal to 3 (time, "
                "sine and cosine)"
            )

        # define positional embedding layer
        self.positional_embedding = PositionalEmbedding(
            filter_positional_embed_dim, seq_len
        )

        # define implicit filters
        self.implicit_filters = []
        for _ in range(num_inner_mlps + 1):
            self.implicit_filters.append(hk.Linear(implicit_filter_mlp_dim))

        self.implicit_filters.append(hk.Linear(filter_embed_dim, with_bias=False))

        # exponential modulation layer
        self.modulation = ExponentialModulation(
            filter_embed_dim,
        )

    def get_long_conv_filters(self, seq_len: jnp.ndarray) -> jnp.ndarray:
        """
        Returns the long convolutional filter for the input sequence length.
        """

        z, t = self.positional_embedding(seq_len)  # 1 x L x 1, 1 x L x 1
        h = z

        for i, mlp in enumerate(self.implicit_filters):
            h = mlp(h)
            if i != self.num_inner_mlps + 1:
                h = self.sin_activation_fn(h * self.frequencies[i])

        h = self.modulation(t, h)  # 1 x L x (model_embed_dim * (order - 1))

        return h

    def fftconv(
        self, embedding: jnp.ndarray, conv_filter: jnp.ndarray, bias: jnp.ndarray
    ) -> jnp.ndarray:
        """
        embedding.shape: B x L x D,
        conv_filter.shape: L x D
        bias.shape: D

        Convolution through the fourier domain (from the Convolution Theorem).
        """
        seq_len = embedding.shape[1]
        fft_size = 2 * seq_len

        conv_filter_fourier = (
            jnp.fft.rfft(
                conv_filter,
                n=fft_size,
                axis=0,
            )
            / fft_size
        )
        embedding_fourier = jnp.fft.rfft(
            embedding,
            n=fft_size,
            axis=1,
        )

        conv_output = jnp.fft.irfft(
            embedding_fourier
            * conv_filter_fourier,  # B x L x D, I hope casting goes well
            n=fft_size,
            norm="forward",
            axis=1,
        )[
            :, :seq_len
        ]  # B x L x D

        out = conv_output + embedding * bias
        # out = embedding * conv_filter

        return out

    def __call__(
        self,
        embeddings: jnp.ndarray,
        seq_len: int,
        conv_filter: Optional[jnp.ndarray] = None,
        bias: Optional[jnp.ndarray] = None,
    ) -> jnp.ndarray:
        if conv_filter is None:
            conv_filter = self.get_long_conv_filters(seq_len)

        # Ensure compatibility with filters that return a tuple
        conv_filter = conv_filter[0] if type(conv_filter) is tuple else conv_filter

        conv_output = self.fftconv(embeddings, conv_filter, bias)
        return conv_output


class HyenaOperator(hk.Module):

    """
    Defines the hyena operator which is composed of a projection layer,
    short convolution, long convolution (of order specified in init), and final output
    projection layer.
    """

    def __init__(
        self,
        model_embed_dim: int,
        max_seq_len: int,
        order: int = 2,
        implicit_filter_mlp_dim: int = 64,
        filter_positional_embed_dim: int = 3,
        dropout: float = 0.0,
        sin_frequency: int = 10,
        name: Optional[str] = None,
    ):
        """
        Initializes hyena operator.

        Args:
            model_embed_dim: Dimension of the input and output embeddings
            max_seq_len: Maximum input sequence length.
            order: Depth of the Hyena recurrence.
            implicit_filter_mlp_dim: Inner dimension of the mlp's in the hyena filter
            module.
            filter_positional_embed_dim: Size of the positional embedding in the
            hyena filter module.
            dropout: Dropout probability.
            sin_frequency: frequency applied in sin activations of hyena filter
            implicit filters
        """
        super().__init__(name=name)

        self.model_embed_dim = model_embed_dim
        self.max_seq_len = max_seq_len
        self.order = order
        self.dropout_rate = dropout

        inner_width = model_embed_dim * (order + 1)

        # in and out projection layers
        self.in_proj = hk.Linear(inner_width)
        self.out_proj = hk.Linear(model_embed_dim)

        # 1D conv
        self.short_filter = hk.Conv1D(
            output_channels=inner_width,
            kernel_shape=3,
            padding=((2, 2)),
            feature_group_count=inner_width,
            data_format="NWC",
            # data_format="NCW",
        )  # input format:  B x L x (D * (order + 1))
        # ( for small 32k: B, 32k, 1024 )

        # hyena filter
        self.hyena_filter = HyenaFilter(
            filter_embed_dim=model_embed_dim * (order - 1),
            filter_positional_embed_dim=filter_positional_embed_dim,
            implicit_filter_mlp_dim=implicit_filter_mlp_dim,
            seq_len=max_seq_len,
            frequency=sin_frequency,
        )

    def __call__(self, embeddings: jnp.ndarray, is_training: bool) -> jnp.ndarray:
        # embeddings.shape = B x L x D
        seq_len = embeddings.shape[-2]
        l_filter = min(seq_len, self.max_seq_len)

        embeddings = self.in_proj(embeddings)  # B x L x (D * (order + 1))
        u = self.short_filter(embeddings)[:, :l_filter]  # B x L x (D * (order + 1))

        *x, v = jnp.split(
            u, u.shape[-1] / self.model_embed_dim, axis=-1
        )  # v.shape : B x L x D

        k = self.hyena_filter.get_long_conv_filters(l_filter)[0]

        k = jnp.reshape(
            k, (seq_len, self.order - 1, -1)
        )  # L x (order - 1) x model_embed_dim

        bias = jnp.reshape(
            self.hyena_filter.bias, (self.order - 1, -1)
        )  # (order - 1) x model_embed_dim

        for i, x_i in enumerate(reversed(x[1:])):
            if is_training and self.dropout_rate:
                v = hk.dropout(hk.next_rng_key(), self.dropout_rate, v * x_i)
            else:
                v = v * x_i
            v = self.hyena_filter(
                embeddings=v, seq_len=l_filter, conv_filter=k[:, i], bias=bias[i]
            )  # v.shape: B x L x D, l_filter.shape: L x D

        y = self.out_proj(v * x[0])

        return y


class Mlp(hk.Module):
    """
    The mlp applied after the mixer layer (HyenaOperator).
    """

    def __init__(
        self,
        in_features: int,
        hidden_features: Optional[int] = None,
        out_features: Optional[int] = None,
        activation: jax.nn = jax.nn.gelu,
        return_residual: bool = False,
        initializer_range: float = 0.2,
        name: Optional[str] = None,
    ):

        """
        Initializes the mlp of the hyena block.

        Args:
          in_features: Number of features of the embedding fed as input to the mlp
          hidden_features: Number of features of the hidden states of the mlp
          out_features: Number of features of the output embedding of the mlp
          activation: Activation function in the mlp
          return_residual: Whether to return the input in the call function
          initializer_range: Standard deviation for weight initialization
          name: Optional name for this module.
        """

        super().__init__(name=name)

        w_init = hk.initializers.RandomNormal(stddev=initializer_range)
        b_init = jnp.zeros

        out_features = out_features or in_features
        hidden_features = hidden_features or in_features

        self.return_residual = return_residual
        self.fc1 = hk.Linear(hidden_features, w_init=w_init, b_init=b_init)
        self.activation = activation
        self.fc2 = hk.Linear(out_features, w_init=w_init, b_init=b_init)

    def __call__(self, x: jnp.ndarray) -> jnp.ndarray:
        y = self.fc1(x)
        y = self.activation(y)
        y = self.fc2(y)
        return y if not self.return_residual else (y, x)


class Block(hk.Module):
    """Defines a hyena block which is composed of a Hyena Operator followed by an
    MLP."""

    def __init__(
        self,
        model_embed_dim: int,
        hyena_operator_config: Dict[str, Union[int, bool, str]],
        mlp_dim: Optional[int] = None,
        resid_dropout1: float = 0.0,
        resid_dropout2: float = 0.0,
        return_residual: bool = False,
        layer_norm_epsilon: float = 1e-5,
        use_gradient_checkpointing_mixer: bool = False,
        use_gradient_checkpointing_mlp: bool = False,
        name: Optional[str] = None,
    ):
        """
        Initializes a Hyena Block.

        Args:
            model_embed_dim: Number of features of the token embeddings.
            hyena_operator_config: Hyena operator configuration dictionary.
            mlp_dim: Dimension of the hidden state in the mlp of the block.
            prenorm: Whether to apply the layer norm before or after the hyena operator.
            resid_dropout1: Dropout rate for dropout layer applied to the hidden_states
            passed as input to the call function
            resid_dropout2: Dropoute rate for drooput layer applied to the
            hidden_states returned by the hyena operator.
            return_residual: Whether to return the residual hidden states
            layer_norm_epsilon: Epsilon used in layer norm to prevent division by zero.
            name: Optional name for this module.
        """
        super().__init__(name=name)

        self.return_residual = return_residual

        self.mixer = HyenaOperator(
            model_embed_dim, **hyena_operator_config  # type:ignore
        )
        if use_gradient_checkpointing_mixer:
            self.mixer = hk.remat(self.mixer, static_argnums=(1,))

        if mlp_dim is None:
            mlp_dim = 4 * model_embed_dim
        self.mlp = Mlp(in_features=model_embed_dim, hidden_features=mlp_dim)

        if use_gradient_checkpointing_mlp:
            self.mlp = hk.remat(self.mlp)

        # dropout layers
        self.resid_dropout1 = resid_dropout1
        self.resid_dropout2 = resid_dropout2

        # layer norms
        self.norm1 = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True, eps=layer_norm_epsilon
        )
        self.norm2 = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True, eps=layer_norm_epsilon
        )

    def __call__(
        self,
        hidden_states: jnp.ndarray,
        residual: Optional[jnp.ndarray] = None,
        is_training: bool = False,
    ) -> Union[jnp.ndarray, Tuple[jnp.ndarray, jnp.ndarray]]:

        if is_training and self.resid_dropout1:
            dropped = hk.dropout(hk.next_rng_key(), self.resid_dropout1, hidden_states)
        else:
            dropped = hidden_states

        residual = (dropped + residual) if residual is not None else dropped
        hidden_states = self.norm1(residual)

        hidden_states = self.mixer(hidden_states, is_training)

        if is_training and self.resid_dropout2:
            dropped = hk.dropout(hk.next_rng_key(), self.resid_dropout2, hidden_states)
        else:
            dropped = hidden_states

        residual = (dropped + residual) if residual is not None else dropped
        hidden_states = self.norm2(residual)

        hidden_states = self.mlp(hidden_states)

        return hidden_states, residual


class TokenEmbeddings(hk.Module):
    def __init__(
        self,
        model_embed_dim: int,
        vocab_size: int,
        word_embed_proj_dim: Optional[int] = None,
        name: Optional[str] = None,
    ) -> None:

        """
        Initializes a token embedding layer.

        Args:
            model_embed_dim: Size which to embed each token.
            vocab_size: The size of the vocabulary which the model is trained.
            word_embed_proj_dim: Optionally, project token embedding to this dimension
            name: Optional name for this module.
        """

        super().__init__(name=name)

        if word_embed_proj_dim is None:
            self.word_embeddings = hk.Embed(
                vocab_size,
                model_embed_dim,
            )
            self.project_in = None
        else:
            self.word_embeddings = hk.Embed(vocab_size, word_embed_proj_dim)
            self.project_in = hk.Linear(model_embed_dim, with_bias=False)

    def __call__(self, token_ids: jnp.ndarray) -> jnp.ndarray:

        token_embeddings = self.word_embeddings(token_ids)

        if self.project_in is not None:
            token_embeddings = self.project_in(token_embeddings)

        return token_embeddings


class LMBackbone(hk.Module):
    def __init__(
        self,
        model_embed_dim: int,
        num_hyena_blocks: int,
        vocab_size: int,
        hyena_operator_config: Dict[str, Union[int, bool, str]],
        mlp_dim: Optional[int] = None,
        resid_dropout: float = 0.0,
        embed_dropout: float = 0.1,
        layer_norm_epsilon: float = 1e-5,
        use_gradient_checkpointing_mixer: bool = False,
        use_gradient_checkpointing_mlp: bool = False,
        name: Optional[str] = None,
    ) -> None:

        """
        Initializes hyena lm backbone which is a token embedding layer and a series
        of hyena blocks.

        Args:
            model_embed_dim: Size which to embed each token.
            num_hyena_blocks: Number of hyena blocks in the backbone.
            vocab_size: The size of the vocabulary which the model is trained.
            hyena_operator_config: Hyena operator layer config
            mlp_dim: Dimension of the hidden state in the mlp of hyena block.
            use_gradient_checkpointing_mixer: enable grad checkpointing for the mixer
                block
            use_gradient_checkpointing_mlp: enable grad checkpointing for the mlp block
            name: Optional name for this module.
        """

        super().__init__(name=name)

        self.embeddings = TokenEmbeddings(model_embed_dim, vocab_size)

        self.hyena_blocks = [
            Block(
                model_embed_dim=model_embed_dim,
                mlp_dim=mlp_dim,
                hyena_operator_config=hyena_operator_config,
                resid_dropout1=embed_dropout if i == 0 else resid_dropout,
                resid_dropout2=resid_dropout,
                use_gradient_checkpointing_mixer=use_gradient_checkpointing_mixer,
                use_gradient_checkpointing_mlp=use_gradient_checkpointing_mlp,
            )
            for i in range(num_hyena_blocks)
        ]  # type:ignore

        self.drop_f = resid_dropout

        self.layer_norm = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True, eps=layer_norm_epsilon
        )

    def __call__(
        self, token_ids: jnp.ndarray, is_training: bool = False
    ) -> jnp.ndarray:

        hidden_states = self.embeddings(token_ids)
        residual = None

        for block in self.hyena_blocks:
            hidden_states, residual = block(
                hidden_states, residual, is_training=is_training
            )

        if (
            is_training and self.drop_f
        ):  # only use dropout if rate is not 0 and training
            dropped = hk.dropout(hk.next_rng_key(), self.drop_f, hidden_states)
            residual = (dropped + residual) if residual is not None else dropped
        else:
            residual = (
                (hidden_states + residual) if residual is not None else hidden_states
            )

        final_embeddings = self.layer_norm(residual)

        return final_embeddings
