from dataclasses import dataclass
from typing import Callable, Dict, Optional

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.models.hyena_dna.layers import LMBackbone


@dataclass
class HyenaDNAConfig:

    # vocab attributes
    vocab_size: int

    # model dimensionality
    model_embed_dim: int  # size of embeddings after token embedding and output
    # of each
    mlp_dim: int  # hidden dimension of mlp in hyena block
    num_hyena_blocks: int  # number of hyena blocks in the lm backbone

    # vocab attributes
    pad_vocab_size_multiple: Optional[int] = None  # whether to pad the vocab size to a
    # multiple
    word_embed_proj_dim: Optional[int] = None  # optional smaller token embedding dim
    # which then are projected to model_embed_dim

    # dropout
    embed_dropout: float = 0.0  # dropout rate applied to input embeddings of each block
    resid_dropout: float = 0.0  # dropout rate applied to output of hyena operator

    # specific config vars for the hyena operator
    hyena_operator_order: int = 2  # the order of the recurrence in the hyena operator
    hyena_operator_implicit_filter_mlp_dim: int = 64  # the dimensionality of the ffn
    # which defines implicit filters for long conv

    hyena_operator_filter_positional_embed_dim: int = 3  # positional embedding dim of
    # the hyena filter
    hyena_operator_max_seq_len: int = 1026  # max sequence length passed to the model,
    # for fftconv
    hyena_operator_sin_frequency: int = 10


class HyenaDNA(hk.Module):
    """
    Defines a hyena dna model which is composed of a language model backbone(token
    embedding layer, a stack of hyena blocks) and a language model head.
    """

    def __init__(
        self,
        config: HyenaDNAConfig,
        name: Optional[str] = None,
        use_gradient_checkpointing_mixer: bool = False,
        use_gradient_checkpointing_mlp: bool = False,
    ) -> None:
        """
        Args:
            config: Hyena dna config dataclass
            name: Optional name for the model
            use_gradient_checkpointing_mixer: enable grad checkpointing for the mixer
                block
            use_gradient_checkpointing_mlp: enable grad checkpointing for the mlp block
        """
        super().__init__(name=name)

        if config.pad_vocab_size_multiple:
            if config.vocab_size % config.pad_vocab_size_multiple != 0:
                config.vocab_size += config.pad_vocab_size_multiple - (
                    config.vocab_size % config.pad_vocab_size_multiple
                )

        self._config = config

        # build a dictionary from the original config that contains just hyena operator
        # config vars
        hyena_operator_config = {
            "order": config.hyena_operator_order,
            "implicit_filter_mlp_dim": config.hyena_operator_implicit_filter_mlp_dim,
            "filter_positional_embed_dim": config.hyena_operator_filter_positional_embed_dim,  # noqa
            "max_seq_len": config.hyena_operator_max_seq_len,
            "sin_frequency": config.hyena_operator_sin_frequency,
        }

        self.backbone = LMBackbone(
            model_embed_dim=config.model_embed_dim,
            num_hyena_blocks=config.num_hyena_blocks,
            vocab_size=config.vocab_size,
            mlp_dim=config.mlp_dim,
            hyena_operator_config=hyena_operator_config,  # type: ignore
            resid_dropout=config.resid_dropout,
            embed_dropout=config.embed_dropout,
            use_gradient_checkpointing_mixer=use_gradient_checkpointing_mixer,
            use_gradient_checkpointing_mlp=use_gradient_checkpointing_mlp,
        )

        self.lm_head = hk.Linear(config.vocab_size, with_bias=False)

    def __call__(
        self, tokens: jnp.ndarray, is_training: bool = False
    ) -> Dict[str, jnp.ndarray]:

        final_embeddings = self.backbone(tokens, is_training=is_training)

        logits = self.lm_head(final_embeddings)

        return {"logits": logits, "embeddings": final_embeddings}


def build_hyena_dna_forward(
    config: HyenaDNAConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
    use_gradient_checkpointing_mixer: bool = False,
    use_gradient_checkpointing_mlp: bool = False,
) -> Callable:
    """
    Creates the forward pass function for Hyena DNA.

    Args:
        model_config: Model hyperparameters.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        model_name: Model's name.
        use_gradient_checkpointing_mixer: enable grad checkpointing for the mixer block
        use_gradient_checkpointing_mlp: enable grad checkpointing for the mlp block

    Returns:
        HyenaDNA model forward function.
    """

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(HyenaDNA, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32, param_dtype=param_dtype, output_dtype=compute_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def forward_fn(
        tokens: jnp.ndarray, is_training: bool = False
    ) -> Dict[str, jnp.ndarray]:
        hyena_dna = HyenaDNA(
            config=config,
            name=model_name,
            use_gradient_checkpointing_mixer=use_gradient_checkpointing_mixer,
            use_gradient_checkpointing_mlp=use_gradient_checkpointing_mlp,
        )
        outs = hyena_dna(tokens, is_training=is_training)
        

        return outs
        
    return forward_fn
