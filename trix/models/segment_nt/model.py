from dataclasses import dataclass
from typing import List, Optional

from trix.models.esm.model import ESMTransformerConfig


@dataclass
class SegmentNTConfig(ESMTransformerConfig):
    """
    Parameters to initialize a Segment NT model. The only supplementary argument is the
    genomic features inferred.

    Args:
        features: The features predicted at the nucleotide level
    """

    features: Optional[List[str]] = None
