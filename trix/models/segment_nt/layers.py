from typing import Dict, List, Optional

import haiku as hk
import jax
import jax.numpy as jnp
from haiku import initializers

from trix.layers.heads.unet_segmentation_head import UNET1DSegmentationHead
from trix.types import SequenceMask


class UNetHead(hk.Module):
    """
    Returns a probability between 0 and 1 over a target feature presence
    for each nucleotide in the input sequence. Assumes the sequence has been tokenized
    with non-overlapping 6-mers.
    """

    def __init__(
        self,
        features: List[str],
        embed_dimension: int = 1024,
        num_layers: int = 2,
        name: Optional[str] = None,
    ):
        """
        Args:
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)
        self._num_features = len(features)

        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        unet = UNET1DSegmentationHead(
            num_classes=embed_dimension // 2,
            output_channels_list=tuple(
                embed_dimension * (2**i) for i in range(num_layers)
            ),
        )
        fc = hk.Linear(
            6 * 2 * self._num_features, w_init=w_init, b_init=b_init, name="fc"
        )
        self._fc = hk.Sequential([unet, jax.nn.swish, fc])

    def __call__(
        self, x: jnp.ndarray, sequence_mask: SequenceMask
    ) -> Dict[str, jnp.ndarray]:
        """
        Input shape: (batch_size, sequence_length + 1, embed_dim)
        Output_shape: (batch_size, 6 * sequence_length, 2)
        """
        batch_size, seq_len = x.shape[0], x.shape[1] - 1
        logits = self._fc(x[:, 1:])  # remove CLS token
        logits = jnp.reshape(logits, (batch_size, seq_len * 6, self._num_features, 2))
        return {"logits": logits}
