"""
This file gathers layers used to finetune pre-trained ESM models.
"""
from dataclasses import replace
from typing import Callable, Dict, Optional

import haiku as hk
import jax
import jax.numpy as jnp
import jmp
from haiku import initializers

from trix.layers.attention.multi_head_attention import MultiHeadAttention
from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.positional.esm_rotary import RotaryEmbedding, RotaryEmbeddingConfig
from trix.models.esm.model import ESMTransformer, ESMTransformerConfig
from trix.types import AttentionMask, Embedding, SequenceMask, Tokens, TransformerOutput


class ESMMultiHeadAttentionIA3Rescaling(MultiHeadAttention):
    """
    This class adds rescaling weights following the IA³ methodology to the
    multi head attention layer of ESM. This aims to be used for fine-tuning
    pre-trained ESM models.
    """

    def __init__(
        self,
        num_heads: int,
        key_size: int,
        add_bias_kv: bool,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        value_size: Optional[int] = None,
        model_size: Optional[int] = None,
        name: Optional[str] = None,
    ):
        super().__init__(
            num_heads=num_heads,
            key_size=key_size,
            rotary_embedding_config=rotary_embedding_config,
            add_bias_kv=add_bias_kv,
            value_size=value_size,
            model_size=model_size,
            name=name,
        )

    @hk.transparent
    def attention_weights(
        self,
        query: jnp.ndarray,
        key: jnp.ndarray,
        attention_mask: Optional[AttentionMask] = None,
        attention_weight_bias: Optional[jnp.ndarray] = None,
    ) -> jnp.ndarray:
        """
        Computes the attention weights.

        Args:
            query: Embedding sequence to compute queries.
            key: Embedding sequence to compute keys.
            attention_mask: Input attention_mask. Defaults to None.

        Returns:
            Attention weights.
        """

        query_heads = self._linear_projection_he_init(query, self.key_size, "query")
        key_heads = self._linear_projection_he_init(key, self.key_size, "key")

        # add IA³ rescaling
        key_ia3_rescaling = hk.get_parameter(
            "key_ia3_rescaling",
            shape=[key_heads.shape[-2], key_heads.shape[-1]],
            dtype=key_heads.dtype,
            init=hk.initializers.Constant(1.0),
        )
        key_heads = key_heads * key_ia3_rescaling

        # Add bias for key (see ESM architecture)
        if self._bias_k is not None:
            batch_size = key_heads.shape[0]
            attention_bias = jnp.tile(self._bias_k, (batch_size, 1, 1, 1))
            key_heads = jnp.concatenate((key_heads, attention_bias), axis=1)
            if attention_mask is not None:
                attention_mask = jnp.concatenate(
                    (
                        attention_mask,
                        jnp.ones(attention_mask.shape[:-1] + (1,), dtype=jnp.bool_),
                    ),
                    axis=-1,
                )

        if self._rotary_embedding_config:
            query_heads, key_heads = RotaryEmbedding(
                self.key_size, self._rotary_embedding_config, name="rotary_embed"
            )(query_heads, key_heads)

        attention_logits = jnp.einsum("...thd,...Thd->...htT", query_heads, key_heads)
        sqrt_key_size = jnp.sqrt(self.key_size).astype(query.dtype)
        attention_logits = attention_logits / sqrt_key_size

        if attention_mask is not None:
            assert len(attention_mask.shape) == len(attention_logits.shape)
            attention_logits = jnp.where(attention_mask, attention_logits, -1e30)

        if attention_weight_bias is None:
            attention_weights = jax.nn.softmax(attention_logits)
        else:
            attention_weights = jax.nn.softmax(attention_logits + attention_weight_bias)

        return attention_weights

    @hk.transparent
    def compute_embeddings(
        self,
        value: jnp.ndarray,
        attention_weights: jnp.ndarray,
    ) -> jnp.ndarray:
        """
        Computes the output embeddings.

        Args:
            value: Embedding sequence to compute values.
            attention_weights: Attention weights.

        Returns:
            Output embeddings.
        """

        # He initialization
        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")

        value_heads = self._linear_projection_he_init(value, self.value_size, "value")
        if self._bias_v is not None:
            batch_size = value_heads.shape[0]
            attention_bias = jnp.tile(self._bias_v, (batch_size, 1, 1, 1))
            value_heads = jnp.concatenate((value_heads, attention_bias), axis=1)

        # add IA³ rescaling
        value_ia3_rescaling = hk.get_parameter(
            "value_ia3_rescaling",
            shape=[value_heads.shape[-2], value_heads.shape[-1]],
            dtype=value_heads.dtype,
            init=hk.initializers.Constant(1.0),
        )
        value_heads = value_heads * value_ia3_rescaling

        attention = jnp.einsum("...htT,...Thd->...thd", attention_weights, value_heads)

        # Concatenate attention matrix of all heads into a single vector.
        attention_vec = jnp.reshape(attention, (*value.shape[:-1], -1))
        return hk.Linear(
            self.model_size, w_init=w_init, b_init=b_init, name="mha_output"
        )(attention_vec)


class ESMAttentionBlockIA3Rescaling(SelfAttentionBlock):
    """
    This class adds rescaling weights following the IA³ methodology to the
    attention block layer of ESM. This aims to be used for fine-tuning
    pre-trained ESM models.
    """

    def __init__(
        self,
        num_heads: int,
        embed_dim: int,
        ffn_embed_dim: int,
        key_size: Optional[int] = None,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        add_bias_kv: bool = False,
        add_bias_fnn: bool = True,
        ffn_activation_name: str = "gelu-no-approx",
        use_glu_in_ffn: bool = False,
        layer_norm_eps: float = 1e-5,  # this is the default haiku value
        pre_layer_norm: bool = True,
        rescaling_factor: Optional[float] = None,
        name: Optional[str] = None,
    ):
        super().__init__(
            num_heads=num_heads,
            embed_dim=embed_dim,
            ffn_embed_dim=ffn_embed_dim,
            key_size=key_size,
            add_bias_kv=add_bias_kv,
            rotary_embedding_config=rotary_embedding_config,
            add_bias_fnn=add_bias_fnn,
            ffn_activation_name=ffn_activation_name,
            use_glu_in_ffn=use_glu_in_ffn,
            layer_norm_eps=layer_norm_eps,
            pre_layer_norm=pre_layer_norm,
            name=name,
        )

        key_size = embed_dim // num_heads
        # Replace multi-head attention by the IA³ rescaling one
        # Force naming in self attention to keep previous name
        self.sa_layer = ESMMultiHeadAttentionIA3Rescaling(
            num_heads=num_heads,
            key_size=key_size,
            add_bias_kv=add_bias_kv,
            rotary_embedding_config=rotary_embedding_config,
            name=hk.experimental.force_name(self.sa_layer.module_name),  # type: ignore
        )

    @hk.transparent
    def mlp(self, embed: Embedding) -> Embedding:
        """
        Applies one layer-norm, one linear layer, a Gelu activation,
        then a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, key_size * num_heads).

        Returns:
            The transformed sequence embedding.
        """
        # TODO: propagate support for GLU here
        if self._pre_layer_norm:
            x = self.layer_norm_mlp(embed)
        else:
            x = embed

        if self._use_glu_in_fnn:
            x1, x2 = jnp.split(self.fc1(x), indices_or_sections=2, axis=-1)
            x = self._ffn_activation_fn(x1) * x2
        else:
            x = self._ffn_activation_fn(self.fc1(x))

        ffn_ia3_rescaling = hk.get_parameter(
            "ffn_ia3_rescaling",
            shape=[x.shape[-1]],
            dtype=x.dtype,
            init=hk.initializers.Constant(1.0),
        )
        x = x * ffn_ia3_rescaling

        x = self.fc2(x)

        if not self._pre_layer_norm:
            x = self.layer_norm_mlp(x + embed)

        return x


class ESMTransformerIA3Rescaling(ESMTransformer):
    """
    This class adds rescaling weights following the IA³ methodology to the
    transformer model of ESM. This aims to be used for fine-tuning
    pre-trained ESM models.
    """

    def __init__(
        self,
        config: ESMTransformerConfig,
        name: Optional[str] = None,
    ):
        super().__init__(config=config, name=name)

    @hk.transparent
    def _attention_block(self, layer_idx: int) -> SelfAttentionBlock:
        return ESMAttentionBlockIA3Rescaling(  # type: ignore
            num_heads=self._config.attention_heads,
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            add_bias_kv=self._config.add_bias_kv,
            rotary_embedding_config=self._rotary_embedding_config,
            add_bias_fnn=self._config.add_bias_ffn,
            ffn_activation_name=self._config.ffn_activation_name,
            use_glu_in_ffn=self._config.use_glu_in_ffn,
            pre_layer_norm=self._config.pre_layer_norm,
            layer_norm_eps=self._config.layer_norm_eps,
            rescaling_factor=self._config.rescaling_factor,
            name=f"attention_layer_{layer_idx}",
        )


def build_esm_ia3_rescaling_with_head_fn(
    model_config: ESMTransformerConfig,
    head_fn: Callable[
        [], Callable[[jnp.ndarray, SequenceMask], Dict[str, jnp.ndarray]]
    ],
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates a forward pass for that ESM, adds rescaling weights and the input head.

    Args:
        model_config: Model hyperparameters.
        head_fn: Wrapper initializing a Classification/Regression head. The head cannot
            be passed directly as haiku modules cannot be initialized outside
            hk.transform.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        model_name: Optional name of the model.

    Example of the function being used with a classification head:
        The classification head is wrapped inside head_fn because
        haiku modules cannot be instantiated outside hk.transform.
        def head_fn():
            return SimpleClassificationHead(num_classes=num_classes)
        finetune_forward_fn = build_esm_ia3_rescaling_with_head_fn(
            model_config=config, head_fn=head_fn, model_name=model_name,
        )
        finetune_forward_fn = hk.transform(finetune_forward_fn)

    Returns:
        ESM model forward function with IA³ rescaling and indicated head.
    """
    # Adding final layer embedding if missing to be used as classification head input.
    num_layers = model_config.num_layers
    if not (num_layers in model_config.embeddings_layers_to_save):
        emb_layers_to_save = model_config.embeddings_layers_to_save + (num_layers,)
        model_config = replace(
            model_config, embeddings_layers_to_save=emb_layers_to_save
        )

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(ESMTransformerIA3Rescaling, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def esm_fn(
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
        sequence_mask: Optional[SequenceMask] = None,
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = ESMTransformerIA3Rescaling(config=model_config, name=model_name)
        outs: TransformerOutput = encoder(
            tokens=tokens,
            attention_mask=attention_mask,
        )
        embeddings = outs[f"embeddings_{num_layers}"]

        # Define head.
        head = head_fn()

        if sequence_mask is None:
            sequence_mask = jnp.ones_like(tokens)

        head_outs = head(  # type: ignore[call-arg]
            x=embeddings, sequence_mask=sequence_mask
        )
        outs.update(head_outs)
        return outs

    return esm_fn


def build_esm_with_head_fn(
    model_config: ESMTransformerConfig,
    head_fn: Callable[
        [], Callable[[jnp.ndarray, SequenceMask], Dict[str, jnp.ndarray]]
    ],
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates a model consisting of an ESM model and the input head.

    Args:
        model_config: Model hyperparameters.
        head_fn: Wrapper initializing a Classification/Regression head. The head cannot
            be passed directly as haiku modules cannot be initialized outside
            hk.transform.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        model_name: Optional name of the model.

    Example of the function being used with a classification head:
        The classification head is wrapped inside head_fn because
        haiku modules cannot be instantiated outside hk.transform.
        def head_fn():
            return SimpleClassificationHead(num_classes=num_classes)
        finetune_forward_fn = build_esm_ia3_rescaling_with_head_fn(
            model_config=config, head_fn=head_fn, model_name=model_name,
        )
        finetune_forward_fn = hk.transform(finetune_forward_fn)

    Returns:
        ESM model forward function with IA³ rescaling and indicated head.
    """
    # Adding final layer embedding if missing to be used as classification head input.
    num_layers = model_config.num_layers
    if not (num_layers in model_config.embeddings_layers_to_save):
        emb_layers_to_save = model_config.embeddings_layers_to_save + (num_layers,)
        model_config = replace(
            model_config, embeddings_layers_to_save=emb_layers_to_save
        )

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(ESMTransformer, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def esm_fn(
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
        sequence_mask: Optional[SequenceMask] = None,
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = ESMTransformer(config=model_config, name=model_name)
        outs: TransformerOutput = encoder(
            tokens=tokens,
            attention_mask=attention_mask,
        )
        embeddings = outs[f"embeddings_{num_layers}"]

        # Define head.
        head = head_fn()

        if sequence_mask is None:
            sequence_mask = jnp.ones_like(tokens)

        head_outs = head(  # type: ignore[call-arg]
            x=embeddings, sequence_mask=sequence_mask
        )
        outs.update(head_outs)
        return outs

    return esm_fn
