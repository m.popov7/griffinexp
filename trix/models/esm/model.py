"""Implementation of ESM in Jax. Similar to the standard transformer class with some
custom layers."""
import logging
from dataclasses import dataclass, field
from typing import Callable, Dict, List, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.heads.lm_head import RobertaLMHead, SimpleLMHead
from trix.layers.positional.alibi_bias import build_alibi_tensor_dnabert2
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.layers.positional.learned import LearnedPositionalEmbeddings
from trix.layers.regularization.tokens_dropout import TokensDropout
from trix.models.esm.layers import (
    ESMLearnedPositionalEmbeddings,
    ESMSinusoidalPositionalEmbedding,
)
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput
from trix.utils.logging import debug_log_tensor
from trix.utils.masking import build_padding_attention_mask

logger = logging.getLogger(__name__)


@dataclass
class ESMTransformerConfig:
    """
    Parameters to initialize an ESM model. While the ESM architecture is an encoder-only
    model, different choices have been made for each version and this configuration aims
    to cover most of them.

    Args:
        alphabet_size: Token vocabulary.
        pad_token_id: ID of pad token.
        mask_token_id: ID of mask token.
        max_positions: Maximum sequence length.
        embed_scale: Correction ratio applied to the embeddings to make up for the
            norm difference between the input during training and inference.
        emb_layer_norm_before: Whether to use layer norm before the first attention
            layer.
        attention_heads: Number of attention heads.
        key_size: The dimension of the query, key, and values within each attention
            head, if not specified, it is set to attention_heads//embed_dim.
            It can be useful to set a custom key size if we want to impose the size of
            the query, key and value tensor ( for example, tensors shaped with
            power of 2 are more efficiently handled on TPUs ).
            Note: Parametrizing the model with a custom key size has been done in :
            Brown, Tom, et al. "Language models are few-shot learners."
            Advances in neural information processing systems 33 (2020): 1877-1901.
        embed_dim: Embedding dimension.
        ffn_embed_dim: Feed forward embedding dimension.
        num_layers: Number of attention blocks.
        positional_embedding: Type of positional embedding to use before the first
            attention layer. Options: "learned", "learned_standard" "sinusoidal" or
            None.
            NOTE: "learned" is the positional embeding of ESM, and "learned_standard" is
             a more standard one, used for example in DNAbert.
        lm_head: type of language model head. Options: "simple", "roberta" or None.
        add_bias_kv: Add bias in attention layer.
        add_bias_ffn: Add bias in feed forward network block.
        use_rotary_embedding: Whether to use rotary embeddings (for ESM2). Requires:
            positional_embeddings = None.
        rescaling_factor: Scaling factor to use for rotary embeddings.
        ffn_activation_name: Activation function to be used in FFN block. Supported
            names are "gelu", "relu", "swish".
        use_glu_in_ffn: Whether to use Gated Linear Unit (GLU) in Feed
            Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
            to True and use swish as ffn_activation_name.
            Same principle for a gated-relu. To keep the same number of parameters in
            the FFN block, one should multiply by 2/3 the ffn_embed_dim when using GLU.
            See https://arxiv.org/pdf/2002.05202.pdf for more details.
        mask_before_attention: Use mask before attention layers (for EMS1b and ESM2).
        layer_norm_eps: the eps factor in the different layer norms of the model (refer
            to layer norm implementation)
        token_dropout: Token dropout.
        masking_ratio: Masking ratio (used if token dropout is enabled).
        masking_prob: Masking probability (used if token dropout is enabled).
        use_gradient_checkpointing: Whether to use gradient checkpointing (checkpoint
            gradients in the forward pass to reduce the computation in the backward).
    """

    alphabet_size: int
    pad_token_id: int
    mask_token_id: int

    max_positions: int = 1024
    embed_scale: float = 1.0

    # architecture
    emb_layer_norm_before: bool = False
    attention_heads: int = 20
    key_size: Optional[int] = None
    embed_dim: int = 1280
    ffn_embed_dim: int = 5120
    num_layers: int = 24
    positional_embedding: Optional[str] = "learned"
    lm_head: Optional[str] = "simple"
    add_bias_kv: bool = False
    add_bias_ffn: bool = True
    use_rotary_embedding: bool = False
    rescaling_factor: Optional[float] = None
    ffn_activation_name: str = "gelu-no-approx"
    use_glu_in_ffn: bool = False
    mask_before_attention: bool = False
    layer_norm_eps: float = 1e-5
    pre_layer_norm: bool = True
    bias_word_embedding: bool = False

    # dropout
    token_dropout: bool = False
    masking_ratio: float = 0.1
    masking_prob: float = 0.8

    # logging
    use_gradient_checkpointing: bool = False

    # return
    embeddings_layers_to_save: Tuple[int, ...] = ()
    attention_maps_to_save: List[Tuple[int, int]] = field(default_factory=list)

    def __post_init__(self) -> None:
        """
        Checks that the given values are compatible.
        """

        if self.key_size is None:
            if not self.embed_dim % self.attention_heads == 0:
                raise ValueError(
                    f"When no key size is provided, the embedding dimension should be "
                    f"divisible by the number of heads, however provided embedding "
                    f"dimension is {self.embed_dim} and the number of heads is "
                    f"{self.attention_heads}."
                )
            self.key_size = self.embed_dim // self.attention_heads
        if self.positional_embedding is not None:
            if type(self.positional_embedding) != str:
                raise TypeError

            if self.positional_embedding not in [
                "learned",
                "sinusoidal",
                "learned_standard",
                "alibi_dnabert_2",
            ]:
                raise ValueError(
                    "The positional_embedding argument should either be None,"
                    "`learned`, `sinusoidal`, 'learned_standard' or 'alibi_dnabert_2'."
                )
        if self.lm_head is not None:
            if type(self.lm_head) != str:
                raise TypeError

            if self.lm_head not in ["simple", "roberta"]:
                raise ValueError(
                    "The lm_head argument should either be None,"
                    "`simple` or `roberta`."
                )

        if self.use_rotary_embedding and self.positional_embedding is not None:
            raise ValueError(
                "When using rotary embedding, positional_embedding must be set to none"
            )

        if self.add_bias_kv and self.use_rotary_embedding:
            raise ValueError(
                "Biases on key and values are not compatible with Rotary embeddings."
            )

        if self.positional_embedding == "alibi_dnabert_2":
            assert not self.add_bias_kv


class ESMTransformer(hk.Module):
    """
    Jax implementation of ESM models. Covers ESM1, ESM1-b and ESM2 models.
    """

    def __init__(
        self,
        config: ESMTransformerConfig,
        name: Optional[str] = None,
    ):
        """
        Initialize an ESM model.

        Args:
            config: Dataclass containing model hyperparameters.
            name: Name for module (custom will break weight loading).
        """

        self._config = config
        super().__init__(name=name)

        self._embed_layer = hk.Embed(self._config.alphabet_size, self._config.embed_dim)

        if config.bias_word_embedding:
            self.embed_bias = hk.Bias(bias_dims=[-1], name="bias_word_embedding")

        if config.positional_embedding == "learned":
            self._pos_embed_layer = ESMLearnedPositionalEmbeddings(
                config.max_positions, config.embed_dim, config.pad_token_id
            )

        elif config.positional_embedding == "learned_standard":
            self._pos_embed_layer = LearnedPositionalEmbeddings(
                config.max_positions, config.embed_dim
            )

        elif config.positional_embedding == "sinusoidal":
            self._pos_embed_layer = ESMSinusoidalPositionalEmbedding(
                config.embed_dim, config.pad_token_id
            )
        elif self._config.positional_embedding == "alibi_dnabert_2":
            self.build_alibi_tensor_fn = build_alibi_tensor_dnabert2

        if config.lm_head is None:
            self._lm_head = config.lm_head

        elif config.lm_head == "roberta":
            self._lm_head = RobertaLMHead(
                embed_dim=self._config.embed_dim,
                alphabet_size=self._config.alphabet_size,
                name="roberta_lm_head",
            )

        elif config.lm_head == "simple":
            self._lm_head = SimpleLMHead(
                embed_dim=self._config.embed_dim,
                alphabet_size=self._config.alphabet_size,
            )

        if self._config.emb_layer_norm_before:
            self.emb_ln_before = hk.LayerNorm(
                axis=-1,
                create_scale=True,
                create_offset=True,
                name="emb_layer_norm_before",
                eps=self._config.layer_norm_eps,
            )

        if config.use_rotary_embedding:
            self._rotary_embedding_config = RotaryEmbeddingConfig(
                rescaling_factor=config.rescaling_factor
            )
        else:
            self._rotary_embedding_config = None  # type: ignore

        # Process attention maps to save requirement into more suitable format
        attention_maps_to_save = config.attention_maps_to_save
        self._attention_layers_to_save = list({t[0] for t in attention_maps_to_save})
        self._attention_maps_per_layer_to_save = {
            layer: [t[1] for t in attention_maps_to_save if t[0] == layer]
            for layer in self._attention_layers_to_save
        }

        # Checking user request can be executed, raise error otherwise
        max_layer = max(self._attention_layers_to_save + [0])
        if max_layer > config.num_layers:
            raise ValueError(
                f"You are requiring attention maps for layer {max_layer}, "
                f"while the model has {config.num_layers} layers only."
            )

        for layer, maps in self._attention_maps_per_layer_to_save.items():
            max_map = max(maps)
            if max_map > config.attention_heads:
                raise ValueError(
                    f"You are requiring attention maps number {max_map} "
                    f"at layer {layer}, while the model has {config.attention_heads} "
                    f"only."
                )

    @hk.transparent
    def apply_attention_blocks(
        self,
        x: Embedding,
        outs: Dict[str, Embedding],
        attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[Embedding, Dict[str, Embedding]]:
        """
        Create the blocks of attention layers and applies them.

        Args:
            x: The sequence embedding.
            outs: A dictionary to carry through the attention layers which stores the
                intermediate sequence embedding and attention maps.
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            The output sequence embedding.
            The optional intermediate results (embeddings of the layer and attention
                weights).
        """

        layers: List[Callable] = [
            self._attention_block(layer_idx)
            for layer_idx in range(self._config.num_layers)
        ]

        if self._config.use_gradient_checkpointing:
            # the remat-ed function cannot take control flow arguments
            layers = [hk.remat(layer) for layer in layers]

        if self._config.positional_embedding and (
            "alibi" in self._config.positional_embedding
        ):
            alibi_bias = self.build_alibi_tensor_fn(
                x.shape[1], self._config.attention_heads
            )
        else:
            alibi_bias = None

        for layer_idx, layer in enumerate(layers):
            output = layer(
                x=x, attention_mask=attention_mask, attention_weight_bias=alibi_bias
            )
            x = output["embeddings"]
            debug_log_tensor(f"Transformer layer {layer_idx} output", x, logger=logger)

            # Save intermediate embeddings if needed
            if (layer_idx + 1) in self._config.embeddings_layers_to_save:
                outs[f"embeddings_{(layer_idx + 1)}"] = output["embeddings"]
            # Save intermediate attention maps if needed
            if (layer_idx + 1) in self._attention_layers_to_save:
                for map_number in self._attention_maps_per_layer_to_save[layer_idx + 1]:
                    dkey = f"attention_map_layer_{layer_idx + 1}_number_{map_number}"
                    outs[dkey] = output["attention_weights"][:, map_number + 1]

        return x, outs

    @hk.transparent
    def _attention_block(self, layer_idx: int) -> SelfAttentionBlock:
        return SelfAttentionBlock(  # type: ignore
            num_heads=self._config.attention_heads,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            add_bias_kv=self._config.add_bias_kv,
            add_bias_fnn=self._config.add_bias_ffn,
            ffn_activation_name=self._config.ffn_activation_name,
            use_glu_in_ffn=self._config.use_glu_in_ffn,
            rotary_embedding_config=self._rotary_embedding_config,
            layer_norm_eps=self._config.layer_norm_eps,
            pre_layer_norm=self._config.pre_layer_norm,
            name=f"attention_layer_{layer_idx}",
        )

    def __call__(
        self,
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the embeddings based on the input tokens.

        Args:
            tokens: Input tokens out of the tokenizer of shape (batch_size, seq_len).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).
                If no mask is provided, a mask by default which equals 1 over all non
                pad tokens and 0 over pad tokens is computed.

        Returns:
            Dictionary containing the final embeddings and logits.
        """

        # Prepare outputs dict
        outs: Dict[str, jnp.ndarray] = {}

        # Compute embeddings
        debug_log_tensor("Tokens Ids", tokens, logger=logger)
        x = self._embed_layer(tokens)

        if self._config.bias_word_embedding:
            x = self.embed_bias(x)

        debug_log_tensor("Tokens embeddings", x, logger=logger)

        # Tokens dropout if needed
        if self._config.token_dropout:
            x = TokensDropout(
                embed_dim=self._config.embed_dim,
                mask_token_id=self._config.mask_token_id,
                pad_token_id=self._config.pad_token_id,
                masking_ratio=self._config.masking_ratio,
                masking_prob=self._config.masking_prob,
            )(x, tokens)
            debug_log_tensor("Tokens embeddings after dropout", x, logger=logger)

        # RoBERTa's mask scaling factor
        x = self._config.embed_scale * x
        if self._config.positional_embedding in [
            "learned",
            "sinusoidal",
            "learned_standard",
        ]:
            if self._config.positional_embedding == "learned":
                # Add check that the sequence fed into the transformer is not longer
                # than the max positions used to instantiate the learned positional
                # embeddings layer
                max_length_authorized = (
                    self._pos_embed_layer._embed_layer.vocab_size
                    - self._pos_embed_layer.padding_idx
                    - 1
                )
                assert tokens.shape[1] <= max_length_authorized, (
                    "Inputs to the learned positional embeddings layer have a length "
                    f"{x.shape[1]} greater than the max positions used to instantiate "
                    f"it: {max_length_authorized}"
                )
            elif self._config.positional_embedding == "learned_standard":
                # Add check that the sequence fed into the transformer is not longer
                # than the max positions used to instantiate the learned positional
                # embeddings layer
                max_length_authorized = self._pos_embed_layer._embed_layer.vocab_size
                assert tokens.shape[1] <= max_length_authorized, (
                    "Inputs to the learned positional embeddings layer have a length "
                    f"{x.shape[1]} greater than the max positions used to instantiate "
                    f"it: {max_length_authorized}"
                )
            x = x + self._pos_embed_layer(tokens)

        if self._config.emb_layer_norm_before:
            x = self.emb_ln_before(x)

        # Attention mask
        if attention_mask is None:
            attention_mask = build_padding_attention_mask(
                tokens=tokens, pad_token_id=self._config.pad_token_id
            )
        debug_log_tensor("Attention mask", attention_mask, logger=logger)

        # Mask before attention for models ESM1b and ESM2
        if self._config.mask_before_attention:
            x = x - jnp.where(attention_mask == 0, 1, 0)[:, 0, 0][..., None] * x
        # construct a tower of attention layers
        debug_log_tensor("Transformer stack input", x, logger=logger)

        x, outs = self.apply_attention_blocks(
            x=x,
            outs=outs,
            attention_mask=attention_mask,
        )

        debug_log_tensor("Transformer stack output", x, logger=logger)

        # Language Model Head
        if self._lm_head:
            lm_head_outs = self._lm_head(x)
            sequence_mask = attention_mask[:, 0, :, 0][:, :, None]
            outs["logits"] = jnp.where(sequence_mask, lm_head_outs["logits"], 0)
            debug_log_tensor("Logits", outs["logits"], logger=logger)

        if self._config.lm_head == "roberta":
            embeddings = lm_head_outs["embeddings"]
        else:
            embeddings = x

        debug_log_tensor("Final embeddings", embeddings, logger=logger)

        # Save final embeddings if needed
        if self._config.num_layers in self._config.embeddings_layers_to_save:
            outs[f"embeddings_{self._config.num_layers}"] = embeddings

        return outs  # type: ignore


def build_esm_fn(
    model_config: ESMTransformerConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        model_config: Model hyperparameters.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        model_name: Model's name.

    Returns:
        ESM model forward function.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(ESMTransformer, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32, param_dtype=param_dtype, output_dtype=compute_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def esm_fn(
        tokens: Tokens, attention_mask: Optional[AttentionMask] = None
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = ESMTransformer(config=model_config, name=model_name)
        outs = encoder(
            tokens=tokens,
            attention_mask=attention_mask,
        )
        return outs

    return esm_fn
