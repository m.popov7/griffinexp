"""
Implementation of layers specific to the ESM architecture, inspired from the original
repository.
"""
import logging
from typing import Optional

import haiku as hk
import jax.numpy as jnp

from trix.utils.logging import debug_log_tensor

logger = logging.getLogger(__name__)


class ESMLearnedPositionalEmbeddings(hk.Module):
    """
    Learned positional embeddings to be added to token embeddings. Specific to ESM as it
    is implemented by shifting the positions by 2 (1 + padding_idx).
    """

    def __init__(
        self,
        vocab_size: int,
        embed_dim: int,
        padding_idx: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            vocab_size: Tokenizer's vocabulary size.
            embed_dim: Embedding size.
            padding_idx: Index attributed to the padding
                token. Defaults to 1.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)
        self.padding_idx = padding_idx
        self._embed_layer = hk.Embed(vocab_size + padding_idx + 1, embed_dim)

    def __call__(self, tokens: jnp.ndarray) -> jnp.ndarray:
        mask = tokens != self.padding_idx
        positions = jnp.cumsum(mask, axis=1) * mask + self.padding_idx

        # debug printing tensor
        debug_log_tensor("Position embeddings", positions, logger=logger)

        return self._embed_layer(positions)


class ESMSinusoidalPositionalEmbedding(hk.Module):
    """
    Sinusoidal embeddings to be added to token embeddings. Specific to ESM as it
    is implemented by shifting the positions by 2 (1 + padding_idx).
    """

    def __init__(
        self,
        embed_dim: int,
        padding_idx: int,
        name: Optional[str] = None,
    ):

        super().__init__(name=name)
        self.embed_dim = embed_dim
        self.padding_idx = padding_idx

    def __call__(self, tokens: jnp.ndarray) -> jnp.ndarray:
        """
        Create the sinusoidal positional embeddings
        """

        bsz, seq_len = tokens.shape
        max_pos = self.padding_idx + 1 + seq_len
        weights = self._get_embedding(max_pos)
        positions = self._make_positions(tokens)

        # debug printing tensor
        debug_log_tensor("Position embeddings", positions, logger=logger)

        return weights[positions.reshape(-1), :].reshape(bsz, seq_len, -1)

    @hk.transparent
    def _make_positions(self, x: jnp.ndarray) -> jnp.ndarray:
        mask = ~jnp.equal(x, self.padding_idx)
        range_buf = (
            jnp.broadcast_to(jnp.arange(x.shape[1]), x.shape) + self.padding_idx + 1
        )
        positions = jnp.broadcast_to(range_buf, x.shape)
        return positions * mask + self.padding_idx * (1 - mask)

    @hk.transparent
    def _get_embedding(self, num_embeddings: int) -> jnp.ndarray:

        half_dim = self.embed_dim // 2
        emb = jnp.log(10000) / (half_dim - 1)
        emb = jnp.exp(jnp.arange(half_dim, dtype=jnp.float32) * -emb)
        emb = jnp.expand_dims(
            jnp.arange(num_embeddings, dtype=jnp.float32), axis=1
        ) * jnp.expand_dims(emb, 0)
        emb = jnp.reshape(
            jnp.concatenate([jnp.sin(emb), jnp.cos(emb)], axis=1), (num_embeddings, -1)
        )

        if self.embed_dim % 2 == 1:
            # zero pad
            emb = jnp.concatenate([emb, jnp.zeros((num_embeddings, 1))], axis=1)

        if self.padding_idx is not None:
            emb = emb.at[self.padding_idx, :].set(0)

        return emb
