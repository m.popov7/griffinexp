from typing import Optional

import haiku as hk
import numpy as np

from trix.utils.sharding.shard import (
    shard_linear_and_psum,
    shard_parallel_linear,
    shard_replicated_embedding,
    shard_replicated_linear,
    shard_replicated_norm,
)


def shard_roberta_lm_head(
    params: hk.Params, num_shards: int, sharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Shards a set of parameters of a RobertaLMHead to a ShardedRobertaLMHead layer.

    Args:
        params: Unsharded parameters.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    for layer_name in params:
        if "lm_final_fc" in layer_name or "lm_head_fc_1" in layer_name:
            sharded_params = shard_replicated_linear(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif "layer_norm" in layer_name:
            sharded_params = shard_replicated_norm(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        else:
            raise ValueError(
                "your parameter dictionary does not correspond to a RobertaLMHead."
                " This can also happen if the names of the layers in RobertaLMHead"
                " have been modified."
            )
    for layer_name in list(params.keys()):
        new_name = layer_name.replace("roberta_lm_head", "sharded_roberta_lm_head")
        params[new_name] = params.pop(layer_name)
    return sharded_params


def shard_attention_block(
    params: hk.Params, num_shards: int, sharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Shards the parameters of a SelfAttentionBlock.

    Args:
        params: Unsharded parameters of a SelfAttentionBlock.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    for layer_name in params:
        if "fc2" in layer_name or "mha_output" in layer_name:
            sharded_params = shard_linear_and_psum(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif (
            ("fc1" in layer_name)
            or ("value" in layer_name)
            or ("key" in layer_name)
            or ("query" in layer_name)
        ):
            sharded_params = shard_parallel_linear(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )

        elif "layer_norm" in layer_name:
            sharded_params = shard_replicated_norm(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif layer_name.split("_")[-1] == "attention":
            bias_k, bias_v = params[layer_name]["bias_k"], params[layer_name]["bias_v"]
            sharded_params[layer_name] = {
                "bias_k": np.reshape(
                    bias_k,
                    bias_k.shape[:2]
                    + (num_shards, bias_k.shape[2] // num_shards, bias_k.shape[3]),
                ).transpose((2, 0, 1, 3, 4)),
                "bias_v": np.reshape(
                    bias_v,
                    bias_v.shape[:2]
                    + (num_shards, bias_v.shape[2] // num_shards, bias_v.shape[3]),
                ).transpose((2, 0, 1, 3, 4)),
            }
        else:
            raise ValueError(
                f"Key: {layer_name} not recognized."
                "Your parameter dictionary does not correspond to a SelfAttentionBlock."
                " This can also happen if the names of the layers in SelfAttentionBlock"
                " have been modified."
            )
    return sharded_params


def shard_esm(params: hk.Params, num_shards: int) -> hk.Params:
    """
    Shards parameters of an ESMTransformer.

    Args:
        params: Parameters of an ESMTransformer. The names
            of the parameters must be the default one chosen
            in the ESMTransformer implementation for this
            function to work.
        num_shards: Number of shards.

    Returns:
        sharded_params: Sharded parameters.
    """
    sharded_params: hk.Params = {}
    keys = list(params.keys())
    num_layers = (
        max(
            [
                int(s.split("/")[1].split("_")[-1])
                for s in keys
                if "attention_layer_" in s
            ]
        )
        + 1
    )
    for n in range(num_layers):
        attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if f"attention_layer_{n}" in layer_name
        }
        sharded_params = shard_attention_block(attn_params, num_shards, sharded_params)
    not_attn_keys = [s for s in keys if "attention_layer_" not in s]
    roberta_keys = [layer for layer in not_attn_keys if "roberta_lm_head" in layer]
    sharded_params = shard_roberta_lm_head(
        {layer_name: params[layer_name] for layer_name in roberta_keys},
        num_shards,
        sharded_params,
    )
    other_keys = [layer for layer in not_attn_keys if "roberta_lm_head" not in layer]
    for layer_name in other_keys:
        if "embed" in layer_name:
            # deal with both token and positional embeddings
            sharded_params = shard_replicated_embedding(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
            if "esm_learned_positional_embeddings" in layer_name:
                sharded_params[
                    layer_name.replace("/embed", "/replicated_embed")
                ] = sharded_params.pop(layer_name)

        elif "lm_final" in layer_name:
            sharded_params = shard_replicated_linear(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif "emb_layer_norm_before" in layer_name:
            sharded_params = shard_replicated_norm(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        else:
            raise ValueError(
                f"{layer_name} is not part of the ShardedTransformer class: "
                "your parameter dictionary does not correspond to the "
                "ShardedTransformer class. This can also happenif the names of the "
                "layers in ShardedTransformerhave been modified."
            )

    for layer_name in list(params.keys()):
        new_name = layer_name.replace("esm_transformer", "sharded_esm_transformer")
        params[new_name] = params.pop(layer_name)

    return sharded_params
