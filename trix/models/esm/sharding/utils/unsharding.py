from typing import Optional

import haiku as hk
import numpy as np

from trix.models.encoder_decoder.sharding.utils.shard_encoder_decoder import (
    rename_params_to_unsharded,
)
from trix.utils.sharding.unshard import (
    unshard_linear_and_psum,
    unshard_parallel_linear,
    unshard_replicated_embedding,
    unshard_replicated_linear,
    unshard_replicated_norm,
)


def unshard_roberta_lm_head(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """

    Args:
        params: Sharded parameters of a ShardedESMRobertaLMHead.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.
    Returns:
        unsharded_params: Updated unsharded parameters.

    """
    if unsharded_params is None:
        unsharded_params = {}
    for layer_name in params:
        if "fc_1" in layer_name or "final_fc" in layer_name:
            unsharded_params = unshard_replicated_linear(
                {layer_name: params[layer_name]}, unsharded_params
            )

        elif "layer_norm" in layer_name:
            unsharded_params = unshard_replicated_norm(
                {layer_name: params[layer_name]}, unsharded_params
            )
        else:
            raise ValueError(
                "your parameter dictionary does not correspond to a SelfAttentionBlock."
                " This can also happen if the names of the layers in SelfAttentionBlock"
                " have been modified."
            )
    return unsharded_params


def unshard_attention_block(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Unshards the parameters of a SelfAttentionBlock/CrossAttentionBlock.

    Args:
        params: Sharded parameters of a SelfAttentionBlock/CrossAttentionBlock.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    for layer_name in params:
        if "fc2" in layer_name:
            if "~/linear" not in layer_name:
                # TODO: pass second layer as it is done twice
                fc2_params = {
                    layer: params[layer] for layer in params if "fc2" in layer
                }
                unsharded_params = unshard_linear_and_psum(fc2_params, unsharded_params)

        elif "mha_output" in layer_name:
            if "~/linear" not in layer_name:
                # TODO: pass second layer as it is done twice
                mha_output_params = {
                    layer: params[layer] for layer in params if "mha_output" in layer
                }
                unsharded_params = unshard_linear_and_psum(
                    mha_output_params, unsharded_params
                )
        elif (
            ("fc1" in layer_name)
            or ("value" in layer_name)
            or ("key" in layer_name)
            or ("query" in layer_name)
        ):
            unsharded_params = unshard_parallel_linear(
                {layer_name: params[layer_name]}, unsharded_params
            )

        elif "layer_norm" in layer_name:

            unsharded_params = unshard_replicated_norm(
                {layer_name: params[layer_name]}, unsharded_params
            )

        elif layer_name.split("_")[-1] == "attention":
            bias_k, bias_v = params[layer_name]["bias_k"], params[layer_name]["bias_v"]
            unsharded_params[layer_name] = {
                "bias_k": np.reshape(
                    bias_k.transpose((1, 2, 0, 3, 4)),
                    bias_k.shape[1:3] + (-1, bias_k.shape[-1]),
                ),
                "bias_v": np.reshape(
                    bias_v.transpose((1, 2, 0, 3, 4)),
                    bias_v.shape[1:3] + (-1, bias_k.shape[-1]),
                ),
            }
        else:
            raise ValueError(
                f"the key {layer_name} caused troubles : \n"
                "your parameter dictionary does not correspond to a SelfAttentionBlock."
                " This can also happen if the names of the layers in SelfAttentionBlock"
                " have been modified."
            )
    return unsharded_params


def unshard_esm(params: hk.Params, rename_params: bool = True) -> hk.Params:
    """
    Unshards parameters of a ShardedTransformer.

    Args:
        params: Parameters of a ShardedTransformer. The names of the parameters must be
            the default one chosen in the ShardedTransformer implementation for this
            function to work.
        rename_params: Whether to rename parameters of the output following default
            Trix values.

    Returns:
        unsharded_params: Unsharded parameters.
    """
    unsharded_params: hk.Params = {}
    keys = list(params.keys())
    num_layers = (
        max(
            [
                int(s.split("/")[1].split("_")[-1])
                for s in keys
                if "attention_layer" in s
            ]
        )
        + 1
    )
    for n in range(num_layers):
        self_attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if (f"layer_{n}/" in layer_name)
            and ("sharded_self_attention" not in layer_name)
        }
        unsharded_params = unshard_attention_block(self_attn_params, unsharded_params)

    not_attn_keys = [s for s in keys if "attention_layer" not in s]

    for layer_name in not_attn_keys:
        if "embed" in layer_name:
            unshard_replicated_embedding(
                {layer_name: params[layer_name]}, unsharded_params
            )

        elif "roberta_lm_head" in layer_name:
            unshard_roberta_lm_head({layer_name: params[layer_name]}, unsharded_params)

        elif "simple_lm_head" in layer_name:
            unshard_replicated_linear(
                {layer_name: params[layer_name]}, unsharded_params
            )
        else:
            raise ValueError(
                f"the key {layer_name} caused troubles : \n"
                "your parameter dictionary does not correspond to the "
                "ShardedTransformer class. This can also happen if the names of the "
                "layers in ShardedTransformer have been modified."
            )

    if rename_params:
        unsharded_params = rename_params_to_unsharded(unsharded_params)

    return unsharded_params
