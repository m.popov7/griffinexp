from collections.abc import Callable
from typing import Optional

import haiku as hk

from trix.layers.sharding.attention.self_attention import ShardedSelfAttentionBlock
from trix.layers.sharding.embeddings.embed import ReplicatedEmbed
from trix.layers.sharding.heads.lm_head import ShardedRobertaLMHead
from trix.layers.sharding.regularization.norms import ReplicatedLayerNorm
from trix.models.esm.layers import ESMSinusoidalPositionalEmbedding
from trix.models.esm.model import ESMTransformer, ESMTransformerConfig
from trix.models.esm.sharding.sharded_layers import (
    ReplicatedESMLearnedPositionalEmbeddings,
    ShardedLMHead,
)
from trix.types import AttentionMask, Tokens, TransformerOutput


class ShardedESMTransformer(ESMTransformer):
    """
    Jax implementation of Sharded ESM models. Covers ESM1, ESM1-b and ESM2 models but
    should only be used for the 3B and 15B parameters models from ESM2.
    """

    def __init__(
        self,
        config: ESMTransformerConfig,
        num_shards: int,
        name: Optional[str] = None,
    ):
        super().__init__(config=config, name=name)
        self._num_shards = num_shards
        # Add checks on dimensions
        if not config.attention_heads % self._num_shards == 0:
            raise ValueError(
                "The number of attention heads must be divisible by the number of "
                "shards, however provided attention heads is "
                f"{config.attention_heads} and the number of shards is "
                f"{self._num_shards}."
            )
        if config.bias_word_embedding:
            raise ValueError("bias_word_embedding is not implemented for sharded ESM")

        self._embed_layer = ReplicatedEmbed(
            self._config.alphabet_size,
            self._config.embed_dim,
            name=hk.experimental.force_name(self.name + "/~/embed"),
        )
        if config.positional_embedding == "learned":
            self._pos_embed_layer = ReplicatedESMLearnedPositionalEmbeddings(
                config.max_positions,
                config.embed_dim,
                config.pad_token_id,
                name=hk.experimental.force_name(
                    self.name + "/~/esm_learned_positional_embeddings"
                ),
            )
        elif config.positional_embedding == "sinusoidal":
            self._pos_embed_layer = ESMSinusoidalPositionalEmbedding(
                config.embed_dim,
                config.pad_token_id,
            )
        if config.lm_head is None:
            self._lm_head = config.lm_head

        elif config.lm_head == "roberta":
            self._lm_head = ShardedRobertaLMHead(
                embed_dim=self._config.embed_dim,
                alphabet_size=self._config.alphabet_size,
                name=hk.experimental.force_name(self.name + "/~/roberta_lm_head"),
            )

        elif config.lm_head == "simple":
            self._lm_head = ShardedLMHead(
                embed_dim=self._config.embed_dim,
                alphabet_size=self._config.alphabet_size,
                name=hk.experimental.force_name(self.name + "/~/simple_lm_head"),
            )
        if self._config.emb_layer_norm_before:
            self.emb_ln_before = ReplicatedLayerNorm(
                create_offset=True,
                name=hk.experimental.force_name(self.name + "/emb_layer_norm_before"),
            )

    @hk.transparent
    def _attention_block(self, layer_idx: int) -> ShardedSelfAttentionBlock:

        return ShardedSelfAttentionBlock(  # type: ignore
            num_heads_per_shard=self._config.attention_heads // self._num_shards,
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            key_size=self._config.key_size,
            num_shards=self._num_shards,
            add_bias_kv=self._config.add_bias_kv,
            rotary_embedding_config=self._rotary_embedding_config,
            name=hk.experimental.force_name(
                self.name + f"/attention_layer_{layer_idx}"
            ),
        )


def build_sharded_esm_fn(
    model_config: ESMTransformerConfig,
    num_shards: int,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        model_config: Model hyperparameters.
        num_shards: Number of shards to shard the model.
        model_name: Name to give to the transformer module.

    Returns:
        ESM sharded model forward function.
    """

    def sharded_esm_fn(
        tokens: Tokens, attention_mask: Optional[AttentionMask] = None
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = ShardedESMTransformer(
            config=model_config, num_shards=num_shards, name=model_name
        )
        outs = encoder(
            tokens=tokens,
            attention_mask=attention_mask,
        )
        return outs

    return sharded_esm_fn
