from typing import Optional

import haiku as hk
import jax.numpy as jnp

from trix.layers.heads.lm_head import SimpleLMHead
from trix.layers.sharding.embeddings.embed import ReplicatedEmbed
from trix.layers.sharding.linear.synchronous_linear import LinearLayerSynchronousInit
from trix.models.esm.layers import ESMLearnedPositionalEmbeddings


class ReplicatedESMLearnedPositionalEmbeddings(ESMLearnedPositionalEmbeddings):
    """
    Class for sharded Learned Positional Embeddings in the ESM version. It differs
    from the traditional Learned Positional Embedding by adding 2 to the sequence
    length.
    """

    def __init__(
        self,
        vocab_size: int,
        embed_dim: int,
        padding_idx: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            vocab_size: Tokenizer's vocabulary size.
            embed_dim: Embedding size.
            padding_idx: Index attributed to the padding
                token. Defaults to 1.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(vocab_size, embed_dim, padding_idx, name=name)
        self._embed_layer = ReplicatedEmbed(vocab_size + padding_idx + 1, embed_dim)

    def __call__(self, tokens: jnp.ndarray) -> jnp.ndarray:
        mask = tokens != self.padding_idx
        positions = jnp.cumsum(mask, axis=1) * mask + self.padding_idx
        return self._embed_layer(positions)


class ShardedLMHead(SimpleLMHead):
    """
    Sharded version of the SimpleLMHead module. Weights are synchronously initialized
    but no further check is made to avoid communication. Adding sanity checks to make
    sure the weights do not de-synchronize during training is recommended.
    """

    def __init__(
        self,
        embed_dim: int,
        alphabet_size: int,
        add_bias_lm_head: bool = True,
        name: Optional[str] = None,
    ):
        """
        Args:
            embed_dim: Embedding dimension.
            alphabet_size: Number of tokens in the alphabet.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(embed_dim=embed_dim, alphabet_size=alphabet_size, name=name)
        self.embed_dim = embed_dim
        self.alphabet_size = alphabet_size

        # Define layers
        self._final_fc = LinearLayerSynchronousInit(
            self.alphabet_size,
            add_bias_lm_head=add_bias_lm_head,
            name=hk.experimental.force_name(
                hk.experimental.current_name() + "/~/lm_final_fc"
            ),
        )
