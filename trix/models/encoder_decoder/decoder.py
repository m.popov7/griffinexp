from typing import Callable, Dict, List, Optional, Tuple

import haiku as hk
import jax.numpy as jnp

from trix.layers.attention.cross_attention import CrossAttentionBlock
from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.heads.lm_head import SimpleLMHead
from trix.layers.positional.learned import LearnedPositionalEmbeddings
from trix.models.encoder_decoder.encoder import EncoderConfig as DecoderConfig
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


class Decoder(hk.Module):
    """
    Creates a decoder module based on the single stack transformer architecture. The
    only difference with the traditional transformer is that it expects output
    embeddings from an encoder to perform cross attention.
    """

    def __init__(
        self,
        config: DecoderConfig,
        name: Optional[str] = None,
    ):
        """
        Initializes the Decoder stack of an Encoder Decoder model.

        Args:
            config: Dataclass containing decoder model hyperparameters.
            name: Name for module.
        """

        self._config = config
        super().__init__(name=name)

        self._embed_layer = hk.Embed(self._config.alphabet_size, self._config.embed_dim)

        self._pos_embed_layer = LearnedPositionalEmbeddings(
            config.max_positions, config.embed_dim
        )

        self._lm_head = SimpleLMHead(
            embed_dim=self._config.embed_dim,
            alphabet_size=self._config.alphabet_size,
        )

    @hk.transparent
    def apply_attention_blocks(
        self,
        x: Embedding,
        encoder_embedding: Embedding,
        outs: Dict[str, Embedding],
        attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[Embedding, Dict[str, Embedding]]:
        """
        Creates the blocks of attention layers and applies them to sequence embeddings.

        Args:
            x: The sequence embeddings.
            encoder_embedding: The output embeddings from the encoder used for
                cross-attention.
            outs: A dictionary to carry through the attention layers which stores the
                intermediate sequence embedding and attention maps.
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).
            cross_attention_mask: Cross attention mask of shape
                (batch_size, 1, seq_len, seq_len).

        Returns:
            Output sequence embedding.
            Optional intermediate results (layer output embeddings and corresponding
            attention weights).
        """

        layers: List[Callable] = [
            self._cross_attention(layer_idx)
            for layer_idx in range(self._config.num_layers)
        ]

        if self._config.use_gradient_checkpointing:
            # the remat-ed function cannot take control flow arguments
            layers = [hk.remat(layer) for layer in layers]

        for _, layer in enumerate(layers):
            output = layer(
                x=x,
                cross_attention_embeddings=encoder_embedding,
                attention_mask=attention_mask,
                cross_attention_mask=cross_attention_mask,
            )
            x = output["embeddings"]
            # Sum attention maps over heads.
            outs["attention_weights"] = jnp.sum(output["attention_weights"], axis=1)
        return x, outs

    @hk.transparent
    def _cross_attention(self, layer_idx: int) -> SelfAttentionBlock:

        return CrossAttentionBlock(  # type: ignore
            num_heads=self._config.num_attention_heads,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            name=f"attention_block_{layer_idx}",
        )

    def __call__(
        self,
        tokens: Tokens,
        encoder_embedding: Embedding,
        attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Compute decoder outputs based on input tokens and encoder embeddings.

        Args:
            tokens: Input tokens from the tokenizer of shape (batch_size, seq_len).
            encoder_embedding: Input embedding from an encoder of shape
                (batch_size, seq_len, embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).
            cross_attention_mask: Cross-attention mask of shape
                (batch_size, 1, seq_len, seq_len).

        Returns:
            Dictionary containing the final embeddings and logits.
        """
        batch_size, seq_len = tokens.shape

        # Prepare outputs dict
        outs: Dict[str, jnp.ndarray] = {}

        # Compute embeddings
        x = self._embed_layer(tokens)

        assert tokens.shape[1] <= self._pos_embed_layer._max_positions, (
            "Inputs to the learned positional embeddings layer have a length "
            f"{tokens.shape[1]} greater than the max positions used to instantiate "
            f"it: {self._pos_embed_layer._max_positions}"
        )

        # Add positional embeddings
        x = x + self._pos_embed_layer(tokens)

        # Attention mask #TODO: here we should feed a causal mask by default
        if attention_mask is None:
            attention_mask = jnp.ones((batch_size, 1, seq_len, seq_len))

        # construct a tower of attention layers
        x, outs = self.apply_attention_blocks(
            x=x,
            encoder_embedding=encoder_embedding,
            outs=outs,
            attention_mask=attention_mask,
            cross_attention_mask=cross_attention_mask,
        )

        # Head
        lm_head_outs = self._lm_head(x)
        outs["logits"] = lm_head_outs["logits"]

        # Save final embeddings if needed
        outs["embeddings"] = x
        return outs  # type: ignore
