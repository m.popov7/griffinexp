"""
This file gathers layers used to finetune pre-trained encoder-decoder models.
"""
from typing import Callable, Dict, Optional

import haiku as hk
import jax
import jax.numpy as jnp
import jmp

from trix.layers.attention.cross_attention import CrossAttentionBlock
from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.models.encoder_decoder.decoder import Decoder, DecoderConfig
from trix.models.encoder_decoder.encoder import Encoder, EncoderConfig
from trix.models.esm.finetuning import (
    ESMAttentionBlockIA3Rescaling,
    ESMMultiHeadAttentionIA3Rescaling,
)
from trix.types import AttentionMask, Embedding, SequenceMask, Tokens, TransformerOutput


class CrossAttentionBlockIA3Rescaling(CrossAttentionBlock):
    """
    This class adds rescaling weights following the IA³ methodology to the
    cross-attention block layer of a standard decoder. This aims to be used for
    fine-tuning pre-trained encoder-decoder models.
    """

    def __init__(
        self,
        num_heads: int,
        embed_dim: int,
        ffn_embed_dim: int,
        add_bias_kv: bool = False,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        name: Optional[str] = None,
    ):
        super().__init__(
            num_heads=num_heads,
            embed_dim=embed_dim,
            ffn_embed_dim=ffn_embed_dim,
            add_bias_kv=add_bias_kv,
            rotary_embedding_config=rotary_embedding_config,
            name=name,
        )

        key_size = embed_dim // num_heads
        # Replace multi-head attention by the IA³ rescaling one
        # Force naming in self attention to keep previous name
        self.self_attention_layer = ESMMultiHeadAttentionIA3Rescaling(
            num_heads=num_heads,
            key_size=key_size,
            add_bias_kv=add_bias_kv,
            rotary_embedding_config=rotary_embedding_config,
            name=hk.experimental.force_name(self.self_attention_layer.module_name),
        )  # type: ignore

        self.cross_attention_layer = ESMMultiHeadAttentionIA3Rescaling(
            num_heads=num_heads,
            key_size=key_size,
            add_bias_kv=add_bias_kv,
            rotary_embedding_config=rotary_embedding_config,
            name=hk.experimental.force_name(self.cross_attention_layer.module_name),
        )  # type: ignore

    @hk.transparent
    def mlp(self, x: Embedding) -> Embedding:
        """
        Applies one layer-norm, one linear layer, a Gelu activation,
        then a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, key_size * num_heads).

        Returns:
            The transformed sequence embedding.
        """
        # TODO: propagate support for GLU here
        x = self.layer_norm_mlp(x)
        x = jax.nn.gelu(
            self.fc1(x),
            approximate=False,
        )

        # add IA³ rescaling
        ffn_ia3_rescaling = hk.get_parameter(
            "ffn_ia3_rescaling",
            shape=[x.shape[-1]],
            dtype=x.dtype,
            init=hk.initializers.Constant(1.0),
        )
        x = x * ffn_ia3_rescaling

        x = self.fc2(x)
        return x


class EncoderIA3Rescaling(Encoder):
    """
    This class adds rescaling weights following the IA³ methodology to the
    encoder model of the encoder-decoder. This aims to be used for fine-tuning
    pre-trained encoder-decoder models.
    """

    def __init__(
        self,
        config: EncoderConfig,
        name: Optional[str] = None,
    ):
        super().__init__(config=config, name=name)

    @hk.transparent
    def _self_attention(self, layer_idx: int) -> SelfAttentionBlock:

        return ESMAttentionBlockIA3Rescaling(  # type: ignore
            num_heads=self._config.num_attention_heads,
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            add_bias_kv=False,
            rotary_embedding_config=None,
            name=f"self_attention_block_{layer_idx}",
        )


class DecoderIA3Rescaling(Decoder):
    """
    This class adds rescaling weights following the IA³ methodology to the
    encoder model of the encoder-decoder. This aims to be used for fine-tuning
    pre-trained encoder-decoder models.
    """

    def __init__(
        self,
        config: DecoderConfig,
        name: Optional[str] = None,
    ):
        super().__init__(config=config, name=name)

    @hk.transparent
    def _cross_attention(self, layer_idx: int) -> SelfAttentionBlock:

        return CrossAttentionBlockIA3Rescaling(  # type: ignore
            num_heads=self._config.num_attention_heads,
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            add_bias_kv=False,
            rotary_embedding_config=None,
            name=f"attention_block_{layer_idx}",
        )


def build_encoder_ia3_rescaling_with_head_fn(
    model_config: EncoderConfig,
    head_fn: Callable[
        [], Callable[[jnp.ndarray, SequenceMask], Dict[str, jnp.ndarray]]
    ],
    mixed_precision: bool = False,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates a forward pass for that ESM, adds rescaling weights and the input head.

    Args:
        model_config: Model hyperparameters.
        head_fn: Wrapper initializing a Classification/Regression head. The head cannot
            be passed directly as haiku modules cannot be initialized outside
            hk.transform.
        mixed_precision: Whether to use mixed precision computation.
        model_name: Optional name of the model.

    Example of the function being used with a classification head:
        The classification head is wrapped inside head_fn because
        haiku modules cannot be instantiated outside hk.transform.
        def head_fn():
            return SimpleClassificationHead(num_classes=num_classes)
        finetune_forward_fn = build_encoder_ia3_rescaling_with_head_fn(
            model_config=config, head_fn=head_fn, model_name=model_name,
        )
        finetune_forward_fn = hk.transform(finetune_forward_fn)

    Returns:
        Encoder model forward function with IA³ rescaling and indicated head.
    """

    if mixed_precision:
        # Use mixed precision (only support A100 GPU and TPU for now)
        half = jnp.bfloat16
        full = jnp.float32

        policy = jmp.Policy(compute_dtype=half, param_dtype=full, output_dtype=full)
        hk.mixed_precision.set_policy(EncoderIA3Rescaling, policy)

        # Remove it in batch norm to avoid instabilities
        policy = jmp.Policy(compute_dtype=full, param_dtype=full, output_dtype=half)
        hk.mixed_precision.set_policy(hk.BatchNorm, policy)
        hk.mixed_precision.set_policy(hk.LayerNorm, policy)

    def encoder_fn(
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
        sequence_mask: Optional[SequenceMask] = None,
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = EncoderIA3Rescaling(config=model_config, name=model_name)
        outs = encoder(
            tokens=tokens,
            attention_mask=attention_mask,
        )
        embeddings = outs["embeddings"]

        # Define head.
        head = head_fn()

        if sequence_mask is None:
            sequence_mask = jnp.ones_like(tokens)

        outs = head(x=embeddings, sequence_mask=sequence_mask)  # type: ignore[call-arg]
        return outs

    return encoder_fn


# Cannot be used for fine-tuning until an encoder-decoder classification trainer is made
def build_encoder_decoder_ia3_rescaling_with_head_fn(
    encoder_config: EncoderConfig,
    decoder_config: EncoderConfig,
    head_fn: Callable[
        [], Callable[[jnp.ndarray, SequenceMask], Dict[str, jnp.ndarray]]
    ],
    mixed_precision: bool = False,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates a forward pass for the encoder-decoder, adds rescaling weights and the input
    head.

    Args:
        encoder_config: Encoder hyperparameters.
        decoder_config: Decoder hyperparameters.
        head_fn: Wrapper initializing a Classification/Regression head. The head cannot
            be passed directly as haiku modules cannot be initialized outside
            hk.transform.
        mixed_precision: Whether to use mixed precision computation.
        model_name: Optional name of the model.

    Example of the function being used with a classification head:
        The classification head is wrapped inside head_fn because
        haiku modules cannot be instantiated outside hk.transform.
        def head_fn():
            return SimpleClassificationHead(num_classes=num_classes)
        finetune_forward_fn = build_encoder_decoder_ia3_rescaling_with_head_fn(
            model_config=config, head_fn=head_fn, model_name=model_name,
        )
        finetune_forward_fn = hk.transform(finetune_forward_fn)

    Returns:
        Encoder-decoder model forward function with IA³ rescaling and indicated head.
    """

    if mixed_precision:
        # Use mixed precision (only support A100 GPU and TPU for now)
        half = jnp.bfloat16
        full = jnp.float32

        policy = jmp.Policy(compute_dtype=half, param_dtype=full, output_dtype=full)
        hk.mixed_precision.set_policy(EncoderIA3Rescaling, policy)
        hk.mixed_precision.set_policy(DecoderIA3Rescaling, policy)

        # Remove it in batch norm to avoid instabilities
        policy = jmp.Policy(compute_dtype=full, param_dtype=full, output_dtype=half)
        hk.mixed_precision.set_policy(hk.BatchNorm, policy)
        hk.mixed_precision.set_policy(hk.LayerNorm, policy)

    # ClassificationTrainer will fail during fine-tuning, as the loss function will only
    # provide 1 tokens argument to this forward function when called
    def encoder_decoder_fn(
        encoder_tokens: Tokens,
        decoder_tokens: Tokens,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
        sequence_mask: Optional[SequenceMask] = None,
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = EncoderIA3Rescaling(config=encoder_config, name=model_name)
        decoder = DecoderIA3Rescaling(config=decoder_config, name=model_name)
        encoder_outs = encoder(
            tokens=encoder_tokens,
            attention_mask=encoder_attention_mask,
        )

        embeddings = decoder(
            decoder_tokens,
            encoder_outs["embeddings"],
            decoder_attention_mask,
            cross_attention_mask,
        )["embeddings"]
        # Define head.
        head = head_fn()

        if sequence_mask is None:
            sequence_mask = jnp.ones_like(decoder_tokens)

        outs = head(x=embeddings, sequence_mask=sequence_mask)  # type: ignore[call-arg]
        return outs

    return encoder_decoder_fn
