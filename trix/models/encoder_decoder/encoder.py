from dataclasses import dataclass
from typing import Callable, Dict, List, Optional, Tuple

import haiku as hk
import jax.numpy as jnp

from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.positional.learned import LearnedPositionalEmbeddings
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


@dataclass
class EncoderConfig:
    """
    Parameters to initialize an encoder model made of an embedding layer, self attention
    blocks, and a final head.

    Args:
        alphabet_size: Size of the vocabulary.
        max_positions: Maximum sequence length.
        num_attention_heads: Number of attention heads.
        embed_dim: Embedding dimension.
        ffn_embed_dim: Feed forward embedding dimension.
        num_layers: Number of attention blocks.
        use_gradient_checkpointing: Whether to use gradient checkpointing (checkpoint
            gradients in the forward pass to reduce the computation in the backward).
    """

    alphabet_size: int
    max_positions: int

    # architecture
    num_attention_heads: int = 20
    key_size: Optional[int] = None
    embed_dim: int = 1280
    ffn_embed_dim: int = 5120
    num_layers: int = 24

    # gradient checkpointing
    use_gradient_checkpointing: bool = False

    def __post_init__(self) -> None:
        """
        Checks that the given values are compatible.
        """
        if self.key_size is None:
            if not self.embed_dim % self.num_attention_heads == 0:
                raise ValueError(
                    f"When no key size is provided, the embedding dimension should be "
                    f"divisible by the number of heads, however provided embedding "
                    f"dimension is {self.embed_dim} and the number of heads is "
                    f"{self.num_attention_heads}."
                )
            self.key_size = self.embed_dim // self.num_attention_heads


class Encoder(hk.Module):
    """
    Creates a single stack Transformer (Encoder only or Decoder only) made of an
    embedding layer, attention blocks, and a final head.
    """

    def __init__(
        self,
        config: EncoderConfig,
        name: Optional[str] = None,
    ):
        """
        Initializes a Transformer model.

        Args:
            config: Dataclass containing model hyperparameters.
            name: Name for module.
        """

        self._config = config
        super().__init__(name=name)

        self._embed_layer = hk.Embed(self._config.alphabet_size, self._config.embed_dim)

        self._pos_embed_layer = LearnedPositionalEmbeddings(
            config.max_positions, config.embed_dim
        )

    @hk.transparent
    def apply_attention_blocks(
        self,
        x: Embedding,
        outs: Dict[str, Embedding],
        attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[Embedding, Dict[str, Embedding]]:
        """
        Creates the blocks of attention layers and applies them.

        Args:
            x: Sequence embedding of shape (batch,seq_len,embed_dim).
            outs: A dictionary to carry through the attention layers which stores the
                intermediate sequence embedding and attention maps.
            attention_mask: attention mask of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            Output sequence embedding.
            Dictionary of optional intermediate results (embeddings of the layer and
            attention weights).
        """

        layers: List[Callable] = [
            self._self_attention(layer_idx)
            for layer_idx in range(self._config.num_layers)
        ]

        if self._config.use_gradient_checkpointing:
            # the remat-ed function cannot take control flow arguments
            layers = [hk.remat(layer) for layer in layers]

        for _, layer in enumerate(layers):
            output = layer(
                x=x,
                attention_mask=attention_mask,
            )
            x = output["embeddings"]

            # Sum attention maps over heads.
            outs["attention_weights"] = jnp.sum(output["attention_weights"], axis=1)
        return x, outs

    @hk.transparent
    def _self_attention(self, layer_idx: int) -> SelfAttentionBlock:

        return SelfAttentionBlock(  # type: ignore
            num_heads=self._config.num_attention_heads,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            name=f"self_attention_block_{layer_idx}",
        )

    def __call__(
        self,
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the embeddings based on the input tokens.

        Args:
            tokens: Input tokens out of the tokenizer of shape (batch_size, seq_len).
            attention_mask: Attention mask (batch_size, 1, seq_len, seq_len).

        Returns:
            Dictionary containing the final embeddings, attention weights and logits if
            a lm_head is appended.
        """
        batch_size, seq_len = tokens.shape

        # Prepare outputs dict
        outs: Dict[str, jnp.ndarray] = {}

        # Compute embeddings
        x = self._embed_layer(tokens)

        assert tokens.shape[1] <= self._pos_embed_layer._max_positions, (
            "Inputs to the learned positional embeddings layer have a length "
            f"{tokens.shape[1]} greater than the max positions used to instantiate "
            f"it: {self._pos_embed_layer._max_positions}"
        )

        # Add positional embeddings
        x = x + self._pos_embed_layer(tokens)

        # Attention mask
        if attention_mask is None:
            attention_mask = jnp.ones((batch_size, 1, seq_len, seq_len))

        # Construct a tower of attention layers
        x, outs = self.apply_attention_blocks(
            x=x,
            outs=outs,
            attention_mask=attention_mask,
        )

        # Save final embeddings if needed
        outs["embeddings"] = x
        return outs  # type: ignore
