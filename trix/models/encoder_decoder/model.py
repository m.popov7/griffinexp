"""Implementation of a standard Encoder Decoder model."""
from typing import Callable, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.models.encoder_decoder.decoder import Decoder, DecoderConfig
from trix.models.encoder_decoder.encoder import Encoder, EncoderConfig
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


class EncoderDecoder(hk.Module):
    """
    Creates an Encoder Decoder model.
    """

    def __init__(
        self,
        encoder_config: EncoderConfig,
        decoder_config: DecoderConfig,
        name: Optional[str] = None,
    ):
        """
        Initializes the Encoder stack and Decoder stack.

        Args:
            encoder_config: Dataclass containing encoder hyperparameters.
            decoder_config: Dataclass containing decoder hyperparameters.
            name: Name for module.
        """

        self._encoder_config = encoder_config
        self._decoder_config = decoder_config

        super().__init__(name=name)
        self._encoder = Encoder(encoder_config)
        self._decoder = Decoder(decoder_config)

    @hk.transparent
    def encode(
        self,
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Encode the input tokens.

        Args:
            tokens: Encoder input tokens from tokenizer of shape (batch_size, seq_len).
            attention_mask: Attention mask to use, of shape
                (batch_size, 1, seq_len, seq_len).

        Returns:
            Dictionary of Encoder output, with last layer embedding stored to feed
            to the Decoder.
        """

        return self._encoder(  # type: ignore
            tokens=tokens, attention_mask=attention_mask
        )

    @hk.transparent
    def decode(
        self,
        tokens: Tokens,
        encoder_embedding: Embedding,
        attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Decode the input tokens with cross attention using encoder_embedding.

        Args:
            tokens: Decoder input tokens from tokenizer of shape (batch_size, seq_len).
            encoder_embedding: Input embedding coming from an encoder of shape
                (batch_size, seq_len, embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).
            cross_attention_mask: Cross-attention mask of shape
                (batch_size, 1, seq_len, seq_len).
        #TODO: add causal attention_mask and cross_attention_mask + padding_mask as
        default for safety.

        Returns:
            Dictionary of Decoder output containing the final embeddings and logits.
        """

        return self._decoder(  # type: ignore
            tokens=tokens,
            encoder_embedding=encoder_embedding,
            attention_mask=attention_mask,
            cross_attention_mask=cross_attention_mask,
        )

    def __call__(
        self,
        encoder_tokens: Tokens,
        decoder_tokens: Tokens,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_attention_mask: Optional[AttentionMask] = None,
        decoder_cross_attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[TransformerOutput, TransformerOutput]:
        """
        Compute the final embeddings based on the encoder input tokens and decoder
        input tokens.

        Args:
            encoder_tokens: Encoder input tokens out of the tokenizer of shape
                (batch_size, seq_len).
            decoder_tokens: Decoder input tokens out of the tokenizer of shape
                (batch_size, seq_len).
            encoder_attention_mask: Attention mask for the self attention in the encoder
                of shape (batch_size, 1, seq_len, seq_len).
            decoder_attention_mask: Attention mask for the self attention in the decoder
                of shape (batch_size, 1, seq_len, seq_len).
            decoder_cross_attention_mask: Attention mask for the cross-attention in the
                decoder of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            Dictionary of Decoder outputs containing logits and embeddings.
            Dictionary of Encoder outputs containing embeddings.
        """

        # Apply the encoder
        encoder_outs = self.encode(encoder_tokens, encoder_attention_mask)
        # Extract encoder embeddings to feed to the decoder
        encoder_embedding = encoder_outs["embeddings"]

        decoder_outs = self.decode(
            decoder_tokens,
            encoder_embedding,
            decoder_attention_mask,
            decoder_cross_attention_mask,
        )
        return decoder_outs, encoder_outs  # type: ignore


def build_encoder_decoder_fn(
    encoder_config: EncoderConfig,
    decoder_config: DecoderConfig,
    mixed_precision: bool = False,
) -> Callable:
    """
    Create the model's forward pass.

    Args:
        encoder_config: Encoder configuration (model hyperparameters).
        decoder_config: Decoder configuration (model hyperparameters).
        mixed_precision: Whether to use mixed precision.

    Returns:
        Encoder Decoder model forward function.
    """
    if mixed_precision:
        # Use mixed precision (only support A100 GPU and TPU for now)
        half = jnp.bfloat16
        full = jnp.float32

        policy = jmp.Policy(compute_dtype=half, param_dtype=full, output_dtype=full)
        hk.mixed_precision.set_policy(EncoderDecoder, policy)

        # Remove it in batch norm to avoid instabilities
        policy = jmp.Policy(compute_dtype=full, param_dtype=full, output_dtype=half)
        hk.mixed_precision.set_policy(hk.BatchNorm, policy)
        hk.mixed_precision.set_policy(hk.LayerNorm, policy)

    def encoder_decoder_fn(
        encoder_tokens: Tokens,
        decoder_tokens: Tokens,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_attention_mask: Optional[AttentionMask] = None,
        decoder_cross_attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[TransformerOutput, TransformerOutput]:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder_decoder = EncoderDecoder(encoder_config, decoder_config)
        decoder_outs, encoder_outs = encoder_decoder(
            encoder_tokens=encoder_tokens,
            decoder_tokens=decoder_tokens,
            encoder_attention_mask=encoder_attention_mask,
            decoder_attention_mask=decoder_attention_mask,
            decoder_cross_attention_mask=decoder_cross_attention_mask,
        )
        return encoder_outs, decoder_outs

    return encoder_decoder_fn
