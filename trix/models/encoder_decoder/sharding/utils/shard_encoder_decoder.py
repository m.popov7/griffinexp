import haiku as hk

from trix.utils.sharding.shard import (
    shard_attention_block,
    shard_replicated_embedding,
    shard_replicated_linear,
)
from trix.utils.sharding.unshard import (
    unshard_attention_block,
    unshard_replicated_embedding,
    unshard_replicated_linear,
)


def shard_transformer(
    params: hk.Params,
    num_shards: int,
    rename_params: bool = True,
) -> hk.Params:
    """
    Shards parameters of an encoder.

    Args:
        params: Parameters of a Transformer. The names
            of the parameters must be the default one chosen
            in the Transformer implementation for this
            function to work.
        num_shards: Number of shards.
        rename_params: Whether to rename parameters of the output following default
            Trix values.

    Returns:
        sharded_params: Sharded parameters.
    """
    sharded_params: hk.Params = {}
    keys = list(params.keys())
    num_layers = (
        max(
            [
                int(s.split("/")[1].split("_")[-1])
                for s in keys
                if "attention_block" in s
            ]
        )
        + 1
    )
    for n in range(num_layers):
        self_attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if (f"block_{n}/" in layer_name)
            and ("sharded_cross_attention" not in layer_name)
        }
        sharded_params = shard_attention_block(
            self_attn_params, num_shards, sharded_params
        )
        cross_attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if (f"block_{n}/" in layer_name)
            and ("sharded_self_attention" not in layer_name)
        }
        sharded_params = shard_attention_block(
            cross_attn_params, num_shards, sharded_params
        )
    not_attn_keys = [s for s in keys if "attention_block" not in s]
    for layer_name in not_attn_keys:
        if "embed" in layer_name:
            # deal with both token and positional embeddings
            shard_replicated_embedding(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif "lm_final" in layer_name:
            shard_replicated_linear(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        else:
            raise ValueError(
                "your parameter dictionary does not correspond to the "
                "ShardedTransformer class. This can also happen if the names of the "
                "layers in ShardedTransformer have been modified."
            )
    if rename_params:
        sharded_params = rename_params_to_sharded(sharded_params)

    return sharded_params


def shard_encoder_decoder(
    params: hk.Params, num_shards: int, rename_params: bool = True
) -> hk.Params:
    """
    Shards parameters of an Encoder Decoder model.

    Args:
        params: Parameters of an Encoder Decoder. The names
            of the parameters must be the default one chosen
            in the Encoder Decoder implementation for this
            function to work.
        num_shards: Number of shards.
        rename_params: Whether to rename parameters of the output following default
            Trix values.

    Returns:
        sharded_params: Sharded parameters.
    """
    keys = params.keys()

    # get encoder and decoder keys
    decoder_keys = [key for key in keys if "/decoder" in key]
    encoder_keys = list(set(keys) - set(decoder_keys))

    # remove encoder_decoder prefix
    encoder_params = {
        key.replace("encoder_decoder/~/", ""): params[key] for key in encoder_keys
    }
    decoder_params = {
        key.replace("encoder_decoder/~/", ""): params[key] for key in decoder_keys
    }

    # shard encoder parameters
    encoder_sharded_params = shard_transformer(
        encoder_params, num_shards, rename_params=False
    )
    # shard decoder  parameters
    decoder_sharded_params = shard_transformer(
        decoder_params, num_shards, rename_params=False
    )

    sharded_params = {**encoder_sharded_params, **decoder_sharded_params}

    # add back the encoder_decoder prefix
    sharded_params = {
        "encoder_decoder/~/" + key: sharded_params[key] for key in sharded_params.keys()
    }
    if rename_params:
        sharded_params = rename_params_to_sharded(sharded_params)

    return sharded_params


def unshard_transformer(params: hk.Params, rename_params: bool = True) -> hk.Params:
    """
    Unshards parameters of a ShardedTransformer.

    Args:
        params: Parameters of a ShardedTransformer. The names of the parameters must be
            the default one chosen in the ShardedTransformer implementation for this
            function to work.
        rename_params: Whether to rename parameters of the output following default
            Trix values.

    Returns:
        unsharded_params: Unsharded parameters.
    """
    unsharded_params: hk.Params = {}
    keys = list(params.keys())
    num_layers = (
        max(
            [
                int(s.split("/")[1].split("_")[-1])
                for s in keys
                if "attention_block" in s
            ]
        )
        + 1
    )
    for n in range(num_layers):
        self_attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if (f"block_{n}/" in layer_name) and ("cross_attention/" not in layer_name)
        }
        unsharded_params = unshard_attention_block(self_attn_params, unsharded_params)
        cross_attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if (f"block_{n}/" in layer_name) and ("self_attention/" not in layer_name)
        }
        unsharded_params = unshard_attention_block(cross_attn_params, unsharded_params)
    not_attn_keys = [s for s in keys if "attention_block" not in s]
    for layer_name in not_attn_keys:
        if "replicated_embed" in layer_name:
            unshard_replicated_embedding(
                {layer_name: params[layer_name]}, unsharded_params
            )
        elif "synchronous_final_fc" in layer_name:
            unshard_replicated_linear(
                {layer_name: params[layer_name]}, unsharded_params
            )
        else:
            raise ValueError(
                "your parameter dictionary does not correspond to the "
                "ShardedTransformer class. This can also happen if the names of the "
                "layers in ShardedTransformer have been modified."
            )
    if rename_params:
        unsharded_params = rename_params_to_unsharded(unsharded_params)

    return unsharded_params


def unshard_encoder_decoder(params: hk.Params, rename_params: bool = True) -> hk.Params:
    """
    Unshards parameters of a ShardedEncoderDecoder.

    Args:
        params: Parameters of a ShardedEncoderDecoder. The names of the parameters must
        be the default one chosen in the ShardedEncoderDecoder implementation for this
        function to work.
        rename_params: Whether to rename parameters of the output following default
            Trix values.

    Returns:
        unsharded_params: Unsharded parameters.
    """

    keys = params.keys()

    # get encoder and decoder keys
    decoder_keys = [key for key in keys if "/sharded_decoder" in key]
    encoder_keys = list(set(keys) - set(decoder_keys))

    # remove sharded encoder_decoder prefix
    encoder_params = {
        key.replace("sharded_encoder_decoder/~/", ""): params[key]
        for key in encoder_keys
    }
    decoder_params = {
        key.replace("sharded_encoder_decoder/~/", ""): params[key]
        for key in decoder_keys
    }

    # unshard encoder parameters
    encoder_unsharded_params = unshard_transformer(encoder_params, rename_params=False)
    # unshard decoder  parameters
    decoder_unsharded_params = unshard_transformer(decoder_params, rename_params=False)

    unsharded_params = {**encoder_unsharded_params, **decoder_unsharded_params}

    # add back the encoder_decoder prefix
    unsharded_params = {
        "sharded_encoder_decoder/~/" + key: unsharded_params[key]
        for key in unsharded_params.keys()
    }
    if rename_params:
        unsharded_params = rename_params_to_unsharded(unsharded_params)
    return unsharded_params


def rename_params_to_sharded(sharded_params: hk.Params) -> hk.Params:
    """
    Changes the keys of sharded_params to fit the ones of a ShardedTransformer.
    It is also usable on subpart of this module.

    Args:
        unsharded_params: Unsharded parameters extracted from a
            sharded model.

    Returns:
        Parameters with the right names to be fit to a Transformer.
    """
    for layer_name in list(sharded_params.keys()):
        new_name = (
            layer_name.replace("transformer", "sharded_transformer")
            .replace("/decoder", "/sharded_decoder")
            .replace("encoder_decoder", "sharded_encoder_decoder")
            .replace("/embed", "/replicated_embed")
            .replace(
                "learned_positional_embeddings",
                "replicated_learned_positional_embeddings",
            )
            .replace("simple_lm_head", "sharded_lm_head")
            .replace("lm_final_fc", "synchronous_final_fc")
            .replace("mha/", "sharded_mha/")
            .replace("esm_transformer", "sharded_esm_transformer")
        )
        sharded_params[new_name] = sharded_params.pop(layer_name)
    return sharded_params


def rename_params_to_unsharded(unsharded_params: hk.Params) -> hk.Params:
    """
    Changes the keys of unsharded_params to fit the ones of an Encoder Decoder.
    It is also usable on subpart of this module.

    Args:
        unsharded_params: Unsharded parameters extracted from a
            sharded model.

    Returns:
        Parameters with the right names to be fit to an Encoder Decoder.
    """
    for layer_name in list(unsharded_params.keys()):
        new_name = (
            layer_name.replace("sharded_transformer", "transformer")
            .replace("sharded_decoder", "decoder")
            .replace("sharded_encoder_decoder", "encoder_decoder")
            .replace("sharded_self_attention", "self_attention")
            .replace("sharded_cross_attention", "cross_attention")
            .replace("replicated_embed", "embed")
            .replace(
                "replicated_learned_positional_embeddings",
                "learned_positional_embeddings",
            )
            .replace("sharded_lm_head", "simple_lm_head")
            .replace("synchronous_final_fc", "lm_final_fc")
            .replace("sharded_mha", "mha")
            .replace("sharded_esm_transformer", "esm_transformer")
        )
        unsharded_params[new_name] = unsharded_params.pop(layer_name)
    return unsharded_params
