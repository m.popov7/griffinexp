from typing import Optional

import haiku as hk

from trix.layers.sharding.attention.cross_attention import ShardedCrossAttentionBlock
from trix.layers.sharding.embeddings.embed import ReplicatedEmbed
from trix.layers.sharding.heads.lm_head import ShardedLMHead
from trix.layers.sharding.positional.learned import (
    ReplicatedLearnedPositionalEmbeddings,
)
from trix.models.encoder_decoder.decoder import Decoder, DecoderConfig


class ShardedDecoder(Decoder):
    """
    Creates a sharded decoder module based on the single stack transformer architecture.
    The only difference with the sharded transformer is that it expects output
    embeddings from an encoder to perform cross attention.
    """

    def __init__(
        self,
        config: DecoderConfig,
        num_shards: int,
        name: Optional[str] = None,
    ):
        """
        Initializes the Decoder of an EncoderDecoder model.

        Args:
            config: Dataclass containing model hyperparameters.
            num_shards: Number of shards to shard the model.
            name: Name for module.
        """
        super().__init__(config=config, name=name)

        self._config = config
        self._num_shards = num_shards

        self._embed_layer = ReplicatedEmbed(
            self._config.alphabet_size, self._config.embed_dim
        )

        self._pos_embed_layer = ReplicatedLearnedPositionalEmbeddings(
            config.max_positions, config.embed_dim
        )

        self._lm_head = ShardedLMHead(
            embed_dim=self._config.embed_dim,
            alphabet_size=self._config.alphabet_size,
        )

    @hk.transparent
    def _cross_attention(self, layer_idx: int) -> ShardedCrossAttentionBlock:

        return ShardedCrossAttentionBlock(  # type: ignore
            num_heads_per_shard=self._config.num_attention_heads // self._num_shards,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            num_shards=self._num_shards,
            name=f"cross_attention_block_{layer_idx}",
        )
