from typing import Callable, Optional

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.layers.sharding.attention.self_attention import ShardedSelfAttentionBlock
from trix.layers.sharding.embeddings.embed import ReplicatedEmbed
from trix.layers.sharding.positional.learned import (
    ReplicatedLearnedPositionalEmbeddings,
)
from trix.models.encoder_decoder.encoder import Encoder, EncoderConfig
from trix.types import AttentionMask, Tokens, TransformerOutput


class ShardedEncoder(Encoder):
    """
    Creates a sharded encoder made of an embedding layer, self attention blocks, and a
    final head.
    """

    def __init__(
        self,
        config: EncoderConfig,
        num_shards: int,
        name: Optional[str] = None,
    ):
        """
        Initializes a Transformer model.

        Args:
            config: Dataclass containing model hyperparameters.
            name: Name for module.
        """
        super().__init__(name=name, config=config)

        self._config = config
        self._num_shards = num_shards
        if not config.num_attention_heads % self._num_shards == 0:
            raise ValueError(
                "The number of attention heads must be divisible by the number of "
                "shards, however provided attention heads is "
                "{config.num_attention_heads} and the number of shards is "
                "{self._num_shards}."
            )
        self._embed_layer = ReplicatedEmbed(
            self._config.alphabet_size, self._config.embed_dim
        )

        self._pos_embed_layer = ReplicatedLearnedPositionalEmbeddings(
            config.max_positions, config.embed_dim
        )

    @hk.transparent
    def _self_attention(self, layer_idx: int) -> ShardedSelfAttentionBlock:

        return ShardedSelfAttentionBlock(  # type: ignore
            num_heads_per_shard=self._config.num_attention_heads // self._num_shards,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            num_shards=self._num_shards,
            name=f"self_attention_block_{layer_idx}",
        )


def build_sharded_encoder_fn(
    model_config: EncoderConfig,
    num_shards: int,
    mixed_precision: bool = False,
    name: Optional[str] = None,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        model_config: Model parameters.
        num_shards: Number of shards to shard the model.
        mixed_precision: Whether to use mixed precision.
        name: Name to give to the transformer module.

    Returns:
        Transformer model forward function.
    """
    if mixed_precision:
        # Use mixed precision (only support A100 GPU and TPU for now)
        half = jnp.bfloat16
        full = jnp.float32

        policy = jmp.Policy(compute_dtype=half, param_dtype=full, output_dtype=full)
        hk.mixed_precision.set_policy(ShardedEncoder, policy)

        # Remove it in batch norm to avoid instabilities
        policy = jmp.Policy(compute_dtype=full, param_dtype=full, output_dtype=half)
        hk.mixed_precision.set_policy(hk.BatchNorm, policy)
        hk.mixed_precision.set_policy(hk.LayerNorm, policy)

    def sharded_transformer_fn(
        tokens: Tokens, attention_mask: Optional[AttentionMask] = None
    ) -> TransformerOutput:
        transformer = ShardedEncoder(
            config=model_config, num_shards=num_shards, name=name
        )
        outs = transformer(
            tokens=tokens,
            attention_mask=attention_mask,
        )
        return outs

    return sharded_transformer_fn
