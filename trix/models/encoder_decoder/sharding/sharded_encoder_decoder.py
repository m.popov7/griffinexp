from typing import Callable, Optional, Tuple

from trix.models.encoder_decoder.model import EncoderDecoder
from trix.models.encoder_decoder.sharding.sharded_decoder import (
    DecoderConfig,
    ShardedDecoder,
)
from trix.models.encoder_decoder.sharding.sharded_encoder import (
    EncoderConfig,
    ShardedEncoder,
)
from trix.types import AttentionMask, Tokens, TransformerOutput


class ShardedEncoderDecoder(EncoderDecoder):
    """
    Creates a sharded EncoderDecoder made of a ShardedEncoder and a ShardedDecoder.
    Sharding is done on attention blocks where attention heads are sharded as well as
    MLP layers.
    """

    def __init__(
        self,
        encoder_config: EncoderConfig,
        decoder_config: DecoderConfig,
        num_shards: int,
        name: Optional[str] = None,
    ):
        """
        Initializes the Encoder stack and Decoder stack.

        Args:
            encoder_config: Dataclass containing encoder hyperparameters.
            decoder_config: Dataclass containing decoder hyperparameters.
            num_shards: Number of shards to shard the model.
            name: Name for module.
        """
        super().__init__(
            encoder_config=encoder_config, decoder_config=decoder_config, name=name
        )

        self._encoder_config = encoder_config
        self._decoder_config = decoder_config
        self._num_shards = num_shards

        self._encoder = ShardedEncoder(encoder_config, num_shards=num_shards)
        self._decoder = ShardedDecoder(decoder_config, num_shards)


def build_sharded_encoder_decoder_fn(
    encoder_config: EncoderConfig,
    decoder_config: DecoderConfig,
    num_shards: int,
) -> Callable:
    """
    Create the model's forward pass.

    Args:
        encoder_config: Encoder configuration (model hyperparameters).
        decoder_config: Decoder configuration (model hyperparameters).
        num_shards: Number of shards to shard the model.

    Returns:
        Encoder Decoder model forward function.
    """

    def sharded_encoder_decoder_fn(
        encoder_tokens: Tokens,
        decoder_tokens: Tokens,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_attention_mask: Optional[AttentionMask] = None,
        decoder_cross_attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[TransformerOutput, TransformerOutput]:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder_decoder = ShardedEncoderDecoder(
            encoder_config, decoder_config, num_shards
        )
        decoder_outs, encoder_outs = encoder_decoder(
            encoder_tokens=encoder_tokens,
            decoder_tokens=decoder_tokens,
            encoder_attention_mask=encoder_attention_mask,
            decoder_attention_mask=decoder_attention_mask,
            decoder_cross_attention_mask=decoder_cross_attention_mask,
        )
        return encoder_outs, decoder_outs

    return sharded_encoder_decoder_fn
