from dataclasses import dataclass
from typing import Callable, Dict, List, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.positional.learned import LearnedPositionalEmbeddings
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


@dataclass
class DecisionTransformerConfig:
    """
    Parameters to initialize a visual transformer model.

    Args:
        embed_dim: Embedding dimension.
        num_attention_heads: Number of attention heads.
        num_layers: Number of attention blocks.
        ffn_embed_dim: Dimension of the intermediate hidden layer at the output of the
            attention.
        num_classes: Number of classes when using a classification head on top of the
            ViT model.
        key_size: Size of the key in the attention.
        use_gradient_checkpointing: Use for gradient checkpointing during training.
        action_tanh: If True, applies tanh to predictions.
    """

    embed_dim: int
    num_attention_heads: int
    num_layers: int
    ffn_embed_dim: int
    action_tanh: bool
    num_classes: int = 1000
    key_size: Optional[int] = None
    # gradient checkpointing
    use_gradient_checkpointing: bool = False

    def __post_init__(self) -> None:
        """
        Checks that the given values are compatible.
        """
        if self.key_size is None:
            if not self.embed_dim % self.num_attention_heads == 0:
                raise ValueError(
                    f"When no key size is provided, the embedding dimension should be "
                    f"divisible by the number of heads, however provided embedding "
                    f"dimension is {self.embed_dim} and the number of heads is "
                    f"{self.num_attention_heads}."
                )
            self.key_size = self.embed_dim // self.num_attention_heads


class DecisionTransformer(hk.Module):
    def __init__(
        self,
        config: DecisionTransformerConfig,
        action_size: int,
        observation_size: int,
        name: Optional[str] = None,
    ) -> None:

        super().__init__(name=name)
        self._config = config
        self._action_size = action_size
        self._observation_size = observation_size
        self._action_tanh = config.action_tanh

        self._returns_to_go_embed_layer = hk.Linear(self._config.embed_dim)
        self._obs_embed_layer = hk.Linear(self._config.embed_dim)
        self._actions_embed_layer = hk.Linear(self._config.embed_dim)

        self._layer_norm_1 = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True
        )

        self._layer_norm_2 = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True
        )

    @hk.transparent
    def compute_embeddings(
        self, tokens: Tokens, positions: Tokens
    ) -> Tuple[jnp.ndarray, jnp.ndarray, jnp.ndarray]:
        """_summary_

        Args:
            tokens: Interleaved tokens, containing returns_to_go, observations and
                actions information.

        Returns:
            The embeddings for each of the objects (returns_to_go, observations and
                actions).
        """
        seq_len = tokens.shape[1]
        episode_length = seq_len // 3
        returns_to_go, obs, actions, positions = self._preprocess_tokens(
            tokens, positions
        )

        # Embed tokens
        returns_to_go_embeddings = self._returns_to_go_embed_layer(returns_to_go)
        obs_embeddings = self._obs_embed_layer(obs)
        actions_embeddings = self._actions_embed_layer(actions)

        # Compute time embeddings
        time_embeddings = LearnedPositionalEmbeddings(
            max_positions=episode_length,
            embed_dim=self._config.embed_dim,
        )(tokens=tokens, positions=positions)
        # Time embeddings are treated similar to positional embeddings
        returns_to_go_embeddings = returns_to_go_embeddings + time_embeddings
        obs_embeddings = obs_embeddings + time_embeddings
        actions_embeddings = actions_embeddings + time_embeddings
        return returns_to_go_embeddings, obs_embeddings, actions_embeddings

    @hk.transparent
    def apply_attention_blocks(
        self,
        x: Embedding,
        outs: Dict[str, Embedding],
        attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[Embedding, Dict[str, Embedding]]:
        """
        Creates the blocks of attention layers and applies them.

        Args:
            x: Sequence embedding of shape (batch,seq_len,embed_dim).
            outs: A dictionary to carry through the attention layers which stores the
                intermediate sequence embedding and attention maps.
            attention_mask: attention mask of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            Output sequence embedding.
            Dictionary of optional intermediate results (embeddings of the layer and
            attention weights).
        """

        layers: List[Callable] = [
            self._self_attention(layer_idx)
            for layer_idx in range(self._config.num_layers)
        ]

        if self._config.use_gradient_checkpointing:
            # the remat-ed function cannot take control flow arguments
            layers = [hk.remat(layer) for layer in layers]

        for _, layer in enumerate(layers):
            output = layer(
                x=x,
                attention_mask=attention_mask,
            )
            x = output["embeddings"]

            # Sum attention maps over heads.
            outs["attention_weights"] = jnp.sum(output["attention_weights"], axis=1)
        return x, outs

    @hk.transparent
    def _preprocess_tokens(
        self,
        tokens: Tokens,
        positions: Tokens,
    ) -> Tuple[jnp.ndarray, jnp.ndarray, jnp.ndarray, jnp.ndarray]:
        """
        Extracts returns_to_go, observations and actions from the interleaved tokens.

        Args:
            tokens: Interleaved tokens.

        Returns:
            Returns-to-go.
            Observations.
            Actions.
            Positions.
        """
        seq_len = tokens.shape[1]
        episode_length = seq_len // 3

        # Returns to go are at index (0,3,6,...)
        returns_to_go_index = jnp.arange(episode_length) * 3
        obs_index = returns_to_go_index + 1
        actions_index = returns_to_go_index + 2

        returns_to_go = tokens[:, returns_to_go_index][..., :1]
        obs = tokens[:, obs_index][..., : self._observation_size]
        actions = tokens[:, actions_index][..., : self._action_size]
        positions = positions[:, returns_to_go_index]
        return (returns_to_go, obs, actions, positions)

    def _action_head(self, x: jnp.ndarray) -> jnp.ndarray:
        pred_actions = hk.Linear(self._action_size)(x[:, 1])
        return pred_actions

    @hk.transparent
    def _self_attention(self, layer_idx: int) -> SelfAttentionBlock:

        return SelfAttentionBlock(  # type: ignore
            num_heads=self._config.num_attention_heads,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            name=f"self_attention_block_{layer_idx}",
        )

    def __call__(
        self,
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:

        # Retrieve objects from tokenized inputs
        positions = tokens["positions"]
        tokens = tokens["tokens"]

        # Get basic info
        batch_size = tokens.shape[0]
        seq_len = tokens.shape[1]

        # Preprocess tokens and extract embeddings
        (
            returns_to_go_embeddings,
            obs_embeddings,
            actions_embeddings,
        ) = self.compute_embeddings(tokens, positions)

        # Interleave tokens as (r_1, s_1, a_1, r_2, s_2, a_2, ...)
        x = jnp.stack(
            (returns_to_go_embeddings, obs_embeddings, actions_embeddings), axis=1
        )
        x = jnp.transpose(x, axes=(0, 2, 1, 3))
        x = x.reshape(batch_size, seq_len, self._config.embed_dim)

        outs: Dict[str, jnp.ndarray] = {}

        x = self._layer_norm_1(x)

        # Attention mask
        if attention_mask is None:
            attention_mask = jnp.tril(jnp.ones((batch_size, 1, seq_len, seq_len)))

        # Construct a tower of attention layers
        x, outs = self.apply_attention_blocks(
            x=x,
            outs=outs,
            attention_mask=attention_mask,
        )

        x = self._layer_norm_2(x)

        # Save final embeddings if needed
        outs["embeddings"] = x

        # Reorganize output such that each timestep is separated
        # output[:,0,t] is the token for r_t, output[:,1,t] is the token for s_t etc.
        x = x.reshape(batch_size, seq_len // 3, 3, self._config.embed_dim)
        x = jnp.transpose(x, axes=(0, 2, 1, 3))

        pred_actions = self._action_head(x)
        if self._action_tanh:
            pred_actions = jnp.tanh(pred_actions)

        outs["predictions"] = pred_actions
        return outs  # type: ignore


def build_decision_transformer_forward_fn(
    model_config: DecisionTransformerConfig,
    action_size: int,
    observation_size: int,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        model_config: Model parameters.
        action_size: The dimension of the action vector.
        observation_size: The dimension of the observation vector.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        name: Name to give to the transformer module.

    Returns:
        Transformer model forward function.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(DecisionTransformer, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def forward_fn(
        tokens: Tokens, attention_mask: Optional[AttentionMask] = None
    ) -> Dict[str, jnp.ndarray]:

        module = DecisionTransformer(
            config=model_config,
            action_size=action_size,
            observation_size=observation_size,
            name=name,
        )
        return module(tokens, attention_mask)  # type: ignore

    return forward_fn
