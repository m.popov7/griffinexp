"""Implementation of the Ankh architecture."""
from typing import Callable, Optional

import jax.numpy as jnp

from trix.models.ankh.layers import ConvBertConfig, ConvBertHead
from trix.models.t5.model import T5Config, T5Encoder, T5EncoderConfig, T5Transformer
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput
from trix.utils.masking import build_padding_attention_mask


def build_ankh_encoder_only_fn(encoder_config: T5EncoderConfig) -> Callable:
    """
    Initializes a function to run the Encoder stack from input token ids
    to output embeddings.  With a pretrained model, this can be used to
    generate the embeddings that are needed to train and run the downstream
    protein analysis tasks discussed in the Ankh paper.

    Args:
        encoder_config: Configuration data class containing the needed parameters.

    Returns:
        The encoder function
    """

    def ankh_encoder_fn(
        encoder_token_ids: jnp.ndarray,
        encoder_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Function to compute the encoder's output embeddings from a

        Args:
            encoder_token_ids: Input sequence of token ids of shape (batch_size,
                seq_len).
            encoder_attention_mask: Input attention mask of shape (batch_size, 1,
                seq_len, seq_len).
            dropout_rate: Fraction of components set to 0 during training.

        Returns:
            The output of model.encode
        """
        model = T5Encoder(config=encoder_config, name="ankh_encoder")

        return model(
            token_ids=encoder_token_ids,
            attention_mask=encoder_attention_mask,
            dropout_rate=dropout_rate,
        )

    return ankh_encoder_fn


def build_ankh_encoder_and_task_fn(
    encoder_config: T5EncoderConfig, head_config: ConvBertConfig
) -> Callable:
    """
    Initializes a function to run the encoder and also train and run downstream
    protein analysis tasks using ConvBert layers.  Note: Using this function
    as is will cause the entire encoder to train during fine-tuning, while using
    build_encoder_task_fn will create a function that executes the encoder stack and
    ConvBert layers while only training the latter.

    Args:
        encoder_config: Configuration data class containing the encoder hyperparameters.
        head_config: Configuration data class containing the head hyperparameters.

    Returns:
        The encoder function.
    """

    def anh_encoder_and_task_fn(
        encoder_token_ids: jnp.ndarray,
        encoder_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Function to compute the encoder's output embeddings and then analyze
        protein sequences using ConvBert layers.

        Args:
            encoder_token_ids: Input sequence of token ids of shape (batch_size,
                seq_len).
            dropout_rate: Fraction of components set to 0 during training.

        Returns:
            The output embeddings.
        """
        model = T5Encoder(encoder_config, name="ankh_encoder")
        conv_bert_model = ConvBertHead(head_config)
        if encoder_attention_mask is None:
            encoder_attention_mask = build_padding_attention_mask(
                encoder_token_ids, encoder_config.pad_token_id
            )
        outs = model(
            token_ids=encoder_token_ids,
            attention_mask=encoder_attention_mask,
            dropout_rate=dropout_rate,
        )
        encoder_output = outs["embeddings"]
        task_out = conv_bert_model(
            encoder_output=encoder_output,
            attention_mask=encoder_attention_mask,
            dropout_rate=dropout_rate,
        )
        outs.update(task_out)
        return outs

    return anh_encoder_and_task_fn


def build_ankh_convbert_head_fn(conv_bert_config: ConvBertConfig) -> Callable:
    """
    Creates a function to a ConvBert head that takes as input encoder outputs. Useful
    for fine-tuning following Ankh strategy.

    Args:
        conv_bert_config: Model configuration.

    Returns:
        A prediction ConvBert head.
    """

    def ankh_convbert_head_fn(
        encoder_output: Embedding,
        attention_mask: AttentionMask,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        ConvBert head.

        Args:
            encoder_output: Output of the encoder, of shape (batch_size, seq_len,
                embed_dim).
            attention_mask: Attention mask computed from the input tokens, of shape
                (batch_size, 1, seq_len, seq_len).
            dropout_rate: Dropout rate, defaults to 0.
        """
        head = ConvBertHead(conv_bert_config)
        return head(
            encoder_output=encoder_output,
            attention_mask=attention_mask,
            dropout_rate=dropout_rate,
        )

    return ankh_convbert_head_fn


def build_ankh_transformer_fn(config: T5Config) -> Callable:
    """
    Initializes a function to run the Ankh encoder-decoder stack, which is used to
    generate new proteins by masking known sequences.

    Args:
        config: Configuration data class containing the other needed parameters.

    Returns:
        The encoder function.
    """

    def ankh_transformer_fn(
        encoder_token_ids: Tokens,
        decoder_token_ids: Tokens,
        encoder_attention_mask: Optional[AttentionMask] = None,
        decoder_self_attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
        dropout_rate: float = 0.0,
    ) -> jnp.ndarray:
        """
        Function to compute the encoder's output embeddings from a

        Args:
            encoder_token_ids: Input sequence of token ids fed to the encoder
                of shape (batch_size, seq_len).
            decoder_token_ids: Input sequence of token ids fed to the decoder
                of shape (batch_size, seq_len).  During inference, each sequence
                should be length 1, containing only the pad token (id = 0).
            encoder_attention_mask: Mask for the self-attention in the encoder, of shape
                (batch_size, 1, seq_len, seq_len). If none is given, will mask only
                pad tokens.
            decoder_self_attention_mask: Mask for the self-attention in the decoder, of
                shape (batch_size, 1, seq_len, seq_len). If none is given, will default
                to a causal mask that also masks pad tokens.
            cross_attention_mask: Mask for the cross-attention in the decoder, of shape
                (batch_size, 1, seq_len). If none is given, will mask only pad tokens.
            dropout_rate: Fraction of components set to 0 during training.

        Returns:
            The output embeddings
        """
        model = T5Transformer(config, name="ankh_transformer")
        return model(
            encoder_token_ids=encoder_token_ids,
            decoder_token_ids=decoder_token_ids,
            encoder_attention_mask=encoder_attention_mask,
            decoder_self_attention_mask=decoder_self_attention_mask,
            cross_attention_mask=cross_attention_mask,
            dropout_rate=dropout_rate,
        )

    return ankh_transformer_fn
