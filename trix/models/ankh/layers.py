from dataclasses import dataclass
from functools import partial
from typing import Callable, Optional, Union

import haiku as hk
import jax
import jax.numpy as jnp

from trix.types import AttentionMask, Embedding, TransformerOutput


@dataclass
class ConvBertConfig:
    """
    Parameters to initialize a Conv Bert head, including the T5 base model and the
    ConvBertLayer at the end. Defaults to the values for Ankh-base and Solubility
    prediction task.

    Args:
        embed_dim: Embedding dimension (input).
        conv_num_layers: Number of convolutional BERT layers to attach at the end
            for downstream protein analysis tasks.
        conv_num_heads: Number of heads in convolutional self-attention
        head_ratio: Reduction in embedding size in ConvBert self-attention
            (currently must be 2).
        conv_fc_embed_dim: Dimension of the hidden layer in the ConvBert mlp.
        conv_dropout_rate: Fraction of embedding components set to 0 during dropout
            in the convolutional BERT layers.
        conv_bert_task: Downstream protein analysis task, which affects the structure
            of the ConvBertHead.  Select from ['solubility','fluorescence',
            'secondary_structure-3', 'secondary_structure-8','fold',
            'gb1_fitness', 'localization', 'generation'].  Or set an integer number of
            labels for classification (1 label for regression or binary classification).
        pooling: How to combine the ConvBertLayer output embeddings when the task only
            requires a single embedding.  Choices are 'max' and 'avg.'
    """

    embed_dim: int = 768
    conv_num_layers: int = 1
    conv_num_heads: int = 4
    head_ratio: int = 2
    kernel_size: int = 7
    conv_fc_embed_dim: int = 384
    conv_dropout_rate: float = 0.2  # Unclear if Ankh uses dropout during inference.

    conv_bert_task: Optional[Union[str, int]] = 1
    pooling: Optional[str] = "max"  # 'max' or 'avg'

    def __post_init__(self) -> None:
        """
        Checks that the given values are compatible.
        """
        if self.conv_num_heads % self.head_ratio != 0:
            raise ValueError(
                f"The number of heads ({self.conv_num_heads}) must be divisible by"
                f"the head ratio ({self.head_ratio})."
            )
        if self.head_ratio != 2:
            raise ValueError(
                f"The only value of head ratio ({self.head_ratio}) currently"
                f"supported is 2, in accordance with the original paper and the"
                f"HuggingFace implementation."
            )
        tasks = [
            "solubility",
            "fluorescence",
            "secondary_structure-3",
            "generation",
            "secondary_structure-8",
            "fold",
            "gb1_fitness",
            "localization",
        ]
        if type(self.conv_bert_task) == str:
            self.conv_bert_task = self.conv_bert_task.lower()
            if self.conv_bert_task not in tasks:
                raise ValueError(
                    f"Given unsupported task {self.conv_bert_task}.  Please select from"
                    f"{tasks} or set an integer number of classes for classification"
                )
        if self.conv_bert_task in [
            "fluorescence",
            "gb1_fitness",
            "solubility",
            "fold",
            "localization",
        ]:
            if self.pooling not in ["max", "avg", "mean"]:
                raise ValueError(
                    f"Pooling method ({self.pooling}) must be one of ['max','avg'] for"
                    f"this task."
                )
        if self.conv_bert_task in ["secondary_structure-3", "secondary_structure-8"]:
            if self.pooling is not None:
                raise ValueError(
                    f"Pooling method ({self.pooling}) must be None for this task."
                )


class ConvBertHead(hk.Module):
    """
    Creates a stack of ConvBert layers, which takes the output embeddings from the
    Ankh Encoder and completes a downstream protein classification task.
    """

    def __init__(self, config: ConvBertConfig, name: Optional[str] = None):
        """
        Initializes the Encoder stack for the T5 Encoder Decoder model.  Defaults to the
        hyperparameters used by T5_Small

        Args:
           config: Configuration data class containing the needed parameters.
           name: Optional name for this module.
        """

        super().__init__(name=name)
        self.layers = [
            ConvBertLayer(
                config.embed_dim,
                config.conv_num_heads,
                config.kernel_size,
                config.head_ratio,
                config.conv_fc_embed_dim,
            )
            for _ in range(config.conv_num_layers)
        ]

        task = config.conv_bert_task
        # 'contact' requires binary classification for all pairs of residues
        #  Not yet sure how to implement

        if type(task) == int:
            self.final_linear = hk.Linear(output_size=task, name="final_linear")
        elif task in ["fluorescence", "gb1_fitness", "solubility"]:
            self.final_linear = hk.Linear(output_size=1, name="final_linear")
        elif task in ["secondary_structure-3", "secondary_structure-8"]:
            assert config.pooling is None
            self.final_linear = hk.Linear(
                output_size=int(task[-1]), name="final_linear"  # type: ignore
            )
        elif task == "fold":
            self.final_linear = hk.Linear(output_size=1194, name="final_linear")
        elif task == "localization":
            self.final_linear = hk.Linear(output_size=10, name="final_linear")
        else:
            raise ValueError

        self.pooling: Union[partial, Callable]
        if config.pooling == "max":

            def masked_max_pooling(
                x: jnp.ndarray, sequence_mask: jnp.ndarray
            ) -> jnp.ndarray:
                y = x - 1e30 * (1 - sequence_mask)
                return jnp.max(y, axis=1)

            self.pooling = masked_max_pooling

        elif config.pooling in ["avg", "mean"]:

            def masked_average_pooling(
                x: jnp.ndarray, sequence_mask: jnp.ndarray
            ) -> jnp.ndarray:
                return jnp.sum(x * sequence_mask, axis=1) / (
                    jnp.sum(sequence_mask, axis=1) + 1e-30
                )

            self.pooling = masked_average_pooling
        else:
            self.pooling = lambda x, _: x

    def __call__(
        self,
        encoder_output: Embedding,
        attention_mask: AttentionMask,
        dropout_rate: float = 0.0,
    ) -> TransformerOutput:
        """
        Complete protein characterization task based on output embeddings from the
        encoder.

        Args:
            encoder_output: Output embeddings from the Ankh encoder of shape
                (batch_size, seq_len, embed_dim).
            attention_mask: Mask used in the convolutional self-attention layer
            dropout_rate: Fraction of components to be set to 0 during dropout

        Returns:
            The final classification produced by the ConvBERT layers, pooling, and final
            linear layer.
        """
        task_out: TransformerOutput = {}
        sequence_mask = jnp.expand_dims(  # type: ignore
            attention_mask[:, 0, :, 0], axis=-1
        )
        # mask pad token embeddings before convolutional attention
        x = encoder_output * sequence_mask
        for layer in self.layers:
            x = layer(
                token_embed_seq=x,
                dropout_rate=dropout_rate,
                attention_mask=attention_mask,
            )
        x = self.pooling(x, sequence_mask)
        x = self.final_linear(x)
        task_out["output"] = x
        return task_out


class SeparableConv1D(hk.Module):
    """
    This class implements separable convolution, i.e. a depthwise and then pointwise
    convolution on the last two dimensions of the input. In Haiku, dim=-2 is
    the convolution input, and dim=-1 is the set of channels
    """

    def __init__(
        self,
        input_channels: int,
        output_channels: int,
        kernel_size: int,
        name: Optional[str] = "separable_conv",
    ):
        super().__init__(name=name)
        """
        Initializes the class.

        Args:
            input_channels: The number of channels in the input "image."
            output_channels: The number of channels in the output.
            kernel_size: Length of the 1D convolutional kernel in the depthwise
                convolution.
            name: Optional name for this module.
        """

        # depth_init = hk.initializers.RandomNormal(stddev=)  initializer_range?
        # Performs a separate convolution for each channel
        self.depthwise = hk.Conv1D(
            output_channels=input_channels,
            kernel_shape=kernel_size,
            feature_group_count=input_channels,
            with_bias=False,
            name="depth_conv1d",
        )
        self.pointwise = hk.Conv1D(
            output_channels, kernel_shape=1, with_bias=False, name="point_conv1d"
        )
        self.bias = hk.get_parameter(
            name="b", shape=(1, output_channels), dtype=float, init=jnp.zeros
        )

    def __call__(self, input_embed: jnp.ndarray) -> jnp.ndarray:
        """
        Performs the separable convolution, first depthwise, and then pointwise.

        Args:
            input_embed: In the context of NLP, the sequence embeddings of
                shape (batch_size, embed_dim, seq_len)

        Returns:
            The result of the separable convolution.
        """
        # The depthwise convolution does not change the shape of the input by design
        x = self.depthwise(input_embed)  # (batch_size, input_length, input_channels)
        x = self.pointwise(x)  # (batch_size, input_length, output_channels)
        x += self.bias
        return x


class ConvBertAttention(hk.Module):
    """
    Multi-head attention with convolution as described in the convBERT paper:
    https://arxiv.org/pdf/2008.02496.pdf.

    """

    def __init__(
        self,
        embed_dim: int,
        num_heads: int,
        kernel_size: int,
        head_ratio: int = 2,
        name: Optional[str] = "conv_bert_attention",
    ):
        """
        Initializes the attention layer.

        Args:
            embed_dim: Length of the token embeddings before reduction by head_ratio.
            num_heads: Number of attention heads, before reduction by head_ratio.
            kernel_size: Length of the 1-d convolutional kernels.
            head_ratio: Factor of reduction for the attention heads and embed_dim.
            name: Optional name for this module.
        """
        super().__init__(name=name)

        self.key_size = embed_dim // num_heads
        self.embed_dim = embed_dim
        self.kernel_size = kernel_size
        self.num_heads = num_heads // head_ratio
        self.new_embed_dim = embed_dim // head_ratio

        self.key_linear = hk.Linear(output_size=self.new_embed_dim, name="key_linear")
        self.query_linear = hk.Linear(
            output_size=self.new_embed_dim, name="query_linear"
        )
        self.value_linear = hk.Linear(
            output_size=self.new_embed_dim, name="value_linear"
        )
        self.out_linear = hk.Linear(output_size=self.embed_dim, name="out_linear")

        self.separable_conv = SeparableConv1D(
            self.embed_dim, self.new_embed_dim, self.kernel_size, name="separable_conv"
        )
        self.conv_kernel_linear = hk.Linear(
            output_size=self.num_heads * self.kernel_size, name="conv_kernel_linear"
        )
        self.conv_out_linear = hk.Linear(self.new_embed_dim, name="conv_out_linear")

    def __call__(
        self,
        query_inputs: jnp.ndarray,
        key_inputs: jnp.ndarray,
        value_inputs: jnp.ndarray,
        dropout_rate: float = 0.1,
        attention_mask: Optional[jnp.ndarray] = None,
    ) -> jnp.ndarray:
        """
        Computes the result of convolutional multiheaded dot-product attention.
        Combines a reduced normal attention among the keys, queries, values with
        a combination involving convolutions of the key_inputs, value_inputs, and
        queries.

        Args:
            query_inputs: Input embeddings to be projected to become the queries.
            key_inputs: Input embeddings to be projected to become the keys.
            value_inputs: Input embeddings to be projected to become the values
            dropout_rate: Fraction of tokens to set to 0 in the attention_weights.
            attention_mask: Mask to be applied in the attention layers.
                Triangular for autoregressive models.

        Returns:
            The output embeddings from convolutional multi-headed attention.
        """
        # Perform the separable convolution on the key_inputs to get K_s.
        conv_keys = self.separable_conv(
            key_inputs
        )  # (batch_size, seq_len, new_embed_dim)

        keys = self.key_linear(key_inputs)  # (batch_size, seq_len, new_embed_dim)
        queries = self.query_linear(query_inputs)
        values = self.value_linear(value_inputs)

        conv_product = conv_keys * queries

        keys = keys.reshape(*keys.shape[:-1], self.num_heads, -1)
        queries = queries.reshape(*queries.shape[:-1], self.num_heads, -1)

        attention_logits = jnp.einsum(
            "...thd,...Thd->...htT", queries, keys
        ) * jax.lax.rsqrt(self.key_size * 1.0)

        if attention_mask is not None:
            attention_logits = jnp.where(attention_mask, attention_logits, -1e30)
        attention_weights = jax.nn.softmax(attention_logits, axis=-1)
        attention_weights = hk.dropout(
            hk.next_rng_key(), dropout_rate, attention_weights
        )  # Follows HF

        values = values.reshape(*values.shape[:-1], self.num_heads, -1)
        attention_result = jnp.einsum(
            "...htT,...Thd->...thd", attention_weights, values
        )
        attention_result = attention_result.reshape(*attention_result.shape[:-2], -1)

        conv_kernel = self.conv_kernel_linear(conv_product)
        conv_kernel = conv_kernel.reshape(
            *conv_kernel.shape[:-1], self.num_heads, -1
        )  # (batch_size, seq_len, num_heads, kernel_size)
        # All conv_kernel heads at position i in seq_len should be the kernels which
        # dot into the corresponding heads at positions i:i+k of the seq in conv_input
        conv_kernel = jax.nn.softmax(conv_kernel, axis=-1)

        # Create a sliding window of kernels with padding so that the convolution
        # can be handled through matrix multiplication.
        # There must be a better way to do this.
        kernel_shape = conv_kernel.shape
        pad_left = (self.kernel_size - 1) // 2
        pad_right = self.kernel_size // 2
        conv_kernel_temp = jnp.zeros(
            (*kernel_shape[:-1], kernel_shape[1] + pad_left + pad_right)
        )
        for j in range(conv_kernel.shape[1]):
            conv_kernel_temp = conv_kernel_temp.at[
                :, j, :, j : j + self.kernel_size
            ].set(conv_kernel[:, j, :])
        conv_kernel = conv_kernel_temp

        conv_input = self.conv_out_linear(
            value_inputs
        )  # (batch_size, seq_len, new_embed_dim)
        conv_shape = conv_input.shape
        conv_input = jnp.concatenate(
            (
                jnp.zeros((conv_shape[0], pad_left, conv_shape[-1])),
                conv_input,
                jnp.zeros((conv_shape[0], pad_right, conv_shape[-1])),
            ),
            axis=1,
        )
        conv_shape = conv_input.shape  # (batch_size, seq_len + pad, new_embed_dim)
        conv_input = conv_input.reshape(*conv_shape[:-1], self.num_heads, -1)

        conv_kernel = conv_kernel.transpose((0, 2, 1, 3))
        conv_input = conv_input.transpose((0, 2, 1, 3))
        conv_output = conv_kernel @ conv_input
        conv_output = conv_output.transpose((0, 2, 1, 3)).reshape(*kernel_shape[:2], -1)

        final_output = jnp.concatenate((attention_result, conv_output), axis=-1)
        return self.out_linear(final_output)


class ConvBertLayer(hk.Module):
    """
    Full layer in a ConvBERT encoder, including convolutional self-attention
    and a fully connected layer, following  https://arxiv.org/pdf/2008.02496.pdf.
    Defaults to the values for the last layer of the Ankh network.
    """

    def __init__(
        self,
        embed_dim: int,
        num_heads: int,
        kernel_size: int,
        head_ratio: int,
        fc_embed_dim: int,
        name: Optional[str] = "conv_bert_layer",
    ):
        """
        Initializes the convbert encoder layer.

        Args:
            embed_dim: Length of the token embeddings before reduction by head_ratio.
            num_heads: Number of attention heads, before reduction by head_ratio.
            kernel_size: Length of the 1-d convolutional kernels.
            head_ratio: Factor of reduction for the attention heads and embed_dim.
            fc_embed_dim: Length of the hidden layer in the fully connected network.
            name: Optional name for this module.
        """
        super().__init__(name=name)

        self.num_heads = num_heads
        self.conv_bert_attention = ConvBertAttention(
            embed_dim=embed_dim,
            num_heads=num_heads,
            kernel_size=kernel_size,
            head_ratio=head_ratio,
        )

        # No layer norm before self-attention.  Appears to be handled before
        # ConvBertLayer isvcalled in Huggingface, meaning it isn't used in Ankh.
        self.fc_layer_norm = hk.LayerNorm(
            axis=-1,
            create_scale=True,
            create_offset=True,
            eps=1e-12,
            name="fc_layer_norm",
        )
        self.final_layer_norm = hk.LayerNorm(
            axis=-1,
            create_scale=True,
            create_offset=True,
            eps=1e-12,
            name="final_layer_norm",
        )

        self.fc1_linear = hk.Linear(output_size=fc_embed_dim, name="fc1_linear")
        self.fc2_linear = hk.Linear(output_size=embed_dim, name="fc2_linear")

    @hk.transparent
    def mlp(self, x: jnp.ndarray) -> jnp.ndarray:
        """
        Applies one linear layer, a gelu activation, dropout, then a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, embed_dim).

        Returns:
            The transformed sequence embedding.
        """
        x = jax.nn.gelu(
            self.fc1_linear(x), False
        )  # Later: implement the ConvBert grouping procedure
        return self.fc2_linear(x)

    def __call__(
        self,
        token_embed_seq: jnp.ndarray,
        dropout_rate: float = 0.0,
        attention_mask: Optional[AttentionMask] = None,
    ) -> jnp.ndarray:
        """
        Computes the output embeddings of the encoder layer

        Args:
            token_embed_seq: Encoder layer input embeddings of shape
                (batch_size,seq_len,embed_dim).
            dropout_rate: Fraction of tokens randomly set to 0 in dropout layers.
            attention_mask: Mask to be applied during convolutional self attention

        Returns:
            The output embeddings that result from the application of the layer
        """
        x1 = token_embed_seq
        x1 = self.conv_bert_attention(
            x1, x1, x1, dropout_rate, attention_mask=attention_mask
        )
        x1 = hk.dropout(hk.next_rng_key(), dropout_rate, x1) + token_embed_seq

        # Note the slight difference in residual connection from T5
        x1 = self.fc_layer_norm(x1)
        x2 = self.mlp(x1)
        x2 = hk.dropout(hk.next_rng_key(), dropout_rate, x2) + x1

        x2 = self.final_layer_norm(x2)
        return x2
