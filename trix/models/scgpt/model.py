"""Implementation of scGPT in Jax. Similar to the standard transformer class with some
custom layers."""
from __future__ import annotations

from dataclasses import dataclass
from typing import Callable

import haiku as hk
import jax.numpy as jnp

from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.models.scgpt.layers import (
    BatchLabelEncoder,
    ContinuousValueEncoder,
    DomainSpecificBatchNorm1d,
    GeneExpressionEmbeddingEncoder,
    GeneIDEncoder,
    MaskedLMHead,
    MVCDecoder,
)
from trix.types import AttentionMask, SequenceMask, Tokens, TransformerOutput


@dataclass
class ScGptConfig:

    """
    Parameters to initialize a scGPT model.

    Args:
        gene_ids_vocabulary_size: Gene IDs tokens vocabulary size.
        gene_expr_vocabulary_size: Gene expression tokens vocabulary size
            (i.e. number of bins). To be provided only when using gene expression as
            discrete tokens. None otherwise.
        use_discrete_gene_expr: whether to consider gene expr as discrete or
            continuous entities. In the discrete case, table lookup is used for
            embedding, logits are returned and a cross-entropy loss is typically to be
            used for training. In the continuous case, a dense network is used for
            embedding, raw values are predicted and a L2 loss is typically
            used for training.
        embed_dim: Embedding dimension.
        num_heads: Number of attention heads.
        ffn_embed_dim: Feed forward embedding dimension.
        num_layers: Number of attention blocks.
        num_batch_labels: Number of different batch labels.

        use_prediction_head: Whether to use a prediction head.
        use_batch_labels: Whether to append batch labels to the embeddings.
        cell_embedding_style: How to get the cell embedding from the transformer,
            choices: "cls", "avg-pool", "w-pool".
        explicit_zero_prob: Whether to explicitly model the probability of zero values.
        use_mvc_decoder: Whether to use the Cell-Embedding Decoder.
        ffn_activation_name: Name of activation function to use
        use_glu_in_ffn: Whether to use Gated Linear Unit in feed-forward. To use
            swiGLU, set to True and set ffn_activation_name to swish.
    """

    gene_ids_vocabulary_size: int
    embed_dim: int
    num_heads: int
    ffn_embed_dim: int
    num_layers: int
    num_batch_labels: int
    gene_expr_vocabulary_size: int | None = None

    use_discrete_gene_expr: bool = False
    use_prediction_head: bool = True
    use_batch_labels: bool = False
    cell_embedding_style: str = "cls"
    explicit_zero_prob: bool = False
    use_mvc_decoder: bool = False
    ffn_activation_name: str = "relu"
    use_glu_in_ffn: bool = False
    # Option to use mask and predict gene ids
    # in addition to expression values
    use_gene_id_mlm: bool = False

    def __post_init__(self) -> None:
        """
        Sanity checks.
        """
        if self.use_discrete_gene_expr:
            if self.gene_expr_vocabulary_size is None:
                raise ValueError(
                    "Gene expr vocabulary size must be provided when considering "
                    "gene expression as discrete tokens."
                )

        if self.use_batch_labels:
            if self.num_batch_labels is None:
                raise ValueError(
                    "num_batch_labels needs to be defined when using batch labels."
                )


class ScGptTransformer(hk.Module):
    def __init__(self, config: ScGptConfig, name: str | None = None):
        super().__init__(name=name)

        self._config = config

        self.encoder = GeneIDEncoder(
            num_embeddings=self._config.gene_ids_vocabulary_size,
            embedding_dim=self._config.embed_dim,
            name="encoder",
        )

        if self._config.use_discrete_gene_expr:
            self.value_encoder = GeneExpressionEmbeddingEncoder(
                vocab_size=self._config.gene_expr_vocabulary_size,  # type: ignore
                embed_dim=self._config.embed_dim,
                name="value_encoder",
            )
        else:
            self.value_encoder = ContinuousValueEncoder(
                embed_dim=self._config.embed_dim, name="value_encoder"
            )

        # Batch Encoder
        if self._config.use_batch_labels:
            self.batch_encoder = BatchLabelEncoder(
                num_embeddings=self._config.num_batch_labels,
                embedding_dim=self._config.embed_dim,
            )

        self.dsbn = DomainSpecificBatchNorm1d(
            self._config.embed_dim,
            eps=6.1e-5,
            affine=False,
        )

        # Define Encoder
        self.encoder_layers = []
        for _ in range(self._config.num_layers):
            self.encoder_layers.append(
                SelfAttentionBlock(
                    num_heads=self._config.num_heads,
                    embed_dim=self._config.embed_dim,
                    ffn_embed_dim=self._config.ffn_embed_dim,
                    ffn_activation_name=self._config.ffn_activation_name,
                    use_glu_in_ffn=self._config.use_glu_in_ffn,
                    pre_layer_norm=False,
                )
            )

        # Define gene expression decoder
        expr_decoder_output_dim = (
            1
            if not self._config.use_discrete_gene_expr
            else self._config.gene_expr_vocabulary_size
        )

        self.expression_lm_head = MaskedLMHead(
            embed_dim=self._config.embed_dim,
            output_dim=expr_decoder_output_dim,  # type: ignore
            explicit_zero_prob=self._config.explicit_zero_prob,
            name="decoder",  # name kept for backwards compatibility with scgpt
        )

        if self._config.use_gene_id_mlm:
            self.gene_id_lm_head = MaskedLMHead(
                embed_dim=self._config.embed_dim,
                output_dim=self._config.gene_ids_vocabulary_size,
                # Do not need to predict zero probs for gene ids
                explicit_zero_prob=False,
                name="gene_id_decoder",
            )

        # Define MVC Decoder
        self.mvc_decoder = MVCDecoder(
            self._config.embed_dim,
            explicit_zero_prob=self._config.explicit_zero_prob,
            use_batch_labels=self._config.use_batch_labels,
        )

    def _encode(
        self,
        gene_indexes: Tokens,
        gene_expression_values: jnp.ndarray,
        attention_mask: AttentionMask,
        is_training: bool,
        cls_embeddings: jnp.ndarray | None = None,
        batch_labels: Tokens | None = None,  # (batch,)
        expression_mask: SequenceMask | None = None,
    ) -> tuple[jnp.ndarray, jnp.ndarray]:
        indexes_embeddings = self.encoder(gene_indexes)  # (batch, seq_len, embsize)
        values_embeddings = self.value_encoder(
            gene_expression_values, expression_mask=expression_mask
        )
        if cls_embeddings is not None:
            indexes_embeddings = indexes_embeddings.at[:, 0, :].set(cls_embeddings)
        total_embeddings = indexes_embeddings + values_embeddings

        if batch_labels is not None and self._config.use_batch_labels:
            batch_label = batch_labels[0]
            total_embeddings = self.dsbn(
                total_embeddings, batch_label, is_training=is_training
            )

        for layer in self.encoder_layers:
            total_embeddings = layer(total_embeddings, attention_mask=attention_mask)[
                "embeddings"
            ]

        return total_embeddings, indexes_embeddings  # (batch, seq_len, embsize)

    def _get_cell_embedding_from_layer(
        self, layer_output: jnp.ndarray, weights: jnp.ndarray | None = None
    ) -> jnp.ndarray:
        """
        Args:
            layer_output: the output of a transformer layer, shape
                [batch_size, seq_len, embsize].

            weights: the weights to use for the weighted pooling.
        Returns:
            The cell embedding.
        """

        if self._config.cell_embedding_style == "cls":
            cell_embedding = layer_output[:, 0, :]  # (batch, embsize)

        else:
            if self._config.cell_embedding_style == "avg-pool":
                cell_embedding = jnp.mean(layer_output, axis=1)
            elif self._config.cell_embedding_style == "w-pool":
                if weights is None:
                    raise ValueError(
                        "weights is required when cell_embedding_style is w-pool"
                    )
                if len(weights.shape) != 2:
                    raise ValueError("weights should be 2D")
                cell_embedding = jnp.sum(layer_output * weights[..., None], axis=1)
                cell_embedding = cell_embedding / jnp.linalg.norm(
                    cell_embedding, ord=2, axis=1, keepdims=True
                )
            else:
                raise NotImplementedError()

        return cell_embedding

    def __call__(
        self,
        gene_indexes: Tokens,
        gene_expression_values: jnp.ndarray,
        attention_mask: AttentionMask,
        cls_embeddings: jnp.ndarray | None = None,
        batch_labels: Tokens | None = None,  # (batch,)
        expression_mask: SequenceMask | None = None,
        is_training: bool = False,
    ) -> TransformerOutput:
        """
        Args:
            gene_indexes: the gene indexes, shape [*, seq_len].
            gene_expression_values: the gene expression values,
                    shape [*, seq_len].
            attention_mask: the attention mask, shape [batch_size, 1, seq_len, seq_len].
            batch_labels: the batch labels, shape [*,].
            expression_mask: the expression mask, shape [*, seq_len]. 1 means
                the gene expression value will be used, 0 means it will be masked.
            is_training: whether the model is in training mode.

        Returns:
            The output of the transformer.
        """

        transformer_output, indexes_embeddings = self._encode(
            gene_indexes=gene_indexes,
            gene_expression_values=gene_expression_values,
            attention_mask=attention_mask,
            batch_labels=batch_labels,
            expression_mask=expression_mask,
            cls_embeddings=cls_embeddings,
            is_training=is_training,
        )

        if self._config.use_batch_labels and batch_labels is not None:
            batch_emb = self.batch_encoder(batch_labels)  # (batch, embsize)

        cell_embedding = self._get_cell_embedding_from_layer(
            transformer_output,
            gene_expression_values,
        )

        if self._config.use_batch_labels and batch_labels is not None:
            batch_emb_repeat = batch_emb[:, None].repeat(
                transformer_output.shape[1], axis=1
            )
            transformer_output = jnp.concatenate(
                [transformer_output, batch_emb_repeat], axis=2
            )

        output = {
            "cell_embedding": cell_embedding,
        }

        if self._config.use_prediction_head:
            decoder_output = self.expression_lm_head(transformer_output)
            output["gene_expression_predictions"] = decoder_output["pred"]
            if self._config.explicit_zero_prob:
                output["zero_probs"] = decoder_output["zero_probs"]

        if self._config.use_gene_id_mlm:
            gene_id_decoder_output = self.gene_id_lm_head(transformer_output)
            output["gene_id_predictions"] = gene_id_decoder_output["pred"]

        if self._config.use_mvc_decoder:
            if self._config.use_batch_labels:
                cell_embedding = jnp.concatenate([cell_embedding, batch_emb], axis=1)

            mvc_decoder_output = self.mvc_decoder(cell_embedding, indexes_embeddings)
            output["mvc_gene_expression_predictions"] = mvc_decoder_output["pred"]
            if self._config.explicit_zero_prob:
                output["mvc_zero_probs"] = mvc_decoder_output["zero_probs"]

        return output


def build_scgpt_fn(config: ScGptConfig) -> Callable:
    def jax_transformer(
        gene_indexes: Tokens,
        gene_expression_values: jnp.ndarray,
        is_training: bool,
        expression_mask: SequenceMask | None = None,
        attention_mask: AttentionMask | None = None,
        batch_labels: Tokens | None = None,
    ) -> TransformerOutput:
        model = ScGptTransformer(config)

        return model(
            gene_indexes=gene_indexes,
            gene_expression_values=gene_expression_values,
            attention_mask=attention_mask,
            expression_mask=expression_mask,
            is_training=is_training,
            batch_labels=batch_labels,
        )

    return jax_transformer
