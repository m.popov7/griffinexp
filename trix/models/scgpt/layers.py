from __future__ import annotations

from typing import Callable

import haiku as hk
import jax
import jax.numpy as jnp

from trix.types import SequenceMask, Tokens, TransformerOutput


class GeneIDEncoder(hk.Module):
    """
    Encode gene indices to a vector using an embedding layer.
    """

    def __init__(
        self,
        num_embeddings: int,
        embedding_dim: int,
        name: str | None = None,
    ):
        super().__init__(name=name)
        self.embedding = hk.Embed(
            num_embeddings,
            embedding_dim,
            w_init=hk.initializers.RandomUniform(minval=-0.1, maxval=0.1),
        )
        self.enc_norm = hk.LayerNorm(axis=-1, create_offset=True, create_scale=True)

    def __call__(self, x: jnp.ndarray) -> jnp.ndarray:
        x = self.embedding(x)  # (batch, seq_len, embsize)
        x = self.enc_norm(x)
        return x


class ContinuousValueEncoder(hk.Module):
    """
    Encode real number values to a vector using neural nets projection.
    """

    def __init__(
        self,
        embed_dim: int,
        max_value: int = 512,
        name: str | None = None,
    ):
        super().__init__(name=name)
        self.linear1 = hk.Linear(embed_dim)
        self.activation = jax.nn.relu
        self.linear2 = hk.Linear(embed_dim)
        self.norm = hk.LayerNorm(axis=-1, create_offset=True, create_scale=True)
        self.max_value = max_value

    def __call__(
        self, x: jnp.ndarray, expression_mask: SequenceMask | None = None
    ) -> jnp.ndarray:
        x = x[..., None]
        x = jnp.clip(x, a_max=self.max_value)
        x = self.activation(self.linear1(x))
        x = self.linear2(x)
        x = self.norm(x)

        if expression_mask is None:
            expression_mask = jnp.ones(x.shape[:2])

        # If expression_mask is 1 then keep the embedding value, otherwise set it to 0
        x = jnp.where(expression_mask[..., None], x, 0.0)

        return x


class GeneExpressionEmbeddingEncoder(hk.Module):
    """
    Encode gene expression bin ids to a vector using an embedding layer.
    """

    def __init__(
        self,
        vocab_size: int,
        embed_dim: int,
        name: str | None = None,
    ):
        super().__init__()
        self.embedding = hk.Embed(
            vocab_size,
            embed_dim,
            name=name,
        )

    def __call__(
        self, x: jnp.ndarray, expression_mask: SequenceMask | None = None
    ) -> jnp.ndarray:
        """

        Args:
            x: (batch, seq_len)
            expression_mask: (batch, seq_len)
        """
        x = self.embedding(x)  # (batch, seq_len, embsize)
        if expression_mask is None:
            expression_mask = jnp.ones(x.shape[:2])
        # replace masked embeddings with zero.
        x = jnp.where(expression_mask[..., None], x, 0.0)
        return x


class BatchLabelEncoder(hk.Module):
    """
    Encode batch labels to a vector using an embedding layer.
    """

    def __init__(
        self,
        num_embeddings: int,
        embedding_dim: int,
        name: str | None = None,
    ):
        super().__init__()
        self.embedding = hk.Embed(num_embeddings, embedding_dim)
        self.enc_norm = hk.LayerNorm(axis=-1, create_offset=True, create_scale=True)

    def __call__(self, x: jnp.ndarray) -> jnp.ndarray:
        x = self.embedding(x)  # (batch, embsize)
        x = self.enc_norm(x)
        return x


class MaskedLMHead(hk.Module):
    """
    Masked language modeling head for gene and id embeddings.
    """

    def __init__(
        self,
        embed_dim: int,
        output_dim: int = 1,
        explicit_zero_prob: bool = False,
        name: str | None = None,
    ):
        super().__init__(name=name)
        self.output_dim = output_dim
        self.fc = hk.Sequential(
            [
                hk.Linear(embed_dim),
                jax.nn.leaky_relu,
                hk.Linear(embed_dim),
                jax.nn.leaky_relu,
                hk.Linear(output_dim),
            ]
        )
        self.explicit_zero_prob = explicit_zero_prob
        if explicit_zero_prob:
            self.zero_logit = hk.Sequential(
                [
                    hk.Linear(embed_dim),
                    jax.nn.leaky_relu,
                    hk.Linear(embed_dim),
                    jax.nn.leaky_relu,
                    # since this is a probability of
                    # an expression being 0, we will always
                    # output a single value
                    hk.Linear(1),
                ]
            )

    def __call__(self, x: jnp.ndarray) -> dict[str, jnp.ndarray]:
        pred_value = self.fc(x)  # (batch, seq_len, output_dim)
        if self.output_dim == 1:
            pred_value = pred_value.squeeze(axis=-1)  # (batch, seq_len)
        outs = {"pred": pred_value}

        if self.explicit_zero_prob:
            zero_logits = self.zero_logit(x).squeeze(axis=-1)  # (batch, seq_len)
            zero_probs = jax.nn.sigmoid(zero_logits)
            outs["zero_probs"] = zero_probs

        return outs


# The code is modified from https://github.com/wgchang/DSBN/blob/master/model/dsbn.py
class _DomainSpecificBatchNorm(hk.Module):
    def __init__(
        self,
        num_domains: int,
        eps: float = 1e-5,
        momentum: float = 0.1,
        affine: bool = True,
        name: str | None = None,
    ):
        super().__init__(name=name)
        self._cur_domain = None
        self.num_domains = num_domains

        self.bns = [
            self.bn_handle(  # type:ignore
                eps=eps,
                create_scale=affine,
                create_offset=affine,
                decay_rate=1 - momentum,
            )
            for _ in range(num_domains)
        ]

    @property
    def bn_handle(self) -> hk.Module:
        raise NotImplementedError

    def reset_running_stats(self) -> None:
        for bn in self.bns:
            bn.reset_running_stats()

    def reset_parameters(self) -> None:
        for bn in self.bns:
            bn.reset_parameters()

    def _check_input_dim(self, inputs: jnp.ndarray) -> None:
        raise NotImplementedError

    def __call__(
        self, x: jnp.ndarray, domain_label: jnp.ndarray, is_training: bool
    ) -> jnp.ndarray:
        outs_l = []
        for i in range(self.num_domains):
            outs_l.append(
                self.bns[i](x, is_training=is_training, test_local_stats=False)
            )

        outs = jnp.stack(outs_l, axis=-1)
        batch_size = x.shape[0]
        out = outs[jnp.arange(batch_size), ..., domain_label]

        return out


class DomainSpecificBatchNorm1d(_DomainSpecificBatchNorm):
    @property
    def bn_handle(self) -> hk.Module:
        return hk.BatchNorm  # type: ignore

    def _check_input_dim(self, inputs: jnp.ndarray) -> None:
        if len(inputs.shape) > 3:
            raise ValueError(f"expected at most 3D input (got {inputs.shape}D input)")


class MVCDecoder(hk.Module):
    """
    Decoder for the masked value prediction for cell embeddings.
    """

    def __init__(
        self,
        embed_dim: int,
        query_activation: Callable = jax.nn.sigmoid,
        explicit_zero_prob: bool = False,
        use_batch_labels: bool = False,
        name: str | None = None,
    ) -> None:
        super().__init__(name=name)

        self.gene2query = hk.Linear(embed_dim)
        self.query_activation = query_activation

        w_out_dim = embed_dim if not use_batch_labels else 2 * embed_dim
        self.W = hk.Linear(w_out_dim, with_bias=False)
        if explicit_zero_prob:  # by default, gene-wise prob rate
            self.W_zero_logit = hk.Linear(w_out_dim)

        self.explicit_zero_prob = explicit_zero_prob

    def __call__(
        self, cell_embedding: jnp.ndarray, gene_embeddings: jnp.ndarray
    ) -> TransformerOutput:
        query_vecs = self.query_activation(self.gene2query(gene_embeddings))
        cell_emb = cell_embedding[..., None]  # (batch, embsize, 1)

        # the pred gene expr values, # (batch, seq_len)
        def matmul(x: jnp.ndarray, y: jnp.ndarray) -> jnp.ndarray:
            return x @ y

        bmm = jax.vmap(matmul)
        pred_value = bmm(self.W(query_vecs), cell_emb).squeeze(axis=2)

        outs = {"pred": pred_value}

        if self.explicit_zero_prob:
            # zero logits need to based on the cell_emb, because of input exprs
            zero_logits = bmm(self.W_zero_logit(query_vecs), cell_emb).squeeze(axis=2)
            zero_probs = jax.nn.sigmoid(zero_logits)
            outs["zero_probs"] = zero_probs

        return outs


def build_scgpt_gene_encoder_fn(
    num_embeddings: int,
    embedding_dim: int,
    name: str | None = None,
) -> Callable[..., jnp.ndarray]:
    """
    Build a gene encoder function that takes gene indices and outputs gene embeddings.
    Args:
        num_embeddings: number of genes
        embedding_dim: embedding dimension
        name: name of the module
    Returns:
        Model function for encoding gene indices as token embeddings.
    """

    def gene_encoder_fn(gene_ids: Tokens) -> jnp.ndarray:
        """
        Return raw gene id token embeddings.
        Args:
            gene_ids: gene indices, (batch, seq_len)
        Returns:
            token embeddings, (batch, seq_len, embedding_dim)
        """
        gene_encoder = GeneIDEncoder(num_embeddings, embedding_dim, name=name)
        return gene_encoder(gene_ids)

    return gene_encoder_fn
