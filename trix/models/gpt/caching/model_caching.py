from typing import Callable, Optional

import haiku as hk
import jax
import jax.numpy as jnp
import jmp

from trix.models.gpt.caching.layers_caching import GPTDecoderLayerWithCache
from trix.models.gpt.layers import GptDecoderLayer
from trix.models.gpt.model import GptConfig, GptDecoder
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


def build_sliced_causal_attention_mask(
    batch_size: int, seq_len: int, positions: jnp.ndarray, size: int
) -> AttentionMask:
    """
    Builds a batch of causal masks of shape (batch_size, 1, num_tokens, seq_len) to feed
    to an attention layer. num_tokens is the number of token ids inputted in the GPT
    model. Can be a single token for fast decoding or the whole sequence for the
    computing of the prompt's keys and values to fill the keys/values cache.

    Args:
        batch_size: Batch size.
        seq_len: Length of the sequences.
        positions: Position that correspond to the first token id inputted, for each
            token sequence in the batch (batch_size, 1)
        size: Number of tokens inputted

    Returns:
        Batch of causal masks.
    """
    mask = jnp.ones((batch_size, 1, seq_len, seq_len))
    causal_mask = jnp.tril(mask)

    # Slice the part of the triangular attention mask needed for the inputted tokens.
    causal_mask = jax.vmap(
        lambda mask, position: jax.lax.dynamic_slice(
            mask,
            (0, position[0], 0),
            (causal_mask.shape[1], size, seq_len),
        )
    )(causal_mask, positions)

    return causal_mask


class GPTDecoderWithCache(GptDecoder):
    """
    Creates the Gpt model ( decoder only ).
    """

    def __init__(
        self,
        config: GptConfig,
        cache_length: int,
        name: Optional[str] = None,
    ):
        """
        Initializes the Decoder stack.

        Args:
            config: Configuration data class
            name: haiku module name
        """

        super().__init__(config=config, name=name)
        self._cache_length = cache_length
        # logging.warning(
        #     f"be sure to provide enough cache_length ({cache_length}). "
        #     f"If there is not enough, no error will be raised and the"
        #     f" results will be wrong. In order to be sure, always compare"
        #     f" to the classic implementation without cache"
        # )

    @hk.transparent
    def apply_transformer_layers(
        self, tokens_embeddings: Embedding, positions: jnp.ndarray
    ) -> Embedding:
        """
        Takes as inputs the tokens embeddings and apply successively an attention mask
        corresponding to their positions and the transformer layers to obtain
        the corresponding final embeddings ready to be decoded.

        Args:
            tokens_embeddings: Embeddings of the input token ids.
                (batch_size, num_input_tokens, embed_dim)
            positions: Position of the first token embeddings passed in
                tokens_embeddings. (batch_size, 1)


        Returns:
            Embeddings transformed through successive transformer layers.
        """

        # Compute the attention mask corresponding to the tokens inputted
        attention_mask = build_sliced_causal_attention_mask(
            tokens_embeddings.shape[0],
            self._cache_length,
            positions,
            tokens_embeddings.shape[1],
        )

        # go through the transformer layer_stack
        layer_stack = [self._decoder_layer(i) for i in range(self._config.num_layers)]

        # use gradient checkpointing if required
        if self._config.use_gradient_checkpointing:
            # the remat-ed function cannot take control flow arguments
            layer_stack = [hk.remat(layer) for layer in layer_stack]

        embeddings = tokens_embeddings
        for i in range(len(layer_stack)):
            embeddings = layer_stack[i](
                embeddings=embeddings,
                attention_mask=attention_mask,
                positions=positions,
            )

        return embeddings

    @hk.experimental.name_like("__call__")
    def _decoder_layer(self, layer_idx: int) -> GptDecoderLayer:
        """
        gives the GPT encoder layer
        Args:
            layer_idx: the layer index

        Returns:
            The named GPTDecoderLayer
        """
        return GPTDecoderLayerWithCache(
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            num_heads=self._config.num_heads,
            rotary_dim=self._config.rope_dimensions,
            max_position_embeddings=self._config.max_position_embeddings,
            norm_type=self._config.norm_type,
            parallel_attention_ff=self._config.parallel_attention_ff,
            add_bias_ffn=self._config.add_bias_ffn,
            ffn_activation_name=self._config.ffn_activation_name,
            use_glu_in_ffn=self._config.use_glu_in_ffn,
            cache_length=self._cache_length,
            name=f"gpt_decoder_layer_{layer_idx}",
        )

    def __call__(  # type: ignore
        self, token_ids: Tokens, positions: jnp.ndarray
    ) -> TransformerOutput:
        """
        This functions takes a batch of token sequences, the position of the first
        token in each sequence, computes the embeddings/logits corresponding to the
        input tokens with the help of they keys/values already present in the cache,
        and updates the cache with the keys/values that were just computed.

        WARNING: If the necessary keys/values have not been computed and stored in the
        cache yet the computation will be wrong as the cache is initialized with zeros.
        Therefore, the computation for the given tokens will be done assuming that the
        cached keys/values are zeros.

        An example of correct use of this call function is in the case of the decoding
        of the following sequence: tokens_sequence = [1,3,2,5,-1,-1,-1,-1]. Here we
        assume that pad_token_id = -1 and that there is only one sequence in the
        batch.

        1. A first pass is done on the whole sequence in order to compute the
        keys/values corresponding to the prompt by passing the following inputs:
            tokens_ids = [1,3,2,5,-1,-1,-1,-1]
            positions = [0]
        We extract the predicted token with the logits located at index 3: 14 and update
        the token sequence with it: tokens_sequence = [1,3,2,5,14,-1,-1,-1]

        2. We iteratively call the function on the tokens that were just predicted:
           Pseudo code:
           for time_step in [4,5,6,7]:
               Call the function on inputs:
                   tokens_ids = [tokens_sequence[time_step]]
                   positions = [time_step]
               Update tokens_sequence with predicted token
           This part is quicker than regular GPT inference because the model does not
           need to compute the keys/values already cached, and also computes the
           attention mechanism only for one query!

        Args:
            token_ids: Sequence of token ids delivered to the decoder of shape
                (batch_size, num_input_tokens)
            positions: Position, in the input sequence, of the first token id passed in
                token_ids. (batch_size, 1)

            For example, let us say we are inferring on the tokens [1,3,2,5]. Two
            examples of inputs might be:
                1. token_ids = [1,3,2,5]
                   positions = [0]
                2. token_ids = [3]
                   positions = [1]

        Returns:
            The embeddings and logits corresponding to the tokens given as input.
        """
        # (batch_size,num_input_tokens) -> (batch_size, num_input_tokens, embed_dim)
        tokens_embeddings = self.token_embed(token_ids)

        # (batch_size, num_input_tokens, embed_dim) -> (batch_size, num_input_tokens, embed_dim)  # noqa
        embeddings = self.apply_transformer_layers(tokens_embeddings, positions)
        embeddings = self.final_norm(embeddings)

        # get outputs
        outs = {}
        outs["embeddings"] = embeddings  # (batch_size, num_input_tokens, embed_dim)

        # compute logits
        logits = self.lm_head(outs["embeddings"])[
            "logits"
        ]  # (batch_size, num_input_tokens, vocab_size)  # noqa
        outs["logits"] = logits

        return outs  # type: ignore


def build_gpt_fn_with_caching(
    config: GptConfig,
    cache_length: int,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    name: Optional[str] = None,
) -> Callable:
    """
    Create the model's forward pass.

    Args:
        config: Configuration data class containing the hyperparameters for the GPT
            forward function.
        cache_length: length of the provisioned cache
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.
        name: the naming of the model. example: gpt_j_encoder


        # NOTE: in inference, the model could be in fp16 without too much degradation
        # NOTE: on NVIDIA accelerator, XLA inter-device operation ( psum, all_gather,
        etc ... ) are not always implemented for bf16. but on TPU hardware yes

    Returns:
        Gpt Decoder model forward function.
    """

    assert {compute_dtype, param_dtype, output_dtype}.issubset(
        {
            jnp.bfloat16,
            jnp.float32,
            jnp.float16,
        }
    ), f"provide a dtype in {jnp.bfloat16, jnp.float32, jnp.float16}"

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(GptDecoder, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.RMSNorm, norm_policy)

    def gpt_fn(
        token_ids: jnp.ndarray, positions: jnp.ndarray = None
    ) -> TransformerOutput:
        """

        Args:
            token_ids: shape ( batch_size, num_input_tokens )
            positions: shape ( batch_size, 1 )

        Returns:
            The logits and embeddings for the corresponding token_ids
        """
        model = GPTDecoderWithCache(
            config,
            cache_length=cache_length,
            name=name,
        )
        return model(
            token_ids=token_ids,
            positions=positions,
        )

    return gpt_fn
