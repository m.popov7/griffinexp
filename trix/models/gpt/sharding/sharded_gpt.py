from collections.abc import Callable
from typing import Optional

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.layers.sharding.embeddings.embed import ReplicatedEmbed
from trix.layers.sharding.heads.lm_head import ShardedLMHead
from trix.layers.sharding.regularization.norms import (
    ReplicatedLayerNorm,
    ReplicatedRMSNorm,
)
from trix.models.gpt.layers import GptDecoderLayer
from trix.models.gpt.model import GptConfig, GptDecoder
from trix.models.gpt.sharding.sharded_layers import ShardedGptDecoderLayer
from trix.types import TransformerOutput


class ShardedGptDecoder(GptDecoder):
    """
    Jax implementation of Sharded GPT models.
    It covers GPTJ and LLAMA.
    """

    def __init__(self, config: GptConfig, num_shards: int, name: Optional[str] = None):
        super().__init__(config=config, name=name)

        self._num_shards = num_shards
        # Add checks on dimensions
        if (not self._config.num_heads % self._num_shards == 0) and (
            not self._config.ffn_embed_dim % self._num_shards == 0
        ):
            raise ValueError(
                "The number of attention heads must be divisible by the number of "
                "shards, however provided attention heads is "
                "{config.num_attention_heads} and the number of shards is "
                f"{self._num_shards}."
            )
        self.token_embed = ReplicatedEmbed(
            self._config.vocab_size,
            self._config.embed_dim,
            name="replicated_token_embed",
        )

        self.lm_head = ShardedLMHead(
            embed_dim=self._config.embed_dim,
            alphabet_size=self._config.vocab_size,
            add_bias_lm_head=self._config.add_bias_lm_head,
            name="sharded_simple_lm_head",
        )

        if self._config.norm_type == "layer_norm":
            self.final_norm = ReplicatedLayerNorm(
                create_offset=True,
                name="final_replicated_layer_norm",
            )
        elif self._config.norm_type == "RMS_norm":
            self.final_norm = ReplicatedRMSNorm(name="final_replicated_rms_norm")
        else:
            raise ValueError(
                f"unrecognized norm_type in config {self._config.norm_type}"
            )

    @hk.experimental.name_like("__call__")
    def decoder_layer(self, layer_idx: int) -> GptDecoderLayer:
        """
        gives the GPT encoder layer
        Args:
            layer_idx: the layer index

        Returns:
            The named GptDecoderLayer
        """
        return ShardedGptDecoderLayer(
            embed_dim=self._config.embed_dim,
            ffn_embed_dim_per_shard=self._config.ffn_embed_dim // self._num_shards,
            num_heads_per_shard=self._config.num_heads // self._num_shards,
            num_shards=self._num_shards,
            rotary_dim=self._config.rope_dimensions,
            max_position_embeddings=self._config.max_position_embeddings,
            norm_type=self._config.norm_type,
            parallel_attention_ff=self._config.parallel_attention_ff,
            add_bias_ffn=self._config.add_bias_ffn,
            ffn_activation_name=self._config.ffn_activation_name,
            use_glu_in_ffn=self._config.use_glu_in_ffn,
            add_bias_attn=self._config.add_bias_attn,
            name=f"sharded_gpt_decoder_layer_{layer_idx}",
        )


def build_sharded_gpt_fn(
    config: GptConfig,
    num_shards: int,
    name: Optional[str] = None,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        config: Configuration data class containing the hyperparameters for the GPT
            forward function.
        num_shards: Number of shards to shard the model.
        name: the naming of the model. example: gpt_j_encoder
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
            NOTE: when training, the gradient is often accumulated in fp32, therefore
            output_dtype need to be in fp32.

    Returns:
        GPT sharded model forward function.
    """
    assert {compute_dtype, param_dtype, output_dtype}.issubset(
        {
            jnp.bfloat16,
            jnp.float32,
            jnp.float16,
        }
    ), f"provide a dtype in {jnp.bfloat16, jnp.float32, jnp.float16}"

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(ShardedGptDecoder, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(ReplicatedRMSNorm, norm_policy)
    hk.mixed_precision.set_policy(ReplicatedLayerNorm, norm_policy)

    def sharded_gpt_fn(
        token_ids: jnp.ndarray,
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the decoder over the inputs.
        model = ShardedGptDecoder(config=config, num_shards=num_shards, name=name)
        outs = model(
            token_ids=token_ids,
        )
        return outs

    return sharded_gpt_fn
