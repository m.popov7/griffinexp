import re
from typing import Any, Optional

import haiku as hk

from trix.utils.sharding.shard import (
    shard_linear_and_psum,
    shard_parallel_linear,
    shard_parallel_linear_glu,
    shard_replicated_embedding,
    shard_replicated_linear,
    shard_replicated_norm,
)

GPT_UNSHARDED_TO_SHARDED = {
    "attn_RMS_norm": "attn_replicated_rms_norm",
    "ffn_RMS_norm": "ffn_replicated_rms_norm",
    "attn_layer_norm": "attn_replicated_layer_norm",
    "ffn_layer_norm": "ffn_replicated_layer_norm",
    "self_attn": "attention",
    "out_linear": "out_linear_and_p_sum",
    "fc2_linear": "fc2_linear_and_p_sum",
    "final_RMS_norm": "final_replicated_rms_norm",
    "final_layer_norm": "final_replicated_layer_norm",
    "token_embed": "replicated_token_embed",
    "simple_lm_head": "sharded_simple_lm_head",
    "gpt_decoder_layer_": "sharded_gpt_decoder_layer_",
    "lm_final_fc": "synchronous_final_fc",
}


def shard_decoder_layer(
    params: hk.Params, num_shards: int, sharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
        Shards the parameters of a GptDecoderLayer.

    Args:
        params: Unsharded parameters of a GptDecoderLayer.
        num_shards: Number of shards.
        sharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        sharded_params: Updated sharded parameters.
    """
    if sharded_params is None:
        sharded_params = {}
    for layer_name in params:
        if "fc2_linear" in layer_name:
            sharded_params = shard_linear_and_psum(
                {layer_name: params[layer_name]},
                num_shards,
                sharded_params,
            )

        elif "out_linear" in layer_name:
            sharded_params = shard_linear_and_psum(
                {layer_name: params[layer_name]},
                num_shards,
                sharded_params,
            )
        elif "fc1_linear" in layer_name:
            if "glu" in layer_name:
                sharded_params = shard_parallel_linear_glu(
                    {layer_name: params[layer_name]},
                    num_shards,
                    sharded_params,
                )
            else:
                sharded_params = shard_parallel_linear(
                    {layer_name: params[layer_name]},
                    num_shards,
                    sharded_params,
                )
        elif (
            ("value_linear" in layer_name)
            or ("key_linear" in layer_name)
            or ("query_linear" in layer_name)
        ):
            sharded_params = shard_parallel_linear(
                {layer_name: params[layer_name]},
                num_shards,
                sharded_params,
            )

        elif "norm" in layer_name:
            sharded_params = shard_replicated_norm(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        else:
            raise ValueError(
                f"Key: {layer_name} not recognized."
                "Your parameter dictionary does not correspond to a SelfAttentionBlock."
                " This can also happen if the names of the layers in SelfAttentionBlock"
                " have been modified."
            )
    return sharded_params


def rename_sharded_layers(sharded_params: hk.Params) -> hk.Params:
    """
    Rename Parameters

    Args:
        sharded_params: Sharded parameters from an unsharded model.

    Returns:
        Parameters with renamed modules to fit ShardedGptDecoder.
    """
    for key in list(sharded_params.keys()):
        new_key = "sharded_" + key
        for k in GPT_UNSHARDED_TO_SHARDED:
            new_key = new_key.replace(k, GPT_UNSHARDED_TO_SHARDED[k])
        sharded_params[new_key] = sharded_params.pop(key)
    return sharded_params


def shard_gpt(params: hk.Params, num_shards: int) -> hk.Params:
    """
    Shards parameters of an GptDecoder.

    Args:
        params: Parameters of an GptDecoder. The names
            of the parameters must be the default one chosen
            in the GptDecoder implementation for this
            function to work.
        num_shards: Number of shards.

    Returns:
        sharded_params: Sharded parameters.
    """
    sharded_params: hk.Params = {}
    keys = list(params.keys())

    def get_int(regex_match: Any) -> int:
        """
            if regex_match is None, returns -1
            else return the regex-extracted value
        Args:
            regex_match: a regex object

        Returns:

        """
        return -1 if (regex_match is None) else int(regex_match.group(1))

    num_layers = (
        max([get_int(re.search(r"gpt_decoder_layer_(\d+)/", s)) for s in keys]) + 1
    )
    for n in range(num_layers):
        attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if f"gpt_decoder_layer_{n}/" in layer_name
        }
        sharded_params = shard_decoder_layer(attn_params, num_shards, sharded_params)
    not_attn_keys = [s for s in keys if "decoder_layer_" not in s]
    for layer_name in not_attn_keys:
        if "token_embed" in layer_name:
            # deal with both token and positional embeddings
            sharded_params = shard_replicated_embedding(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )

        elif "simple_lm_head" in layer_name:
            sharded_params = shard_replicated_linear(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        elif "norm" in layer_name:
            sharded_params = shard_replicated_norm(
                {layer_name: params[layer_name]}, num_shards, sharded_params
            )
        else:
            raise ValueError(f"the key {layer_name} wasn't recognized during sharding")

    sharded_params = rename_sharded_layers(sharded_params)

    return sharded_params
