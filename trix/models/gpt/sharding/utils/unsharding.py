import re
from typing import Any, Optional

import haiku as hk

from trix.models.gpt.sharding.utils.sharding import GPT_UNSHARDED_TO_SHARDED
from trix.utils.sharding.unshard import (
    unshard_linear_and_psum,
    unshard_parallel_linear,
    unshard_parallel_linear_glu,
    unshard_replicated_embedding,
    unshard_replicated_linear,
    unshard_replicated_norm,
)


def unshard_decoder_layer(
    params: hk.Params, unsharded_params: Optional[hk.Params] = None
) -> hk.Params:
    """
    Unshards the parameters of a ShardedGptDecoderLayer.

    Args:
        params: Sharded parameters of a ShardedGptDecoderLayer.
        unsharded_params: Optional unsharded parameters to which new weights will be
            appended.

    Returns:
        unsharded_params: Updated unsharded parameters.
    """
    if unsharded_params is None:
        unsharded_params = {}
    for layer_name in params:
        if "fc2" in layer_name:
            if "~/linear" in layer_name:
                # TODO: pass second layer as it is done twice
                fc2_params = {
                    layer: params[layer] for layer in params if "fc2" in layer
                }
                unsharded_params = unshard_linear_and_psum(fc2_params, unsharded_params)

        elif "out_linear" in layer_name:
            unsharded_params = unshard_linear_and_psum(
                {layer_name: params[layer_name]}, unsharded_params
            )
        elif "fc1_linear" in layer_name:
            if "glu" in layer_name:
                unsharded_params = unshard_parallel_linear_glu(
                    {layer_name: params[layer_name]},
                    unsharded_params,
                )
            else:
                unsharded_params = unshard_parallel_linear(
                    {layer_name: params[layer_name]},
                    unsharded_params,
                )
        elif (
            ("value_linear" in layer_name)
            or ("key_linear" in layer_name)
            or ("query_linear" in layer_name)
        ):
            unsharded_params = unshard_parallel_linear(
                {layer_name: params[layer_name]},
                unsharded_params,
            )

        elif "norm" in layer_name:

            unsharded_params = unshard_replicated_norm(
                {layer_name: params[layer_name]}, unsharded_params
            )

        else:
            raise ValueError(
                f"the key {layer_name} wasn't recognized during unsharding"
            )
    return unsharded_params


def rename_unsharded_layers(unsharded_params: hk.Params) -> hk.Params:
    """
    Rename unsharded layers to right names for GptDecoder.

    Args:
        unsharded_params: Parameters un-sharded from ShardedGptDecoder.

    Returns:
        Parameters that can fit in GptDecoder.
    """
    unsharded_to_sharded = {v: k for k, v in GPT_UNSHARDED_TO_SHARDED.items()}
    for key in list(unsharded_params.keys()):
        new_key = key.replace("sharded_", "")
        for k in unsharded_to_sharded:
            new_key = new_key.replace(k, unsharded_to_sharded[k])
        unsharded_params[new_key] = unsharded_params.pop(key)
    return unsharded_params


def unshard_gpt(params: hk.Params) -> hk.Params:
    """
    Unshards parameters of a ShardedGptDecoder.

    Args:
        params: Parameters of a ShardedGptDecoder. The names of the parameters must be
            the default one chosen in the ShardedGptDecoder implementation for this
            function to work.

    Returns:
        unsharded_params: Unsharded parameters.
    """
    unsharded_params: hk.Params = {}
    keys = list(params.keys())

    def get_int(regex_match: Any) -> int:
        """
            if regex_match is None, returns -1
            else return the regex-extracted value
        Args:
            regex_match: a regex object

        Returns:

        """
        return -1 if (regex_match is None) else int(regex_match.group(1))

    num_layers = (
        max([get_int(re.search(r"gpt_decoder_layer_(\d+)/", s)) for s in keys]) + 1
    )
    for n in range(num_layers):
        attn_params = {
            layer_name: params[layer_name]
            for layer_name in params
            if f"gpt_decoder_layer_{n}/" in layer_name
        }

        unsharded_params = unshard_decoder_layer(attn_params, unsharded_params)

    not_attn_keys = [s for s in keys if "decoder_layer_" not in s]

    for layer_name in not_attn_keys:
        if "token_embed" in layer_name:
            unsharded_params = unshard_replicated_embedding(
                {layer_name: params[layer_name]}, unsharded_params
            )
        elif "simple_lm_head" in layer_name:
            unsharded_params = unshard_replicated_linear(
                {layer_name: params[layer_name]}, unsharded_params
            )
        elif "norm" in layer_name:
            unsharded_params = unshard_replicated_norm(
                {layer_name: params[layer_name]}, unsharded_params
            )
        else:
            raise ValueError(
                f"the key {layer_name} wasn't recognized during unsharding"
            )
    unsharded_params = rename_unsharded_layers(unsharded_params=unsharded_params)

    return unsharded_params
