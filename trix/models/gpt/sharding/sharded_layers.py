from typing import Optional

from trix.layers.sharding.linear.linear_and_p_sum import LinearAndPsum
from trix.layers.sharding.regularization.norms import (
    ReplicatedLayerNorm,
    ReplicatedRMSNorm,
)
from trix.models.gpt.layers import GptDecoderLayer, GptMultiHeadAttention


class ShardedGptMultiHeadAttention(GptMultiHeadAttention):
    def __init__(
        self,
        embed_dim: int,
        num_heads_per_shard: int,
        num_shards: int,
        rotary_dim: Optional[int],
        max_position_embeddings: int,
        name: str = "attention",
        add_bias_attn: bool = False,
    ):
        """
        Initializes the attention layer.

        Args:
            embed_dim: Length of the token embedding at each position in the sequence.
            num_heads_per_shard: Number of independent attention heads per
                device ( shard )
            num_shards: number of shards
            rotary_dim: The dimension in key space to apply the rotary positional
                embeddings
            max_position_embeddings: The maximum length to apply rotary positional
                embeddings
            name: Optional name for this module.
            add_bias_attn: Add bias to the attention mechanism (key, query,
                value, and output projections).
        """
        super().__init__(
            embed_dim=embed_dim,
            num_heads=num_heads_per_shard,
            rotary_dim=rotary_dim,
            max_position_embeddings=max_position_embeddings,
            key_size=embed_dim // num_shards // num_heads_per_shard,
            name=name,
            add_bias_attn=add_bias_attn,
        )
        self.out_linear = LinearAndPsum(
            self.embed_dim, with_bias=False, name="out_linear_and_p_sum"
        )


class ShardedGptDecoderLayer(GptDecoderLayer):
    """
    Single layer in the decoder, including self-attention and feed-forward operations.
    The feed-forward network uses a ReLU activation and has no biases.
    """

    def __init__(
        self,
        embed_dim: int,
        ffn_embed_dim_per_shard: int,
        num_heads_per_shard: int,
        num_shards: int,
        rotary_dim: Optional[int],
        max_position_embeddings: int,
        norm_type: str,
        parallel_attention_ff: bool,
        add_bias_ffn: bool,
        ffn_activation_name: str,
        use_glu_in_ffn: bool,
        add_bias_attn: bool = False,
        name: Optional[str] = None,
    ):
        """
        Initializes the encoder layer, including the projections needed for
        self-attention and the linear layers applied in the fully connected portion

        Args:
            embed_dim: Dimension of the embeddings
            ffn_embed_dim_per_shard: Dimension of the hidden layer in the MLP per shard
            num_heads_per_shard: Number of independent attention heads per shard
            num_shards: number of shards
            rotary_dim: The dimension in key space to apply the rotary positional
                embeddings
            max_position_embeddings: The maximum length to apply rotary positional
                embeddings
            norm_type: The type of norm used ( pre normalization scheme ) used. can be
                one of ["layer_norm", "RMS_norm"]
            parallel_attention_ff: Whether to do the attention and the MLP in parallel,
                and then sum up the results as it is done in Gpt-NeoX :
                Black, Sid, et al. "Gpt-neoX-20b: An open-source autoregressive
                language model." arXiv preprint arXiv:2204.06745 (2022).
                It is said to improve the training time of 15% when compiling with JAX
            add_bias_ffn: Add bias in feed forward network block.
            ffn_activation_name: Activation function to be used in FFN block. Supported
                names are "gelu", "gelu-no-approx", "relu", "swish", and "silu"
            use_glu_in_ffn: Whether to use Gated Linear Unit (GLU) in Feed
                Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
                to True and use swish as ffn_activation_name.
                Same principle for a gated-relu.
            name: Optional name for this module.
            add_bias_attn: Add bias to the attention mechanism (key, query, value, and
                output projections).
        """
        super().__init__(
            embed_dim=embed_dim,
            ffn_embed_dim=ffn_embed_dim_per_shard,
            num_heads=num_heads_per_shard,
            rotary_dim=rotary_dim,
            max_position_embeddings=max_position_embeddings,
            norm_type=norm_type,
            parallel_attention_ff=parallel_attention_ff,
            add_bias_ffn=add_bias_ffn,
            ffn_activation_name=ffn_activation_name,
            use_glu_in_ffn=use_glu_in_ffn,
            add_bias_attn=add_bias_attn,
            name=name,
        )

        if embed_dim % (num_heads_per_shard * num_shards) != 0:
            raise ValueError(
                f"The embedding dimension should be divisible by the total number "
                f"of heads,  however provided embed dim is {embed_dim} and the "
                f"total number of heads is {num_heads_per_shard*num_shards}."
            )
        self.sa_layer = ShardedGptMultiHeadAttention(
            embed_dim=embed_dim,
            num_heads_per_shard=num_heads_per_shard,
            num_shards=num_shards,
            rotary_dim=rotary_dim,
            max_position_embeddings=max_position_embeddings,
            # name=name + "/~/self_attn",
            add_bias_attn=add_bias_attn,
        )

        if norm_type == "layer_norm":
            self.attn_norm = ReplicatedLayerNorm(
                create_offset=True,
                name="attn_replicated_layer_norm",
            )
            if not self.parallel_attention_ff:
                self.ffn_norm = ReplicatedLayerNorm(
                    create_offset=True,
                    name="ffn_replicated_layer_norm",
                )
        elif norm_type == "RMS_norm":
            self.attn_norm = ReplicatedRMSNorm(
                eps=1e-6,
                name="attn_replicated_rms_norm",
            )
            if not self.parallel_attention_ff:
                self.ffn_norm = ReplicatedRMSNorm(
                    name="ffn_replicated_rms_norm",
                )
        else:
            raise ValueError(f"unrecognized norm_type : {norm_type}")

        self.fc2_linear = LinearAndPsum(
            output_size=embed_dim,
            with_bias=add_bias_ffn,
            name="fc2_linear_and_p_sum"
            # name=name + "/~/fc2_linear",
        )
