"""Implementation of the Perceiver resampler from the Flamingo paper in Jax."""

import logging
from dataclasses import dataclass
from typing import Callable, Dict, List, Optional, Tuple

import haiku as hk
import jax.numpy as jnp
import jmp

from trix.models.perceiver_resampler.layers import PerceiverResamplerBlock
from trix.types import AttentionMask, Embedding, TransformerOutput
from trix.utils.logging import debug_log_tensor

logger = logging.getLogger(__name__)


@dataclass
class PerceiverResamplerConfig:
    """
    Parameters to initialize an PerceiverResampler model. Based on the ESM architecture.

    Args:
        emb_layer_norm_before: Whether to use layer norm before the first attention
            layer.
        attention_heads: Number of attention heads.
        key_size: The dimension of the query, key, and values within each attention
            head, if not specified, it is set to attention_heads//embed_dim.
            It can be useful to set a custom key size if we want to impose the size of
            the query, key and value tensor ( for example, tensors shaped with
            power of 2 are more efficiently handled on TPUs ).
            Note: Parametrizing the model with a custom key size has been done in :
            Brown, Tom, et al. "Language models are few-shot learners."
            Advances in neural information processing systems 33 (2020): 1877-1901.
        embed_dim: Embedding dimension.
        ffn_embed_dim: Feed forward embedding dimension.
        num_layers: Number of attention blocks.
        ffn_activation_name: Activation function to be used in FFN block. Supported
            names are "gelu", "relu", "swish".
        use_glu_in_ffn: Whether to use Gated Linear Unit (GLU) in Feed
            Forward Network (FFN) block. To do a swiGLU (gated-swish) put this arg
            to True and use swish as ffn_activation_name.
            Same principle for a gated-relu. To keep the same number of parameters in
            the FFN block, one should multiply by 2/3 the ffn_embed_dim when using GLU.
            See https://arxiv.org/pdf/2002.05202.pdf for more details.
        resampled_length: length of the resampled output of the module
        use_gradient_checkpointing: Whether to use gradient checkpointing (checkpoint
            gradients in the forward pass to reduce the computation in the backward).
    """

    # architecture
    emb_layer_norm_before: bool = False
    attention_heads: int = 20
    key_size: Optional[int] = None
    embed_dim: int = 1280
    ffn_embed_dim: int = 5120
    num_layers: int = 24
    add_bias_kv: bool = False
    add_bias_ffn: bool = True
    ffn_activation_name: str = "gelu-no-approx"
    use_glu_in_ffn: bool = False
    resampled_length: int = 64

    # performance
    use_gradient_checkpointing: bool = False

    def __post_init__(self) -> None:
        """
        Checks that the given values are compatible.
        """

        if self.key_size is None:
            if not self.embed_dim % self.attention_heads == 0:
                raise ValueError(
                    f"When no key size is provided, the embedding dimension should be "
                    f"divisible by the number of heads, however provided embedding "
                    f"dimension is {self.embed_dim} and the number of heads is "
                    f"{self.attention_heads}."
                )
            self.key_size = self.embed_dim // self.attention_heads


class PerceiverResampler(hk.Module):
    """
    Perceiver Resampler model, made of successive PerceiverResamplerBlocks.
    """

    def __init__(
        self,
        config: PerceiverResamplerConfig,
        name: Optional[str] = None,
    ):
        """
        Initialize a Perceiver Resampler model.

        Args:
            config: Dataclass containing model hyperparameters.
            name: Name for module (custom will break weight loading).
        """

        self._config = config
        super().__init__(name=name)

    @hk.transparent
    def apply_attention_blocks(
        self,
        x: Embedding,
        xf: Embedding,
        outs: Dict[str, Embedding],
        attention_mask: Optional[AttentionMask] = None,
    ) -> Tuple[Embedding, Dict[str, Embedding]]:
        """
        Create the blocks of attention layers and applies them.

        Args:
            x: The sequence embedding.
            xf: Input embeddings flowing in the perceiver.
            outs: A dictionary to carry through the attention layers which stores the
                intermediate sequence embedding and attention maps.
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            The output sequence embedding.
            The optional intermediate results (embeddings of the layer and attention
                weights).
        """

        layers: List[Callable] = [
            self._attention_block(layer_idx)
            for layer_idx in range(self._config.num_layers)
        ]

        if self._config.use_gradient_checkpointing:
            # the remat-ed function cannot take control flow arguments
            layers = [hk.remat(layer) for layer in layers]

        for layer_idx, layer in enumerate(layers):
            concat_input = jnp.concatenate([xf, x], axis=1)
            output = layer(
                x=x,
                cross_attention_embeddings=concat_input,
                attention_mask=attention_mask,
            )
            x = output["embeddings"]
            debug_log_tensor(f"Perceiver layer {layer_idx} output", x, logger=logger)

        return x, outs

    @hk.transparent
    def _attention_block(self, layer_idx: int) -> PerceiverResamplerBlock:
        return PerceiverResamplerBlock(  # type: ignore
            num_heads=self._config.attention_heads,
            embed_dim=self._config.embed_dim,
            key_size=self._config.key_size,
            ffn_embed_dim=self._config.ffn_embed_dim,
            add_bias_kv=self._config.add_bias_kv,
            add_bias_fnn=self._config.add_bias_ffn,
            ffn_activation_name=self._config.ffn_activation_name,
            use_glu_in_ffn=self._config.use_glu_in_ffn,
            name=f"perceiver_layer_{layer_idx}",
        )

    def __call__(
        self,
        input_embeddings: Embedding,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the embeddings based on the input tokens.

        Args:
            input_embeddings: Input embeddings to be resampled.
            attention_mask: Attention mask to feed to the attention layers.

        Returns:
            Dictionary containing the final embeddings and logits.
        """
        assert (
            input_embeddings.shape[-1] == self._config.embed_dim
        ), "The input embedding dim should match the model embed dim"

        batch_size = input_embeddings.shape[0]
        # Get latent queries
        w_init = hk.initializers.TruncatedNormal(1.0 / jnp.sqrt(self._config.embed_dim))
        latent_queries = hk.get_parameter(
            "latent_queries",
            shape=[self._config.resampled_length, self._config.embed_dim],
            dtype=input_embeddings.dtype,
            init=w_init,
        )
        latent_queries = jnp.expand_dims(latent_queries, axis=0)
        latent_queries = jnp.repeat(latent_queries, repeats=batch_size, axis=0)

        # Prepare outputs dict
        outs: Dict[str, jnp.ndarray] = {}
        x = latent_queries

        # Apply attention blocks
        x, outs = self.apply_attention_blocks(
            x=x, xf=input_embeddings, outs=outs, attention_mask=attention_mask
        )

        debug_log_tensor("Perceiver stack output", x, logger=logger)
        outs["embeddings"] = x

        return outs  # type: ignore


def build_perceiver_resampler_fn(
    model_config: PerceiverResamplerConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        model_config: Model hyperparameters.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        model_name: Model's name.

    Returns:
        Perceiver model forward function.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(PerceiverResampler, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=jnp.float32, param_dtype=param_dtype, output_dtype=compute_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def perceiver_resampler_fn(
        input_embeddings: Embedding, attention_mask: Optional[AttentionMask] = None
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = PerceiverResampler(config=model_config, name=model_name)
        outs = encoder(input_embeddings=input_embeddings, attention_mask=attention_mask)
        return outs

    return perceiver_resampler_fn
