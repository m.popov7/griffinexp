from typing import Optional

import haiku as hk
import jax.numpy as jnp

from trix.layers.attention.multi_head_attention import MultiHeadAttention
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput
from trix.utils.activations import get_activation_fn


class PerceiverResamplerBlock(hk.Module):
    """
    Block of the perceiver resampler architecture (used to process encoder embeddings
    for multi-modalities models) as introduced in Flamingo
    (https://arxiv.org/abs/2204.14198).
    """

    def __init__(
        self,
        num_heads: int,
        embed_dim: int,
        ffn_embed_dim: int,
        key_size: Optional[int] = None,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        add_bias_kv: bool = False,
        add_bias_fnn: bool = True,
        ffn_activation_name: str = "gelu-no-approx",
        use_glu_in_ffn: bool = False,
        name: Optional[str] = None,
    ):
        super().__init__(name=name)
        # Add checks on dimensions
        if key_size is None:
            if embed_dim % num_heads != 0:
                raise ValueError(
                    f"The embedding dimension should be divisible by the number of "
                    f"heads, however provided embedding dimension is {embed_dim} and "
                    f"the number of heads is {num_heads}."
                )
            else:
                key_size = embed_dim // num_heads

        # Hyperparameters internalization
        self._ffn_activation_fn = get_activation_fn(activation_name=ffn_activation_name)
        self._use_glu_in_ffn = use_glu_in_ffn

        # Define layers
        if use_glu_in_ffn:
            # user should multiply ffn_embed_dim by 2/3 when using GLU
            # to keep total number of parameters equal
            # see https://arxiv.org/pdf/2002.05202.pdf. for more details
            # we multiply by 2 here as the output will be split in 2 for GLU
            ffn_embed_dim = int(2 * ffn_embed_dim)

        # Define layers
        self.fc1 = hk.Linear(ffn_embed_dim, name="fc1", with_bias=add_bias_fnn)
        self.fc2 = hk.Linear(embed_dim, name="fc2", with_bias=add_bias_fnn)

        self.layer_norm_cross_attention = hk.LayerNorm(
            axis=-1,
            create_scale=True,
            create_offset=True,
            name="cross_attention_layer_norm",
        )
        self.layer_norm_mlp = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True, name="final_layer_norm"
        )

        self.cross_attention_layer = MultiHeadAttention(
            num_heads=num_heads,
            key_size=key_size,
            rotary_embedding_config=rotary_embedding_config,
            add_bias_kv=add_bias_kv,
            name="cross_attention",
        )

    @hk.transparent
    def cross_attention(
        self,
        x: Embedding,
        cross_attention_embeddings: Embedding,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Applies the cross attention mechanism.

        Args:
            x: Input token embeddings of shape (batch_size, seq_len, embed_dim).
            cross_attention_embeddings: Embeddings to be used for cross attention
                (in encoder-decoder models, it is the output of the encoder) of shape
                (batch_size, seq_len, embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).


        Returns:
            A dictionary containing the output embeddings and the attention weights.
        """

        return self.cross_attention_layer(
            query=x,
            key=cross_attention_embeddings,
            value=cross_attention_embeddings,
            attention_mask=attention_mask,
        )

    @hk.transparent
    def mlp(self, x: Embedding) -> Embedding:
        """
        Applies one layer-norm, one linear layer, a Gelu activation,
        then a final linear layer

        Args:
            x: Embeddings of shape (batch_size, seq_len, key_size * num_heads).

        Returns:
            The transformed sequence embedding.
        """
        x = self.layer_norm_mlp(x)
        if self._use_glu_in_ffn:
            x1, x2 = jnp.split(self.fc1(x), indices_or_sections=2, axis=-1)
            x = self._ffn_activation_fn(x1) * x2
        else:
            x = self._ffn_activation_fn(self.fc1(x))
        x = self.fc2(x)
        return x

    def __call__(
        self,
        x: Tokens,
        cross_attention_embeddings: Embedding,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the output embeddings of the attention block.

        Args:
            x: Input tokens.
            cross_attention_embeddings: Embeddings to be used for cross attention
                (in encoder-decoder models, it is the output of the encoder).
            attention_mask: Attention mask of shape (batch_size, 1,seq_len, seq_len).

        Returns:
            A dictionary containing the output embeddings and the attention weights.
        """

        # Cross-Attention
        res = x
        x = self.layer_norm_cross_attention(x)
        output = self.cross_attention(
            x=x,
            cross_attention_embeddings=cross_attention_embeddings,
            attention_mask=attention_mask,
        )
        x = output["embeddings"]
        x = res + x

        # MLP
        x = x + self.mlp(x)

        output["embeddings"] = x
        return output  # type: ignore
