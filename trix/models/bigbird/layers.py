from typing import Dict, Optional

import haiku as hk
import jax
import jax.numpy as jnp
from haiku import initializers

from trix.layers.attention.multi_head_attention import MultiHeadAttention
from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.models.bigbird.types import (
    BandBlockMask,
    BlockSequenceMask,
    RandomBlockSelector,
)
from trix.types import Embedding, RNGKey, SequenceMask, TransformerOutput


class BigbirdMultiHeadAttention(MultiHeadAttention):
    """
    Implements the sparse attention mechanism of BigBird.
    """

    def __init__(
        self,
        num_heads: int,
        key_size: int,
        connectivity_seed: int,
        block_size: int,
        num_rand_blocks: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            num_heads: Number of attention heads.
            key_size: Size of each head's embedding.
            connectivity_seed: The seed of the random attention.
            block_size: The size of each block.
            num_rand_blocks: The number of bigbird random blocks.
            name: Model's name.
        """

        MultiHeadAttention.__init__(
            self,
            num_heads=num_heads,
            key_size=key_size,
            add_bias_kv=False,
            rotary_embedding_config=None,
            name=name,
        )
        self._connectivity_seed = connectivity_seed
        self._block_size = block_size
        self._output_block_size = block_size
        self._input_block_size = block_size
        self._model_size = key_size * num_heads

        self._num_rand_blocks = num_rand_blocks
        self._key_size = key_size
        self._num_heads = num_heads

    def sparse_dot_product_attention(
        self,
        queries: jnp.ndarray,
        keys: jnp.ndarray,
        values: jnp.ndarray,
        bigbird_attn_mask: SequenceMask = None,
    ) -> Dict[str, Embedding]:
        """
        Implements sparse dot product attention given query, key, and value.

        This is the core function for applying attention based on
        https://arxiv.org/abs/1706.03762.
        It calculates the attention dot-product given the  queries and keys.

        Args:
            queries: queries for calculating attention with shape of
                `(batch_size,length, num_heads, mem_channels)`.
            keys: keys for calculating attention with shape of
                `(batch_size, length,num_heads, mem_channels)`.
            values: values to be used in attention with shape of
                `(batch_size, length, num_heads, value_channels)`.
            bigbird_attn_mask (SequenceMask, optional):
                input mask. Defaults to None.

        Returns:
            A dictionary storing the computed contexts and attention weights.
        """

        (batch_size, seq_length, _, _) = keys.shape
        assert (self._num_heads, self._key_size) == keys.shape[2:]
        assert seq_length % self._block_size == 0, (
            f"sequence length {seq_length}"
            + f"is not a multiple of block size {self._block_size}"
        )

        rng_keys_rand_attn_heads = jax.random.split(
            key=jax.random.PRNGKey(self._connectivity_seed), num=self._num_heads
        )
        rand_attn = jax.vmap(
            lambda key: self.get_block_rand_mask(
                rng_key=key, output_seq_len=seq_length, input_seq_len=seq_length
            )
        )(rng_keys_rand_attn_heads)
        rand_attn = jnp.expand_dims(rand_attn, 0)
        rand_attn = jnp.repeat(rand_attn, batch_size, 0)

        input_mask = jnp.logical_not(bigbird_attn_mask)

        # reshape and cast for blocking
        blocked_input_mask = jnp.reshape(
            input_mask, (batch_size, seq_length // self._block_size, self._block_size)
        )
        input_mask = jnp.reshape(input_mask, (batch_size, 1, seq_length, 1))
        output_mask = jnp.reshape(input_mask, (batch_size, 1, 1, seq_length))

        # create band padding
        band_pad = self.create_band_mask_from_inputs(
            blocked_input_mask, blocked_input_mask
        )
        rand_pad = self.create_rand_mask_from_inputs(
            blocked_input_mask, blocked_input_mask, rand_attn
        )

        queries = jnp.transpose(queries, (0, 2, 1, 3))
        keys = jnp.transpose(keys, (0, 2, 1, 3))
        values = jnp.transpose(values, (0, 2, 1, 3))

        context_layer, attn_weights = self.blocked_attention_computation(
            query_matrix=queries,
            key_matrix=keys,
            value_matrix=values,
            rand_attn=rand_attn,
            band_mask=band_pad,
            rand_mask=rand_pad,
            sequence_mask_m=input_mask,
            sequence_mask_n=output_mask,
        )
        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        context_layer = hk.Linear(
            self._model_size,
            w_init=w_init,
            b_init=b_init,
        )(context_layer)

        return {"embeddings": context_layer, "attention_weights": attn_weights}

    def compute_first_context(
        self,
        blocked_query_matrix: jnp.ndarray,
        blocked_key_matrix: jnp.ndarray,
        blocked_value_matrix: jnp.ndarray,
        sequence_mask: SequenceMask,
    ) -> Embedding:
        """
        Computes the first block row of the attention dot-product.

        Args:
            blocked_query_matrix: the blocked query matrix.
            blocked_key_matrix: the blocked key matrix.
            blocked_value_matrix: the blocked value matrix.
            sequence_mask: the boolean sequence mask.

        Returns:
            The result of the attention dot product corresponding to
            the first block of token in the attention matrix.
        """

        first_product = jnp.einsum(
            "BHQD,BHKD->BHQK", blocked_query_matrix[:, :, 0], blocked_key_matrix
        )
        first_product = first_product / jnp.sqrt(self._key_size)

        first_product = jnp.where(sequence_mask, first_product, -1e30)

        # (batch_size, num_heads, output_block_size, input_seq_len)
        first_attn_weights = jax.nn.softmax(first_product)

        # (batch_size, num_heads, output_block_size, -1)
        first_context_layer = jnp.einsum(
            "BHQK,BHKD->BHQD", first_attn_weights, blocked_value_matrix
        )
        first_context_layer = jnp.expand_dims(first_context_layer, 2)
        return first_context_layer, first_attn_weights

    def compute_second_context(
        self,
        blocked_key_matrix: jnp.ndarray,
        blocked_value_matrix: jnp.ndarray,
        blocked_query_matrix: jnp.ndarray,
        gathered_key: jnp.ndarray,
        gathered_value: jnp.ndarray,
        sequence_mask: SequenceMask,
        rand_mask: BlockSequenceMask,
        batch_size: int,
    ) -> Embedding:
        """
        Computes the second block row of the attention dot-product.

        Args:
            blocked_key_matrix: the blocked key matrix.
            blocked_value_matrix: the blocked value matrix.
            blocked_query_matrix: the blocked query matrix.
            gathered_key: the blocked gathered key from the
                random blocks.
            gathered_value: the blocked gathered value from the
                random blocks.
            sequence_mask: the boolean sequence mask.
            rand_mask: contains the padding for the "rand_pad_product" which is
                the random block attention.
            batch_size: the batch size.

        Returns:
            The result of the attention dot product corresponding
            to the second block of token in the attention matrix.
        """

        second_key_mat = jnp.concatenate(
            [
                blocked_key_matrix[:, :, 0],
                blocked_key_matrix[:, :, 1],
                blocked_key_matrix[:, :, 2],
                blocked_key_matrix[:, :, -1],
                gathered_key[:, :, 0],
            ],
            2,
        )

        second_value_mat = jnp.concatenate(
            [
                blocked_value_matrix[:, :, 0],
                blocked_value_matrix[:, :, 1],
                blocked_value_matrix[:, :, 2],
                blocked_value_matrix[:, :, -1],
                gathered_value[:, :, 0],
            ],
            2,
        )
        second_product = jnp.einsum(
            "BHQD,BHKD->BHQK", blocked_query_matrix[:, :, 1], second_key_mat
        )
        second_seq_pad = jnp.concatenate(
            [
                sequence_mask[:, :, :, : 3 * self._input_block_size],
                sequence_mask[:, :, :, -self._input_block_size :],
                jnp.ones(
                    [batch_size, 1, 1, self._num_rand_blocks * self._input_block_size],
                    dtype=jnp.bool_,
                ),
            ],
            3,
        )
        second_rand_pad = jnp.concatenate(
            [
                jnp.ones(
                    [
                        batch_size,
                        self._num_heads,
                        self._output_block_size,
                        4 * self._input_block_size,
                    ],
                    dtype=jnp.bool_,
                ),
                rand_mask[:, :, 0],
            ],
            3,
        )
        second_product = second_product / jnp.sqrt(self._key_size)

        second_product = jnp.where(
            jnp.minimum(second_seq_pad, second_rand_pad),
            second_product,
            -1e30,
        )
        # (batch_size , num_heads, output_block_size,
        # (4+num_rand_blocks)*input_block_size)
        second_attn_weights = jax.nn.softmax(second_product)

        # (batch_size, num_heads, output_block_size, -1)
        second_context_layer = jnp.einsum(
            "BHQK,BHKD->BHQD", second_attn_weights, second_value_mat
        )
        second_context_layer = jnp.expand_dims(second_context_layer, 2)

        return second_context_layer, second_attn_weights

    def compute_middle_context(
        self,
        blocked_key_matrix: jnp.ndarray,
        blocked_value_matrix: jnp.ndarray,
        blocked_query_matrix: jnp.ndarray,
        gathered_key: jnp.ndarray,
        gathered_value: jnp.ndarray,
        band_mask: BlockSequenceMask,
        sequence_mask: SequenceMask,
        rand_mask: BlockSequenceMask,
    ) -> Embedding:
        """
        Computes the attention dot-product for the  2nd-->2nd row of the blocked
        attention (middle context).

        Args:
            blocked_key_matrix: the blocked key matrix.
            blocked_value_matrix: the blocked value matrix.
            blocked_query_matrix: the blocked query matrix.
            gathered_key: the blocked gathered key from the
                random blocks.
            gathered_value: the blocked gathered value from the
                random blocks.
            band_mask: the blocked sequence mask to apply
                to the band product.
            sequence_mask: the boolean sequence mask.
            rand_mask: the blocked sequence mask to apply
                to the random band product.

        Returns:
            The corresponding embeddings of the dot-product
            (middle context, attention weights).
        """
        exp_blocked_key_matrix = jnp.concatenate(
            [
                blocked_key_matrix[:, :, 1:-3],
                blocked_key_matrix[:, :, 2:-2],
                blocked_key_matrix[:, :, 3:-1],
            ],
            3,
        )
        exp_blocked_value_matrix = jnp.concatenate(
            [
                blocked_value_matrix[:, :, 1:-3],
                blocked_value_matrix[:, :, 2:-2],
                blocked_value_matrix[:, :, 3:-1],
            ],
            3,
        )
        middle_query_matrix = blocked_query_matrix[:, :, 2:-2]

        inner_band_product = jnp.einsum(
            "BHLQD,BHLKD->BHLQK", middle_query_matrix, exp_blocked_key_matrix
        )
        inner_band_product = inner_band_product / jnp.sqrt(self._key_size)

        rand_band_product = jnp.einsum(
            "BHLQD,BHLKD->BHLQK", middle_query_matrix, gathered_key[:, :, 1:-1]
        )
        rand_band_product = rand_band_product / jnp.sqrt(self._key_size)

        first_band_product = jnp.einsum(
            "BHLQD,BHKD->BHLQK", middle_query_matrix, blocked_key_matrix[:, :, 0]
        )
        first_band_product = first_band_product / jnp.sqrt(self._key_size)

        # (batch_size, num_heads, output_seq_len//output_block_size-4,
        # output_block_size, input_block_size)
        last_band_product = jnp.einsum(
            "BHLQD,BHKD->BHLQK", middle_query_matrix, blocked_key_matrix[:, :, -1]
        )
        last_band_product = last_band_product / jnp.sqrt(self._key_size)

        inner_band_product = jnp.where(band_mask, inner_band_product, -1e30)
        first_band_product = jnp.where(
            jnp.expand_dims(sequence_mask[:, :, :, : self._input_block_size], 3),
            first_band_product,
            -1e30,
        )
        last_band_product = jnp.where(
            jnp.expand_dims(sequence_mask[:, :, :, -self._input_block_size :], 3),
            last_band_product,
            -1e30,
        )
        rand_band_product = jnp.where(
            rand_mask[:, :, 1:-1],
            rand_band_product,
            -1e30,
        )

        # (batch_size, num_heads, output_seq_len//output_block_size-4,
        # output_block_size, (5+num_rand_blocks)*input_block_size)
        band_product = jnp.concatenate(
            [
                first_band_product,
                inner_band_product,
                rand_band_product,
                last_band_product,
            ],
            -1,
        )
        # (batch_size, num_heads, output_seq_len//output_block_size-4,
        # output_block_size, (5+num_rand_blocks)*input_block_size)
        attn_weights = jax.nn.softmax(band_product)

        middle_context_layer = jnp.einsum(
            "BHLQK,BHLKD->BHLQD",
            attn_weights[
                :, :, :, :, self._input_block_size : 4 * self._input_block_size
            ],
            exp_blocked_value_matrix,
        )
        middle_context_layer += jnp.einsum(
            "BHLQK,BHLKD->BHLQD",
            attn_weights[
                :, :, :, :, 4 * self._input_block_size : -self._input_block_size
            ],
            gathered_value[:, :, 1:-1],
        )
        middle_context_layer += jnp.einsum(
            "BHLQK,BHKD->BHLQD",
            attn_weights[:, :, :, :, : self._input_block_size],
            blocked_value_matrix[:, :, 0],
        )

        # (batch_size, num_heads, output_seq_len//output_block_size-4,
        # output_block_size, -1)
        middle_context_layer += jnp.einsum(
            "BHLQK,BHKD->BHLQD",
            attn_weights[:, :, :, :, -self._input_block_size :],
            blocked_value_matrix[:, :, -1],
        )
        return middle_context_layer, attn_weights

    def compute_second_last_context(
        self,
        blocked_key_matrix: jnp.ndarray,
        blocked_value_matrix: jnp.ndarray,
        blocked_query_matrix: jnp.ndarray,
        gathered_key: jnp.ndarray,
        gathered_value: jnp.ndarray,
        sequence_mask: SequenceMask,
        rand_mask: SequenceMask,
        batch_size: int,
    ) -> Embedding:
        """
        Computes the dot-product attention for the second last
        row of the attention.

        Args:
            blocked_key_matrix: the blocked key matrix.
            blocked_value_matrix: the blocked value matrix.
            blocked_query_matrix: the blocked query matrix.
            gathered_key: the blocked gathered key from the
                random blocks.
            gathered_value: the blocked gathered value from the
                random blocks.
            sequence_mask: the boolean sequence mask.
            rand_mask: the blocked sequence mask to apply
                to the random band product.
            batch_size: the batch size.

        Returns:
            The corresponding embeddings of the
            dot-product.
        """
        second_last_key_mat = jnp.concatenate(
            [
                blocked_key_matrix[:, :, 0],
                blocked_key_matrix[:, :, -3],
                blocked_key_matrix[:, :, -2],
                blocked_key_matrix[:, :, -1],
                gathered_key[:, :, -1],
            ],
            2,
        )

        second_last_value_mat = jnp.concatenate(
            [
                blocked_value_matrix[:, :, 0],
                blocked_value_matrix[:, :, -3],
                blocked_value_matrix[:, :, -2],
                blocked_value_matrix[:, :, -1],
                gathered_value[:, :, -1],
            ],
            2,
        )

        second_last_product = jnp.einsum(
            "BHQD,BHKD->BHQK", blocked_query_matrix[:, :, -2], second_last_key_mat
        )
        second_last_seq_pad = jnp.concatenate(
            [
                sequence_mask[:, :, :, : self._input_block_size],
                sequence_mask[:, :, :, -3 * self._input_block_size :],
                jnp.ones(
                    [batch_size, 1, 1, self._num_rand_blocks * self._input_block_size],
                    dtype=jnp.bool_,
                ),
            ],
            3,
        )
        second_last_rand_pad = jnp.concatenate(
            [
                jnp.ones(
                    [
                        batch_size,
                        self._num_heads,
                        self._output_block_size,
                        4 * self._input_block_size,
                    ],
                    dtype=jnp.bool_,
                ),
                rand_mask[:, :, -1],
            ],
            3,
        )
        second_last_product = second_last_product / jnp.sqrt(self._key_size)

        second_last_product = jnp.where(
            jnp.minimum(second_last_seq_pad, second_last_rand_pad),
            second_last_product,
            -1e30,
        )
        # (batch_size, num_heads, output_block_size,
        # (4+num_rand_blocks)*input_block_size)
        second_last_attn_weights = jax.nn.softmax(second_last_product)

        # (batch_size, num_heads, output_block_size, -1)
        second_last_context_layer = jnp.einsum(
            "BHQK,BHKD->BHQD", second_last_attn_weights, second_last_value_mat
        )
        second_last_context_layer = jnp.expand_dims(second_last_context_layer, 2)

        return second_last_context_layer, second_last_attn_weights

    def compute_last_context_layer(
        self,
        blocked_query_matrix: jnp.ndarray,
        blocked_key_matrix: jnp.ndarray,
        blocked_value_matrix: jnp.ndarray,
        sequence_mask: SequenceMask,
    ) -> Embedding:
        """
        Computes the last row of the dot-product attention.

        Args:
            blocked_query_matrix: the blocked query matrix.
            blocked_key_matrix: the blocked key matrix.
            blocked_value_matrix: the blocked value matrix.
            sequence_mask: the input sequence mask.

        Returns:
            The resulting context and attention weights.
        """
        last_product = jnp.einsum(
            "BHQD,BHKD->BHQK", blocked_query_matrix[:, :, -1], blocked_key_matrix
        )
        last_product = last_product / jnp.sqrt(self._key_size)

        last_product = jnp.where(
            sequence_mask,
            last_product,
            -1e30,
        )

        # (batch_size, num_heads, output_block_size, input_seq_len)
        last_attn_weights = jax.nn.softmax(last_product)

        # (batch_size, num_heads, output_block_size, -1)
        last_context_layer = jnp.einsum(
            "BHQK,BHKD->BHQD", last_attn_weights, blocked_value_matrix
        )
        last_context_layer = jnp.expand_dims(last_context_layer, 2)
        return last_context_layer, last_attn_weights

    def blocked_attention_computation(
        self,
        query_matrix: jnp.ndarray,
        key_matrix: jnp.ndarray,
        value_matrix: jnp.ndarray,
        rand_attn: RandomBlockSelector,
        band_mask: BlockSequenceMask,
        rand_mask: BlockSequenceMask,
        sequence_mask_m: SequenceMask,
        sequence_mask_n: SequenceMask,
    ) -> Embedding:
        """
        Applies sparse block band rand attention in an efficient way.
        This is where the engineering of bigbird is.

        Args:
            query_matrix: the query matrix of shape
                [batch_size, num_heads, input_seq_len, hidden_dim]
            key_matrix: the key matrix of shape
                [batch_size, num_heads, input_seq_len, hidden_dim]
            value_matrix: the value matrix of shape
                [batch_size, num_heads, input_seq_len, hidden_dim]
            rand_attn: For each query, contains the block-location
                of the random attention blocks, of shape [batch_size,
                num_heads, output_seq_len//output_block_size-2,
                num_rand_blocks].
            band_mask: Contains the padding for the "inner_band_product"
                which is the sliding window attention, of shape
                [batch_size, 1, output_seq_len//output_block_size-4,
                output_block_size, 3*input_block_size].
            rand_mask: the blocked sequence mask to apply to the random
                band product, of shape [batch_size, num_heads,
                output_seq_len//output_block_size-2, output_block_size,
                num_rand_blocks*input_block_size]
            sequence_mask_m: a boolean mask to apply on the output sequence,
                of shape [ batch_size, 1, output_seq_len, 1].
            sequence_mask_n: a boolean mask to apply on the input sequence,
                of shape [batch_size, 1, 1, input_seq_len].

        Returns:
            The context layer: [batch_size, output_seq_len, num_heads, -1]
            and the attention weights
        """

        batch_size, _, output_seq_len, _ = query_matrix.shape
        _, _, input_seq_len, _ = key_matrix.shape

        assert output_seq_len == input_seq_len, (
            f"input shape length {input_seq_len} and output shape "
            + "length {output_seq_len} should be equal"
        )
        blocked_query_matrix = jnp.reshape(
            query_matrix,
            (
                batch_size,
                self._num_heads,
                output_seq_len // self._output_block_size,
                self._output_block_size,
                self._key_size,
            ),
        )
        blocked_key_matrix = jnp.reshape(
            key_matrix,
            (
                batch_size,
                self._num_heads,
                input_seq_len // self._input_block_size,
                self._input_block_size,
                self._key_size,
            ),
        )
        blocked_value_matrix = jnp.reshape(
            value_matrix,
            (
                batch_size,
                self._num_heads,
                input_seq_len // self._input_block_size,
                self._input_block_size,
                self._key_size,
            ),
        )

        # (batch_size, num_heads, input_seq_len//input_block_size-2,
        # num_rand_blocks, input_block_size, -1)
        gather_fn_ = jax.vmap(lambda tensor, indices: jnp.take(tensor, indices, axis=0))
        gather_fn = jax.vmap(lambda tensor, indices: gather_fn_(tensor, indices))
        gathered_key = jnp.reshape(
            gather_fn(blocked_key_matrix, rand_attn),
            (
                batch_size,
                self._num_heads,
                output_seq_len // self._output_block_size - 2,
                self._num_rand_blocks * self._input_block_size,
                self._key_size,
            ),
        )
        # (batch_size, num_heads, input_seq_len//input_block_size-2,
        # num_rand_blocks, input_block_size, -1)
        gathered_value = jnp.reshape(
            gather_fn(blocked_value_matrix, rand_attn),
            (
                batch_size,
                self._num_heads,
                output_seq_len // self._output_block_size - 2,
                self._num_rand_blocks * self._input_block_size,
                self._key_size,
            ),
        )  #

        first_context_layer, first_attn_weights = self.compute_first_context(
            blocked_query_matrix=blocked_query_matrix,
            blocked_key_matrix=key_matrix,
            blocked_value_matrix=value_matrix,
            sequence_mask=sequence_mask_n,
        )

        second_context_layer, second_attn_weights = self.compute_second_context(
            blocked_key_matrix=blocked_key_matrix,
            blocked_value_matrix=blocked_value_matrix,
            blocked_query_matrix=blocked_query_matrix,
            gathered_key=gathered_key,
            gathered_value=gathered_value,
            sequence_mask=sequence_mask_n,
            rand_mask=rand_mask,
            batch_size=batch_size,
        )
        middle_context_layer, attn_weights = self.compute_middle_context(
            blocked_key_matrix=blocked_key_matrix,
            blocked_value_matrix=blocked_value_matrix,
            blocked_query_matrix=blocked_query_matrix,
            gathered_key=gathered_key,
            gathered_value=gathered_value,
            band_mask=band_mask,
            sequence_mask=sequence_mask_n,
            rand_mask=rand_mask,
        )

        (
            second_last_context_layer,
            second_last_attn_weights,
        ) = self.compute_second_last_context(
            blocked_key_matrix=blocked_key_matrix,
            blocked_value_matrix=blocked_value_matrix,
            blocked_query_matrix=blocked_query_matrix,
            gathered_key=gathered_key,
            gathered_value=gathered_value,
            sequence_mask=sequence_mask_n,
            rand_mask=rand_mask,
            batch_size=batch_size,
        )

        last_context_layer, last_attn_weights = self.compute_last_context_layer(
            blocked_query_matrix=blocked_query_matrix,
            blocked_key_matrix=key_matrix,
            blocked_value_matrix=value_matrix,
            sequence_mask=sequence_mask_n,
        )

        # gathering the different context tensor
        context_layer = jnp.concatenate(
            [
                first_context_layer,
                second_context_layer,
                middle_context_layer,
                second_last_context_layer,
                last_context_layer,
            ],
            2,
        )
        context_layer = (
            jnp.reshape(
                context_layer,
                (batch_size, self._num_heads, output_seq_len, self._key_size),
            )
            * sequence_mask_m
        )
        context_layer = jnp.transpose(context_layer, (0, 2, 1, 3))
        context_layer = jnp.reshape(context_layer, (*context_layer.shape[:-2], -1))

        attn_weights = {
            "first_attn_weights": first_attn_weights,
            "second_attn_weights": second_attn_weights,
            "attn_weights": attn_weights,
            "second_last_attn_weights": second_last_attn_weights,
            "last_attn_weights": last_attn_weights,
            "rand_attn": rand_attn,
        }

        return context_layer, attn_weights

    def get_block_rand_mask(
        self,
        rng_key: RNGKey,
        output_seq_len: int,
        input_seq_len: int,
    ) -> RandomBlockSelector:
        """
        Generates the random block selector for the random attention.

        Args:
            rng_key: random seed to generate the mask.
            output_seq_len: output sequence length.
            input_seq_len: input sequence length.

        Returns:
            A tensor containing int indices representing
                the location of the random block at each block-row of the attention
        """
        available_cols = jnp.arange(1, input_seq_len // self._input_block_size - 1)
        assert input_seq_len // self._input_block_size - 4 > self._num_rand_blocks, (
            f"a block size of {self._input_block_size} is too big compared"
            + f"to the input length {input_seq_len}"
        )

        (
            second_layer_rng_key,
            second_last_layer_rng_key,
            middle_layer_rng_key,
        ) = jax.random.split(rng_key, num=3)

        second_layer_rand_attn = jax.random.choice(
            second_layer_rng_key,
            available_cols[2:],
            shape=(self._num_rand_blocks,),
            replace=False,
        )
        second_last_layer_rand_attn = jax.random.choice(
            second_last_layer_rng_key,
            available_cols[0:-2],
            shape=(self._num_rand_blocks,),
            replace=False,
        )

        middle_available_cols = available_cols[:-3]
        middle_range = jnp.arange(1, output_seq_len // self._output_block_size - 3)
        band_available_cols = middle_available_cols * (
            middle_available_cols < jnp.expand_dims(middle_range, 1)
        ) + (middle_available_cols + 3) * (
            middle_available_cols >= jnp.expand_dims(middle_range, 1)
        )

        middle_keys = jax.random.split(
            middle_layer_rng_key, num=band_available_cols.shape[0]
        )

        middle_layer_rand_attn = jax.vmap(
            lambda key, x: jax.random.choice(
                key, x, shape=(self._num_rand_blocks,), replace=False
            )
        )(middle_keys, band_available_cols)

        rand_attn = jnp.concatenate(
            (
                jnp.expand_dims(second_layer_rand_attn, axis=0),
                middle_layer_rand_attn,
                jnp.expand_dims(second_last_layer_rand_attn, axis=0),
            ),
            axis=0,
        )

        return rand_attn

    def create_band_mask_from_inputs(
        self, from_blocked_mask: BlockSequenceMask, to_blocked_mask: BlockSequenceMask
    ) -> BandBlockMask:
        """
        Creates the boolean array for masking during the
        attention operations for sliding window/rolled keys.

        Args:
            from_blocked_mask: batches of 2D Tensors of shape
                (batch_size, from_seq_length//from_block_size, from_block_size).
            to_blocked_mask: batches of int32 Tensors of shape
                (batch_size, to_seq_length//to_block_size, to_block_size).

        Returns:
            batches of float boolean Tensors of shape
                (batch_size,
                1,
                from_seq_length//from_block_size-4,
                from_block_size,
                3*to_block_size)
        """

        exp_blocked_to_pad = jnp.concatenate(
            [
                to_blocked_mask[:, 1:-3],
                to_blocked_mask[:, 2:-2],
                to_blocked_mask[:, 3:-1],
            ],
            2,
        )

        band_pad = jnp.einsum(
            "BLQ,BLK->BLQK", from_blocked_mask[:, 2:-2], exp_blocked_to_pad
        )
        band_pad = jnp.expand_dims(band_pad, 1)
        return band_pad

    def create_rand_mask_from_inputs(
        self,
        from_blocked_mask: BlockSequenceMask,
        to_blocked_mask: BlockSequenceMask,
        rand_attn: RandomBlockSelector,
    ) -> jnp.ndarray:
        """
        Creates the boolean array for masking during the
        attention operations for random gathered blocks.

        Args:
            from_blocked_mask: batch of 2D Tensors of shape
                (batch_size, from_seq_length//from_block_size, from_block_size).
            to_blocked_mask: batch of int32 Tensor of shape
                (batch_size, to_seq_length//to_block_size, to_block_size).
            rand_attn: batch of 3D int Tensors
                (batch_size, num_attention_heads,
                from_seq_length//from_block_size-2, num_rand_blocks)

        Returns:
            binary float Tensor of shape
                (batch_size,
                num_attention_heads,
                from_seq_length//from_block_size-2,
                from_block_size,
                num_rand_blocks)

        """

        batch_size, num_attention_heads, num_windows, _ = rand_attn.shape
        gather_fn = jax.vmap(lambda tensor, indices: jnp.take(tensor, indices, axis=0))
        rand_pad = jnp.reshape(
            gather_fn(to_blocked_mask, rand_attn),
            [batch_size, num_attention_heads, num_windows, -1],
        )

        rand_pad = jnp.einsum("BLQ,BHLK->BHLQK", from_blocked_mask[:, 1:-1], rand_pad)

        return rand_pad

    def __call__(
        self,
        query: jnp.ndarray,
        key: jnp.ndarray,
        value: jnp.ndarray,
        attention_mask: Optional[jnp.ndarray] = None,
        attention_weight_bias: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        """
        Computes both the embeddings and the attention weights.

        Args:
            query: Embedding sequence to compute queries.
            key: Embedding sequence to compute keys.
            value: Embedding sequence to compute values.
            attention_mask: Mask to be applied during the attention layers.
                Triangular for autoregressive models. Defaults to None.

        Returns:
            Dictionary containing the output embeddings and the attention weights.
        """
        if attention_mask is None:
            raise NotImplementedError(
                "Big bird attention layer does not support None attention mask."
            )

        query_heads = self._linear_projection_he_init(
            x=query, head_size=self._key_size, name="query"
        )
        key_heads = self._linear_projection_he_init(
            x=key, head_size=self._key_size, name="key"
        )
        value_heads = self._linear_projection_he_init(
            x=value, head_size=self._key_size, name="value"
        )

        bigbird_attn_mask = 1 - attention_mask[:, 0, :, 0]  # type: ignore

        out = self.sparse_dot_product_attention(
            queries=query_heads,
            keys=key_heads,
            values=value_heads,
            bigbird_attn_mask=bigbird_attn_mask,
        )
        return out


class BigbirdAttentionBlock(SelfAttentionBlock):
    """Bigbird Transformer layer."""

    def __init__(
        self,
        num_heads: int,
        embed_dim: int,
        ffn_embed_dim: int,
        connectivity_seed: int,
        block_size: int,
        num_rand_blocks: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            num_heads: Number of attention heads.
            embed_dim: Embedding dimension.
            ffn_embed_dim: Dimension of the feed-forward layers.
            connectivity_seed: The seed of the random attention.
            block_size: The size of each block.
            num_rand_blocks: The number of bigbird random blocks.
            name: Model's name.
        """
        SelfAttentionBlock.__init__(
            self,
            num_heads=num_heads,
            embed_dim=embed_dim,
            ffn_embed_dim=ffn_embed_dim,
            rotary_embedding_config=None,
            name=name,
        )

        key_size = embed_dim // num_heads
        # Force naming in self attention to keep previous name
        self.sa_layer = BigbirdMultiHeadAttention(
            num_heads=num_heads,
            key_size=key_size,
            connectivity_seed=connectivity_seed,
            block_size=block_size,
            num_rand_blocks=num_rand_blocks,
            name=hk.experimental.force_name(self.sa_layer.module_name),  # type: ignore
        )
