import warnings
from dataclasses import dataclass
from typing import Callable, Dict, Mapping, Optional

import haiku as hk
import jax
import jax.numpy as jnp
import jmp

from trix.layers.regularization.tokens_dropout import TokensDropout
from trix.models.bigbird.layers import BigbirdAttentionBlock
from trix.models.esm.model import ESMTransformer, ESMTransformerConfig
from trix.types import AttentionMask, Tokens, TransformerOutput


@dataclass
class BigbirdConfig(ESMTransformerConfig):
    """
    Extends the ESMTransformerConfig class

    Args:
        block_size : The size of the blocks in the sparse attention.
        num_rand_blocks : The number of random blocks.
        mask_before_attention: Use mask before attention layers.
        lm_head: The type of language model head.
    """

    block_size: int = 64
    num_rand_blocks: int = 3
    mask_before_attention: bool = False
    lm_head: str = "roberta"


class BigbirdTransformer(ESMTransformer):
    """
    Implementation of the Bigbird Transformer in Jax. Inherits from ESMTransformer.
    """

    def __init__(self, config: BigbirdConfig, name: Optional[str] = None):
        super().__init__(config=config, name=name)
        self._config: BigbirdConfig = config
        warnings.warn(
            "Current implementation of BigBird does not support padding and will output"
            " NaNs when padding is present in input tokens.",
            UserWarning,
            stacklevel=2,
        )

    @hk.transparent
    def _attention_block(self, layer_idx: int) -> BigbirdAttentionBlock:

        return BigbirdAttentionBlock(
            num_heads=self._config.attention_heads,
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            block_size=self._config.block_size,
            num_rand_blocks=self._config.num_rand_blocks,
            connectivity_seed=layer_idx,
            name=f"bigbird_attention_layer_{layer_idx}",
        )

    def __call__(
        self,
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the embeddings based on the input tokens. Re-written from
        ESMTransformer to return NaNs in presence of padded_inputs.

        Args:
            tokens: Input tokens out of the tokenizer of shape (batch_size, seq_len).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).
                If no mask is provided, a mask by default which equals 1 over all tokens
                is computed.

        Returns:
            Dictionary containing the final embeddings and logits.
        """
        # Prepare outputs dict
        outs: Dict[str, jnp.ndarray] = {}
        # Compute embeddings
        x = self._embed_layer(tokens)
        # Tokens dropout if needed
        if self._config.token_dropout:
            x = TokensDropout(
                embed_dim=self._config.embed_dim,
                mask_token_id=self._config.mask_token_id,
                pad_token_id=self._config.pad_token_id,
                masking_ratio=self._config.masking_ratio,
                masking_prob=self._config.masking_prob,
            )(x, tokens)

        # RoBERTa's mask scaling factor
        x = self._config.embed_scale * x
        if self._config.positional_embedding is not None:
            x = x + self._pos_embed_layer(tokens)

        if self._config.emb_layer_norm_before:
            x = self.emb_ln_before(x)

        # Attention mask
        if attention_mask is None:
            attention_mask = jnp.ones(
                (tokens.shape[0], 1, tokens.shape[1], tokens.shape[1])
            )

        # Mask before attention for models ESM1b and ESM2
        if self._config.mask_before_attention:
            x = x - jnp.where(attention_mask == 0, 1, 0)[:, 0, 0][..., None] * x
        # construct a tower of attention layers
        x, outs = self.apply_attention_blocks(
            x=x,
            outs=outs,
            attention_mask=attention_mask,
        )

        # Language Model Head
        if self._lm_head:
            lm_head_outs = self._lm_head(x)
            outs["logits"] = lm_head_outs["logits"]

        if self._config.lm_head == "roberta":
            embeddings = lm_head_outs["embeddings"]
        else:
            embeddings = x

        # Save final embeddings if needed
        if self._config.num_layers in self._config.embeddings_layers_to_save:
            outs[f"embeddings_{self._config.num_layers}"] = embeddings

        # Return NaNs if there are pads in the input (note that it cannot be handled
        # with errors while being jittable with current jax versions)
        outs = jax.tree_util.tree_map(
            lambda out: out / jnp.all(tokens != self._config.pad_token_id), outs
        )
        return outs  # type: ignore


def build_bigbird_forward_fn(
    model_config: BigbirdConfig,
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates the model's forward pass.

    Args:
        model_config: Model's parameters.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.
        model_name: Model's name.

    Returns:
        The forward function.
    """

    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(BigbirdTransformer, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    def forward_fn(
        tokens: jnp.ndarray, attention_mask: Optional[AttentionMask] = None
    ) -> Mapping[str, jnp.ndarray]:
        """
        Applies a forward pass.
        """

        # Run the transformer over the inputs.
        transformer = BigbirdTransformer(config=model_config, name=model_name)
        outs: Dict[str, jnp.array] = transformer(
            tokens=tokens, attention_mask=attention_mask
        )
        return outs

    return forward_fn
