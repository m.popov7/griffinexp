"""
This file gathers layers used to finetune pre-trained BigBird models.
"""
from dataclasses import replace
from typing import Callable, Dict, Optional

import haiku as hk
import jax
import jax.numpy as jnp
import jmp

from trix.layers.regularization.tokens_dropout import TokensDropout
from trix.models.bigbird.layers import BigbirdMultiHeadAttention
from trix.models.bigbird.model import BigbirdConfig, BigbirdTransformer
from trix.models.esm.finetuning import ESMAttentionBlockIA3Rescaling
from trix.types import AttentionMask, SequenceMask, Tokens, TransformerOutput


class BigbirdMultiHeadAttentionIA3Rescaling(BigbirdMultiHeadAttention):
    """
    This class adds rescaling weights following the IA³ methodology to the
    multi head attention layer of BigBird. This aims to be used for fine-tuning
    pre-trained ESM models.
    """

    def __init__(
        self,
        num_heads: int,
        key_size: int,
        connectivity_seed: int,
        block_size: int,
        num_rand_blocks: int,
        name: Optional[str] = None,
    ):
        super().__init__(
            num_heads=num_heads,
            key_size=key_size,
            connectivity_seed=connectivity_seed,
            block_size=block_size,
            num_rand_blocks=num_rand_blocks,
            name=name,
        )

    def __call__(
        self,
        query: jnp.ndarray,
        key: jnp.ndarray,
        value: jnp.ndarray,
        attention_mask: Optional[jnp.ndarray] = None,
        attention_weight_bias: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        """
        Computes both the embeddings and the attention weights.
        Adds IA^3 rescaling weights.

        Args:
            query: Embedding sequence to compute queries.
            key: Embedding sequence to compute keys.
            value: Embedding sequence to compute values.
            attention_mask: Mask to be applied during the attention layers.
                Triangular for autoregressive models. Defaults to None.

        Returns:
            Dictionary containing the output embeddings and the attention weights.
        """
        if attention_mask is None:
            raise NotImplementedError(
                "Big bird attention layer does not support None attention mask."
            )

        query_heads = self._linear_projection_he_init(
            x=query, head_size=self._key_size, name="query"
        )
        key_heads = self._linear_projection_he_init(
            x=key, head_size=self._key_size, name="key"
        )
        value_heads = self._linear_projection_he_init(
            x=value, head_size=self._key_size, name="value"
        )

        # add IA³ rescaling
        key_ia3_rescaling = hk.get_parameter(
            "key_ia3_rescaling",
            shape=[key_heads.shape[-2], key_heads.shape[-1]],
            dtype=key_heads.dtype,
            init=hk.initializers.Constant(1.0),
        )
        key_heads = key_heads * key_ia3_rescaling

        # add IA³ rescaling
        value_ia3_rescaling = hk.get_parameter(
            "value_ia3_rescaling",
            shape=[value_heads.shape[-2], value_heads.shape[-1]],
            dtype=value_heads.dtype,
            init=hk.initializers.Constant(1.0),
        )
        value_heads = value_heads * value_ia3_rescaling

        bigbird_attn_mask = 1 - attention_mask[:, 0, :, 0]  # type: ignore

        out = self.sparse_dot_product_attention(
            queries=query_heads,
            keys=key_heads,
            values=value_heads,
            bigbird_attn_mask=bigbird_attn_mask,
        )
        return out


class BigbirdAttentionBlockIA3Rescaling(ESMAttentionBlockIA3Rescaling):
    """
    This class adds rescaling weights following the IA³ methodology to the
    attention block layer of BigBird. This aims to be used for fine-tuning
    pre-trained ESM models.
    """

    def __init__(
        self,
        num_heads: int,
        embed_dim: int,
        ffn_embed_dim: int,
        connectivity_seed: int,
        block_size: int,
        num_rand_blocks: int,
        name: Optional[str] = None,
    ):
        super().__init__(
            num_heads=num_heads,
            embed_dim=embed_dim,
            ffn_embed_dim=ffn_embed_dim,
            name=name,
        )

        key_size = embed_dim // num_heads
        # Replace multi-head attention by the IA³ rescaling one
        # Force naming in self attention to keep previous name
        self.sa_layer = BigbirdMultiHeadAttentionIA3Rescaling(
            num_heads=num_heads,
            key_size=key_size,
            connectivity_seed=connectivity_seed,
            block_size=block_size,
            num_rand_blocks=num_rand_blocks,
            name=hk.experimental.force_name(self.sa_layer.module_name),  # type: ignore
        )


class ESMRobertaLayerNorm(hk.Module):
    """
    Layer that utilizes only the first layer norm of a RobertaLMHead to obtain
    the embedddings without computing the logits. This allows to save the computing
    and memory taken by the logits calculation during the finetuning.
    """

    def __init__(self, name: Optional[str] = None):
        """
        Args:
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)

        # Define layers
        self._first_layer_norm = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True, name="emb_layer_norm_after"
        )

    def __call__(self, x: jnp.ndarray) -> Dict[str, jnp.ndarray]:
        x = self._first_layer_norm(x)
        # Embeddings are computed after the first layer norm to be consistent with ESM
        embeddings = x
        return {"embeddings": embeddings}


class BigbirdTransformerIA3Rescaling(BigbirdTransformer):
    """
    This class adds rescaling weights following the IA³ methodology to the
    transformer model of ESM. This aims to be used for fine-tuning
    pre-trained ESM models.
    """

    def __init__(
        self,
        config: BigbirdConfig,
        name: Optional[str] = None,
    ):
        super().__init__(config=config, name=name)
        self._lm_head = ESMRobertaLayerNorm(
            name=hk.experimental.force_name(self._lm_head.module_name)  # type: ignore
        )

    @hk.transparent
    def _attention_block(self, layer_idx: int) -> BigbirdAttentionBlockIA3Rescaling:
        return BigbirdAttentionBlockIA3Rescaling(  # type: ignore
            num_heads=self._config.attention_heads,
            embed_dim=self._config.embed_dim,
            ffn_embed_dim=self._config.ffn_embed_dim,
            block_size=self._config.block_size,
            num_rand_blocks=self._config.num_rand_blocks,
            connectivity_seed=layer_idx,
            name=f"bigbird_attention_layer_{layer_idx}",
        )

    def __call__(
        self,
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the embeddings based on the input tokens.

        Args:
            tokens: Input tokens out of the tokenizer of shape (batch_size, seq_len).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).
                If no mask is provided, a mask by default which equals 1 over all tokens
                is computed.

        Returns:
            Dictionary containing the final embeddings and logits.
        """
        # Prepare outputs dict
        outs: Dict[str, jnp.ndarray] = {}

        # Compute embeddings
        x = self._embed_layer(tokens)
        # Tokens dropout if needed
        if self._config.token_dropout:
            x = TokensDropout(
                embed_dim=self._config.embed_dim,
                mask_token_id=self._config.mask_token_id,
                pad_token_id=self._config.pad_token_id,
                masking_ratio=self._config.masking_ratio,
                masking_prob=self._config.masking_prob,
            )(x, tokens)

        # RoBERTa's mask scaling factor
        x = self._config.embed_scale * x
        if self._config.positional_embedding is not None:
            x = x + self._pos_embed_layer(tokens)

        if self._config.emb_layer_norm_before:
            x = self.emb_ln_before(x)

        # Attention mask
        if attention_mask is None:
            attention_mask = jnp.ones(
                (tokens.shape[0], 1, tokens.shape[1], tokens.shape[1])
            )

        # Mask before attention for models ESM1b and ESM2
        if self._config.mask_before_attention:
            x = x - jnp.where(attention_mask == 0, 1, 0)[0, 0, 0][:, None] * x
        # construct a tower of attention layers
        x, outs = self.apply_attention_blocks(
            x=x,
            outs=outs,
            attention_mask=attention_mask,
        )

        # Language Model Head
        if self._lm_head:
            lm_head_outs = self._lm_head(x)

        if self._config.lm_head == "roberta":
            embeddings = lm_head_outs["embeddings"]
        else:
            embeddings = x

        # Save final embeddings if needed
        if self._config.num_layers in self._config.embeddings_layers_to_save:
            outs[f"embeddings_{self._config.num_layers}"] = embeddings

        # Return NaNs if there are pads in the input (note that it cannot be handled
        # with errors while being jittable with current jax versions)
        outs = jax.tree_util.tree_map(
            lambda out: out / jnp.all(tokens != self._config.pad_token_id), outs
        )

        return outs  # type: ignore


def build_bigbird_ia3_rescaling_with_head_fn(
    model_config: BigbirdConfig,
    head_fn: Callable[
        [], Callable[[jnp.ndarray, SequenceMask], Dict[str, jnp.ndarray]]
    ],
    compute_dtype: jnp.dtype = jnp.float32,
    param_dtype: jnp.dtype = jnp.float32,
    output_dtype: jnp.dtype = jnp.float32,
    model_name: Optional[str] = None,
) -> Callable:
    """
    Creates a forward pass for Bigbird, adds rescaling weights and a classification
    head.

    Args:
        model_config: Model hyperparameters.
        head_fn: Wrapper initializing a Classification/Regression head. The head cannot
            bepassed directly as haiku modules cannot be initialized outside of
            hk.transform.
        compute_dtype: the type of the activations. fp16 runs faster and is lighter in
            memory. bf16 handles better large int, and is hence more stable ( it avoids
            float overflows ).
        param_dtype: if compute_dtype is fp16, the model weights will be cast to fp16
            during the forward pass anyway. So in inference mode ( not training mode ),
            it is better to use params in fp16 if compute_dtype is fp16 too. During
            training, it is preferable to keep parameters in float32 for better
            numerical stability.
        output_dtype: the output type of the model. it determines the float precioson
            of the gradient when training the model.

    Example of the function being used with a classification head:
        The classification head is wrapped inside head_fn because
        haiku modules cannot be instantiated outside hk.transform.
        def head_fn():
            return SimpleClassificationHead(num_classes=num_classes)
        finetune_forward_fn = build_bigbird_ia3_rescaling_with_head_fn(
            model_config=config, head_fn=head_fn, model_name=model_name,
        )
        finetune_forward_fn = hk.transform(finetune_forward_fn)

    Returns:
        BigBird model forward function with IA³ rescaling.
    """
    policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(BigbirdTransformerIA3Rescaling, policy)

    # Remove it in batch norm to avoid instabilities
    norm_policy = jmp.Policy(
        compute_dtype=compute_dtype, param_dtype=param_dtype, output_dtype=output_dtype
    )
    hk.mixed_precision.set_policy(hk.BatchNorm, norm_policy)
    hk.mixed_precision.set_policy(hk.LayerNorm, norm_policy)

    # Adding final layer embedding if missing to be used as classification head input
    num_layers = model_config.num_layers
    if not (num_layers in model_config.embeddings_layers_to_save):
        emb_layers_to_save = model_config.embeddings_layers_to_save + (num_layers,)
        model_config = replace(
            model_config, embeddings_layers_to_save=emb_layers_to_save
        )

    def bigbird_fn(
        tokens: Tokens,
        attention_mask: Optional[AttentionMask] = None,
        sequence_mask: Optional[SequenceMask] = None,
    ) -> TransformerOutput:
        """Forward pass."""
        # Run the encoder over the inputs.
        encoder = BigbirdTransformerIA3Rescaling(config=model_config, name=model_name)
        outs = encoder(
            tokens=tokens,
            attention_mask=attention_mask,
        )

        # Define head
        head = head_fn()

        if sequence_mask is None:
            sequence_mask = jnp.ones_like(tokens)

        outs = head(x=outs[f"embeddings_{num_layers}"], sequence_mask=sequence_mask)  # type: ignore[call-arg] # noqa: E501
        return outs

    return bigbird_fn
