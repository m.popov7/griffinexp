import jax.numpy as jnp
from typing_extensions import TypeAlias

RandomBlockSelector: TypeAlias = jnp.ndarray
BlockSequenceMask: TypeAlias = jnp.ndarray
BandBlockMask: TypeAlias = jnp.ndarray
