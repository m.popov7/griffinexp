from __future__ import annotations

import functools
from typing import Callable

import haiku as hk
import jax
import jax.numpy as jnp
import optax

from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer
from trix.training.base import TrainingState
from trix.types import AttentionMask, Metrics, RNGKey, SequenceMask, Tokens
from trix.utils.masking import build_causal_attention_mask
from trix.utils.rl import get_mask_from_dones


class DecisionTransformerTrainer:
    """
    A stateless abstraction around an init_fn/update_fn set of funcs.
    This extracts some common boilerplate from the training loop. This trainer trains a
    DecisionTransformer through causal predictions.
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        loss_fn: Callable[
            [
                hk.Params,
                RNGKey,
                Tokens,
                Tokens,
                Callable[
                    [hk.Params, RNGKey, Tokens, AttentionMask | None],
                    dict[str, jnp.ndarray],
                ],
                SequenceMask | None,
                AttentionMask | None,
            ],
            tuple[jnp.ndarray, Metrics],
        ],
        tokenizer: DecisionTransformerTokenizer,
        optimizer: optax.GradientTransformation,
    ):
        """
        Args:
            forward_fn: The transformed forward function of the model.
            loss_fn: The loss function.
            tokenizer: Tokenizer.
            optimizer: Optimizer.

        """
        self._forward_fn = forward_fn

        self._loss_fn = functools.partial(
            loss_fn,
            forward_fn=self._forward_fn.apply,
        )

        self._optimizer = optimizer
        self._tokenizer = tokenizer

    @functools.partial(jax.jit, static_argnums=0)
    def init(
        self, random_key: RNGKey, tokens: Tokens, positions: Tokens
    ) -> TrainingState:
        """
        Initializes the Training State.

        Args:
            random_key: Random JAX key.
            tokens: Tokens (batch_size, seq_length).

        Returns:
            Initialized Training state.
        """
        random_key, subkey = jax.random.split(random_key)
        model_inputs = {"tokens": tokens, "positions": positions}
        params = self._forward_fn.init(subkey, model_inputs)
        optimizer_state = self._optimizer.init(params)

        return TrainingState(
            step=jnp.array(0),
            random_key=random_key,
            optimizer_state=optimizer_state,
            params=params,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self,
        state: TrainingState,
        tokens: Tokens,
        positions: Tokens,
    ) -> tuple[TrainingState, Metrics]:
        """
        Updates the training state. This function is to be called inside
        a pmap operator with `axis_name="batch"`.

        Args:
            state: Current training state.
            tokens: Tokens (num_accum_grads, batch_size, seq_length, *).

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads = tokens.shape[0]
        batch_size = tokens.shape[1]
        seq_len = tokens.shape[2]

        random_key = state.random_key
        params = state.params

        random_key, subkey = jax.random.split(random_key)

        # Select labels in interleaved tokens (r0, s0, a0, r1, s1, ...)
        returns_to_go_index = jnp.arange(seq_len // 3) * 3
        actions_index = returns_to_go_index + 2
        labels = tokens[:, :, actions_index]

        labels = labels[..., : self._tokenizer.action_size]

        # Generate Attention Mask

        attention_mask = build_causal_attention_mask(batch_size, seq_len)

        # Generate Sequence Mask

        # Select mask from rt=(returns_to_go_t, dones_t, positions_t, pad, pad, ...)
        dones = tokens[:, :, returns_to_go_index][..., 1:2]

        sequence_mask = get_mask_from_dones(dones)

        model_inputs = {"tokens": tokens, "positions": positions}
        # accumulate gradients if needed

        def loop_fun(
            i: int, val: tuple[Metrics, hk.Params]
        ) -> tuple[Metrics, hk.Params]:

            metrics, gradient = val
            (add_loss, add_metrics), add_gradient = jax.value_and_grad(
                self._loss_fn, has_aux=True
            )(
                params,
                random_key,
                tokens=jax.tree_map(lambda x: x[i + 1, :, :], model_inputs),
                targets=labels[i + 1, :, :],
                sequence_mask=sequence_mask[i + 1, :, :],
                attention_mask=attention_mask,
            )
            add_metrics["loss"] = add_loss
            metrics = jax.tree_map(lambda x, y: x + y, metrics, add_metrics)
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # evaluate gradients
        (loss, metrics), gradient = jax.value_and_grad(self._loss_fn, has_aux=True)(
            params,
            random_key,
            tokens=jax.tree_map(lambda x: x[0, :, :], model_inputs),
            targets=labels[0, :, :],
            sequence_mask=sequence_mask[0, :, :],
            attention_mask=attention_mask,
        )

        metrics["loss"] = loss

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics = jax.tree_map(lambda x: x / num_acc_grads, metrics)
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # gather losses and gradients across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)
        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(gradient, optimizer_state)
        params = optax.apply_updates(params, updates)

        new_state = TrainingState(
            step=state.step + 1,
            random_key=subkey,
            optimizer_state=optimizer_state,
            params=params,
        )
        return new_state, metrics
