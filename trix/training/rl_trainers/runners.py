from __future__ import annotations

from typing import Callable

import jax
import jaxlib
import tqdm

from trix.dataloaders.rl.datasets import TrajectoryDataset
from trix.training.base import TrainingState, aggregate_metrics
from trix.types import Metrics, Tokens


def run_train_epoch_decision_transformer(
    update_fn: Callable[[TrainingState, Tokens], tuple[TrainingState, Metrics]],
    dataset: TrajectoryDataset,
    devices: list[jaxlib.xla_extension.Device],
    num_acc_grads: int,
    batch_size: int,
    training_state: TrainingState,
    epoch_num: int = 0,
) -> tuple[TrainingState, Metrics]:
    """
    Performs one training epoch over given dataset. It assumes that the training
    state is already sharded on the specified devices.

    Args:
        update_fn: Function that does a forward/backward pass and
            updates training state.
        dataset: Training dataset.
        devices: Training devices.
        num_acc_grads: Number of gradients to accumulate.
        batch_size: Batch size per device per pass.
        training_state: Current training state (already sharded).
        epoch_num: Number of the epoch (for printing purpose only).

    Returns:
        Final training state.
        Aggregated training metrics over the epoch.
    """
    num_devices = len(devices)

    # Training loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Training"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []

    for tokenizer_outs in generator:
        # Reshape data to send to devices, and gradient accumulation
        tokenizer_outs = jax.tree_map(
            lambda x: x.reshape((num_devices, num_acc_grads, batch_size) + x.shape[1:]),
            tokenizer_outs,
        )

        tokens = tokenizer_outs["tokens"]
        positions = tokenizer_outs["positions"]
        # Update neural network and optimizer
        training_state, training_metrics = jax.pmap(
            update_fn, devices=devices, axis_name="batch"
        )(
            training_state,
            tokens=tokens,
            positions=positions,
        )

        epoch_metrics.append(jax.tree_map(lambda x: x[0], training_metrics))

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return training_state, aggregated_metrics
