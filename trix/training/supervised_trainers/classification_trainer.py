import functools
from typing import Any, Callable, Dict, Optional, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
import optax

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.training.base import TrainingState
from trix.training.supervised_trainers.utils import build_predictions
from trix.types import AttentionMask, Labels, Metrics, RNGKey, SequenceMask, Tokens
from trix.utils.masking import build_padding_attention_mask


class ClassificationTrainer:
    """
    A stateless abstraction around an init_fn/update_fn/compute_metrics_fn set of funcs.
    This extracts some common boilerplate from the training loop.
    This trainer trains an Encoder-style model (Transformer) with a final
    single or multi label classification head. Might be used for instance for
    fine-tuning.
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        loss_fn: Callable[
            [
                hk.Params,
                RNGKey,
                Tokens,
                Tokens,
                Callable[
                    [
                        hk.Params,
                        RNGKey,
                        Tokens,
                        Optional[AttentionMask],
                        Optional[SequenceMask],
                    ],
                    Dict[str, jnp.ndarray],
                ],
                Optional[AttentionMask],
            ],
            Tuple[jnp.ndarray, jnp.ndarray],
        ],
        tokenizer: BaseTokenizer,
        optimizer: optax.GradientTransformation,
        num_classes: int = 2,
        parameters_partition_fn: Optional[Callable[[str, str, Any], bool]] = None,
    ):
        """
        Args:
            forward_fn: The transformed forward function of the model.
            loss_fn: The loss function.
            tokenizer: Tokenizer.
            optimizer: Optimizer.
            num_classes: Number of classes predicted. Defaults to 2 for binary
                classification;
            parameters_partition_fn: if specified, this function is used to split
                parameters into trainable and non-trainable parameters. Can be used
                to specify to the trainer not to train some parameters from the model.
                The function takes as inputs the name of the module,
                the name of a given entry in the module data bundle
                (e.g. parameter name) and the corresponding data and returns a boolean.
                See haiku.data_structures.partition documentation for more details.
        """
        self._forward_fn = forward_fn

        self._loss_fn = functools.partial(
            loss_fn,
            forward_fn=self._forward_fn.apply,
        )
        self._optimizer = optimizer
        self._tokenizer = tokenizer
        self._num_classes = num_classes
        self._parameters_partition_fn = parameters_partition_fn

        # Define the function to initialize the predictions.
        self._build_predictions = functools.partial(
            build_predictions, num_classes=num_classes
        )

    def build_init_fn(self) -> Callable:
        xmapped_init_fn: Callable = jax.experimental.maps.xmap(
            fun=self.init,
            in_axes=(
                ["shard", "batch", ...],
                [
                    ...,
                ],
            ),
            out_axes=["shard", "batch", ...],
            axis_resources={"shard": "shard"},
            donate_argnums=(0,),
        )
        return xmapped_init_fn

    def build_init_fn_from_params(self) -> Callable:
        xmapped_init_fn: Callable = jax.experimental.maps.xmap(
            fun=self.init,
            in_axes=(
                ["shard", "batch", ...],
                [
                    ...,
                ],
                ["shard", "batch", ...],
            ),
            out_axes=["shard", "batch", ...],
            axis_resources={"shard": "shard"},
            donate_argnums=(0,),
        )
        return xmapped_init_fn

    def build_xmapped_update_fn(self) -> Callable:
        """
        Xmaps the self._update function nover the defined devices_mesh.
            The training_state has in_axis= ["shard", "batch", ...] since the first axis
        corresponds to the number of shards and the second corresponds to the number of
        data_parallel_ways axis.
            The tokens correspond to 'axis_name="batch"' since the first axis
        corresponds to the number of data_parallel_ways.
            The labels correspond to 'axis_name="batch"' since the first axis
        corresponds to the number of data_parallel_ways.

        The function returns a new training_state and training metrics.

        For updates where the optimizer updates are different along the batch_axis,
        if the out_axis indicate ["shard", ...], the following error is raised by xmap:
        "One of xmap results has an out_axes specification of ['shard', ...], but is
        actually mapped along more axes defined by this xmap call: batch". The out_axis
        needs to indicate ["shard", "batch", ...].

        For updates where the accumulated gradients are synchronized and the
        optimizer_state have been pmean along the "batch" axis, the out_axis
        should be ["shard", ...], but we observe that it also works with
        ["shard", "batch", ...].

        Since the optimizer state is different along "batch" axis in all training steps
        except those that call the pmean operation on this "batch" axis, i.e the steps
        where the gradient accumulation is applied, the training state needs to have its
        first two axis being [num_shards, num_data_parallel]. Therefore, the xmapped
        update function has in_axes ["shard", "batch", ...] for the training_state and
        equal out_axes.


        Returns:
            The jitted function with xmap
        """

        xmapped_update_fn: Callable = jax.experimental.maps.xmap(
            fun=self.update,
            in_axes=(["shard", "batch", ...], ["batch", ...], ["batch", ...]),
            out_axes=(
                ["shard", "batch", ...],
                [
                    "shard",
                    "batch",
                    ...,
                ],
            ),
            axis_resources={"shard": "shard", "batch": "batch"},
            donate_argnums=(0,),
        )
        return xmapped_update_fn

    def build_xmapped_metrics_fn(self) -> Callable:
        """
        Xmaps the self._compute_metrics function nover the defined devices_mesh.
            The training_state has in_axis= ["shard", "batch", ...] since the first axis
        corresponds to the number of shards and the second corresponds to the number of
        data_parallel_ways axis.
            The tokens correspond to 'axis_name="batch"' since the first axis
        corresponds to the number of data_parallel_ways.

        The function returns a new training_state and training metrics.

        Returns:
            The jitted function with xmap
        """

        xmapped_metrics_fn: Callable = jax.experimental.maps.xmap(
            fun=self.compute_metrics,
            in_axes=(["shard", "batch", ...], ["batch", ...], ["batch", ...]),
            out_axes=[
                "shard",
                "batch",
                ...,
            ],
            axis_resources={"shard": "shard", "batch": "batch"},
        )
        return xmapped_metrics_fn

    @functools.partial(jax.jit, static_argnums=0)
    def init(
        self,
        random_key: RNGKey,
        tokens: Tokens,
        params: Optional[hk.Params] = None,
    ) -> TrainingState:
        """
        Initializes the Training state.

        Args:
            random_key: Random JAX key.
            tokens: Tokens (batch_size, seq_length, *).
            params: Optionally provide params if already initialized.

        Returns:
            Initialized training state.
        """
        random_key, subkey = jax.random.split(random_key)
        if params is None:
            params = self._forward_fn.init(subkey, tokens)

        if self._parameters_partition_fn is None:
            trainable_params = params
        else:
            trainable_params, _ = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )
        optimizer_state = self._optimizer.init(trainable_params)

        return TrainingState(
            step=jnp.array(0),
            random_key=random_key,
            optimizer_state=optimizer_state,
            params=params,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self,
        state: TrainingState,
        tokens: Tokens,
        labels: Labels,
    ) -> Tuple[TrainingState, Metrics]:
        """
        Updates the training state. This function is to be called inside
        a pmap operator with `axis_name="batch"`.

        Args:
            state: Current training state.
            tokens: Tokens (num_accum_grads, batch_size, seq_length, *).
            labels: Labels (num_accum_grads, batch_size, *).

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads = tokens.shape[0]
        random_key = state.random_key
        params = state.params

        *mask_random_key, random_key, subkey = jax.random.split(
            random_key, num=num_acc_grads + 2
        )

        if self._parameters_partition_fn is None:
            trainable_params = params
            non_trainable_params = {}
        else:
            trainable_params, non_trainable_params = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )

        def _split_loss_fn(  # type: ignore
            trainable_params, non_trainable_params, *args, **kwargs
        ):
            params = hk.data_structures.merge(trainable_params, non_trainable_params)
            return self._loss_fn(params, *args, **kwargs)

        # accumulate gradients if needed
        def loop_fun(
            i: int, val: Tuple[Metrics, hk.Params]
        ) -> Tuple[Metrics, hk.Params]:

            metrics, gradient = val

            # Generate Attention Mask
            attention_mask = build_padding_attention_mask(
                tokens[i + 1, :, :], self._tokenizer.pad_token_id
            )
            sequence_mask = tokens[i + 1, :, :] != self._tokenizer.pad_token_id

            (add_loss, add_metrics), add_gradient = jax.value_and_grad(
                _split_loss_fn, has_aux=True
            )(
                trainable_params,
                non_trainable_params,
                random_key,
                tokens=tokens[i + 1, :, :],
                targets=labels[i + 1, :],
                attention_mask=attention_mask,
                sequence_mask=sequence_mask,
            )

            metrics["predictions"] = (
                metrics["predictions"].at[i + 1, :].set(add_metrics["predictions"])
            )
            metrics["loss"] = jax.tree_map(
                lambda x, y: x + y, metrics["loss"], add_loss
            )
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # Generate Attention Mask
        attention_mask = build_padding_attention_mask(
            tokens[0, :, :], self._tokenizer.pad_token_id
        )
        sequence_mask = tokens[0, :, :] != self._tokenizer.pad_token_id

        # evaluate gradients
        (loss, metrics), gradient = jax.value_and_grad(_split_loss_fn, has_aux=True)(
            trainable_params,
            non_trainable_params,
            random_key,
            tokens=tokens[0, :, :],
            targets=labels[0, :],
            attention_mask=attention_mask,
            sequence_mask=sequence_mask,
        )

        metrics["loss"] = loss

        # Build empty predictions array
        all_predictions = self._build_predictions(labels=labels)
        all_predictions = all_predictions.at[0, :].set(metrics["predictions"])

        # Incorporate in the metrics dictionary
        metrics["predictions"] = all_predictions

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics["loss"] = jax.tree_map(lambda x: x / num_acc_grads, metrics["loss"])
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # # gather losses, predictions and labels and gradients across devices
        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # returns predictions and labels, metrics can be computed outside at the end
        # of the epoch to avoid small batches side effects with f1 and mcc
        metrics["labels"] = labels

        # Check that we are not in a discretized classification setting:

        # Build confusion matrix to add metrics.
        confusion_matrix = jnp.zeros(
            labels.shape[2:] + (self._num_classes, self._num_classes),
            dtype=jnp.int32,
        )
        # for loops are ok because they run over num_classes which is always assumed
        # to be a rather small number
        classes_predictions = jnp.argmax(all_predictions, axis=-1)
        for i in range(self._num_classes):
            for j in range(self._num_classes):
                confusion_matrix = confusion_matrix.at[..., i, j].set(
                    jnp.logical_and(labels == i, classes_predictions == j).sum(
                        axis=(0, 1)
                    )
                )
        confusion_matrix = jax.lax.psum(confusion_matrix, axis_name="batch")
        metrics["confusion_matrix"] = confusion_matrix

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(gradient, optimizer_state)
        trainable_params = optax.apply_updates(trainable_params, updates)
        params = hk.data_structures.merge(trainable_params, non_trainable_params)

        new_state = TrainingState(
            step=state.step + 1,
            random_key=subkey,
            optimizer_state=optimizer_state,
            params=params,
        )

        return new_state, metrics

    @functools.partial(jax.jit, static_argnums=0)
    def compute_metrics(
        self, state: TrainingState, tokens: Tokens, labels: Labels
    ) -> Metrics:
        """
        Carries out inference over a batch of data and returns metrics.
        This function is to be called inside a pmap operator
        with `axis_name="batch"`.

        Args:
            state: Training state.
            tokens: Tokens (batch_size, seq_length, *).
            labels: Labels (batch_size, *).

        Returns:
            Metrics.
        """

        random_key = state.random_key
        params = state.params

        mask_random_key, random_key, subkey = jax.random.split(random_key, num=3)

        # Generate Attention Mask
        attention_mask = build_padding_attention_mask(
            tokens, self._tokenizer.pad_token_id
        )
        sequence_mask = tokens != self._tokenizer.pad_token_id

        # Calculate loss
        loss, metrics = self._loss_fn(
            params,
            random_key,
            tokens,
            labels,
            attention_mask=attention_mask,
            sequence_mask=sequence_mask,
        )

        # gather losses, predictions and labels
        # returns predictions and labels, metrics can be computed outside at the end
        # of the epoch to avoid small batches side effects with f1 and mcc
        metrics["loss"] = loss
        metrics["labels"] = labels

        # Build confusion matrix to add to metrics
        confusion_matrix = jnp.zeros(
            labels.shape[1:] + (self._num_classes, self._num_classes),
            dtype=jnp.int32,
        )
        # for loops are ok because they run over num_classes which is always assumed
        # to be a rather small number
        classes_predictions = jnp.argmax(metrics["predictions"], axis=-1)
        for i in range(self._num_classes):
            for j in range(self._num_classes):
                confusion_matrix = confusion_matrix.at[..., i, j].set(
                    jnp.logical_and(labels == i, classes_predictions == j).sum(axis=0)
                )
        confusion_matrix = jax.lax.psum(confusion_matrix, axis_name="batch")
        metrics["confusion_matrix"] = confusion_matrix

        return metrics  # type:ignore
