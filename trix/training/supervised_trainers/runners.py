from typing import Callable, List, Optional, Tuple

import jax
import jaxlib
import tqdm

from trix.dataloaders.language_models.sequence_datasets import LabeledSequenceDataset
from trix.training.base import TrainingState
from trix.training.supervised_trainers.utils import aggregate_metrics_with_predictions
from trix.types import Labels, Metrics, Tokens


def run_train_supervised_epoch(
    update_fn: Callable[[TrainingState, Tokens, Labels], Tuple[TrainingState, Metrics]],
    dataset: LabeledSequenceDataset,
    devices: List[jaxlib.xla_extension.Device],
    num_acc_grads: int,
    batch_size: int,
    training_state: TrainingState,
    epoch_num: int = 0,
) -> Tuple[TrainingState, Metrics]:
    """
    Performs one training epoch over given dataset. It assumes that the training
    state is already sharded on the specified devices.

    Args:
        update_fn: Function that does a forward/backward pass and
            updates training state.
        dataset: Training dataset.
        devices: Training devices.
        num_acc_grads: Number of gradients to accumulate.
        batch_size: Batch size per device per pass.
        training_state: Current training state (already sharded).
        epoch_num: Number of the epoch (for printing purpose only).

    Returns:
        Final training state.
        Aggregated training metrics over the epoch.
    """
    num_devices = len(devices)

    # Training loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Training"

    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []

    for tokens, labels in generator:
        # Reshape data to send to devices, and gradient accumulation
        tokens = tokens.reshape(
            (num_devices, num_acc_grads, batch_size) + tokens.shape[1:]
        )
        labels = labels.reshape(
            (num_devices, num_acc_grads, batch_size) + labels.shape[1:]
        )

        # Update neural network and optimizer
        training_state, training_metrics = jax.pmap(
            update_fn, devices=devices, axis_name="batch", donate_argnums=(0,)
        )(training_state, tokens, labels)

        epoch_metrics.append(training_metrics)

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics_with_predictions(epoch_metrics)

    return training_state, aggregated_metrics


def run_test_supervised_epoch(
    compute_metrics_fn: Callable[[TrainingState, Tokens, Labels], Metrics],
    dataset: LabeledSequenceDataset,
    num_data_parallel_ways: int,
    batch_size: int,
    training_state: TrainingState,
    description: Optional[str] = "Validation",
    num_steps: Optional[int] = None,
) -> Metrics:
    """
    Performs one epoch over validation dataset and returns testing metrics. It assumes
    that the training state is already sharded on the specified devices.

    WARNING: unlike `run_train_epoch` this function does not need gradient accumulation,
    so it expects `batch_size` to be the effective batch size passed to the model.

    Args:
        compute_metrics_fn: Function to carry out forward pass and compute metrics.
            This function is already pmapped or xmapped
        dataset: Dataset to validate on.
        num_data_parallel_ways: Number of data parallel ways used during the validation.
            If the model is not sharded, this is equal to the number of devices.
        batch_size: Effective batch size per device.
        training_state: Current training state (already sharded).
        description: Description that gets printed in the tqdm progress bar.
        num_steps: Number of batches on which the validation will run. If not provided,
            the validation will run over the whole dataset.

    Returns:
        Aggregated validation metrics over num_steps batches. If num_steps is not
            provided, the validation metrics are aggregated over the whole epoch.
    """
    # Testing loop
    total = dataset.num_batches_per_epoch
    desc = description

    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []
    for idx, (tokens, labels) in enumerate(generator):

        if (num_steps is not None) and (idx == num_steps):
            break

        # Reshape data to send to devices
        tokens = tokens.reshape(
            (
                num_data_parallel_ways,
                batch_size,
            )
            + tokens.shape[1:]
        )
        labels = labels.reshape(
            (
                num_data_parallel_ways,
                batch_size,
            )
            + labels.shape[1:]
        )

        # Make predictions
        metrics = compute_metrics_fn(training_state, tokens, labels)

        epoch_metrics.append(metrics)

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics_with_predictions(epoch_metrics)

    return aggregated_metrics  # type: ignore
