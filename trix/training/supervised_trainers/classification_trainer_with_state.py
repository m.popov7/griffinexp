import functools
from typing import Any, Callable, Dict, Optional, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
import optax

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.training.base import TrainingStateWithModelState
from trix.training.supervised_trainers.utils import build_predictions
from trix.types import AttentionMask, Labels, Metrics, RNGKey, SequenceMask, Tokens


class ClassificationTrainerWithState:
    """
    A stateless abstraction around an init_fn/update_fn/compute_metrics_fn
    set of funcs for models that need to maintain internal state
    (e.g. due to moving averages in batch norm).
    This extracts some common boilerplate from the training loop.
    This trainer trains an Encoder-style model (Transformer) with a final
    single or multi label classification head. Might be used for instance for
    fine-tuning.
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        loss_fn: Callable[
            [
                hk.Params,
                RNGKey,
                hk.State,
                Tokens,
                Tokens,
                Callable[
                    [
                        hk.Params,
                        RNGKey,
                        hk.State,
                        Tokens,
                        Optional[AttentionMask],
                        Optional[SequenceMask],
                    ],
                    Dict[str, jnp.ndarray],
                ],
                bool,
            ],
            Tuple[jnp.ndarray, jnp.ndarray],
        ],
        tokenizer: BaseTokenizer,
        optimizer: optax.GradientTransformation,
        num_classes: int = 2,
        parameters_partition_fn: Optional[Callable[[str, str, Any], bool]] = None,
    ):
        """
        Args:
            forward_fn: The transformed forward function of the model (with state).
            loss_fn: The loss function.
            tokenizer: Tokenizer.
            optimizer: Optimizer.
            num_classes: Number of classes predicted. Defaults to 2 for binary
                classification;
            parameters_partition_fn: if specified, this function is used to split
                parameters into trainable and non-trainable parameters. Can be used
                to specify to the trainer not to train some parameters from the model.
                The function takes as inputs the name of the module,
                the name of a given entry in the module data bundle
                (e.g. parameter name) and the corresponding data and returns a boolean.
                See haiku.data_structures.partition documentation for more details.
        """
        self._forward_fn = forward_fn

        self._loss_fn = functools.partial(
            loss_fn,
            forward_fn=self._forward_fn.apply,
        )
        self._optimizer = optimizer
        self._tokenizer = tokenizer
        self._num_classes = num_classes
        self._parameters_partition_fn = parameters_partition_fn

        # Define the function to initialize the predictions.
        self._build_predictions = functools.partial(
            build_predictions, num_classes=num_classes
        )

    @functools.partial(jax.jit, static_argnums=0)
    def init(
        self,
        random_key: RNGKey,
        tokens: Tokens,
        params: Optional[hk.Params] = None,
        state: Optional[hk.State] = None,
    ) -> TrainingStateWithModelState:
        """
        Initializes the Training state.

        Args:
            random_key: Random JAX key.
            tokens: Tokens (batch_size, seq_length, *).
            params: Optionally provide params if already initialized.
            state: Optionally provide model state. Needed if params are provided.
                If params are not provided, it will initiate a new model state.

        Returns:
            Initialized training state.
        """
        random_key, subkey = jax.random.split(random_key)
        if params is None:
            # is_training needs to be True for model.init
            params, state = self._forward_fn.init(subkey, tokens, is_training=True)

        if self._parameters_partition_fn is None:
            trainable_params = params
        else:
            trainable_params, _ = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )
        optimizer_state = self._optimizer.init(trainable_params)

        return TrainingStateWithModelState(
            step=jnp.array(0),
            random_key=random_key,
            model_state=state,
            optimizer_state=optimizer_state,
            params=params,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self,
        state: TrainingStateWithModelState,
        tokens: Tokens,
        labels: Labels,
        is_training: bool = False,
    ) -> Tuple[TrainingStateWithModelState, Metrics]:
        """
        Updates the training state. This function is to be called inside
        a pmap operator with `axis_name="batch"`.

        Args:
            state: Current training state.
            tokens: Tokens (num_accum_grads, batch_size, seq_length, *).
            labels: Labels (num_accum_grads, batch_size, *).
            is_training: Pass to True during training, will change the
                behaviour of Convolutional layers. If starting from a
                pre-trained model, can be set to False.

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads = tokens.shape[0]
        random_key = state.random_key
        params = state.params
        model_state = state.model_state

        *mask_random_key, random_key, subkey = jax.random.split(
            random_key, num=num_acc_grads + 2
        )

        if self._parameters_partition_fn is None:
            trainable_params = params
            non_trainable_params = {}
        else:
            trainable_params, non_trainable_params = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )

        def _split_loss_fn(  # type: ignore
            trainable_params, non_trainable_params, *args, **kwargs
        ):
            params = hk.data_structures.merge(trainable_params, non_trainable_params)
            return self._loss_fn(params, *args, **kwargs)

        # accumulate gradients if needed
        def loop_fun(
            i: int, val: Tuple[Metrics, hk.Params]
        ) -> Tuple[Metrics, hk.Params]:

            metrics, gradient = val

            (add_loss, (add_metrics, _)), add_gradient = jax.value_and_grad(
                _split_loss_fn, has_aux=True
            )(
                trainable_params,
                non_trainable_params,
                random_key=random_key,
                state=model_state,
                tokens=tokens[i + 1, :, :],
                targets=labels[i + 1, :],
                is_training=is_training,
            )

            metrics["predictions"] = (
                metrics["predictions"].at[i + 1, :].set(add_metrics["predictions"])
            )
            metrics["loss"] = jax.tree_map(
                lambda x, y: x + y, metrics["loss"], add_loss
            )
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # evaluate gradients
        (loss, (metrics, new_state)), gradient = jax.value_and_grad(
            _split_loss_fn, has_aux=True
        )(
            trainable_params,
            non_trainable_params,
            random_key=random_key,
            state=model_state,
            tokens=tokens[0, :, :],
            targets=labels[0, :],
            is_training=is_training,
        )
        metrics["loss"] = loss

        # Build empty predictions array
        all_predictions = self._build_predictions(labels=labels)

        all_predictions = all_predictions.at[0, :].set(metrics["predictions"])

        # Incorporate in the metrics dictionary
        metrics["predictions"] = all_predictions

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics["loss"] = jax.tree_map(lambda x: x / num_acc_grads, metrics["loss"])
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # gather losses, predictions and labels and gradients across devices
        metrics["loss"] = jax.tree_map(
            lambda x: jax.lax.pmean(x, axis_name="batch"), metrics["loss"]
        )
        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # returns predictions and labels, metrics can be computed outside at the end
        # of the epoch to avoid small batches side effects with f1 and mcc
        metrics["labels"] = labels

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(gradient, optimizer_state)
        trainable_params = optax.apply_updates(trainable_params, updates)
        params = hk.data_structures.merge(trainable_params, non_trainable_params)

        new_state = TrainingStateWithModelState(
            step=state.step + 1,
            random_key=subkey,
            optimizer_state=optimizer_state,
            params=params,
            model_state=new_state,
        )

        return new_state, metrics

    @functools.partial(jax.jit, static_argnums=0)
    def compute_metrics(
        self,
        state: TrainingStateWithModelState,
        tokens: Tokens,
        labels: Labels,
    ) -> Metrics:
        """
        Carries out inference over a batch of data and returns metrics.
        This function is to be called inside a pmap operator
        with `axis_name="batch"`.

        Args:
            state: Training state.
            tokens: Tokens (batch_size, seq_length, *).
            labels: Labels (batch_size, *).

        Returns:
            Metrics.
        """

        random_key = state.random_key
        params = state.params
        model_state = state.model_state

        mask_random_key, random_key, subkey = jax.random.split(random_key, num=3)

        # Calculate loss
        loss, (metrics, _) = self._loss_fn(
            params,
            random_key=random_key,
            state=model_state,
            tokens=tokens,
            targets=labels,
            is_training=False,  # False for predictions
        )

        # gather losses, predictions and labels
        loss = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), loss)

        # returns predictions and labels, metrics can be computed outside at the end
        # of the epoch to avoid small batches side effects with f1 and mcc
        metrics["loss"] = loss
        metrics["labels"] = labels

        return metrics  # type:ignore
