from typing import List

import jax
import jax.numpy as jnp
import numpy as np

from trix.types import Metrics


def aggregate_metrics_with_predictions(metrics_list: List[Metrics]) -> Metrics:
    """
    Aggregates a list of metrics into a single pytree that contains mean metrics. Here
    we use numpy to avoid memory issues caused when using jnp.arrays and jitting the
    function. These errors are encountered when there is a significant number of batch
    metrics to aggregate (individual predictions in this case).

    Args:
         metrics_list: List of metrics.

    Returns:
        Stacked metrics.
    """
    metrics: Metrics = jax.tree_map(
        lambda *leaf: np.stack(leaf, axis=0),
        *metrics_list,
    )
    return metrics


def build_predictions(labels: jnp.ndarray, num_classes: int) -> jnp.ndarray:
    """
    Initialize the array containing the predictions for the batch in the case of
    single label prediction. Therefore, the array is a zero-array of shape
    [num_acc_grads, batch_size].


    Args:
        labels: Labels array.
        num_classes: Number of classes.

    Returns:
        Zero array where the predictions will be assigned along the gradient
            accumulation.
    """

    predictions = jnp.zeros(
        labels.shape + (num_classes,), dtype=jnp.float32
    )  # [batch_size, num_labels, num_classes]
    return predictions


def build_predictions_regression(labels: jnp.ndarray) -> jnp.ndarray:
    """
    Initialize the array containing the predictions for the batch in the case of
    multi label regression treated as classification.
    Therefore, the array is a zero-array of shape
    [num_acc_grads, batch_size, num_labels, num_bins +1]. num_bins is the number of
    bins chosen to discretize the continuous labels.
    Since it is much easier to transform the labels offline and treat these
    (of shape [num_seq, num_labels, num_bins]), the labels are slightly different than
    in the classification case where they have the shape
    [num_seq, num_labels].

    Args:
        labels (jnp.ndarray): Labels array.
        num_classes (int): Number of classes.

    Returns:
        jnp.ndarray: Zero array where the predictions will be assigned along the
            gradient accumulation.
    """

    predictions = jnp.zeros(
        labels.shape, dtype=jnp.float32
    )  # [batch_size, num_labels, num_classes]
    return predictions
