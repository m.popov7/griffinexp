import functools
from typing import Any, Callable, Dict, Optional, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
import optax

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.training.base import TrainingState
from trix.training.supervised_trainers.classification_trainer import (
    ClassificationTrainer,
)
from trix.training.supervised_trainers.utils import build_predictions_regression
from trix.types import AttentionMask, Labels, Metrics, RNGKey, SequenceMask, Tokens
from trix.utils.masking import build_padding_attention_mask


class DiscretizedRegressionTrainer(ClassificationTrainer):
    """
    A stateless abstraction around an init_fn/update_fn/compute_metrics_fn set of funcs.
    This extracts some common boilerplate from the training loop.
    This trainer trains an Encoder-style model (Transformer) with a final classification
    head with one hot vectors.
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        loss_fn: Callable[
            [
                hk.Params,
                RNGKey,
                Tokens,
                Tokens,
                Callable[
                    [
                        hk.Params,
                        RNGKey,
                        Tokens,
                        Optional[AttentionMask],
                        Optional[SequenceMask],
                    ],
                    Dict[str, jnp.ndarray],
                ],
                Optional[AttentionMask],
            ],
            Tuple[jnp.ndarray, jnp.ndarray],
        ],
        tokenizer: BaseTokenizer,
        optimizer: optax.GradientTransformation,
        parameters_partition_fn: Optional[Callable[[str, str, Any], bool]] = None,
    ):
        """
        Args:
            forward_fn: The transformed forward function of the model.
            loss_fn: The loss function.
            tokenizer: Tokenizer.
            optimizer: Optimizer.
            parameters_partition_fn: if specified, this function is used to split
                parameters into trainable and non-trainable parameters. Can be used
                to specify to the trainer not to train some parameters from the model.
                The function takes as inputs the name of the module,
                the name of a given entry in the module data bundle
                (e.g. parameter name) and the corresponding data and returns a boolean.
                See haiku.data_structures.partition documentation for more details.
        """

        ClassificationTrainer.__init__(
            self,
            forward_fn=forward_fn,
            loss_fn=loss_fn,
            tokenizer=tokenizer,
            optimizer=optimizer,
            parameters_partition_fn=parameters_partition_fn,
        )

        self._build_predictions = build_predictions_regression  # type: ignore[assignment] # noqa: E501

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self,
        state: TrainingState,
        tokens: Tokens,
        labels: Labels,
    ) -> Tuple[TrainingState, Metrics]:
        """
        Updates the training state. This function is to be called inside
        a pmap operator with `axis_name="batch"`.

        Args:
            state: Current training state.
            tokens: Tokens (num_accum_grads, batch_size, seq_length, *).
            labels: Labels (num_accum_grads, batch_size, *).

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads = tokens.shape[0]
        random_key = state.random_key
        params = state.params

        *mask_random_key, random_key, subkey = jax.random.split(
            random_key, num=num_acc_grads + 2
        )

        if self._parameters_partition_fn is None:
            trainable_params = params
            non_trainable_params = {}
        else:
            trainable_params, non_trainable_params = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )

        def _split_loss_fn(  # type: ignore
            trainable_params, non_trainable_params, *args, **kwargs
        ):
            params = hk.data_structures.merge(trainable_params, non_trainable_params)
            return self._loss_fn(params, *args, **kwargs)

        # accumulate gradients if needed
        def loop_fun(
            i: int, val: Tuple[Metrics, hk.Params]
        ) -> Tuple[Metrics, hk.Params]:

            metrics, gradient = val

            # Generate Attention Mask
            attention_mask = build_padding_attention_mask(
                tokens[i + 1, :, :], self._tokenizer.pad_token_id
            )
            sequence_mask = tokens[i + 1, :, :] != self._tokenizer.pad_token_id

            (add_loss, add_metrics), add_gradient = jax.value_and_grad(
                _split_loss_fn, has_aux=True
            )(
                trainable_params,
                non_trainable_params,
                random_key,
                tokens=tokens[i + 1, :, :],
                targets=labels[i + 1, :],
                attention_mask=attention_mask,
                sequence_mask=sequence_mask,
            )

            metrics["predictions"] = (
                metrics["predictions"].at[i + 1, :].set(add_metrics["predictions"])
            )
            metrics["loss"] = jax.tree_map(
                lambda x, y: x + y, metrics["loss"], add_loss
            )
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # Generate Attention Mask
        attention_mask = build_padding_attention_mask(
            tokens[0, :, :], self._tokenizer.pad_token_id
        )
        sequence_mask = tokens[0, :, :] != self._tokenizer.pad_token_id

        # evaluate gradients
        (loss, metrics), gradient = jax.value_and_grad(_split_loss_fn, has_aux=True)(
            trainable_params,
            non_trainable_params,
            random_key,
            tokens=tokens[0, :, :],
            targets=labels[0, :],
            attention_mask=attention_mask,
            sequence_mask=sequence_mask,
        )

        metrics["loss"] = loss

        # Build empty predictions array
        all_predictions = self._build_predictions(labels=labels)
        all_predictions = all_predictions.at[0, :].set(metrics["predictions"])

        # Incorporate in the metrics dictionary
        metrics["predictions"] = all_predictions

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics["loss"] = jax.tree_map(lambda x: x / num_acc_grads, metrics["loss"])
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # # gather losses, predictions and labels and gradients across devices
        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # returns predictions and labels, metrics can be computed outside at the end
        # of the epoch to avoid small batches side effects with f1 and mcc
        metrics["labels"] = labels

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(gradient, optimizer_state)
        trainable_params = optax.apply_updates(trainable_params, updates)
        params = hk.data_structures.merge(trainable_params, non_trainable_params)

        new_state = TrainingState(
            step=state.step + 1,
            random_key=subkey,
            optimizer_state=optimizer_state,
            params=params,
        )

        return new_state, metrics

    @functools.partial(jax.jit, static_argnums=0)
    def compute_metrics(
        self, state: TrainingState, tokens: Tokens, labels: Labels
    ) -> Metrics:
        """
        Carries out inference over a batch of data and returns metrics.
        This function is to be called inside a pmap operator
        with `axis_name="batch"`.

        Args:
            state: Training state.
            tokens: Tokens (batch_size, seq_length, *).
            labels: Labels (batch_size, *).

        Returns:
            Metrics.
        """

        random_key = state.random_key
        params = state.params

        mask_random_key, random_key, subkey = jax.random.split(random_key, num=3)

        # Generate Attention Mask
        attention_mask = build_padding_attention_mask(
            tokens, self._tokenizer.pad_token_id
        )
        sequence_mask = tokens != self._tokenizer.pad_token_id

        # Calculate loss
        loss, metrics = self._loss_fn(
            params,
            random_key,
            tokens,
            labels,
            attention_mask=attention_mask,
            sequence_mask=sequence_mask,
        )

        # gather losses, predictions and labels
        # returns predictions and labels, metrics can be computed outside at the end
        # of the epoch to avoid small batches side effects with f1 and mcc
        metrics["loss"] = loss
        metrics["labels"] = labels

        return metrics  # type:ignore
