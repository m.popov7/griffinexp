from __future__ import annotations

import functools
from typing import Any, Callable

import haiku as hk
import jax
import jax.numpy as jnp
import optax

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.training.base import TrainingState
from trix.training.losses import cross_entropy_loss
from trix.training.utils import (
    mask_sequence_bert_style,
    synchronize_accumulated_gradients,
)
from trix.types import Metrics, RNGKey, Tokens
from trix.utils.masking import build_padding_attention_mask


class EncoderMLMTrainer:
    """
    A stateless abstraction around an init_fn/update_fn/predict_fn set of funcs.
    This extracts some common boilerplate from the training loop.
    This trainer trains an Encoder language model through masked language
    modelling (MLM) also considered as BERT style.
    """

    def __init__(
        self,
        apply_fn: hk.Transformed.apply,
        init_fn: hk.Transformed.apply,
        tokenizer: BaseTokenizer,
        optimizer: optax.MultiSteps,
        noising_ratio: float = 0.15,
        masking_prob: float = 0.8,
        random_token_prob: float = 0.1,
        parameters_partition_fn: Callable[[str, str, Any], bool] | None = None,
        masking_fn: Callable | None = None,
    ):
        """
        Args:
            apply_fn: apply_fn
            init_fn: init_fn
            tokenizer: Tokenizer.
            optimizer: Optimizer, must be multi steps optimizer to allow for
                gradient accumulation.
            noising_ratio: Masking ratio.
            masking_prob: Masking probability.
            random_token_prob: Random token probability in masking.
            parameters_partition_fn: if specified, this function is used to split
                parameters into trainable and non-trainable parameters. Can be used
                to specify to the trainer not to train some parameters from the model.
                The function takes as inputs the name of the module,
                the name of a given entry in the module data bundle
                (e.g. parameter name) and the corresponding data and returns a boolean.
                See haiku.data_structures.partition documentation for more details.
            masking_fn: Masking function used to mask the inputs to the model during
                the pre-training. The masking function should have the following
                signature:

                def masking_fn(
                    tokens: Tokens,
                    random_key: RNGKey,
                    random_token_indices: jnp.ndarray,
                    mask_id: int,
                    pad_id: int,
                    noising_ratio: float,
                    masking_prob: float = 0.8,
                    random_token_prob: float = 0.1,
                ) -> Tuple[Tokens, Tokens]

                It returns the tokens masked and the associated targets.
        """
        self._init_fn = init_fn
        self._apply_fn = apply_fn
        self._optimizer = optimizer
        self._tokenizer = tokenizer
        self._parameters_partition_fn = parameters_partition_fn
        self._loss_fn = functools.partial(cross_entropy_loss, apply_fn=apply_fn)

        # MLM specific attributes
        random_token_indices = jnp.asarray(
            [tokenizer.token_to_id(tok) for tok in tokenizer.standard_tokens]
        )

        if masking_fn is None:
            masking_fn = mask_sequence_bert_style

        self._bert_masking_fn = functools.partial(
            masking_fn,
            random_token_indices=random_token_indices,
            mask_id=tokenizer.mask_token_id,
            pad_id=tokenizer.pad_token_id,
            noising_ratio=noising_ratio,
            masking_prob=masking_prob,
            random_token_prob=random_token_prob,
        )

    def build_init_fn(self) -> Callable:
        xmapped_init_fn: Callable = jax.experimental.maps.xmap(
            fun=self.init,
            in_axes=(
                ["shard", "batch", ...],
                [
                    ...,
                ],
            ),
            out_axes=["shard", "batch", ...],
            axis_resources={"shard": "shard"},
            donate_argnums=(0,),
        )
        return xmapped_init_fn

    def build_init_fn_from_params(self) -> Callable:
        xmapped_init_fn: Callable = jax.experimental.maps.xmap(
            fun=self.init,
            in_axes=(
                ["shard", "batch", ...],
                [
                    ...,
                ],
                ["shard", "batch", ...],
            ),
            out_axes=["shard", "batch", ...],
            axis_resources={"shard": "shard"},
            donate_argnums=(0,),
        )
        return xmapped_init_fn

    def build_xmapped_update_fn(self) -> Callable:
        """
        Xmaps the self._update function nover the defined devices_mesh.
            The training_state has in_axis= ["shard", "batch", ...] since the first axis
        corresponds to the number of shards and the second corresponds to the number of
        data_parallel_ways axis.
            The tokens correspond to 'axis_name="batch"' since the first axis
        corresponds to the number of data_parallel_ways.

        The function returns a new training_state and training metrics.

        For updates where the optimizer updates are different along the batch_axis,
        if the out_axis indicate ["shard", ...], the following error is raised by xmap:
        "One of xmap results has an out_axes specification of ['shard', ...], but is
        actually mapped along more axes defined by this xmap call: batch". The out_axis
        needs to indicate ["shard", "batch", ...].

        For updates where the accumulated gradients are synchronized and the
        optimizer_state have been pmean along the "batch" axis, the out_axis
        should be ["shard", ...], but we observe that it also works with
        ["shard", "batch", ...].

        Since the optimizer state is different along "batch" axis in all training steps
        except those that call the pmean operation on this "batch" axis, i.e the steps
        where the gradient accumulation is applied, the training state needs to have its
        first two axis being [num_shards, num_data_parallel]. Therefore, the xmapped
        update function has in_axes ["shard", "batch", ...] for the training_state and
        equal out_axes.


        Returns:
            The jitted function with xmap
        """

        xmapped_update_fn: Callable = jax.experimental.maps.xmap(
            fun=self.update,
            # in_axes=(["shard", ...], ["batch", ...]),
            in_axes=(["shard", "batch", ...], ["batch", ...]),
            out_axes=(
                # ["shard", ...],
                ["shard", "batch", ...],
                [
                    "shard",
                    ...,
                ],
            ),
            axis_resources={"shard": "shard", "batch": "batch"},
            donate_argnums=(0,),
        )
        return xmapped_update_fn

    def build_xmapped_metrics_fn(self) -> Callable:
        """
        Xmaps the self._compute_metrics function nover the defined devices_mesh.
            The training_state has in_axis= ["shard", "batch", ...] since the first axis
        corresponds to the number of shards and the second corresponds to the number of
        data_parallel_ways axis.
            The tokens correspond to 'axis_name="batch"' since the first axis
        corresponds to the number of data_parallel_ways.

        The function returns a new training_state and training metrics.

        Returns:
            The jitted function with xmap
        """

        xmapped_metrics_fn: Callable = jax.experimental.maps.xmap(
            fun=self.compute_metrics,
            in_axes=(["shard", "batch", ...], ["batch", ...]),
            out_axes=["shard", ...],
            axis_resources={"shard": "shard", "batch": "batch"},
        )
        return xmapped_metrics_fn

    @functools.partial(jax.jit, static_argnums=0)
    def init(
        self,
        random_key: RNGKey,
        tokens: Tokens,
        pretrained_params: hk.Params | None = None,
    ) -> TrainingState:
        """
        Initializes the TrainingState. Initialization is done on accelerators but
        in the future it might be preferable to do it on CPU when doable.
        This function is to be:
            - called inside a pmap operator with axis_name="batch"
            - called inside a x-mapped operator with `axis_name="shard"' for the
            random_key (use the self.build_init_fn for standard use).

        Args:
            random_key: Random JAX keys of shape (2,).
            tokens: Tokens of shape (batch_size, seq_length, *).

        Returns:
            Sharded initialized training state.
        """

        random_key, subkey = jax.random.split(random_key)
        if pretrained_params is None:
            params = self._init_fn(subkey, tokens)
        else:
            params = pretrained_params

        if self._parameters_partition_fn is None:
            trainable_params = params
        else:
            trainable_params, _ = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )
        optimizer_state = self._optimizer.init(trainable_params)

        return TrainingState(  # type: ignore
            step=jnp.array(0),
            params=params,
            optimizer_state=optimizer_state,
            random_key=random_key,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self,
        state: TrainingState,
        tokens: Tokens,
    ) -> tuple[TrainingState, Metrics]:
        """
        Updates the training state. This function is to be:
            - called inside a pmap operator with axis_name="batch"
            - called inside a x-mapped operator as described in the documentation of
            self.build_xmapped_update_fn.


        Args:
            state: Current training state.
            tokens: Tokens (batch_size, seq_length, *).

        Returns:
            Updated training state.
            Metrics.
        """
        random_key = state.random_key
        params = state.params

        random_key, subkey = jax.random.split(random_key)

        # partition params into trainable and non-trainable if needed
        if self._parameters_partition_fn is None:
            trainable_params = params
            non_trainable_params = {}
        else:
            trainable_params, non_trainable_params = hk.data_structures.partition(
                self._parameters_partition_fn, params
            )

        # Mask tokens and obtain labels based on MLM objective
        tokens, targets = self._bert_masking_fn(tokens, jnp.stack(subkey, axis=0))
        random_key, subkey = jax.random.split(random_key)

        # Generate Attention Mask
        attention_mask = build_padding_attention_mask(
            tokens, self._tokenizer.pad_token_id
        )
        # Generate Sequence Mask
        sequence_mask = targets != self._tokenizer.pad_token_id

        def _split_loss_fn(  # type: ignore
            trainable_params, non_trainable_params, *args, **kwargs
        ):
            params = hk.data_structures.merge(trainable_params, non_trainable_params)
            return self._loss_fn(params, *args, **kwargs)

        (loss, metrics), gradient = jax.value_and_grad(_split_loss_fn, has_aux=True)(
            trainable_params,
            non_trainable_params,
            random_key,
            tokens=tokens,
            targets=targets,
            sequence_mask=sequence_mask,
            attention_mask=attention_mask,
        )
        metrics["loss"] = loss
        # update optimizer and weights
        optimizer_state = state.optimizer_state

        # synchronize workers only when done accumulating gradients
        optimizer_state, gradient = jax.lax.cond(
            optimizer_state.mini_step == self._optimizer._every_k_schedule(0) - 1,
            lambda x, y: synchronize_accumulated_gradients(x, y),
            lambda x, y: (x, y),
            optimizer_state,
            gradient,
        )

        updates, optimizer_state = self._optimizer.update(
            gradient, optimizer_state, params
        )

        # update parameters
        trainable_params = optax.apply_updates(trainable_params, updates)
        params = hk.data_structures.merge(trainable_params, non_trainable_params)

        new_state = TrainingState(
            step=state.step + 1,
            random_key=subkey,
            optimizer_state=optimizer_state,
            params=params,
        )

        # gather metrics across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)

        return new_state, metrics  # type: ignore

    @functools.partial(jax.jit, static_argnums=0)
    def compute_metrics(self, state: TrainingState, tokens: Tokens) -> Metrics:
        """
        Computes inference over a batch of data and returns metrics.
        his function is to be:
            - called inside a pmap operator with axis_name="batch"
            - called inside a x-mapped operator as described in the documentation of
            self.build_xmapped_update_fn.

        Args:
            state: Training state.
            tokens: Tokens (batch_size, seq_length, *).

        Returns:
            Training Metrics.
        """

        random_key = state.random_key
        params = state.params

        mask_random_key, random_key, _ = jax.random.split(random_key, num=3)

        # MLM-Mask tokens and obtain labels based on masking
        tokens, labels = self._bert_masking_fn(tokens, mask_random_key)

        # Generate Attention Mask
        attention_mask = build_padding_attention_mask(
            tokens, self._tokenizer.pad_token_id
        )

        # Generate Sequence Mask
        sequence_mask = labels != self._tokenizer.pad_token_id

        # Calculate loss
        (loss, metrics) = self._loss_fn(
            params,
            random_key,
            tokens,
            labels,
            sequence_mask=sequence_mask,
            attention_mask=attention_mask,
        )
        metrics["loss"] = loss

        # Average metrics across the data parallelism axis
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)

        return metrics  # type: ignore
