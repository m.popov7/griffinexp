from __future__ import annotations

from typing import Callable

import jax
import jaxlib
import tqdm

from trix.dataloaders.language_models.sequence_datasets import PairedSequenceDataset
from trix.training.base import TrainingState, aggregate_metrics
from trix.types import Metrics, Tokens


def run_train_epoch_enc_dec(
    update_fn: Callable[[TrainingState, Tokens], tuple[TrainingState, Metrics]],
    dataset: PairedSequenceDataset,
    devices: list[jaxlib.xla_extension.Device],
    num_acc_grads: int,
    batch_size: int,
    training_state: TrainingState,
    epoch_num: int = 0,
) -> tuple[TrainingState, Metrics]:
    """
    Performs one training epoch over given dataset. It assumes that the training
    state is already sharded on the specified devices. Used specifically for encoder
    decoder models where the dataset yields both encoder and decoder tokens.

    Args:
        update_fn: Functions which does a forward/backward pass and updates parameters.
        dataset: Training dataset.
        devices: Training devices.
        num_acc_grads: Number of gradients to accumulate.
        batch_size: Batch size per device per pass.
        training_state: Current training state (already sharded).
        epoch_num: Number of the epoch (for printing purpose only).

    Returns:
        Final training state and the aggregated training metrics over the epoch.
    """
    num_devices = len(devices)

    # Training loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Training"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []

    for encoder_tokens, decoder_tokens in generator:

        # Reshape data to send to devices, and gradient accumulation
        encoder_tokens = encoder_tokens.reshape(
            (num_devices, num_acc_grads, batch_size) + encoder_tokens.shape[1:]
        )

        decoder_tokens = decoder_tokens.reshape(
            (num_devices, num_acc_grads, batch_size) + decoder_tokens.shape[1:]
        )

        # Update neural network and optimizer
        training_state, training_metrics = jax.pmap(
            update_fn, devices=devices, axis_name="batch"
        )(training_state, encoder_tokens, decoder_tokens)

        epoch_metrics.append(jax.tree_map(lambda x: x[0], training_metrics))

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return training_state, aggregated_metrics


def run_test_epoch_enc_dec(
    compute_metrics_fn: Callable[[TrainingState, Tokens], Metrics],
    dataset: PairedSequenceDataset,
    devices: list[jaxlib.xla_extension.Device],
    batch_size: int,
    training_state: TrainingState,
    epoch_num: int = 0,
) -> Metrics:
    """
    Performs one epoch over test dataset and returns testing metrics. It assumes
    that the training state is already sharded on the specified devices. Used
    specifically for encoder decoder models where the dataset yields both encoder
    and decoder tokens.

    WARNING: unlike `run_train_epoch` this function does not need gradient accumulation,
    so it expects `batch_size` to be the effective batch size passed to the model.

    Args:
        compute_metrics_fn: Function which carries out forward pass and computes metrics
        dataset: Dataset to validate on.
        devices: Devices to use for training.
        batch_size: Effective batch size per device.
        training_state: Current training state (already sharded).
        epoch_num: Current epoch (for printing purpose only).

    Returns:
        Aggregated validation metrics over the epoch.
    """
    num_devices = len(devices)

    # Testing loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Test"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []
    for encoder_tokens, decoder_tokens in generator:

        # Reshape data to send to devices
        encoder_tokens = encoder_tokens.reshape(
            (
                num_devices,
                batch_size,
            )
            + encoder_tokens.shape[1:]
        )

        decoder_tokens = decoder_tokens.reshape(
            (
                num_devices,
                batch_size,
            )
            + decoder_tokens.shape[1:]
        )

        # Make predictions
        metrics = jax.pmap(compute_metrics_fn, devices=devices, axis_name="batch")(
            training_state, encoder_tokens, decoder_tokens
        )

        epoch_metrics.append(jax.tree_map(lambda x: x[0], metrics))

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return aggregated_metrics  # type: ignore
