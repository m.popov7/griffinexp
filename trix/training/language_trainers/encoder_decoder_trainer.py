from __future__ import annotations

import functools
from typing import Callable

import haiku as hk
import jax
import jax.numpy as jnp
import optax
from transformers import PreTrainedTokenizer

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.training.base import TrainingState
from trix.training.utils import get_causal_labels, mask_sequence_bert_style
from trix.types import AttentionMask, Metrics, RNGKey, SequenceMask, Tokens
from trix.utils.masking import build_causal_attention_mask, build_padding_attention_mask


class EncoderDecoderCausalTrainer:
    """
    A stateless abstraction around an init_fn/update_fn/compute_metrics_fn set of funcs.
    This extracts some common boilerplate from the training loop. This trainer trains an
    Encoder - Decoder model through Alexa TM's causal and denoising objective where
    sentences are split in two for encoder and decoder tokens and teacher forcing
    is used to train the decoder. Additional noise is added to decoder tokens.

    Example:
        Input token: "How are you today ? I am fine, thank you"
        Encoder token: "How are you today ?"
        Input Decoder token: "<bos> I am fine, <mask> you"
        Output Decoder token: "I am fine, thank you"
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        tokenizer: BaseTokenizer | PreTrainedTokenizer,
        loss_fn: Callable[
            [
                hk.Params,
                RNGKey,
                Tokens,
                Tokens,
                Tokens,
                Callable[
                    [
                        hk.Params,
                        RNGKey,
                        Tokens,
                        Tokens,
                        AttentionMask | None,
                        AttentionMask | None,
                        AttentionMask | None,
                    ],
                    tuple[dict[str, jnp.ndarray], dict[str, jnp.ndarray]],
                ],
                SequenceMask,
                SequenceMask | None,
                SequenceMask | None,
                SequenceMask | None,
                AttentionMask | None,
            ],
            tuple[jnp.ndarray, Metrics],
        ],
        optimizer: optax.GradientTransformation,
        num_decoding_steps: int,
        noising_ratio: float = 0.15,
        masking_prob: float = 0.8,
        random_token_prob: float = 0.1,
    ):
        """
        Args:
            forward_fn: The transformed forward function of the model.
            tokenizer: Tokenizer.
            loss_fn: The loss function to optimize.
            optimizer: Optimizer.
            num_decoding_steps: Number of decoding steps to take during inference.
            noising_ratio: Masking ratio.
            masking_prob: Masking probability.
            random_token_prob: Random token probability in masking.

        """
        self._forward_fn = forward_fn
        self._loss_fn = functools.partial(
            loss_fn,
            forward_fn=self._forward_fn.apply,
        )

        self._optimizer = optimizer
        self._tokenizer = tokenizer

        self._num_decoding_steps = num_decoding_steps

        # T5 specific attributes
        if isinstance(tokenizer, BaseTokenizer):
            random_token_indices = jnp.asarray(
                [tokenizer.token_to_id(tok) for tok in tokenizer.standard_tokens]
            )
        else:
            random_token_indices = jnp.asarray(
                [
                    tokenizer.convert_tokens_to_ids(tok)
                    for tok in tokenizer.standard_tokens
                ]
            )

        self._bert_masking_fn = functools.partial(
            mask_sequence_bert_style,
            random_token_indices=random_token_indices,
            mask_id=tokenizer.mask_token_id,
            pad_id=tokenizer.pad_token_id,
            noising_ratio=noising_ratio,
            masking_prob=masking_prob,
            random_token_prob=random_token_prob,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def init(
        self, random_key: jnp.ndarray, encoder_tokens: Tokens, decoder_tokens: Tokens
    ) -> TrainingState:
        """
        Initializes the training state.

        Args:
            random_key: Random JAX key.
            encoder_tokens: Tokens (batch_size,encoder_seq_length).
            decoder_tokens: Tokens (batch_size,decoder_seq_length).

        Returns:
            Initialized Training state.
        """
        random_key, sub_key = jax.random.split(random_key)
        params = self._forward_fn.init(sub_key, encoder_tokens, decoder_tokens)
        optimizer_state = self._optimizer.init(params)

        return TrainingState(
            step=jnp.array(0),
            random_key=random_key,
            optimizer_state=optimizer_state,
            params=params,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self, state: TrainingState, encoder_tokens: Tokens, decoder_tokens: Tokens
    ) -> tuple[TrainingState, Metrics]:
        """
        Updates the training state. *This is a pmapped function.

        Args:
            state: Current training state.
            encoder_tokens: Encoder input tokens
                of shape (num_acc_grads, batch_size, seq_length, *).
            decoder_tokens: Decoder input tokens
                of shape (num_acc_grads, batch_size, seq_length, *).

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads, batch_size, decoder_seq_len = decoder_tokens.shape[0:3]
        params = state.params

        random_key = state.random_key
        random_key, sub_key = jax.random.split(random_key)

        *mask_random_key, random_key, sub_key = jax.random.split(
            random_key, num=num_acc_grads + 2
        )

        # Mask for all non-padded tokens
        decoder_sequence_mask = decoder_tokens != self._tokenizer.pad_token_id

        # Get CLM labels from decoder_tokens before they are bert style masked
        labels = jax.vmap(get_causal_labels, in_axes=(0, None, None))(
            decoder_tokens,
            self._tokenizer.eos_token_id,
            self._tokenizer.pad_token_id,
        )

        # Bert style masking of decoder tokens
        decoder_tokens, bert_targets = jax.vmap(self._bert_masking_fn)(
            decoder_tokens, jnp.stack(mask_random_key, axis=0)
        )

        # Mask for tokens that were masked/randomly assigned by bert style masking
        denoising_sequence_mask = bert_targets != self._tokenizer.pad_token_id

        # Generate Attention Masks
        encoder_attention = build_padding_attention_mask(
            encoder_tokens[0, :, :], self._tokenizer.pad_token_id
        )

        decoder_attention = build_causal_attention_mask(batch_size, decoder_seq_len)

        # accumulate gradients if needed
        def loop_fun(
            i: int, val: tuple[Metrics, hk.Params]
        ) -> tuple[Metrics, hk.Params]:
            metrics, gradient = val

            # Generate Attention Masks
            encoder_attention = build_padding_attention_mask(
                encoder_tokens[i + 1, :, :], self._tokenizer.pad_token_id
            )

            (add_loss, add_metrics), add_gradient = jax.value_and_grad(
                self._loss_fn, has_aux=True
            )(
                params,
                random_key,
                encoder_tokens=encoder_tokens[i + 1, :, :],
                decoder_tokens=decoder_tokens[i + 1, :, :],
                targets=labels[i + 1, :, :],
                denoising_sequence_mask=denoising_sequence_mask[i + 1],
                decoder_sequence_mask=decoder_sequence_mask[i + 1],
                encoder_attention_mask=encoder_attention,
                decoder_attention_mask=decoder_attention,
            )
            add_metrics["loss"] = add_loss

            metrics = jax.tree_map(lambda x, y: x + y, metrics, add_metrics)
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # evaluate gradients
        (loss, metrics), gradient = jax.value_and_grad(self._loss_fn, has_aux=True)(
            params,
            random_key,
            encoder_tokens[0, :, :],
            decoder_tokens[0, :, :],
            targets=labels[0, :, :],
            denoising_sequence_mask=denoising_sequence_mask[0],
            decoder_sequence_mask=decoder_sequence_mask[0],
            encoder_attention_mask=encoder_attention,
            decoder_attention_mask=decoder_attention,
        )
        metrics["loss"] = loss

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics = jax.tree_map(lambda x: x / num_acc_grads, metrics)
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # gather metrics and gradients across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)
        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(gradient, optimizer_state)
        params = optax.apply_updates(params, updates)

        new_state = TrainingState(
            step=state.step + 1,
            random_key=sub_key,
            optimizer_state=optimizer_state,
            params=params,
        )

        return new_state, metrics

    @functools.partial(jax.jit, static_argnums=0)
    def compute_metrics(
        self, state: TrainingState, encoder_tokens: Tokens, decoder_tokens: Tokens
    ) -> Metrics:
        """
        Autoregressive prediction. The prediction from the decoder from timestep t
        are appended to input for timestep t + 1. *This is a pmapped function.

        Args:
            state: Training state.
            encoder_tokens: Encoder input tokens
                of shape (batch_size, encoder_seq_length,*).
            decoder_tokens: Decoder input tokens
                of shape (batch_size, decoder_seq_length,*).

        Returns:
            Metrics.
        """

        batch_size, decoder_seq_len = decoder_tokens.shape[0:2]
        params = state.params

        random_key = state.random_key
        mask_random_key, random_key, sub_key = jax.random.split(random_key, num=3)

        # Mask for all non-padded tokens
        decoder_sequence_mask = decoder_tokens != self._tokenizer.pad_token_id

        # Get CLM labels from decoder_tokens before they are masked bert style
        labels = get_causal_labels(
            decoder_tokens,
            self._tokenizer.eos_token_id,
            self._tokenizer.pad_token_id,
        )

        # Bert style masking of decoder tokens
        decoder_tokens, bert_targets = self._bert_masking_fn(
            decoder_tokens, mask_random_key
        )

        # Mask for tokens that were masked/randomly assigned by bert style masking
        denoising_sequence_mask = bert_targets != self._tokenizer.pad_token_id

        # Generate Attention Masks
        encoder_attention = build_padding_attention_mask(
            encoder_tokens, self._tokenizer.pad_token_id
        )

        decoder_attention = build_causal_attention_mask(batch_size, decoder_seq_len)

        # autoregression with greedy selection
        def greedy_decode(seq_position: int, inputs: jnp.array) -> jnp.array:
            decoded_tokens = inputs

            # calculate logits
            _, decoder_outputs = self._forward_fn.apply(
                params,
                random_key,
                encoder_tokens,
                decoded_tokens,
                encoder_attention_mask=encoder_attention,
                decoder_attention_mask=decoder_attention,
            )

            logits = decoder_outputs["logits"]

            # take only the predictions from seq_position
            preds = jnp.argmax(logits, axis=-1)[..., seq_position]

            # set preds as input for next iteration
            decoded_tokens = decoded_tokens.at[:, seq_position].set(preds)

            return decoded_tokens

        # create an input of <bos> token ids as first position,<pad> tokens elsewhere
        init_inputs = jnp.full_like(decoder_tokens, self._tokenizer.bos_token_id)
        init_inputs = init_inputs.at[:, 1:].set(self._tokenizer.pad_token_id)

        # autoregressive prediction for num_decoding_steps
        decoded_preds = jax.lax.fori_loop(
            lower=0,
            upper=self._num_decoding_steps,
            body_fun=greedy_decode,
            init_val=init_inputs,
        )

        loss, metrics = self._loss_fn(
            params,
            random_key,
            encoder_tokens=encoder_tokens,
            decoder_tokens=decoded_preds,
            targets=labels,
            denoising_sequence_mask=denoising_sequence_mask,
            decoder_sequence_mask=decoder_sequence_mask,
            encoder_attention_mask=encoder_attention,
            decoder_attention_mask=decoder_attention,
        )
        metrics["loss"] = loss

        # gather  metrics across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)

        return metrics  # type:ignore


class DecoderOnlyCausalTrainer(EncoderDecoderCausalTrainer):
    """
    Used for training decoder-only model versions of Encoder-decoder model.
    Specifically T5 decoder-only models.
    Class added to be analogous to t5x for training decoder-only T5 models.
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        tokenizer: BaseTokenizer | PreTrainedTokenizer,
        loss_fn: Callable,
        optimizer: optax.GradientTransformation,
        num_decoding_steps: int,
        noising_ratio: float = 0.15,
        masking_prob: float = 0.8,
        random_token_prob: float = 0.1,
        mask_first_token: bool = True,
    ):
        """
        Args:
            forward_fn: Forward function.
            tokenizer: Tokenizer.
            loss_fn: Loss function.
            optimizer: Optimizer.
            num_decoding_steps: Number of decoding steps.
            noising_ratio: Ratio of noising to apply to decoder input tokens.
            masking_prob: Probability of masking tokens.
            random_token_prob: Probability of replacing tokens with random tokens.
            causal_masking: Whether to mask the first token in the target sequence.
                This is sometimes used for a <BOS> token to start the decoding process.
                By default, this is set to True.
        """
        super().__init__(
            forward_fn=forward_fn,
            tokenizer=tokenizer,
            loss_fn=loss_fn,
            optimizer=optimizer,
            num_decoding_steps=num_decoding_steps,
            noising_ratio=noising_ratio,
            masking_prob=masking_prob,
            random_token_prob=random_token_prob,
        )
        self.mask_first_token = mask_first_token

    @functools.partial(jax.jit, static_argnums=0)
    def init(
        self,
        random_key: jnp.ndarray,
        decoder_input_tokens: Tokens,
    ) -> TrainingState:
        """
        Initializes the training state.
        Note: Decoder only architecture does not need encoder tokens. The hidden states
        are derived from the input decoder tokens alone
        Args:
            random_key: Random JAX key.
            decoder_tokens: Tokens (batch_size,decoder_seq_length).

        Returns:
            Initialized Training state.
        """
        random_key, sub_key = jax.random.split(random_key)
        params = self._forward_fn.init(sub_key, decoder_input_tokens)
        optimizer_state = self._optimizer.init(params)

        return TrainingState(
            step=jnp.array(0),
            random_key=random_key,
            optimizer_state=optimizer_state,
            params=params,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def update(
        self,
        state: TrainingState,
        decoder_input_tokens: Tokens,
        decoder_output_tokens: Tokens,
    ) -> tuple[TrainingState, Metrics]:
        """
        Updates the training state. *This is a pmapped function.

        Args:
            state: Current training state.
            decoder_tokens: Decoder input tokens
                of shape (num_acc_grads, batch_size, seq_length, *).
            decoder_output_tokens: Decoder output tokens
                of shape (num_acc_grads, batch_size, seq_length, *)

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads, batch_size, decoder_seq_len = decoder_input_tokens.shape[0:3]
        params = state.params

        random_key = state.random_key
        random_key, sub_key = jax.random.split(random_key)

        *mask_random_key, random_key, sub_key = jax.random.split(
            random_key, num=num_acc_grads + 2
        )

        # Mask for all non-padded tokens

        decoder_sequence_mask = decoder_output_tokens != self._tokenizer.pad_token_id

        # Get CLM labels from decoder_tokens before they are bert style masked
        if self.mask_first_token:
            labels = jax.vmap(get_causal_labels, in_axes=(0, None, None))(
                decoder_output_tokens,
                self._tokenizer.eos_token_id,
                self._tokenizer.pad_token_id,
            )
        else:
            labels = decoder_output_tokens

        # Bert style masking of decoder tokens
        decoder_input_tokens, bert_targets = jax.vmap(self._bert_masking_fn)(
            decoder_input_tokens, jnp.stack(mask_random_key, axis=0)
        )

        # Mask for tokens that were masked/randomly assigned by bert style masking
        denoising_sequence_mask = bert_targets != self._tokenizer.pad_token_id

        decoder_attention = build_causal_attention_mask(batch_size, decoder_seq_len)

        # accumulate gradients if needed
        def loop_fun(
            i: int, val: tuple[Metrics, hk.Params]
        ) -> tuple[Metrics, hk.Params]:
            metrics, gradient = val

            (add_loss, add_metrics), add_gradient = jax.value_and_grad(
                self._loss_fn, has_aux=True
            )(
                params,
                random_key,
                decoder_tokens=decoder_input_tokens[i + 1, :, :],
                targets=labels[i + 1, :, :],
                denoising_sequence_mask=denoising_sequence_mask[i + 1],
                decoder_sequence_mask=decoder_sequence_mask[i + 1],
                decoder_attention_mask=decoder_attention,
            )
            add_metrics["loss"] = add_loss

            metrics = jax.tree_map(lambda x, y: x + y, metrics, add_metrics)
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # evaluate gradients
        (loss, metrics), gradient = jax.value_and_grad(self._loss_fn, has_aux=True)(
            params,
            random_key,
            decoder_tokens=decoder_input_tokens[0, :, :],
            targets=labels[0, :, :],
            denoising_sequence_mask=denoising_sequence_mask[0],
            decoder_sequence_mask=decoder_sequence_mask[0],
            decoder_attention_mask=decoder_attention,
        )
        metrics["loss"] = loss

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics = jax.tree_map(lambda x: x / num_acc_grads, metrics)
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # gather metrics and gradients across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)
        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(
            gradient, optimizer_state, params
        )
        params = optax.apply_updates(params, updates)

        new_state = TrainingState(
            step=state.step + 1,
            random_key=sub_key,
            optimizer_state=optimizer_state,
            params=params,
        )

        return new_state, metrics

    @functools.partial(jax.jit, static_argnums=0)
    def compute_metrics(
        self,
        state: TrainingState,
        decoder_input_tokens: Tokens,
        decoder_output_tokens: Tokens,
    ) -> Metrics:
        """
        Autoregressive prediction. The prediction from the decoder from timestep t
        are appended to input for timestep t + 1. *This is a pmapped function.

        Args:
            state: Training state.
            decoder_tokens: Decoder input tokens
                of shape (batch_size, decoder_seq_length,*).
            decoder_mask: Optional decoder attention mask
                of shape (batch_size, 1, decoder_seq_length, decoder_seq_length).

        Returns:
            Metrics.
        """
        batch_size, decoder_seq_len = decoder_input_tokens.shape[0:2]
        params = state.params

        random_key = state.random_key
        mask_random_key, random_key, sub_key = jax.random.split(random_key, num=3)

        # Mask for all non-padded tokens
        decoder_sequence_mask = decoder_input_tokens != self._tokenizer.pad_token_id

        # Get CLM labels from decoder_tokens before they are masked bert style
        labels = get_causal_labels(
            decoder_output_tokens,
            self._tokenizer.eos_token_id,
            self._tokenizer.pad_token_id,
        )

        # Bert style masking of decoder tokens
        decoder_input_tokens, bert_targets = self._bert_masking_fn(
            decoder_input_tokens, mask_random_key
        )

        # Mask for tokens that were masked/randomly assigned by bert style masking
        denoising_sequence_mask = bert_targets != self._tokenizer.pad_token_id

        # Generate Attention Masks

        decoder_attention = build_causal_attention_mask(batch_size, decoder_seq_len)

        # autoregression with greedy selection
        def greedy_decode(seq_position: int, inputs: jnp.array) -> jnp.array:
            decoded_tokens = inputs

            # calculate logits
            decoder_outputs = self._forward_fn.apply(
                params,
                random_key,
                decoded_tokens,
                decoder_attention_mask=decoder_attention,
            )

            logits = decoder_outputs["logits"]

            # take only the predictions from seq_position
            preds = jnp.argmax(logits, axis=-1)[..., seq_position]

            # set preds as input for next iteration
            decoded_tokens = decoded_tokens.at[:, seq_position].set(preds)

            return decoded_tokens

        # create an input of <bos> token ids as first position,<pad> tokens elsewhere
        init_inputs = jnp.full_like(decoder_input_tokens, self._tokenizer.bos_token_id)
        init_inputs = init_inputs.at[:, 1:].set(self._tokenizer.pad_token_id)

        # autoregressive prediction for num_decoding_steps
        decoded_preds = jax.lax.fori_loop(
            lower=0,
            upper=self._num_decoding_steps,
            body_fun=greedy_decode,
            init_val=init_inputs,
        )

        loss, metrics = self._loss_fn(
            params,
            random_key,
            decoder_tokens=decoded_preds,
            targets=labels,
            denoising_sequence_mask=denoising_sequence_mask,
            decoder_sequence_mask=decoder_sequence_mask,
            decoder_attention_mask=decoder_attention,
        )
        metrics["loss"] = loss

        # gather  metrics across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)

        return metrics  # type:ignore
