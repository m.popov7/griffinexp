from __future__ import annotations

import functools
import os
from typing import Callable, NamedTuple

import haiku as hk
import jax
import jax.numpy as jnp
import optax
import tqdm

from trix.dataloaders.language_models.sequence_datasets import (
    PairedSequenceDataset,
    SequencesDataset,
)
from trix.tokenizers.language_models.base import BaseTokenizer
from trix.training.base import Metrics, aggregate_metrics
from trix.training.utils import get_causal_labels, mask_sequence_bert_style
from trix.types import AttentionMask, RNGKey, SequenceMask, Tokens
from trix.utils.masking import build_causal_attention_mask, build_padding_attention_mask
from trix.utils.parameters import load_params, save_params


class ShardedTrainingState(NamedTuple):
    """
    Contains a full training state.

    step: Epoch.
    params: Network parameters as PyTree.
    optimizer_state: Optimizer state as Pytree.
    random_key: Jax random PRNG Key.
    """

    step: jnp.ndarray
    params: hk.Params
    optimizer_state: optax.OptState
    random_key: jnp.ndarray

    def save(
        self,
        save_dir: str,
    ) -> None:
        """
        Save state using joblilb. Filename should end with .joblib.
        It returns the tree structure of the state, which is used to
        load the state.

        Args:
            save_dir: Directory where the training state is to be saved.

        """
        os.makedirs(save_dir, exist_ok=True)
        save_params(
            self.params,
            os.path.join(save_dir, "params.joblib"),
        )
        save_params(
            self.optimizer_state,
            os.path.join(save_dir, "opt_state.joblib"),
        )

        jnp.save(os.path.join(save_dir, "step.npy"), self.step[:1])
        jnp.save(os.path.join(save_dir, "key.npy"), self.random_key[:1])

    @property
    def tree_def(self) -> jax.tree_util.PyTreeDef:
        return jax.tree_util.tree_structure(self)

    @classmethod
    def load(
        cls,
        save_dir: str,
        num_shards_per_host: int,
    ) -> ShardedTrainingState:
        """
        Load the training state from a given directory. Assumes that the parameters are
        already sharded.

        Args:
            save_dir: Directory where the training state is saved.
            num_shards_per_host: The number of device per host.

        Returns:
            Loaded training state.
        """

        params = load_params(os.path.join(save_dir, "params.joblib"))
        optimizer_state = load_params(
            os.path.join(save_dir, "opt_state.joblib"),
        )
        step = jnp.load(os.path.join(save_dir, "step.npy"))
        key = jnp.load(os.path.join(save_dir, "key.npy"))

        return ShardedTrainingState(  # type: ignore
            step=step.repeat(num_shards_per_host),
            params=params,
            optimizer_state=optimizer_state,
            random_key=key.repeat(num_shards_per_host, axis=0),
        )


class ShardedEncoderDecoderCausalTrainer:
    """
    A stateless abstraction around an init_fn/update_fn/compute_metrics_fn set of funcs.
    This extracts some common boilerplate from the training loop. This trainer trains an
    Encoder - Decoder model through Alexa TM's causal and denoising objective where
    sentences are split in two for encoder and decoder tokens and teacher forcing
    is used to train the decoder. Additional noise is added to decoder tokens.

    Example:
        Input token: "How are you today ? I am fine, thank you"
        Encoder token: "How are you today ?"
        Input Decoder token: "<bos> I am fine, <mask> you"
        Output Decoder token: "I am fine, thank you"
    """

    def __init__(
        self,
        forward_fn: hk.Transformed,
        tokenizer: BaseTokenizer,
        loss_fn: Callable[
            [
                hk.Params,
                RNGKey,
                Tokens,
                Tokens,
                Tokens,
                Callable[
                    [
                        hk.Params,
                        RNGKey,
                        Tokens,
                        Tokens,
                        AttentionMask | None,
                        AttentionMask | None,
                        AttentionMask | None,
                    ],
                    tuple[dict[str, jnp.ndarray], dict[str, jnp.ndarray]],
                ],
                SequenceMask,
                SequenceMask | None,
                SequenceMask | None,
                SequenceMask | None,
                AttentionMask | None,
            ],
            tuple[jnp.ndarray, Metrics],
        ],
        optimizer: optax.GradientTransformation,
        num_decoding_steps: int,
        noising_ratio: float = 0.15,
        masking_prob: float = 0.8,
        random_token_prob: float = 0.1,
    ):
        """
        Args:
            forward_fn: The transformed forward function of the model.
            tokenizer: Tokenizer.
            loss_fn: The loss function to optimize.
            optimizer: Optimizer.
            num_decoding_steps: Number of decoding steps to take during inference.
            noising_ratio: Masking ratio.
            masking_prob: Masking probability.
            random_token_prob: Random token probability.

        """
        self._forward_fn = forward_fn
        self._loss_fn = functools.partial(
            loss_fn,
            forward_fn=self._forward_fn.apply,
        )

        self._optimizer = optimizer
        self._tokenizer = tokenizer

        self._num_decoding_steps = num_decoding_steps

        # T5 specific attributes
        random_token_indices = jnp.asarray(
            [tokenizer.token_to_id(tok) for tok in tokenizer.standard_tokens]
        )
        self._bert_masking_fn = functools.partial(
            mask_sequence_bert_style,
            random_token_indices=random_token_indices,
            mask_id=tokenizer.mask_token_id,
            pad_id=tokenizer.pad_token_id,
            noising_ratio=noising_ratio,
            masking_prob=masking_prob,
            random_token_prob=random_token_prob,
        )

    def build_init_fn(self) -> Callable:
        xmapped_init_fn: Callable = jax.experimental.maps.xmap(
            fun=self._init,
            in_axes=(
                ["shard", ...],
                [
                    ...,
                ],
                [
                    ...,
                ],
            ),
            out_axes=["shard", ...],
            axis_resources={"shard": "shard"},
            donate_argnums=(0,),
        )
        return xmapped_init_fn

    def build_xmapped_update_fn(self) -> Callable:
        """
        Xmaps the update function to be used in a run_train_epoch

        Returns:
            The jitted function with xmap
        """

        xmapped_update_fn: Callable = jax.experimental.maps.xmap(
            fun=self._update,
            in_axes=(["shard", ...], ["batch", ...], ["batch", ...]),
            out_axes=(
                ["shard", ...],
                [
                    ...,
                ],
            ),
            axis_resources={"shard": "shard", "batch": "batch"},
            donate_argnums=(0,),
        )
        return xmapped_update_fn

    def build_xmapped_metrics_fn(self) -> Callable:
        """
        Xmaps the update function to be used in a run_train_epoch

        Returns:
            The jitted function with xmap
        """

        xmapped_metrics_fn: Callable = jax.experimental.maps.xmap(
            fun=self._compute_metrics,
            in_axes=(["shard", ...], ["batch", ...], ["batch", ...]),
            out_axes=[
                ...,
            ],
            axis_resources={"shard": "shard", "batch": "batch"},
        )
        return xmapped_metrics_fn

    def _init(
        self, random_key: RNGKey, encoder_tokens: Tokens, decoder_tokens: Tokens
    ) -> ShardedTrainingState:
        """
        Initializes the ShardedTrainingState. Initialization is done on accelerators but
        in the future it might be preferable to do it on CPU when doable.

        Args:
            random_keys: Random JAX keys of shape (num_shards, 2).
            encoder_tokens: Tokens of shape (batch_size, encoder_seq_length, *).
            decoder_tokens: Tokens of shape (batch_size, decoder_seq_length, *).

        Returns:
            Initialized training state.
        """

        params = self._forward_fn.init(random_key, encoder_tokens, decoder_tokens)

        optimizer_state = self._optimizer.init(params)

        return ShardedTrainingState(  # type: ignore
            step=jnp.array(0),
            params=params,
            optimizer_state=optimizer_state,
            random_key=random_key,
        )

    @functools.partial(jax.jit, static_argnums=0)
    def _update(
        self,
        state: ShardedTrainingState,
        encoder_tokens: Tokens,
        decoder_tokens: Tokens,
    ) -> tuple[ShardedTrainingState, Metrics]:
        """
        Updates the training state. This function is supposed to be called inside
        a x-mapped operator with `axis_name="shard"' for the state and 'axis_name=
        "batch"' for the tokens (use the build_xmapped_update_fn for standard use).

        Args:
            state: Current training state.
            encoder_tokens: Tokens of shape
            (num_acc_grads,batch_size, encoder_seq_length, *).
            decoder_tokens: Tokens of shape
            (num_acc_grads,batch_size, decoder_seq_length, *).

        Returns:
            Updated training state.
            Metrics.
        """

        num_acc_grads, batch_size, decoder_seq_len = decoder_tokens.shape[0:3]
        params = state.params

        random_key = state.random_key

        # Make sure the key is the same on each shard for the masking function
        random_key = jax.lax.all_gather(random_key, axis_name="shard")[0]

        random_key, sub_key = jax.random.split(random_key)

        *mask_random_key, random_key, sub_key = jax.random.split(
            random_key, num=num_acc_grads + 2
        )

        # Mask for all non-padded tokens
        decoder_sequence_mask = decoder_tokens != self._tokenizer.pad_token_id

        # Get CLM labels from decoder_tokens before they are bert style masked
        labels = get_causal_labels(
            decoder_tokens,
            self._tokenizer.eos_token_id,
            self._tokenizer.pad_token_id,
        )

        # Bert style masking of decoder tokens
        decoder_tokens, bert_targets = jax.vmap(self._bert_masking_fn)(
            decoder_tokens, jnp.stack(mask_random_key, axis=0)
        )

        # Mask for tokens that were masked/randomly assigned by bert style masking
        denoising_sequence_mask = bert_targets != self._tokenizer.pad_token_id

        # Generate Attention Masks
        encoder_attention_mask = build_padding_attention_mask(
            encoder_tokens[0, :, :], self._tokenizer.pad_token_id
        )

        decoder_attention_mask = build_causal_attention_mask(
            batch_size, decoder_seq_len
        )

        # accumulate gradients if needed
        def loop_fun(
            i: int, val: tuple[Metrics, hk.Params]
        ) -> tuple[Metrics, hk.Params]:
            metrics, gradient = val

            # Generate Attention Masks
            encoder_attention_mask = build_padding_attention_mask(
                encoder_tokens[i + 1], self._tokenizer.pad_token_id
            )

            (add_loss, add_metrics), add_gradient = jax.value_and_grad(
                self._loss_fn, has_aux=True
            )(
                params,
                random_key,
                encoder_tokens[i + 1],
                decoder_tokens[i + 1],
                targets=labels[i + 1],
                denoising_sequence_mask=denoising_sequence_mask[i + 1],
                decoder_sequence_mask=decoder_sequence_mask[i + 1],
                encoder_attention_mask=encoder_attention_mask,
                decoder_attention_mask=decoder_attention_mask,
            )
            add_metrics["loss"] = add_loss

            metrics = jax.tree_map(lambda x, y: x + y, metrics, add_metrics)
            gradient = jax.tree_map(lambda x, y: x + y, gradient, add_gradient)

            return metrics, gradient

        # evaluate gradients
        (loss, metrics), gradient = jax.value_and_grad(self._loss_fn, has_aux=True)(
            params,
            random_key,
            encoder_tokens[0],
            decoder_tokens[0],
            targets=labels[0],
            denoising_sequence_mask=denoising_sequence_mask[0],
            decoder_sequence_mask=decoder_sequence_mask[0],
            encoder_attention_mask=encoder_attention_mask,
            decoder_attention_mask=decoder_attention_mask,
        )
        metrics["loss"] = loss

        # accumulate gradients if necessary
        metrics, gradient = jax.lax.fori_loop(
            lower=0,
            upper=num_acc_grads - 1,
            body_fun=loop_fun,
            init_val=(metrics, gradient),
        )

        metrics = jax.tree_map(lambda x: x / num_acc_grads, metrics)
        gradient = jax.tree_map(lambda g: g / num_acc_grads, gradient)

        # gather losses and gradients across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="shard"), metrics)

        gradient = jax.tree_map(lambda g: jax.lax.pmean(g, axis_name="batch"), gradient)

        # update optimizer and weights
        optimizer_state = state.optimizer_state
        updates, optimizer_state = self._optimizer.update(gradient, optimizer_state)
        params = optax.apply_updates(params, updates)

        new_state = ShardedTrainingState(
            step=state.step + 1,
            random_key=sub_key,
            optimizer_state=optimizer_state,
            params=params,
        )

        return new_state, metrics

    @functools.partial(jax.jit, static_argnums=0)
    def _compute_metrics(
        self,
        state: ShardedTrainingState,
        encoder_tokens: Tokens,
        decoder_tokens: Tokens,
    ) -> Metrics:
        """
        This function is supposed to be called inside
        a x-mapped operator with `axis_name="shard"' for the state and 'axis_name=
        "batch"' for the tokens (use the build_xmapped_update_fn for standard use).

        Args:
            state: Training state.
            encoder_tokens: Tokens (batch_size, encoder_seq_length, *).
            decoder_tokens: Tokens (batch_size, decoder_seq_length, *).

        Returns:
            Training Metrics.

        """

        batch_size, decoder_seq_len = decoder_tokens.shape[0:2]
        params = state.params

        random_key = state.random_key
        mask_random_key, random_key, sub_key = jax.random.split(random_key, num=3)

        # Mask for all non-padded tokens
        decoder_sequence_mask = decoder_tokens != self._tokenizer.pad_token_id

        # Get CLM labels from decoder_tokens before they are masked bert style
        labels = get_causal_labels(
            decoder_tokens,
            self._tokenizer.eos_token_id,
            self._tokenizer.pad_token_id,
        )

        # Bert style masking of decoder tokens
        decoder_tokens, bert_targets = self._bert_masking_fn(
            decoder_tokens, mask_random_key
        )

        # Mask for tokens that were masked/randomly assigned by bert style masking
        denoising_sequence_mask = bert_targets != self._tokenizer.pad_token_id

        # Generate Attention Masks
        encoder_attention_mask = build_padding_attention_mask(
            encoder_tokens, self._tokenizer.pad_token_id
        )

        decoder_attention_mask = build_causal_attention_mask(
            batch_size, decoder_seq_len
        )

        # autoregression with greedy selection
        def greedy_decode(seq_position: int, inputs: jnp.array) -> jnp.array:
            decoded_tokens = inputs

            # calculate logits
            _, decoder_outputs = self._forward_fn.apply(
                params,
                random_key,
                encoder_tokens,
                decoded_tokens,
                encoder_attention_mask=encoder_attention_mask,
                decoder_attention_mask=decoder_attention_mask,
            )

            logits = decoder_outputs["logits"]

            # take only the predictions from seq_position
            preds = jnp.argmax(logits, axis=-1)[..., seq_position]

            # set preds as input for next iteration
            decoded_tokens = decoded_tokens.at[:, seq_position].set(preds)

            return decoded_tokens

        # create an input of <bos> token ids as first position,<pad> tokens elsewhere
        init_inputs = jnp.full_like(decoder_tokens, self._tokenizer.bos_token_id)
        init_inputs = init_inputs.at[:, 1:].set(self._tokenizer.pad_token_id)

        # autoregressive prediction for num_decoding_steps
        decoded_preds = jax.lax.fori_loop(
            lower=0,
            upper=self._num_decoding_steps,
            body_fun=greedy_decode,
            init_val=init_inputs,
        )

        loss, metrics = self._loss_fn(
            params,
            random_key,
            encoder_tokens,
            decoded_preds,
            targets=labels,
            denoising_sequence_mask=denoising_sequence_mask,
            decoder_sequence_mask=decoder_sequence_mask,
            encoder_attention_mask=encoder_attention_mask,
            decoder_attention_mask=decoder_attention_mask,
        )
        metrics["loss"] = loss

        # gather losses and gradients across devices
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="batch"), metrics)
        metrics = jax.tree_map(lambda x: jax.lax.pmean(x, axis_name="shard"), metrics)

        return metrics  # type: ignore


def run_sharded_train_epoch(
    update_fn: Callable[
        [ShardedTrainingState, Tokens], tuple[ShardedTrainingState, Metrics]
    ],
    dataset: SequencesDataset,
    num_acc_grads: int,
    batch_size: int,
    num_data_parallel: int,
    tokens_length: int,
    training_state: ShardedTrainingState,
    epoch_num: int = 0,
) -> tuple[ShardedTrainingState, Metrics]:
    """
    Performs one training epoch over given dataset. It assumes that the training
    state is already sharded on the specified devices.

    Args:
        update_fn: Functions which does a forward/backward pass and updates params: in
            this script we assumed it is x-mapped over 'batch' and 'shard'.
        dataset: Training dataset.
        num_acc_grads: Number of gradients to accumulate.
        batch_size: Batch size per device per pass.
        num_data_parallel: Number of data-parallel ways for one host.
        tokens_length: Length of each sequence.
        training_state: Current training state (already sharded).
        epoch_num: Number of the epoch (for printing purpose only).

    Returns:
        Final training state and the aggregated training metrics over the epoch
    """

    # Training loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Training"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []

    for tokens in generator:

        # Reshape data to send to devices, and gradient accumulation
        tokens = jnp.reshape(
            tokens,
            (
                num_data_parallel,
                num_acc_grads,
                batch_size,
                tokens_length,
            ),
        )

        for i in range(num_acc_grads):
            # Update neural network and optimizer
            training_state, training_metrics = update_fn(
                training_state, tokens[:, i, :, :]
            )
            epoch_metrics.append(training_metrics)
            training_state.step.block_until_ready()
    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return training_state, aggregated_metrics


def run_sharded_test_epoch(
    compute_metrics_fn: Callable[[ShardedTrainingState, Tokens], Metrics],
    dataset: SequencesDataset,
    batch_size: int,
    num_data_parallel: int,
    tokens_length: int,
    training_state: ShardedTrainingState,
    epoch_num: int = 0,
) -> Metrics:
    """
    Performs one epoch over validation dataset and returns testing metrics. It assumes
    that the training state is already sharded on the specified devices.

    WARNING: unlike `run_train_epoch` this function does not need gradient accumulation,
    so it expects `batch_size` to be the effective batch size passed to the model.

    Args:
        compute_metrics_fn: Function to carry out forward pass and compute metrics.
        dataset: Dataset to validate on.
        num_data_parallel: Number of data-parallel ways for one host.
        batch_size: Effective batch size per device.
        training_state: Current training state (already sharded).
        epoch_num: Current epoch (for printing purpose only).

    Returns:
        Aggregated validation metrics over the epoch.
    """

    # Testing loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Test"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []
    for tokens in generator:

        # Reshape data to send to devices
        tokens = tokens.reshape((num_data_parallel, batch_size, tokens_length))

        # Make predictions
        metrics = compute_metrics_fn(training_state, tokens)

        epoch_metrics.append(metrics)

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return aggregated_metrics  # type: ignore


def run_sharded_train_epoch_enc_dec(
    update_fn: Callable[
        [ShardedTrainingState, Tokens, Tokens], tuple[ShardedTrainingState, Metrics]
    ],
    dataset: PairedSequenceDataset,
    num_acc_grads: int,
    num_data_parallel: int,
    batch_size: int,
    training_state: ShardedTrainingState,
    epoch_num: int = 0,
) -> tuple[ShardedTrainingState, Metrics]:
    """
    Performs one training epoch over given dataset. It assumes that the training
    state is already sharded on the specified devices. Used specifically for encoder
    decoder models where the dataset yields both encoder and decoder tokens.

    Args:
        update_fn: Functions which does a forward/backward pass and updates params: in
            this script we assumed it is x-mapped over 'batch' and 'shard'.
        dataset: Training dataset.
        num_acc_grads: Number of gradients to accumulate.
        num_data_parallel: Number of data-parallel ways for one host.
        batch_size: Batch size per device per pass.
        training_state: Current training state (already sharded).
        epoch_num: Number of the epoch (for printing purpose only).

    Returns:
        Final training state and the aggregated training metrics over the epoch.
    """

    # Training loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Training"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []

    for encoder_tokens, decoder_tokens in generator:

        # Reshape data to send to devices, and gradient accumulation
        encoder_tokens = encoder_tokens.reshape(
            (num_data_parallel, num_acc_grads, batch_size) + encoder_tokens.shape[1:]
        )

        decoder_tokens = decoder_tokens.reshape(
            (num_data_parallel, num_acc_grads, batch_size) + decoder_tokens.shape[1:]
        )

        # Update neural network and optimizer
        training_state, training_metrics = update_fn(
            training_state, encoder_tokens, decoder_tokens
        )
        epoch_metrics.append(training_metrics)
        training_state.step.block_until_ready()

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return training_state, aggregated_metrics


def run_sharded_test_epoch_enc_dec(
    compute_metrics_fn: Callable[[ShardedTrainingState, Tokens, Tokens], Metrics],
    dataset: PairedSequenceDataset,
    batch_size: int,
    num_data_parallel: int,
    training_state: ShardedTrainingState,
    epoch_num: int = 0,
) -> Metrics:
    """
    Performs one epoch over validation dataset and returns testing metrics. It assumes
    that the training state is already sharded on the specified devices.

    WARNING: unlike `run_train_epoch` this function does not need gradient accumulation,
    so it expects `batch_size` to be the effective batch size passed to the model.

    Args:
        compute_metrics_fn: Function to carry out forward pass and compute metrics.
        dataset: Dataset to validate on.
        batch_size: Effective batch size per device.
        num_data_parallel: Number of data-parallel ways for one host.
        training_state: Current training state (already sharded).
        epoch_num: Current epoch (for printing purpose only).

    Returns:
        Aggregated validation metrics over the epoch.
    """

    # Testing loop
    total = dataset.num_batches_per_epoch
    desc = f"Epoch {epoch_num} - Test"
    generator = tqdm.tqdm(dataset.get_epoch_batches(), total=total, desc=desc)

    epoch_metrics = []
    for encoder_tokens, decoder_tokens in generator:

        # Reshape data to send to devices
        encoder_tokens = encoder_tokens.reshape(
            (num_data_parallel, batch_size) + encoder_tokens.shape[1:]
        )

        decoder_tokens = decoder_tokens.reshape(
            (num_data_parallel, batch_size) + decoder_tokens.shape[1:]
        )

        # Make predictions
        metrics = compute_metrics_fn(training_state, encoder_tokens, decoder_tokens)

        epoch_metrics.append(metrics)

    # Average metrics over epoch
    aggregated_metrics = aggregate_metrics(epoch_metrics)

    return aggregated_metrics  # type: ignore
