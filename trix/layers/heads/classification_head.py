from typing import Dict, Optional

import haiku as hk
import jax.numpy as jnp
from haiku import initializers

from trix.types import SequenceMask


class SimpleClassificationHead(hk.Module):
    """
    Basic classification head. Transforms final attention block output
    into a distribution over the number of classes through a dense net that takes
    the first embedding as input.
    """

    def __init__(self, num_classes: int, name: Optional[str] = None):
        """
        Args:
            num_classes: Number of classes.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)

        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        self._fc = hk.Linear(num_classes, w_init=w_init, b_init=b_init, name="fc")

    def __call__(
        self, x: jnp.ndarray, sequence_mask: SequenceMask
    ) -> Dict[str, jnp.ndarray]:
        sequence_mask = jnp.expand_dims(sequence_mask, axis=-1)
        mean_embeddings = jnp.sum(x * sequence_mask, axis=1) / jnp.sum(
            sequence_mask, axis=1
        )
        logits = self._fc(mean_embeddings)
        return {"logits": logits}


class PerTokenClassificationHead(hk.Module):
    """
    Returns logits over classes per token in the sequence.
    """

    def __init__(
        self,
        num_classes: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            num_classes: Number of classes.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)
        self._num_classes = num_classes

        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        self._fc = hk.Linear(self._num_classes, w_init=w_init, b_init=b_init, name="fc")

    def __call__(
        self, x: jnp.ndarray, sequence_mask: SequenceMask
    ) -> Dict[str, jnp.ndarray]:
        """
        Input shape: (batch_size, sequence_length, embed_dim)
        Output_shape: (batch_size, sequence_length, num_classes)
        """
        batch_size, seq_len = x.shape[0], x.shape[1]
        logits = self._fc(x)
        logits = jnp.reshape(logits, (batch_size, seq_len, self._num_classes))
        return {"logits": logits}


class SimpleMultiClassificationHead(hk.Module):
    """
    Basic classification head. Transforms final attention block output
    into a distribution over the number of classes and number of labels through a dense
    net that takes the first embedding as input. This head is designed for a task
    where all the labels have the same number of classes. This allows to predict
    over a single vector of shape (num_labels x num_classes) that we can reshape into
    an array of shape (num_labels, num_classes). This mainly allow not build a for loop
    with one classification head per task (which is extremely slow to compile). In case
    the task should present different number of classes, this should be modified to
    build multiple output heads in a way that does not slow down the compilation.
    """

    def __init__(
        self, num_classes: int, num_labels: int = 1, name: Optional[str] = None
    ):
        """
        Args:
            num_classes: Number of classes.
            num_labels: Number of labelsum_classes: Number of classes..
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)

        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        self._fc = hk.Linear(
            num_classes * num_labels, w_init=w_init, b_init=b_init, name="fc"
        )
        self.num_classes = num_classes
        self.num_labels = num_labels

    def __call__(
        self, x: jnp.ndarray, sequence_mask: SequenceMask
    ) -> Dict[str, jnp.ndarray]:
        sequence_mask = jnp.expand_dims(sequence_mask, axis=-1)
        mean_embeddings = jnp.sum(x * sequence_mask, axis=1) / jnp.sum(
            sequence_mask, axis=1
        )

        logits = self._fc(mean_embeddings)

        batch_size = x.shape[0]
        # Reshaping so that we get the output (num_labels, num_classes)
        logits = jnp.reshape(
            logits, newshape=(batch_size, self.num_labels, self.num_classes)
        )

        return {"logits": logits}
