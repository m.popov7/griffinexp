from typing import Optional

import haiku as hk
import jax.numpy as jnp


class LearnedPositionalEmbeddings(hk.Module):
    """Position embeddings to be added to token embeddings."""

    def __init__(
        self,
        max_positions: int,
        embed_dim: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            max_positions: Max number of positions in sequence.
            embed_dim: Embedding size.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)
        self._max_positions = max_positions
        self._embed_layer = hk.Embed(max_positions, embed_dim)

    def __call__(
        self, tokens: jnp.ndarray, positions: Optional[jnp.ndarray] = None
    ) -> jnp.ndarray:
        """
        Creates the positional embeddings.

        Args:
            tokens: Input tokens, shape (batch_size, seq_len).

        Returns:
            The positional embeddings.
        """

        batch_size, seq_len = tokens.shape[0], tokens.shape[1]

        if positions is None:
            positions = jnp.tile(jnp.arange(seq_len), (batch_size, 1))

        return self._embed_layer(positions)
