import math

import jax.numpy as jnp


def build_alibi_tensor_dnabert2(sequence_len: int, num_heads: int) -> jnp.ndarray:
    """
    Compute the attention bias tensor for ALIBI-based attention
    ref: https://arxiv.org/pdf/2108.12409.pdf
    BUT, it does in the dnabert2 fashion, which not exactly the way explained in the
    Alibi paper

    Args:
        sequence_len: the sequence_len of the sequence
        num_heads: the number of heads

    Returns:
        The alibi attention bias tensor
    """

    context_position = jnp.arange(
        sequence_len,
    )[:, jnp.newaxis]
    memory_position = jnp.arange(
        sequence_len,
    )[jnp.newaxis, :]
    relative_position = jnp.abs(memory_position - context_position)
    relative_position = jnp.tile(
        jnp.expand_dims(relative_position, 0), (num_heads, 1, 1)
    )
    abstract_num_heads = int(2 ** math.floor(math.log2(num_heads)))
    all_slopes = jnp.power(
        2,
        -(jnp.arange(abstract_num_heads * 2) + 1) * 8 / (abstract_num_heads * 2),
    )
    slopes_a = all_slopes[1::2]
    slopes_b = all_slopes[0::2][: num_heads - abstract_num_heads]

    slopes = jnp.concatenate((slopes_a, slopes_b), axis=0)

    alibi_attention_bias = slopes[:, jnp.newaxis, jnp.newaxis] * -relative_position
    alibi_attention_bias = jnp.expand_dims(alibi_attention_bias, 0)

    return alibi_attention_bias
