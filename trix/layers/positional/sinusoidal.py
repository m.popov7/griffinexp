from math import ceil
from typing import Optional

import haiku as hk
import jax.numpy as jnp

from trix.types import Tokens

# Constant used in Sinusoidal/Rotary Embeddings, reference to this value can be found
# on page 6 of https://arxiv.org/pdf/1706.03762.pdf and page 5 of
# https://arxiv.org/abs/2104.09864
UPPER_FREQ = 10000


class SinusoidalPositionalEmbedding(hk.Module):

    """
    Sinusoidal embeddings to be added to token embeddings. Implementation follows
    https://arxiv.org/pdf/1706.03762.pdf .
    """

    def __init__(
        self,
        embed_dim: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            embed_dim: Embedding size.
            name: Name of the layer. Defaults to None.
        """

        super().__init__(name=name)
        self.embed_dim = embed_dim

    def __call__(self, tokens: Tokens) -> jnp.ndarray:
        """
        Creates the sinusoidal positional embeddings.

        Args:
            tokens: Input tokens, shape (batch_size, seq_len).

        Returns:
            The sinusoidal positional embeddings.
        """

        bsz, seq_len = tokens.shape
        max_pos = 1 + seq_len
        weights = self._get_embedding(max_pos)
        positions = self._make_positions(tokens)

        return weights[positions.reshape(-1), :].reshape(bsz, seq_len, -1)

    @hk.transparent
    def _make_positions(self, x: jnp.ndarray) -> jnp.ndarray:
        """
        Computes an array of token positions.

        Args:
            x: Input Tokens of shape (batch,seq_len).

        Returns:
            An array of positions corresponding to input tokens of the same shape.
        """
        range_buf = jnp.broadcast_to(jnp.arange(x.shape[1]), x.shape)
        positions = jnp.broadcast_to(range_buf, x.shape)
        return positions

    @hk.transparent
    def _get_embedding(self, num_embeddings: int) -> jnp.ndarray:
        """
        Computes the sinusoidal positional embeddings.

        Args:
            num_embeddings: Number of positions to embed.

        Returns:
            An array of alternating sin and cos values from frequencies based on
            position and embedding dimension values.
        """
        half_dim = ceil(self.embed_dim / 2)

        sin_mask = ~jnp.arange(half_dim * 2) % 2
        cos_mask = 1 - sin_mask

        emb = jnp.log(UPPER_FREQ)
        log_positions = jnp.expand_dims(
            jnp.log(jnp.arange(num_embeddings, dtype=jnp.float32)), axis=1
        )

        emb = jnp.exp(
            log_positions
            - 2 * jnp.arange(half_dim, dtype=jnp.float32) / self.embed_dim * emb
        )

        emb = (
            jnp.repeat(jnp.sin(emb), 2, axis=-1) * sin_mask
            + jnp.repeat(jnp.cos(emb), 2, axis=-1) * cos_mask
        )

        if self.embed_dim % 2 == 1:
            emb = emb[:, :-1]

        return emb
