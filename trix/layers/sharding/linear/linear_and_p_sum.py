from typing import Any, Optional, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
from haiku import initializers


@jax.custom_vjp
def f_psum(x: jnp.ndarray) -> jnp.ndarray:
    """
    Performs the identity during the forward pass but a jax.lax.psum over the axis name
    "shard" during the backward pass.
    """
    return jax.lax.psum(x, "shard")


def f_psum_fwd(x: jnp.ndarray) -> Tuple[jnp.ndarray, Any]:
    return f_psum(x), None


def f_psum_bwd(_: Any, g: jnp.ndarray) -> Tuple[jnp.ndarray]:
    return (jax.lax.psum(g, "shard"),)


f_psum.defvjp(f_psum_fwd, f_psum_bwd)


class LinearAndPsum(hk.Module):
    """
    Linear layer with he initialization (asynchronous) followed by a psum.
    """

    def __init__(
        self,
        output_size: int,
        init_rescale: float = 1.0,
        with_bias: bool = True,
        name: Optional[str] = None,
    ):
        """
        Args:
            output_size: Size of the layer output.
            init_rescale: Scalar parameter to scale the init scheme of the linear layer.
            with_bias: whether or not to add a bias in the linear layer
            name: Layer's name.
        """
        super().__init__(name=name)
        w_init = initializers.VarianceScaling(init_rescale * 2.0, "fan_in", "uniform")
        self.with_bias = with_bias
        if self.with_bias:
            self.b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")

        # Define layer
        self._fc = hk.Linear(
            output_size=output_size, w_init=w_init, with_bias=False, name="linear"
        )
        self.output_size = output_size

    def __call__(self, x: jnp.ndarray) -> jnp.ndarray:
        y = f_psum(self._fc(x))

        if self.with_bias:
            b = hk.get_parameter("bias", (self.output_size,), y.dtype, init=self.b_init)
            b = jax.lax.all_gather(b, "shard")[0]
            y = y + b

        return y
