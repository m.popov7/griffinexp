import haiku as hk
from haiku import initializers

from trix.layers.sharding.synchronous_init import SynchronousInitializer


class LinearLayerSynchronousInit(hk.Linear):
    """
    Dense layer with a synchronous HE initialization between different shards.
    """

    def __init__(self, output_size: int, name: str, add_bias_lm_head: bool = True):
        """
        Args:
            output_size: Size of the layer output.@
            name: Layer's name.
            add_bias_lm_head: Add bias to linear layer
        """
        super().__init__(
            output_size=output_size,
            w_init=SynchronousInitializer(
                initializers.VarianceScaling,
                scale=2.0,
                mode="fan_in",
                distribution="uniform",
            ),
            b_init=SynchronousInitializer(
                initializers.VarianceScaling,
                scale=2.0,
                mode="fan_in",
                distribution="uniform",
            ),
            with_bias=add_bias_lm_head,
            name=name,
        )
