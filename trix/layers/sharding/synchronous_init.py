import jax
import jax.numpy as jnp
from haiku import initializers


class SynchronousInitializer(initializers.Initializer):
    """
    An initializer to synchronize the weights at initialization.
    """

    def __init__(  # type: ignore
        self, initializer: initializers.Initializer, *args, **kwargs
    ):
        """
        Initializes an initializer from haiku.initializers.

        Args:
            initializer: a class of haiku.initializers
        """
        self._initializer = initializer(*args, **kwargs)

    def __call__(self, *args, **kwargs) -> jnp.ndarray:  # type: ignore
        weights: jnp.ndarray = self._initializer.__call__(*args, **kwargs)

        weights = jax.lax.all_gather(weights, axis_name="shard")[0]
        return weights
