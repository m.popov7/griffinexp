from typing import Optional

import haiku as hk
import jax.numpy as jnp

from trix.layers.sharding.embeddings.embed import ReplicatedEmbed


class ReplicatedLearnedPositionalEmbeddings(hk.Module):
    """
    Position embeddings to be added to token embeddings to be used when sharding the
    model.
    """

    def __init__(
        self,
        max_positions: int,
        embed_dim: int,
        name: Optional[str] = None,
    ):
        """
        Args:
            max_positions: Tokenizer vocabulary size.
            embed_dim: Embedding size.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(name=name)
        self._max_positions = max_positions
        self._embed_layer = ReplicatedEmbed(max_positions, embed_dim)

    def __call__(self, tokens: jnp.ndarray) -> jnp.ndarray:
        batch_size, seq_len = tokens.shape[0], tokens.shape[1]
        positions = jnp.tile(jnp.arange(seq_len), (batch_size, 1))
        return self._embed_layer(positions)
