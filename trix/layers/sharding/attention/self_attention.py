from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp

from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.layers.sharding.attention.multi_head_attention import (
    ShardedMultiHeadAttention,
)
from trix.layers.sharding.linear.linear_and_p_sum import LinearAndPsum
from trix.layers.sharding.regularization.norms import ReplicatedLayerNorm
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


class ShardedSelfAttentionBlock(hk.Module):
    """Sharded implementation of the SelfAttentionBlock"""

    def __init__(
        self,
        num_heads_per_shard: int,
        embed_dim: int,
        ffn_embed_dim: int,
        num_shards: int,
        key_size: Optional[int] = None,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        add_bias_kv: bool = False,
        name: Optional[str] = None,
    ):
        """
        Initializes the ShardedSelfAttentionBlock. Be aware that num_heads_per_shard is
        the number of heads of one shard only.

        Args:
            num_heads_per_shard: Number of heads per shard (total number of heads is
                num_heads * num_shards).
            embed_dim: Embedding dimension of the attention layer.
            ffn_embed_dim: Hidden dimension of the MLP head.
            num_shards: Number of shards for the model.
            key_size: The size of keys and queries used for attention. If no value is
                given, then it is set to embed_dim/num_heads.
            rotary_embedding_config: Configuration to specify hyperparameters for
                RotaryEmbeddig layer
                (see RoFormer https://arxiv.org/pdf/2104.09864.pdf). If None,
                rotary embeddings are not used. If specified, it contains the
                hyperparameters specifying the type of rotary embedding applied.
            name: Name of the model.
        """
        super().__init__(name=name)
        # Add checks on dimensions
        if key_size is None:
            if embed_dim % (num_heads_per_shard * num_shards) != 0:
                raise ValueError(
                    f"The embedding dimension should be divisible by the total number "
                    f"of heads,  however provided embed dim is {embed_dim} and the "
                    f"total number of heads is {num_heads_per_shard*num_shards}."
                )

            # Hyperparameters internalization
            key_size = embed_dim // num_heads_per_shard // num_shards

        # Define layers
        self.fc1 = hk.Linear(ffn_embed_dim // num_shards, name="fc1")
        self.fc2 = LinearAndPsum(embed_dim, name="fc2")

        self.layer_norm_self_attention = ReplicatedLayerNorm(
            create_offset=True,
            name="self_attention_layer_norm",
        )
        self.layer_norm_mlp = ReplicatedLayerNorm(
            create_offset=True, name="final_layer_norm"
        )
        self.self_attention_layer = ShardedMultiHeadAttention(
            num_heads_per_shard=num_heads_per_shard,
            key_size=key_size,
            model_size=embed_dim,
            num_shards=num_shards,
            rotary_embedding_config=rotary_embedding_config,
            add_bias_kv=add_bias_kv,
            name="self_attention",
        )

    @hk.transparent
    def self_attention(
        self,
        x: Embedding,
        attention_mask: Optional[AttentionMask] = None,
        attention_weight_bias: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        """
        Applies the self attention mechanism.

        Args:
            x: Input token embeddings of shape (batch_size, seq_len, embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            Dictionary containing the output embeddings and the attention weights.
        """

        return self.self_attention_layer(
            x,
            x,
            x,
            attention_mask=attention_mask,
            attention_weight_bias=attention_weight_bias,
        )

    @hk.transparent
    def mlp(self, x: Embedding) -> Embedding:
        """
        Applies one layer-norm, one linear layer, a Gelu activation,
        then a final linear layer

        Args:
            x: Embeddings of shape (batch_size, seq_len, key_size * num_heads).

        Returns:
            The transformed sequence embedding.
        """
        x = self.layer_norm_mlp(x)
        x = jax.nn.gelu(
            self.fc1(x),
            approximate=False,
        )
        x = self.fc2(x)
        return x

    def __call__(
        self,
        x: Tokens,
        attention_mask: Optional[AttentionMask] = None,
        attention_weight_bias: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        """
        Computes the output of the attention layer.

        Args:
            x: Input token embeddings of shape (batch_size,seq_len,embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1,seq_len, seq_len).

        Returns:
            A dictionary containing the output embeddings and the attention weights.
        """

        # Self-Attention
        res = x
        x = self.layer_norm_self_attention(x)
        output = self.self_attention(
            x=x,
            attention_mask=attention_mask,
            attention_weight_bias=attention_weight_bias,
        )
        x = output["embeddings"]
        x = res + x

        # MLP
        x = x + self.mlp(x)

        output["embeddings"] = x
        return output  # type: ignore
