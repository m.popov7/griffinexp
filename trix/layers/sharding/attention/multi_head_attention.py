from typing import Optional

import haiku as hk
import jax.numpy as jnp

from trix.layers.attention.multi_head_attention import MultiHeadAttention
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.layers.sharding.linear.linear_and_p_sum import LinearAndPsum
from trix.types import AttentionWeights


class ShardedMultiHeadAttention(MultiHeadAttention):
    """Sharded MHA."""

    def __init__(
        self,
        num_heads_per_shard: int,
        key_size: int,
        num_shards: int,
        init_rescale: float = 1.0,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        add_bias_kv: bool = False,
        value_size: Optional[int] = None,
        model_size: Optional[int] = None,
        name: Optional[str] = None,
    ):
        """
        This layer assumes that the number of heads is the number of heads on one shard,
        ie the total number of heads is equal to num_heads_per_shard*num_shards.

        Args:
            num_heads_per_shard: Number of heads per shard (total number of heads is
                num_heads * num_shards).
            key_size: The size of keys and queries used for attention.
            num_shards: Number of shards (to compute the output model size).
            init_rescale: Rescaling factor for the final linear layer.
            rotary_embedding_config: Configuration to specify hyperparameters for
                RotaryEmbeddig layer
                (see RoFormer https://arxiv.org/pdf/2104.09864.pdf). If None,
                rotary embeddings are not used. If specified, it contains the
                hyperparameters specifying the type of rotary embedding applied.
            value_size: Optional size of the value projection. If None, defaults
                to the key size.
            model_size: Optional size of the output embedding. If None, defaults
                to the key size multiplied by the number of heads and the number of
                shards.
            name: Optional name for this module.
        """
        super().__init__(
            num_heads=num_heads_per_shard,
            key_size=key_size,
            rotary_embedding_config=rotary_embedding_config,
            add_bias_kv=add_bias_kv,
            value_size=value_size,
            model_size=model_size,
            name=name,
        )
        if model_size is None:
            self.model_size = num_shards * num_heads_per_shard * key_size
        self._init_rescale = init_rescale

    @hk.transparent
    def compute_embeddings(
        self,
        value: jnp.ndarray,
        attention_weights: AttentionWeights,
    ) -> jnp.ndarray:
        """
        Computes the output embeddings.

        Args:
            value: Embedding sequence to compute values.
            attention_weights: Attention weights.

        Returns:
            Output embeddings.
        """

        value_heads = self._linear_projection_he_init(value, self.value_size, "value")
        if self._bias_v is not None:
            batch_size = value_heads.shape[0]
            attention_bias = jnp.tile(self._bias_v, (batch_size, 1, 1, 1))
            value_heads = jnp.concatenate((value_heads, attention_bias), axis=1)
        attention = jnp.einsum("...htT,...Thd->...thd", attention_weights, value_heads)

        # Concatenate attention matrix of all heads into a single vector.
        attention_vec = jnp.reshape(attention, (*attention.shape[:-2], -1))

        return LinearAndPsum(self.model_size, name="mha_output")(attention_vec)
