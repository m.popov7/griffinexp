from typing import Optional

import haiku as hk

from trix.layers.heads.lm_head import RobertaLMHead, SimpleLMHead
from trix.layers.sharding.linear.synchronous_linear import LinearLayerSynchronousInit
from trix.layers.sharding.regularization.norms import ReplicatedLayerNorm


class ShardedLMHead(SimpleLMHead):
    """
    Sharded version of the SimpleLMHead module. Weights are synchronously initialized
    but no further check is made to avoid communication. Adding sanity checks to make
    sure the weights do not de-synchronize during training is recommended.
    """

    def __init__(
        self,
        embed_dim: int,
        alphabet_size: int,
        add_bias_lm_head: bool = True,
        name: Optional[str] = None,
    ):
        """
        Args:
            embed_dim: Embedding dimension.
            alphabet_size: Number of tokens in the alphabet.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(embed_dim=embed_dim, alphabet_size=alphabet_size, name=name)
        self.embed_dim = embed_dim
        self.alphabet_size = alphabet_size

        # Define layers
        self._final_fc = LinearLayerSynchronousInit(
            self.alphabet_size,
            add_bias_lm_head=add_bias_lm_head,
            name="synchronous_final_fc",
        )


class ShardedRobertaLMHead(RobertaLMHead):
    """
    Class for sharded Roberta LM head in the ESM version. It consists into an MLP with
    LayerNorm included. In a sharded version we write it with ReplicatedLayerNorm,
    LinearAndPSum (sharded Linear) and ReplicatedLinear layer (for prediction).
    """

    def __init__(self, embed_dim: int, alphabet_size: int, name: Optional[str] = None):
        """
        Args:
            embed_dim: Embedding dimension.
            alphabet_size: Number of tokens in the alphabet.
            name: Name of the layer. Defaults to None.
        """
        super().__init__(embed_dim=embed_dim, alphabet_size=alphabet_size, name=name)

        # Define layers
        prefix = hk.experimental.current_name()
        self._first_layer_norm = ReplicatedLayerNorm(
            create_offset=True,
            name=hk.experimental.force_name(prefix + "/~/emb_layer_norm_after"),
        )

        self._fc1 = LinearLayerSynchronousInit(
            self.embed_dim, name=hk.experimental.force_name(prefix + "/~/lm_head_fc_1")
        )

        self._final_fc = LinearLayerSynchronousInit(
            self.alphabet_size,
            name=hk.experimental.force_name(prefix + "/~/lm_final_fc"),
        )
        self._second_layer_norm = ReplicatedLayerNorm(
            create_offset=True,
            name=hk.experimental.force_name(prefix + "/~/lm_head_layer_norm"),
        )
