from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp

from trix.layers.sharding.synchronous_init import SynchronousInitializer


class ReplicatedEmbed(hk.Module):
    """
    Embed layer to be used when sharding the model. It replicates the embedding matrix
    of the first device on other devices. Theoretically it should be sufficient to
    synchronize initialization as gradients are synchronized but this is done for more
    safety.
    """

    def __init__(self, vocab_size: int, embed_dim: int, name: Optional[str] = None):
        """
        Args:
            vocab_size: Number of tokens in the vocabulary.
            embed_dim: Embeddings size.
            name: Name of the layer.
        """
        super().__init__(name=name)
        self.vocab_size = vocab_size
        self.embed_dim = embed_dim
        self.w_init = SynchronousInitializer(hk.initializers.TruncatedNormal)

    def __call__(self, ids: jnp.ndarray) -> jnp.ndarray:
        embeddings = hk.get_parameter(
            name="embeddings", shape=[self.vocab_size, self.embed_dim], init=self.w_init
        )
        embeddings = jax.lax.all_gather(embeddings, "shard")[0]
        return jnp.asarray(embeddings)[(ids,)]
