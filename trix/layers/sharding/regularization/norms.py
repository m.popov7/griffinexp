from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp


class ReplicatedLayerNorm(hk.Module):
    """
    LayerNorm to be used when sharding the model. It ensures that the inputs
    are scaled the same way on all devices.
    """

    def __init__(self, create_offset: bool = True, name: Optional[str] = None):
        """
        Args:
            create_offset: Whether to create an offset or not.
            name: Layer's name.
        """
        super().__init__(name=name)
        self.offset = create_offset

    def __call__(self, inputs: jnp.ndarray) -> jnp.ndarray:
        mean = jnp.mean(inputs, axis=-1, keepdims=True)
        variance = jnp.var(inputs, axis=-1, keepdims=True)

        param_shape = inputs.shape[-1:]
        scale = hk.get_parameter("scale", param_shape, inputs.dtype, init=jnp.ones)
        scale = jax.lax.all_gather(scale, "shard")[0]
        if self.offset:
            offset = hk.get_parameter(
                "offset", param_shape, inputs.dtype, init=jnp.zeros
            )
            offset = jax.lax.all_gather(offset, "shard")[0]
            offset = jnp.broadcast_to(offset, inputs.shape)

        scale = jnp.broadcast_to(scale, inputs.shape)
        mean = jnp.broadcast_to(mean, inputs.shape)

        inv = scale * jax.lax.rsqrt(variance + 1e-5)
        if self.offset:
            return inv * (inputs - mean) + offset
        else:
            return inv * (inputs - mean)


class ReplicatedRMSNorm(hk.Module):
    """
    RMSNorm to be used when sharding the model. It ensures that the inputs
    are scaled the same way on all devices.
    """

    def __init__(self, name: Optional[str] = None, eps: float = 1e-6):
        """
        Args:
            name: Layer's name.
            eps: constant used for stability purpose
        """
        super().__init__(name=name)
        self.eps = eps

    def __call__(self, inputs: jnp.ndarray) -> jnp.ndarray:

        param_shape = inputs.shape[-1:]
        scale = hk.get_parameter("scale", param_shape, inputs.dtype, init=jnp.ones)
        scale = jax.lax.all_gather(scale, "shard")[0]

        mean_squared = jnp.mean(jnp.square(inputs), axis=-1, keepdims=True)

        mean_squared = jnp.broadcast_to(mean_squared, inputs.shape)

        return inputs * scale * jax.lax.rsqrt(mean_squared + self.eps)
