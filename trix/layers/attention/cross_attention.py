from typing import Optional

import haiku as hk
import jax
from haiku import initializers

from trix.layers.attention.multi_head_attention import MultiHeadAttention
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.types import AttentionMask, Embedding, Tokens, TransformerOutput


class CrossAttentionBlock(hk.Module):
    """
    Multi-headed attention block made of self-attention followed by cross-attention
    between input sequence embeddings and cross-embeddings.
    """

    def __init__(
        self,
        num_heads: int,
        embed_dim: int,
        ffn_embed_dim: int,
        rotary_embedding_config: Optional[RotaryEmbeddingConfig] = None,
        key_size: Optional[int] = None,
        add_bias_kv: bool = False,
        name: Optional[str] = None,
    ):
        super().__init__(name=name)
        # Add checks on dimensions
        if key_size is None:
            if embed_dim % num_heads != 0:
                raise ValueError(
                    f"The embedding dimension should be divisible by the number of "
                    f"heads, however provided embedding dimension is {embed_dim} and "
                    f"the number of heads is {num_heads}."
                )
            else:
                key_size = embed_dim // num_heads

        # Define layers
        w_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        b_init = initializers.VarianceScaling(2.0, "fan_in", "uniform")
        self.fc1 = hk.Linear(ffn_embed_dim, w_init=w_init, b_init=b_init, name="fc1")
        self.fc2 = hk.Linear(embed_dim, w_init=w_init, b_init=b_init, name="fc2")

        self.layer_norm_self_attention = hk.LayerNorm(
            axis=-1,
            create_scale=True,
            create_offset=True,
            name="self_attention_layer_norm",
        )
        self.layer_norm_cross_attention = hk.LayerNorm(
            axis=-1,
            create_scale=True,
            create_offset=True,
            name="cross_attention_layer_norm",
        )
        self.layer_norm_mlp = hk.LayerNorm(
            axis=-1, create_scale=True, create_offset=True, name="final_layer_norm"
        )
        self.self_attention_layer = MultiHeadAttention(
            num_heads=num_heads,
            key_size=key_size,
            add_bias_kv=add_bias_kv,
            name="self_attention",
            rotary_embedding_config=rotary_embedding_config,
        )

        self.cross_attention_layer = MultiHeadAttention(
            num_heads=num_heads,
            key_size=key_size,
            add_bias_kv=add_bias_kv,
            name="cross_attention",
            rotary_embedding_config=rotary_embedding_config,
        )

    @hk.transparent
    def self_attention(
        self,
        x: Embedding,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Applies the self attention mechanism.

        Args:
            x: Input token embeddings of shape (batch_size, seq_len, embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).

        Returns:
            Dictionary containing the output embeddings and the attention weights.
        """

        return self.self_attention_layer(x, x, x, attention_mask=attention_mask)

    @hk.transparent
    def cross_attention(
        self,
        x: Embedding,
        cross_attention_embeddings: Embedding,
        attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Applies the cross attention mechanism.

        Args:
            x: Input token embeddings of shape (batch_size, seq_len, embed_dim).
            cross_attention_embeddings: Embeddings to be used for cross attention
                (in encoder-decoder models, it is the output of the encoder) of shape
                (batch_size, seq_len, embed_dim).
            attention_mask: Attention mask of shape (batch_size, 1, seq_len, seq_len).


        Returns:
            A dictionary containing the output embeddings and the attention weights.
        """

        return self.cross_attention_layer(
            query=x,
            key=cross_attention_embeddings,
            value=cross_attention_embeddings,
            attention_mask=attention_mask,
        )

    @hk.transparent
    def mlp(self, x: Embedding) -> Embedding:
        """
        Applies one layer-norm, one linear layer, a Gelu activation,
        then a final linear layer

        Args:
            x: Embeddings of shape (batch_size, seq_len, key_size * num_heads).

        Returns:
            The transformed sequence embedding.
        """
        x = self.layer_norm_mlp(x)
        x = jax.nn.gelu(
            self.fc1(x),
            approximate=False,
        )
        x = self.fc2(x)
        return x

    def __call__(
        self,
        x: Tokens,
        cross_attention_embeddings: Embedding,
        attention_mask: Optional[AttentionMask] = None,
        cross_attention_mask: Optional[AttentionMask] = None,
    ) -> TransformerOutput:
        """
        Computes the output embeddings of the attention block.

        Args:
            x: Input tokens.
            cross_attention_embeddings: Embeddings to be used for cross attention
                (in encoder-decoder models, it is the output of the encoder).
            attention_mask: Attention mask of shape (batch_size, 1,seq_len, seq_len).
            cross_attention_mask: Cross-attention mask of shape
                (batch_size, 1, seq_len, seq_len].

        Returns:
            A dictionary containing the output embeddings and the attention weights.
        """

        # Self-Attention
        res = x
        x = self.layer_norm_self_attention(x)
        output = self.self_attention(
            x=x,
            attention_mask=attention_mask,
        )
        x = output["embeddings"]
        x = res + x

        # Cross-Attention
        res = x
        x = self.layer_norm_cross_attention(x)
        output = self.cross_attention(
            x=x,
            cross_attention_embeddings=cross_attention_embeddings,
            attention_mask=cross_attention_mask,
        )
        x = output["embeddings"]
        x = res + x

        # MLP
        x = x + self.mlp(x)

        output["embeddings"] = x
        return output  # type: ignore
