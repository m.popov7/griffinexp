import math
import random
from typing import Iterator, List, Tuple

import jax
import jax.numpy as jnp
import numpy as np

from trix.tokenizers.language_models.base import BaseTokenizer
from trix.types import RNGKey


class SequencesDataset:
    def __init__(
        self,
        sequences: List[str],
        tokenizer: BaseTokenizer,
        batch_size: int,
        shuffle: bool = True,
        drop_last: bool = True,
        num_hosts: int = 1,
        host_id: int = 0,
        seed: int = 0,
    ):
        """
        Args:
            sequences: Input sequences to be tokenized.
            tokenizer: Tokenizer.
            batch_size: Batch size.
            shuffle: Determines if the sequences are to be shuffled.
                Defaults to True.
            drop_last: Determines if the last batch is dropped. This is
                important because a difference in the input's shape will force JAX to
                recompile. Defaults to True.
            num_hosts: Number of hosts that share the data.
            host_id: Id of the current host.
            seed: Random seed to use to shuffle sequences.
        """

        # Take only subpart of all sequences corresponding to this host
        sequences = sequences[: (len(sequences) // num_hosts) * num_hosts]
        length = len(sequences) // num_hosts
        sequences = sequences[(host_id * length) : ((host_id + 1) * length)]

        # If needed, remove not full last batch
        if drop_last:
            sequences = sequences[: (len(sequences) // batch_size) * batch_size]

        # Internalize hyperparameters
        self._sequences = sequences
        self._tokenizer = tokenizer
        self._batch_size = batch_size
        self._shuffle = shuffle
        self._drop_last = drop_last
        self._seed = seed

    @property
    def num_batches_per_epoch(self) -> int:
        if self._drop_last:
            return math.floor(len(self._sequences) / self._batch_size)
        else:
            return math.ceil(len(self._sequences) / self._batch_size)

    def update_seed(self, seed: int) -> None:
        self._seed = seed

    def get_epoch_batches(self) -> Iterator[jnp.ndarray]:
        """
        Yields successive batches of tokens ids (jnp.ndarray)
        corresponding to one epoch.
        """
        if self._shuffle:
            random.seed(self._seed)
            random.shuffle(self._sequences)

        for i in range(0, len(self._sequences), self._batch_size):
            batch = self._sequences[i : i + self._batch_size]
            batch = self._tokenizer.batch_tokenize(batch)
            tokens_ids = [b[1] for b in batch]
            tokens_ids = jnp.asarray(tokens_ids, dtype=jnp.int32)
            yield tokens_ids


class LabeledSequenceDataset(SequencesDataset):
    """
    A dataset that pairs sequences with labels (int or float labels), to be used
    for instance to train classifiers or regressors.
    """

    def __init__(
        self,
        sequences: List[str],
        labels: np.ndarray,
        tokenizer: BaseTokenizer,
        batch_size: int,
        shuffle: bool = True,
        drop_last: bool = True,
        num_hosts: int = 1,
        host_id: int = 0,
        seed: int = 0,
        labels_continuous: bool = False,
    ):
        """
        Args:
            sequences: Input sequences to be tokenized.
            labels: Labels associated to the sequences.
            tokenizer: Tokenizer.
            batch_size: Batch size.
            shuffle: Determines if the sequences are to be shuffled.
                Defaults to True.
            drop_last: Determines if the last batch is dropped. This is
                important because a difference in the input's shape will force JAX to
                recompile. Defaults to True.
            num_hosts: Number of hosts that share the data.
            host_id: Id of the current host.
            seed: Seed to set stochasticity.
            labels_continuous: Determines if the labels are treated as integers
                (classification) or continuous values (regression).
        """

        super().__init__(
            sequences=sequences,
            tokenizer=tokenizer,
            batch_size=batch_size,
            shuffle=shuffle,
            drop_last=drop_last,
            num_hosts=num_hosts,
            host_id=host_id,
        )

        if len(sequences) != len(labels):
            raise ValueError("Sequences and labels lists must have the same length.")

        # Take only subpart of all labels corresponding to this host
        labels = labels[: (len(labels) // num_hosts) * num_hosts]
        length = len(labels) // num_hosts
        labels = labels[(host_id * length) : ((host_id + 1) * length)]

        # If needed, remove not full last batch
        if drop_last:
            labels = labels[: (len(labels) // batch_size) * batch_size]

        # Internalize hyperparameters
        self._labels = labels
        self._seed = seed
        self._num_sequences = len(labels)
        self._shuffled_indices = list(range(self._num_sequences))
        self._labels_continuous = labels_continuous

    def get_epoch_batches(
        self,
    ) -> Iterator[Tuple[jnp.ndarray, jnp.ndarray]]:
        """
        Yields successive batches of tokens ids (jnp.ndarray)
        corresponding to one epoch.
        """

        # Increment the seed
        self._seed += 1

        # Create the associated key
        shuffle_key = jax.random.PRNGKey(seed=self._seed)

        if self._shuffle:
            self._shuffled_indices = list(
                jax.random.choice(
                    shuffle_key,
                    np.array(range(self._num_sequences)),
                    shape=(self._num_sequences,),
                    replace=False,
                ).__array__()
            )

        for i in range(0, self._num_sequences, self._batch_size):
            batch = [
                self._sequences[idx]
                for idx in self._shuffled_indices[i : i + self._batch_size]
            ]
            batch = self._tokenizer.batch_tokenize(batch)
            labels_batch = [
                self._labels[idx]
                for idx in self._shuffled_indices[i : i + self._batch_size]
            ]

            # np.array before jnp.array because it is much faster for unknown reasons
            labels_batch = jnp.asarray(
                np.asarray(labels_batch),
                dtype=jnp.float32 if self._labels_continuous else jnp.int32,
            )
            tokens_ids = [b[1] for b in batch]
            tokens_ids = jnp.asarray(
                np.asarray(tokens_ids),
                dtype=jnp.int32,
            )
            yield tokens_ids, labels_batch


class PairedSequenceDataset:
    """
    Sequence dataset used for encoder decoder models where sequences are randomly split
    into two sequences. The prefix sequence is fed into the encoder and the suffix
    sequence is fed into the decoder. Two distinct tokenizers are used for prefix and
    suffix sequences separately.
    """

    def __init__(
        self,
        sequences: List[str],
        prefix_tokenizer: BaseTokenizer,
        suffix_tokenizer: BaseTokenizer,
        batch_size: int,
        min_sequence_len: int,
        random_key: RNGKey,
        shuffle: bool = True,
        drop_last: bool = True,
        num_hosts: int = 1,
        host_id: int = 0,
    ):
        """
        Args:
            sequences: Input sequences to be tokenized.
            prefix_tokenizer: Tokenizer for prefix sequences to be fed to Encoder.
            suffix_tokenizer: Tokenizer for suffix sequences to be fed to Decoder.
            batch_size: Batch size.
            min_sequence_len: The minimum sequence length of the encoder
                and decoder inputs. This value is used for selecting the random split
                point of every input sequence.
            random_key: JAX random key to use for selecting random points
                within sequences.
            shuffle: Determines if the sequences are to be shuffled.
                Defaults to True.
            drop_last: Determines if the last batch is dropped. This is important
                because a difference in the input's shape will force JAX to recompile.
                Defaults to True.
        """

        # Take only subpart of all sequences corresponding to this host
        sequences = sequences[: (len(sequences) // num_hosts) * num_hosts]
        length = len(sequences) // num_hosts
        sequences = sequences[(host_id * length) : ((host_id + 1) * length)]

        # If needed, remove last incomplete batch
        if drop_last:
            sequences = sequences[: (len(sequences) // batch_size) * batch_size]

        # Internalize hyperparameters
        self._min_sequence_len = min_sequence_len
        self._prefix_tokenizer = prefix_tokenizer
        self._suffix_tokenizer = suffix_tokenizer
        self._batch_size = batch_size
        self._shuffle = shuffle
        self._drop_last = drop_last

        # generate split sequences
        self._paired_sequences = self.random_split(sequences, random_key)

    @property
    def num_batches_per_epoch(self) -> int:
        if self._drop_last:
            return math.floor(len(self._paired_sequences) / self._batch_size)
        else:
            return math.ceil(len(self._paired_sequences) / self._batch_size)

    def random_split(self, sequences: List[str], random_key: RNGKey) -> List[List[str]]:
        # get all sequence's lengths
        seq_lengths = jnp.array([len(seq) for seq in sequences])

        # check that min sequence length is less than or equal to every sequence's
        # length divided by 2
        if not (self._min_sequence_len <= seq_lengths // 2).all():
            raise ValueError(
                "min sequence length must be smaller than or equal to every "
                "sequence's length divided by 2"
            )

        # randomly select a position to split each sequence
        split_positions = jax.random.randint(
            random_key,
            seq_lengths.shape,
            self._min_sequence_len,
            seq_lengths - self._min_sequence_len,
        )

        # split sequences
        # Example: ['ABABAAA','DCDCDCDCD'] --> [['ABA', 'BAAA'], ['DCDCD', 'CDCD']]
        paired_sequences = [
            [seq[:pos], seq[pos:]] for seq, pos in zip(sequences, split_positions)
        ]

        return paired_sequences

    def get_epoch_batches(
        self,
    ) -> Iterator[Tuple[jnp.ndarray, jnp.ndarray]]:
        """
        Yield successive batches of paired tokens ids (jnp.ndarray) corresponding
        to one epoch.
        """
        if self._shuffle:
            random.shuffle(self._paired_sequences)

        for i in range(0, len(self._paired_sequences), self._batch_size):
            # create two lists of sequences
            prefix_sequences, suffix_sequences = list(zip(*self._paired_sequences))

            # tokenize prefix sequences
            prefix_batch = list(prefix_sequences[i : i + self._batch_size])
            prefix_batch = self._prefix_tokenizer.batch_tokenize(prefix_batch)
            prefix_tokens_ids = np.array([b[1] for b in prefix_batch])
            prefix_tokens_ids = jnp.array(prefix_tokens_ids, dtype=jnp.int32)

            # tokenize suffix sequences
            suffix_batch = list(suffix_sequences[i : i + self._batch_size])
            suffix_batch = self._suffix_tokenizer.batch_tokenize(suffix_batch)
            suffix_tokens_ids = np.array([b[1] for b in suffix_batch])
            suffix_tokens_ids = jnp.array(suffix_tokens_ids, dtype=jnp.int32)

            yield prefix_tokens_ids, suffix_tokens_ids
