from typing import List, Tuple

import numpy as np


def generate_fake_dataset(
    min_sequence_length: int,
    max_sequence_length: int,
    num_sequences: int,
    standard_tokens: List[str],
    seed: int,
) -> List[str]:
    """
    Generates a random dataset for testing/debugging purpose.

    Args:
        min_sequence_length: Min seq length.
        max_sequence_length: Max seq length.
        num_sequences: Number of sequences.
        standard_tokens: List of standard tokens.
        seed: Random seed.

    Returns:
        A list of randomly generated sequences.
    """
    if max_sequence_length < min_sequence_length:
        raise ValueError("max_sequence_length must be higher than min_sequence_length")
    rng = np.random.default_rng(seed)
    lengths = rng.integers(
        low=min_sequence_length, high=max_sequence_length + 1, size=num_sequences
    )
    sequences = [
        "".join(rng.choice(standard_tokens, size=length)) for length in lengths
    ]
    return sequences


def generate_fake_labeled_dataset(
    min_sequence_length: int,
    max_sequence_length: int,
    num_sequences: int,
    standard_tokens: List[str],
    num_labels: int = 1,
    seed: int = 0,
) -> Tuple[List[str], List[List[int]]]:
    """
    Generates a random labeled classification dataset for testing/debugging purpose.
    Labels (classes) are simply computed as the id of the num_labels first tokens in
    each sequence.

    Args:
        min_sequence_length: Min seq length.
        max_sequence_length: Max seq length.
        num_sequences: Number of sequences.
        standard_tokens: List of standard tokens.
        num_labels: Number of labels
        seed: Random seed.

    Returns:
        A list of randomly generated sequences with their corresponding labels
    """
    if max_sequence_length < min_sequence_length:
        raise ValueError("max_sequence_length must be higher than min_sequence_length")

    rng = np.random.default_rng(seed)
    lengths = rng.integers(
        low=min_sequence_length, high=max_sequence_length + 1, size=num_sequences
    )
    sequences = [
        "".join(rng.choice(standard_tokens, size=length)) for length in lengths
    ]
    toks_to_id = {tok: i for i, tok in enumerate(standard_tokens)}
    labels = [[toks_to_id[seq[idx]] for idx in range(num_labels)] for seq in sequences]

    return sequences, labels


def generate_fake_regression_dataset(
    min_sequence_length: int,
    max_sequence_length: int,
    num_sequences: int,
    standard_tokens: List[str],
    num_labels: int = 1,
    seed: int = 0,
) -> Tuple[List[str], List[List[int]]]:
    """
    Generates a random labeled classification dataset for testing/debugging purpose.
    Labels (classes) are simply computed as the id of the num_labels first tokens in
    each sequence.

    Args:
        min_sequence_length: Min seq length.
        max_sequence_length: Max seq length.
        num_sequences: Number of sequences.
        standard_tokens: List of standard tokens.
        num_labels: Number of labels
        seed: Random seed.

    Returns:
        A list of randomly generated sequences with their corresponding labels
    """
    if max_sequence_length < min_sequence_length:
        raise ValueError("max_sequence_length must be higher than min_sequence_length")

    rng = np.random.default_rng(seed)
    lengths = rng.integers(
        low=min_sequence_length, high=max_sequence_length + 1, size=num_sequences
    )
    sequences = [
        "".join(rng.choice(standard_tokens, size=length)) for length in lengths
    ]
    labels = [rng.standard_normal(num_labels) for seq in sequences]

    return sequences, labels
