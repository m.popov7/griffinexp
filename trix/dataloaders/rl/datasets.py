import math
from functools import partial
from typing import Iterator

import jax
import jax.numpy as jnp

from trix.tokenizers.rl.base import RLTokenizer, Transition
from trix.types import RNGKey, Tokens
from trix.utils.rl import chunk_trajectory_tokens


class TrajectoryDataset:
    def __init__(
        self,
        data: Transition,
        tokenizer: RLTokenizer,
        batch_size: int,
        random_key: RNGKey,
        horizon_size: int,
        num_chunks_per_trajectory: int,
        shuffle: bool = True,
        drop_last: bool = True,
        resample_chunks: bool = False,
    ):
        """
        Args:
            data: Transitions to be tokenized.
            tokenizer: Tokenizer.
            batch_size: Batch size.
            random_key: Random key.
            horizon_size: Sequence length of the tokens in the dataset.
            num_chunks_per_trajectory: Number of chunks we sample from a trajectory.
            shuffle: Determines if the sequences are to be shuffled.
                Defaults to True.
            drop_last: Determines if the last batch is dropped. This is
                important because a difference in the input's shape will force JAX to
                recompile. Defaults to True.
            resample_chunks: Whether to sample new chunks each time `get_epoch_batches`
                is called. Defaults to False.
        """

        self._num_sequences = data.obs.shape[0]
        self._episode_length = data.obs.shape[1]

        self._tokenizer = tokenizer
        # Internalize hyperparameters
        self._tokens = jax.jit(tokenizer.batch_tokenize)(data)  # type: ignore
        self._chunks = None
        self._batch_size = batch_size
        self._shuffle = shuffle
        self._drop_last = drop_last

        self._horizon_size = horizon_size
        self._num_chunks_per_trajectory = num_chunks_per_trajectory
        self._resample_chunks = resample_chunks

        self._shuffle_key, self._key_resample = jax.random.split(random_key)

    @property
    def num_batches_per_epoch(self) -> int:
        if self._drop_last:
            return math.floor(
                self._num_sequences * self._num_chunks_per_trajectory / self._batch_size
            )
        else:
            return math.ceil(
                self._num_sequences * self._num_chunks_per_trajectory / self._batch_size
            )

    def get_epoch_batches(self) -> Iterator[jnp.ndarray]:
        """
        Yields successive batches of tokens ids (jnp.ndarray)
        corresponding to one epoch.
        """
        # If resample_chunks is False, we want to cache the chunks to speed up
        # the data loading, otherwise we recompute chunks every time this method
        # is called
        if self._resample_chunks:
            random_key, subkey = jax.random.split(self._key_resample)
            self._key_resample = random_key
            cached = False
        else:
            subkey = self._key_resample
            cached = True

        if cached and self._chunks is not None:
            chunks = self._chunks
        else:
            chunks = chunk_trajectory_tokens(
                tokens=self._tokens,
                random_key=self._key_resample,
                horizon_size=self._horizon_size,
                concat_size=self._tokenizer.concat_size,
                num_chunks_per_trajectory=self._num_chunks_per_trajectory,
            )

            if cached:
                self._chunks = chunks

        indices = jnp.arange(self._num_sequences * self._num_chunks_per_trajectory)

        # Permute the chunks order
        if self._shuffle:
            random_key, subkey = jax.random.split(self._shuffle_key)
            indices = jax.random.permutation(subkey, indices, independent=True)
            self._shuffle_key = random_key

        indices = indices[: (self.num_batches_per_epoch * self._batch_size)]

        batches = indices.reshape(self.num_batches_per_epoch, self._batch_size)

        # as the for loop is not binding we define a function to index x, otherwise
        # the loop will only return the last batch
        def index_batch(chunk: Tokens, batch: jnp.ndarray) -> Tokens:
            return chunk[batch]

        for batch in batches:
            tokens = jax.tree_map(partial(index_batch, batch=batch), chunks)
            yield tokens
