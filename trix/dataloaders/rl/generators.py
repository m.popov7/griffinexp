import jax
import jax.numpy as jnp

from trix.tokenizers.rl.base import Transition
from trix.types import RNGKey
from trix.utils.rl import get_mask_from_dones


def generate_fake_rl_trajectories(
    num_trajectories: int,
    episode_length: int,
    observation_size: int,
    action_size: int,
    random_key: RNGKey,
    allow_incomplete: bool = False,
    padding_token_id: float = -1.0,
    std: float = 10.0,
) -> Transition:
    """
    Generates a random dataset of RL trajectories for testing/debugging purposes.
    Args:
        num_trajectories: Number of trajectories.
        episode_length: Episode length.
        observation_size: Dimension of the observation space.
        action_size: Dimension of the action space.
        random_key: Random key.
        allow_incomplete: Whether to generate incomplete trajectories.
            Defaults to False.
        padding_token_id: The value to input for padded transitions.
        std: The standard deviation of the gaussian used to generate the observations.

    Returns:
        A sequence of Transition objects.
    """

    random_key, subkey = jax.random.split(random_key)
    obs = (
        jax.random.normal(
            subkey, shape=(num_trajectories, episode_length, observation_size)
        )
        * std
    )
    random_key, subkey = jax.random.split(random_key)
    actions = jax.random.uniform(
        subkey,
        minval=-1,
        maxval=1,
        shape=(num_trajectories, episode_length, action_size),
    )
    random_key, subkey = jax.random.split(random_key)
    rewards = (
        jax.random.normal(subkey, shape=(num_trajectories, episode_length)) + 0.5
    ) * 10

    if allow_incomplete:
        random_key, subkey = jax.random.split(random_key)

        dones = jax.random.randint(
            subkey, shape=(num_trajectories,), minval=0, maxval=episode_length
        )
        dones = jax.nn.one_hot(dones, episode_length)
        mask = jax.vmap(get_mask_from_dones)(dones)

    else:
        dones = jnp.zeros_like(rewards)
        dones = dones.at[:, -1].set(1)
        mask = jnp.ones_like(rewards)

    actions = jnp.where(mask[..., None], actions, padding_token_id)
    obs = jnp.where(mask[..., None], obs, padding_token_id)
    rewards = jnp.where(mask, rewards, padding_token_id)
    transitions = Transition(obs=obs, actions=actions, rewards=rewards, dones=dones)

    return transitions
