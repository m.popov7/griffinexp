# type: ignore

import argparse
import logging
import os
import subprocess
from dataclasses import asdict
from pathlib import Path

import haiku as hk
import jax
import jax.numpy as jnp
import neptune.new as neptune
import numpy as np
import optax
from Bio import SeqIO

from trix.dataloaders.language_models.sequence_datasets import SequencesDataset
from trix.models.esm.sharding.sharded_esm import (
    ESMTransformerConfig,
    build_sharded_esm_fn,
)
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.language_trainers.encoder_lm_trainer import EncoderMLMTrainer
from trix.training.utils import split_sequences
from trix.utils.parameters import get_num_parameters


def get_parser_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    # Add our training arguments
    parser.add_argument(
        "--neptune-project",
        type=str,
        help="Neptune projec to which the metrics are logged.",
    )
    parser.add_argument(
        "--dataset-path",
        type=str,
        help="path to dataset, expect a .fasta file",
        default="/app/data/uniref50.01.fasta",
    )
    parser.add_argument(
        "--max-length",
        type=int,
        help="max sequence length, discard chain if longer than this value.",
        default=511,
    )
    parser.add_argument(
        "--training-set-proportion",
        type=float,
        help="percentage of the genome to be used for training. The remaining sequences"
        "will be used for validation.",
        default=0.95,
    )
    parser.add_argument(
        "--effective-batch-size",
        type=int,
        help="Effective batch size. Gradient accumulation "
        "will be determined accordingly.",
        default=2048,
    )
    parser.add_argument(
        "--num-layers", type=int, help="Number of attention layers", default=6
    )
    parser.add_argument(
        "--token-dropout", type=bool, default=True, help="token dropout"
    )
    parser.add_argument(
        "--embed-dim", type=int, help="Encoder Embedding size.", default=1024
    )
    parser.add_argument(
        "--ffn-embed-dim",
        type=int,
        help="Feed Forward Network Embedding size.",
        default=4096,
    )
    parser.add_argument(
        "--attention-heads",
        type=int,
        help="Number of attention heads",
        default=32,
    )
    parser.add_argument(
        "--noising-ratio", type=float, help="noising ratio", default=0.15
    )
    parser.add_argument(
        "--masking-prob", type=float, help="masking probability", default=0.8
    )
    parser.add_argument(
        "--random-token-prob", type=float, help="random token probability", default=0.1
    )
    parser.add_argument("--num-epochs", type=int, help="number of epochs", default=1)

    parser.add_argument(
        "--batch-size",
        type=int,
        help="num sequences per batch, "
        "to be adapted given the accelerators RAM available",
        default=32,
    )
    parser.add_argument("--num-shards", type=int, help="number of shards", default=1)

    parser.add_argument("--seed", type=int, help="seed", default=0)

    parser.add_argument("--lr", type=float, help="learning rate", default=1e-4)
    args = parser.parse_args()
    return args


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    # Get command line arguments
    args = get_parser_args()

    # Get devices
    devices = jax.local_devices()
    num_devices = len(devices)
    print(f"Detected the following local devices: {tuple(devices)}")

    # Global devices
    global_devices = jax.devices()
    num_global_devices = len(global_devices)
    print(
        f"Detected {num_devices} local devices "
        f"and {num_global_devices} global devices."
    )

    # Download data if needed
    dataset_path = Path(args.dataset_path).resolve()
    if not dataset_path.exists():
        dataset_filename = dataset_path.stem + dataset_path.suffix
        logging.warning(
            f"No data available at {str(dataset_path)}. "
            f"Downloading the dataset {dataset_filename} from the dcr bucket."
            f"Please make sure that the dataset is there, that AWS is "
            f"installed and the AWS env keys are exported."
        )
        dataset_dir = f"{str(dataset_path.parent)}"
        os.makedirs(dataset_dir, exist_ok=True)
        subprocess.run(
            [
                "aws",
                "s3",
                "cp",
                f"s3://trix/datasets/proteins/{dataset_filename}",
                f"{dataset_dir}/.",
                "--endpoint=https://s3.kao.instadeep.io",
            ]
        )

    # Get data
    with open(args.dataset_path) as handle:
        sequences = [str(record.seq) for record in SeqIO.parse(handle, "fasta")]

    lengths = [len(seq) for seq in sequences]
    standard_tokens = list({aa for seq in sequences for aa in seq})

    print(
        f"Number of sequences: {len(sequences)}, "
        f"Maximum length: {max(lengths)}, "
        f"total number of amino-acids: {sum(lengths)}"
    )
    print(f"tokens found: {standard_tokens}")

    # Filter data
    max_length = args.max_length

    sequences = [seq for seq in sequences if len(seq) <= max_length]
    lengths = [len(seq) for seq in sequences]
    standard_tokens = list({aa for seq in sequences for aa in seq})

    print(
        f"After filtering, "
        f"Number of sequences: {len(sequences)}, "
        f"Maximum length: {max(lengths)}, "
        f"total number of amino-acids: {sum(lengths)}"
    )
    print(f"tokens found: {standard_tokens}")

    train_proportion = args.training_set_proportion
    seed = args.seed

    print(
        f"Use {train_proportion}% of the data for training "
        f"and the remaining data for validation."
    )
    train_sequences, validation_sequences = split_sequences(
        sequences, train_proportion, seed
    )

    # Get hyper-parameters
    prepend_cls_token = True
    effective_batch_size = args.effective_batch_size
    batch_size = args.batch_size
    num_shards = args.num_shards
    num_data_parallel_ways = num_devices // num_shards
    assert effective_batch_size % (batch_size * num_data_parallel_ways) == 0, (
        f"Effective batch size {effective_batch_size} needs to be dividible by "
        f"batch size ({batch_size}) x num_data_parallel_ways ({num_data_parallel_ways})"
    )
    num_acc_grads = int(effective_batch_size // (batch_size * num_data_parallel_ways))

    print(f"Gradient accumulation: {num_acc_grads}")
    print(f"Num shards: {num_shards}")
    print(f"Data parallel ways: {num_data_parallel_ways}")

    noising_ratio = args.noising_ratio
    masking_prob = args.masking_prob
    random_token_prob = args.random_token_prob
    learning_rate = args.lr

    # Get model and tokenizer
    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=standard_tokens,
        fixed_length=max_length + int(prepend_cls_token),
        prepend_cls_token=prepend_cls_token,
    )
    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        mask_token_id=tokenizer.mask_token_id,
        pad_token_id=tokenizer.pad_token_id,
        max_positions=max_length + int(prepend_cls_token),
        attention_heads=args.attention_heads,
        embed_dim=args.embed_dim,
        ffn_embed_dim=args.ffn_embed_dim,
        num_layers=args.num_layers,
        positional_embedding=None,
        use_rotary_embedding=True,
    )

    # Get datasets
    train_dataset = SequencesDataset(
        sequences=train_sequences,
        tokenizer=tokenizer,
        batch_size=batch_size * num_data_parallel_ways,
        shuffle=True,
        drop_last=True,
        seed=0,
    )
    train_generator = iter(train_dataset.get_epoch_batches())
    validation_dataset = SequencesDataset(
        sequences=validation_sequences,
        tokenizer=tokenizer,
        batch_size=batch_size * num_data_parallel_ways,
        shuffle=False,
        drop_last=True,
    )

    # Get forward function
    forward_fn = build_sharded_esm_fn(model_config, num_shards=num_shards)
    forward_fn = hk.transform(forward_fn)

    optimizer = optax.adam(learning_rate=learning_rate)
    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    trainer = EncoderMLMTrainer(
        apply_fn=forward_fn.apply,
        init_fn=forward_fn.init,
        tokenizer=tokenizer,
        optimizer=optimizer,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
        random_token_prob=random_token_prob,
    )  # type: ignore

    # Initialize on devices
    devices_mesh = np.array(devices).reshape((-1, num_shards))
    print(f"Devices mesh: {devices_mesh}")

    random_key = jax.random.PRNGKey(args.seed)
    keys = jnp.tile(random_key, reps=(num_shards, num_data_parallel_ways, 1))
    dummy_tokens = next(train_dataset.get_epoch_batches())
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        training_state = trainer.build_init_fn()(keys, dummy_tokens[:1, :1])

    number_params = get_num_parameters(
        jax.device_put(
            jax.tree_map(lambda x: x[:, 0], training_state.params),
            jax.devices("cpu")[0],
        )
    )
    print(f"Number of parameters: {number_params}")

    # Initialize neptune
    if args.neptune_project:
        run = neptune.init(
            project=args.neptune_project,
            tags=[
                "sharded",
                "encoder",
                f"num_shards: {args.num_shards}",
                f"num_params: {number_params}",
            ],
        )

        run_parameters = vars(args)

        run_parameters.update(asdict(model_config))
        run["run_parameters"] = run_parameters

    # Training steps
    # number of total backward propagation computed
    num_training_steps = int(
        args.num_epochs * train_dataset.num_batches_per_epoch / num_acc_grads
    )

    # step to do before doing validation
    validation_step = 100
    # number of steps to perform to do validation
    num_validation_steps = 10
    # number of steps before resetting the training generator
    num_steps_per_epoch = train_dataset.num_batches_per_epoch

    all_metrics = {
        "train_loss": [],
        "train_acc": [],
        "train_step": [],
        "val_loss": [],
        "val_acc": [],
        "val_step": [],
    }

    print(f"Training for {num_training_steps} steps")
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        # Build xmapped update function
        update_fn = trainer.build_xmapped_update_fn()
        compute_metrics_fn = trainer.build_xmapped_metrics_fn()

        for step in range(1, num_training_steps + 1):
            train_acc = 0.0
            train_loss = 0.0
            for _ in range(num_acc_grads):
                tokens = next(train_generator)
                tokens = jnp.reshape(
                    tokens,
                    (num_data_parallel_ways, batch_size, tokens.shape[-1]),
                )
                # Update neural network and optimizer
                training_state, training_metrics = update_fn(training_state, tokens)
                # Get rid of the shard dimension, along which metrics are replicated
                training_metrics = jax.tree_map(lambda x: x[0], training_metrics)
                train_acc += float(training_metrics["accuracy"])
                train_loss += float(training_metrics["loss"])

            train_acc /= num_acc_grads
            train_loss /= num_acc_grads

            print(
                f"Step {step} | Training loss {train_loss} ; "
                f"Training accuracy {train_acc}"
            )

            all_metrics["train_loss"].append(train_loss)
            all_metrics["train_acc"].append(train_acc)
            all_metrics["train_step"].append(step)

            # Log to Neptune. Training metrics have a first axis of dim=num_shards.
            if args.neptune_project:
                run["train_loss"].log(train_loss, step=step)
                run["train_acc"].log(train_acc, step=step)

            # Validation step
            if step % validation_step == 0:
                for _ in range(num_validation_steps):
                    validation_generator = iter(validation_dataset.get_epoch_batches())
                    validation_metrics = []
                    tokens = next(validation_generator)

                    # Reshape data to send to devices
                    tokens = jnp.reshape(
                        tokens,
                        (num_data_parallel_ways, batch_size, tokens.shape[-1]),
                    )

                    # Make predictions
                    metrics = compute_metrics_fn(training_state, tokens)

                    validation_metrics.append(jax.tree_map(lambda x: x[0], metrics))
                validation_metrics = jax.tree_map(
                    lambda *leaf: jnp.mean(jnp.stack(leaf, axis=0), axis=0),
                    *validation_metrics,
                )

                # update the seed to perform validatio on new data next time
                validation_dataset.update_seed(validation_dataset._seed + 1)

                print(
                    f"Step {step} | Validation loss {validation_metrics['loss']} ; "
                    f"Validation accuracy {validation_metrics['accuracy']}"
                )
                all_metrics["val_loss"].append(validation_metrics["loss"])
                all_metrics["val_acc"].append(validation_metrics["accuracy"])
                all_metrics["val_step"].append(step)

                # Log to Neptune
                if args.neptune_project:
                    run["val_loss"].log(validation_metrics["loss"], step=step)
                    run["val_acc"].log(validation_metrics["accuracy"], step=step)

            # After reaching an epoch, update the seed and reset the generator
        if step % num_steps_per_epoch == 0:
            train_dataset.update_seed(train_dataset._seed + 1)
            train_generator = iter(train_dataset.get_epoch_batches())
