# @noqa
"""
Training script for T5 model on NetMHC data.
"""

import argparse
import logging
import os
import pickle as pkl
import sys

# import time
from dataclasses import asdict
from pathlib import Path

import haiku as hk
import jax
import jax.numpy as jnp
import neptune.new as neptune
import numpy as np
import optax
from tqdm import tqdm

from trix.dataloaders.language_models.huggingface_datasets import (
    HFEncoderDecoderDataset,
)
from trix.models.t5.decoder import T5DecoderConfig, build_t5_decoder_only
from trix.pretrained.t5 import get_t5_decoder_config
from trix.tokenizers.language_models.spm_tokenizer import SentencePieceTokenizer
from trix.training.language_trainers.encoder_decoder_trainer import (
    DecoderOnlyCausalTrainer,
)
from trix.training.losses import cross_entropy_loss_decoder_only
from trix.types import AttentionMask, Tokens
from trix.utils.parameters import save_params

HF_ACCESS_TOKEN = os.environ.get("HF_ACCESS_TOKEN")


def get_decoder_config(model_name: str) -> T5DecoderConfig:
    if model_name == "netmhc-ba-el":
        config = T5DecoderConfig(
            vocab_size=256,
            embed_dim=128,
            num_layers=4,
            num_heads=4,
            attention_embed_dim=192,
            ffn_embed_dim=128,
            ffn_activation_name="gelu",
            logits_from_embed=False,
        )
    else:
        config = get_t5_decoder_config(model_name)
    return config


def get_parser_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    # Training parameters
    parser.add_argument("--neptune-project", type=str, default=None)
    parser.add_argument(
        "--train-dataset",
        type=str,
        default="InstaDeepAI/Multitask-T5-BA-EL-data",
        help="Dataset name for HF datasets.",
    )
    parser.add_argument(
        "--test-dataset",
        type=str,
        default="InstaDeepAI/Multitask-T5-BA-EL-data",
        help="Dataset name for HF datasets.",
    )
    parser.add_argument("--model-name", type=str, default="t5-base")
    parser.add_argument(
        "--spm-model-path",
        type=str,
        required=True,
        help="Pretrained SentencePiece model for tokenization. \
            An example tokenizer \
            can be found at \
            tests/F-tokenizers/test_files/vocabs_neo_spm_el_ba_sep.model",
    )
    parser.add_argument(
        "--backend",
        type=str,
        default="gpu",
        choices=["gpu", "tpu", "cpu"],
        help="Backend to use for computations.",
    )
    parser.add_argument("--batch-size", type=int, default=32)
    parser.add_argument("--effective-batch-size", type=int, default=2048)
    parser.add_argument("--lr", type=float, default=1e-4)
    parser.add_argument("--warmup-steps", type=int, default=10000)
    parser.add_argument("--weight-decay", type=float, default=0.01)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--dropout", type=float, default=0.0)
    parser.add_argument("--results-dir", type=str, default="results")
    parser.add_argument("--val-freq", type=int, default=1000)
    parser.add_argument(
        "--num-training-steps",
        type=int,
        default=10000,
    )
    parser.add_argument("--num-val-steps", type=int, default=100)
    parser.add_argument("--tokenized-seq-len", type=int, default=60)
    parser.add_argument("--checkpoint-save-freq", type=int, default=50)

    return parser.parse_args()


if __name__ == "__main__":
    args = get_parser_args()
    batch_size = args.batch_size

    # Setup logging

    # Setup results directory
    results_dir = Path(args.results_dir)
    results_dir.mkdir(exist_ok=True)

    logger = logging.getLogger("")
    logger.setLevel(logging.ERROR)
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.addHandler(logging.FileHandler(results_dir / "log_all.txt", mode="w"))

    train_logger = logging.getLogger("train")
    train_logger.setLevel(logging.INFO)
    train_logger.addHandler(
        logging.FileHandler(results_dir / "log_train.txt", mode="w")
    )

    # Setup tokenizer
    tokenizer = SentencePieceTokenizer(args.spm_model_path, add_eos_token=True)

    # set default device to CPU
    jax.config.update("jax_platform_name", "cpu")

    # get jax devices
    devices = jax.devices(backend=args.backend)
    num_devices = len(devices)
    logger.info(f"Detected devices: {devices}")
    effective_batch_size = args.effective_batch_size
    assert effective_batch_size % args.batch_size * num_devices == 0
    num_acc_grads = int(effective_batch_size // (args.batch_size * num_devices))
    model_config: T5DecoderConfig = get_decoder_config(args.model_name)
    model_config.dropout_rate = args.dropout
    # Setup model
    model_fn = build_t5_decoder_only(
        model_config,
    )
    model_fn = hk.transform(model_fn)

    # Setup optimizer
    lr = optax.linear_schedule(
        init_value=args.lr,
        end_value=0.0,
        transition_steps=args.num_training_steps,
        transition_begin=args.warmup_steps,
    )
    optimizer = optax.MultiSteps(
        optax.chain(
            optax.clip_by_global_norm(max_norm=1.0),
            optax.adamw(lr, weight_decay=args.weight_decay),
        ),
        every_k_schedule=num_acc_grads,
    )

    if args.neptune_project:
        run = neptune.init_run(
            project=args.neptune_project,
            tags=["non_sharded", "decoder", "t5", "netmhc1"],
        )
        run_parameters = vars(args)

        run_parameters.update(asdict(model_config))
        run["run_parameters"] = run_parameters

    # Setup data
    train_ds = HFEncoderDecoderDataset(
        dataset_name=args.train_dataset,
        tokenizer=tokenizer,
        batch_size=effective_batch_size,
        shuffle=True,
        shuffle_buffer_size=100000,
        seed=args.seed,
        split="train",
        streaming=True,
        tokenized_sequence_length=args.tokenized_seq_len,
        task_name="mixed_data",
        input_key="input",
        output_key="target",
        use_auth_token=HF_ACCESS_TOKEN,
    )
    val_ds = HFEncoderDecoderDataset(
        dataset_name=args.test_dataset,
        tokenizer=tokenizer,
        batch_size=batch_size * num_devices,
        shuffle=False,
        seed=args.seed,
        split="test",
        streaming=True,
        tokenized_sequence_length=args.tokenized_seq_len,
        task_name="mixed_data",
        input_key="input",
        output_key="target",
        use_auth_token=HF_ACCESS_TOKEN,
    )

    train_gen = train_ds.get_iterator()
    val_gen = val_ds.get_iterator()

    logger.debug("getting iterators")
    num_training_steps = args.num_training_steps
    logger.debug(f"{num_training_steps} training steps")
    num_val_steps = args.num_val_steps

    # Setup trainer
    trainer = DecoderOnlyCausalTrainer(
        forward_fn=model_fn,
        tokenizer=tokenizer,
        optimizer=optimizer,
        loss_fn=cross_entropy_loss_decoder_only,
        num_decoding_steps=2,
        noising_ratio=0.2,
        masking_prob=0.0,
        random_token_prob=0.0,
        mask_first_token=False,
    )

    # Initialize on devices
    keys = jnp.stack(jax.random.split(jax.random.PRNGKey(args.seed), num_devices))
    dummy_tokens: Tokens
    dummy_output: Tokens
    dummy_tokens, _, dummy_output, _ = next(train_gen)
    logger.debug(f"{dummy_tokens.shape}, {dummy_output.shape}")
    dummy_tokens = jnp.reshape(dummy_tokens, (num_devices, batch_size, -1))
    dummy_output = jnp.reshape(dummy_output, (num_devices, batch_size, -1))
    training_state = jax.pmap(trainer.init, devices=devices)(
        keys,
        decoder_input_tokens=dummy_tokens,
    )

    pmapped_update_fn = jax.pmap(
        trainer.update, devices=devices, axis_name="batch", donate_argnums=(0,)
    )

    pmapped_metrics_fn = jax.pmap(
        trainer.compute_metrics, devices=devices, axis_name="batch"
    )

    logger.info(f"Training for {num_training_steps} steps")
    epoch_metrics = []
    best_val_loss = float("inf")
    for step_num in tqdm(
        range(0, num_training_steps),
        total=num_training_steps,
        desc="Training...",
    ):
        train_loss = 0.0
        tokens: Tokens
        attn_mask: AttentionMask
        output: Tokens
        output_attn_mask: AttentionMask
        try:
            tokens, attn_mask, output, output_attn_mask = next(train_gen)
        except StopIteration:
            train_ds._seed = train_ds._seed + 1
            train_gen = train_ds.get_iterator()
            tokens, attn_mask, output, output_attn_mask = next(train_gen)
        tokens = jnp.reshape(
            tokens,
            (
                num_devices,
                num_acc_grads,
                batch_size,
            )
            + tokens.shape[1:],
        )
        output = jnp.reshape(
            output,
            (
                num_devices,
                num_acc_grads,
                batch_size,
            )
            + output.shape[1:],
        )[:, :, :]
        training_state, training_metrics = pmapped_update_fn(
            training_state,
            decoder_input_tokens=tokens,
            decoder_output_tokens=output,
        )
        train_logger.info(
            f"step {step_num},{training_metrics['loss']},{training_metrics['accuracy']}"
        )
        epoch_metrics.append(jax.tree_map(lambda x: x[0], training_metrics))
        if args.neptune_project:
            for k, v in epoch_metrics[-1].items():
                run[f"train_{k}"].log(v)
        if step_num % args.checkpoint_save_freq == 0:
            logger.info(f"saving checkpoint at {step_num}")
            saved_training_state = jax.tree_util.tree_map(
                lambda x: x[0], training_state
            )
            save_params(
                saved_training_state, os.path.join(args.results_dir, "latest_model.pkl")
            )
        if step_num % args.val_freq == 0 and step_num > 0:
            aggregated_metrics = jax.tree_map(
                lambda x: x.mean(),
                jax.tree_map(lambda *x: jnp.stack(x), *epoch_metrics),
            )
            val_metrics = []
            for _ in tqdm(range(num_val_steps), desc="Validating..."):
                try:
                    tokens, attn_mask, output, output_attn_mask = next(val_gen)
                except StopIteration:
                    val_gen = val_ds.get_iterator()
                    tokens, attn_mask, output, output_attn_mask = next(val_gen)
                    logging.info("finished one epoch of validation")

                tokens = jnp.reshape(
                    tokens,
                    (
                        num_devices,
                        batch_size,
                    )
                    + tokens.shape[1:],
                )
                output = jnp.reshape(
                    output,
                    (
                        num_devices,
                        batch_size,
                    )
                    + output.shape[1:],
                )[:, :, :]
                val_metrics.append(
                    jax.tree_map(
                        lambda x: x[0],
                        pmapped_metrics_fn(training_state, tokens, output),
                    )
                )
            val_metrics = jax.tree_map(
                lambda *arguments: np.mean(arguments), *val_metrics
            )
            if args.neptune_project:
                for k, v in val_metrics.items():
                    run[f"val_{k}"].log(v)
            logger.info(f"val metrics: {val_metrics}")
            logger.info(f"metrics:{aggregated_metrics}")

            if val_metrics["loss"] < best_val_loss:
                best_val_loss = val_metrics["loss"]
                logger.info("Saving best model")
                saved_training_state = jax.tree_util.tree_map(
                    lambda x: x[0], training_state
                )
                save_params(
                    saved_training_state,
                    os.path.join(args.results_dir, f"best_model_{step_num}.pkl"),
                )
    with open(f"{args.results_dir}/metrics.pkl", "wb") as handle:
        pkl.dump(aggregated_metrics, handle)
