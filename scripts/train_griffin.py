# type: ignore
import argparse
import functools
import logging
import os
import pickle
from typing import Dict

import haiku as hk
import jax
import jax.numpy as jnp
import optax
from datasets import load_dataset
from tqdm import tqdm
import itertools

from trix.dataloaders.language_models.sequence_datasets import SequencesDataset
from trix.tokenizers.language_models.bio import (
    FixedSizeNucleotidesKmersTokenizer,
    compute_dcnuc_tokens_to_ids,
)
from trix.models.griffin.model import build_Griffin_fn
from trix.models.griffin import common
from trix.training.language_trainers.decoder_causal_lm_trainer import DecoderCLMTrainer
from trix.utils.parameters import get_num_parameters


import time
#from absl import flags
#FLAGS = flags.FLAGS
#flags.DEFINE_integer("_KEY", 1241312, "Key to use for randomization.")
_KEY = 1241312
class DNADataset(SequencesDataset):
    def __init__(
        self,
        huggingface_dataset,
        tokenizer,
        batch_size,
        num_tokens_per_seq,
        shuffle=True,
        drop_last=True,
        seed=0,
        num_hosts=1,
        host_id=0,
        buffer_size=None,
    ):

        super().__init__(
            sequences=[],
            tokenizer=tokenizer,
            batch_size=batch_size,
            shuffle=shuffle,
            drop_last=drop_last,
            num_hosts=num_hosts,
            host_id=host_id,
        )

        # Internalize hyperparameters
        self._seed = seed
        self._num_tokens_per_seq = num_tokens_per_seq
        self._huggingface_dataset = huggingface_dataset
        self._buffer_size = (
            buffer_size if buffer_size is not None else num_tokens_per_seq * 1000
        )
        if shuffle:
            self._huggingface_dataset = self._huggingface_dataset.shuffle(
                seed=seed, buffer_size=self._buffer_size
            )

        self._batch_size = batch_size
        self._tokenizer = tokenizer

    def get_epoch_batches(self):

        batch_of_examples = []

        for example in self._huggingface_dataset:

            sequence = example["sequence"]
            if len(sequence) > self._num_tokens_per_seq - 2:
                sequence = sequence[: self._num_tokens_per_seq - 2]
            batch_of_examples.append(sequence)

            if len(batch_of_examples) == self._batch_size:

                batch_token_ids = self._tokenizer.batch_tokenize(batch_of_examples)

                if self._num_tokens_per_seq is not None:
                    batch_token_ids = self._tokenizer.pad_tokens_batch(batch_token_ids)
                batch_token_ids = [token_ids[1] for token_ids in batch_token_ids]

                yield batch_token_ids
                batch_of_examples = []


def get_parser_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    # Add our training arguments
    # TODO: Do I have to add more args?
    parser.add_argument(
        "--batch-size",
        type=int,
        help="num sequences per batch, "
        "to be adapted given the accelerators RAM available",
        default=1,
    )
    parser.add_argument(
        "--effective-batch-size",
        type=int,
        default=32,
        help="Effective batch size. Gradient accumulation "
        "will be determined accordingly.",
    )
    parser.add_argument(
        "--embed-dim",
        type=int,
        help="Encoder embedding size",
        default=5,
    )
    parser.add_argument(
        "--num-blocks",
        type=int,
        help="Number of griffin blocks",
        default=8,
    )
    parser.add_argument(
        "--num-kmers",
        type=int,
        help="Number of griffin blocks",
        default=6,
    )

    parser.add_argument(
        "--mlp-dim",
        type=int,
        help="Size of MLP embedding",
        default=1024,
    )
    parser.add_argument(
        "--max-seq-len",
        type=int,
        help="Maximum sequence length",
        default=66,
    )
    parser.add_argument(
        "--dataset-hf-path",
        type=str,
        default="InstaDeepAI/human_reference_genome",
        help="path to HuggingFace dataset",
    )
    parser.add_argument(
        "--total-number-tokens",
        type=int,
        help="Number of tokens seen during training",
        default=int(1e7),
    )
    parser.add_argument(
        "--validation-freq", type=int, help="In terms of steps.", default=1000
    )
    parser.add_argument(
        "--checkpoint-freq", type=int, help="In terms of steps.", default=2500
    )
    parser.add_argument(
        "--num-validation-steps",
        type=int,
        help="Number of steps to do during validation.",
        default=100,
    )
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--lr", type=float, default=3e-4)
    parser.add_argument("--num-warmup-steps", type=int, default=10)
    parser.add_argument("--result-folder", type=str, default="results/")
    parser.add_argument("--backend", type=str, default="tpu")
    parser.add_argument(
        "--streaming",
        type=bool,
        help="Whether to download the whole dataset at once (False)"
        "or by batches (True)",
        default=True,
    )
    parser.add_argument(
        "--buffer-size",
        type=int,
        help="Buffer size when loading the dataset with streaming",
        default=10000,
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    # Set default device to CPU
    jax.config.update("jax_platform_name", "cpu")

    # get jax devices
    args = get_parser_args()
    backend = args.backend
    devices = jax.devices(backend=backend)
    num_devices = len(devices)
    local_devices = jax.local_devices(backend=backend)
    num_local_devices = len(local_devices)
    print(
        f"Detected {num_local_devices} local devices "
        f"and {num_devices} global devices."
    )

    # get arguments
    seed = args.seed
    batch_size = args.batch_size
    effective_batch_size = args.effective_batch_size
    max_seq_len = args.max_seq_len
    total_number_tokens = args.total_number_tokens
    assert effective_batch_size % (batch_size * num_devices) == 0
    num_acc_grads = effective_batch_size // (batch_size * num_devices)
    effective_num_tokens_per_batch = effective_batch_size * max_seq_len
    total_num_steps = int(total_number_tokens / effective_num_tokens_per_batch)
    print(f"Gradient accumulation: {num_acc_grads}")
    print(f"Effective number of tokens per batch: {effective_num_tokens_per_batch}")

    # parameters following Llama recommendations
    init_learning_rate = args.lr
    end_learning_rate = 0.1 * init_learning_rate
    num_warmup_steps = args.num_warmup_steps
    beta1 = 0.9
    beta2 = 0.95
    weight_decay = 0.1

    # validation steps
    validation_freq = args.validation_freq
    num_validation_steps = args.num_validation_steps

    # model hyper-parameters
    model_embed_dim = args.embed_dim
    mlp_dim = args.mlp_dim
    num_blocks = args.num_blocks

    num_kmers = args.num_kmers

    tokenizer = FixedSizeNucleotidesKmersTokenizer(
            k_mers=num_kmers,
            fixed_length=max_seq_len,
            prepend_cls_token=True,
        )

    # get datasets
    # TODO: Is dataset management correct?
    dataset_path = args.dataset_hf_path
    streaming = args.streaming
    buffer_size = args.buffer_size

    train_dataset = DNADataset(
        huggingface_dataset=load_dataset(
            path=dataset_path,
            split="train",
            streaming=streaming,
            chunk_length=max_seq_len,
        ),
        tokenizer=tokenizer,
        batch_size=effective_batch_size,
        num_tokens_per_seq=max_seq_len,
        seed=seed,
        buffer_size=buffer_size,
        shuffle=True,
    )
    train_generator = train_dataset.get_epoch_batches()

    validation_dataset = DNADataset(
        huggingface_dataset=load_dataset(
            path=dataset_path,
            split="validation",
            streaming=streaming,
            chunk_length=max_seq_len,
        ),
        tokenizer=tokenizer,
        batch_size=effective_batch_size,
        num_tokens_per_seq=max_seq_len,
        seed=seed,
        buffer_size=buffer_size,
        shuffle=False,
    )

    pattern = (
        common.TemporalBlockType.RECURRENT,
        common.TemporalBlockType.RECURRENT,
        common.TemporalBlockType.ATTENTION,
    )

    random_key = jax.random.PRNGKey(_KEY)

    """
    model_config = common.GriffinConfig(
        vocab_size=len(tokenizer.vocabulary),
        width=2560,
        mlp_expanded_width=3 * 2560,
        num_heads=10,
        block_types=tuple(
            itertools.islice(itertools.cycle(pattern), num_blocks)),
        rng = random_key
    )
    """
    model_config = common.GriffinConfig(
        vocab_size=len(tokenizer.vocabulary),
        width=66,
        mlp_expanded_width=3 * 66,
        num_heads=1,
        block_types=tuple(
            itertools.islice(itertools.cycle(pattern), 1)),
        rng = random_key
    )

    # get haiku functions
    griffin_fn = build_Griffin_fn(model_config)
    griffin_fn = hk.transform(griffin_fn)

    init_fn = functools.partial(griffin_fn.init)
    apply_fn = functools.partial(griffin_fn.apply)

    # setup trainer
    # TODO: Everything looks good here? (schedule, optimizer, trainer)
    # total_num_steps = int(
    #     args.num_epochs * train_dataset.num_batches_per_epoch / num_acc_grads
    # )
    print(f"total_num_steps: {total_num_steps}")
    schedule = optax.warmup_cosine_decay_schedule(
        init_value=init_learning_rate,
        peak_value=init_learning_rate,
        warmup_steps=num_warmup_steps,
        decay_steps=total_num_steps,
        end_value=end_learning_rate,
    )
    optimizer_adamw = optax.adamw(
        learning_rate=schedule, b1=beta1, b2=beta2, weight_decay=weight_decay
    )
    optimizer = optax.MultiSteps(
        optax.chain(
            optax.clip(1.0),
            optimizer_adamw,
        ),
        every_k_schedule=num_acc_grads,
    )

    trainer = DecoderCLMTrainer(
        apply_fn=apply_fn,
        init_fn=init_fn,
        pad_token_id=tokenizer.pad_token_id,
        eos_token_id=tokenizer.eos_token_id,
        bos_token_id=tokenizer.bos_token_id,
        optimizer=optimizer,
    )

    # init model and optimizer
    init_tokens = next(train_generator)
    init_tokens = jnp.array(init_tokens)

    #random_key = jax.random.PRNGKey(seed)

    training_state = trainer.init(random_key=random_key, tokens=init_tokens)
    training_state = jax.device_put_replicated(training_state, local_devices)
    print(training_state)
    exit()
    init_step_num = 0
    
    #print(training_state)
    #print(training_state.params)
    #print(jax.tree_util.tree_map(lambda x: x[0], training_state.params))    
    
    num_params = get_num_parameters(jax.tree_util.tree_map(lambda x: x[0], training_state.params))
    print(f"Num parameters: {num_params}")

    checkpoint_freq = args.checkpoint_freq

    all_metrics: Dict = {
        "train_loss": [],
        "train_ppl": [],
        "num_tokens_seen": [],
        "learning_rate": [],
        "val_loss": [],
        "val_ppl": [],
    }

    # get mapped update and validate functions
    update_fn = jax.pmap(
        trainer.update, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    validation_fn = jax.pmap(
        trainer.compute_metrics, devices=devices, axis_name="batch"
    )

    # TODO: Check training loop
    for step_num in tqdm(
        range(init_step_num, total_num_steps),
        total=total_num_steps - init_step_num,
        desc="Training loop",
    ):
        train_perplexity = 0.0
        train_loss = 0.0
        #t = time.time()
        # Update step
        try:
            tokens_ids = next(train_generator)
        except StopIteration:
            train_generator = train_dataset.get_epoch_batches()
            tokens_ids = next(train_generator)
        tokens_ids = jnp.array(tokens_ids)

        sequence_mask = jnp.zeros_like(tokens_ids, dtype=tokens_ids.dtype)
        sequence_mask = sequence_mask.at[:, -2].set(1)

        tokens_ids = jnp.reshape(
            tokens_ids, newshape=(num_acc_grads, num_local_devices, batch_size, -1)
        )
        sequence_mask = jnp.reshape(
            sequence_mask, newshape=(num_acc_grads, num_local_devices, batch_size, -1)
        )

        for i in range(num_acc_grads):
            training_state, metrics = update_fn(
                training_state, tokens_ids[i], sequence_mask[i]
            )
            train_perplexity += float(metrics["perplexity"][0])
            train_loss += float(metrics["loss"][0])
        train_perplexity /= num_acc_grads
        train_loss /= num_acc_grads

        # Log metrics
        num_tokens_so_far = (step_num + 1) * effective_batch_size * max_seq_len
        current_lr = schedule(step_num)

        print(
            f"Step {step_num} | Training loss {train_loss} ; "
            f"Training perplexity {train_perplexity}"
        )
        
        all_metrics["train_loss"].append(train_loss)
        all_metrics["train_ppl"].append(train_perplexity)
        all_metrics["num_tokens_seen"].append(num_tokens_so_far)
        all_metrics["learning_rate"].append(current_lr)

        if step_num % validation_freq == 0 and step_num > 0:

            # validation loop
            validation_batch_generator = validation_dataset.get_epoch_batches()
            validation_perplexities = []
            validation_losses = []
            for _ in tqdm(
                range(num_validation_steps),
                total=num_validation_steps,
                desc="Validation loop",
            ):
                try:
                    tokens_ids = next(validation_batch_generator)
                except StopIteration:
                    tokens_ids = next(validation_batch_generator)
                    validation_batch_generator = validation_dataset.get_epoch_batches()
                tokens_ids = jnp.array(tokens_ids)
                sequence_mask = jnp.zeros_like(tokens_ids, dtype=tokens_ids.dtype)
                sequence_mask = sequence_mask.at[:, -2].set(1)
                tokens_ids = jnp.reshape(
                    tokens_ids,
                    newshape=(num_acc_grads, num_local_devices, batch_size, -1),
                )
                sequence_mask = jnp.reshape(
                    sequence_mask,
                    newshape=(num_acc_grads, num_local_devices, batch_size, -1),
                )

                validation_perplexity = 0.0
                validation_loss = 0.0
                for i in range(num_acc_grads):
                    metrics = validation_fn(
                        training_state, tokens_ids[i], sequence_mask[i]
                    )
                    validation_perplexity += float(metrics["perplexity"][0])
                    validation_loss += float(metrics["loss"][0])
                validation_perplexity /= num_acc_grads
                validation_loss /= num_acc_grads
                validation_perplexities.append(validation_perplexity)
                validation_losses.append(validation_loss)

            # Compute validation metrics and log
            validation_perplexity = jnp.mean(jnp.asarray(validation_perplexities))
            validation_loss = jnp.mean(jnp.asarray(validation_losses))

            print(
                f"Step {step_num} | Validation loss {validation_loss} ; "
                f"Validation accuracy {validation_perplexity}"
            )
            all_metrics["val_loss"].append(validation_loss)
            all_metrics["val_ppl"].append(validation_perplexity)

    os.makedirs(args.result_folder, exist_ok=True)
    with open(f"{args.result_folder}/metrics.pkl", "wb") as handle:
        pickle.dump(all_metrics, handle)
