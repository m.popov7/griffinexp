import argparse
import functools
import os
import pickle
from dataclasses import asdict
from typing import Dict

import haiku as hk
import jax
import jax.numpy as jnp
import neptune.new as neptune
import optax
from tqdm import tqdm
from transformers import AutoTokenizer

from trix.dataloaders.language_models.huggingface_datasets import HFTextDataset
from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.training.language_trainers.decoder_causal_lm_trainer import DecoderCLMTrainer
from trix.utils.parameters import get_num_parameters


def get_parser_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    # Add our training arguments
    parser.add_argument(
        "--batch-size",
        type=int,
        help="num sequences per batch, "
        "to be adapted given the accelerators RAM available",
        default=4,
    )
    parser.add_argument(
        "--block-size",
        type=int,
        help="length of the sequences used to train the model",
        default=2048,
    )
    parser.add_argument(
        "--num-layers", type=int, help="Number of attention layers", default=6
    )
    parser.add_argument(
        "--embed-dim", type=int, help="Encoder Embedding size.", default=1024
    )
    parser.add_argument(
        "--ffn-embed-dim",
        type=int,
        help="Feed Forward Network Embedding size.",
        default=4096,
    )
    parser.add_argument("--num-tokens-per-batch", type=int, default=int(4e6))
    parser.add_argument(
        "--total-number-tokens",
        type=int,
        help="Total number of tokens to see during training.",
        default=int(3e11),
    )
    parser.add_argument(
        "--validation-freq", type=int, help="In terms of steps.", default=1000
    )
    parser.add_argument(
        "--checkpoint-freq", type=int, help="In terms of steps.", default=2500
    )
    parser.add_argument(
        "--num-validation-steps",
        type=int,
        help="Number of steps to do during validation.",
        default=100,
    )
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--lr", type=float, default=3e-4)
    parser.add_argument("--num-warmup-steps", type=int, default=2000)
    parser.add_argument("--neptune-project", type=str)
    parser.add_argument("--result-folder", type=str, default="results/")
    args = parser.parse_args()
    return args


if __name__ == "__main__":

    # set default device to CPU
    jax.config.update("jax_platform_name", "cpu")

    # get jax devices
    devices = jax.devices(backend="tpu")
    num_devices = len(devices)
    local_devices = jax.local_devices(backend="tpu")
    num_local_devices = len(local_devices)
    print(f"Detected devices: {devices}")

    # get arguments
    args = get_parser_args()
    neptune_project = args.neptune_project
    seed = args.seed
    block_size = args.block_size
    batch_size = args.batch_size
    num_acc_grads = int(
        args.num_tokens_per_batch / (batch_size * num_devices * block_size)
    )

    # parameters following Llama recommendations
    init_learning_rate = args.lr
    end_learning_rate = 0.1 * init_learning_rate
    num_warmup_steps = args.num_warmup_steps
    beta1 = 0.9
    beta2 = 0.95
    weight_decay = 0.1

    # total number of tokens to see during training
    total_number_tokens = args.total_number_tokens

    effective_batch_size = batch_size * num_devices * num_acc_grads
    effective_num_tokens_per_batch = effective_batch_size * block_size
    total_num_steps = int(total_number_tokens / effective_num_tokens_per_batch)

    print(f"Effective number of tokens per batch: {effective_num_tokens_per_batch}")
    print(f"Total num steps to see {total_number_tokens} tokens: {total_num_steps}")

    # validation steps
    validation_freq = args.validation_freq
    num_validation_steps = args.num_validation_steps

    # model hyper-params
    embed_dim = args.embed_dim
    ffn_embed_dim = args.ffn_embed_dim
    num_heads = 16
    num_layers = args.num_layers
    rope_dimensions = 8
    max_position_embeddings = block_size
    add_bias_ffn = False
    ffn_activation_name = "swish"
    use_glu_in_ffn = True

    # get tokenizer
    tokenizer_name = "EleutherAI/gpt-j-6B"
    tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
    # TODO: quick fix for now but to me removed
    tokenizer.pad_token = tokenizer.eos_token
    tokenizer.pad_token_id = tokenizer.eos_token_id

    # get datasets
    train_dataset = HFTextDataset(
        dataset_name="EleutherAI/pile",
        split="train",
        tokenizer=tokenizer,
        batch_size=batch_size * num_local_devices,
        tokenized_sequence_length=block_size,
        shuffle=True,
        drop_last=True,
        seed=seed,
    )
    batch_generator = train_dataset.get_iterator()

    validation_dataset = HFTextDataset(
        dataset_name="EleutherAI/pile",
        split="validation",
        tokenizer=tokenizer,
        batch_size=batch_size * num_local_devices,
        tokenized_sequence_length=block_size,
        shuffle=False,
        drop_last=True,
        seed=seed,
    )
    # create config
    model_config = GptConfig(
        vocab_size=tokenizer.vocab_size,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=embed_dim,
        ffn_embed_dim=ffn_embed_dim,
        num_heads=num_heads,
        num_layers=num_layers,
        rope_dimensions=rope_dimensions,
        max_position_embeddings=max_position_embeddings,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=False,
        use_gradient_checkpointing=False,
        norm_type="layer_norm",
        parallel_attention_ff=False,
    )

    # get haiku functions
    gptj_fn = build_gpt_fn(model_config)
    gptj_fn = hk.transform(gptj_fn)

    init_fn = functools.partial(gptj_fn.init)
    apply_fn = functools.partial(gptj_fn.apply)

    # setup trainer
    schedule = optax.warmup_cosine_decay_schedule(
        init_value=init_learning_rate,
        peak_value=init_learning_rate,
        warmup_steps=num_warmup_steps,
        decay_steps=total_num_steps,
        end_value=end_learning_rate,
    )
    optimizer_adamw = optax.adamw(
        learning_rate=schedule, b1=beta1, b2=beta2, weight_decay=weight_decay
    )
    optimizer = optax.MultiSteps(
        optax.chain(
            optax.clip(1.0),
            optimizer_adamw,
        ),
        every_k_schedule=num_acc_grads,
    )

    trainer = DecoderCLMTrainer(
        apply_fn=apply_fn,
        init_fn=init_fn,
        pad_token_id=tokenizer.pad_token_id,
        eos_token_id=tokenizer.eos_token_id,
        bos_token_id=tokenizer.bos_token_id,
        optimizer=optimizer,
    )

    # init model and optimizer
    init_tokens = next(batch_generator)
    random_key = jax.random.PRNGKey(seed)
    training_state = trainer.init(random_key=random_key, tokens=init_tokens[:1, :1])
    training_state = jax.device_put_replicated(training_state, local_devices)
    init_step_num = 0

    num_params = get_num_parameters(jax.tree_map(lambda x: x[0], training_state.params))
    print(f"Num parameters: {num_params}")

    # create Neptune logger
    if args.neptune_project:
        neptune_run = neptune.init(
            project=neptune_project,
            tags=["decoder", "non_sharded", f"num_params: {num_params}"],
        )
        run_parameters = vars(args)
        # precision problem in Neptune
        run_parameters["total_number_tokens"] = f"{total_number_tokens:.2e}"

        # update neptune params
        run_parameters.update(
            {
                "end_learning_rate": end_learning_rate,
                "beta1": 0.9,
                "beta2": 0.95,
                "weight_decay": 0.1,
                "effective_num_tokens_per_batch": effective_num_tokens_per_batch,
                "total_num_steps": total_num_steps,
                "num_params": f"{num_params:.2e}",
                "tokenizer_name": tokenizer_name,
            }
        )
        run_parameters.update(asdict(model_config))
        neptune_run["run_parameters"] = run_parameters

    checkpoint_freq = args.checkpoint_freq

    all_metrics: Dict = {
        "train_loss": [],
        "train_ppl": [],
        "num_tokens_seen": [],
        "learning_rate": [],
        "val_loss": [],
        "val_ppl": [],
    }

    # get pmapped update and validate functions
    update_fn = jax.pmap(
        trainer.update, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    validation_fn = jax.pmap(
        trainer.compute_metrics, devices=devices, axis_name="batch"
    )

    for step_num in tqdm(
        range(init_step_num, total_num_steps),
        total=total_num_steps - init_step_num,
        desc="Training loop",
    ):
        train_perplexity = 0.0
        train_loss = 0.0
        for _ in range(num_acc_grads):
            # Update step
            tokens_ids = next(batch_generator)
            tokens_ids = jnp.reshape(
                tokens_ids, newshape=(num_local_devices, batch_size, -1)
            )
            training_state, metrics = update_fn(training_state, tokens_ids)
            train_perplexity += float(metrics["perplexity"][0])
            train_loss += float(metrics["loss"][0])
        train_perplexity /= num_acc_grads
        train_loss /= num_acc_grads

        # Log metrics
        num_tokens_so_far = (step_num + 1) * effective_num_tokens_per_batch
        current_lr = schedule(step_num)

        print(
            f"Step {step_num} | Training loss {train_loss} ; "
            f"Training perplexity {train_perplexity}"
        )
        all_metrics["train_loss"].append(train_loss)
        all_metrics["train_ppl"].append(train_perplexity)
        all_metrics["num_tokens_seen"].append(num_tokens_so_far)
        all_metrics["learning_rate"].append(current_lr)

        if args.neptune_project:
            neptune_run["train/loss"].log(float(train_loss))
            neptune_run["train/ppl"].log(float(train_perplexity))
            neptune_run["num_tokens_seen"].log(num_tokens_so_far)
            neptune_run["learning_rate"].log(current_lr)

        if step_num % validation_freq == 0 and step_num > 0:

            # validation loop
            validation_batch_generator = validation_dataset.get_iterator()
            validation_perplexities = []
            validation_losses = []
            for _ in tqdm(
                range(num_validation_steps),
                total=num_validation_steps,
                desc="Validation loop",
            ):
                tokens_ids = next(validation_batch_generator)
                tokens_ids = jnp.reshape(
                    tokens_ids, newshape=(num_local_devices, batch_size, -1)
                )
                metrics = validation_fn(training_state, tokens_ids)
                validation_perplexities.append(float(metrics["perplexity"][0]))
                validation_losses.append(float(metrics["loss"][0]))

            # Compute validation metrics and log
            validation_perplexity = jnp.mean(jnp.asarray(validation_perplexities))
            validation_loss = jnp.mean(jnp.asarray(validation_losses))

            print(
                f"Step {step_num} | Validation loss {validation_loss} ; "
                f"Validation accuracy {validation_perplexity}"
            )
            all_metrics["val_loss"].append(validation_loss)
            all_metrics["val_ppl"].append(validation_perplexity)

            if args.neptune_project:
                neptune_run["validation/loss"].log(float(validation_loss))
                neptune_run["validation/ppl"].log(float(validation_perplexity))

    os.makedirs(args.result_folder, exist_ok=True)
    with open(f"{args.result_folder}/metrics.pkl", "wb") as handle:
        pickle.dump(all_metrics, handle)
