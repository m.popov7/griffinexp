# Language Model Terminology in Trix

Trix provides a set of utility functions as well as pre-built examples to implement many flavors of language models.
Architecture names may sometimes vary from one paper or group to another.
In this page, we aim to clarify the notations we use in the Trix repository to make sure we are all on the same page!

Trix provides utilities for users to implement any of their favourite transformer architectures. The library natively
provides several ready-to-use architectures that can be classified into 3 main categories: encoder-only, decoder-only
and encoder-decoder models. Trix also provides utils and trainer classes to train these architectures at scale.

The advised default architecture is based on ESM (1), a large language model for proteins. It is composed of
an embedding layer with embedding dimension of 1280, a learned positional embedding layer also of dimension
1280, a series of  24 attention blocks with each block having 20 attention , and a simple network head.
Also note that within each attention block layer normalization layers precede the attention mechanism/feed forward network,
the hidden dimension of the feed forward network is 5120, and Gelu activations are used.

<img src="../img/transformers_terminology.png" alt="Language Models Naming" />


## The Encoder-Only architecture

The encoder-only architecture, introduced in BERT, is composed of an embedding layer, a stack of transformer blocks (using only self-attention)
and a final language model head. It takes as input a sequence of tokens (referred in the image as query), converts it into
a sequence of embeddings that is then processed sequentially by each transformer block. The final layer transforms
the sequence of embeddings into a sequence of probabilities over the considered vocabulary.

The blocks in the encoder-only use full-attention (i.e. non causal)
which means that the information in the sequences can flow in both directions. Therefore, this architecture is typically
trained with masked language-modeling: tokens in the input sequence are replaced randomly by mask tokens and the network
is trained to maximize the probability of the original token at the masked positions through a cross-entropy loss objective.

An example of such architecture is based on ESM, a large language model for proteins.

#### [Model Config](/trix/api/models/esm/model/#trix.models.esm.model.ESMTransformerConfig)
#### [Model Class](/trix/api/models/esm/model/#trix.models.transformer.ESMTransformer)
#### [Encoder Masked Language Trainer](/trix/api/training/trainer/#trix.training.trainer.EncoderMLMTrainer)


## The Decoder-Only architecture

Similar to the encoder-only architecture, the decoder-only is composed of an embedding layer, a stack of transformer blocks (using only self-attention)
and a final language model head. However, this architecture used for instance in GPT differs from the encoder-only as its transformer blocks
rely on causal attention in which each element in the sequence only attends to its predecessors in the sequence. This architecture
allows the information to flow only from left to right. Thus, it is well-suited for next-word-prediction.

#### [Model Config](/trix/api/models/esm/model/#trix.models.transformer.ESMTransformerConfig)
#### [Model Class](/trix/api/models/esm/model/#trix.models.transformer.ESMTransformer)
#### [Decoder Causal Language Trainer](/trix/api/training/trainer/#trix.training.trainer.DecoderCLMTrainer)


## Encoder-Decoder architecture

The Encoder-Decoder model roughly follows the architecture introduced in the famous "Attention is All You Need" paper. The Encoder component
is composed of an embedding layer and a stack of transformer blocks using self-attention mechanisms. Full attention masks allowing for attention in both
direction are used. Notice that there is no language model head added to Encoder as it is in the Encoder-Only model.

The Decoder component is composed of an embedding layer, a stack of transformer blocks using both cross-attention and self-attention, and a final language model head. Causal attention masks
are used in the self-attention mechanisms and an optional,user specified cross-attention mask is used in the cross-attention layer. Notice that the Decoder-Only
model's transformers block only use self-attention.

The Encoder first ingests a set of tokens and outputs sequences embeddings. Next the Decoder ingests a separate set of tokens. The output sequence embeddings from
the Encoder are passed to every transformer block of the Decoder and used as the key and value in the cross-attention mechanism.

Important note for the Decoder in both the Decoder-Only and Encoder-Decoder models are that during training the Decoder uses "teacher forcing" which provides the model
with the correct input for timestep t + 1. However, during inference this is not known and the prediction must be done in an autoregressive fashion where
the prediction for timestep t must be fed as the input for prediction at timestep t+1.

#### [Model Config](/trix/api/models/encoder_decoder/encoder/#trix.models.encoder_decoder.encoder.EncoderConfig)
#### [Model Class](/trix/api/models/encoder_decoder/model/#trix.models.encoder_decoder.encoder_decoder.EncoderDecoder)
#### [Encoder Decoder T5 Trainer](/trix/api/training/trainer/#trix.training.trainer.EncoderDecoderCausalTrainer)
