# Training Large Language Models with the ShardedTransformer

As larger models become more popular, training them becomes more and more challenging. In NLP, recent successful models such as T5 or Alexa-TM
have more than 10 billion parameters. It means that it is impossible to train them (and sometimes even infer through them) on a single
accelerator (GPU or TPU). Hence, being able to shard the parameters of a model and train it is crucial to reach cutting edge results
using Transformers. TEST

## A quick glossary on parallelism

Training a model on several devices can lead to two main parallelism paradigms:

- **Data Parallelism**
- **Model Parallelism**

### Data Parallelism

If the number of devices is large enough, the model can be replicated
on groups of devices (or even one device), such that a large batch of data can be split into
smaller batches that are sent to each group of devices.

<center> <img src="../img/data_parallelism.png" alt="Data Parallelism" width="600"/> </center>

During training, only gradients need to be synchronized by averaging them over each group of devices.

### Model Parallelism

If the model is too large to train on one device, it is necessary to split its parameters
across devices. This can be done in several ways. The simplest way to "shard" a model
would be to split the model sequentially but this would clearly be suboptimal as most
devices would have "wait" for the others to finish computations. Our implementation uses
the fact that matrix multiplications can be parallelized across devices with some synchronization
operations in between.

<center> <img src="../img/model_parallelism.png" alt="Model Parallelism" width="600"/> </center>

### Model and Data Parallelism

Of course both paradigms can be combined. But one must be aware that model parallelism induces more inter-device communication than data-parallelism ( where there is a communication of gradients only at the end of a batch ). Therefore it is better to shard the model over the minimum number of devices, even if it means working with mini batch size of 1.

<center> <img src="../img/data_and_model_parallelism.png" alt="Data & Model Parallelism" width="600"/> </center>

## Sharding a Transformer: a step by step guide

To illustrate how a Transformer can be sharded, we give detailed explanation in the
case where we want to shard our Transformer over 2 devices without data parallelism.
This means the same batch of data will be transferred to each device where operations
will run in parallel.

<center> <img src="../img/illustrated_sharded_transformer.png" alt="Sharded Transformer on 2 devices" width="300"/> </center>

### Embeddings

The first two layers are the token embedding layer and the positional embedding layer.

```python
...
    x = ReplicatedEmbed(
        alphabet_size, embed_dim
    )(tokens)
    x = x + ReplicatedLearnedPositionalEmbeddings(
        l, embed_dim
    )(x)
```

Both of those operations are **completely identical in both shards**: the embedding matrices
are synchronized between shards such that the embedded sequences are the same after those
two operations.


### Sharded Multi-Head Attention

In Multi-Head Attention, **operations on different heads are completely independent from each other**.
We take advantage of this feature to shard a `MultiHeadAttention` layer across attention heads.

Namely, for an input $x \in \mathbb{R}^{l \times d}$ where $l$ is the sequence length and
$d$ the input dimension, a self attention layer with $h$ attention heads can be sharded
onto $2$ devices by computing every head specific operation on one device for $h//2$ heads.

<center> <img src="../img/sharded_attention.png" alt="Sharded Multi-Head Attention" width="600"/> </center>

The results of those operations **need to be synchronized** in a final `LinearLayer`. A simple
way to do it would be to communicate the outputs of the heads, concatenate them and use
a replicated linear layer (with synchronized weights). Instead, we reduce the communication
using the `LinearAndPSum` layer.


#### LinearAndPSum

In practice, this operation overrides a `LinearLayer` to synchronize intermediate results from each shard.

Following the previous example on Sharded MHA, each input of the linear layer can be
written as $x_{1,2} \in \mathbb{R}^{l \times \frac{\tilde{d}}{2}}$ where $\tilde{d}=k*h$
is the dimension of the output of the attention.

Writing $x = [x_1, x_2] \in \mathbb{R}^{l \times \tilde{d}}$, in an unsharded transformer, the output of a `LinearLayer` would be written as $y = xW^T + b$ where

- $y \in \mathbb{R}^{l \times e}$ is the output
- $W \in \mathbb{R}^{e \times \tilde{d}}$ is the weight matrix
- $b \in \mathbb{R}^{e}$ is the bias.

An equivalent formulation is to split the weight matrix into two submatrices $W_{1,2} \in \mathbb{R}^{e/2 \times \tilde{d}}$
such that $W = [W_1, W_2 ]$. Then, on each shard the following operations are computed:

- **Linear**: $\tilde{y_{1, 2}} = x_{1, 2}W_{1, 2}^T$
- **PSum**: $y_1 = \tilde{y_1} + \tilde{y_2}$ ;  $y_2 = \tilde{y_1} + \tilde{y_2}$
- **Add Bias**: $y_{1, 2} = y_{1, 2} + b = y$

The bias is always synchronized between shards. Note that during training and backward pass,
gradients of the weights $W_1$ and $W_2$ are also psummed.

<center> <img src="../img/linear_and_psum.png" alt="Linear And PSum" width="600"/> </center>


### Sharded MLP

Using `LinearAndPSum` it is also straightforward to shard Multi-Layer Perceptrons. Let us
assume that we want to shard a two layer MLP where the first linear layer has a hidden
dimension $\tilde{e}$ and the second a dimension $e$. Then, for an input $x \in \mathbb{R}^{l \times d}$,
the first layer can be sharded by splitting according to the output dimension (each shard
handles the computation of $\tilde{e} / 2$ elements) while the second layer synchronizes
those intermediate results using `LinearAndPSum`.

<center> <img src="../img/sharded_mlp.png" alt="Sharded MLP" width="400"/> </center>


### Sharded Transformer

Piling up those layers allow to create a Sharded Transformer. We could reduce the number
of parameters per shard even more by sharding some replicated layers such as embedding
layers. As of now, they are a neglectable part of the model parameters, especially for
large models with a large number of attention heads.


<center> <img src="../img/sharded_transformer.png" alt="Sharded Transformer" width="300"/> </center>


## Sharding a Transformer in Trix

We provide utilities to shard both the ESM and Encoder-Decoder architectures in Trix,
both for training and inference.

#### [Model Class](/trix/api/models/esm/sharding/sharded_esm.ShardedESMTransformer)
#### [Forward Function](/trix/api/models/esm/sharding/sharded_esm.build_sharded_esm_fn)

### Example Notebook

An example notebook detailing how to train a sharded transformer with a BERT objective
is available in the folder `notebooks/example_sharded_training_uniref.ipynb`.

### Code Snippet Example

Find below an code snippet on to instantiate a sharded transformer and use it for inference on fake data.

```python

import haiku as hk
import jax
import jax.numpy as jnp

from trix.models.esm.sharding.sharded_esm import (
    ESMTransformerConfig,
    build_sharded_esm_fn,
)
from trix.pretrained.esm import get_pretrained_esm_model
from trix.models.esm.sharding.utils.sharding import shard_esm

alphabet_size = 10
batch_size = 16
embed_dim = 512
max_positions = 40
num_shards = 4

# Setup the model
model_config = ESMTransformerConfig(
    alphabet_size=tokenizer.vocabulary_size,
    max_positions=max_length + int(prepend_cls_token),
    attention_heads=16,
    embed_dim=1024,
    ffn_embed_dim=2048,
    num_layers=6,
    pad_token_id=tokenizer.pad_token_id,
    mask_token_id=tokenizer.mask_token_id,
    positional_embedding=None,
    use_rotary_embedding=True
)

parameters, forward_fn, tokenizer, config = get_pretrained_esm_model(
    model_name="esm2_t36_3B_UR50D"
)

sharded_esm_fn = build_sharded_transformer_fn(model_config, num_shards=num_shards)
sharded_esm_fn = hk.transform(sharded_esm_fn)

sharded_parameters = shard_esm(parameters)

random_key = jax.random.PRNGKey(0)
random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
random_keys = jnp.array(random_keys)

tokens = jax.random.choice(random_key, 10, shape=(batch_size, max_positions))
sharded_tokens = jnp.tile(tokens, (num_shards, 1, 1))

# The init and apply function expect to be pmapped or xmapped over an axis "shard"

outs = jax.pmap(transformer_fn.apply, axis_name="shard")(
    sharded_parameters, random_keys, sharded_tokens
)
```
