# Natural Language Processing in Trix

Trix offers several pre-trained Transformers for Natural Language Processing (NLP).
All of them are translated into Haiku / Jax and can be used with other Trix utilities.
Tokenizers are based on [*sentencepiece*](https://github.com/google/sentencepiece)

## T5

T5 ([Text-to-Text Transfer Transformer](https://arxiv.org/pdf/1910.10683.pdf)
is an encoder-decoder model that can frame pre-training and fine-tuning as a text-to-text
prediction task.  Thus, the input to the encoder is a sequence of tokens corresponding to
natural language, and the resulting embeddings are used by the decoder to produce a natural
language response.  T5 was primarily pretrained using masked language modeling on the C4
dataset scraped from the web and fine-tuned on a number of downstream tasks such as
language translation and paragraph summarization.

The original T5 model was released with 5 sizes, consisting of 60M, 220M, 770M, 3B,
and 11B parameters.  Trix currently supports the first three of those models.  An
updated version of T5 (1.1) was later released with slight modifications to the
architecture.  This iteration was then used to create the
[Flan-T5 model](https://arxiv.org/pdf/2210.11416.pdf), which outperforms the original
T5 on a wide variety of tasks.  Flan-T5 was also released with 5 sizes, and Trix also
supports the first three of these.

To load an original T5 model into a Haiku function, use the following code snippet:
```python
    parameters, forward_fn, tokenizer, config = get_pretrained_t5_model('t5-small')
    forward_fn = hk.transform(forward_fn)
```
For a Flan-T5 model, simply change this to
```python
    parameters, forward_fn, tokenizer, config = get_pretrained_t5_model('flan-t5-small')
    forward_fn = hk.transform(forward_fn)
```
In either case, it is also possible to load only the encoder by adding an argument:
```python
    parameters, forward_fn, tokenizer, config = get_pretrained_t5_model(
	't5-small', encoder_only = True
    )
    forward_fn = hk.transform(forward_fn)
```

Once the model is loaded, it can be used for inference (generation) in the following way:
```python
    sentence = ['translate English to German: The house is wonderful.']

    # Tokenize them (using sentencepiece)
    input_ids = tokenizer(
        sentences,
        padding="longest",
        truncation=True,
        return_tensors="np"
    )['input_ids']

    # Apply the model
    random_key = jax.random.PRNGKey(0)
    outputs = forward_fn.apply(
    	parameters,
    	random_key,
    	enc_token_ids=input_ids,
    	dec_token_ids=jnp.array([[tokenizer.pad_token_id]] * len(sentences)),
    	drop_rate=0.,
    	is_inference=True,
    	output_len=50
	)
    output_sequences = tokenizer.batch_decode(
		outputs['output_sequence'], skip_special_tokens=True
	)
```

## GPT-J

GPT-J 6B is a transformer model trained using Ben Wang's
[Mesh Transformer JAX](https://github.com/kingoflolz/mesh-transformer-jax/).
It is a decoder-only model that can be used to generate text.
It has been pre-trained on [The pile](https://huggingface.co/datasets/the_pile), an open-source NLP dataset created by gathering several high-quality datasets and comprising 800GiB of text data.
The architecture is decoder only  (causal attention), rotary positional embeddings and an efficient attention scheme: parallel attention (more details in [GPT-NeoX-20B paper](https://arxiv.org/abs/2204.06745)). It also uses a Byte-Pair Encoding (BPE) tokenizer trained on the pile, plus a special handling of whitespaces.

A pretrained model with 6B parameters is available. The weights have been released in full precision (ie float32) and weighs 24GB.

Note: when finetuned, this model model gives very competitive performance with GPT models of comparable sizes, but it seems to be more performant on knowledge-based and mathematical tasks.

Disclaimer: GPT-J has not been fine-tuned, so this model needs finetuning to produce satisfying prompts.


To load an original GPTJ model into a Haiku function, use the following code snippet:
```python
parameters, forward_fn, tokenizer, config = get_pretrained_gptj_model()
forward_fn = hk.transform(forward_fn)
```

Once the model is loaded, it can be used for inference (generation) in the following way:
```python

prompts = ["Dear Ann,", "Sorry I'm"]
inputs = tokenizer(prompts, return_tensors="np", padding=True)['input_ids']
output_sequences = forward_fn.apply(parameters,
                   jax.random.PRNGKey(42),
                   token_ids=inputs,
                   is_inference=True,
                   output_len=20, )["output_sequence"]
answers = tokenizer.batch_decode(output_sequences)
for prompt, answer in zip(prompts, answers):
    print(prompt + answer)

```

When using greedy decoding, the generated sequences tend to repeat the same text such as: "Sorry I'm late. Sorry I'm late. Sorry I'm late. ...". Using temperature sampling mitigates this effect:

(Higher temperature yields more variety)
```python
prompts = ["Dear Ann,", "Sorry I'm"]
inputs = tokenizer(prompts, return_tensors="np", padding=True)['input_ids']
output_sequences = forward_fn.apply(parameters,
                   jax.random.PRNGKey(42),
                   token_ids=inputs,
                   is_inference=True,
                   output_len=20,
                   temperature_sampling=True,
                   temperature=0.5)["output_sequence"]
answers = tokenizer.batch_decode(output_sequences)
for prompt, answer in zip(prompts, answers):
    print(prompt + answer)
```
