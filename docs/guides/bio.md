# Unleash the power of Transformers in Biology with Trix

## Pre-trained Protein Language Models

### ESM

The ESM models (see https://github.com/facebookresearch/esm for more information) , released by Meta AI Research team, are a collection of state-of-the-art protein language models originally developed in PyTorch. Trix offers support for you to play with these models in Jax.

Users can easily get pre-trained weights for these models and either use them for inference or fine-tune them easily. This code uses Meta support to download the PyTorch checkpoints locally on your machine. Then, when calling the function the PyTorch weights are translated in real time into Jax weights.

The supported models are listed in the table below:

| Model Name                 | Number Parameters | Architecture Tyoe |
|----------------------------|-------------------|:-----------------:|
| esm1_t12_85M_UR50S         | 85M               |       ESM 1       |
| esm1_t34_670M_UR100        | 670M              |       ESM 1       |
| esm1_t34_670M_UR50D        | 670M              |       ESM 1       |
| esm1_t34_670M_UR50S        | 670M              |       ESM 1       |
| esm1_t6_43M_UR50S          | 43M               |       ESM 1       |
| esm1b_t33_650M_UR50S       | 650M              |      ESM 1b       |
| esm2_t6_8M_UR50D           | 8M                |       ESM 2       |
| esm2_t12_35M_UR50D         | 35M               |       ESM 2       |
| :star: esm2_t30_150M_UR50D | 150M              |       ESM 2       |
| esm2_t33_650M_UR50D        | 650M              |       ESM 2       |
| esm2_t36_3B_UR50D          | 3B                |       ESM 2       |
| esm2_t48_15B_UR50D         | 15B               |       ESM 2       |

As of now, we found the best trade-off between model size and representation quality is *esm2_t30_150M_UR50D*.

#### Code Example

Find below an example on how to easily load such a model and use it for inference.
```python
import haiku as hk
import jax
import jax.numpy as jnp

from trix.pretrained.esm import get_pretrained_esm_model

# Get model
parameters, forward_fn, tokenizer, config = get_pretrained_esm_model(
    model_name='esm2_t30_150M_UR50D',
    # collect embeddings at layer 28, None to not save any embeddings
    embeddings_layers_to_save=(28,),
)
forward_fn = hk.transform(forward_fn)

# Get data and tokenize it
sequences = ['LKMFP', 'HKLFAAAE', 'QEGCHI']
tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

# Initialize random key
random_key = jax.random.PRNGKey(0)

# Perform inference
outs = forward_fn.apply(parameters, random_key, tokens)
print(outs['logits'].shape, outs['embeddings_28'].shape)

```
#### Model Sharding

Every model with less than a billion parameters should run on any modern device. Two
models released with ESM-2 have 3 and 15 billion parameters respectively (*esm2_t36_3B_UR50D*
and *esm2_t48_15B_UR50D*).
For those models, we advise to shard the model as user will most
likely face OOM issues.

#### Code Example

For larger models, it is possible to shard them easily as described below.

```python
import haiku as hk
import jax
import jax.numpy as jnp

from trix.pretrained.esm import get_pretrained_esm_model
from trix.models.esm.sharding.utils.sharding import shard_esm
from trix.models.esm.sharding.sharded_esm import ShardedESMTransformer

# Get model
parameters, forward_fn, tokenizer, config = get_pretrained_esm_model(
    model_name='esm2_t48_15B_UR50D',
    # collect embeddings at layer 48, None to not save any embeddings
    embeddings_layers_to_save=(48,),
)

num_shards = 2 # can be changed if more devices are required or available
def sharded_forward_fn(tokens):
    model = ShardedESMTransformer(
        config=config,
        num_shards=num_shards ,
        model_name="'esm2_t48_15B_UR50D" # must match the one of the unsharded model
    )
    return model(tokens)
sharded_forward_fn = hk.transform(sharded_forward_fn)
sharded_params = shard_esm(parameters, num_shards=num_shards)

# Get data and tokenize it
sequences = ['LKMFP', 'HKLFAAAE', 'QEGCHI']
tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
# Duplicate the tokens num_shards times
tokens = jnp.tile(jnp.asarray(tokens_ids, dtype=jnp.int32), (num_shards, 1, 1))

# Initialize num_shard random keys
random_key = jax.random.PRNGKey(0)
random_keys = jax.random.split(random_key, num=num_shards)

# Perform inference
outs = jax.pmap(forward_fn.apply, axis_name='shard')(
    sharded_params, random_key, tokens
)

# You will notice outs are duplicated num_shards times
print(outs['logits'].shape, outs['embeddings_48'].shape)

```

### Ankh

[Ankh](https://arxiv.org/abs/2301.06568) is a protein encoder-decoder language model.
The goal of the Ankh project was to reduce model size by optimizing the hyperparameters of
the architecture.  Thus, Ankh is significantly smaller than most protein-folding models of
similar accuracy.  The full encoder-decoder model is used to generate new protein
sequences by masking an input sequence.  The encoder portion of the model can also
be utilized separately to analyze various characteristics of an input protein
sequence such as solubility, fluorescence, and secondary structure.  However,
the encoder model must be combined with a ConvBertLayer and fine-tuned
before it can be used for these tasks.

Ankh was released with two sizes, Ankh-base and Ankh-large, both of which are supported
in Trix. To load the Ankh encoder-decoder into a Haiku function, use the following code
snippet:
```python
    parameters, enc_dec_fn, tokenizer, config = get_pretrained_ankh_model(
	base_or_large='base', encoder_only=False)
    enc_dec_fn = hk.transform(enc_dec_fn)
```
The model can be used to generate sequences with the following script:
```python
    def get_n_mask_tokens(n):
	return [f"<extra_id_{i}>" for i in range(n)]
    def append_n_mask_tokens(input_, n):
	return input_ + "".join(get_n_mask_tokens(n))

    enc_token_ids = jnp.ones((1,20),dtype=int)
    dec_token_ids = jnp.zeros((1,1),dtype=int)
    drop_rate = 0.0
    is_inference = True
    random_key = jax.random.PRNGKey(0)

    generate_output = functools.partial(
	enc_dec_fn.apply,
	rng=random_key,
	dec_token_ids=jnp.array([[0] for x in enc_token_ids]),
	drop_rate=drop_rate,
	is_inference=is_inference
    )
    generate_output_jit = jax.jit(generate_output)

    # The model is trained to "unmask" each masked token
    enc_token_ids = jnp.array(
	tokenizer.encode(append_n_mask_tokens("QVQLVESGGGLVQPGGSL",7))
    ).reshape(1,-1)

    outs = generate_output_jit(
	params=params,
	enc_token_ids=enc_token_ids
    )
    print(outs['output_sequence'])
```

The encoder-only tasks require a bit more effort, as the Ankh paper does not
provide any pretrained weights for the ConvBertLayer.  Nevertheless, the
model and ConvBertLayer can be loaded in the following way:
```python
    parameters, encoder_fn, tokenizer, config = get_pretrained_ankh_model(
	base_or_large='base', encoder_only=True)
    encoder_fn = hk.transform(encoder_fn)
    encoder_embed_fn = functools.partial(
	encoder_fn.apply,
	rng=random_key,
	drop_rate=drop_rate
    )
   simple_config = ConvBertConfig(conv_bert_task='solubility')
   solubility_fn = hk.transform(build_ankh_convbert_head_fn(simple_config))
```
The solubility_fn can then be trained separately using output embeddings from
the encoder in order to obtain an accurate classifier.

### RITA

[RITA](https://arxiv.org/abs/2205.05789) is a family of decoder-only protein language
models developed by a collaboration of [Lighton](https://lighton.ai/), the
[OATML group](https://oatml.cs.ox.ac.uk/) at Oxford, and the
[Debbie Marks Lab](https://www.deboramarkslab.com/) at Harvard.
RITA was originally developed in PyTorch and released on HuggingFace. Trix offers
support for you to play with these models in Jax. The supported models are listed in the
table below:

| Model                                              | #Params | d_model | layers | lm loss uniref-100 |
| -------------------------------------------------- | ------- | ------- | ------ | ------------------ |
| [Small](https://huggingface.co/lightonai/RITA_s)   | 85M     | 768     | 12     | 2.31               |
| [Medium](https://huggingface.co/lightonai/RITA_m)  | 300M    | 1024    | 24     | 2.01               |
| [Large](https://huggingface.co/lightonai/RITA_l)   | 680M    | 1536    | 24     | 1.82               |
| [XLarge](https://huggingface.co/lightonai/RITA_xl) | 1.2B    | 2048    | 24     | 1.70               |

#### Code Example

Below is an example of loading a RITA model and using it to generate sequences.

```python
import haiku as hk
from trix.pretrained.rita import get_pretrained_rita_model
from trix.utils.decoding import decode_greedy

parameters, forward_fn, tokenizer, config = get_pretrained_rita_model(
    model_name="RITA_s",
)
forward_fn = hk.transform(forward_fn)

prompts = ["M", "MAB", "MALL"]
tokenizer_outputs = tokenizer(
    prompts,
    return_tensors="np",
    padding=False,
    # Avoid adding EOS since this is a decoder-only model and we only want to continue
    # the input sequence.
    add_special_tokens=False,
)
inputs = tokenizer_outputs["input_ids"]
greedy_tokens = [
    decode_greedy(
        init_tokens_ids=tokenizer(
            [prompt], return_tensors="np", add_special_tokens=False
        )["input_ids"],
        random_key=PRNGKey(0),
        params=params,
        apply_fn=rita_fn.apply,
        num_tokens_to_decode=10,
        eos_token_id=tokenizer.eos_token_id,
    )[0]
    for prompt in prompts
]
answers = [tokenizer.batch_decode(tokens)[0] for tokens in greedy_tokens]
for i, (prompt, answer) in enumerate(zip(prompts, answers):
    print(f"Sample {i}")
    print(f"  Prompt: {prompt}")
    print(f"  Answer: {answer.replace(' ', '')}")
```

Note that the vocabulary in RITA consists of the EOS token (`<EOS>`) and the
[25 amino acid symbols](https://www.sciencefriday.com/wp-content/uploads/2018/07/amino-acid-abbreviation-chart.pdf)
(some symbols, e.g., `B`, indicate that more than one amino acid is permissible in a
location). In this case, during
[normalization](https://huggingface.co/learn/nlp-course/chapter6/4), the tokenizer
replaces the `B` in `MAB` with `D` since `B` may be either `D` (aspartic acid) or `N`
(asparagine).

## Nucleotide Transformer: General Pre-trained DNA Language Models

InstaDeep Research & Genomics have trained a collection of DNA language models that can
understand sequences of nucleotides. These models are based on the ESM-1b architecture
but use specific tokenizers tailored for DNA and have been trained on different datasets
comprising human and other species genomes. Trix offers support for you to play
with these models in Jax. As these model use an ESM architecture, all supports such as
sharding support will work for these models.

Users can easily get pre-trained weights for these models and either use them
for inference or fine-tune them easily.
The will code download the checkpoints locally on your machine from our `deepchain-research`
s3 bucket hosted on kao. Make sure you have access and export the required secret
env variable to access this bucket. If the models have already been downloaded locally,
they will simply be loaded from the disk into the RAM directly.


The supported models are listed in the table below:

| Model Name                 | Number Parameters |        Dataset         |
|----------------------------|-------------------|:----------------------:|
| dcnuc_500M_human_ref       | 500M              | human reference genome |
| dcnuc_500M_1000G           | 500M              |         1000G          |
| dcnuc_2B5_1000G            | 2.5B              |         1000G          |
| dcnuc_2B5_multi_species    | 2.5B              |     multi-species      |

### Code Example

Find below an example on how to easily load such a model and use it for inference.
```python
import haiku as hk
import jax
import jax.numpy as jnp

from trix.pretrained.nucleotide_transformer import get_pretrained_dcnuc_model

# Get pretrained model
parameters, forward_fn, tokenizer, config = get_pretrained_dcnuc_model(
    model_name="dcnuc_500M_1000G",
    embeddings_layers_to_save=(20,),
    max_positions=1024,
)
forward_fn = hk.transform(forward_fn)

# Get data and tokenize it
sequences = ['ATTCCG' * 3]
tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

# Initialize random key
random_key = jax.random.PRNGKey(0)

# Perform inference
outs = forward_fn.apply(parameters, random_key, tokens)
print(outs['logits'].shape, outs['embeddings_28'].shape)
```

## Enformer: Long-range interactions for gene expression prediction

[Enformer](https://www.nature.com/articles/s41592-021-01252-x) is a model developed
for gene expression prediction. It has been tailored to handle long sequences (more than
100kbp).

Note that the API of the model differs from others as we need to maintain an internal
state for mean / variance computation over batches. Also the model takes as input
one-hot vectors instead of integers.

### Code Example

```python
from trix.pretrained.enformer import get_pretrained_enformer_model

parameters, state, enformer_fn, config = get_pretrained_enformer_model()
enformer_fn = hk.transform_with_state(enformer_fn)

# Get data and tokenize it
sequences = ['ATTCCG' * 3]

def tokenize_sequence(sequence: str) -> jnp.ndarray:
    codon = {"A": 0, "C": 1, "G": 2, "T": 3}
    x = jnp.array([codon[nuc] for nuc in sequence])
    x = jax.nn.one_hot(x, num_classes=4)
    return x

tokens = jnp.array([tokenize_sequence(sequence) for sequence in sequences])

# Initialize random key
random_key = jax.random.PRNGKey(0)

# Perform inference
outs = enformer_fn.apply(parameters, state, random_key, tokens)
print(outs['embedding'].shape)
```



## scGPT: single-cell multi-omics using generative pretraining

[scGPT](https://www.biorxiv.org/content/10.1101/2023.04.30.538439v2) is a model tailored for single-cell sequencing data, it is a foundational model that can be used for a variety of downstream tasks such as batch correction, cell type identification, and gene expression prediction.

Note that the API of the model differs from others as we need to maintain an internal state for mean / variance computation over batches.

### Code Example

```python
from trix.pretrained.scgpt import get_pretrained_scgpt_model, reset_dsbn_statistics
from trix.utils.masking import build_padding_attention_mask
import jax.numpy as jnp
import jax

params, scgpt_fn, vocab, config = get_pretrained_scgpt_model(
        model_name="human"
    )

dummy_batch = {
    "gene_indexes": jnp.array([[345, 234, 123, 0, 0, 0, 0, 0, 0, 0]]),
    "gene_expression_values": jnp.array([[1.0, 2.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0]]),
    "batch_labels": jnp.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
}


attention_mask = build_padding_attention_mask(
    dummy_batch["gene_indexes"], config.pad_token_id
)

# Initialize random key
random_key = jax.random.PRNGKey(0)

_, initial_state = scgpt_fn.init(
    random_key,
    gene_indexes=dummy_batch["gene_indexes"],
    gene_expression_values=dummy_batch["gene_expression_values"],
    attention_mask=attention_mask,
    batch_labels=dummy_batch["batch_labels"],
    is_training=True,
)
state = reset_dsbn_statistics(initial_state)

# Perform inference
outs, new_state = scgpt_fn.apply(params,
    state,
    random_key,
    gene_indexes=dummy_batch["gene_indexes"],
    gene_expression_values=dummy_batch["gene_expression_values"],
    attention_mask=attention_mask,
    batch_labels=dummy_batch["batch_labels"],
    is_training=True,
    )
print(outs['gene_expression_predictions'].shape)
```

## HyenaDNA: Long-Range Genomic Sequence Modeling at Single Nucleotide Resolution

[HyenaDNA](https://arxiv.org/abs/2306.15794) is a genomic foundation model based on
implicit long convolutions and was pre-trained on the human reference genome.
HyenaDNA is capable of processing context length of up to 1M base pairs.

Note that the API of the model contains an `is_training` argument in the forward
function since Hyena DNA uses dropout.

The list of available models is below: <br>
-- **hyenadna-tiny-1k-seqlen** <br>
-- **hyenadna-tiny-1k-seqlen-d256** <br>
-- **hyenadna-tiny-16k-seqlen-d128** <br>
-- **hyenadna-small-32k-seqlen** <br>
-- **hyenadna-medium-160k-seqlen** <br>
-- **hyenadna-medium-450k-seqlen** <br>
-- **hyenadna-large-1m-seqlen** <br>

### Code Example
```python
from trix.pretrained.hyena_dna import get_pretrained_hyena_dna
import jax.numpy as jnp
import jax
import haiku as hk

pretrained_model_name = 'hyenadna-tiny-1k-seqlen-d256'

# get jax defined model
params, forward_fn, tokenizer, config = get_pretrained_hyena_dna(
    pretrained_model_name
)

# generate fake inputs
sequences = ["ATACG" * 10, "CGCCC" * 12, "CGCGCGCTTTAfk" * 4]

# check that tokenized sequences are the same across tokenizers
token_ids_jax = jnp.array([b[1] for b in tokenizer.batch_tokenize(sequences)])


forward_fn = hk.transform(forward_fn)
rng = jax.random.PRNGKey(0)

# jax forward
jax_outputs = forward_fn.apply(params, rng, token_ids_jax, is_training=False)
jax_embeddings = jax_outputs["embeddings"]
jax_logits = jax_outputs["logits"]

print(f"Embeddings are of shape {jax_embeddings.shape}")
print(f"Logits are of shape {jax_logits.shape}")

```

## RNA Transformer: General Pre-trained RNA Language Models

InstaDeep, for SOW 33 - codon optimisation, has trained RNA language models that can
understand RNA sequences. These models are based on the ESM-1b architecture
but use specific tokenizers tailored for RNA (CodonTokenizer) and have been trained
on different datasets comprising Bacteria, Eukaryotes, and even non-coding data. Trix
offers support for you  to play with these models in Jax. As these model use an ESM
architecture, all supports such as sharding support will work for these models.

The CodonTokenizer uses codon tokens but not only: it can use nucleotide tokens
if `use_codon_tokens` is set to False. It is also possible to use a mix of both:
nucleotide tokens for the untranslated regions (UTRs) and codon tokens for the coding
sequence. This all relies on the `RNASequence` class which forces the user to tag
parts of each sequence as 5'UTR, coding sequence, or 3'UTR.

Users can easily get pre-trained weights for these models and either use them
for inference or fine-tune them easily.
The code will download the checkpoints locally on your machine from our GCP bucket.
Make sure you have access and export the required secret env variable to access this bucket.
If the models have already been downloaded locally, they will simply be loaded from the disk
into the RAM directly.


The supported models are listed in the table below (for now, only one model is added):

| Model Name                            | Number Parameters |        Dataset        |        Tokenization         |
|---------------------------------------|-------------------|-----------------------|:---------------------------:|
| rna_transformer_v1_150M_coding_region | 150M              | Bacteria + Eukaryotes | codons (coding region only) |

### Code Example

```python
import haiku as hk
import jax
import jax.numpy as jnp
from trix.pretrained.rna_transformer import get_pretrained_rna_transformer_model
from trix.utils.rna_sequence import RNASequence

parameters, forward_fn, tokenizer, config = get_pretrained_rna_transformer_model(
    model_name="rna_transformer_v1_150M_coding_region",
    fixed_length=1024,
    embeddings_layers_to_save=(29,),
)
forward_fn = hk.transform(forward_fn)

# Tokenize and do a forward pass
random_key = jax.random.PRNGKey(0)
batch = tokenizer.batch_tokenize([RNASequence(five_prime_utr="A", coding_sequence="AUGCUA", three_prime_utr="CG")])
tokens_ids = [b[1] for b in batch]
tokens = jnp.array(tokens_ids)
outs = forward_fn.apply(parameters, random_key, tokens)

print(outs['logits'].shape, outs[f"embeddings_{config.num_layers - 1}"].shape)
```
