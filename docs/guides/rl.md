# Transformers in RL with Trix
In Trix, we aim to provide a collection of tools, algorithms and examples to facilitate the development of language models for RL.

## Decision Transformer

The [Decision Transformer](https://arxiv.org/pdf/2106.01345.pdf) is a language model designed to perform Offline Reinforcement Learning. It frames the offline RL problem as a Causal Language Modeling (CLM) problem and uses tools developed by NLP and computer vision community to tackle it. Decision transformer has shown competitive behaviour against other Offline RL and behaviour cloning methods on multiple datasets and environments and it demonstrated interesting extrapolation and generalization capabilities.

 - Decision transformer acts directly on raw RL data (it doesn't tokenize inputs), still it embeds inputs with dense layers to a fixed-size encoding.
 - At train time, the decision transformer aims to predict, in a causal manner, the next action `a_t` based on
 the sequence `(r_0,s_0,a_0,r_1,s_1,a_1,...)`. Where `r_t` denotes the return-to-go, that is the return (over the whole episode) computed
 starting from the current timestep, `s_t` is the observations and `a_t` the actions.
 - For continuous actions, we can directly use mean-squared-error loss, and cross-entropy for discrete actions.
 - At inference time, the decision transformer constructs the return-to-go with a given target-return and predicts each next actions
 with the trajectory it has observed so far (similarly to training time).
<img src="../img/decision_transformer.png" alt="Decision Transformer" />


### Decision Transformer architecture

 The DecisionTransformer follows the decoder-only architecture which consists of an embedding layer followed by multiple self-attention blocks, with a prediction head at the end. The main differences lie in the embedding layer, there are three of them in the DecisionTransformer (one for the returns-to-go, one for the observations and one for the actions) and they are linear (dense) layers instead of a lookup-table-like layer. As a decoder-only architecture, we always use causal attention masks, so the model can only attend to the past. Additionally, the way the data is processed is also specific owing to the fact the returns-to-go, observations and actions must be interleaved. These pre-preprocessing steps are currently performed by a [DecisionTransformerTokenizer](/trix/api/tokenizers/rl/#trix.tokenizers.rl.DecisionTransformerTokenizer) which is not a tokenizer per se, but it computes automatically returns-to-go and masks and it returns an interleaved sequence of tokens.

#### [Model Config](/trix/api/models/decision_transformer/#trix.models.decision_transformer.DecisionTransformerConfig)
#### [Model Class](/trix/api/models/decision_transformer/#trix.models.decision_transformer.DecisionTransformer)
#### [Tokenizer](/trix/api/tokenizers/rl/#trix.tokenizers.rl.DecisionTransformerTokenizer)
#### [Decoder Causal Language Trainer](/trix/api/training/trainer/#trix.training.trainer.DecoderCLMTrainer)

### Example notebook
**A commented example on a robotics environment including training, inference and visualisation is present in the notebooks folder `notebooks/decision_transformer_example.ipynb`.**

### Code snippet example

Find below an code snippet on to instantiate a decision transformer and use it for inference on fake data.
```python
import haiku as hk
import jax
import jax.numpy as jnp

from trix.models.decision_transformer.model import (
    DecisionTransformerConfig,
    build_decision_transformer_forward_fn,
)
from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer
from trix.dataloaders.rl.generators import generate_fake_rl_trajectories


observation_size = 7
action_size = 4
random_key = jax.random.PRNGKey(0)

# Generate fake data
random_key, subkey = jax.random.split(random_key)
transitions = generate_fake_rl_trajectories(
    num_trajectories=100,
    episode_length=200,
    observation_size=observation_size,
    action_size=action_size,
    allow_incomplete=False,
    random_key=subkey,
)

# Decision Transformer parameters
dt_config = DecisionTransformerConfig(
    embed_dim=256,
    num_layers=1,
    num_attention_heads=1,
    ffn_embed_dim=128,
    action_tanh=True,
)

# Initialize tokenizer and tokenize (and arrange) transitions
tokenizer = DecisionTransformerTokenizer(pad_token_id=jnp.nan)
tokens = tokenizer.batch_tokenize(transitions)

# Get model functions
forward_fn = build_decision_transformer_forward_fn(
    dt_config, action_size=action_size, observation_size=observation_size
)
decision_transformer = hk.without_apply_rng(hk.transform(forward_fn))

# Initializer parameters
random_key, init_key = jax.random.split(random_key)
params = decision_transformer.init(init_key, tokens)

# Get predictions
outs = decision_transformer.apply(params, tokens)
```
