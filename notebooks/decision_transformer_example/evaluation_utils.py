"""
This file is not included into Trix, because it is specific to the Brax environment
suite.
"""
from functools import partial
from typing import Callable, Dict, Optional, Tuple

import brax.envs
import haiku as hk
import jax
import jax.numpy as jnp

from trix.tokenizers.rl.base import Transition
from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer
from trix.types import AttentionMask, RNGKey, Tokens
from trix.utils.masking import build_causal_attention_mask


@partial(
    jax.jit,
    static_argnames=(
        "env",
        "forward_fn",
        "normalize_observations",
        "tokenizer",
        "horizon_size",
    ),
)
def play_step(
    params: hk.Params,
    random_key: RNGKey,
    env_state: brax.envs.State,
    trajectory: Transition,
    target_return: jnp.ndarray,
    current_timestep: int,
    mean: jnp.ndarray,
    std: jnp.ndarray,
    env: brax.envs.Env,
    forward_fn: Callable[
        [hk.Params, RNGKey, Tokens, Optional[AttentionMask]],
        Dict[str, jnp.ndarray],
    ],
    normalize_observations: bool,
    tokenizer: DecisionTransformerTokenizer,
    horizon_size: int,
) -> Tuple[brax.envs.State, Transition]:
    """
    Plays a single step in the environment and returns the new state,
    policy parameters, random key and the transition.

    Args:
        params: Model parameters.
        random_key: Random key.
        env_state: Environment state.
        trajectory: Sequence of transitions, each element should have
            (episode_length) as first dimension.
        target_return: Target return for the trajectory.
        current_timestep: The current timestep in the trajectory.
        mean: Mean for observation normalization.
        std: Standard deviation for observation normalization.
        env: Environment.
        forward_fn: Model forward function.
        normalize_observations: Whether to normalize or not the observations.
        tokenizer: Tokenizer.
        horizon_size: Horizon size.

    Returns:
        New environment state.
        Updated trajectory.
    """

    concat_size = tokenizer.concat_size

    # Determine the chunk of the trajectory to give to the model
    def _chunk_tokens(tokens: Tokens, current_timestep: int) -> Tuple[Tokens, int]:
        def episode_beggining(operand):  # type: ignore
            tokens, current_timestep = operand
            pred_index = current_timestep
            tokens = jax.tree_map(lambda x: x[: concat_size * horizon_size], tokens)
            return tokens, pred_index

        def from_horizon_size(operand):  # type: ignore
            tokens, current_timestep = operand
            start_idx = (current_timestep + 1 - horizon_size) * concat_size
            tokens = jax.tree_map(
                lambda x: jax.lax.dynamic_slice_in_dim(
                    operand=x,
                    start_index=start_idx,
                    slice_size=concat_size * horizon_size,
                    axis=0,
                ),
                tokens,
            )

            pred_index = -1
            return tokens, pred_index

        tokens, pred_index = jax.lax.cond(
            pred=current_timestep < horizon_size,
            true_fun=episode_beggining,
            false_fun=from_horizon_size,
            operand=(tokens, current_timestep),
        )
        return tokens, pred_index

    # Tokenize the chunk and feed it to the model
    tokens = tokenizer.tokenize(trajectory, target_return)
    tokens, pred_index = _chunk_tokens(tokens=tokens, current_timestep=current_timestep)
    tokens = jax.tree_map(lambda x: x[None], tokens)
    attention_mask = build_causal_attention_mask(1, concat_size * horizon_size)

    # Get predicted action - this part is completely model-dependant
    random_key, subkey = jax.random.split(random_key)
    pred_actions = forward_fn(
        params,
        subkey,
        tokens,
        attention_mask,
    )["predictions"]
    actions = pred_actions[0, pred_index]

    # Perform action in env and store infos
    next_env_state = env.step(env_state, actions)

    if normalize_observations:
        next_obs = (next_env_state.obs - mean) / std
    else:
        next_obs = next_env_state.obs

    rewards = next_env_state.reward
    dones = next_env_state.done

    trajectory = trajectory._replace(
        obs=trajectory.obs.at[current_timestep + 1].set(next_obs),
        actions=trajectory.actions.at[current_timestep].set(actions),
        rewards=trajectory.rewards.at[current_timestep + 1].set(rewards),
        dones=trajectory.dones.at[current_timestep + 1].set(dones),
    )

    return next_env_state, trajectory


def generate_unroll(
    target_return: jnp.ndarray,
    params: hk.Params,
    random_key: RNGKey,
    env_state: brax.envs.State,
    mean: jnp.ndarray,
    std: jnp.ndarray,
    env: brax.envs.Env,
    forward_fn: Callable[
        [hk.Params, RNGKey, Tokens, Optional[AttentionMask]],
        Dict[str, jnp.ndarray],
    ],
    normalize_observations: bool,
    tokenizer: DecisionTransformerTokenizer,
    horizon_size: int,
) -> Tuple[Transition, brax.envs.State]:
    """Loops over `play_step` to generate a trajectory.

    Args:
        target_return: Target return for the trajectory.
        params: Model parameters.
        random_key: Random key.
        env_state: Environment state.
        mean: Mean for observation normalization.
        std: Standard deviation for observation normalization.
        env: Environment.
        forward_fn: Model forward function.
        normalize_observations: Whether to normalize or not the observations.
        tokenizer: Tokenizer.
        horizon_size: Horizon size.

    Returns:
        The complete trajectory.
        The sequence of encountered states.
    """

    episode_length = env.episode_length
    rewards = jnp.ones(shape=(episode_length)) * -1e4
    obs = jnp.ones(shape=(episode_length, env.observation_size)) * -1e4
    actions = jnp.ones(shape=(episode_length, env.action_size)) * -1e4
    dones = jnp.zeros(shape=(episode_length))
    if normalize_observations:
        obs_0 = (env_state.obs - mean) / std
    else:
        obs_0 = env_state.obs

    obs = obs.at[0].set(obs_0)
    rewards = rewards.at[0].set(env_state.reward)
    dones = dones.at[0].set(env_state.done)

    trajectory = Transition(obs=obs, actions=actions, rewards=rewards, dones=dones)

    def _scan_play_step(carry, unused):  # type: ignore
        env_state, trajectory, i = carry

        env_state, trajectory = play_step(
            env_state=env_state,
            trajectory=trajectory,
            current_timestep=i,
            random_key=random_key,
            params=params,
            env=env,
            forward_fn=forward_fn,
            normalize_observations=normalize_observations,
            mean=mean,
            std=std,
            tokenizer=tokenizer,
            target_return=target_return,
            horizon_size=horizon_size,
        )
        i += 1

        return (env_state, trajectory, i), env_state

    i = 0
    (env_state, trajectory, i), env_states = jax.lax.scan(
        _scan_play_step,
        (env_state, trajectory, i),
        (),
        length=episode_length,
    )

    return trajectory, env_states


def evaluate_model(
    init_state: brax.envs.State,
    target_returns: jnp.ndarray,
    params: hk.Params,
    random_key: RNGKey,
    mean: jnp.ndarray,
    std: jnp.ndarray,
    env: brax.envs.Env,
    forward_fn: Callable[
        [hk.Params, RNGKey, Tokens, Optional[AttentionMask]],
        Dict[str, jnp.ndarray],
    ],
    normalize_observations: bool,
    tokenizer: DecisionTransformerTokenizer,
    horizon_size: int,
) -> Tuple[jnp.ndarray, brax.envs.State]:
    """
    For each element of target_returns, uses `generate_unroll` to
    generate a trajectory and collects returns and states.

    Args:
        init_state: Initial environment state.
        target_returns: Target returns for the trajectories.
        params: Model parameters.
        random_key: Random key.
        mean: Mean for observation normalization.
        std: Standard deviation for observation normalization.
        env: Environment.
        forward_fn: Model forward function.
        normalize_observations: Whether to normalize or not the observations.
        tokenizer: Tokenizer.
        horizon_size: Horizon size.

    Returns:
        The episodic returns.
        The trajectories of states.

    """

    # Evaluate
    random_key, subkey = jax.random.split(random_key)
    unroll_fn = partial(
        generate_unroll,
        env_state=init_state,
        random_key=subkey,
        params=params,
        env=env,
        forward_fn=forward_fn,
        normalize_observations=normalize_observations,
        mean=mean,
        std=std,
        tokenizer=tokenizer,
        horizon_size=horizon_size,
    )
    transitions, states = jax.vmap(unroll_fn)(target_return=target_returns)

    is_done = jnp.clip(jnp.cumsum(transitions.dones, axis=-1), 0, 1)
    mask = jnp.roll(is_done, 1, axis=-1)
    mask = mask.at[..., 0].set(0)

    # Scores
    returns = jnp.sum(transitions.rewards * (1.0 - mask), axis=-1)

    return returns, states
