{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95d2e196-13e2-4b9b-a76e-858f52884e75",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "!pip install brax==0.0.12 flax==0.4.1\n",
    "!pip install seaborn==0.12.1\n",
    "!pip install lz4==4.0.2\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e624bb32-a2fc-4182-8e16-f0781d8c699a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "import os\n",
    "from functools import partial\n",
    "from typing import Optional, Tuple\n",
    "\n",
    "import brax.envs\n",
    "import haiku as hk\n",
    "import jax\n",
    "import jax.numpy as jnp\n",
    "import joblib\n",
    "import numpy as np\n",
    "import optax\n",
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "from brax.io import html\n",
    "from evaluation_utils import evaluate_model\n",
    "from IPython.display import HTML, Image\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "from trix.dataloaders.rl.datasets import TrajectoryDataset\n",
    "from trix.models.decision_transformer.model import (\n",
    "    DecisionTransformerConfig,\n",
    "    build_decision_transformer_forward_fn,\n",
    ")\n",
    "from trix.tokenizers.rl.base import Transition\n",
    "from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer\n",
    "from trix.training.rl_trainers.decision_transformer_trainer import (\n",
    "    DecisionTransformerTrainer,\n",
    ")\n",
    "from trix.training.rl_trainers.runners import (\n",
    "    run_train_epoch_decision_transformer,\n",
    ")\n",
    "from trix.utils.rl import get_mask_from_dones\n",
    "from trix.training.losses import mse_loss\n",
    "from trix.utils.parameters import get_num_parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49aab8c0-e4b9-4e62-9b60-99a1d37c1aa7",
   "metadata": {},
   "outputs": [],
   "source": [
    "jax.config.update(\"jax_platform_name\", \"cpu\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c507146e-b393-4c93-ab3e-0f3e75b06dae",
   "metadata": {
    "tags": []
   },
   "source": [
    "\n",
    "## Outline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75bd85c4-13b4-44f8-b468-f2ce264302d7",
   "metadata": {},
   "source": [
    "In this notebook we will showcase the use of a **DecisionTransformer** on a continuous control environment from the *Brax* suite, namely **HalfCheetah**.\n",
    "\n",
    "This notebook will show you how to:\n",
    " - **Train** a DecisionTransformer on a dataset of trajectories collected by an RL agent.\n",
    " - **Evaluate**, during the training phase, the learned DecisionTransformer on the RL environment.\n",
    " - **Visualize** the performance of the trained DecisionTransformer, both with evaluation metrics and simulation videos."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b662edf3-d3e3-41e7-b5b8-701d326828e5",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Notebook configuration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b03e6163-af47-429f-8dfc-e46c141f1c2f",
   "metadata": {},
   "outputs": [],
   "source": [
    "device_type = \"tpu\"  # tpu, gpu or cpu\n",
    "# Setup devices\n",
    "devices = jax.devices(device_type)\n",
    "num_devices = len(devices)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "679f1098-9794-418b-be27-be180e860299",
   "metadata": {},
   "outputs": [],
   "source": [
    "env_name = \"halfcheetah\"\n",
    "episode_length = 1000\n",
    "\n",
    "# Training config\n",
    "seed = 0\n",
    "learning_rate = 1e-4\n",
    "num_epochs = 10\n",
    "effective_batch_size = 4096\n",
    "batch_size = 128\n",
    "num_acc_grads = 4  # Only on multi-device (e.g. TPU pods or several GPUs)\n",
    "normalize_observations = True\n",
    "\n",
    "# Eval config\n",
    "eval_freq = 5\n",
    "\n",
    "# Architecture config\n",
    "embed_dim = 128\n",
    "num_layers = 3\n",
    "num_attention_heads = 1\n",
    "ffn_embed_dim = 128  # embedding size of the hidden MLP layer in a decoder\n",
    "action_tanh = True\n",
    "horizon_size = 20\n",
    "num_chunks_per_trajectory = 500\n",
    "\n",
    "# Data Configuration\n",
    "num_samples = 16000 # to modify if your machine has low RAM as the dataset is loaded directly in RAM"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b562b52c-18bd-4265-a537-621b8dc09909",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Evaluation utils for the DecisionTransformer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01e03b05-5fe3-4f92-80f4-1c0cd9da0ac3",
   "metadata": {},
   "source": [
    "In the helper file `evaluation_utils.py`  we define **utilities** to evaluate the policy learned by the DecisionTransformer, by unrolling episode in an online fashion in the environment.\n",
    " - The `play_step` takes as an input an incomplete trajectory, feed this trajectory to the DecisionTransformer to chose the next action, and finally performs a step in this environment with this action.\n",
    " - The `generate_unroll` function repeatedly calls `play_step` to generate a whole trajectory.\n",
    " - The `evaluate_model` function wraps `generate_unroll` to evaluate several times the policy with different *target returns* and gather \n",
    "relevant metrics.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00462e13-aa08-4b09-9638-ffcb9ed96a5a",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Training the DecisionTransformer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d89d856-292e-4e94-993c-edeaaccaa3f0",
   "metadata": {},
   "source": [
    "In this section, we initialize:\n",
    " - The environment.\n",
    " - The tokenizer (a DecisionTransformerTokenizer that takes a transitions, and interleaves token to feed to the transformer).\n",
    " - The dataset (we load a pre-existing dataset).\n",
    " - The DecisionTransformer itself.\n",
    " - The trainer and the TrainingState, containing the parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50560e68-800a-4c54-880f-74f2ba368015",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Init environment, tokenizer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2e570ac-07fb-452f-b285-0dc56a873b05",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Init RNG key\n",
    "random_key = jax.random.PRNGKey(seed)\n",
    "\n",
    "# Init env\n",
    "env_name = env_name\n",
    "env = brax.envs.create(env_name, episode_length=episode_length, legacy_spring=True)\n",
    "action_size = env.action_size\n",
    "observation_size = env.observation_size\n",
    "\n",
    "# Init tokenizer\n",
    "tokenizer = DecisionTransformerTokenizer(\n",
    "    action_size=action_size, observation_size=observation_size\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48a07b26-b99e-49d6-a08a-c9f944aad32e",
   "metadata": {},
   "source": [
    "### Init dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4139a5a-aa99-47d1-8b63-d7e876598f73",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Init dataset\n",
    "dataset_dir = os.path.join(\"datasets\", \"decision_transformer\", \"halfcheetah\")\n",
    "if not os.path.isdir(dataset_dir):\n",
    "    !aws s3 cp  s3://trix/datasets/decision_transformer/halfcheetah/dataset_halfcheetah_pbtsac.lz4 ./datasets/decision_transformer/halfcheetah/ --endpoint=https://s3.kao.instadeep.io\n",
    "with open(os.path.join(dataset_dir, \"dataset_halfcheetah_pbtsac.lz4\"), \"rb\") as f:\n",
    "    data = joblib.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d5407ba-18fa-429d-8f46-d93abf3df64e",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "@jax.jit\n",
    "def normalize_observations(obs, dones):\n",
    "    mask = jax.vmap(get_mask_from_dones)(dones)\n",
    "    mask = mask[..., None]\n",
    "    mean = jnp.mean(obs, axis=(0, 1), where=mask)\n",
    "    std = jnp.std(obs, axis=(0, 1), where=mask)\n",
    "    std = jnp.where(std == 0, 1, std)\n",
    "    normalized_obs = (obs - mean) / std\n",
    "    return normalized_obs, mean, std\n",
    "\n",
    "if normalize_observations:\n",
    "    obs, mean, std = normalize_observations(data[\"obs\"], data[\"dones\"])\n",
    "else:\n",
    "    mean = None\n",
    "    std = None\n",
    "    obs = data[\"obs\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "181ad842-d117-421b-ae61-ac2b34439b9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "transitions = Transition(\n",
    "    actions=data[\"actions\"][:num_samples],\n",
    "    obs=obs[:num_samples],\n",
    "    rewards=data[\"rewards\"][:num_samples],\n",
    "    dones=data[\"dones\"][:num_samples],\n",
    ")\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc231e63-0a03-4c37-b37a-c59d1173ba83",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc529a76-31e2-4a5a-90bb-559a4ce0c8e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = TrajectoryDataset(\n",
    "    transitions,\n",
    "    tokenizer=tokenizer,\n",
    "    batch_size=effective_batch_size,\n",
    "    random_key=random_key,\n",
    "    drop_last=True,\n",
    "    shuffle=True,\n",
    "    horizon_size=horizon_size,\n",
    "    num_chunks_per_trajectory=num_chunks_per_trajectory,\n",
    "    resample_chunks=False,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2388a6d6-a437-4b56-985c-a9fde1159b8d",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Init DecisionTransformer and trainer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "990a7ef4-9775-4787-9094-25352e514135",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Init Decision Transformer\n",
    "decision_transformer_config = DecisionTransformerConfig(\n",
    "    embed_dim=embed_dim,\n",
    "    num_layers=num_layers,\n",
    "    num_attention_heads=num_attention_heads,\n",
    "    ffn_embed_dim=ffn_embed_dim,\n",
    "    action_tanh=action_tanh,\n",
    ")\n",
    "\n",
    "decision_transformer = build_decision_transformer_forward_fn(\n",
    "    decision_transformer_config,\n",
    "    action_size=action_size,\n",
    "    observation_size=observation_size,\n",
    ")\n",
    "decision_transformer_fn = hk.transform(decision_transformer)\n",
    "\n",
    "# Init trainer\n",
    "trainer = DecisionTransformerTrainer(\n",
    "    forward_fn=decision_transformer_fn,\n",
    "    loss_fn=mse_loss,\n",
    "    optimizer=optax.adam(learning_rate=learning_rate),\n",
    "    tokenizer=tokenizer,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64facbea-b1dd-4898-87c2-649cbddb4963",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Init TraningState"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e57cf0eb-8d8b-42a6-a9f4-7c795f8def9a",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Init training state\n",
    "random_key, subkey = jax.random.split(random_key)\n",
    "tokenizer_outs = next(dataset.get_epoch_batches())\n",
    "tokenizer_outs = jax.tree_map(\n",
    "    lambda x: x.reshape(\n",
    "        (\n",
    "            num_devices,\n",
    "            num_acc_grads,\n",
    "            batch_size,\n",
    "        )\n",
    "        + x.shape[1:]\n",
    "    ),\n",
    "    tokenizer_outs,\n",
    ")\n",
    "\n",
    "random_key, subkey = jax.random.split(random_key)\n",
    "subkeys = jax.device_put_replicated(subkey, devices=devices)\n",
    "init = trainer.init\n",
    "\n",
    "tokens = tokenizer_outs[\"tokens\"]\n",
    "positions = tokenizer_outs[\"positions\"]\n",
    "\n",
    "training_state = jax.pmap(init, axis_name=\"batch\", devices=devices)(\n",
    "    random_key=subkeys, tokens=tokens[:, 0], positions=positions[:, 0]\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "88ff1a4a-3a2b-4a14-91c2-dabb82ed5c31",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Log some infos\n",
    "single_device_training_state = jax.tree_map(lambda x: x[0], training_state)\n",
    "num_model_parameters = get_num_parameters(single_device_training_state.params)\n",
    "num_trajectories = transitions.obs.shape[0] * num_chunks_per_trajectory\n",
    "print(f\"Number of params: {num_model_parameters:.1e}\")\n",
    "print(\n",
    "    f\"Number of data points (samples, not trajectories) in the dataset: \"\n",
    "    f\"{ horizon_size * num_trajectories:.1e}\"\n",
    ")\n",
    "print(\n",
    "    f\"Ratio of data points with respect to the number of model's parameters: \"\n",
    "    f\"{ horizon_size * num_trajectories / num_model_parameters:.1f}\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bddb9eb2-52be-4f49-bcba-fcf96333fed9",
   "metadata": {},
   "source": [
    "### Init initial environment states and target returns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a026019c-e870-408d-9cb8-34d198d7f38e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample init states for the evaluation and setup evaluation\n",
    "random_key, subkey = jax.random.split(random_key)\n",
    "init_states = jax.jit(jax.vmap(env.reset))(jax.random.split(subkey, 80))\n",
    "init_states = jax.tree_map(\n",
    "    lambda x: x.reshape(num_devices, -1, *x.shape[1:]), init_states\n",
    ")\n",
    "target_returns = jnp.ones((1,)) * 6000.0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62bc01b6-18a5-45b8-af92-35ffa03f51aa",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Training"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "581c5183-20b1-4a51-8d3f-ee7aae2fb9e4",
   "metadata": {},
   "source": [
    "Here we define a wrapper function to easily evaluate our model by calling `evaluate_model_vmap`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f38304c3-aead-499d-8614-784f40cfe6b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "def evaluate_model_vmap(\n",
    "    target_returns: jnp.ndarray, init_states: brax.envs.State, params: hk.Params\n",
    ") -> Tuple[jnp.ndarray, brax.envs.State]:\n",
    "    \"\"\"\n",
    "    Vmap wrapper around `evaluate_model` for evaluating the model.\n",
    "    \"\"\"\n",
    "\n",
    "    partial_fn = partial(\n",
    "        evaluate_model,\n",
    "        params=params,\n",
    "        random_key=subkey,\n",
    "        env=env,\n",
    "        forward_fn=decision_transformer_fn.apply,\n",
    "        target_returns=target_returns,\n",
    "        normalize_observations=normalize_observations,\n",
    "        mean=mean,\n",
    "        std=std,\n",
    "        tokenizer=tokenizer,\n",
    "        horizon_size=horizon_size,\n",
    "    )\n",
    "\n",
    "    return jax.vmap(partial_fn)(init_states)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b58684c-d2eb-4aa3-a555-5d6019b0e67c",
   "metadata": {},
   "source": [
    "The training loop consists in two parts:\n",
    "- We call `run_train_epoch` to perform a complete training epochs and collect metrics.\n",
    "- Every `eval_freq` epochs we run an evaluation on different initial states."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d07d416-ad28-4756-bff9-ed71f1967ee2",
   "metadata": {},
   "outputs": [],
   "source": [
    "losses = []\n",
    "eval_mean_returns = []"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec2b8997-0073-4b92-99d7-84e0ac108084",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Training loop\n",
    "update = trainer.update\n",
    "for epoch in range(num_epochs):\n",
    "    random_key, _ = jax.random.split(random_key)\n",
    "\n",
    "    training_state, metrics = run_train_epoch_decision_transformer(\n",
    "        update_fn=update,\n",
    "        dataset=dataset,\n",
    "        devices=devices,\n",
    "        batch_size=batch_size,\n",
    "        num_acc_grads=num_acc_grads,\n",
    "        training_state=training_state,\n",
    "        epoch_num=epoch,\n",
    "    )\n",
    "    print(f\"Training loss: {metrics['loss']:.5f}\")\n",
    "    losses.append(metrics[\"loss\"])\n",
    "    # Evaluation\n",
    "    if ((epoch + 1) % eval_freq) == 0 or epoch in [0, (num_epochs - 1)]:\n",
    "        print(\"Evaluation\")\n",
    "        returns, states = jax.pmap(\n",
    "            evaluate_model_vmap,\n",
    "            axis_name=\"batch\",\n",
    "            devices=devices,\n",
    "            in_axes=(None, 0, 0),\n",
    "        )(target_returns, init_states, training_state.params)\n",
    "\n",
    "        returns.block_until_ready()\n",
    "        returns = jax.tree_map(jnp.ravel, returns)\n",
    "\n",
    "        print(f\"Mean evaluation return: {jnp.mean(returns):.2f}\")\n",
    "        eval_mean_returns.append(jnp.mean(returns))\n",
    "        # Checkpoint\n",
    "        save_dir = os.path.join(\"checkpoints\", \"halfcheetah\")\n",
    "        os.makedirs(save_dir, exist_ok=True)\n",
    "        with open(\n",
    "            os.path.join(save_dir, f\"checkpoint-{epoch+1}-epochs.lz4\"), \"wb\"\n",
    "        ) as f:\n",
    "            save_params = jax.tree_map(np.array, training_state.params)\n",
    "            joblib.dump(save_params, f, compress=\"lz4\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59b87994-bcfb-4508-893d-2c2cf0721904",
   "metadata": {},
   "outputs": [],
   "source": [
    "palette = sns.color_palette(\"deep\")\n",
    "\n",
    "fig, axes = plt.subplots(ncols=2, nrows=1, figsize=(12, 3))\n",
    "\n",
    "axes[0].plot(\n",
    "    np.arange(num_epochs),\n",
    "    losses,\n",
    "    \"-o\",\n",
    "    linewidth=2.5,\n",
    ")\n",
    "axes[0].set_xlabel(\"Epochs\")\n",
    "axes[0].set_title(\"Training loss\")\n",
    "\n",
    "\n",
    "axes[1].plot(\n",
    "    np.arange(num_epochs // eval_freq + 1) * eval_freq,\n",
    "    eval_mean_returns,\n",
    "    \"-o\",\n",
    "    linewidth=2.5,\n",
    ")\n",
    "axes[1].set_xlabel(\"Epochs\")\n",
    "axes[1].set_title(\"Mean evaluation return\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f04dfca-58db-41a4-ae73-3cde7d5b7c7f",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Visualize results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c0c326d-a36e-4d19-b0e6-8cc2ca0e0496",
   "metadata": {},
   "source": [
    "Load model parameters and plot observed returns as a function of target returns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aaec35e3-c2f5-4d25-b7b2-19a6795dc1ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load model\n",
    "save_dir = os.path.join(\"checkpoints\", \"halfcheetah\")\n",
    "with open(os.path.join(save_dir, f\"checkpoint-10-epochs.lz4\"), \"rb\") as f:\n",
    "    params = joblib.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "452c206e-34f1-4d0b-aea5-95d26eef4c07",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define evaluation target returns\n",
    "target_returns = jnp.linspace(0, 7000, 80)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1014aeee",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate 5 random seeds by value of target_return\n",
    "results_df = pd.DataFrame(index=range(5), columns=range(80))\n",
    "reset_fn = jax.jit(jax.vmap(env.reset))\n",
    "\n",
    "random_keys = jax.random.split(random_key, 6)\n",
    "random_key = random_keys[0]\n",
    "subkeys = random_keys[1:]\n",
    "\n",
    "init_states = reset_fn(subkeys)\n",
    "returns, states = jax.pmap(\n",
    "    evaluate_model_vmap, axis_name=\"batch\", devices=devices, in_axes=(0, None, 0)\n",
    ")(target_returns.reshape(num_devices, -1), init_states, params)\n",
    "\n",
    "returns.block_until_ready()\n",
    "results_df.iloc[:5] = returns.transpose(0, 2, 1).reshape(-1, 5).T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd727b63-1023-4c14-ad72-15673e8779c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "palette = sns.color_palette(\"deep\")\n",
    "plt.plot(\n",
    "    target_returns,\n",
    "    results_df.mean(axis=0),\n",
    "    color=palette[0],\n",
    "    linewidth=2,\n",
    "    label=\"Sampled returns\",\n",
    ")\n",
    "plt.fill_between(\n",
    "    target_returns,\n",
    "    results_df.mean(axis=0) - results_df.std(axis=0),\n",
    "    results_df.mean(axis=0) + results_df.std(axis=0),\n",
    "    alpha=0.3,\n",
    "    color=palette[0],\n",
    ")\n",
    "plt.plot(\n",
    "    target_returns,\n",
    "    target_returns,\n",
    "    color=palette[2],\n",
    "    linestyle=\"--\",\n",
    "    linewidth=2.5,\n",
    "    label=\"Oracle\",\n",
    ")\n",
    "plt.axvline(\n",
    "    data[\"rewards\"].sum(axis=1).max(),\n",
    "    color=palette[-2],\n",
    "    linestyle=\"--\",\n",
    "    linewidth=2.5,\n",
    "    label=\"Best trajectory in dataset\",\n",
    ")\n",
    "plt.xlabel(\"Target return\")\n",
    "plt.ylabel(\"Mean return\")\n",
    "plt.title(\"Halfcheetah\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4b1dbbf",
   "metadata": {},
   "source": [
    "### Visualize trajectories inside this environment simulator"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0961548e-6602-4924-8012-baba7994299f",
   "metadata": {},
   "source": [
    "Visualize the trajectory inside the simulator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "defc0c31-f6ba-4d86-8a6c-001ddd2f85ab",
   "metadata": {},
   "outputs": [],
   "source": [
    "states_0 = jax.tree_map(lambda x: x[:, 0], states)\n",
    "states_reshaped = jax.tree_map(lambda x: x.reshape((-1,) + x.shape[2:]), states_0)\n",
    "states_highest_target = jax.tree_map(lambda x: x[-1], states_reshaped)\n",
    "qps = [jax.tree_map(lambda x: x[i], states_highest_target.qp) for i in range(1000)]\n",
    "res = HTML(html.render(env.sys, qps))\n",
    "res"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d20e8604-753f-4162-8271-e68d70a6bdc3",
   "metadata": {},
   "source": [
    "Optionally save the trajectoire to a HTML file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c80de0e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"traj.html\", \"w\") as f:\n",
    "    f.write(res.data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20347389-23b0-425b-bb03-eb5b8e3fd019",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.16"
  },
  "vscode": {
   "interpreter": {
    "hash": "f31268c7839cb35f9e649b15bd684de5545dd42c6f0e58f82db3ad495a433b92"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
