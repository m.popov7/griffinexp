import codecs
import os.path

from setuptools import find_packages, setup


# source: https://packaging.python.org/en/latest/guides/single-sourcing-package-version/
def read(rel_path: str) -> str:
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path: str) -> str:
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


__version__ = get_version("trix/__init__.py")


requirements = [
    "datasets>=2.12.0",
    "dm-haiku>=0.0.9",
    "jax>=0.4.13",
    "jaxlib>=0.4.13",
    "matplotlib>=3.6",
    "optax>=0.1.7",
    "protobuf>=3.20.1",
    "python-dotenv>=0.21.0",
    "regex>=2022.10.31",
    "tqdm>=4.64.0",
]

dna_requirements = [
    "biopython>=1.79",
]

esm_requirements = [
    *dna_requirements,
    "fair-esm>=2.0.0",
    "torch>=1.11.0",
]

nlp_requirements = [
    "sentencepiece>=0.1.97",
    "torch>=2.0.0",
    "transformers>=4.30.2",
]

full_requirements = [
    *esm_requirements,
    *dna_requirements,
    *nlp_requirements,
]


setup(
    name="id-trix",
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    scripts=["trix/pretrained/download_llama_weights.sh"],
    zip_safe=False,
    license="MIT",
    author="InstaDeep Research",
    description="A Python Research Library for Transformers in Jax",
    install_requires=requirements,
    python_requires=">=3.10",
    dependency_links=[
        "https://storage.googleapis.com/jax-releases/jax_releases.html",
        "https://download.pytorch.org/whl/torch_stable.htm",
    ],
    extras_require={
        # "pip install id-trix[full]" installs everything
        "full": full_requirements,
        # "pip install id-trix[esm]" installs fair-esm and biopython
        "esm": esm_requirements,
        # "pip install id-trix[dna]" installs biopython
        "dna": dna_requirements,
        # "pip install id-trix[nlp]" installs transformers and torch
        "nlp": nlp_requirements,
    },
    keywords=["Transformers", "Language Models", "JAX"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.10",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ],
)
