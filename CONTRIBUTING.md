# Contributing to Trix :rabbit:

First and foremost, thanks for considering contributing to Trix! :pray: :tada:

There are several ways in which you can contribute to Trix: raising issues, writing documentation, reviewing code and ultimately writing code. This guide focuses on contributing through writing code.

Before you start making changes and send us your Merge Requests, be aware that:

- We might decide not to merge your changes if it significantly increases our maintenance burden unless it is a very compelling functionality or an obvious bug fix :bug:.
- Your project might need specific one-off unicorn functionalities that most other projects do not :unicorn:. In that case, we invite you to fork the project if it is unlikely that your changes would be used by other projects.

:point_right: **Anytime you plan to modify the codebase, start by creating a issue, and a branch starting from `develop` with the name and numeration of the created issue.**


## Code Style
### Linters
To guarantee the quality and uniformization of the codebase, we use various linters:

- [Black](https://black.readthedocs.io/en/stable/) is a deterministic code formatter that is compliant with PEP8 standards.
- [Isort](https://pycqa.github.io/isort/) sorts imports alphabetically and separates them into sections.
- [Flake8](https://flake8.pycqa.org/en/latest/) is a library that wraps PyFlakes, pycodestyle and Ned Batchelder's McCabe script. It is a great toolkit for checking your codebase against coding style (PEP8), programming, and syntax errors. Flake8 also benefits from an ecosystem of plugins developed by the community that extend its capabilities. You can read more about Flake8 plugins on the [documentation](https://flake8.pycqa.org/en/latest/user/using-plugins.html) and find a curated list of plugins [here](https://github.com/DmytroLitvinov/awesome-flake8-extensions).
- [MyPy](https://mypy.readthedocs.io/en/stable/) is a static type checker that can help you detect inconsistent typing of variables.

### Pre-Commit

To help in automating the quality of the code, we use pre-commit, a framework that manages the installation and execution of git hooks that will be run before every commit.
These hooks help to automatically point out issues in code such as formatting mistakes, unused variables, trailing whitespace, debug statements, etc.
By pointing these issues out before code review, it allows a code reviewer to focus on the architecture of a change while not wasting time with trivial style nitpicks.

Each commit should be preceded by a call to
[pre-commit](https://pre-commit.com/) to ensure code quality and formatting.
The configuration is in `.pre-commit-config.yaml` and includes: `isort`, `black`,
`flake8`, `mypy`, `nbstripout` and `codespell` and checks for the yaml formatting,
trimming, trailing whitespace, etc.

To install the environment, please use

```
conda env create -f .environment.pre-commit.yaml && conda activate trix-pre-commit
pre-commit install
```

Try running: `pre-commit run --all-files` - all linters must pass before
committing your change.

#### Install the hooks
You should install the hooks via
```
pre-commit install -t pre-commit -t commit-msg
```

## Coding conventions
Please follow as much as possible the following coding conventions:

  - Classes' names are Caml case (example: MyClass).
  - Functions and variables are in lower case with _ as separator (example: `my_function, my_var`).
  - Names are explicit: avoid mathematical notations, functions' names start with a verb.
  - Use python typing library: each class and method should be typed (both for inputs and outputs).
  - Create custom types if needed.
  - Avoid having more than 5 arguments for a function, if you need more create a `dataclass`.
  - Avoid dictionaries to pass arguments when possible and prefer `dataclasses` instead.
  - Repeat inputs names when calling a function: ex: `compute_custom(arg1=arg1, arg2=my_arg2)`.
  - Use f strings to add variables in strings: ex: `print(f'my var value is {my_var}')`.

### Jax specific conventions
To keep the codebase standardized, we follow several guidelines when it comes to Jax practices:

- When dealing with random keys, try to stick the following notation: `random_key, subkey = jax.random.split(random_key)`, and always use `subkey` in your code and use `random_key` for creating new keys.
- When it comes to chose a data structure for a Jax `Pytree`, we recommend to use `NamedTuple`.
- When defining a function that is destined to be called into a `jax.lax.scan`, we recommend defining the function normally, then writing a wrapper `def _scan_my_func(carry, x):` to provide to the `scan` primitive. Same for other Jax primitives such as `foriloop` or `cond`.


### Docstrings

Docstrings should be harmonized throughout the repository. We use [Google-style](https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google) docstrings. Specifically we respect these few conventions:

  - Every sentence starts with a capital letter and ends with a final point.
  - Types are not repeated in the `Args` part.
  - Returned variables names are not included in the `Returns` statement.
  - When possible it is a good practice to specify the shape of the arrays.

Example:
``` python
    def mlp(self, x: Embedding) -> Embedding:
        """
        Applies one layer-norm, one linear layer, a Gelu activation,
        then a final linear layer.

        Args:
            x: Embeddings of shape (batch_size, seq_len, key_size * num_heads).

        Returns:
            The transformed sequence embedding.
        """
        x = self.layer_norm_mlp(x)
        x = jax.nn.gelu(
            self.fc1(x),
            approximate=False,
        )
        x = self.fc2(x)
        return x
```


## Commits
This repository uses [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/), which provides a lightweight convention on top of commit messages.
It consists of an easy set of rules for creating an explicit commit history; which makes it easier to write automated tools on top of.
The commit message should be structured as follows:

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

where `type` must be: `build`, `chore`, `ci`, `docs`, `feat`, `fix`, `perf`, `refactor`, `revert`, `style` or `test`.

example:
```
refactor(logger): import a common utils function for key formatting
```

## Tests
We use `pytest` to test our code.


For any code you write, you should also write its unit tests. If you write a new file `path_to_foo/foo.py`, you should place its unit tests in `tests/path_to_foo/foo_test.py`. Please ensure that tests pass locally before pushing to the repo.
Tests should be deterministic, setting a seed (and ideally running for several seeds) can ensure this.

## Documentation :memo:

Documentation is written using [`mkdocs`](https://www.mkdocs.org/) which builds documentation from markdown files. We also use [`mkdocstrings`](https://mkdocstrings.github.io/) to automatically generate documentation from the code. For more information see the [documentation guide](../docs) and the [current documentation](https://instadeep.gitlab.io/trix).
To Install mkdocs and its plugins:
```
pip install -r requirements-docs.txt
```

Run the server
```
mkdocs serve
```

## Merge Request Guidelines

## Reviewing Code

Maintainers should prioritise reviewing MRs. If everyone does this, we will keep the backlog of
work moving consistently and no one should be blocked for too long.

## Small Merge Requests

- All MRs should be as small as possible. A 10 line MR will be reviewed more thoroughly (and quickly) than a
  1000 line MR.
- If a task requires a large MR, you should break it up into multiple small MRs. These smaller
  MRs do not have to be fully functional, but they must adhere to the repos standards and not
  break any existing functionality.
- For example, if you were adding a new logger to be used for all agents. Instead of creating
  the logger and updating all of the agents to use it, you should break the task down into multiple
  MRs for 1) creating the logger class 2) adding the new logger to a single agent and training run 3) adding the logger to the remaining agents. This will be much easier to review and catch any
  issues in the change with the least amount of risk.
- If a task is broken up into smaller MRs, work on another low priority task while the subtask
  is being reviewed, instead of moving on with the next subtask. This is to prevent any
  sub-branching issues (If everyone is reviewing consistently the wait shouldn't be long).
- If a task is large enough, contributors and maintainers should do some form of upfront design
  and/or a discussion of the task before submitting an MR
  (as opposed to having these discussions as we review).

## Submitting A Merge Request

- Provide an overview of your MR giving any necessary information that would make it easier to
  review. This should include the context of the solution, why it works and highlight any key areas
  of interest for review.
- Make sure that your MR commit message follows the
  [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) format. If your merge commit
  message does not follow this format, your changes will not be added to the changelog and no one
  will ever know about the cool new changes you added, and we will be sad.
- Our current CI implements different pipelines to make sure the submitted code passes pre-commit
  and passes tests defined in `tests`. Note that it is not possible to merge code if those pipelines
  have not passed. Triggering those pipelines is manual and can be done directly from the MR page on
  GitLab. You will need to trigger *test* (that requires *build*) manually. We also provide a *test-large*
  pipeline for large tests that are necessary before merging into the `main` branch.
