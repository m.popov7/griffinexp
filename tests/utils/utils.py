import numpy as np


def print_error_message_assert_array_equal(a: np.ndarray, b: np.ndarray) -> str:
    try:
        np.testing.assert_array_equal(a, b)
        raise AssertionError("assertion did not fail as expected")
    except AssertionError as e:
        return str(e)
