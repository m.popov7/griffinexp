import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.bigbird.model import BigbirdConfig, BigbirdTransformer
from trix.types import AttentionMask


@pytest.mark.parametrize("seq_len", [256])
@pytest.mark.parametrize("num_heads", [20])
@pytest.mark.parametrize("block_size", [32])
@pytest.mark.parametrize("num_rand_blocks", [3])
@pytest.mark.parametrize("embed_dim", [128])
@pytest.mark.parametrize("batch_size", [2])
def test_self_attention(
    seq_len: int,
    num_heads: int,
    block_size: int,
    num_rand_blocks: int,
    embed_dim: int,
    batch_size: int,
) -> None:
    """
    Tests that self attention works properly and returns the right context shapes.
    """

    random_key = jax.random.PRNGKey(0)
    inputs = jax.random.uniform(random_key, shape=(batch_size, seq_len, embed_dim))

    def self_attention(x: jnp.ndarray, mask: AttentionMask) -> jnp.ndarray:
        config = BigbirdConfig(
            alphabet_size=10,
            max_positions=1024,
            embed_dim=embed_dim,
            ffn_embed_dim=4096,
            attention_heads=8,
            num_layers=6,
            block_size=block_size,
            num_rand_blocks=num_rand_blocks,
            pad_token_id=0,
            mask_token_id=1,
        )
        model = BigbirdTransformer(config)

        sa = model._attention_block(0).self_attention(x, mask)

        return sa  # type: ignore

    attn_mask = jnp.ones((batch_size, 1, seq_len, seq_len), dtype=float)

    attention_module = hk.transform(self_attention)
    params = attention_module.init(random_key, inputs, attn_mask)
    out = attention_module.apply(params, random_key, inputs, attn_mask)

    pytest.assume(out["embeddings"].shape == (batch_size, seq_len, embed_dim))


@pytest.mark.parametrize("seq_len", [256])
@pytest.mark.parametrize("num_heads", [20])
@pytest.mark.parametrize("block_size", [32])
@pytest.mark.parametrize("num_rand_blocks", [3])
@pytest.mark.parametrize("embed_dim", [128])
def test_get_block_rand_mask(
    seq_len: int, num_heads: int, block_size: int, num_rand_blocks: int, embed_dim: int
) -> None:
    """
    Tests that the random mask is correctly defined and shaped.
    """
    random_key = jax.random.PRNGKey(0)

    def get_block_rand_mask(output_seq_len: int, input_seq_len: int) -> jnp.ndarray:
        random_key = jax.random.PRNGKey(0)
        config = BigbirdConfig(
            alphabet_size=10,
            max_positions=1024,
            embed_dim=embed_dim,
            ffn_embed_dim=4096,
            attention_heads=8,
            num_layers=6,
            block_size=block_size,
            num_rand_blocks=num_rand_blocks,
            pad_token_id=0,
            mask_token_id=1,
        )
        model = BigbirdTransformer(config)

        return model._attention_block(0).sa_layer.get_block_rand_mask(
            rng_key=random_key,
            output_seq_len=output_seq_len,
            input_seq_len=input_seq_len,
        )  # type: ignore

    block_rand_mask = hk.transform(get_block_rand_mask)
    params = block_rand_mask.init(
        random_key,
        seq_len,
        seq_len,
    )
    rand_attn = block_rand_mask.apply(
        params,
        None,
        seq_len,
        seq_len,
    )
    pytest.assume(rand_attn.shape == (seq_len // block_size - 2, num_rand_blocks))
