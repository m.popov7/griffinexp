import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.regularization.tokens_dropout import TokensDropout


@pytest.mark.parametrize("mask_ratio", [0.5, 0.2])
@pytest.mark.parametrize("mask_prob", [0.15, 0.35])
@pytest.mark.parametrize("embed_dim", [5, 10])
@pytest.mark.parametrize(
    "tokens", [jnp.array([[4, 1, 6, 2, 3, 1, 0, 0], [1, 10, 3, 7, 1, 2, 3, 0]])]
)
def test_tokens_dropout(
    mask_ratio: float, mask_prob: float, embed_dim: int, tokens: jnp.array
) -> None:

    """
    Checks that the output of the TokensDropout layer is as expected when
    tokens as shape (batch x seq_len) and embeddings of shape
    (batch x seq_len x num_heads x embed_dim) are passed as input.
    Specifically checks that all masked tokens have zero vectors
    for embedding and all non-mask tokens are scaled according to
    mask_ratio and mask_prob
    """
    # batch x seq_length x embed_dim
    x = jax.random.normal(jax.random.PRNGKey(0), list(tokens.shape) + [embed_dim])
    pad_id, mask_id = 0, 1

    def forward_fn(x: jnp.array, tokens: jnp.array) -> jnp.array:
        dropout = TokensDropout(
            embed_dim=embed_dim,
            pad_token_id=pad_id,
            mask_token_id=mask_id,
            masking_ratio=mask_ratio,
            masking_prob=mask_prob,
        )
        return dropout(x, tokens)

    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))
    params = forward_fn_trans.init(
        jax.random.PRNGKey(0), jnp.zeros_like(x), jnp.zeros_like(tokens)
    )
    x_dropout = forward_fn_trans.apply(params, x=x, tokens=tokens)

    ratio = jnp.where(tokens == mask_id, 1, 0).sum(-1) / jnp.where(
        tokens == pad_id, 0, 1
    ).sum(-1)
    scaling_factor = (1 - mask_ratio * mask_prob) / (
        1 - jnp.reshape(ratio, (tokens.shape[0], 1, 1))
    )

    mask_idxs = jnp.repeat(
        jnp.where(tokens == mask_id, True, False)[..., None], embed_dim, -1
    )

    # check that mask tokens have zero vectors for embeddings
    pytest.assume(x_dropout[mask_idxs].all() == 0)
    # check that all non-mask tokens have proper scaling
    pytest.assume(jnp.allclose(x_dropout[~mask_idxs], (x * scaling_factor)[~mask_idxs]))
