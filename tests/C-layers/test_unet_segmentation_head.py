import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.heads.unet_segmentation_head import UNET1DSegmentationHead


@pytest.mark.parametrize("embed_dim", [512])
@pytest.mark.parametrize("batch_size", [1, 2])
@pytest.mark.parametrize("num_classes", [5])
@pytest.mark.parametrize("seq_length", [128, 1024])
def test_unet_segmentation_head(
    batch_size: int, embed_dim: int, seq_length: int, num_classes: int
) -> None:
    """Shape test on unet segmentation head."""
    random_key = jax.random.PRNGKey(0)

    model_input = jax.random.uniform(
        random_key, shape=(batch_size, seq_length, embed_dim)
    )

    def segmentation_fn(x: jnp.ndarray) -> jnp.ndarray:
        x = UNET1DSegmentationHead(
            num_classes=num_classes,
            output_channels_list=(64, 128, 256),
            activation_fn="swish",
            upsampling_interpolation_method="nearest",
        )(x)

        return x

    segmentation_fn = hk.transform(segmentation_fn)
    params = segmentation_fn.init(random_key, model_input)  # type: ignore
    model_output = jax.jit(segmentation_fn.apply)(  # type: ignore
        params, random_key, model_input
    )

    pytest.assume(model_output.shape == (batch_size, seq_length, num_classes * 2))


@pytest.mark.parametrize("embed_dim", [512])
@pytest.mark.parametrize("batch_size", [1])
@pytest.mark.parametrize("num_classes", [5])
@pytest.mark.parametrize("seq_length", [38, 501])
def test_unet_segmentation_head_errors(
    batch_size: int, embed_dim: int, seq_length: int, num_classes: int
) -> None:
    """Shape test on unet segmentation head."""
    with pytest.raises(ValueError):
        random_key = jax.random.PRNGKey(0)

        model_input = jax.random.uniform(
            random_key, shape=(batch_size, seq_length, embed_dim)
        )

        def segmentation_fn(x: jnp.ndarray) -> jnp.ndarray:
            x = UNET1DSegmentationHead(
                num_classes=num_classes,
                output_channels_list=(64, 128, 256),
                activation_fn="swish",
                upsampling_interpolation_method="nearest",
            )(x)

            return x

        segmentation_fn = hk.transform(segmentation_fn)
        params = segmentation_fn.init(random_key, model_input)  # type: ignore
        model_output = jax.jit(segmentation_fn.apply)(  # type: ignore
            params, random_key, model_input
        )

        pytest.assume(model_output.shape == (batch_size, seq_length, num_classes * 2))
