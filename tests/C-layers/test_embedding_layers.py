from math import ceil
from typing import Callable, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest

from trix.layers.positional.esm_rotary import (
    UPPER_FREQ,
    RotaryEmbedding,
    RotaryEmbeddingConfig,
)
from trix.layers.positional.sinusoidal import SinusoidalPositionalEmbedding
from trix.models.esm.layers import ESMSinusoidalPositionalEmbedding


@pytest.mark.parametrize(
    "tokens", [jax.random.randint(jax.random.PRNGKey(0), (3, 1024), 0, 10)]
)
@pytest.mark.parametrize("embed_dim", [5, 100])
def test_sinusoidal_positional_embedding(tokens: jnp.ndarray, embed_dim: int) -> None:
    """
    Checks that the sinusoidal positional embedding layer computes
    the proper output of shape (batch x seq_length x embed_dim)
    given an input of tokens of shape (batch x seq_length).
    """

    def forward_fn(x: jnp.ndarray) -> jnp.ndarray:
        sin_pos_embed_layer = SinusoidalPositionalEmbedding(embed_dim)
        return sin_pos_embed_layer(x)

    # calculate layer positional encodings
    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))
    params = forward_fn_trans.init(jax.random.PRNGKey(0), tokens)
    pos_embeddings = forward_fn_trans.apply(params, tokens)

    seq_length = tokens.shape[1]

    # calculate expected tensor
    expected_pos_embeddings = jnp.zeros((seq_length, embed_dim), dtype=float)
    position = jnp.arange(0, seq_length).reshape(seq_length, 1)

    freqs = jnp.exp(
        jnp.log(position)
        - (2 * jnp.arange(0, ceil(embed_dim / 2)) / embed_dim) * jnp.log(UPPER_FREQ)
    )

    # odd index
    expected_pos_embeddings = expected_pos_embeddings.at[
        :, 1 : embed_dim // 2 * 2 : 2
    ].set(jnp.cos(freqs)[:, : embed_dim // 2])
    # even index
    expected_pos_embeddings = expected_pos_embeddings.at[:, 0::2].set(jnp.sin(freqs))

    # batch broadcast
    expected_pos_embeddings = jnp.broadcast_to(
        expected_pos_embeddings, pos_embeddings.shape
    )

    pytest.assume(jnp.allclose(expected_pos_embeddings, pos_embeddings))


@pytest.mark.parametrize(
    "tokens",
    [
        jnp.array(
            [
                [4, 0, 4, 3, 9, 0, 1],
                [3, 6, 6, 5, 7, 1, 4],
                [2, 9, 0, 5, 6, 5, 1],
            ]
        )
    ],
)
@pytest.mark.parametrize("embed_dim", [4])
@pytest.mark.parametrize("padding_idx", [1])
def test_esm_sinusoidal_positional_embedding(
    tokens: jnp.array, embed_dim: int, padding_idx: int
) -> None:
    """
    Checks that our ESM sinusoidal positional embedding layer output
    matches the output that was calculated directly from the ESM repo.
    """

    def forward_fn(x: jnp.array) -> jnp.array:
        # ESM specific layer
        sin_pos_embed_layer = ESMSinusoidalPositionalEmbedding(embed_dim, padding_idx)

        return sin_pos_embed_layer(x)

    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))
    params = forward_fn_trans.init(jax.random.PRNGKey(0), jnp.ones_like(tokens))
    pos_embeddings = forward_fn_trans.apply(params, tokens)

    expected_pos_embeddings = jnp.array(
        [
            [
                [9.0929741e-01, 1.9999998e-04, -4.1614684e-01, 1.0000000e00],
                [1.4112000e-01, 2.9999996e-04, -9.8999250e-01, 9.9999994e-01],
                [-7.5680250e-01, 3.9999996e-04, -6.5364361e-01, 9.9999994e-01],
                [-9.5892429e-01, 4.9999997e-04, 2.8366220e-01, 9.9999988e-01],
                [-2.7941549e-01, 5.9999985e-04, 9.6017027e-01, 9.9999982e-01],
                [6.5698659e-01, 6.9999986e-04, 7.5390226e-01, 9.9999976e-01],
                [0.0000000e00, 0.0000000e00, 0.0000000e00, 0.0000000e00],
            ],
            [
                [9.0929741e-01, 1.9999998e-04, -4.1614684e-01, 1.0000000e00],
                [1.4112000e-01, 2.9999996e-04, -9.8999250e-01, 9.9999994e-01],
                [-7.5680250e-01, 3.9999996e-04, -6.5364361e-01, 9.9999994e-01],
                [-9.5892429e-01, 4.9999997e-04, 2.8366220e-01, 9.9999988e-01],
                [-2.7941549e-01, 5.9999985e-04, 9.6017027e-01, 9.9999982e-01],
                [0.0000000e00, 0.0000000e00, 0.0000000e00, 0.0000000e00],
                [9.8935825e-01, 7.9999986e-04, -1.4550003e-01, 9.9999970e-01],
            ],
            [
                [9.0929741e-01, 1.9999998e-04, -4.1614684e-01, 1.0000000e00],
                [1.4112000e-01, 2.9999996e-04, -9.8999250e-01, 9.9999994e-01],
                [-7.5680250e-01, 3.9999996e-04, -6.5364361e-01, 9.9999994e-01],
                [-9.5892429e-01, 4.9999997e-04, 2.8366220e-01, 9.9999988e-01],
                [-2.7941549e-01, 5.9999985e-04, 9.6017027e-01, 9.9999982e-01],
                [6.5698659e-01, 6.9999986e-04, 7.5390226e-01, 9.9999976e-01],
                [0.0000000e00, 0.0000000e00, 0.0000000e00, 0.0000000e00],
            ],
        ]
    )

    pytest.assume(jnp.allclose(expected_pos_embeddings, pos_embeddings))


def rotary_positional_embedding_test_parametrization(
    test_function: Callable,
) -> Callable:
    parametrizations = [
        pytest.mark.parametrize("seq_length", [1, 10, 256]),
        pytest.mark.parametrize("embed_dim", [4, 16]),
        pytest.mark.parametrize("num_heads", [5, 10]),
        pytest.mark.parametrize("rescaling_factor", [2, None]),
    ]
    for parametrization in parametrizations:
        test_function = parametrization(test_function)
    return test_function


@rotary_positional_embedding_test_parametrization
def test_rotary_positional_embedding(
    seq_length: int, num_heads: int, embed_dim: int, rescaling_factor: float
) -> None:
    """
    Checks that the output from the RotaryEmbedding layer is as expected when given a
    key and query inputs of shape (batch x seq_length x num_heads x embed_dim)
    """
    # batch x seq_length x num_heads x dim
    queries = jax.random.normal(
        jax.random.PRNGKey(0), shape=(5, seq_length, num_heads, embed_dim)
    )
    keys = jax.random.normal(
        jax.random.PRNGKey(1), shape=(5, seq_length, num_heads, embed_dim)
    )

    def forward_fn(x: jnp.array, y: jnp.array) -> Tuple[jnp.array, jnp.array]:
        rotary_embedding_config = RotaryEmbeddingConfig(
            rescaling_factor=rescaling_factor
        )
        rot_embed_layer = RotaryEmbedding(embed_dim, rotary_embedding_config)
        return rot_embed_layer(x, y)  # type: ignore

    # calculate layer positional encodings
    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))
    params = forward_fn_trans.init(
        jax.random.PRNGKey(0), jnp.ones_like(queries), jnp.ones_like(keys)
    )
    queries_rot, keys_rot = forward_fn_trans.apply(params, queries, keys)

    seq_length = queries.shape[1]

    # embedding dim  --> frequency  and positions
    positions = np.arange(0, seq_length).reshape(seq_length, 1)

    if rescaling_factor is not None:
        updated_base = UPPER_FREQ * (rescaling_factor ** (embed_dim / (embed_dim - 2)))
    else:
        updated_base = UPPER_FREQ

    freqs = 1 / updated_base ** (np.arange(0, embed_dim, 2) / embed_dim)

    # (1,seq_length, 1 , embed_dim)
    sin = np.sin(np.concatenate([positions * freqs, positions * freqs], axis=-1))[
        None, :, None, :
    ]
    cos = np.cos(np.concatenate([positions * freqs, positions * freqs], axis=-1))[
        None, :, None, :
    ]

    # (batch,seq_length, num_heads, embed_dim)
    queries_neg = jnp.concatenate(
        [-queries[..., embed_dim // 2 :], queries[..., : embed_dim // 2]], axis=-1
    )
    queries_rot_expected = queries * cos + queries_neg * sin

    # (batch,seq_length, num_heads, embed_dim)
    keys_neg = jnp.concatenate(
        [-keys[..., embed_dim // 2 :], keys[..., : embed_dim // 2]], axis=-1
    )
    keys_rot_expected = keys * cos + keys_neg * sin
    pytest.assume(jnp.allclose(queries_rot_expected, queries_rot))
    pytest.assume(jnp.allclose(keys_rot_expected, keys_rot))


@rotary_positional_embedding_test_parametrization
def test_rotary_positional_embedding_bf16_stability(
    seq_length: int,
    num_heads: int,
    embed_dim: int,
    rescaling_factor: float,
) -> None:
    """
    Check that the output of rotary embedding between f32 and bf16 is not
    significantly different.
    """
    # batch x seq_length x num_heads x dim
    queries = jax.random.normal(
        jax.random.PRNGKey(0), shape=(5, seq_length, num_heads, embed_dim)
    )
    keys = jax.random.normal(
        jax.random.PRNGKey(1), shape=(5, seq_length, num_heads, embed_dim)
    )

    def forward_fn(x: jnp.array, y: jnp.array) -> Tuple[jnp.array, jnp.array]:
        rotary_embedding_config = RotaryEmbeddingConfig(
            rescaling_factor=rescaling_factor
        )
        rot_embed_layer = RotaryEmbedding(embed_dim, rotary_embedding_config)
        return rot_embed_layer(x, y)  # type: ignore

    # calculate layer positional encodings
    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))

    queries_rot_f32, keys_rot_f32 = forward_fn_trans.apply({}, queries, keys)
    queries_rot_bf16, keys_rot_bf16 = forward_fn_trans.apply(
        {}, queries.astype(jnp.bfloat16), keys.astype(jnp.bfloat16)
    )

    atol = 0.1
    pytest.assume(
        np.allclose(queries_rot_f32, queries_rot_bf16.astype(jnp.float32), atol=atol)
    )
    pytest.assume(
        np.allclose(keys_rot_f32, keys_rot_bf16.astype(jnp.float32), atol=atol)
    )
