import haiku as hk
import jax
import jax.numpy as jnp
import pytest
from haiku.data_structures import to_immutable_dict, to_mutable_dict

from trix.layers.heads.lm_head import SimpleLMHead

jax.config.update("jax_platform_name", "cpu")


@pytest.mark.parametrize("embed_dim", [5, 10])
@pytest.mark.parametrize("alphabet_size", [1, 3])
@pytest.mark.parametrize("seq_len", [1, 7])
@pytest.mark.parametrize("input_dim", [1, 9])
def test_simple_lm_head(
    embed_dim: int, alphabet_size: int, seq_len: int, input_dim: int
) -> None:
    """Tests is the SimpleLMHead outputs are consistent with a hardcoded version."""

    def head_fn(x: jnp.ndarray) -> jnp.ndarray:
        lm_head = SimpleLMHead(embed_dim=embed_dim, alphabet_size=alphabet_size)
        return lm_head(x)

    head = hk.without_apply_rng(hk.transform(head_fn))

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)
    mock_inputs = jax.random.normal(subkey, shape=(1, seq_len, input_dim))

    random_key, subkey = jax.random.split(random_key)
    head_params = head.init(subkey, mock_inputs)
    w = head_params["simple_lm_head/~/lm_final_fc"]["w"]
    b = head_params["simple_lm_head/~/lm_final_fc"]["b"]

    def linear_layer_fn(x: jnp.ndarray) -> jnp.ndarray:
        return hk.Linear(alphabet_size, name="linear")(x)

    linear = hk.without_apply_rng(hk.transform(linear_layer_fn))

    random_key, subkey = jax.random.split(random_key)
    linear_params = linear.init(subkey, mock_inputs)

    linear_params = to_mutable_dict(linear_params)
    linear_params["linear"]["w"] = w
    linear_params["linear"]["b"] = b
    linear_params = to_immutable_dict(linear_params)

    outs = head.apply(head_params, mock_inputs)
    logits = linear.apply(linear_params, mock_inputs)

    pytest.assume(jnp.allclose(logits, outs["logits"]))
