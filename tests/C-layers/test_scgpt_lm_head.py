import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.scgpt.layers import MaskedLMHead


@pytest.mark.parametrize("embed_dim", [512, 768])
@pytest.mark.parametrize("output_dim", [1, 51, 60211])
@pytest.mark.parametrize("explicit_zero_prob", [True, False])
def test_maskedlmhead(
    embed_dim: int, output_dim: int, explicit_zero_prob: bool
) -> None:
    """
    Tests that masked lm head shapes are consistent.
    """

    random_key = jax.random.PRNGKey(0)
    mock_inputs = jax.random.normal(random_key, shape=(1, 20, embed_dim))

    def masked_lm_fn(x: jnp.ndarray) -> jnp.ndarray:
        masked_lm_head = MaskedLMHead(
            embed_dim=embed_dim,
            output_dim=output_dim,
            explicit_zero_prob=explicit_zero_prob,
        )
        return masked_lm_head(x)

    forward_fn = hk.transform(masked_lm_fn)

    params = forward_fn.init(random_key, mock_inputs)
    outputs = forward_fn.apply(params, random_key, mock_inputs)
    if output_dim == 1:
        pytest.assume(
            outputs["pred"].shape == (1, 20), "output shape is incorrect for dim=1"
        )
    else:
        pytest.assume(
            outputs["pred"].shape == (1, 20, output_dim),
            "output shape is incorrect for dim>1",
        )

    if explicit_zero_prob:
        pytest.assume("zero_probs" in outputs, "zero_probs not in outputs")
        pytest.assume(
            outputs["zero_probs"].shape == (1, 20), "zero_logit shape is incorrect"
        )
