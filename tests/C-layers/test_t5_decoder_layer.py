from typing import Callable

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.t5.decoder import T5DecoderConfig, T5DecoderOnlyLayer


@pytest.mark.parametrize("seq_len", [256])
@pytest.mark.parametrize("embed_dim", [128])
@pytest.mark.parametrize("attention_embed_dim", [128, 64])
@pytest.mark.parametrize("batch_size", [2])
@pytest.mark.parametrize("num_heads", [4])
@pytest.mark.parametrize("ffn_embed_dim", [512])
@pytest.mark.parametrize("use_glu_in_ffn", [True, False])
@pytest.mark.parametrize("dropout_rate", [0.0, 0.1])
def test_t5_decoder_only_layer(
    seq_len: int,
    num_heads: int,
    embed_dim: int,
    attention_embed_dim: int,
    batch_size: int,
    ffn_embed_dim: int,
    use_glu_in_ffn: bool,
    dropout_rate: float,
) -> None:
    """
    Test T5 decoder only layer to ensure deterministic output
    and expected shapes.
    """
    config = T5DecoderConfig(
        embed_dim=embed_dim,
        attention_embed_dim=attention_embed_dim,
        num_heads=num_heads,
        ffn_embed_dim=ffn_embed_dim,
        use_glu_in_ffn=use_glu_in_ffn,
    )

    random_key = jax.random.PRNGKey(0)
    token_embeddings = jax.random.uniform(
        key=random_key, shape=(batch_size, seq_len, embed_dim)
    )

    def build_layer(config: T5DecoderConfig) -> Callable:
        def t5_decoder_layer(
            decoder_token_ids: jnp.ndarray,
            dropout_rate: float = 0.0,
        ) -> jnp.ndarray:
            layer = T5DecoderOnlyLayer(
                embed_dim=config.embed_dim,
                attention_embed_dim=config.attention_embed_dim,
                num_heads=config.num_heads,
                ffn_embed_dim=config.ffn_embed_dim,
                use_glu_in_ffn=config.use_glu_in_ffn,
            )
            return layer(decoder_token_ids, dropout_rate)

        return t5_decoder_layer

    layer = build_layer(config)
    layer = hk.transform(layer)

    params = layer.init(random_key, token_embeddings)
    output = layer.apply(params, random_key, token_embeddings)
    pytest.assume(
        output.shape == (batch_size, seq_len, embed_dim), "Output shape is incorrect."
    )

    random_key_2 = jax.random.PRNGKey(1)
    output_2 = layer.apply(params, random_key_2, token_embeddings)
    pytest.assume(
        jnp.allclose(output, output_2),
        "random key changes outputs with dropout disabled.",
    )

    # Test dropout implementation
    if dropout_rate > 0:
        output3 = layer.apply(
            params, random_key, token_embeddings, dropout_rate=dropout_rate
        )
        pytest.assume(
            not jnp.allclose(output, output3),
            "random key does not change outputs with dropout enabled.",
        )
