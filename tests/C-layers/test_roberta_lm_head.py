import collections

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.heads.lm_head import RobertaLMHead


@pytest.mark.parametrize("embed_dim", [5])
@pytest.mark.parametrize("alphabet_size", [10])
@pytest.mark.parametrize("seq_len", [20])
@pytest.mark.parametrize("input_dim", [4])
def test_roberta_lm_head(
    embed_dim: int, alphabet_size: int, seq_len: int, input_dim: int
) -> None:
    """
    Tests that RobertaLMHead outputs are consistent with a hardcoded version.
    """

    def roberta_lm_fn(x: jnp.ndarray) -> jnp.ndarray:
        roberta_lm_head = RobertaLMHead(
            embed_dim=embed_dim, alphabet_size=alphabet_size
        )
        return roberta_lm_head(x)

    forward_fn = hk.transform(roberta_lm_fn)

    random_key = jax.random.PRNGKey(0)
    mock_inputs = jax.random.normal(random_key, shape=(1, seq_len, input_dim))

    # Weights and biases
    w_1 = jax.random.normal(random_key, shape=(input_dim, embed_dim))
    random_key, _ = jax.random.split(random_key)
    b_1 = jax.random.normal(random_key, shape=(embed_dim,))
    random_key, _ = jax.random.split(random_key)
    w_2 = jax.random.normal(random_key, shape=(embed_dim, alphabet_size))
    random_key, _ = jax.random.split(random_key)
    b_2 = jax.random.normal(random_key, shape=(alphabet_size,))
    random_key, _ = jax.random.split(random_key)
    scale_1 = jnp.abs(jax.random.normal(random_key, shape=(input_dim,)))
    random_key, _ = jax.random.split(random_key)
    offset_1 = jax.random.normal(random_key, shape=(input_dim,))
    random_key, _ = jax.random.split(random_key)
    scale_2 = jnp.abs(jax.random.normal(random_key, shape=(embed_dim,)))
    random_key, _ = jax.random.split(random_key)
    offset_2 = jax.random.normal(random_key, shape=(embed_dim,))
    random_key, _ = jax.random.split(random_key)

    eps = 1e-5
    x = (
        mock_inputs - mock_inputs.mean(axis=(0, 2), keepdims=True)
    ) * scale_1 / jnp.sqrt(mock_inputs.var(axis=(0, 2), keepdims=True) + eps) + offset_1
    embeds = x.copy()
    x = x @ w_1 + b_1
    x = jax.nn.gelu(x, approximate=False)
    x = (x - x.mean(axis=(0, 2), keepdims=True)) * scale_2 / jnp.sqrt(
        x.var(axis=(0, 2), keepdims=True) + eps
    ) + offset_2
    x = x @ w_2 + b_2

    parameters = collections.defaultdict(dict)  # type: ignore

    parameters["roberta_lm_head/~/emb_layer_norm_after"]["scale"] = scale_1
    parameters["roberta_lm_head/~/emb_layer_norm_after"]["offset"] = offset_1
    parameters["roberta_lm_head/~/lm_head_layer_norm"]["scale"] = scale_2
    parameters["roberta_lm_head/~/lm_head_layer_norm"]["offset"] = offset_2
    parameters["roberta_lm_head/~/lm_head_fc_1"]["w"] = w_1
    parameters["roberta_lm_head/~/lm_head_fc_1"]["b"] = b_1
    parameters["roberta_lm_head/~/lm_final_fc"]["w"] = w_2
    parameters["roberta_lm_head/~/lm_final_fc"]["b"] = b_2

    outs = forward_fn.apply(parameters, random_key, mock_inputs)

    pytest.assume(jnp.allclose(x, outs["logits"]))
    pytest.assume(jnp.allclose(embeds, outs["embeddings"]))
