import haiku as hk
import jax
import pytest

from trix.models.scgpt.layers import build_scgpt_gene_encoder_fn


@pytest.mark.parametrize("embed_dim", [512])
@pytest.mark.parametrize("batch_size", [1, 2])
@pytest.mark.parametrize("seq_length", [128, 1024])
@pytest.mark.parametrize("num_embeddings", [512])
def test_scgpt_gene_encoder(
    num_embeddings: int, embed_dim: int, batch_size: int, seq_length: int
) -> None:
    """Shape test on scgpt gene encoder."""
    random_key = jax.random.PRNGKey(0)

    model_input = jax.random.randint(
        random_key,
        shape=(batch_size, seq_length),
        minval=0,
        maxval=num_embeddings,
    )

    gene_encoder_fn = build_scgpt_gene_encoder_fn(
        num_embeddings=num_embeddings,
        embedding_dim=embed_dim,
    )

    gene_encoder_fn = hk.transform(gene_encoder_fn)
    params = gene_encoder_fn.init(random_key, model_input)  # type: ignore
    model_output = jax.jit(gene_encoder_fn.apply)(  # type: ignore
        params, random_key, model_input
    )

    pytest.assume(
        model_output.shape == (batch_size, seq_length, embed_dim),
        "Shape does not match",
    )
