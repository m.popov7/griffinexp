from typing import Callable

import haiku as hk
import jax
import jax.numpy as jnp
import pytest
from haiku.data_structures import to_immutable_dict, to_mutable_dict

from trix.layers.attention.cross_attention import CrossAttentionBlock
from trix.layers.attention.multi_head_attention import MultiHeadAttention
from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.models.t5.layers import T5MultiHeadAttention
from trix.types import AttentionMask, TransformerOutput


@pytest.mark.parametrize("is_causal_mask", (False, True))
@pytest.mark.parametrize("layer_norm_eps", (1e-5, 1e-3))
@pytest.mark.parametrize("pre_layer_norm", (False, True))
def test_self_attention_weights(
    is_causal_mask: bool, layer_norm_eps: float, pre_layer_norm: bool
) -> None:
    """Tests on a simple example if the attention weights are computed correctly given
    the mask attention matrix (covers normal and causal masks).
    """

    def self_attention(x: jnp.ndarray, mask: AttentionMask) -> jnp.ndarray:
        layer = SelfAttentionBlock(
            num_heads=1,
            embed_dim=4,
            ffn_embed_dim=16,
            layer_norm_eps=layer_norm_eps,
            pre_layer_norm=pre_layer_norm,
        )

        return layer.self_attention(x, attention_mask=mask)  # type: ignore

    attention_module = hk.transform(self_attention)

    random_key = jax.random.PRNGKey(0)

    x0 = jnp.array(
        [
            [1.0, 1.0],
            [2.0, 0.0],
            [0.0, 0.0],
        ]
    )

    e0 = jax.nn.one_hot(x0, 4)
    print("Input:\n", x0)
    mask = x0 != 0
    print("Mask:\n", mask * 1.0)
    mask = mask[:, None]
    mask = jnp.einsum("bhT, bht->bhtT", mask, mask)

    if is_causal_mask:
        mask = jnp.tril(mask)

    params = attention_module.init(random_key, e0, mask)

    params = jax.tree_map(lambda x: jnp.zeros_like(x), params)
    params = to_mutable_dict(params)
    params["self_attention_block/~/self_attention/key"]["w"] = jnp.ones((4, 4))
    params["self_attention_block/~/self_attention/query"]["w"] = jnp.ones((4, 4))
    params = to_immutable_dict(params)
    out0 = attention_module.apply(params, random_key, e0, mask)

    print("Output:\n", out0["embeddings"])
    print("Attention weights:\n", out0["attention_weights"])

    if is_causal_mask:

        result = (
            jnp.array(
                [
                    [[1.0, 0.0], [0.5, 0.5]],
                    [[1.0, 0.0], [0.5, 0.5]],
                    [[0.5, 0.5], [0.5, 0.5]],
                ]
            )[:, None]
            * mask
        )
    else:
        result = (
            jnp.array(
                [
                    [[0.5, 0.5], [0.5, 0.5]],
                    [[1.0, 0.0], [0.5, 0.5]],
                    [[0.5, 0.5], [0.5, 0.5]],
                ]
            )[:, None]
            * mask
        )
    error_text = (
        f"Failed with "
        f"{is_causal_mask * 'causal' + (1 - is_causal_mask) * 'normal'}"
        f" mask"
    )
    pytest.assume(jnp.allclose(out0["attention_weights"] * mask, result), error_text)


@pytest.mark.parametrize("is_causal_mask", (False, True))
@pytest.mark.parametrize("layer_norm_eps", (1e-5, 1e-3))
@pytest.mark.parametrize("pre_layer_norm", (False, True))
def test_self_attention_values(
    is_causal_mask: bool, layer_norm_eps: float, pre_layer_norm: bool
) -> None:
    """Tests on a random example if the embeddings and attention masks returned by the
    attention layers have consistent values with numpy-computed values.
    """

    num_heads = 2
    embed_dim = 4

    def self_attention(x: jnp.ndarray, mask: AttentionMask) -> jnp.ndarray:
        layer = SelfAttentionBlock(
            num_heads=num_heads,
            embed_dim=embed_dim,
            ffn_embed_dim=16,
            layer_norm_eps=layer_norm_eps,
            pre_layer_norm=pre_layer_norm,
        )

        return layer.self_attention(x, attention_mask=mask)  # type: ignore

    attention_module = hk.transform(self_attention)
    random_key = jax.random.PRNGKey(0)

    random_key, subkey = jax.random.split(random_key)
    x0 = jax.random.randint(subkey, minval=0, maxval=4, shape=(3, 5))

    e0 = jax.nn.one_hot(x0, 4)
    print("Input:\n", x0)
    mask = x0 != 0
    print("Mask:\n", mask * 1.0)
    mask = mask[:, None]
    mask = jnp.einsum("bhT, bht->bhtT", mask, mask)

    if is_causal_mask:
        mask = jnp.tril(mask)

    random_key, subkey = jax.random.split(random_key)
    params = attention_module.init(subkey, e0, mask)

    random_key, subkey = jax.random.split(random_key)
    out0 = attention_module.apply(params, subkey, e0, mask)

    print("Output:\n", out0["embeddings"])
    print("Attention weights:\n", out0["attention_weights"])

    def heads_layer(x: jnp.ndarray) -> jnp.ndarray:
        layer = MultiHeadAttention(
            num_heads=num_heads, key_size=embed_dim // num_heads, name="self_attention"
        )
        query_heads = layer._linear_projection_he_init(x, layer.key_size, "query")
        key_heads = layer._linear_projection_he_init(x, layer.key_size, "key")
        value_heads = layer._linear_projection_he_init(x, layer.value_size, "value")

        return query_heads, key_heads, value_heads

    heads = hk.transform(heads_layer)
    heads_params = {}
    heads_params["query"] = params["self_attention_block/~/self_attention/query"]
    heads_params["key"] = params["self_attention_block/~/self_attention/key"]
    heads_params["value"] = params["self_attention_block/~/self_attention/value"]
    query_heads, key_heads, value_heads = heads.apply(heads_params, subkey, e0)
    query_heads = query_heads.reshape((*e0.shape[:-1], num_heads, -1))
    key_heads = key_heads.reshape((*e0.shape[:-1], num_heads, -1))
    value_heads = value_heads.reshape((*e0.shape[:-1], num_heads, -1))

    attention_logits = jnp.einsum("...thd,...Thd->...htT", query_heads, key_heads)
    sqrt_key_size = (embed_dim // num_heads) ** 0.5
    attention_logits = attention_logits / sqrt_key_size
    attention_logits = jnp.where(mask, attention_logits, -1e30)

    attention_weights = jax.nn.softmax(attention_logits)
    attention = jnp.einsum("...htT,...Thd->...thd", attention_weights, value_heads)

    # Concatenate attention matrix of all heads into a single vector.
    attention_vec = jnp.reshape(attention, (*e0.shape[:-1], -1))

    def output_layer(x: jnp.ndarray) -> jnp.ndarray:
        layer = MultiHeadAttention(
            num_heads=num_heads, key_size=embed_dim // num_heads, name="self_attention"
        )

        return hk.Linear(layer.model_size, name="mha_output")(x)

    output_params = {}
    output_params["mha_output"] = params[
        "self_attention_block/~/self_attention/mha_output"
    ]

    output_l = hk.transform(output_layer)
    output = output_l.apply(output_params, subkey, attention_vec)
    error_text = (
        f"Failed with "
        f"{is_causal_mask * 'causal' + (1 - is_causal_mask) * 'normal'} "
        f"mask"
    )
    pytest.assume(
        jnp.allclose(out0["attention_weights"] * mask, attention_weights * mask),
        error_text,
    )
    pytest.assume(jnp.allclose(out0["embeddings"], output), error_text)


@pytest.mark.parametrize(
    ("num_heads, embed_dim, ffn_embed_dim"), [[2, 4, 8], [2, 3, 6]]
)
@pytest.mark.parametrize("use_glu_in_ffn", [True, False])
@pytest.mark.parametrize(
    "ffn_activation_name", ["gelu", "gelu-no-approx", "relu", "swish", "linear"]
)
@pytest.mark.parametrize("layer_norm_eps", (1e-5, 1e-3))
@pytest.mark.parametrize("pre_layer_norm", (False, True))
def test_self_attention_block_output(
    num_heads: int,
    embed_dim: int,
    ffn_embed_dim: int,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    layer_norm_eps: float,
    pre_layer_norm: bool,
) -> None:
    """
    Tests that the final output of the self attention block is the correct shape.
    """

    def self_attention(x: jnp.ndarray, mask: AttentionMask) -> TransformerOutput:
        layer = SelfAttentionBlock(
            num_heads=num_heads,
            embed_dim=embed_dim,
            ffn_embed_dim=ffn_embed_dim,
            use_glu_in_ffn=use_glu_in_ffn,
            ffn_activation_name=ffn_activation_name,
            layer_norm_eps=layer_norm_eps,
            pre_layer_norm=pre_layer_norm,
        )
        return layer(x, attention_mask=mask)

    random_key = jax.random.PRNGKey(42)
    random_key, subkey = jax.random.split(random_key)

    x0 = jax.random.randint(subkey, minval=0, maxval=4, shape=(16, 10))

    random_key, subkey = jax.random.split(random_key)
    e0 = jax.random.normal(subkey, (16, 10, embed_dim))

    mask = x0 != 0
    mask = mask[:, None]
    mask = jnp.einsum("bhT, bht->bhtT", mask, mask)

    random_key, subkey = jax.random.split(random_key)
    attention_module = hk.transform(self_attention)

    # Test invalid config
    if embed_dim % num_heads != 0:
        with pytest.raises(ValueError):
            params = attention_module.init(subkey, jnp.ones((16, 10, embed_dim)), mask)
        return
    if ffn_activation_name not in ["gelu", "gelu-no-approx", "relu", "swish"]:
        with pytest.raises(NotImplementedError):
            params = attention_module.init(subkey, jnp.ones((16, 10, embed_dim)), mask)
        return

    params = attention_module.init(subkey, jnp.ones((16, 10, embed_dim)), mask)

    random_key, subkey = jax.random.split(random_key)
    output = attention_module.apply(params, subkey, e0, mask)

    pytest.assume(output["embeddings"].shape == (16, 10, embed_dim))


@pytest.mark.parametrize("is_causal_mask", (False, True))
def test_cross_attention_weights(is_causal_mask: bool) -> None:
    """Tests on a simple example if the attention weights are computed correctly given
    the mask attention matrix (covers normal and causal masks).
    """

    def cross_attention(
        x: jnp.ndarray, y: jnp.ndarray, mask: AttentionMask
    ) -> TransformerOutput:
        layer = CrossAttentionBlock(num_heads=1, embed_dim=4, ffn_embed_dim=512)

        return layer.cross_attention(x, y, attention_mask=mask)  # type: ignore

    attention_module = hk.transform(cross_attention)

    random_key = jax.random.PRNGKey(0)

    x0 = jnp.array(
        [
            [1.0, 1.0],
            [2.0, 0.0],
            [0.0, 0.0],
        ]
    )

    x1 = jnp.array(
        [
            [1.0, 1.0],
            [2.0, 0.0],
            [0.0, 0.0],
        ]
    )

    e0 = jax.nn.one_hot(x0, 4)
    e1 = jax.nn.one_hot(x1, 4)

    print("Input:\n", x0)
    mask = x0 != 0
    print("Mask:\n", mask * 1.0)
    mask = mask[:, None]
    mask = jnp.einsum("bhT, bht->bhtT", mask, mask) * 1.0

    if is_causal_mask:
        mask = jnp.tril(mask)

    params = attention_module.init(random_key, e0, e1, mask)

    params = jax.tree_map(lambda x: jnp.zeros_like(x), params)
    params = to_mutable_dict(params)
    print(jax.tree_map(lambda x: print(x.shape), params))
    params["cross_attention_block/~/cross_attention/key"]["w"] = jnp.ones((4, 4))
    params["cross_attention_block/~/cross_attention/query"]["w"] = jnp.ones((4, 4))

    params = to_immutable_dict(params)
    print(e0.shape)
    out0 = attention_module.apply(params, random_key, x=e0, y=e1, mask=mask)

    print("Output:\n", out0["embeddings"])
    print("Attention weights:\n", out0["attention_weights"])

    if is_causal_mask:

        result = (
            jnp.array(
                [
                    [[1.0, 0.0], [0.5, 0.5]],
                    [[1.0, 0.0], [0.5, 0.5]],
                    [[0.5, 0.5], [0.5, 0.5]],
                ]
            )[:, None]
            * mask
        )
    else:
        result = (
            jnp.array(
                [
                    [[0.5, 0.5], [0.5, 0.5]],
                    [[1.0, 0.0], [0.5, 0.5]],
                    [[0.5, 0.5], [0.5, 0.5]],
                ]
            )[:, None]
            * mask
        )
    error_text = (
        f"Failed with"
        f" {is_causal_mask * 'causal' + (1 - is_causal_mask) * 'normal'}"
        f" mask"
    )
    pytest.assume(jnp.allclose(out0["attention_weights"] * mask, result), error_text)


@pytest.mark.parametrize("is_causal_mask", (False, True))
def test_cross_attention_values(is_causal_mask: bool) -> None:
    """Tests on a random example if the embeddings and attention masks returned by the
    attention layers have consistent values with numpy-computed values.
    """

    num_heads = 2
    embed_dim = 4

    def cross_attention(
        x: jnp.ndarray, y: jnp.ndarray, mask: AttentionMask
    ) -> TransformerOutput:
        layer = CrossAttentionBlock(
            num_heads=num_heads, embed_dim=embed_dim, ffn_embed_dim=512
        )  # type: ignore

        return layer.cross_attention(x, y, attention_mask=mask)  # type: ignore

    attention_module = hk.transform(cross_attention)
    random_key = jax.random.PRNGKey(0)

    random_key, subkey = jax.random.split(random_key)
    x0 = jax.random.randint(subkey, minval=0, maxval=4, shape=(3, 5))

    random_key, subkey = jax.random.split(random_key)
    x1 = jax.random.randint(subkey, minval=0, maxval=4, shape=(3, 5))

    e0 = jax.nn.one_hot(x0, 4)
    e1 = jax.nn.one_hot(x1, 4)

    print("Input:\n", x0)
    mask = x0 != 0
    print("Mask:\n", mask * 1.0)
    mask = mask[:, None]
    mask = jnp.einsum("bhT, bht->bhtT", mask, mask) * 1.0

    if is_causal_mask:
        mask = jnp.tril(mask)

    random_key, subkey = jax.random.split(random_key)
    params = attention_module.init(subkey, x=e0, y=e1, mask=mask)

    random_key, subkey = jax.random.split(random_key)
    out0 = attention_module.apply(params, subkey, x=e0, y=e1, mask=mask)

    print("Output:\n", out0["embeddings"])
    print("Attention weights:\n", out0["attention_weights"])

    def heads_layer(
        x: jnp.ndarray, cross_attention_embeddings: jnp.ndarray
    ) -> jnp.ndarray:
        layer = MultiHeadAttention(
            num_heads=num_heads, key_size=embed_dim // num_heads, name="cross_attention"
        )
        query_heads = layer._linear_projection_he_init(x, layer.key_size, "query")
        key_heads = layer._linear_projection_he_init(
            cross_attention_embeddings, layer.key_size, "key"
        )
        value_heads = layer._linear_projection_he_init(
            cross_attention_embeddings, layer.value_size, "value"
        )

        return query_heads, key_heads, value_heads

    heads = hk.transform(heads_layer)
    heads_params = {}
    heads_params["query"] = params["cross_attention_block/~/cross_attention/query"]
    heads_params["key"] = params["cross_attention_block/~/cross_attention/key"]
    heads_params["value"] = params["cross_attention_block/~/cross_attention/value"]
    query_heads, key_heads, value_heads = heads.apply(heads_params, subkey, e0, e1)
    query_heads = query_heads.reshape((*e0.shape[:-1], num_heads, -1))
    key_heads = key_heads.reshape((*e0.shape[:-1], num_heads, -1))
    value_heads = value_heads.reshape((*e0.shape[:-1], num_heads, -1))

    attention_logits = jnp.einsum("...thd,...Thd->...htT", query_heads, key_heads)
    sqrt_key_size = (embed_dim // num_heads) ** 0.5
    attention_logits = attention_logits / sqrt_key_size
    attention_logits = jnp.where(mask, attention_logits, -1e30)

    attention_weights = jax.nn.softmax(attention_logits)
    attention = jnp.einsum("...htT,...Thd->...thd", attention_weights, value_heads)

    # Concatenate attention matrix of all heads into a single vector.
    attention_vec = jnp.reshape(attention, (*e0.shape[:-1], -1))

    def output_layer(x: jnp.ndarray) -> jnp.ndarray:
        layer = MultiHeadAttention(
            num_heads=num_heads, key_size=embed_dim // num_heads, name="cross_attention"
        )

        return hk.Linear(layer.model_size, name="mha_output")(x)

    output_params = {}
    output_params["mha_output"] = params[
        "cross_attention_block/~/cross_attention/mha_output"
    ]

    output_l = hk.transform(output_layer)
    output = output_l.apply(output_params, subkey, attention_vec)
    error_text = (
        f"Failed with "
        f"{is_causal_mask * 'causal' + (1 - is_causal_mask) * 'normal'}"
        f" mask"
    )
    pytest.assume(
        jnp.allclose(out0["attention_weights"] * mask, attention_weights * mask),
        error_text,
    )
    pytest.assume(jnp.allclose(out0["embeddings"], output), error_text)


def test_cross_attention_block_output() -> None:
    """
    Tests that the final output of the cross attention block is the correct shape.
    """

    num_heads = 1
    embed_dim = 4

    def self_attention(
        x: jnp.ndarray, y: jnp.ndarray, mask: AttentionMask
    ) -> TransformerOutput:
        layer = CrossAttentionBlock(
            num_heads=num_heads, embed_dim=embed_dim, ffn_embed_dim=16
        )
        return layer(x, y, attention_mask=mask)

    random_key = jax.random.PRNGKey(42)
    random_key, subkey = jax.random.split(random_key)

    x0 = jax.random.randint(subkey, minval=0, maxval=embed_dim, shape=(16, 10))

    random_key, subkey = jax.random.split(random_key)
    e0 = jax.random.normal(subkey, (16, 10, embed_dim))

    random_key, subkey = jax.random.split(random_key)
    e1 = jax.random.normal(subkey, (16, 10, embed_dim))

    mask = x0 != 0
    mask = mask[:, None]
    mask = jnp.einsum("bhT, bht->bhtT", mask, mask)

    attention_module = hk.transform(self_attention)
    params = attention_module.init(
        random_key, jnp.ones((16, 10, embed_dim)), jnp.ones((16, 10, embed_dim)), mask
    )

    random_key, subkey = jax.random.split(random_key)
    output = attention_module.apply(params, subkey, x=e0, y=e1, mask=mask)

    pytest.assume(output["embeddings"].shape == (16, 10, embed_dim))


@pytest.mark.parametrize("seq_len", [256])
@pytest.mark.parametrize("dropout_rate", [0.1, 0.5, 0.9])
def test_t5_multihead_attention_dropout(seq_len: int, dropout_rate: float) -> None:
    """
    Tests dropout functionality in the attention layer
    """
    token_emb_dim = 128
    num_heads = 3
    embed_dim = token_emb_dim * num_heads
    attention_embed_dim = token_emb_dim * num_heads

    queries = jnp.ones((1, seq_len, token_emb_dim))
    keys = jnp.ones((1, seq_len, token_emb_dim))
    values = jnp.ones((1, seq_len, token_emb_dim))

    def build_layer() -> Callable:
        def t5_multihead_attention_layer(
            query_inputs: jnp.ndarray,
            key_inputs: jnp.ndarray,
            value_inputs: jnp.ndarray,
            dropout_rate: float = 0.0,
        ) -> jnp.ndarray:
            attention_layer = T5MultiHeadAttention(
                embed_dim=embed_dim,
                attention_embed_dim=attention_embed_dim,
                num_heads=num_heads,
                name="t5_attention",
            )
            return attention_layer(
                query_inputs=query_inputs,
                key_inputs=key_inputs,
                value_inputs=value_inputs,
                dropout_rate=dropout_rate,
            )

        return t5_multihead_attention_layer

    attention_layer = build_layer()
    attention_layer = hk.transform(attention_layer)

    random_key = jax.random.PRNGKey(0)
    params = attention_layer.init(
        random_key,
        queries,
        keys,
        values,
        dropout_rate=0.0,
    )

    output_with_dropout = attention_layer.apply(
        params,
        random_key,
        queries,
        keys,
        values,
        dropout_rate=dropout_rate,
    )
    output_without_dropout = attention_layer.apply(
        params,
        random_key,
        queries,
        keys,
        values,
        dropout_rate=0.0,
    )

    pytest.assume(
        not jnp.all(output_with_dropout == output_without_dropout),
        "Dropout does not change the outputs",
    )

    # verify dropout is not applied during evaluation
    random_key_new = jax.random.PRNGKey(1)
    output_without_dropout_new = attention_layer.apply(
        params, random_key_new, queries, keys, values, dropout_rate=0.0
    )
    pytest.assume(
        jnp.allclose(output_without_dropout, output_without_dropout_new),
        "Dropout is applied during evaluation",
    )
