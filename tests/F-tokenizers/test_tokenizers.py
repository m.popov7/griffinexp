import functools
import itertools
import os
from typing import List, Union

import jax.numpy as jnp
import numpy as np
import pytest

import trix.tokenizers.language_models.bio
import trix.tokenizers.language_models.standard
from trix.tokenizers.language_models.codon_tokenizer import (
    CodonTokenizer,
    CodonTokenizerParams,
)
from trix.tokenizers.language_models.spm_tokenizer import SentencePieceTokenizer
from trix.utils.constants.rna import RNA_NUCLEOTIDE_TYPES
from trix.utils.rna_sequence import RNASequence
from trix.utils.s3 import init_s3_trix_bucket


# check that joined tokens equal original input, cls/eos token
def check_tokenization(
    tokenizer: trix.tokenizers.language_models.standard.StandardTokenizer,
    tokens: jnp.ndarray,
    seq: jnp.ndarray,
    cls_or_bos: Union[bool, str],
    eos: bool,
) -> None:
    """
    Checks for class/end of sentence tokens if set as true and checks
    that the original sequence can be rebuilt from the tokenizer,assuming
    no unknown tokens in the tokens sequence.
    """

    seq_length = len(tokens)

    # padding check
    idxs = np.where(np.array(tokens) == tokenizer.pad_token)[0]
    if idxs.size == 0:
        eos_idx = seq_length - 1
    else:
        pad_idx = idxs[0]
        eos_idx = pad_idx - 1
        pytest.assume(tokens[pad_idx:] == [tokenizer.pad_token] * idxs.size)

    # class/eos and original seq check
    if cls_or_bos and not eos:
        pytest.assume(tokens[0] == cls_or_bos)
        pytest.assume("".join(tokens[1 : eos_idx + 1]) == seq)
    elif cls_or_bos and eos:
        pytest.assume(tokens[0] == cls_or_bos)
        pytest.assume(tokens[eos_idx] == tokenizer.eos_token)
        pytest.assume("".join(tokens[1:eos_idx]) == seq)
    elif eos and not cls_or_bos:
        pytest.assume(tokens[eos_idx] == tokenizer.eos_token)
        pytest.assume("".join(tokens[:eos_idx]) == seq)
    elif not cls_or_bos and not eos:
        pytest.assume("".join(tokens[: eos_idx + 1]) == seq)


single_char_vocab = list(set("abcdefghijklmnopqrstuvwxyz_"))
two_char_vocab = [
    "".join(comb) for comb in itertools.product(single_char_vocab, repeat=2)
]
two_char_vocab.extend(single_char_vocab)


@pytest.mark.parametrize("extra_special", [None, ["\\$", "\\#", "\\@"]])
@pytest.mark.parametrize(
    "str_input",
    [
        "testing_basic_tokenizer",
        ["testing_the_deep_chain_transformer_repo", "testing_the_batch_tokenization"],
    ],
)
@pytest.mark.parametrize("vocab", [single_char_vocab, two_char_vocab])
@pytest.mark.parametrize("cls_token", [True, False])
@pytest.mark.parametrize("eos_token", [True, False])
@pytest.mark.parametrize("bos_token", [True, False])
@pytest.mark.parametrize("toks_ids_bool", [None, True, False])
def test_basic_tokenizer(
    str_input: Union[str, List[str]],
    vocab: list,
    cls_token: bool,
    eos_token: bool,
    bos_token: bool,
    toks_ids_bool: Union[bool, None],
    extra_special: Union[None, List[str]],
) -> None:
    """
    Checks that tokenization of text input is correct given a specific vocabulary.
    Also checks that extra special tokens are added to the dict and that if a
    toks_to_ids dict is provided that it is complete.
    """

    # check that error is raised when two prepend tokens are passed
    if cls_token and bos_token:
        with pytest.raises(ValueError):
            _ = trix.tokenizers.language_models.standard.StandardTokenizer(
                vocab,
                prepend_cls_token=cls_token,
                prepend_bos_token=bos_token,
            )
        return

    # Create either a complete or incomplete dict
    if toks_ids_bool:
        if extra_special is None:
            extra_special_list: list = []
        else:
            extra_special_list = extra_special
        toks_to_ids = {
            token: i
            for i, token in enumerate(
                vocab
                + extra_special_list
                + ["<unk>", "<pad>", "<mask>", "<cls>", "<eos>", "<bos>"]
            )
        }
        tokenizer = trix.tokenizers.language_models.standard.StandardTokenizer(
            vocab,
            prepend_cls_token=cls_token,
            prepend_bos_token=bos_token,
            append_eos_token=eos_token,
            tokens_to_ids=toks_to_ids,
            extra_special_tokens=extra_special_list,
        )

    # Given incomplete token_to_id dict check that a Value error is raised when
    elif toks_ids_bool is False:
        toks_to_ids = {token: i for i, token in enumerate(vocab) if i % 2 == 0}
        with pytest.raises(ValueError):
            _ = trix.tokenizers.language_models.standard.StandardTokenizer(
                vocab,
                prepend_cls_token=cls_token,
                prepend_bos_token=bos_token,
                append_eos_token=eos_token,
                tokens_to_ids=toks_to_ids,
                extra_special_tokens=extra_special,
            )
        return

    else:
        tokenizer = trix.tokenizers.language_models.standard.StandardTokenizer(
            vocab,
            prepend_cls_token=cls_token,
            prepend_bos_token=bos_token,
            append_eos_token=eos_token,
            tokens_to_ids=None,
            extra_special_tokens=extra_special,
        )

    # prepend token to pass to check tokenization
    if not cls_token and not bos_token:
        prepend_token = False  # type: ignore
    elif cls_token:
        prepend_token = tokenizer.class_token  # type: ignore
    elif bos_token:
        prepend_token = tokenizer.bos_token  # type: ignore

    # batch tokenization
    if isinstance(str_input, list):
        batch = tokenizer.batch_tokenize(str_input)
        for i, (tokens, _) in enumerate(batch):
            check_tokenization(
                tokenizer=tokenizer,
                tokens=tokens,
                seq=str_input[i],
                cls_or_bos=prepend_token,
                eos=eos_token,
            )

    # single sequence tokenization
    elif isinstance(str_input, str):
        tokens, _ = tokenizer.tokenize(str_input)
        check_tokenization(
            tokenizer=tokenizer,
            tokens=tokens,
            seq=str_input,
            cls_or_bos=prepend_token,
            eos=eos_token,
        )

    # check that key error is raised when searching for token not in dict
    with pytest.raises(KeyError):
        tokenizer.id_to_token(1000)

    # assert that the tokens have been added to dict
    if extra_special:
        for special_token in extra_special:
            pytest.assume(special_token in tokenizer._tokens_to_ids.keys())


@pytest.mark.parametrize(
    "str_input", ["ATCGNATCTCGATACTNNATC", ["ATCGNNATCTTTTNNN", "TNCGCGCGCGCGN"]]
)
@pytest.mark.parametrize("k_mers", [1, 2, 3])
@pytest.mark.parametrize("vocab", [list("ATCG")])
@pytest.mark.parametrize(
    "cls_token,bos_token", [(True, False), (False, True), (False, False)]
)
@pytest.mark.parametrize("eos_token", [True, False])
def test_nucleotide_kmers_tokenizer(
    str_input: Union[str, List[str]],
    vocab: list,
    k_mers: int,
    cls_token: bool,
    bos_token: bool,
    eos_token: bool,
) -> None:
    """
    Checks for proper tokenization of nucleotide sequences.
    """

    if cls_token:
        prepend_token = "<cls>"  # type: ignore
    elif bos_token:
        prepend_token = "<bos>"  # type: ignore
    else:
        prepend_token = False  # type: ignore

    # initialize tokenizer instance
    tokenizer = trix.tokenizers.language_models.bio.NucleotidesKmersTokenizer(
        k_mers,
        prepend_cls_token=cls_token,
        prepend_bos_token=bos_token,
        append_eos_token=eos_token,
    )
    # batch tokenization
    if isinstance(str_input, list):
        batch = tokenizer.batch_tokenize(str_input)
        for i, (tokens, _) in enumerate(batch):
            check_tokenization(
                tokenizer=tokenizer,
                tokens=tokens,
                seq=str_input[i],
                cls_or_bos=prepend_token,
                eos=eos_token,
            )

    # single sequence tokenization
    elif isinstance(str_input, str):
        tokens, _ = tokenizer.tokenize(str_input)
        check_tokenization(
            tokenizer=tokenizer,
            tokens=tokens,
            seq=str_input,
            cls_or_bos=prepend_token,
            eos=eos_token,
        )


@pytest.mark.parametrize(
    "sequence", [["testing_the_deep_chain_transformer_repo", "fixed_length"]]
)
@pytest.mark.parametrize("standard_tokens", [single_char_vocab])
@pytest.mark.parametrize("fixed_length", [5, 10])
def test_fixed_basic_tokenizer(
    standard_tokens: List[str], fixed_length: int, sequence: List[str]
) -> None:
    """
    Checks that an error is raised when any provided sequence is longer
    than the fixed_length arg
    """

    tokenizer = trix.tokenizers.language_models.standard.FixedSizeStandardTokenizer(
        standard_tokens, fixed_length
    )

    # check that value error occurs because of token length > fixed_length
    with pytest.raises(ValueError):
        _ = tokenizer.batch_tokenize(sequence)


@pytest.mark.parametrize("sequences", [["ATCTGATCNANAAA", "TCNCGGAGA"]])
@pytest.mark.parametrize("fixed_length", [5, 8, 20])
@pytest.mark.parametrize("vocab", [list(set("ATCG"))])
@pytest.mark.parametrize("k_mers", [2, 3])
def test_fixed_nucleotide_tokenizer(
    vocab: List[str], fixed_length: int, sequences: List[str], k_mers: int
) -> None:
    """
    Checks that an error is raised when any provided sequence is longer
    than the fixed_length arg and for correct tokenization.
    """
    tokenizer = trix.tokenizers.language_models.bio.FixedSizeNucleotidesKmersTokenizer(
        k_mers, fixed_length
    )

    basic_tokenizer = trix.tokenizers.language_models.bio.NucleotidesKmersTokenizer(
        k_mers
    )
    toks = list(zip(*basic_tokenizer.batch_tokenize(sequences)))[0]
    lengths = [len(tokenized_seq) for tokenized_seq in toks]

    # check that value error occurs because of token length > fixed_length
    if jnp.max(jnp.asarray(lengths)) > fixed_length:
        with pytest.raises(ValueError):
            _ = tokenizer.batch_tokenize(sequences)

    # check for correct tokenization
    else:
        batch = tokenizer.batch_tokenize(sequences)
        for i, (tokens, _) in enumerate(batch):
            check_tokenization(
                tokenizer=tokenizer,
                tokens=tokens,
                seq=sequences[i],
                cls_or_bos=False,
                eos=False,
            )


@pytest.mark.parametrize(
    "sequences",
    [
        [
            "ba allele: YYAMYQENVAQTDVDTLYIIYRDYTWAAQAYRWY peptide: AIIEVDRSAAK",
            "ba allele: YYATYRNIFTNTYESNLYIRYDSYTWAVLAYLWY peptide: FPNITLKII",
        ]
    ],
)
@pytest.mark.parametrize("max_length", [512, 1024])
@pytest.mark.parametrize("add_eos_token", [True, False])
def test_sentencepiece_tokenizer(
    sequences: List[str], max_length: int, add_eos_token: bool
) -> None:
    """
    Checks that an error is raised when any provided sequence is longer
    than the fixed_length arg and for correct tokenization.
    """
    s3_client, bucket = init_s3_trix_bucket()
    test_path = os.path.dirname(os.path.realpath(__file__))
    test_dir = os.path.join(test_path, "test_files")
    spm_model_path = f"{test_dir}/vocabs_neo_spm_el_ba_sep.model"
    s3_model_path = "tests/tokenizers/sentencepiece/vocabs_neo_spm_el_ba_sep.model"

    os.makedirs(test_dir, exist_ok=True)
    s3_client.download_file(bucket, s3_model_path, spm_model_path)

    tokenizer = SentencePieceTokenizer(spm_model_path, add_eos_token=add_eos_token)
    batch = tokenizer(
        sequences,
        max_length=max_length,
        padding="max_length",
    )

    tokens = jnp.array(batch["input_ids"])

    pytest.assume(len(tokens) == len(sequences), "Batch size is not correct")
    pytest.assume(tokens.shape[1] == max_length, "Max length is not correct")

    if add_eos_token:
        eos_idxes = jnp.argwhere(tokens == tokenizer.eos_token_id)
        pytest.assume(jnp.all(eos_idxes == jnp.array([[0, 50], [1, 48]])))

    # check for correct tokenization
    decoded_sequence = tokenizer.batch_decode(tokens, skip_special_tokens=True)

    pytest.assume(
        functools.reduce(
            lambda x, y: x & y, [i == j for i, j in zip(sequences, decoded_sequence)]
        )
    )


@pytest.mark.parametrize("cds_only", [True, False])
@pytest.mark.parametrize("use_codon_tokens", [True, False])
@pytest.mark.parametrize("use_non_nucleotide_tokens", [True, False])
def test_codon_tokenizer(
    cds_only: bool, use_codon_tokens: bool, use_non_nucleotide_tokens: bool
) -> None:
    """
    Check that the tokenization with codon tokenizer returns a list of tokens as
    expected.
    """
    tokenizer = CodonTokenizer(
        CodonTokenizerParams(cds_only=True),
        fixed_length=np.random.choice([20, 300, 1024]),
    )
    tokens_basic_seq = tokenizer.tokenize(
        RNASequence(five_prime_utr="", coding_sequence="AUGGGG", three_prime_utr="")
    )[1]

    assert tokens_basic_seq == [2, 43, 71, 3]

    def _random_nucleotide_seq(seq_length: int) -> str:
        return "".join(
            list(RNA_NUCLEOTIDE_TYPES)[x]
            for x in np.random.randint(len(RNA_NUCLEOTIDE_TYPES), size=seq_length)
        )

    np.random.seed(0)

    tokenizer_params = CodonTokenizerParams(
        use_codon_tokens=use_codon_tokens,
        cds_only=cds_only,
        use_non_standard_nucleotide_tokens=use_non_nucleotide_tokens,
    )

    for _ in range(100):
        mrna_seq = RNASequence(
            five_prime_utr=_random_nucleotide_seq(np.random.randint(1, 100)),
            coding_sequence=_random_nucleotide_seq(np.random.randint(1, 100) * 3),
            three_prime_utr=_random_nucleotide_seq(np.random.randint(1, 100)),
        )
        tokenizer = CodonTokenizer(
            tokenizer_params, fixed_length=np.random.choice([20, 300, 1024])
        )
        _, all_token_ids = tokenizer.tokenize(mrna_seq)
        mrna_seq_with_special_tokens = "".join(
            [tokenizer.id_to_token(token_id) for token_id in all_token_ids]
        )
        assert (
            mrna_seq_with_special_tokens
            == (
                f"{tokenizer_params.bos_token}"
                f"{mrna_seq.five_prime_utr}"
                f"{tokenizer_params.cds_bos_token}"
                f"{mrna_seq.coding_sequence}"
                f"{tokenizer_params.cds_eos_token}"
                f"{mrna_seq.three_prime_utr}"
                f"{tokenizer_params.eos_token}"
            )
            if not cds_only
            else (
                f"{tokenizer_params.cds_bos_token}"
                f"{mrna_seq.coding_sequence}"
                f"{tokenizer_params.cds_eos_token}"
            )
        )
