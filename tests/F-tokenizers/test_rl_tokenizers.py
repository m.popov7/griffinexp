from typing import Dict

import jax
import jax.numpy as jnp
import numpy as np
import pytest

from trix.dataloaders.rl.generators import generate_fake_rl_trajectories
from trix.tokenizers.rl.base import Transition
from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer
from trix.tokenizers.rl.gato import GATOTokenizer
from trix.tokenizers.rl.voronoi import VoronoiTokenizer, VoronoiTokenizerParams
from trix.utils.rl import compute_mu_law, get_mask_from_dones


@pytest.mark.parametrize("episode_length", [1, 37, 1000])
@pytest.mark.parametrize("num_trajectories", [1, 37, 1000])
@pytest.mark.parametrize("action_size", [0, 1, 13])
@pytest.mark.parametrize("observation_size", [0, 1, 13])
def test_decision_transformer_tokenizer(
    episode_length: int, num_trajectories: int, action_size: int, observation_size: int
) -> None:
    """
    Tests the DecisionTransformerTokenizer, validates that we can properly
    the transitions from the tokens.
    """

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)
    transitions = generate_fake_rl_trajectories(
        num_trajectories=num_trajectories,
        episode_length=episode_length,
        observation_size=observation_size,
        action_size=action_size,
        allow_incomplete=False,
        random_key=subkey,
    )

    tokenizer = DecisionTransformerTokenizer(action_size, observation_size)

    tokenizer_outs = tokenizer.batch_tokenize(transitions)
    returns_to_go = jnp.sum(transitions.rewards, axis=1, keepdims=True) - jnp.cumsum(
        transitions.rewards, axis=1
    )

    tokens = tokenizer_outs["tokens"]

    decoded = tokenizer.decode(tokens)
    pytest.assume(jnp.array_equal(returns_to_go, decoded["returns_to_go"]))
    pytest.assume(jnp.array_equal(transitions.obs, decoded["obs"]))
    pytest.assume(jnp.array_equal(transitions.actions, decoded["actions"]))


@pytest.mark.parametrize(
    "inputs, outputs",
    (
        (
            {
                "obs": jnp.array([[[-50.0, 0.0, 50.0], [-50.0, 0.0, 50.0]]]),
                "actions": jnp.array([[[-1.0], [0.0]]]),
                "num_quantization_tokens": 3,
                "dones": jnp.array([[0, 0]]),
                "rewards": jnp.array([[5.0, 5.0]]),
            },
            {
                "tokens": jnp.array([[6, 7, 8, 3, 6, 6, 7, 8, 3, 7]]),
                "rewards_tokens": jnp.array([[8, 8]]),
                "positions": jnp.array([[0, 0, 0, 0, 0, 1, 1, 1, 1, 1]]),
            },
        ),
        (
            {
                "obs": jnp.array([[[-50.0, 0.0, 50.0], [-50.0, 0.0, 50.0]]]),
                "actions": jnp.array([[[-1.0], [0.0]]]),
                "num_quantization_tokens": 3,
                "dones": jnp.array([[1, 0]]),
                "rewards": jnp.array([[-5.0, 5.0]]),
            },
            {
                "tokens": jnp.array([[6, 7, 8, 3, 6, 1, 1, 1, 1, 1]]),
                "rewards_tokens": jnp.array([[6, 8]]),
                "positions": jnp.array([[0, 0, 0, 0, 0, 1, 1, 1, 1, 1]]),
            },
        ),
    ),
)
def test_gato_tokenize(
    inputs: Dict[str, jnp.ndarray], outputs: Dict[str, jnp.ndarray]
) -> None:
    """
    Tests on a few inputs-outputs pairs that the expected result is valid.
    """

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    observation_size = inputs["obs"].shape[-1]
    action_size = inputs["actions"].shape[-1]

    tokenizer = GATOTokenizer(
        observation_size=observation_size,
        action_size=action_size,
        num_quantization_tokens=inputs["num_quantization_tokens"],
        prepend_bos_token=False,
        add_separator_token=True,
    )

    transitions = Transition(
        obs=inputs["obs"],
        actions=inputs["actions"],
        dones=inputs["dones"],
        rewards=inputs["rewards"],
    )
    rewards_tokens = jax.vmap(tokenizer.tokenize_rewards)(inputs["rewards"])

    tokenizer_outs = tokenizer.batch_tokenize(transitions)

    pytest.assume(jnp.array_equal(rewards_tokens, outputs["rewards_tokens"]))
    pytest.assume(jnp.array_equal(tokenizer_outs["tokens"], outputs["tokens"]))
    pytest.assume(jnp.array_equal(tokenizer_outs["positions"], outputs["positions"]))


@pytest.mark.parametrize(
    "inputs",
    (
        {
            "obs": jnp.array([[[-50.0, 0.0, 50.0], [-50.0, 0.0, 50.0]]]),
            "actions": jnp.array([[[-1.0], [0.0]]]),
            "num_quantization_tokens": 4096,
            "dones": jnp.array([[0, 0]]),
            "rewards": jnp.array([[5.0, 5.0]]),
        },
        {
            "obs": jnp.array([[[-5.0, 0.7, 33.1], [-50.0, 0.0, 50.0]]]),
            "actions": jnp.array([[[-0.7], [0.3]]]),
            "num_quantization_tokens": 4096,
            "dones": jnp.array([[1, 0]]),
            "rewards": jnp.array([[-5.0, 5.0]]),
        },
    ),
)
def test_gato_decode(inputs: Dict[str, jnp.ndarray]) -> None:
    """
    Tests on a few inputs-outputs pairs that the expected result is valid.
    """

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    observation_size = inputs["obs"].shape[-1]
    action_size = inputs["actions"].shape[-1]

    tokenizer = GATOTokenizer(
        observation_size=observation_size,
        action_size=action_size,
        num_quantization_tokens=inputs["num_quantization_tokens"],
        prepend_bos_token=False,
        add_separator_token=True,
    )

    transitions = Transition(
        obs=inputs["obs"],
        actions=inputs["actions"],
        dones=inputs["dones"],
        rewards=inputs["rewards"],
    )

    tokenizer_outs = tokenizer.batch_tokenize(transitions)
    decoded = tokenizer.decode(tokenizer_outs["tokens"])

    mask = jax.vmap(get_mask_from_dones)(inputs["dones"])[..., None]

    decoded_obs = jnp.where(mask, decoded["obs"], 0)
    inputs_obs = jnp.where(mask, inputs["obs"], 0)

    decoded_actions = jnp.where(mask, decoded["actions"], 0)
    inputs_actions = jnp.where(mask, inputs["actions"], 0)

    pytest.assume(jnp.allclose(decoded_obs, inputs_obs, atol=1e-3, rtol=1e-2))
    pytest.assume(jnp.allclose(decoded_actions, inputs_actions, atol=1e-3, rtol=1e-2))


@pytest.mark.parametrize(
    "inputs",
    (
        {
            "tokens": np.array(["<pad>", "2", "1"]),
            "num_quantization_tokens": 3,
        },
        {
            "tokens": np.array(["<pad>", "9", "<unk>", "0"]),
            "num_quantization_tokens": 10,
        },
    ),
)
def test_gato_id_to_token(inputs: Dict[str, jnp.ndarray]) -> None:
    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    tokenizer = GATOTokenizer(
        observation_size=5,
        action_size=3,
        num_quantization_tokens=inputs["num_quantization_tokens"],
        prepend_bos_token=False,
        add_separator_token=True,
    )

    ids = [tokenizer.token_to_id(token) for token in inputs["tokens"]]
    reconst_tokens = [tokenizer.id_to_token(id_) for id_ in ids]
    pytest.assume(np.array_equal(inputs["tokens"], reconst_tokens))


@pytest.mark.parametrize(
    "inputs, outputs",
    (
        (
            {
                "obs": jnp.array([[[-50.0, -50], [-50.0, 50.0]]]),
                "actions": jnp.array([[[-1.0], [0.0]]]),
                "num_centroids": 4,
                "dones": jnp.array([[0, 0]]),
                "rewards": jnp.array([[5.0, 5.0]]),
            },
            {
                "positions": jnp.array([[0, 0, 0, 1, 1, 1]]),
            },
        ),
        (
            {
                "obs": jnp.array([[[-50.0, -50], [-50.0, 50.0]]]),
                "actions": jnp.array([[[-1.0], [0.0]]]),
                "num_centroids": 16,
                "dones": jnp.array([[1, 0]]),
                "rewards": jnp.array([[5.0, 5.0]]),
            },
            {
                "positions": jnp.array([[0, 0, 0, 1, 1, 1]]),
            },
        ),
    ),
)
def test_voronoi_tokenizer(
    inputs: Dict[str, jnp.ndarray], outputs: Dict[str, jnp.ndarray]
) -> None:
    """
    Tests on a few inputs-outputs pairs that the expected result is valid.
    """

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    observation_size = inputs["obs"].shape[-1]
    action_size = inputs["actions"].shape[-1]
    num_centroids = inputs["num_centroids"]

    params = VoronoiTokenizerParams(
        observation_size=observation_size,
        action_size=action_size,
        num_centroids=num_centroids,
        num_init_cvt_samples=50000,
        prepend_bos_token=False,
        add_separator_token=True,
        cache_centroids=False,
    )

    tokenizer = VoronoiTokenizer(subkey, params)

    transitions = Transition(
        obs=inputs["obs"],
        actions=inputs["actions"],
        dones=inputs["dones"],
        rewards=inputs["rewards"],
    )

    tokenizer_outs = tokenizer.batch_tokenize(transitions)

    action_centroids = tokenizer.centroids["action_centroids"]
    obs_centroids = tokenizer.centroids["obs_centroids"]

    tokens = tokenizer_outs["tokens"]
    positions = tokenizer_outs["positions"]

    # For each element of obs and actions, check that the assigned centroid
    # is indeed the closest centroid.
    action_tokens = tokens.reshape(-1, tokenizer.concat_size)
    action_tokens = action_tokens[..., -1:]
    action_tokens_shifted = (
        action_tokens - num_centroids - len(tokenizer._special_tokens)
    )

    def is_closest(
        array: jnp.ndarray, potential_closest: jnp.ndarray, value: jnp.ndarray
    ) -> bool:
        dist = jnp.sum((array - value) ** 2, axis=-1)
        closest = array[jnp.argmin(dist)]
        return (closest == potential_closest).all()  # type: ignore

    for i in range(action_tokens.shape[0]):
        if action_tokens[i] != tokenizer.pad_token_id:
            true_action = inputs["actions"][0, i]
            action = jnp.take(action_centroids, action_tokens_shifted[i, 0], axis=0)

            pytest.assume(
                is_closest(
                    array=action_centroids, potential_closest=action, value=true_action
                )
            )

    obs_tokens = tokens.reshape(-1, tokenizer.concat_size)
    obs_tokens = obs_tokens[..., :1]
    obs_tokens_shifted = obs_tokens - num_centroids - len(tokenizer._special_tokens)

    for i in range(obs_tokens.shape[0]):
        if obs_tokens[i] != tokenizer.pad_token_id:
            true_obs = inputs["obs"][0, i]
            obs = jnp.take(obs_centroids, obs_tokens_shifted[i, 0], axis=0)

            true_obs = compute_mu_law(true_obs)
            pytest.assume(
                is_closest(array=obs_centroids, potential_closest=obs, value=true_obs)
            )

    pytest.assume(jnp.array_equal(positions, outputs["positions"]))


@pytest.mark.parametrize(
    "inputs",
    (
        {
            "tokens": np.array(["<pad>", "2", "1"]),
            "num_centroids": 10,
        },
        {
            "tokens": np.array(["<pad>", "9", "<unk>", "0"]),
            "num_centroids": 1024,
        },
    ),
)
def test_voronoi_id_to_token(inputs: Dict[str, jnp.ndarray]) -> None:
    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    num_centroids = inputs["num_centroids"]

    params = VoronoiTokenizerParams(
        observation_size=3,
        action_size=5,
        num_centroids=num_centroids,
        num_init_cvt_samples=50000,
        prepend_bos_token=False,
        add_separator_token=True,
        cache_centroids=False,
    )

    tokenizer = VoronoiTokenizer(subkey, params)

    ids = [tokenizer.token_to_id(token) for token in inputs["tokens"]]
    reconst_tokens = [tokenizer.id_to_token(id_) for id_ in ids]
    pytest.assume(np.array_equal(inputs["tokens"], reconst_tokens))


@pytest.mark.parametrize(
    "inputs",
    (
        {
            "obs": jnp.array([[[-0.5, 0.5], [0.7, -0.3]]]),
            "actions": jnp.array([[[-1.0], [0.0]]]),
            "num_centroids": 4096,
            "dones": jnp.array([[0, 0]]),
            "rewards": jnp.array([[5.0, 5.0]]),
        },
        {
            "obs": jnp.array([[[0.5, 0.7], [0.9, -0.4]]]),
            "actions": jnp.array([[[-0.7], [0.3]]]),
            "num_centroids": 4096,
            "dones": jnp.array([[1, 0]]),
            "rewards": jnp.array([[-5.0, 5.0]]),
        },
    ),
)
def test_voronoi_decode(inputs: Dict[str, jnp.ndarray]) -> None:
    """
    Tests on a few inputs-outputs pairs that the expected result is valid.
    """

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    observation_size = inputs["obs"].shape[-1]
    action_size = inputs["actions"].shape[-1]

    num_centroids = inputs["num_centroids"]

    params = VoronoiTokenizerParams(
        observation_size=observation_size,
        action_size=action_size,
        num_centroids=num_centroids,
        num_init_cvt_samples=50000,
        prepend_bos_token=False,
        add_separator_token=True,
        cache_centroids=False,
    )

    tokenizer = VoronoiTokenizer(subkey, params)

    transitions = Transition(
        obs=inputs["obs"],
        actions=inputs["actions"],
        dones=inputs["dones"],
        rewards=inputs["rewards"],
    )

    tokenizer_outs = tokenizer.batch_tokenize(transitions)
    decoded = tokenizer.decode(tokenizer_outs["tokens"])

    mask = jax.vmap(get_mask_from_dones)(inputs["dones"])[..., None]
    decoded_obs = jnp.where(mask, decoded["obs"], 0)
    inputs_obs = jnp.where(mask, inputs["obs"], 0)

    decoded_actions = jnp.where(mask, decoded["actions"], 0)
    inputs_actions = jnp.where(mask, inputs["actions"], 0)

    pytest.assume(jnp.allclose(decoded_obs, inputs_obs, atol=1e-1, rtol=1e-2))
    pytest.assume(jnp.allclose(decoded_actions, inputs_actions, atol=1e-3, rtol=1e-2))
