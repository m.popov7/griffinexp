from typing import Any

import haiku as hk
import jax
import jax.numpy as jnp
import optax
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.dataloaders.language_models.sequence_datasets import SequencesDataset
from trix.layers.heads.classification_head import (
    PerTokenClassificationHead,
    SimpleClassificationHead,
)
from trix.models.esm.finetuning import build_esm_with_head_fn
from trix.models.esm.model import ESMTransformerConfig
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.losses import cross_entropy_loss_classification
from trix.training.supervised_trainers.classification_trainer import (
    ClassificationTrainer,
)
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.metrics import get_confusion_metrics


def test_classification_trainer_label_per_sequence() -> None:
    """
    Test if the classification trainer runs without errors and that training state is
    the same across device after some steps.

    Test in the case of a single label for the sequence.
    """
    devices = jax.devices("cpu")
    num_devices = len(devices)

    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 32
    num_classes = 3

    effective_batch_size = 4
    batch_size = 1
    num_acc_grads = int(effective_batch_size / (num_devices * batch_size))

    seed = 0

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length + 1,
        prepend_cls_token=True,
    )

    # use as example an ESM model for the MLM case
    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        max_positions=max_sequence_length + 1,
        attention_heads=2,
        embed_dim=2,
        ffn_embed_dim=2,
        num_layers=1,
        mask_token_id=tokenizer.mask_token_id,
        pad_token_id=tokenizer.pad_token_id,
        lm_head="simple",
    )

    # very simple linear classification head
    def head_fn() -> Any:
        return SimpleClassificationHead(num_classes=num_classes)

    forward_fn = build_esm_with_head_fn(model_config, head_fn=head_fn)
    # transform forward fn with haiku
    forward_fn = hk.transform(forward_fn)

    # get optimizer
    optimizer = optax.chain(
        optax.clip(1.0),
        optax.adam(learning_rate=3e-4),
    )
    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    # get datasets
    sequences = generate_fake_dataset(
        min_sequence_length=min_sequence_length,
        max_sequence_length=max_sequence_length,
        num_sequences=num_sequences,
        standard_tokens=AMINO_ACIDS,
        seed=seed,
    )
    train_dataset = SequencesDataset(
        sequences=sequences,
        tokenizer=tokenizer,
        batch_size=num_devices * batch_size * num_acc_grads,
        shuffle=False,
        drop_last=True,
    )

    # get trainer
    trainer = ClassificationTrainer(
        forward_fn=forward_fn,
        loss_fn=cross_entropy_loss_classification,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=num_classes,
    )  # type: ignore

    random_key_cpu = jax.random.PRNGKey(seed)
    random_keys = jax.device_put_replicated(random_key_cpu, devices=devices)
    dummy_tokens = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )
    training_state = jax.pmap(trainer.init, devices=devices, axis_name="batch")(
        random_keys, dummy_tokens[:, 0, :, :]
    )

    pmapped_update_fn = jax.pmap(
        trainer.update, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    generator = iter(train_dataset.get_epoch_batches())
    tokens = next(generator)
    effective_batch_size = num_devices * num_acc_grads * batch_size
    labels = jax.random.randint(
        jax.random.PRNGKey(0),
        shape=(effective_batch_size,),
        minval=0,
        maxval=num_classes,
    )
    tokens = tokens.reshape((num_devices, num_acc_grads, batch_size, -1))
    labels = labels.reshape((num_devices, num_acc_grads, batch_size))
    training_state, training_metrics = pmapped_update_fn(training_state, tokens, labels)

    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    if num_devices > 1:
        for x in jax.tree_util.tree_leaves(training_state):
            pytest.assume(jnp.all(x[0] == x[1]), "Desynchronized Training State")

    for x in jax.tree_util.tree_leaves(training_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    pmapped_metrics_fn = jax.pmap(
        trainer.compute_metrics, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    testing_metrics = pmapped_metrics_fn(
        training_state, tokens[:, 0, :, :], labels[:, 0, :]
    )
    for x in jax.tree_util.tree_leaves(testing_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    pytest.assume(
        training_metrics["confusion_matrix"].shape
        == (num_devices, num_classes, num_classes)
    )
    pytest.assume(
        testing_metrics["confusion_matrix"].shape
        == (num_devices, num_classes, num_classes)
    )

    confusion_metrics = get_confusion_metrics(testing_metrics["confusion_matrix"][0])
    for x in jax.tree_util.tree_leaves(confusion_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in confusion metrics")


def test_classification_trainer_label_per_token() -> None:
    """
    Test if the classification trainer runs without errors and that training state is
    the same across device after some steps.

    Test in the case of a one label per token in the sequence.
    """
    devices = jax.devices("cpu")
    num_devices = len(devices)

    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 32
    num_classes = 3

    effective_batch_size = 4
    batch_size = 1
    num_acc_grads = int(effective_batch_size / (num_devices * batch_size))

    seed = 0

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length + 1,
        prepend_cls_token=True,
    )

    # use as example an ESM model for the MLM case
    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        max_positions=max_sequence_length + 1,
        attention_heads=2,
        embed_dim=2,
        ffn_embed_dim=2,
        num_layers=1,
        mask_token_id=tokenizer.mask_token_id,
        pad_token_id=tokenizer.pad_token_id,
        lm_head="simple",
    )

    # very simple linear classification head
    def head_fn() -> Any:
        return PerTokenClassificationHead(num_classes=num_classes)

    forward_fn = build_esm_with_head_fn(model_config, head_fn=head_fn)
    # transform forward fn with haiku
    forward_fn = hk.transform(forward_fn)

    # get optimizer
    optimizer = optax.chain(
        optax.clip(1.0),
        optax.adam(learning_rate=3e-4),
    )
    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    # get datasets
    sequences = generate_fake_dataset(
        min_sequence_length=min_sequence_length,
        max_sequence_length=max_sequence_length,
        num_sequences=num_sequences,
        standard_tokens=AMINO_ACIDS,
        seed=seed,
    )
    train_dataset = SequencesDataset(
        sequences=sequences,
        tokenizer=tokenizer,
        batch_size=num_devices * batch_size * num_acc_grads,
        shuffle=False,
        drop_last=True,
    )

    # get trainer
    trainer = ClassificationTrainer(
        forward_fn=forward_fn,
        loss_fn=cross_entropy_loss_classification,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=num_classes,
    )  # type: ignore

    random_key_cpu = jax.random.PRNGKey(seed)
    random_keys = jax.device_put_replicated(random_key_cpu, devices=devices)
    dummy_tokens = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )
    training_state = jax.pmap(trainer.init, devices=devices, axis_name="batch")(
        random_keys, dummy_tokens[:, 0, :, :]
    )

    pmapped_update_fn = jax.pmap(
        trainer.update, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    generator = iter(train_dataset.get_epoch_batches())
    tokens = next(generator)
    effective_batch_size = num_devices * num_acc_grads * batch_size
    labels = jax.random.randint(
        jax.random.PRNGKey(0),
        shape=(effective_batch_size, tokens.shape[-1]),
        minval=0,
        maxval=num_classes,
    )
    tokens = tokens.reshape((num_devices, num_acc_grads, batch_size, -1))
    labels = labels.reshape((num_devices, num_acc_grads, batch_size, tokens.shape[-1]))
    training_state, training_metrics = pmapped_update_fn(training_state, tokens, labels)

    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    if num_devices > 1:
        for x in jax.tree_util.tree_leaves(training_state):
            pytest.assume(jnp.all(x[0] == x[1]), "Desynchronized Training State")

    for x in jax.tree_util.tree_leaves(training_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    pmapped_metrics_fn = jax.pmap(
        trainer.compute_metrics, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    testing_metrics = pmapped_metrics_fn(
        training_state, tokens[:, 0, :, :], labels[:, 0, :]
    )
    for x in jax.tree_util.tree_leaves(testing_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    pytest.assume(
        training_metrics["confusion_matrix"].shape
        == (num_devices, tokens.shape[-1], num_classes, num_classes)
    )
    pytest.assume(
        testing_metrics["confusion_matrix"].shape
        == (num_devices, tokens.shape[-1], num_classes, num_classes)
    )

    for feature_dim in range(tokens.shape[-1]):
        confusion_metrics = get_confusion_metrics(
            testing_metrics["confusion_matrix"][0][feature_dim]
        )
        for x in jax.tree_util.tree_leaves(confusion_metrics):
            pytest.assume(not jnp.isnan(x).any(), "NaN in confusion metrics")
