import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.rl.datasets import TrajectoryDataset
from trix.dataloaders.rl.generators import generate_fake_rl_trajectories
from trix.models.decision_transformer.model import (
    DecisionTransformerConfig,
    build_decision_transformer_forward_fn,
)
from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer
from trix.training.losses import mse_loss
from trix.training.rl_trainers.decision_transformer_trainer import (
    DecisionTransformerTrainer,
)
from trix.training.rl_trainers.runners import run_train_epoch_decision_transformer
from trix.utils.s3 import init_s3_trix_bucket, load_checkpoint_from_s3


def test_decision_transformer_trainer() -> None:
    """
    Tests if the trainer runs without errors.
    """
    num_trajectories = 64
    observation_size = 2
    action_size = 1
    episode_length = 10
    allow_incomplete = True
    seed = 0
    random_key = jax.random.PRNGKey(seed)
    random_key, subkey = jax.random.split(random_key)
    transitions = generate_fake_rl_trajectories(
        num_trajectories=num_trajectories,
        episode_length=episode_length,
        observation_size=observation_size,
        action_size=action_size,
        allow_incomplete=allow_incomplete,
        random_key=subkey,
    )

    embed_dim = 4
    num_layers = 1
    num_attention_heads = 1
    ffn_embed_dim = 4
    action_tanh = True

    # Decision Transformer parameters
    dt_config = DecisionTransformerConfig(
        embed_dim=embed_dim,
        num_layers=num_layers,
        num_attention_heads=num_attention_heads,
        ffn_embed_dim=ffn_embed_dim,
        action_tanh=action_tanh,
    )

    tokenizer = DecisionTransformerTokenizer(
        observation_size=observation_size, action_size=action_size
    )

    # Get model functions
    forward_fn = build_decision_transformer_forward_fn(
        dt_config, action_size=action_size, observation_size=observation_size
    )
    decision_transformer = hk.transform(forward_fn)

    optimizer = optax.adam(learning_rate=3e-4)

    trainer = DecisionTransformerTrainer(
        forward_fn=decision_transformer,
        loss_fn=mse_loss,
        tokenizer=tokenizer,
        optimizer=optimizer,
    )

    # Initialize on devices
    devices = jax.devices("cpu")
    num_devices = len(devices)
    effective_batch_size = 4
    batch_size = 1
    num_acc_grads = int(effective_batch_size / (num_devices * batch_size))

    keys = jnp.stack([jax.random.PRNGKey(seed) for _ in range(num_devices)])

    random_key, subkey = jax.random.split(random_key)
    dataset = TrajectoryDataset(
        data=transitions,
        tokenizer=tokenizer,
        batch_size=effective_batch_size,
        random_key=subkey,
        horizon_size=episode_length,
        num_chunks_per_trajectory=1,
        shuffle=False,
    )

    dummy_tokenizer_outs = next(dataset.get_epoch_batches())
    dummy_shape = (
        num_devices,
        num_acc_grads,
        batch_size,
        tokenizer.concat_size * episode_length,
    )
    dummy_tokenizer_outs = jax.tree_map(
        lambda x: x.reshape(
            *dummy_shape,
            -1,
        )[:, 0],
        dummy_tokenizer_outs,
    )

    training_state = jax.pmap(trainer.init)(
        keys,
        tokens=dummy_tokenizer_outs["tokens"],
        positions=dummy_tokenizer_outs["positions"][..., 0],
    )

    update = trainer.update

    training_state, metrics = run_train_epoch_decision_transformer(
        update,
        dataset=dataset,
        devices=devices,
        num_acc_grads=num_acc_grads,
        batch_size=batch_size,
        training_state=training_state,
    )

    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    for x in jax.tree_util.tree_leaves(metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    if num_devices > 1:
        for x in jax.tree_util.tree_leaves(training_state):
            pytest.assume(jnp.all(x[0] == x[1]), "Desynchronized Training State")

    training_state_cpu = jax.tree_map(lambda x: x[0], training_state)

    # Load reference training state
    s3_client, bucket = init_s3_trix_bucket()

    training_state_loaded = load_checkpoint_from_s3(
        s3_client=s3_client,
        bucket=bucket,
        bucket_checkpoint_directory="tests/trainers/decision_transformer_trainer",
    )

    comparisons = jax.tree_map(
        lambda x, y: np.allclose(x, y, rtol=1e-2, atol=1e-3),
        training_state_cpu,
        training_state_loaded,
    )
    pytest.assume(
        all(jax.tree_util.tree_leaves(comparisons)),
        "Training state inconsistent with the reference one",
    )
