import os
import shutil

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest
from jax import tree_util

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.dataloaders.language_models.sequence_datasets import SequencesDataset
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.language_trainers.decoder_causal_lm_trainer import DecoderCLMTrainer
from trix.training.language_trainers.encoder_lm_trainer import (
    EncoderMLMTrainer,
    TrainingState,
)
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.s3 import init_s3_trix_bucket, load_checkpoint_from_s3


def test_training_state() -> None:
    """
    Checks that given a TrainingState, saving and loading the TrainingState maintains
    the same values of the NamedTuple.
    """

    def forward_fn(x: jnp.array) -> jnp.array:
        layer = hk.Conv2D(4, 3, 1)
        return layer(x)

    # build forward fn and params
    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))
    params = forward_fn_trans.init(jax.random.PRNGKey(0), jnp.zeros((10, 20, 20)))

    # initialize optim
    optim = optax.adam(0.1)
    optim_state = optim.init(params)

    rand_key = jax.random.PRNGKey(1)
    step = 1

    # initialize Training State class
    train_state = TrainingState(
        step=step, params=params, optimizer_state=optim_state, random_key=rand_key
    )

    # save then load
    try:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        save_path = os.path.join(current_dir, "tpm_data")
        train_state.save(save_path)
        train_state_load = train_state.load(save_path)

        # fake input and grads
        x = jax.random.normal(jax.random.PRNGKey(2), (10, 20, 20))
        grads = jax.tree_map(lambda x: x + 2, params)

        # check saved then loaded training state is equal to
        # training state as argument to save
        pytest.assume(
            jnp.allclose(
                forward_fn_trans.apply(train_state_load.params, x),
                forward_fn_trans.apply(params, x),
            )
        )

        load_updates = tree_util.tree_leaves(
            optim.update(grads, train_state_load.optimizer_state, params)[0]
        )
        updates = tree_util.tree_leaves(optim.update(grads, optim_state, params)[0])
        for x, y in zip(load_updates, updates):
            pytest.assume(jnp.allclose(x, y))

        pytest.assume(train_state_load.step == step)
        pytest.assume(jnp.allclose(train_state_load.random_key, rand_key))
    finally:
        shutil.rmtree(save_path)


def test_mlm_trainers() -> None:
    """Tests if the MLM trainer runs without errors and that training state is
    the same across device after some steps."""
    devices = jax.devices("cpu")
    num_devices = len(devices)

    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 32

    effective_batch_size = 4
    batch_size = 1
    num_acc_grads = int(effective_batch_size / (num_devices * batch_size))

    seed = 0

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length + 1,
        prepend_cls_token=True,
    )

    # use as example an ESM model for the MLM case
    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        max_positions=max_sequence_length + 1,
        attention_heads=2,
        embed_dim=2,
        ffn_embed_dim=2,
        num_layers=1,
        mask_token_id=tokenizer.mask_token_id,
        pad_token_id=tokenizer.pad_token_id,
        lm_head="simple",
    )

    forward_fn = build_esm_fn(model_config)
    # transform forward fn with haiku
    forward_fn = hk.transform(forward_fn)

    # get optimizer
    optimizer = optax.chain(
        optax.clip(1.0),
        optax.adam(learning_rate=3e-4),
    )
    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    # get datasets
    train_dataset = SequencesDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed,
        ),
        tokenizer=tokenizer,
        batch_size=num_devices * batch_size,
        shuffle=False,
        drop_last=True,
    )

    # get trainer
    noising_ratio = 0.15
    masking_prob = 0.8
    random_token_prob = 0.1

    trainer = EncoderMLMTrainer(
        apply_fn=forward_fn.apply,
        init_fn=forward_fn.init,
        tokenizer=tokenizer,
        optimizer=optimizer,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
        random_token_prob=random_token_prob,
    )  # type: ignore

    random_key_cpu = jax.random.PRNGKey(seed)
    random_keys = jax.device_put_replicated(random_key_cpu, devices=devices)
    dummy_tokens = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(dummy_tokens, (num_devices, batch_size, -1))
    training_state = jax.pmap(trainer.init, devices=devices, axis_name="batch")(
        random_keys, dummy_tokens
    )

    pmapped_update_fn = jax.pmap(
        trainer.update, devices=devices, axis_name="batch", donate_argnums=(0,)
    )
    generator = iter(train_dataset.get_epoch_batches())
    for _ in range(num_acc_grads):
        tokens = next(generator)
        tokens = tokens.reshape((num_devices, batch_size, -1))
        training_state, training_metrics = pmapped_update_fn(training_state, tokens)

    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    if num_devices > 1:
        for x in jax.tree_util.tree_leaves(training_state):
            pytest.assume(jnp.all(x[0] == x[1]), "Desynchronized Training State")

    for x in jax.tree_util.tree_leaves(training_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    # Since consistency is evaluated along the devices, we compare only the parameters
    # on device 0
    training_state_cpu = jax.tree_map(lambda x: x[0], training_state)

    # Load reference training state from kao
    s3_client, bucket = init_s3_trix_bucket()

    training_state_loaded = load_checkpoint_from_s3(
        s3_client=s3_client,
        bucket=bucket,
        bucket_checkpoint_directory="tests/trainers/encoder_mlm_trainer",
    )

    comparisons = jax.tree_map(
        lambda x, y: np.allclose(x, y, rtol=1e-3, atol=1e-5),
        training_state_cpu,
        training_state_loaded,
    )
    pytest.assume(
        all(jax.tree_util.tree_leaves(comparisons)),
        "Training state inconsistent with the reference one",
    )


# TODO: fix the bucket and re-activate this test
@pytest.mark.skip(reason="the time to fix the kao bucket")
def test_clm_trainer() -> None:
    """Tests if the MLM trainer runs without errors and that training state is
    the same across device after some steps."""
    devices = jax.devices("cpu")
    num_devices = len(devices)

    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 16

    effective_batch_size = 4
    batch_size = 1
    num_acc_grads = int(effective_batch_size / (num_devices * batch_size))

    seed = 0

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length + 1,
        append_eos_token=True,
    )

    # take GPT like model for CLM case
    gpt_config = GptConfig(
        vocab_size=tokenizer.vocabulary_size,
        eos_token_id=tokenizer.eos_token_id,
        num_heads=2,
        embed_dim=4,
        ffn_embed_dim=8,
        num_layers=1,
        rope_dimensions=2,
        max_position_embeddings=512,
        add_bias_ffn=True,
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=True,
        norm_type="layer_norm",
        parallel_attention_ff=True,
        use_gradient_checkpointing=False,
    )

    # Get forward function and optimizer
    forward_fn = build_gpt_fn(gpt_config)
    # transform forward fn with haiku
    forward_fn = hk.transform(forward_fn)

    # get optimizer
    optimizer = optax.MultiSteps(
        optax.adam(learning_rate=3e-4), every_k_schedule=num_acc_grads
    )

    # get datasets
    train_dataset = SequencesDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed,
        ),
        tokenizer=tokenizer,
        batch_size=num_devices * batch_size,
        shuffle=False,
        drop_last=True,
    )

    # get trainer
    trainer = DecoderCLMTrainer(
        apply_fn=forward_fn.apply,  # type: ignore
        init_fn=forward_fn.init,  # type: ignore
        pad_token_id=tokenizer.pad_token_id,
        eos_token_id=tokenizer.eos_token_id,
        bos_token_id=tokenizer.bos_token_id,
        optimizer=optimizer,
    )  # type: ignore

    # initialize on devices
    keys = jax.device_put_replicated(jax.random.PRNGKey(seed), devices=devices)
    dummy_tokens = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(dummy_tokens, (num_devices, batch_size, -1))

    training_state = jax.pmap(trainer.init, devices=devices, axis_name="batch")(
        keys, dummy_tokens[:, :1, :1]
    )

    update_fn = jax.pmap(trainer.update, devices=devices, axis_name="batch")

    # run train epoch
    generator = train_dataset.get_epoch_batches()

    for _ in range(num_acc_grads):

        tokens = next(generator)
        # Reshape data to send to devices, and gradient accumulation
        tokens = jnp.reshape(tokens, (num_devices, batch_size, -1))

        # Update neural network and optimizer
        training_state, training_metrics = update_fn(training_state, tokens)

    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    for x in jax.tree_util.tree_leaves(training_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")
    if num_devices > 1:
        for x in jax.tree_util.tree_leaves(training_state):
            pytest.assume(jnp.all(x[0] == x[1]), "Desynchronized Training State")

    # Since consistency is evaluated along the devices, we compare only the parameters
    # on device 0
    training_state_cpu = jax.tree_map(lambda x: x[0], training_state)

    # Load reference training state from kao
    s3_client, bucket = init_s3_trix_bucket()

    training_state_loaded = load_checkpoint_from_s3(
        s3_client=s3_client,
        bucket=bucket,
        bucket_checkpoint_directory="tests/trainers/decoder_clm_trainer",
    )

    comparisons = jax.tree_map(
        lambda x, y: np.allclose(x, y, rtol=1e-3, atol=1e-5),
        training_state_cpu,
        training_state_loaded,
    )
    pytest.assume(
        all(jax.tree_util.tree_leaves(comparisons)),
        "Training state inconsistent with the reference one",
    )
