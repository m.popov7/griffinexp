import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.dataloaders.language_models.sequence_datasets import PairedSequenceDataset
from trix.models.encoder_decoder.model import (
    DecoderConfig,
    EncoderConfig,
    build_encoder_decoder_fn,
)
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.language_trainers.encoder_decoder_trainer import (
    EncoderDecoderCausalTrainer,
)
from trix.training.language_trainers.runners import (
    run_test_epoch_enc_dec,
    run_train_epoch_enc_dec,
)
from trix.training.losses import cross_entropy_loss_encoder_decoder
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.s3 import init_s3_trix_bucket, load_checkpoint_from_s3


@pytest.mark.parametrize("objective", ["T5"])
@pytest.mark.parametrize("batch_size", [1])
def test_encoder_decoder_trainers(objective: str, batch_size: int) -> None:
    """Tests if the encoder decoder trainer runs without errors."""
    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 16
    effective_batch_size = 4

    noising_ratio = 0.15
    masking_prob = 0.8
    random_token_prob = 0.1

    seed = 0

    prefix_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=max_sequence_length
    )

    suffix_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length,
        prepend_bos_token=True,
    )

    encoder_config = EncoderConfig(
        alphabet_size=prefix_tokenizer.vocabulary_size,
        max_positions=max_sequence_length,
        num_attention_heads=2,
        embed_dim=2,
        ffn_embed_dim=2,
        num_layers=1,
    )

    decoder_config = DecoderConfig(
        alphabet_size=suffix_tokenizer.vocabulary_size,
        max_positions=max_sequence_length,
        num_attention_heads=2,
        embed_dim=2,
        ffn_embed_dim=2,
        num_layers=1,
    )

    # Get Datasets
    train_dataset = PairedSequenceDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed,
        ),
        prefix_tokenizer=prefix_tokenizer,
        suffix_tokenizer=suffix_tokenizer,
        batch_size=effective_batch_size,
        min_sequence_len=4,
        random_key=jax.random.PRNGKey(42),
        shuffle=False,
    )
    test_dataset = PairedSequenceDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed + 1,
        ),
        prefix_tokenizer=prefix_tokenizer,
        suffix_tokenizer=suffix_tokenizer,
        batch_size=effective_batch_size,
        min_sequence_len=4,
        random_key=jax.random.PRNGKey(42),
        shuffle=False,
    )

    # Get forward function
    forward_fn = build_encoder_decoder_fn(
        encoder_config, decoder_config, mixed_precision=False
    )
    forward_fn = hk.transform(forward_fn)

    optimizer = optax.adam(learning_rate=3e-4)

    num_decoding_steps = 2

    if objective == "T5":
        trainer = EncoderDecoderCausalTrainer(
            forward_fn=forward_fn,
            tokenizer=prefix_tokenizer,
            loss_fn=cross_entropy_loss_encoder_decoder,
            optimizer=optimizer,
            num_decoding_steps=num_decoding_steps,
            noising_ratio=noising_ratio,
            masking_prob=masking_prob,
            random_token_prob=random_token_prob,
        )  # type: ignore

    # Initialize on devices
    devices = jax.devices("cpu")
    num_devices = len(devices)
    num_acc_grads = effective_batch_size // (batch_size * num_devices)

    keys = jnp.stack([jax.random.PRNGKey(seed) for _ in range(num_devices)])
    encoder_tokens, decoder_tokens = next(train_dataset.get_epoch_batches())

    encoder_tokens = jnp.reshape(
        encoder_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )[:, 0, :, :]
    decoder_tokens = jnp.reshape(
        decoder_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )[:, 0, :, :]
    training_state = jax.pmap(trainer.init, devices=devices)(
        keys, encoder_tokens, decoder_tokens
    )

    update = trainer.update

    training_state, metrics = run_train_epoch_enc_dec(
        update,
        dataset=train_dataset,
        devices=devices,
        num_acc_grads=num_acc_grads,
        batch_size=batch_size,
        training_state=training_state,
    )
    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    for x in jax.tree_util.tree_leaves(metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    if num_devices > 1:
        for x in jax.tree_util.tree_leaves(training_state):
            pytest.assume(jnp.all(x[0] == x[1]), "Desynchronized Training State")

    compute_metrics = trainer.compute_metrics

    metrics = run_test_epoch_enc_dec(
        compute_metrics,
        dataset=test_dataset,
        devices=devices,
        batch_size=effective_batch_size // num_devices,
        training_state=training_state,
    )

    for x in jax.tree_util.tree_leaves(metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in eval metrics")

    # Since consistency is evaluated along the devices, we compare only the parameters
    # on device 0
    training_state_cpu = jax.tree_map(lambda x: x[0], training_state)

    # Load reference training state from kao
    s3_client, bucket = init_s3_trix_bucket()

    training_state_loaded = load_checkpoint_from_s3(
        s3_client=s3_client,
        bucket=bucket,
        bucket_checkpoint_directory="tests/trainers/encoder_decoder_causal_trainer",
    )

    comparisons = jax.tree_map(
        lambda x, y: np.allclose(x, y, rtol=1e-3, atol=1e-5),
        training_state_cpu,
        training_state_loaded,
    )
    pytest.assume(
        all(jax.tree_util.tree_leaves(comparisons)),
        "Training state inconsistent with the reference one",
    )
