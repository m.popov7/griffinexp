import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.rl.generators import generate_fake_rl_trajectories
from trix.models.decision_transformer.model import (
    DecisionTransformerConfig,
    build_decision_transformer_forward_fn,
)
from trix.tokenizers.rl.decision_transformer import DecisionTransformerTokenizer


@pytest.mark.parametrize("episode_length", [1, 7])
@pytest.mark.parametrize("embed_dim", [32])
@pytest.mark.parametrize("allow_incomplete", [False, True])
def test_decision_transformer(
    episode_length: int, embed_dim: int, allow_incomplete: bool
) -> None:
    """
    Tests the DecisionTransformer, validates that inference runs without error and that
    outputs shape are consistent.
    """
    num_trajectories = 2
    observation_size = 2
    action_size = 1

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)
    transitions = generate_fake_rl_trajectories(
        num_trajectories=num_trajectories,
        episode_length=episode_length,
        observation_size=observation_size,
        action_size=action_size,
        allow_incomplete=allow_incomplete,
        random_key=subkey,
    )

    # print("Generated transitions:\n", transitions)
    # jax.tree_map(lambda x: print(x.shape), transitions)
    embed_dim = 4
    num_layers = 1
    num_attention_heads = 1
    ffn_embed_dim = 4
    action_tanh = True

    # Decision Transformer parameters
    dt_config = DecisionTransformerConfig(
        embed_dim=embed_dim,
        num_layers=num_layers,
        num_attention_heads=num_attention_heads,
        ffn_embed_dim=ffn_embed_dim,
        action_tanh=action_tanh,
    )

    tokenizer = DecisionTransformerTokenizer(
        observation_size=observation_size, action_size=action_size
    )

    tokens = tokenizer.batch_tokenize(transitions)
    # Get model functions
    forward_fn = build_decision_transformer_forward_fn(
        dt_config,
        action_size=action_size,
        observation_size=observation_size,
    )
    decision_transformer = hk.without_apply_rng(hk.transform(forward_fn))
    random_key, init_key = jax.random.split(random_key)

    params = decision_transformer.init(init_key, tokens)

    outs = decision_transformer.apply(params, tokens)

    mixed_forward_fn = build_decision_transformer_forward_fn(
        dt_config,
        action_size=action_size,
        observation_size=observation_size,
        compute_dtype=jnp.float16,
        param_dtype=jnp.float32,
        output_dtype=jnp.float16,
    )
    decision_transformer = hk.without_apply_rng(hk.transform(mixed_forward_fn))

    mixed_outs = decision_transformer.apply(params, tokens)
    pytest.assume(
        jnp.allclose(outs["predictions"], mixed_outs["predictions"], atol=1e-2)
    )
