import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.bigbird.model import BigbirdConfig, build_bigbird_forward_fn
from trix.tokenizers.language_models.standard import StandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS


def test_bigbird_mixed() -> None:
    """Tests that the bigbird model returns the same output no matter the batch_size."""

    # Generate fake protein sequences
    sequences = generate_fake_dataset(
        min_sequence_length=100,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    # Tokenize
    tokenizer = StandardTokenizer(
        standard_tokens=AMINO_ACIDS,
    )

    tokens = jnp.array([tok[1] for tok in tokenizer.batch_tokenize(sequences)])

    model_config = BigbirdConfig(
        alphabet_size=tokenizer.vocabulary_size,
        max_positions=300,
        embed_dim=8,
        ffn_embed_dim=16,
        attention_heads=2,
        num_layers=1,
        block_size=10,
        num_rand_blocks=3,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
    )

    forward_fn = build_bigbird_forward_fn(
        model_config,
        compute_dtype=jnp.float16,
        param_dtype=jnp.float32,
        output_dtype=jnp.float16,
    )
    forward_fn = hk.transform(forward_fn)
    apply_fn = jax.jit(forward_fn.apply)
    random_key = jax.random.PRNGKey(0)
    parameters = forward_fn.init(random_key, tokens[:1])

    # Create attention masks and apply the model
    outs = apply_fn(
        parameters,
        random_key,
        tokens=tokens,
    )
    logits = outs["logits"]

    full_forward_fn = build_bigbird_forward_fn(
        model_config,
        compute_dtype=jnp.float32,
        param_dtype=jnp.float32,
        output_dtype=jnp.float32,
    )
    full_forward_fn = hk.transform(full_forward_fn)
    full_apply_fn = jax.jit(full_forward_fn.apply)

    full_outs = full_apply_fn(parameters, random_key, tokens=tokens)
    full_logits = full_outs["logits"]

    pytest.assume(logits.dtype == jnp.float16, "wrong dtype output")

    pytest.assume(full_logits.dtype == jnp.float32, "wrong dtype output")

    pytest.assume(
        jnp.allclose(logits, full_logits, atol=1e-1),
        "mixed precision changes the output.",
    )

    pytest.assume(
        jnp.mean(jnp.abs(full_logits - logits)) < 1e-3,
        "mixed precision changes the output.",
    )
