import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.masking import build_padding_attention_mask


@pytest.mark.parametrize(
    ("positional_embedding", "use_rotary_embedding", "add_bias_kv"),
    [
        ["learned", False, False],
        ["sinusoidal", False, False],
        ["sinusoidal", False, True],
        [None, True, False],
    ],
)
@pytest.mark.parametrize("lm_head", ["simple", "roberta"])
@pytest.mark.parametrize("token_dropout", [True, False])
@pytest.mark.parametrize("mask_before_attention", [True, False])
def test_esm_architecture(
    mask_before_attention: bool,
    token_dropout: bool,
    lm_head: str,
    positional_embedding: str,
    use_rotary_embedding: bool,
    add_bias_kv: bool,
) -> None:
    """
    Tests that ESM models return the same outputs for the same input with different
    values of batch_size and pad.
    """
    # Generate fake protein sequences
    sequences = generate_fake_dataset(
        min_sequence_length=20,
        max_sequence_length=50,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    # Tokenize with two lengths
    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=64,
    )

    tokens = jnp.array([tok[1] for tok in tokenizer.batch_tokenize(sequences)])

    # Intntiate ESM model
    config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        max_positions=64,
        attention_heads=2,
        embed_dim=8,
        ffn_embed_dim=16,
        num_layers=1,
        positional_embedding=positional_embedding,
        lm_head=lm_head,
        add_bias_kv=add_bias_kv,
        use_rotary_embedding=use_rotary_embedding,
        mask_before_attention=mask_before_attention,
        token_dropout=token_dropout,
    )

    esm_fn = build_esm_fn(
        model_config=config,
        compute_dtype=jnp.float32,
        param_dtype=jnp.float32,
        output_dtype=jnp.float32,
    )
    esm_fn = hk.transform(esm_fn)
    random_key = jax.random.PRNGKey(0)
    parameters = esm_fn.init(random_key, tokens[:1])

    # Create attention masks and apply the model
    attention_mask = build_padding_attention_mask(tokens, tokenizer.pad_token_id)
    apply_fn = jax.jit(esm_fn.apply)
    outs = apply_fn(
        parameters,
        random_key,
        tokens=tokens,
        attention_mask=attention_mask,
    )
    mixed_esm_fn = build_esm_fn(
        model_config=config,
        compute_dtype=jnp.float16,
        param_dtype=jnp.float32,
        output_dtype=jnp.float32,
    )
    mixed_esm_fn = hk.transform(mixed_esm_fn)

    mixed_apply_fn = jax.jit(mixed_esm_fn.apply)

    mixed_outs = mixed_apply_fn(
        parameters,
        random_key,
        tokens=tokens,
        attention_mask=attention_mask,
    )

    # Get the logits on non pad tokens
    sequence_mask = attention_mask[:, 0, :, 0][:, :, None]
    logits = outs["logits"] * sequence_mask
    mixed_logits = mixed_outs["logits"] * sequence_mask

    pytest.assume(
        jnp.allclose(logits, mixed_logits, atol=1e-1),
        "mixed precision changes the logits",
    )
