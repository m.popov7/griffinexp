from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest
from jax.random import PRNGKey
from transformers import AutoTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.utils.decoding import decode_greedy


@pytest.mark.parametrize(
    "compute_dtype",
    [jnp.float16, jnp.bfloat16],
)
@pytest.mark.parametrize(
    (
        "rope_dimensions",
        "add_bias_ffn",
        "norm_type",
        "parallel_attention_ff",
        "use_glu_in_ffn",
        "ffn_activation_name",
        "add_bias_lm_head",
    ),
    [
        [
            4,
            True,
            "layer_norm",
            True,
            False,
            "gelu",
            True,
        ],  # GPTJ,
        [
            None,
            False,
            "RMS_norm",
            False,
            True,
            "silu",
            False,
        ],  # LLAMA
    ],
)
def test_gptj_model(
    compute_dtype: jnp.dtype,
    rope_dimensions: Optional[int],
    add_bias_ffn: bool,
    norm_type: str,
    parallel_attention_ff: bool,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    add_bias_lm_head: bool,
) -> None:
    """
    Tests a tiny random initialized gptj model :
    Test the jitted call fn and check that batching yields coherent results
    """

    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=32,
        ffn_embed_dim=32 * 4,
        num_heads=4,
        num_layers=2,
        rope_dimensions=rope_dimensions,
        max_position_embeddings=512,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=add_bias_lm_head,
        norm_type=norm_type,
        parallel_attention_ff=parallel_attention_ff,
        use_gradient_checkpointing=False,
    )
    gpt_fn = build_gpt_fn(
        config,
        param_dtype=jnp.float32,
        compute_dtype=compute_dtype,
        output_dtype=compute_dtype,
    )
    gpt_fn = hk.transform(gpt_fn)

    init_prompt = "Hello, I"
    input_token_ids = tokenizer([init_prompt], return_tensors="np", padding=True)[
        "input_ids"
    ]

    params = gpt_fn.init(PRNGKey(1), input_token_ids)  # type: ignore

    prompts = ["My dog is", "I'd like to go"]
    input_token_ids = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]
    pytest.assume(
        (input_token_ids == tokenizer.pad_token_id).sum() > 0,
        "there should be some padding tokens in the batch",
    )

    # Test the jitted call fn and check that batching yields coherent results
    logits_array = jax.jit(
        lambda input_token_ids: gpt_fn.apply(  # type: ignore
            params,
            jax.random.PRNGKey(42),
            token_ids=input_token_ids,
        )["logits"]
    )(input_token_ids)

    pytest.assume(
        logits_array.dtype == compute_dtype,
        f"unexpected type : {logits_array.dtype}",
    )

    output_tokens, _ = decode_greedy(
        init_tokens_ids=input_token_ids,
        random_key=jax.random.PRNGKey(42),
        params=params,
        apply_fn=gpt_fn.apply,
        num_tokens_to_decode=8,
        eos_token_id=tokenizer.eos_token_id,
    )
    pytest.assume(jnp.all(~jnp.isnan(output_tokens)), "NaNs in the output sequence.")
