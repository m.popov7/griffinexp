from typing import Any

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.generators import generate_fake_labeled_dataset
from trix.dataloaders.language_models.sequence_datasets import LabeledSequenceDataset
from trix.layers.heads.classification_head import SimpleClassificationHead
from trix.models.esm.finetuning import build_esm_ia3_rescaling_with_head_fn
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.standard import StandardTokenizer
from trix.training.losses import cross_entropy_loss_classification
from trix.training.supervised_trainers.classification_trainer import (
    ClassificationTrainer,
)
from trix.training.supervised_trainers.runners import (
    run_test_supervised_epoch,
    run_train_supervised_epoch,
)
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize("add_bias_kv", [False, True])
def test_finetuning_classification_loop(add_bias_kv: bool) -> None:
    """
    Test supports to fine-tune pre-trained ESM models with IA³ technique for
    classification. This test does not assess any performance yet, only that code
    executes properly.
    """
    # Tokenize
    tokenizer = StandardTokenizer(
        standard_tokens=AMINO_ACIDS,
    )
    # Intntiate ESM model
    config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        max_positions=256,
        attention_heads=2,
        embed_dim=8,
        ffn_embed_dim=16,
        num_layers=1,
        positional_embedding=None,
        lm_head="simple",
        add_bias_kv=add_bias_kv,
        use_rotary_embedding=bool(1 - add_bias_kv),
        mask_before_attention=False,
        token_dropout=False,
    )

    forward_fn = build_esm_fn(
        model_config=config,
        compute_dtype=jnp.float16,
        param_dtype=jnp.float32,
        output_dtype=jnp.float32,
    )
    forward_fn = hk.transform(forward_fn)
    random_key = jax.random.PRNGKey(0)

    # Get fine-tunable model for classification
    num_classes = len(tokenizer.standard_tokens)  # dummy example

    # The classification head is wrapped inside head_fn because
    # haiku modules cannot be instantiated outside hk.transform.
    def head_fn() -> hk.Module:
        return SimpleClassificationHead(num_classes=num_classes)

    finetune_forward_fn = build_esm_ia3_rescaling_with_head_fn(
        model_config=config,  # type: ignore
        head_fn=head_fn,
        model_name="esm_transformer",
        compute_dtype=jnp.float16,
        param_dtype=jnp.float32,
        output_dtype=jnp.float32,
    )
    finetune_forward_fn = hk.transform(finetune_forward_fn)

    # Random initialize fine-tunable model
    # Get data and tokenize it
    # Generate fake protein sequences
    sequences, labels = generate_fake_labeled_dataset(
        min_sequence_length=100,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
    tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

    # Initialize random key
    random_key = jax.random.PRNGKey(0)

    # Initialize finetunable model
    pretrained_params = forward_fn.init(rng=random_key, tokens=tokens)
    finetune_params = finetune_forward_fn.init(rng=random_key, tokens=tokens)

    # Filter out the unwanted RobertaLMHead parameters.
    def parameters_roberta_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "roberta" not in module_name
        cond_2 = "emb_layer_norm_after" in module_name
        return cond_1 or cond_2

    pretrained_params, _ = hk.data_structures.partition(
        parameters_roberta_partition_fn, pretrained_params
    )

    # Partition its params into trainable and non-trainable explicitly.
    def parameters_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "ia3_rescaling" in param_name
        cond_2 = "classification_head" in module_name
        return cond_1 or cond_2

    trainable_params, _ = hk.data_structures.partition(
        parameters_partition_fn, finetune_params
    )

    # Replace randomly initialized non-trainable params by pretrained ones
    finetune_params = hk.data_structures.merge(trainable_params, pretrained_params)

    # Get first device
    devices = jax.devices()[:1]
    num_devices = len(devices)

    # Setup dummy parameters for testing purpose
    mini_batch_size = 1
    batch_size = 4
    num_acc_grads = batch_size // (num_devices * mini_batch_size)
    learning_rate = 3e-3

    tokens = jnp.array([tok[1] for tok in tokenizer.batch_tokenize(sequences)])

    # Convert labels to numpy arrays
    labels = np.array(labels)[:, 0]

    # Convert labels to arrays
    train_dataset = LabeledSequenceDataset(
        sequences=sequences[:16],
        labels=labels[:16],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
    )

    test_dataset = LabeledSequenceDataset(
        sequences=sequences[16:],
        labels=labels[16:],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
    )

    # Setup optimizer
    optimizer = optax.adam(learning_rate=learning_rate)

    # Setup trainer
    trainer = ClassificationTrainer(
        forward_fn=finetune_forward_fn,
        loss_fn=cross_entropy_loss_classification,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=num_classes,
        parameters_partition_fn=parameters_partition_fn,
    )

    # Initialize on devices
    key_cpu = jax.random.PRNGKey(seed=0)
    keys = jnp.stack([key_cpu for _ in range(num_devices)])
    dummy_tokens, _ = next(train_dataset.get_epoch_batches())  # type: ignore
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, mini_batch_size, -1)
    )[:, 0, :, :]
    finetune_params = jax.device_put_replicated(finetune_params, devices=devices)
    training_state = jax.pmap(trainer.init, devices=devices, donate_argnums=(2,))(
        keys, dummy_tokens, finetune_params
    )

    # Performs an epoch training and testing loop
    compute_metrics = trainer.compute_metrics
    update = trainer.update

    training_state, _ = run_train_supervised_epoch(
        update_fn=update,
        dataset=train_dataset,
        devices=devices,
        num_acc_grads=num_acc_grads,
        batch_size=mini_batch_size,
        training_state=training_state,
        epoch_num=0,
    )
    compute_metrics = jax.pmap(compute_metrics, devices=devices, axis_name="batch")
    _ = run_test_supervised_epoch(
        compute_metrics_fn=compute_metrics,
        dataset=test_dataset,
        num_data_parallel_ways=num_devices,
        batch_size=mini_batch_size * num_acc_grads,
        training_state=training_state,
    )
