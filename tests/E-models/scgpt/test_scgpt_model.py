import copy

import haiku as hk
import jax.numpy as jnp
import numpy as np
import pytest
from jax.random import PRNGKey

from trix.models.scgpt.model import ScGptConfig, build_scgpt_fn

default_config = ScGptConfig(
    gene_ids_vocabulary_size=30000,
    embed_dim=512,
    num_heads=8,
    ffn_embed_dim=512,
    num_layers=3,
    num_batch_labels=1,
    gene_expr_vocabulary_size=51,
    use_discrete_gene_expr=False,
    use_prediction_head=True,
    use_batch_labels=False,
    cell_embedding_style="cls",
    explicit_zero_prob=False,
    use_mvc_decoder=False,
    ffn_activation_name="relu",
    use_glu_in_ffn=False,
    # Option to use mask and predict gene ids
    # in addition to expression values
    use_gene_id_mlm=False,
)


@pytest.mark.parametrize("use_gene_id_mlm", [True, False])
@pytest.mark.parametrize("use_discrete_gene_expr", [True, False])
def test_mlm_decoders(use_gene_id_mlm: bool, use_discrete_gene_expr: bool) -> None:
    """
    Test that the model can be run with a random input.
    Currently does not test for MVC decoder
    """

    config = copy.deepcopy(default_config)
    config.use_gene_id_mlm = use_gene_id_mlm
    config.use_discrete_gene_expr = use_discrete_gene_expr

    model_fn = build_scgpt_fn(config)
    model_fn = hk.transform(model_fn)
    # Test that the model can be run
    # with a random input
    rng = PRNGKey(0)
    batch_size = 2
    num_genes = 100
    gene_ids = np.random.randint(
        0, config.gene_ids_vocabulary_size, size=(batch_size, num_genes)
    )
    gene_expr = np.random.randint(0, 51, size=(batch_size, num_genes))
    if use_discrete_gene_expr:
        gene_expr = gene_expr.astype(np.int32)
    else:
        gene_expr = gene_expr.astype(np.float32)
    attention_mask = jnp.ones((batch_size, 1, num_genes, num_genes))
    params = model_fn.init(
        rng,
        gene_indexes=gene_ids,
        gene_expression_values=gene_expr,
        attention_mask=attention_mask,
        is_training=True,
    )

    outputs = model_fn.apply(
        params,
        rng,
        gene_indexes=gene_ids,
        gene_expression_values=gene_expr,
        attention_mask=attention_mask,
        is_training=True,
    )

    # Check that the output has the correct shape
    pytest.assume(outputs is not None, "Outputs is None")
    if use_discrete_gene_expr:
        pytest.assume(
            outputs["gene_expression_predictions"].shape
            == (batch_size, num_genes, config.gene_expr_vocabulary_size),
            "Gene expr logits shape is incorrect",
        )
    else:
        pytest.assume(
            outputs["gene_expression_predictions"].shape == (batch_size, num_genes),
            "Gene expr logits shape is incorrect",
        )
    if use_gene_id_mlm:
        pytest.assume(
            "gene_id_predictions" in outputs, "Gene id predictions not in outputs"
        )
        pytest.assume(
            outputs["gene_id_predictions"].shape
            == (batch_size, num_genes, config.gene_ids_vocabulary_size),
            "Gene id logits shape is incorrect",
        )
