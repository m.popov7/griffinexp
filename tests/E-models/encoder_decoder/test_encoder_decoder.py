import haiku as hk
import jax
import pytest

from trix.models.encoder_decoder.model import (
    DecoderConfig,
    EncoderConfig,
    build_encoder_decoder_fn,
)


@pytest.mark.parametrize("alphabet_size", [10])
@pytest.mark.parametrize("batch_size", [16])
@pytest.mark.parametrize("embed_dim", [512])
@pytest.mark.parametrize("max_positions", [40])
def test_build_encoder_decoder_fn(
    alphabet_size: int, batch_size: int, max_positions: int, embed_dim: int
) -> None:
    """Tests that the call of EncoderDecoder returns the right shapes."""
    encoder_config = EncoderConfig(
        alphabet_size=alphabet_size,
        max_positions=max_positions,
        embed_dim=embed_dim,
        num_attention_heads=8,
        num_layers=6,
    )
    decoder_config = DecoderConfig(
        alphabet_size=alphabet_size,
        max_positions=max_positions,
        embed_dim=embed_dim,
        num_attention_heads=8,
        num_layers=6,
    )

    encoder_decoder_fn = build_encoder_decoder_fn(encoder_config, decoder_config)
    encoder_decoder_fn = hk.transform(encoder_decoder_fn)

    random_key = jax.random.PRNGKey(0)
    tokens = jax.random.choice(
        random_key, alphabet_size, shape=(batch_size, max_positions)
    )
    random_key, _ = jax.random.split(random_key)
    decoder_tokens = jax.random.choice(
        random_key, alphabet_size, shape=(batch_size, max_positions)
    )

    params = encoder_decoder_fn.init(random_key, tokens, decoder_tokens)
    apply_fn = jax.jit(encoder_decoder_fn.apply)
    encoder_outs, decoder_outs = apply_fn(params, random_key, tokens, decoder_tokens)

    pytest.assume(
        encoder_outs["embeddings"].shape == (batch_size, max_positions, embed_dim)
    )
    pytest.assume(
        decoder_outs["logits"].shape == (batch_size, max_positions, alphabet_size)
    )
    pytest.assume(
        decoder_outs["attention_weights"].shape
        == (batch_size, max_positions, max_positions)
    )
