import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.perceiver_resampler.model import (
    PerceiverResamplerConfig,
    build_perceiver_resampler_fn,
)
from trix.utils.masking import build_perceiver_padding_attention_mask


def test_perceiver_resampler_config() -> None:
    """Tests the configuration."""
    embed_dim = 1024
    attention_heads = 20
    with pytest.raises(ValueError):
        _ = PerceiverResamplerConfig(
            embed_dim=embed_dim, attention_heads=attention_heads
        )


@pytest.mark.parametrize("batch_size", [1, 4])
@pytest.mark.parametrize("resampled_length", [3])
def test_perceiver_resampler_model(batch_size: int, resampled_length: int) -> None:
    """
    Initialize and do one pass in a perceiver resampler model. Check output shape.
    Add a test to make sure that masking does not change the output.
    """

    # Testing params
    seq_length = 512

    num_heads = 16
    embed_dim = 32
    ffn_embed_dim = 64

    num_layers = 6

    # Get config
    config = PerceiverResamplerConfig(
        attention_heads=num_heads,
        embed_dim=embed_dim,
        ffn_embed_dim=ffn_embed_dim,
        num_layers=num_layers,
        add_bias_kv=False,
        add_bias_ffn=False,
        use_glu_in_ffn=True,
        resampled_length=resampled_length,
    )

    # Define forward fn
    forward_fn = build_perceiver_resampler_fn(model_config=config)
    forward_fn = hk.transform(forward_fn)

    # Create tokens
    random_key = jax.random.PRNGKey(0)

    tokens = jax.random.randint(
        key=random_key, shape=(batch_size, seq_length), minval=1, maxval=10
    )
    random_key, _ = jax.random.split(random_key)
    pad_idx = jax.random.choice(
        key=random_key, a=jnp.array([0, 1]), shape=(tokens.shape)
    )
    # mask random tokens
    tokens = tokens * pad_idx

    # Initialize params
    random_key, _ = jax.random.split(random_key)
    input_embeddings = jax.random.uniform(
        random_key, shape=(batch_size, seq_length, embed_dim)
    )
    params = forward_fn.init(random_key, input_embeddings=input_embeddings)

    # Forward pass
    attention_mask = build_perceiver_padding_attention_mask(
        tokens=tokens, resampled_length=resampled_length, pad_token_id=0
    )
    outs = forward_fn.apply(
        params,
        random_key,
        input_embeddings=input_embeddings,
        attention_mask=attention_mask,
    )

    # Create modified embeddings at padding positions to check attention_mask.
    random_key, _ = jax.random.split(random_key)
    embedding_pad_idx = jnp.expand_dims(pad_idx, axis=2)
    new_input_embeddings = jax.random.uniform(
        random_key, shape=(batch_size, seq_length, embed_dim)
    )
    new_input_embeddings = (
        input_embeddings * embedding_pad_idx
        + new_input_embeddings * (1 - embedding_pad_idx)
    )
    new_outs = forward_fn.apply(
        params,
        random_key,
        input_embeddings=new_input_embeddings,
        attention_mask=attention_mask,
    )

    # Check shape
    output_shape = outs["embeddings"].shape
    expected_shape = (batch_size, resampled_length, embed_dim)
    pytest.assume(
        output_shape == expected_shape,
        f"Output shape is {output_shape} while expected shape is {expected_shape}",
    )

    embeddings = outs["embeddings"]
    new_embeddings = new_outs["embeddings"]
    pytest.assume(
        jnp.allclose(embeddings, new_embeddings),
        "attention mask is not working properly",
    )
