from typing import Any

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.generators import (
    generate_fake_labeled_dataset,
    generate_fake_regression_dataset,
)
from trix.dataloaders.language_models.sequence_datasets import LabeledSequenceDataset
from trix.layers.heads.classification_head import (
    SimpleClassificationHead,
    SimpleMultiClassificationHead,
)
from trix.models.esm.finetuning import build_esm_ia3_rescaling_with_head_fn
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.standard import StandardTokenizer
from trix.training.losses import (
    cross_entropy_loss_classification,
    cross_entropy_loss_multiregression,
)
from trix.training.supervised_trainers.classification_trainer import (
    ClassificationTrainer,
)
from trix.training.supervised_trainers.discretized_regression_trainer import (
    DiscretizedRegressionTrainer,
)
from trix.training.supervised_trainers.runners import (
    run_test_supervised_epoch,
    run_train_supervised_epoch,
)
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.supervised import compute_binned_labels


@pytest.mark.parametrize(
    (
        "add_bias_kv",
        "positional_embedding",
    ),
    [[True, None], [False, "alibi_dnabert_2"], [False, None]],
)
@pytest.mark.parametrize("num_layers", [2])
@pytest.mark.parametrize("pre_layer_norm", [False, True])
@pytest.mark.parametrize("add_bias_ffn", [False, True])
def test_finetuning_classification_loop(
    add_bias_kv: bool,
    num_layers: int,
    pre_layer_norm: bool,
    positional_embedding: str,
    add_bias_ffn: bool,
) -> None:
    """
    Test supports to fine-tune pre-trained ESM models with IA³ technique for
    classification. This test does not assess any performance yet, only that code
    executes properly.
    """
    # Tokenize
    tokenizer = StandardTokenizer(
        standard_tokens=AMINO_ACIDS,
    )
    # Intntiate ESM model
    config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        max_positions=256,
        attention_heads=2,
        embed_dim=8,
        ffn_embed_dim=16,
        num_layers=num_layers,
        positional_embedding=positional_embedding,
        lm_head="simple",
        add_bias_kv=add_bias_kv,
        add_bias_ffn=add_bias_ffn,
        use_glu_in_ffn=True,
        ffn_activation_name="gelu",
        use_rotary_embedding=(not add_bias_kv) and (positional_embedding is None),
        pre_layer_norm=pre_layer_norm,
        mask_before_attention=False,
        token_dropout=False,
        embeddings_layers_to_save=(num_layers,),
    )

    forward_fn = build_esm_fn(model_config=config)
    forward_fn = hk.transform(forward_fn)
    random_key = jax.random.PRNGKey(0)

    # Get fine-tunable model for classification
    num_classes = len(tokenizer.standard_tokens)  # dummy example

    # The classification head is wrapped inside head_fn because
    # haiku modules cannot be instantiated outside hk.transform.
    def head_fn() -> hk.Module:
        return SimpleClassificationHead(num_classes=num_classes)

    finetune_forward_fn = build_esm_ia3_rescaling_with_head_fn(
        model_config=config,  # type: ignore
        head_fn=head_fn,
        model_name="esm_transformer",
    )
    finetune_forward_fn = hk.transform(finetune_forward_fn)

    # Random initialize fine-tunable model
    # Get data and tokenize it
    # Generate fake protein sequences
    sequences, labels = generate_fake_labeled_dataset(
        min_sequence_length=100,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
    tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

    # Initialize random key
    random_key = jax.random.PRNGKey(0)

    # Initialize finetunable model
    pretrained_params = forward_fn.init(rng=random_key, tokens=tokens)
    finetune_params = finetune_forward_fn.init(rng=random_key, tokens=tokens)

    # Filter out the unwanted RobertaLMHead parameters.
    def parameters_roberta_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "roberta" not in module_name
        cond_2 = "emb_layer_norm_after" in module_name
        return cond_1 or cond_2

    pretrained_params, _ = hk.data_structures.partition(
        parameters_roberta_partition_fn, pretrained_params
    )

    # Partition its params into trainable and non-trainable explicitly.
    def parameters_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "ia3_rescaling" in param_name
        cond_2 = "classification_head" in module_name
        return cond_1 or cond_2

    trainable_params, _ = hk.data_structures.partition(
        parameters_partition_fn, finetune_params
    )
    pytest.assume(
        "ffn_ia3_rescaling" in trainable_params["esm_transformer/attention_layer_0"],
        "missing ffn_ia3_rescaling in IA3 parameters",
    )
    pytest.assume(
        "key_ia3_rescaling"
        in trainable_params["esm_transformer/attention_layer_0/~/self_attention"],
        "missing key_ia3_rescaling in IA3 parameters",
    )
    pytest.assume(
        "value_ia3_rescaling"
        in trainable_params["esm_transformer/attention_layer_0/~/self_attention"],
        "missing value_ia3_rescaling in IA3 parameters",
    )
    pytest.assume(
        "simple_classification_head/~/fc" in trainable_params,
        "missing simple_classification_head in IA3 parameters",
    )

    # Replace randomly initialized non-trainable params by pretrained ones
    finetune_params = hk.data_structures.merge(trainable_params, pretrained_params)

    # Get first device
    devices = jax.devices()[:1]
    num_devices = len(devices)

    # Setup dummy parameters for testing purpose
    mini_batch_size = 1
    batch_size = 4
    num_acc_grads = batch_size // (num_devices * mini_batch_size)
    learning_rate = 3e-3

    tokens = jnp.array([tok[1] for tok in tokenizer.batch_tokenize(sequences)])

    # Convert labels to numpy arrays
    labels = np.array(labels)[:, 0]

    outs_finetune = finetune_forward_fn.apply(finetune_params, random_key, tokens)
    outs_no_finetune = forward_fn.apply(pretrained_params, random_key, tokens)
    pytest.assume(
        np.allclose(
            outs_no_finetune[f"embeddings_{config.num_layers}"],
            outs_finetune[f"embeddings_{config.num_layers}"],
            atol=1e-6,
        ),
        "untrained IA3 model should yield the same result as the original model",
    )
    # Convert labels to arrays
    train_dataset = LabeledSequenceDataset(
        sequences=sequences[:16],
        labels=labels[:16],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
    )

    test_dataset = LabeledSequenceDataset(
        sequences=sequences[16:],
        labels=labels[16:],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
    )

    # Setup optimizer
    optimizer = optax.adam(learning_rate=learning_rate)

    # Setup trainer
    trainer = ClassificationTrainer(
        forward_fn=finetune_forward_fn,
        loss_fn=cross_entropy_loss_classification,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=num_classes,
        parameters_partition_fn=parameters_partition_fn,
    )

    # Initialize on devices
    key_cpu = jax.random.PRNGKey(seed=0)
    keys = jnp.stack([key_cpu for _ in range(num_devices)])
    dummy_tokens, _ = next(train_dataset.get_epoch_batches())  # type: ignore
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, mini_batch_size, -1)
    )[:, 0, :, :]
    finetune_params = jax.device_put_replicated(finetune_params, devices=devices)
    training_state = jax.pmap(trainer.init, devices=devices, donate_argnums=(2,))(
        keys, dummy_tokens, finetune_params
    )

    # Performs an epoch training and testing loop
    compute_metrics = trainer.compute_metrics
    update = trainer.update

    training_state, _ = run_train_supervised_epoch(
        update_fn=update,
        dataset=train_dataset,
        devices=devices,
        num_acc_grads=num_acc_grads,
        batch_size=mini_batch_size,
        training_state=training_state,
        epoch_num=0,
    )

    compute_metrics = jax.pmap(compute_metrics, devices=devices, axis_name="batch")
    _ = run_test_supervised_epoch(
        compute_metrics_fn=compute_metrics,
        dataset=test_dataset,
        num_data_parallel_ways=num_devices,
        batch_size=mini_batch_size * num_acc_grads,
        training_state=training_state,
    )


@pytest.mark.parametrize("num_labels", [2])
@pytest.mark.parametrize("num_bins", [50])
def test_finetuning_discretized_regression_loop(num_bins: int, num_labels: int) -> None:
    """
    Test supports to fine-tune pre-trained ESM models with IA³ technique for
    discretized regression. This test does not assess any performance yet, only that
    code executes properly.
    For more information on discretized regression, see the introduction in
    notebooks/finetuning_esm_example_discretized_regression.ipynb
    """
    # Tokenize
    tokenizer = StandardTokenizer(
        standard_tokens=AMINO_ACIDS,
    )
    # Intntiate ESM model
    config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        max_positions=256,
        attention_heads=2,
        embed_dim=8,
        ffn_embed_dim=16,
        num_layers=1,
        positional_embedding=None,
        lm_head="simple",
        add_bias_kv=False,
        use_rotary_embedding=True,
        mask_before_attention=False,
        token_dropout=False,
    )

    forward_fn = build_esm_fn(model_config=config)
    forward_fn = hk.transform(forward_fn)
    random_key = jax.random.PRNGKey(0)

    # The classification head is wrapped inside head_fn because
    # haiku modules cannot be instantiated outside hk.transform.
    def head_fn() -> hk.Module:
        return SimpleMultiClassificationHead(
            num_classes=num_bins + 1, num_labels=num_labels
        )

    finetune_forward_fn = build_esm_ia3_rescaling_with_head_fn(
        model_config=config,  # type: ignore
        head_fn=head_fn,
        model_name="esm_transformer",
    )
    finetune_forward_fn = hk.transform(finetune_forward_fn)

    # Random initialize fine-tunable model
    # Get data and tokenize it
    # Generate fake protein sequences
    sequences, labels = generate_fake_regression_dataset(
        min_sequence_length=100,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        num_labels=num_labels,
        seed=0,
    )

    # Convert labels to numpy arrays
    labels = np.array(labels)

    # Compute the binned version of the labels as well as the corresponding bins.
    labels_binned, labels_bins = compute_binned_labels(labels, num_bins)

    tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
    tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

    # Initialize random key
    random_key = jax.random.PRNGKey(0)

    # Initialize finetunable model
    pretrained_params = forward_fn.init(rng=random_key, tokens=tokens)
    finetune_params = finetune_forward_fn.init(rng=random_key, tokens=tokens)

    # Filter out the unwanted RobertaLMHead parameters.
    def parameters_roberta_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "roberta" not in module_name
        cond_2 = "emb_layer_norm_after" in module_name
        return cond_1 or cond_2

    pretrained_params, _ = hk.data_structures.partition(
        parameters_roberta_partition_fn, pretrained_params
    )

    # Partition its params into trainable and non-trainable explicitly.
    def parameters_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "ia3_rescaling" in param_name
        cond_2 = "classification_head" in module_name
        return cond_1 or cond_2

    trainable_params, _ = hk.data_structures.partition(
        parameters_partition_fn, finetune_params
    )

    # Replace randomly initialized non-trainable params by pretrained ones
    finetune_params = hk.data_structures.merge(trainable_params, pretrained_params)

    # Get first device
    devices = jax.devices()[:1]
    num_devices = len(devices)

    # Setup dummy parameters for testing purpose
    mini_batch_size = 1
    batch_size = 4
    num_acc_grads = batch_size // (num_devices * mini_batch_size)
    learning_rate = 3e-3

    tokens = jnp.array([tok[1] for tok in tokenizer.batch_tokenize(sequences)])

    # Convert labels to arrays
    train_dataset = LabeledSequenceDataset(
        sequences=sequences[:16],
        labels=labels_binned[:16],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
        labels_continuous=True,
    )

    test_dataset = LabeledSequenceDataset(
        sequences=sequences[16:],
        labels=labels_binned[16:],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
        labels_continuous=True,
    )

    # Setup optimizer
    optimizer = optax.adam(learning_rate=learning_rate)

    # Setup trainer
    trainer = DiscretizedRegressionTrainer(
        forward_fn=finetune_forward_fn,
        loss_fn=cross_entropy_loss_multiregression,
        tokenizer=tokenizer,
        optimizer=optimizer,
        parameters_partition_fn=parameters_partition_fn,
    )

    # Initialize on devices
    key_cpu = jax.random.PRNGKey(seed=0)
    keys = jnp.stack([key_cpu for _ in range(num_devices)])
    dummy_tokens, _ = next(train_dataset.get_epoch_batches())  # type: ignore
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, mini_batch_size, -1)
    )[:, 0, :, :]
    finetune_params = jax.device_put_replicated(finetune_params, devices=devices)
    training_state = jax.pmap(trainer.init, devices=devices, donate_argnums=(2,))(
        keys, dummy_tokens, finetune_params
    )

    # Performs an epoch training and testing loop
    compute_metrics = trainer.compute_metrics
    update = trainer.update

    training_state, _ = run_train_supervised_epoch(
        update_fn=update,
        dataset=train_dataset,
        devices=devices,
        num_acc_grads=num_acc_grads,
        batch_size=mini_batch_size,
        training_state=training_state,
        epoch_num=0,
    )

    compute_metrics = jax.pmap(compute_metrics, devices=devices, axis_name="batch")
    _ = run_test_supervised_epoch(
        compute_metrics_fn=compute_metrics,
        dataset=test_dataset,
        num_data_parallel_ways=num_devices,
        batch_size=mini_batch_size * num_acc_grads,
        training_state=training_state,
    )
