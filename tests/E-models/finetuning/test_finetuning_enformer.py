from typing import Any

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.generators import generate_fake_labeled_dataset
from trix.dataloaders.language_models.sequence_datasets import LabeledSequenceDataset
from trix.layers.heads.classification_head import SimpleClassificationHead
from trix.models.enformer.finetuning import build_enformer_fn_ia3_rescaling_with_head_fn
from trix.models.enformer.model import EnformerConfig, build_enformer_fn
from trix.tokenizers.language_models.bio import NucleotidesKmersTokenizer
from trix.training.losses import cross_entropy_loss_classification_with_state
from trix.training.supervised_trainers.classification_trainer_with_state import (
    ClassificationTrainerWithState,
)
from trix.training.supervised_trainers.runners import (
    run_test_supervised_epoch,
    run_train_supervised_epoch,
)


@pytest.mark.parametrize("seq_length", [196_608, 1000, 300])
def test_finetuning_classification_loop(seq_length: int) -> None:
    """
    Test supports to fine-tune pre-trained Enformer models with IA³ technique for
    classification. This test does not assess any performance yet, only that code
    executes properly.
    """

    # Instantiate Enformer model
    config = EnformerConfig(
        embed_dim=384,
        num_transformer_layers=2,
        num_attention_heads=2,
        num_human_output_heads=2,
        num_mouse_output_heads=2,
        target_length=896,
        attention_dim_key=64,
        num_downsamples=7,
        dim_divisible_by=128,
    )

    forward_fn = build_enformer_fn(config=config)
    forward_fn = hk.transform_with_state(forward_fn)

    # Get fine-tunable model for classification
    num_classes = 10  # dummy example

    # The classification head is wrapped inside head_fn because
    # haiku modules cannot be instantiated outside hk.transform.
    def head_fn() -> hk.Module:
        return SimpleClassificationHead(num_classes=num_classes)

    # if using a different length, do not trim sequences
    if seq_length != 196_608:
        config.target_length = -1

    finetune_forward_fn = build_enformer_fn_ia3_rescaling_with_head_fn(
        config=config, head_fn=head_fn, name="enformer"
    )
    finetune_forward_fn = hk.transform_with_state(finetune_forward_fn)

    # Random initialize fine-tunable model
    # Get data and tokenize it
    # Generate fake DNA sequences
    dna_nucleotides = ["A", "C", "G", "T", "N"]
    num_sequences = 10
    sequences, labels = generate_fake_labeled_dataset(
        min_sequence_length=seq_length,
        max_sequence_length=seq_length,
        num_sequences=num_sequences,
        standard_tokens=dna_nucleotides,
        seed=0,
    )

    # Tokenize sequences
    tokenizer = NucleotidesKmersTokenizer(k_mers=1)
    tokens_ids = [b[1] for b in tokenizer.batch_tokenize(sequences)]
    tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

    # Initialize random key
    random_key = jax.random.PRNGKey(0)

    # Initialize finetunable model with two sequences
    pretrained_params, pretrained_state = forward_fn.init(
        rng=random_key, tokens=tokens[:2], is_training=True
    )
    finetune_params, finetune_state = finetune_forward_fn.init(
        rng=random_key, tokens=tokens[:2], is_training=True
    )

    # Partition its params into trainable and non-trainable explicitly.
    def parameters_partition_fn(
        module_name: str, param_name: str, param_data: Any
    ) -> bool:
        cond_1 = "ia3_rescaling" in param_name
        cond_2 = "classification_head" in module_name
        return cond_1 or cond_2

    trainable_params, _ = hk.data_structures.partition(
        parameters_partition_fn, finetune_params
    )
    pytest.assume(
        "ffn_ia3_rescaling"
        in trainable_params["enformer/~_transformer_ffn_block/layer_0"],
        "missing ffn_ia3_rescaling in IA3 parameters",
    )
    pytest.assume(
        "key_ia3_rescaling"
        in trainable_params["enformer/~_transformer_attention/layer_0/attention"],
        "missing key_ia3_rescaling in IA3 parameters",
    )
    pytest.assume(
        "value_ia3_rescaling"
        in trainable_params["enformer/~_transformer_attention/layer_0/attention"],
        "missing value_ia3_rescaling in IA3 parameters",
    )
    pytest.assume(
        "simple_classification_head/~/fc" in trainable_params,
        "missing simple_classification_head in IA3 parameters",
    )

    # Replace randomly initialized non-trainable params by pretrained ones
    finetune_params = hk.data_structures.merge(trainable_params, pretrained_params)

    # Get first device
    devices = jax.devices()[:1]
    num_devices = len(devices)

    # Setup dummy parameters for testing purpose
    mini_batch_size = 1
    batch_size = 2
    n_train = 6
    num_acc_grads = batch_size // (num_devices * mini_batch_size)
    learning_rate = 3e-3

    # Test if untrained IA3 model yields the same result as the original model
    # with only 2 sequences
    outs_no_finetune, _ = forward_fn.apply(
        params=pretrained_params,
        rng=random_key,
        state=pretrained_state,
        tokens=tokens[:2],
        is_training=True,
    )
    outs_finetune, _ = finetune_forward_fn.apply(
        params=finetune_params,
        rng=random_key,
        state=finetune_state,
        tokens=tokens[:2],
        is_training=True,
    )
    pytest.assume(
        np.allclose(
            outs_no_finetune["embedding"],
            outs_finetune["embedding"],
            atol=1e-6,
        ),
        "untrained IA3 model should yield the same result as the original model",
    )

    # Convert labels to numpy arrays
    labels = np.array(labels)[:, 0]

    # create train dataset
    train_dataset = LabeledSequenceDataset(
        sequences=sequences[:n_train],
        labels=labels[:n_train],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
    )

    # create test dataset
    test_dataset = LabeledSequenceDataset(
        sequences=sequences[n_train:],
        labels=labels[n_train:],
        tokenizer=tokenizer,
        batch_size=batch_size,
        shuffle=True,
        drop_last=True,
    )

    # Setup optimizer
    optimizer = optax.adam(learning_rate=learning_rate)

    # Setup trainer
    trainer = ClassificationTrainerWithState(
        forward_fn=finetune_forward_fn,
        loss_fn=cross_entropy_loss_classification_with_state,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=num_classes,
        parameters_partition_fn=parameters_partition_fn,
    )

    # Initialize training state on devices
    key_cpu = jax.random.PRNGKey(seed=0)
    keys = jnp.stack([key_cpu for _ in range(num_devices)])
    dummy_tokens, _ = next(train_dataset.get_epoch_batches())  # type: ignore
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, mini_batch_size, -1)
    )[:, 0, :, :]
    finetune_params_split = jax.device_put_replicated(finetune_params, devices=devices)
    finetune_state_split = jax.device_put_replicated(pretrained_state, devices=devices)
    training_state = jax.pmap(
        trainer.init,
        devices=devices,
        donate_argnums=(
            2,
            3,
        ),
    )(keys, dummy_tokens, finetune_params_split, finetune_state_split)

    # Test running one epoch (train + test)
    compute_metrics = trainer.compute_metrics
    update = trainer.update

    training_state, _ = run_train_supervised_epoch(
        update_fn=update,
        dataset=train_dataset,
        devices=devices,
        num_acc_grads=num_acc_grads,
        batch_size=mini_batch_size,
        training_state=training_state,
        epoch_num=0,
    )

    compute_metrics = jax.pmap(compute_metrics, devices=devices, axis_name="batch")
    _ = run_test_supervised_epoch(
        compute_metrics_fn=compute_metrics,
        dataset=test_dataset,
        num_data_parallel_ways=num_devices,
        batch_size=mini_batch_size * num_acc_grads,
        training_state=training_state,
    )
