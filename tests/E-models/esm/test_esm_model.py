import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.masking import build_padding_attention_mask


def test_esm_config() -> None:
    """
    Tests that value errors are raised for specific arguments into ESMTransformerConfig.
    """
    with pytest.raises(ValueError):

        embed_dim = 5
        num_heads = 2
        _ = ESMTransformerConfig(
            20, 0, 1, embed_dim=embed_dim, attention_heads=num_heads
        )

        positional_emeddings = "pretrained"
        rotary_embeddings = True
        _ = ESMTransformerConfig(20, 0, 1, positional_embedding=positional_emeddings)
        _ = ESMTransformerConfig(
            20,
            0,
            1,
            positional_embedding=positional_emeddings,
            use_rotary_embedding=rotary_embeddings,
        )

        lm_head = "weighted"
        _ = ESMTransformerConfig(20, 0, 1, lm_head=lm_head)

        use_rotary_embedding = True
        add_bias_kv = True
        _ = ESMTransformerConfig(
            20, 0, 1, use_rotary_embedding=use_rotary_embedding, add_bias_kv=add_bias_kv
        )


@pytest.mark.parametrize(
    (
        "positional_embedding",
        "use_rotary_embedding",
        "add_bias_kv",
    ),
    [
        [
            "learned",
            False,
            False,
        ],
        [
            "sinusoidal",
            False,
            False,
        ],
        [
            "sinusoidal",
            False,
            True,
        ],
        [
            None,
            True,
            False,
        ],
        [
            "learned_standard",
            False,
            False,
        ],  # dnabert1 config
        [
            "alibi_dnabert_2",
            False,
            False,
        ],  # dnabert2 config
    ],
)
@pytest.mark.parametrize("lm_head", ["simple", "roberta"])
@pytest.mark.parametrize("token_dropout", [True, False])
@pytest.mark.parametrize("mask_before_attention", [True, False])
@pytest.mark.parametrize("bias_word_embedding", [True, False])
def test_esm_model(
    mask_before_attention: bool,
    token_dropout: bool,
    lm_head: str,
    positional_embedding: str,
    use_rotary_embedding: bool,
    add_bias_kv: bool,
    bias_word_embedding: bool,
) -> None:
    """
    Tests that ESM models return the same outputs for the same input with different
    values of batch_size and pad.
    """
    # Generate fake protein sequences
    sequences = generate_fake_dataset(
        min_sequence_length=20,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    # Tokenize with two lengths
    tokenizer_short_pad = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=128,
    )
    tokenizer_long_pad = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=256,
    )

    token_short_pad = jnp.array(
        [tok[1] for tok in tokenizer_short_pad.batch_tokenize(sequences)]
    )
    token_long_pad = jnp.array(
        [tok[1] for tok in tokenizer_long_pad.batch_tokenize(sequences)]
    )

    # Intntiate ESM model
    config = ESMTransformerConfig(
        alphabet_size=tokenizer_short_pad.vocabulary_size,
        pad_token_id=tokenizer_short_pad.pad_token_id,
        mask_token_id=tokenizer_long_pad.mask_token_id,
        max_positions=256,
        attention_heads=2,
        embed_dim=8,
        ffn_embed_dim=16,
        num_layers=1,
        positional_embedding=positional_embedding,
        lm_head=lm_head,
        add_bias_kv=add_bias_kv,
        use_rotary_embedding=use_rotary_embedding,
        mask_before_attention=mask_before_attention,
        token_dropout=token_dropout,
        bias_word_embedding=bias_word_embedding,
    )

    esm_fn = build_esm_fn(model_config=config)
    esm_fn = hk.transform(esm_fn)
    random_key = jax.random.PRNGKey(0)
    parameters = esm_fn.init(random_key, token_short_pad[:1])

    # Create attention masks and apply the model
    short_attention_mask = build_padding_attention_mask(
        token_short_pad, tokenizer_short_pad.pad_token_id
    )
    long_attention_mask = build_padding_attention_mask(
        token_long_pad, tokenizer_long_pad.pad_token_id
    )
    apply_fn = jax.jit(esm_fn.apply)
    outs_short_pad = apply_fn(
        parameters,
        random_key,
        tokens=token_short_pad,
        attention_mask=short_attention_mask,
    )
    outs_long_pad = apply_fn(
        parameters,
        random_key,
        tokens=token_long_pad,
        attention_mask=long_attention_mask,
    )

    # Get the logits on non pad tokens
    short_sequence_mask = short_attention_mask[:, 0, :, 0][:, :, None]
    long_sequence_mask = long_attention_mask[:, 0, :, 0][:, :, None]
    logits_short_pad = outs_short_pad["logits"] * short_sequence_mask
    logits_long_pad = outs_long_pad["logits"] * long_sequence_mask

    pytest.assume(
        jnp.allclose(logits_short_pad, logits_long_pad[:, :128]),
        "padding changes the logits",
    )

    # Check if batch_size affects the results
    outs_small_batch = apply_fn(parameters, random_key, token_short_pad[:16])
    logits_small_batch = outs_small_batch["logits"] * short_sequence_mask[:16]

    pytest.assume(
        jnp.allclose(logits_short_pad[:16], logits_small_batch),
        "batch size changes the logits",
    )
