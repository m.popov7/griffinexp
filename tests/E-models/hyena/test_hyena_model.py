import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest

from tests.utils.utils import print_error_message_assert_array_equal
from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.hyena_dna.model import HyenaDNAConfig, build_hyena_dna_forward
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS


def test_hyena_model() -> None:
    """
    Tests that Hyena model is autoregressive, meaning that tokens do not influence
     embeddings from tokens left-wise.
    Tests also that the model returns the same outputs for the same input with different
    values of batch_size.
    """
    # Generate fake protein sequences
    sequences_a = generate_fake_dataset(
        min_sequence_length=20,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )
    sequences_b = generate_fake_dataset(
        min_sequence_length=20,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=1,
    )

    # Tokenize with two lengths
    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=128,
    )

    tokens_a = np.array([tok[1] for tok in tokenizer.batch_tokenize(sequences_a)])
    tokens_b = np.array([tok[1] for tok in tokenizer.batch_tokenize(sequences_b)])

    equality_up_to = 30
    for k in range(len(sequences_b)):
        for i in range(equality_up_to):
            tokens_b[k][i] = tokens_a[k][i]

    # Intntiate ESM model
    config = HyenaDNAConfig(
        vocab_size=tokenizer.vocabulary_size,
        pad_vocab_size_multiple=8,
        model_embed_dim=4,
        mlp_dim=16,
        num_hyena_blocks=2,
        embed_dropout=0.1,
        resid_dropout=0.1,
        hyena_operator_filter_positional_embed_dim=5,
        hyena_operator_max_seq_len=256,
        hyena_operator_implicit_filter_mlp_dim=2,
    )

    hyena_fn = build_hyena_dna_forward(
        config,
        model_name="hyena_dna",
    )
    hyena_fn = hk.transform(hyena_fn)
    random_key = jax.random.PRNGKey(0)
    parameters = hyena_fn.init(random_key, tokens_a[:1])

    apply_fn = jax.jit(hyena_fn.apply)
    outs_a = apply_fn(
        parameters,
        random_key,
        tokens=tokens_a,
    )
    outs_b = apply_fn(
        parameters,
        random_key,
        tokens=tokens_b,
    )

    # Get the logits on non pad tokens
    logits_a = outs_a["logits"]
    logits_b = outs_b["logits"]

    logits_a_left_side = logits_a[:, :equality_up_to]
    logits_b_left_side = logits_b[:, :equality_up_to]

    logits_a_right_side = logits_a[:, equality_up_to:]
    logits_b_right_side = logits_b[:, equality_up_to:]

    pytest.assume(
        np.allclose(logits_a_left_side, logits_b_left_side, atol=1e-4),
        "the model is not autoregressive:\n{}".format(
            print_error_message_assert_array_equal(
                logits_a_left_side, logits_b_left_side
            )
        ),
    )
    pytest.assume(
        not (np.allclose(logits_a_right_side, logits_b_right_side, atol=1e-1)),
        "the model is not autoregressive: \n{}".format(
            print_error_message_assert_array_equal(
                logits_a_right_side, logits_b_right_side
            )
        ),
    )

    # Check if batch_size affects the results
    outs_a_small_batch = apply_fn(parameters, random_key, tokens_a[:16])
    logits_a_small_batch = outs_a_small_batch["logits"]

    pytest.assume(
        jnp.allclose(logits_a[:16], logits_a_small_batch, atol=1e-5),
        "batch size changes the logits:\n{}".format(
            print_error_message_assert_array_equal(logits_a[:16], logits_a_small_batch)
        ),
    )
