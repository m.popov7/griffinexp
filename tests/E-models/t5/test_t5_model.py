import functools

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.t5.decoder import build_t5_decoder_only
from trix.models.t5.model import T5Config, T5DecoderConfig, T5EncoderConfig, build_t5_fn
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS


def test_t5_config() -> None:
    """
    Tests that value errors are raised for specific arguments into T5Config.
    """
    with pytest.raises(ValueError):

        # attention_embed_dim % num_heads != 0
        attention_embed_dim = 5
        num_heads = 2
        _ = T5EncoderConfig(
            attention_embed_dim=attention_embed_dim, num_heads=num_heads
        )
        _ = T5DecoderConfig(
            attention_embed_dim=attention_embed_dim, num_heads=num_heads
        )

    with pytest.raises(ValueError):
        # activation not supported
        ffn_activation_name = "elu"
        _ = T5EncoderConfig(ffn_activation_name=ffn_activation_name)
        _ = T5DecoderConfig(ffn_activation_name=ffn_activation_name)

    with pytest.raises(ValueError):
        # different encoder vs decoder
        encoder_config = T5EncoderConfig(vocab_size=26)
        decoder_config = T5DecoderConfig(vocab_size=24)
        _ = T5Config(encoder_config=encoder_config, decoder_config=decoder_config)

    with pytest.raises(ValueError):
        # different encoder vs decoder
        encoder_config = T5EncoderConfig(embed_dim=26)
        decoder_config = T5DecoderConfig(embed_dim=24)
        _ = T5Config(encoder_config=encoder_config, decoder_config=decoder_config)

    with pytest.raises(ValueError):
        # different encoder vs decoder
        encoder_config = T5EncoderConfig(eos_token_id=0)
        decoder_config = T5DecoderConfig(eos_token_id=1)
        _ = T5Config(encoder_config=encoder_config, decoder_config=decoder_config)

    with pytest.raises(ValueError):
        # different encoder vs decoder
        encoder_config = T5EncoderConfig(max_bucket_distance=32)
        decoder_config = T5DecoderConfig(max_bucket_distance=64)
        _ = T5Config(encoder_config=encoder_config, decoder_config=decoder_config)

    with pytest.raises(ValueError):
        # different encoder vs decoder
        encoder_config = T5EncoderConfig(num_buckets=32)
        decoder_config = T5DecoderConfig(num_buckets=64)
        _ = T5Config(encoder_config=encoder_config, decoder_config=decoder_config)


@pytest.mark.parametrize(
    "ffn_activation_name", ["gelu", "gelu-no-approx", "relu", "swish"]
)
@pytest.mark.parametrize("logits_from_embed", [False, True])
@pytest.mark.parametrize("use_glu_in_ffn", [False, True])
def test_t5_model(
    ffn_activation_name: str, logits_from_embed: bool, use_glu_in_ffn: bool
) -> None:
    """
    Tests that T5 model is deterministic, returns the same output for the same input
    with a different padding and the same output for different batch sizes.
    """

    encoder_sequences = generate_fake_dataset(
        min_sequence_length=10,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )
    decoder_sequences = generate_fake_dataset(
        min_sequence_length=10,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=1,
    )
    encoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=128, append_eos_token=True
    )
    decoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=128, append_eos_token=True
    )

    short_pad_encoder_tokens = jnp.array(
        [tok[1] for tok in encoder_tokenizer.batch_tokenize(encoder_sequences)]
    )
    short_pad_decoder_tokens = jnp.array(
        [tok[1] for tok in decoder_tokenizer.batch_tokenize(decoder_sequences)]
    )

    encoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=256, append_eos_token=True
    )
    decoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=256, append_eos_token=True
    )

    long_pad_encoder_tokens = jnp.array(
        [tok[1] for tok in encoder_tokenizer.batch_tokenize(encoder_sequences)]
    )
    long_pad_decoder_tokens = jnp.array(
        [tok[1] for tok in decoder_tokenizer.batch_tokenize(decoder_sequences)]
    )
    pytest.assume(
        jnp.allclose(
            short_pad_encoder_tokens[:, :128], long_pad_encoder_tokens[:, :128]
        ),
        "tokenizers give different inputs for different fixed lengths",
    )
    encoder_config = T5EncoderConfig(
        vocab_size=encoder_tokenizer.vocabulary_size,
        embed_dim=8,
        attention_embed_dim=4,
        ffn_embed_dim=16,
        num_heads=2,
        num_layers=1,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        num_buckets=4,
        max_bucket_distance=128,
        pad_token_id=encoder_tokenizer.pad_token_id,
        eos_token_id=encoder_tokenizer.eos_token_id,
    )
    decoder_config = T5DecoderConfig(
        vocab_size=encoder_tokenizer.vocabulary_size,
        embed_dim=8,
        attention_embed_dim=4,
        ffn_embed_dim=16,
        num_heads=2,
        num_layers=1,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        num_buckets=4,
        max_bucket_distance=128,
        logits_from_embed=logits_from_embed,
        pad_token_id=encoder_tokenizer.pad_token_id,
        eos_token_id=encoder_tokenizer.eos_token_id,
    )
    config = T5Config(encoder_config=encoder_config, decoder_config=decoder_config)

    t5_fn = build_t5_fn(config)
    t5_fn = hk.transform(t5_fn)

    random_key = jax.random.PRNGKey(0)
    parameters = t5_fn.init(
        random_key,
        encoder_token_ids=short_pad_encoder_tokens,
        decoder_token_ids=short_pad_decoder_tokens[:, :1],
    )

    apply_fn = jax.jit(functools.partial(t5_fn.apply))
    short_pad_logits = apply_fn(
        parameters,
        random_key,
        encoder_token_ids=short_pad_encoder_tokens,
        decoder_token_ids=short_pad_decoder_tokens,
    )["logits"]

    new_key, _ = jax.random.split(random_key)
    short_pad_logits_2 = apply_fn(
        parameters,
        new_key,
        encoder_token_ids=short_pad_encoder_tokens,
        decoder_token_ids=short_pad_decoder_tokens,
    )["logits"]

    decoder_pad_mask = short_pad_decoder_tokens != decoder_tokenizer.pad_token_id
    decoder_pad_mask = jnp.expand_dims(decoder_pad_mask, axis=-1)
    pytest.assume(
        jnp.allclose(
            decoder_pad_mask * short_pad_logits_2, decoder_pad_mask * short_pad_logits
        ),
        "random_key changes the logits",
    )

    long_pad_logits = apply_fn(
        parameters,
        random_key,
        encoder_token_ids=long_pad_encoder_tokens,
        decoder_token_ids=long_pad_decoder_tokens,
    )["logits"][:, :128]

    pytest.assume(
        jnp.allclose(
            long_pad_logits * decoder_pad_mask, short_pad_logits * decoder_pad_mask
        ),
        "padding changes the logits",
    )

    small_batch_logits = apply_fn(
        parameters,
        random_key,
        encoder_token_ids=short_pad_encoder_tokens[:16, :],
        decoder_token_ids=short_pad_decoder_tokens[:16, :],
    )["logits"]
    pytest.assume(
        jnp.allclose(
            small_batch_logits * decoder_pad_mask[:16],
            short_pad_logits[:16] * decoder_pad_mask[:16],
        ),
        "batch size changes the logits",
    )


@pytest.mark.parametrize(
    "ffn_activation_name", ["gelu", "gelu-no-approx", "relu", "swish"]
)
@pytest.mark.parametrize("logits_from_embed", [False, True])
@pytest.mark.parametrize("use_glu_in_ffn", [False, True])
def test_t5_decoder_only(
    ffn_activation_name: str, logits_from_embed: bool, use_glu_in_ffn: bool
) -> None:
    """
    Tests that T5 model is deterministic, returns the same output for the same input
    with a different padding and the same output for different batch sizes.
    """
    decoder_sequences = generate_fake_dataset(
        min_sequence_length=10,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=1,
    )
    decoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=128, append_eos_token=True
    )

    short_pad_decoder_tokens = jnp.array(
        [tok[1] for tok in decoder_tokenizer.batch_tokenize(decoder_sequences)]
    )

    decoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=256, append_eos_token=True
    )

    long_pad_decoder_tokens = jnp.array(
        [tok[1] for tok in decoder_tokenizer.batch_tokenize(decoder_sequences)]
    )

    decoder_config = T5DecoderConfig(
        vocab_size=decoder_tokenizer.vocabulary_size,
        embed_dim=8,
        attention_embed_dim=4,
        ffn_embed_dim=16,
        num_heads=2,
        num_layers=1,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        num_buckets=4,
        max_bucket_distance=128,
        logits_from_embed=logits_from_embed,
        pad_token_id=decoder_tokenizer.pad_token_id,
        eos_token_id=decoder_tokenizer.eos_token_id,
    )

    t5_fn = build_t5_decoder_only(decoder_config)
    t5_fn = hk.transform(t5_fn)

    random_key = jax.random.PRNGKey(0)
    parameters = t5_fn.init(
        random_key,
        decoder_token_ids=short_pad_decoder_tokens,
    )

    apply_fn = jax.jit(functools.partial(t5_fn.apply))
    short_pad_logits = apply_fn(
        parameters,
        random_key,
        decoder_token_ids=short_pad_decoder_tokens,
    )["logits"]

    new_key, _ = jax.random.split(random_key)
    short_pad_logits_2 = apply_fn(
        parameters,
        new_key,
        decoder_token_ids=short_pad_decoder_tokens,
    )["logits"]

    decoder_pad_mask = short_pad_decoder_tokens != decoder_tokenizer.pad_token_id
    decoder_pad_mask = jnp.expand_dims(decoder_pad_mask, axis=-1)
    pytest.assume(
        jnp.allclose(
            decoder_pad_mask * short_pad_logits_2, decoder_pad_mask * short_pad_logits
        ),
        "random_key changes the logits",
    )

    long_pad_logits = apply_fn(
        parameters,
        random_key,
        decoder_token_ids=long_pad_decoder_tokens,
    )["logits"][:, :128]

    pytest.assume(
        jnp.allclose(
            long_pad_logits * decoder_pad_mask, short_pad_logits * decoder_pad_mask
        ),
        "padding changes the logits",
    )

    small_batch_logits = apply_fn(
        parameters,
        random_key,
        decoder_token_ids=short_pad_decoder_tokens[:16, :],
    )["logits"]
    pytest.assume(
        jnp.allclose(
            small_batch_logits * decoder_pad_mask[:16],
            short_pad_logits[:16] * decoder_pad_mask[:16],
        ),
        "batch size changes the logits",
    )
