import functools

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.ankh.layers import ConvBertConfig
from trix.models.ankh.model import (
    build_ankh_convbert_head_fn,
    build_ankh_encoder_and_task_fn,
    build_ankh_encoder_only_fn,
    build_ankh_transformer_fn,
    build_padding_attention_mask,
)
from trix.models.t5.model import T5Config, T5DecoderConfig, T5EncoderConfig
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS


def test_ankh_convbert_head_fn() -> None:
    """
    Test that you can use Ankh build_..._fn and that the encoder + task is invariant to
    padding.
    """
    encoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=128, append_eos_token=True
    )

    head_config = ConvBertConfig(
        embed_dim=8,
        conv_fc_embed_dim=16,
        kernel_size=3,
        conv_num_heads=2,
        conv_dropout_rate=0.0,
        pooling="max",
    )
    encoder_config = T5EncoderConfig(
        vocab_size=8,
        embed_dim=8,
        attention_embed_dim=4,
        ffn_embed_dim=16,
        num_heads=2,
        num_layers=1,
        ffn_activation_name="swish",
        use_glu_in_ffn=False,
        num_buckets=4,
        max_bucket_distance=128,
        pad_token_id=encoder_tokenizer.pad_token_id,
        eos_token_id=encoder_tokenizer.eos_token_id,
    )
    decoder_config = T5DecoderConfig(
        vocab_size=8,
        embed_dim=8,
        attention_embed_dim=4,
        ffn_embed_dim=16,
        num_heads=2,
        num_layers=1,
        ffn_activation_name="swish",
        use_glu_in_ffn=False,
        num_buckets=4,
        max_bucket_distance=128,
        logits_from_embed=False,
        pad_token_id=encoder_tokenizer.pad_token_id,
        eos_token_id=encoder_tokenizer.eos_token_id,
    )
    model_config = T5Config(
        encoder_config=encoder_config, decoder_config=decoder_config
    )

    convbert_head_fn = build_ankh_convbert_head_fn(conv_bert_config=head_config)
    convbert_head_fn = hk.transform(convbert_head_fn)

    encoder_and_task_fn = build_ankh_encoder_and_task_fn(
        encoder_config=encoder_config, head_config=head_config
    )
    encoder_and_task_fn = hk.transform(encoder_and_task_fn)

    encoder_fn = build_ankh_encoder_only_fn(encoder_config=encoder_config)
    encoder_fn = hk.transform(encoder_fn)

    ankh_fn = build_ankh_transformer_fn(config=model_config)
    ankh_fn = hk.transform(ankh_fn)

    encoder_sequences = generate_fake_dataset(
        min_sequence_length=10,
        max_sequence_length=100,
        num_sequences=32,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )
    short_pad_encoder_tokens = jnp.array(
        [tok[1] for tok in encoder_tokenizer.batch_tokenize(encoder_sequences)]
    )

    encoder_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=256, append_eos_token=True
    )

    long_pad_encoder_tokens = jnp.array(
        [tok[1] for tok in encoder_tokenizer.batch_tokenize(encoder_sequences)]
    )
    pytest.assume(
        jnp.allclose(
            short_pad_encoder_tokens[:, :128], long_pad_encoder_tokens[:, :128]
        ),
        "tokenizers give different inputs for different fixed lengths",
    )

    random_key = jax.random.PRNGKey(0)
    parameters = encoder_and_task_fn.init(
        random_key,
        encoder_token_ids=short_pad_encoder_tokens,
    )

    apply_fn = jax.jit(encoder_and_task_fn.apply)
    short_pad_outs = apply_fn(
        parameters,
        random_key,
        encoder_token_ids=short_pad_encoder_tokens,
    )["output"]

    new_key, _ = jax.random.split(random_key)
    short_pad_outs_2 = apply_fn(
        parameters,
        new_key,
        encoder_token_ids=short_pad_encoder_tokens,
    )["output"]

    pytest.assume(
        jnp.allclose(short_pad_outs, short_pad_outs_2),
        "random_key changes the outputs",
    )

    long_pad_outs = apply_fn(
        parameters,
        random_key,
        encoder_token_ids=long_pad_encoder_tokens,
    )["output"]

    pytest.assume(
        jnp.allclose(long_pad_outs, short_pad_outs),
        "padding changes the outputs",
    )

    small_batch_outs = apply_fn(
        parameters,
        random_key,
        encoder_token_ids=short_pad_encoder_tokens[:16, :],
    )["output"]

    pytest.assume(
        jnp.allclose(small_batch_outs, short_pad_outs[:16], atol=1e-6),
        "batch size changes the outputs",
    )

    encoder_output = encoder_fn.apply(
        parameters, random_key, encoder_token_ids=short_pad_encoder_tokens
    )["embeddings"]

    # test that those functions do not give NaNs
    head_output = convbert_head_fn.apply(
        parameters,
        random_key,
        encoder_output=encoder_output,
        attention_mask=build_padding_attention_mask(
            short_pad_encoder_tokens, encoder_tokenizer.pad_token_id
        ),
    )
    pytest.assume(not jnp.isnan(head_output["output"]).all(), "NaNs in head output")

    ankh_parameters = ankh_fn.init(
        random_key,
        encoder_token_ids=short_pad_encoder_tokens,
        decoder_token_ids=short_pad_encoder_tokens[:, :1],
    )

    apply_fn = jax.jit(functools.partial(ankh_fn.apply))
    logits = apply_fn(
        ankh_parameters,
        random_key,
        encoder_token_ids=short_pad_encoder_tokens,
        decoder_token_ids=short_pad_encoder_tokens,
    )["logits"]
    pytest.assume(not jnp.isnan(logits).any(), "NaNs in transformer output")
