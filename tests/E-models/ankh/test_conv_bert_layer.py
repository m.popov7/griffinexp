from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.ankh.layers import ConvBertHead
from trix.models.ankh.model import ConvBertConfig
from trix.utils.masking import build_padding_attention_mask


def test_conv_bert_config() -> None:
    """
    Tests that ConvBertConfig raises errors for invalid configurations.
    """
    with pytest.raises(ValueError):
        conv_num_heads = 3
        head_ratio = 2
        _ = ConvBertConfig(conv_num_heads=conv_num_heads, head_ratio=head_ratio)

    with pytest.raises(ValueError):
        conv_num_heads = 8
        head_ratio = 4
        _ = ConvBertConfig(head_ratio=head_ratio)

    with pytest.raises(ValueError):
        conv_num_heads = 4
        head_ratio = 2
        conv_bert_task = "encode"
        _ = ConvBertConfig(conv_bert_task=conv_bert_task)

    with pytest.raises(ValueError):
        conv_bert_task = "fluorescence"
        pooling = None
        _ = ConvBertConfig(conv_bert_task=conv_bert_task, pooling=pooling)

    with pytest.raises(ValueError):
        conv_bert_task = "secondary_structure-3"
        pooling = "avg"
        _ = ConvBertConfig(conv_bert_task=conv_bert_task, pooling=pooling)


@pytest.mark.parametrize(
    ("conv_bert_task", "pooling"),
    [
        ["fluorescence", "avg"],
        ["fold", "max"],
        ["localization", "avg"],
        ["secondary_structure-3", None],
    ],
)
def test_conv_bert_head(conv_bert_task: str, pooling: Optional[str]) -> None:
    """
    Tests that the ConvBertLayer works and outputs the same results with permutation in
    the batch.
    """
    config = ConvBertConfig(
        embed_dim=8,
        conv_num_heads=2,
        kernel_size=3,
        head_ratio=2,
        conv_fc_embed_dim=16,
        conv_bert_task=conv_bert_task,
        pooling=pooling,
    )

    def conv_bert_head(
        encoder_output: jnp.ndarray, attention_mask: jnp.ndarray
    ) -> jnp.ndarray:
        layer = ConvBertHead(config=config)
        return layer(encoder_output=encoder_output, attention_mask=attention_mask)

    conv_bert_fn = hk.transform(conv_bert_head)

    random_key = jax.random.PRNGKey(0)
    encoder_output = jax.random.normal(
        random_key,
        shape=(16, 100, 8),
    )
    random_key, _ = jax.random.split(random_key)
    attention_mask = build_padding_attention_mask(
        tokens=jax.random.bernoulli(random_key, p=0.9, shape=(16, 100)).astype(
            jnp.int32
        ),
        pad_token_id=0,
    )
    parameters = conv_bert_fn.init(
        random_key, encoder_output=encoder_output, attention_mask=attention_mask
    )
    output = conv_bert_fn.apply(
        parameters,
        random_key,
        encoder_output=encoder_output,
        attention_mask=attention_mask,
    )["output"]

    random_key, _ = jax.random.split(random_key)
    random_idx = jax.random.permutation(random_key, jnp.arange(16))
    permuted_output = conv_bert_fn.apply(
        parameters,
        random_key,
        encoder_output=encoder_output[random_idx],
        attention_mask=attention_mask[random_idx],
    )["output"]
    pytest.assume(
        jnp.allclose(output[random_idx], permuted_output),
        "permutation changes the output",
    )
