import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest

from trix.models.gpt.caching.model_caching import build_gpt_fn_with_caching
from trix.models.gpt.model import GptConfig, build_gpt_fn


def _replace_tokens(
    tokens: jnp.ndarray, tokens_predicted: jnp.ndarray, idx_to_replace: jnp.ndarray
) -> jnp.ndarray:
    """Replace a token in each sequence of the batch at the indices indicated
    by idx_to_replace

    Returns:
        jnp.ndarray: Updated tokens
    """
    return jax.vmap(
        lambda token_seq, token_predicted, idx: token_seq.at[idx].set(token_predicted)
    )(tokens, tokens_predicted, idx_to_replace)


def test_gpt_caching_consistency() -> None:
    """Tests that GPT with cache produces the same results at inference than
    the standard GPT architecture where we recompute the keys/values pairs
    at each forward pass"""

    # get GPT config
    gpt_config = GptConfig(
        vocab_size=15,
        eos_token_id=2,
        embed_dim=8,
        ffn_embed_dim=16,
        num_heads=2,
        num_layers=1,
        rope_dimensions=None,
        max_position_embeddings=128,
        add_bias_ffn=False,
        norm_type="RMS_norm",
        parallel_attention_ff=False,
        ffn_activation_name="silu",
        use_glu_in_ffn=True,
        add_bias_lm_head=False,
    )

    # Hyperparameters
    seed = 0
    pad_token_id = 1
    batch_size = 2
    max_token_len = 10

    # Padded dummy prompts
    tokens_prompt = jnp.asarray(
        [
            [
                4,
                5,
                pad_token_id,
                pad_token_id,
                pad_token_id,
                pad_token_id,
                pad_token_id,
                pad_token_id,
                pad_token_id,
                pad_token_id,
            ],
            [4, 5, 14, 2, 3, 14, 10, pad_token_id, pad_token_id, pad_token_id],
        ]
    )  # (2,10)
    prompt_lengths = jnp.sum(tokens_prompt != pad_token_id, axis=1)

    # Standard GPT
    # build finetun apply_fn
    gpt_fn = build_gpt_fn(
        config=gpt_config,
        name="tiny_gpt",
    )
    gpt_fn = hk.transform(gpt_fn)

    # Initialize the GPT forward function
    tokens_ids = jnp.tile(jnp.asarray([4]), reps=(2, 1))  # (1,10)
    random_key = jax.random.PRNGKey(seed)
    params = gpt_fn.init(random_key, tokens_ids[:, :1])

    # Infer next two tokens
    tokens = tokens_prompt
    for time_step in range(2):
        # Where the next token is predicted in each element of the batch
        idx_looked_at = prompt_lengths - 1 + time_step  # (batch_size,)
        outs = gpt_fn.apply(params, random_key, tokens)
        logits = outs["logits"]  # (batch_size, seq_len)
        tokens_predicted = jnp.asarray(
            [
                tokens[idx]
                for tokens, idx in zip(jnp.argmax(logits, axis=-1), idx_looked_at)
            ]
        )  # (batch_size,)
        tokens = _replace_tokens(
            tokens, tokens_predicted, idx_looked_at + 1
        )  # (batch_size, seq_len)

    tokens_gpt = tokens
    logits_gpt = logits

    # GPT with caching
    # build finetun apply_fn
    gpt_fn_with_caching = build_gpt_fn_with_caching(
        config=gpt_config,
        cache_length=max_token_len,
        name="tiny_gpt",
    )
    gpt_fn_with_caching = hk.transform_with_state(gpt_fn_with_caching)
    gpt_fn_with_caching_apply = jax.jit(gpt_fn_with_caching.apply)

    # Initialize the GPT with cache forward function
    positions = jnp.tile(jnp.asarray([[0]]), (2, 1))  # (2,1)
    random_key = jax.random.PRNGKey(seed)
    _, cache_state = gpt_fn_with_caching.init(random_key, tokens_ids[:, :1], positions)

    # Infer next two tokens
    tokens = tokens_prompt
    for time_step in range(2):
        # Where the next token is predicted in each element of the batch
        idx_looked_at = prompt_lengths - 1 + time_step  # (batch_size,)
        if time_step == 0:
            # Empty the cache_state
            cache_state = jax.tree_map(lambda x: jnp.zeros_like(x), cache_state)

            # The positions is set at zero to ensure the keys/values pairs are stored
            # in the first positions in the cache
            positions = jnp.zeros((batch_size, 1), dtype=jnp.int32)
            tokens_ids = tokens  # (batch_size, seq_len)
        else:
            positions = jnp.expand_dims(idx_looked_at, axis=1)
            tokens_ids = jax.vmap(lambda array, idx: array[idx])(
                tokens, positions
            )  # (batch_size, num_input_tokens)

        outs, cache_state = gpt_fn_with_caching_apply(
            params, cache_state, random_key, tokens_ids, positions
        )
        logits = outs["logits"]  # (batch_size, seq_len)
        tokens_predicted = jnp.asarray(
            [
                tokens[idx] if time_step == 0 else tokens[0]
                for tokens, idx in zip(jnp.argmax(logits, axis=-1), idx_looked_at)
            ]
        )  # (batch_size,)

        tokens = _replace_tokens(
            tokens, tokens_predicted, idx_looked_at + 1
        )  # (batch_size, seq_len)

    tokens_gpt_cache = tokens
    logits_gpt_cache = logits

    pytest.assume(
        np.allclose(tokens_gpt, tokens_gpt_cache),
        "Tokens inferred by standard GPT not the same as tokens inferred by GPT with cache",  # noqa
    )

    for i, idx in enumerate(idx_looked_at):
        pytest.assume(
            np.allclose(logits_gpt[i, idx], logits_gpt_cache[i], rtol=5e-5),
            "LOgits inferred by standard GPT not the same as tokens inferred by GPT with cache",  # noqa
        )
