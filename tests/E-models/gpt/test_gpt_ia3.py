from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.gpt.finetuning import GptConfig, build_gpt_ia3_rescaling_fn
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize(
    (
        "rope_dimensions",
        "add_bias_ffn",
        "norm_type",
        "parallel_attention_ff",
        "use_glu_in_ffn",
        "ffn_activation_name",
        "add_bias_lm_head",
    ),
    [
        [
            4,
            True,
            "layer_norm",
            True,
            False,
            "gelu",
            True,
        ],  # GPTJ,
        [
            None,
            False,
            "RMS_norm",
            False,
            True,
            "silu",
            False,
        ],  # LLAMA
    ],
)
def test_gpt_ia3_rescaling(
    rope_dimensions: Optional[int],
    add_bias_ffn: bool,
    norm_type: str,
    parallel_attention_ff: bool,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    add_bias_lm_head: bool,
) -> None:
    """Tests that GPT-j IA3 does not produce NaNs"""
    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=64,
    )

    sequences = generate_fake_dataset(
        min_sequence_length=10,
        max_sequence_length=40,
        num_sequences=16,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    token_ids = jnp.array([toks[1] for toks in tokenizer.batch_tokenize(sequences)])

    config = GptConfig(
        vocab_size=tokenizer.vocabulary_size,
        eos_token_id=tokenizer.pad_token_id,
        embed_dim=4,
        ffn_embed_dim=8,
        num_heads=1,
        num_layers=1,
        max_position_embeddings=512,
        rope_dimensions=rope_dimensions,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=add_bias_lm_head,
        norm_type=norm_type,
        parallel_attention_ff=parallel_attention_ff,
        use_gradient_checkpointing=False,
    )

    gpt_ia3_fn = build_gpt_ia3_rescaling_fn(config=config)

    gpt_ia3_fn = hk.transform(gpt_ia3_fn)

    parameters = gpt_ia3_fn.init(
        rng=jax.random.PRNGKey(0),
        token_ids=token_ids[:1],
    )

    logits = gpt_ia3_fn.apply(
        params=parameters,
        rng=jax.random.PRNGKey(0),
        token_ids=token_ids,
    )["logits"]
    print(logits)
    pytest.assume(jnp.all(~jnp.isnan(logits)), "NaNs in output logits")
