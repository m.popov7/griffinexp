from typing import Optional

import haiku as hk
import jax
import numpy as np
import pytest
from jax.random import PRNGKey
from transformers import AutoTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.utils.masking import create_stacked_causal_mask_func


@pytest.mark.parametrize(
    (
        "rope_dimensions",
        "add_bias_ffn",
        "norm_type",
        "parallel_attention_ff",
        "use_glu_in_ffn",
        "ffn_activation_name",
        "add_bias_lm_head",
    ),
    [
        [
            4,
            True,
            "layer_norm",
            True,
            False,
            "gelu",
            True,
        ],  # GPTJ,
        [
            None,
            False,
            "RMS_norm",
            False,
            True,
            "silu",
            False,
        ],  # LLAMA
    ],
)
def test_gpt_model(
    rope_dimensions: Optional[int],
    add_bias_ffn: bool,
    norm_type: str,
    parallel_attention_ff: bool,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    add_bias_lm_head: bool,
) -> None:
    """
    Tests a tiny random initialized gpt model :
    Test the jitted call fn and check that batching yields coherent results
    """

    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=32,
        ffn_embed_dim=32 * 4,
        num_heads=4,
        num_layers=2,
        rope_dimensions=rope_dimensions,
        max_position_embeddings=512,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=add_bias_lm_head,
        norm_type=norm_type,
        parallel_attention_ff=parallel_attention_ff,
        use_gradient_checkpointing=False,
    )
    gpt_fn = build_gpt_fn(
        config,
    )
    gpt_fn = hk.transform(gpt_fn)

    init_prompt = "Hello, I"
    input_token_ids = tokenizer([init_prompt], return_tensors="np", padding=True)[
        "input_ids"
    ]

    params = gpt_fn.init(PRNGKey(1), input_token_ids)  # type: ignore

    prompts = ["My dog is", "I'd like to go"]
    input_token_ids = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]
    pytest.assume(
        (input_token_ids == tokenizer.pad_token_id).sum() > 0,
        "there should be some padding tokens in the batch",
    )

    # Test the jitted call fn and check that batching yields coherent results
    input_token_ids = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]
    logits_array = jax.jit(
        lambda input_token_ids: gpt_fn.apply(  # type: ignore
            params,
            jax.random.PRNGKey(42),
            token_ids=input_token_ids,
        )["logits"]
    )(input_token_ids)

    for prompt, logits_array_element in zip(prompts, logits_array):
        input_token_ids = tokenizer([prompt], return_tensors="np", padding=True)[
            "input_ids"
        ]
        logits = jax.jit(
            lambda input_token_ids: gpt_fn.apply(  # type: ignore
                params,
                PRNGKey(2),
                token_ids=input_token_ids,
            )["logits"]
        )(input_token_ids)[0]

        pytest.assume(
            np.allclose(
                logits_array_element[: logits.shape[0]].astype(np.float32),
                logits.astype(np.float32),
                atol=1e-5,
            ),
            "batching makes outputs different \n"
            "max absolute difference = {x} \n".format(
                x=np.abs(
                    logits_array_element[: logits.shape[0]].astype(np.float32)
                    - logits.astype(np.float32)
                ).max()
            )
            + "mean absolute difference = {x}".format(
                x=np.abs(
                    logits_array_element[: logits.shape[0]].astype(np.float32)
                    - logits.astype(np.float32)
                ).mean()
            ),
        )


@pytest.mark.parametrize(
    (
        "rope_dimensions",
        "add_bias_ffn",
        "norm_type",
        "parallel_attention_ff",
        "use_glu_in_ffn",
        "ffn_activation_name",
        "add_bias_lm_head",
        "max_num_stacked_examples",
    ),
    [
        [4, True, "layer_norm", True, False, "gelu", True, 3],
        [None, False, "RMS_norm", False, True, "silu", False, 5],
    ],
)
def test_gpt_stacked_seq(
    rope_dimensions: Optional[int],
    add_bias_ffn: bool,
    norm_type: str,
    parallel_attention_ff: bool,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    add_bias_lm_head: bool,
    max_num_stacked_examples: int,
) -> None:
    """
    Tests a tiny random initialized gpt model :
    Test the jitted call fn and check that batching yields coherent results
    """

    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=32,
        ffn_embed_dim=32 * 4,
        num_heads=4,
        num_layers=2,
        rope_dimensions=rope_dimensions,
        max_position_embeddings=512,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=add_bias_lm_head,
        norm_type=norm_type,
        parallel_attention_ff=parallel_attention_ff,
        use_gradient_checkpointing=False,
    )
    gpt_fn = build_gpt_fn(config)
    gpt_fn = hk.transform(gpt_fn)
    stacked_mask_fn = create_stacked_causal_mask_func(max_num_stacked_examples)

    init_prompt = "Hello, I <|endoftext|> How are you doing today"
    input_token_ids = tokenizer([init_prompt], return_tensors="np", padding=True)[
        "input_ids"
    ]
    attention_mask = stacked_mask_fn(input_token_ids, tokenizer.eos_token_id)
    params = gpt_fn.init(PRNGKey(1), input_token_ids, attention_mask)  # type: ignore

    prompts = [
        "Hello, I <|endoftext|> How are <|endoftext|> you doing today hippopotamus too",
        "I'd <|endoftext|> like to go <|endoftext|> with you <|endoftext|> now",
    ]
    input_token_ids = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]
    pytest.assume(
        (input_token_ids == tokenizer.pad_token_id).sum() > 5,
        "there should be some padding tokens in the batch",
    )

    # Test the jitted call fn and check that batching yields coherent results
    attention_mask = stacked_mask_fn(input_token_ids, tokenizer.eos_token_id)
    logits_array = jax.jit(
        lambda input_token_ids: gpt_fn.apply(  # type: ignore
            params,
            jax.random.PRNGKey(42),
            token_ids=input_token_ids,
            attention_mask=attention_mask,
        )["logits"]
    )(input_token_ids)

    individual_prompts = [
        "Hello, I <|endoftext|>",
        " How are <|endoftext|>",
        " you doing today hippopotamus too",
        "I'd <|endoftext|>",
        " like to go <|endoftext|>",
        " with you <|endoftext|>",
        " now",
    ]

    individual_token_ids = tokenizer(individual_prompts)["input_ids"]
    ids1_from_ind = np.array(
        individual_token_ids[0] + individual_token_ids[1] + individual_token_ids[2]
    )
    ids2_from_ind = np.array(
        individual_token_ids[3]
        + individual_token_ids[4]
        + individual_token_ids[5]
        + individual_token_ids[6]
    )
    pytest.assume(
        np.sum(input_token_ids[0][: len(ids1_from_ind)] - ids1_from_ind) == 0,
        "Individual token ids don't match first prompt",
    )
    pytest.assume(
        np.sum(input_token_ids[1][: len(ids2_from_ind)] - ids2_from_ind) == 0,
        "Individual token ids don't match second prompt",
    )
    cum_lengths = [
        np.cumsum(np.array([0] + [len(x) for x in individual_token_ids[:3]])),
        np.cumsum(np.array([0] + [len(x) for x in individual_token_ids[3:]])),
    ]
    individual_logits = [
        logits_array[0, cum_lengths[0][i] : cum_lengths[0][i + 1]]
        for i in range(len(cum_lengths[0]) - 1)
    ] + [
        logits_array[1, cum_lengths[1][i] : cum_lengths[1][i + 1]]
        for i in range(len(cum_lengths[1]) - 1)
    ]

    for prompt, logits_array_element in zip(individual_prompts, individual_logits):
        input_token_ids = tokenizer([prompt], return_tensors="np", padding=True)[
            "input_ids"
        ]
        logits = jax.jit(
            lambda input_token_ids: gpt_fn.apply(  # type: ignore
                params,
                PRNGKey(2),
                token_ids=input_token_ids,
            )["logits"]
        )(input_token_ids)[0]

        pytest.assume(
            np.allclose(
                logits_array_element[: logits.shape[0]].astype(np.float32),
                logits.astype(np.float32),
                atol=1e-5,
            ),
            "batching makes outputs different \n"
            f"shape comparison: {logits.shape}, {logits_array_element.shape} \n"
            "max absolute difference = {x} \n".format(
                x=np.abs(
                    logits_array_element[: logits.shape[0]].astype(np.float32)
                    - logits.astype(np.float32)
                ).max()
            )
            + "mean absolute difference = {x} \n".format(
                x=np.abs(
                    logits_array_element[: logits.shape[0]].astype(np.float32)
                    - logits.astype(np.float32)
                ).mean()
            )
            + "num nonzero = {x} \n".format(
                x=np.sum(
                    np.abs(
                        logits_array_element[: logits.shape[0]].astype(np.float32)
                        - logits.astype(np.float32)
                    )
                    > 1e-5
                )
            ),
        )
