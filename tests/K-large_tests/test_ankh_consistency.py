import os

import haiku as hk
import jax
import jax.numpy as jnp
import pytest
import transformers
from Bio import SeqIO
from transformers import T5EncoderModel

from trix.pretrained.ankh import get_pretrained_ankh_model

transformers.logging.set_verbosity_error()


@pytest.mark.skip("Test will fail and should be modified.")
def test_large_consistency() -> None:
    # Load Data
    test_path = os.path.dirname(os.path.realpath(__file__))

    with open(f"{test_path}/test_files/proteins.fasta") as handle:
        sequences = [str(record.seq) for record in SeqIO.parse(handle, "fasta")]

    parameters, enc_fn, tokenizer, _ = get_pretrained_ankh_model(
        base_or_large="base", encoder_only=True, embeddings_layers_to_save=(3,)
    )
    enc_fn = hk.transform(enc_fn)
    tokenized_seqs = tokenizer(
        sequences,
        padding="longest",
        truncation=True,
        return_tensors="pt",
    )
    tokens = tokenized_seqs["input_ids"]
    attention_mask = tokenized_seqs["attention_mask"]

    outs = enc_fn.apply(
        parameters,
        jax.random.PRNGKey(42),
        enc_token_ids=tokens.cpu().numpy(),
        drop_rate=0,
    )
    trix_embeds = outs["embeddings"]

    # Instantiate Huggingface Model
    ankh_model = T5EncoderModel.from_pretrained(
        "ElnaggarLab/ankh-base",
        output_hidden_states=True,  # return intermediate embddings
    )
    hf_outs = ankh_model(input_ids=tokens, attention_mask=attention_mask)

    hf_embeds = hf_outs["last_hidden_state"].detach().numpy()
    mask = jnp.expand_dims(tokens.cpu().numpy() != 0, axis=-1)
    pytest.assume(
        jnp.allclose(trix_embeds * mask, hf_embeds * mask, atol=1e-5),
        "trix is inconsistent with huggingface",
    )

    trix_intermediate_embed = outs["embeddings_3"]
    hf_intermediate_embed = hf_outs["hidden_states"][3].detach().numpy()
    pytest.assume(
        jnp.allclose(
            trix_intermediate_embed * mask,
            hf_intermediate_embed * mask,
            atol=1e-3,  # atol is higher as the embeddings are not normalized
        ),
        "trix is inconsistent with huggingface at the intermediate embedding",
    )
