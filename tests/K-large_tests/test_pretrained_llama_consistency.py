import haiku as hk
import jax.numpy as jnp
import pytest
from jax.random import PRNGKey
from transformers import GenerationConfig

from trix.pretrained.llama import (
    download_from_kao,
    get_model_type_and_size,
    get_pretrained_llama_model,
    get_pretrained_llama_tokenizer,
)
from trix.utils.decoding import decode_greedy


@pytest.mark.parametrize(
    "model_name",
    [
        "llama_7B",
        "llama_13B",
        "vicuna_7B",
        "vicuna_13B",
        "llama_2_7B",
        "llama_2_13B",
        "llama_2_chat_7B",
        "llama_2_chat_13B",
    ],
)
def test_pretrained_llama_model(model_name: str) -> None:
    """
    Tests a pretrained LLama model :
    - Compare generated sequences against hugging face implementation (torch)
    """
    model_type, model_size = get_model_type_and_size(model_name)

    if model_name == "llama_2_7B":
        with pytest.raises(NotImplementedError):
            _, _ = download_from_kao(
                model_size=model_size,
                model_type=model_type,
                download_in_float32=True,
            )
        return
    original_model, original_tokenizer = download_from_kao(
        model_size=model_size,
        model_type=model_type,
        download_in_float32=True,
    )
    assert original_model is not None, "The returned model can't be None"

    def prepare_inputs_for_generation(self, input_ids, **kwargs):  # type: ignore
        return {"input_ids": input_ids}

    original_model.prepare_inputs_for_generation = (
        prepare_inputs_for_generation.__get__(original_model)
    )

    params, llama_fn, tokenizer, _ = get_pretrained_llama_model(model_name=model_name)
    llama_fn = hk.transform(llama_fn)

    downloaded_tokenizer = get_pretrained_llama_tokenizer(model_name=model_name)

    prompts = ["DNA is", "RNA is a"]
    # step 1 : compare tokenizers
    input_token_ids = tokenizer(prompts, return_tensors="np", add_special_tokens=False)[
        "input_ids"
    ]
    test_token_ids = downloaded_tokenizer(
        prompts, return_tensors="np", add_special_tokens=False
    )["input_ids"]
    pytest.assume(
        jnp.allclose(input_token_ids[0], test_token_ids[0]),
        "tokenizers differ between get_pretrained_model and get_pretrained_tokenizer.",
    )

    # step 2 : Compare generated sequences against hugging face implementation ( torch )
    greedy_tokens = [
        decode_greedy(
            init_tokens_ids=tokenizer(
                [prompt],
                return_tensors="np",
                add_special_tokens=False,
            )["input_ids"],
            random_key=PRNGKey(0),
            params=params,
            apply_fn=llama_fn.apply,
            num_tokens_to_decode=10,
            eos_token_id=tokenizer.eos_token_id,
        )[0]
        for prompt in prompts
    ]

    answers = [tokenizer.batch_decode(tokens)[0] for tokens in greedy_tokens]

    # In older versions of HuggingFace, generate() had arguments such as `eos_token_id`
    # that would default to values found in `model.config`. Later versions of
    # HuggingFace instead take in a GenerationConfig that we define below.
    generation_config = GenerationConfig(
        max_new_tokens=10,
        do_sample=False,
        eos_token_id=original_model.config.eos_token_id,
    )

    for prompt, answer in zip(prompts, answers):
        input_ids = original_tokenizer(
            prompt, return_tensors="pt", add_special_tokens=False
        ).input_ids
        gen_tokens = original_model.generate(input_ids, generation_config)
        answer_hf = original_tokenizer.batch_decode(gen_tokens)[0]
        pytest.assume(
            answer == answer_hf[: len(answer)],
            "decoded sequences don't match the HuggingFace ones",
        )
