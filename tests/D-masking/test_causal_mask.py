import jax.numpy as jnp
import pytest

from trix.utils.masking import build_causal_attention_mask


def test_build_causal_mask() -> None:
    """
    Tests if the correct causal attention mask is built.
    """
    batch_size = 2
    seq_len = 4

    causal_mask = jnp.array(
        [
            [
                [1, 0, 0, 0],
                [1, 1, 0, 0],
                [1, 1, 1, 0],
                [1, 1, 1, 1],
            ],
            [
                [1, 0, 0, 0],
                [1, 1, 0, 0],
                [1, 1, 1, 0],
                [1, 1, 1, 1],
            ],
        ]
    )

    test_mask = build_causal_attention_mask(batch_size, seq_len)

    pytest.assume(jnp.allclose(test_mask, causal_mask))
