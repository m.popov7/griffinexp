import jax
import jax.numpy as jnp
import pytest

from trix.utils.masking import build_perceiver_padding_attention_mask


@pytest.mark.parametrize("resampled_length", [8])
def test_perceiver_mask(resampled_length: int) -> None:
    """
    Builds a perceiver mask and checks that the logits computations does not change
    if pad embeddings change.
    """
    batch_size = 4
    seq_len = 30
    embed_dim = 16
    random_key = jax.random.PRNGKey(0)
    # create tokens
    tokens = jax.random.randint(
        key=random_key, shape=(batch_size, seq_len), minval=1, maxval=10
    )
    random_key, _ = jax.random.split(random_key)
    pad_idx = jax.random.choice(
        key=random_key, a=jnp.array([0, 1]), shape=(tokens.shape)
    )
    # mask random tokens
    tokens = tokens * pad_idx
    # create mask
    attention_mask = build_perceiver_padding_attention_mask(
        tokens=tokens, resampled_length=resampled_length, pad_token_id=0
    )

    # create random key and query heads

    random_key, _ = jax.random.split(random_key)
    key_heads = jax.random.normal(
        key=random_key, shape=(batch_size, seq_len + resampled_length, 1, embed_dim)
    )

    random_key, _ = jax.random.split(random_key)
    query_heads = jax.random.normal(
        key=random_key, shape=(batch_size, resampled_length, 1, embed_dim)
    )
    # compute and mask logits
    attention_logits = jnp.einsum("...thd,...Thd->...htT", query_heads, key_heads)
    attention_logits = attention_mask * attention_logits

    # create new heads to compare with previous ones
    head_pad_idx = jnp.concatenate(
        (pad_idx, jnp.ones((batch_size, resampled_length))), axis=1
    ).astype(jnp.int32)
    head_pad_idx = jnp.expand_dims(head_pad_idx, axis=(2, 3))
    random_key, _ = jax.random.split(random_key)
    new_key_heads = jax.random.normal(
        key=random_key, shape=(batch_size, seq_len + resampled_length, 1, embed_dim)
    )

    new_heads = key_heads * head_pad_idx + new_key_heads * (1 - head_pad_idx)
    # compute new logits
    new_logits = jnp.einsum("...thd,...Thd->...htT", query_heads, new_heads)
    new_logits = attention_mask * new_logits

    # test
    pytest.assume(
        jnp.allclose(attention_logits, new_logits),
        "logits differ when padded values differ",
    )
