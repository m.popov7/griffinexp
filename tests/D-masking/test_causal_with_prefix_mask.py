import jax.numpy as jnp
import pytest

from trix.utils.masking import build_prefix_causal_attention_mask


@pytest.mark.parametrize("prefix", [jnp.array([2, 3])])
def test_build_causal_with_prefix_mask(prefix: jnp.ndarray) -> None:
    """
    Tests if the correct causal attention mask with prefix is built.
    """
    batch_size = 2
    seq_len = 4

    prefix_mask = jnp.array(
        [
            [
                [1, 1, 0, 0],
                [1, 1, 0, 0],
                [1, 1, 1, 0],
                [1, 1, 1, 1],
            ],
            [
                [1, 1, 1, 0],
                [1, 1, 1, 0],
                [1, 1, 1, 0],
                [1, 1, 1, 1],
            ],
        ]
    )
    prefix_mask = prefix_mask[:, None, :, :]

    test_mask = build_prefix_causal_attention_mask(batch_size, seq_len, prefix)
    pytest.assume(jnp.allclose(test_mask, prefix_mask))
