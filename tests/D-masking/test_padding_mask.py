import jax.numpy as jnp
import pytest

from trix.utils.masking import build_padding_attention_mask


@pytest.mark.parametrize("pad_token_id", [0])
@pytest.mark.parametrize(
    "tokens", [jnp.array([[1, 2, 1, 3, 0], [2, 1, 0, 0, 0], [3, 0, 2, 1, 0]])]
)
def test_build_padding_mask(pad_token_id: int, tokens: jnp.ndarray) -> None:
    """
    Tests that an attention mask, masking out padding tokens,is built correctly.
    """
    padding_mask = jnp.array(
        [
            [
                [1, 1, 1, 1, 0],
                [1, 1, 1, 1, 0],
                [1, 1, 1, 1, 0],
                [1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0],
            ],
            [
                [1, 1, 0, 0, 0],
                [1, 1, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0],
            ],
            [
                [1, 0, 1, 1, 0],
                [0, 0, 0, 0, 0],
                [1, 0, 1, 1, 0],
                [1, 0, 1, 1, 0],
                [0, 0, 0, 0, 0],
            ],
        ]
    )

    padding_mask = padding_mask[:, None, :, :]
    test_mask = build_padding_attention_mask(tokens=tokens, pad_token_id=pad_token_id)
    pytest.assume(jnp.allclose(test_mask, padding_mask))
