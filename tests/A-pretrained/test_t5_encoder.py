import haiku as hk
import jax
import jax.numpy as jnp
import pytest
import transformers

from trix.pretrained.t5 import get_pretrained_t5_model

transformers.logging.set_verbosity_error()


def test_t5_encoder() -> None:
    """
    Tests that the T5 Encoder is deterministic, returns the same output for different
    batch sizes and padding length.
    """
    # Get Trix Model
    parameters, encoder_fn, tokenizer, _ = get_pretrained_t5_model(
        model_name="t5-small", encoder_only=True, embeddings_layers_to_save=(3,)
    )
    encoder_fn = hk.transform(encoder_fn)
    sequences = [
        "translate English to German: The house is wonderful.",
        "Studies have been shown that owning a dog is good for you",
        "cola sentence: John made Bill master of himself.",
        "marouane fellaini and adnan januzaj continue to show the world "
        "they are not just teammates but also best mates. the manchester",
    ]
    tokens = tokenizer(
        sequences,
        padding="longest",
        truncation=True,
        return_tensors="np",
    )["input_ids"]

    encoder_output = encoder_fn.apply(
        parameters,
        jax.random.PRNGKey(42),
        encoder_token_ids=tokens,
        dropout_rate=0,
    )["embeddings"]

    encoder_output_1 = encoder_fn.apply(
        parameters,
        jax.random.PRNGKey(42),
        encoder_token_ids=tokens[:1],
        dropout_rate=0,
    )["embeddings"]

    mask = jnp.expand_dims(tokens == 0, axis=-1)
    pytest.assume(
        jnp.allclose(encoder_output[:1] * mask, encoder_output_1 * mask, atol=1e-6),
        "batch size makes outputs different",
    )

    small_pad_token = tokenizer(
        sequences[:1],
        padding="longest",
        truncation=True,
        return_tensors="np",
    )["input_ids"]

    encoder_output_2 = encoder_fn.apply(
        parameters,
        jax.random.PRNGKey(42),
        encoder_token_ids=small_pad_token,
        dropout_rate=0,
    )["embeddings"]

    token_length = small_pad_token.shape[1]
    pytest.assume(
        jnp.allclose(
            encoder_output_2[:, :token_length],
            encoder_output[:1, :token_length],
            atol=1e-6,
        ),
        "batch size makes outputs different",
    )
