import json
import os
from typing import Callable, List, Tuple

import haiku as hk
import numpy as np
import pytest
import torch
from jax.random import PRNGKey
from transformers import AutoTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.pretrained.llama import LLAMA_MODEL_NAME, translate_torch_params_from_original
from trix.utils.decoding import decode_greedy
from trix.utils.s3 import download_from_s3_bucket, init_s3_trix_bucket

vocab_size = 32


def get_pretrained_tiny_llama() -> Tuple[hk.Params, Callable, AutoTokenizer, GptConfig]:
    """
    Create a Haiku tiny llama model either by downloading the model directly or by
    downloading transforming a pretrained PyTorch model.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config
    """
    test_path = os.path.dirname(os.path.realpath(__file__))

    s3_client, bucket = init_s3_trix_bucket()

    model_path = f"{test_path}/test_files/tiny_llama_model.pt"
    config_path = f"{test_path}/test_files/tiny_llama_config.json"

    download_from_s3_bucket(
        s3_client=s3_client,
        bucket=bucket,
        key="tests/tiny_llama_model/tiny_llama_config.json",
        filename=config_path,
    )
    download_from_s3_bucket(
        s3_client=s3_client,
        bucket=bucket,
        key="tests/tiny_llama_model/tiny_llama_model.pt",
        filename=model_path,
    )
    os.system(f"rm f{model_path} f{config_path}")

    model_params = torch.load(model_path)

    # TODO: add the hugging-face tokenizer when released. For now we use
    #  tokenizer_batch_encode to mock the behavior of the sentencepiece tokenizer
    #  from LLAMA
    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = 1000
    tokenizer.pad_token = "<pad>"

    with open(config_path) as f:
        config_json = json.loads(f.read())

    ffn_embed_dim = config_json["multiple_of"] * (
        (config_json["dim"] * 4 * 2 / 3 + config_json["multiple_of"] - 1)
        // config_json["multiple_of"]
    )
    config = GptConfig(
        vocab_size=vocab_size,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=config_json["dim"],
        ffn_embed_dim=ffn_embed_dim,
        num_heads=config_json["n_heads"],
        num_layers=config_json["n_layers"],
        rope_dimensions=None,
        max_position_embeddings=512,
        add_bias_ffn=False,
        norm_type="RMS_norm",
        parallel_attention_ff=False,
        ffn_activation_name="silu",
        use_glu_in_ffn=True,
        add_bias_lm_head=False,
        use_gradient_checkpointing=False,
    )

    parameters = translate_torch_params_from_original(model_params, config.num_layers)

    llama_fn = build_gpt_fn(config, name=LLAMA_MODEL_NAME)

    return parameters, llama_fn, tokenizer, config


def test_pretrained_tiny_llama_model() -> None:
    """
    Tests a pretrained llama model :
    - step 1 : Model initialisation sanity : init the model, and test if the shapes of
        the tensors match the ones from the parameters of the pre-trained model
    - step 2 : simply run an inference
    """

    params, llama_fn, tokenizer, _ = get_pretrained_tiny_llama()
    pad_token_id = -1

    def tokenizer_batch_encode(prompts: List[str]) -> np.ndarray:
        """
        Tokenizes a batch of prompts using the `tokenizer` function,
        maps the resulting input IDs to a vocabulary of size vocab_size,
        and replaces padding tokens with -1.

        Args:
            prompts (List[str]): A list of prompts to be tokenized.

        Returns:
            np.ndarray: An array of tokenized input IDs,
            with shape (batch_size, sequence_length).
        """
        input_ids = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]
        input_ids = input_ids % vocab_size  # we need to map to a vocab of size 32
        input_ids[input_ids == tokenizer.pad_token_id] = pad_token_id
        return input_ids

    llama_fn = hk.transform(llama_fn)

    # step 1 : Model initialisation sanity : init the model, and test if the shapes of
    # the tensors match the ones from the parameters of the pre-trained model
    init_prompt = "Hello, I"
    input_token_ids = tokenizer_batch_encode([init_prompt])

    init_params = llama_fn.init(  # type: ignore
        PRNGKey(1),
        input_token_ids,
    )

    for layer_name in init_params:
        pytest.assume(
            layer_name in params,
            f"the layer name {layer_name} should be in the loaded ones",
        )
        if type(init_params[layer_name]) == dict:
            for weight_name in init_params[layer_name]:
                pytest.assume(
                    params[layer_name][weight_name].shape
                    == init_params[layer_name][weight_name].shape,
                    "parameters at init are inconsistent with the loaded ones : \n"
                    f"{layer_name} : {weight_name}\n"
                    f"{params[layer_name][weight_name].shape=},\n"
                    f"{init_params[layer_name][weight_name].shape=}",
                )

    prompts = ["My dog is", "I'd like to go"]
    greedy_tokens = [
        decode_greedy(
            init_tokens_ids=tokenizer_batch_encode([prompt]),
            random_key=PRNGKey(0),
            params=params,
            apply_fn=llama_fn.apply,  # type: ignore
            num_tokens_to_decode=10,
            eos_token_id=tokenizer.eos_token_id,
        )[0]
        for prompt in prompts
    ]

    sampled_answers = [tokenizer.batch_decode(tokens)[0] for tokens in greedy_tokens]

    expected_answer = "My dog is-9'0-<|endoftext|>%834!1834"
    produced_answer = prompts[0] + sampled_answers[0]
    pytest.assume(
        produced_answer == expected_answer[: len(produced_answer)],
        "decoded sequences don't match the expected ones"
        f"\n{produced_answer}\n{expected_answer[: len(produced_answer)]}",
    )

    expected_answer = "I'd like to go)'$:/#4<'30*/$,0*"
    produced_answer = prompts[1] + sampled_answers[1]

    pytest.assume(
        produced_answer == expected_answer[: len(produced_answer)],
        f"decoded sequences don't match the expected ones"
        f"\n{produced_answer}\n{expected_answer[: len(produced_answer)]}",
    )
