import haiku as hk
import jax
import numpy as np
import pytest
import torch
from jax.random import PRNGKey
from transformers import AutoModel, AutoTokenizer

from trix.pretrained.dnabert import (
    get_fixed_size_dnabert_tokenizer,
    get_pretrained_dnabert,
)


@pytest.mark.parametrize("model_name", ["dnabert_1", "dnabert_2"])
def test_pretrained_dnabert_model(model_name: str) -> None:
    """
    Tests the pretrained dnabert models ( 1 and 2 ) :
    - step 1 : Model initialisation sanity : init the model, and test if the shapes of
        the tensors match the ones from the parameters of the pre-trained model
    - step 2 : Compare generated sequences against hugging face implementation ( torch )
    - step 3 : Compare the logits against hugging face implementation ( torch )
    """

    params, dnabert_fn, tokenizer, config = get_pretrained_dnabert(model_name)
    dnabert_fn = hk.transform(dnabert_fn)

    # step 0: test tokenizer
    with pytest.raises(AssertionError):
        tokenizer.tokenize("AANNAT")

    sequence_list = ["AATCCGATCGTAC", "TTCATCTACGATTG", "ATCATT"]
    tokenized_seq = tokenizer.batch_tokenize(sequence_list)

    if model_name == "dnabert_1":
        retrieved_seq = [
            "".join(
                tokenizer.convert_ids_to_tokens(
                    [
                        token
                        for token in token_ids
                        if token != tokenizer.pad_token_id
                        and token != tokenizer.cls_token_id
                        and token != tokenizer.sep_token_id
                    ][::6]
                )
            )
            for (_, token_ids) in tokenized_seq
        ]
        pytest.assume(
            [seq[: len(seq) // 6 * 6] for seq in sequence_list] == retrieved_seq,
            f"retrievec {sequence_list} \n expected {retrieved_seq}",
        )
    elif model_name == "dnabert_2":
        retrieved_seq = [
            "".join(
                tokenizer.convert_ids_to_tokens(
                    [
                        token
                        for token in token_ids
                        if token != tokenizer.pad_token_id
                        and token != tokenizer.cls_token_id
                        and token != tokenizer.sep_token_id
                    ]
                )
            )
            for (_, token_ids) in tokenized_seq
        ]
        pytest.assume(
            sequence_list == retrieved_seq,
            f"retrievec {sequence_list} \n expected {retrieved_seq}",
        )

    # step 0.5: test fixed length tokenizer
    fixed_len = 20
    fixed_size_tokenizer = get_fixed_size_dnabert_tokenizer(
        fixed_padding_len=fixed_len, model_name=model_name
    )
    tokenized_seq = fixed_size_tokenizer.batch_tokenize(sequence_list)
    print(tokenized_seq)
    for (_, token_ids) in tokenized_seq:
        pytest.assume(token_ids.shape[0] == fixed_len, f"wrong len {token_ids.shape}")

    # step 1 : Model initialisation sanity : init the model, and test if the shapes of
    # the tensors match the ones from the parameters of the pre-trained model

    init_prompt = "AAATTAAAAGTA"
    input_token_ids = np.array([tokenizer.tokenize(init_prompt)[1]])
    init_params = dnabert_fn.init(PRNGKey(1), input_token_ids)  # type: ignore

    for layer_name in init_params:
        pytest.assume(
            layer_name in params,
            f"the layer name {layer_name} should be in the loaded ones",
        )
        for weight_name in init_params[layer_name]:
            pytest.assume(
                params[layer_name][weight_name].shape
                == init_params[layer_name][weight_name].shape,
                "parameters at init are inconsistent with the loaded ones\n"
                f"{layer_name}:\n"
                f"{params[layer_name][weight_name].shape} vs "
                f"{init_params[layer_name][weight_name].shape}",
            )

    for layer_name in params:
        pytest.assume(
            layer_name in init_params,
            f"the layer name {layer_name} should be in the loaded ones",
        )
        for weight_name in params[layer_name]:
            pytest.assume(
                init_params[layer_name][weight_name].shape
                == params[layer_name][weight_name].shape,
                "parameters are inconsistent with the initialized ones\n"
                f"{layer_name}:\n"
                f"{init_params[layer_name][weight_name].shape} vs "
                f"{params[layer_name][weight_name].shape}",
            )

    # # step 3 : Compare the logits against hugging face implementation ( torch )

    nucl = ["AAATTAAAAGTA", "AAACTAAAAGTTAAACTAAAAGTT"]

    if model_name == "dnabert_1":
        huggingfacehub_model_name = "DNA_bert_6"
        revision = "55e0c0eb7b734c8b9b77bc083bf89eb6fbda1341"

    elif model_name == "dnabert_2":
        huggingfacehub_model_name = "DNABERT-2-117M"
        revision = "25abaf0bd247444fcfa837109f12088114898d98"

    huggingface_model = AutoModel.from_pretrained(
        f"zhihan1996/{huggingfacehub_model_name}",
        trust_remote_code=True,
        revision=revision,
    )
    huggingface_tokenizer = AutoTokenizer.from_pretrained(
        f"zhihan1996/{huggingfacehub_model_name}",
        revision=revision,
    )

    inputs = huggingface_tokenizer(
        nucl
        if model_name == "dnabert_2"
        else [
            " ".join([seq[i : i + 6] for i in range(len(seq) - 6 + 1)]) for seq in nucl
        ],
        return_tensors="pt",
        padding="longest",
        truncation=True,
    )
    huggingface_input_token_ids = np.array(inputs["input_ids"])

    input_token_ids = np.array([elt[1] for elt in tokenizer.batch_tokenize(nucl)])

    pytest.assume(
        np.allclose(np.array(huggingface_input_token_ids), input_token_ids),
        f"tokenizer doesn't match the huggingface one\n"
        f"{huggingface_input_token_ids=}\n"
        f"{input_token_ids=}",
    )

    with torch.no_grad():
        outputs = huggingface_model(**inputs)

    outs = dnabert_fn.apply(  # type: ignore
        params, jax.random.PRNGKey(42), tokens=input_token_ids
    )
    embeddings = outs[f"embeddings_{config.num_layers}"]

    embeddings_hf = outputs[0].detach().numpy()

    pytest.assume(
        np.allclose(
            embeddings_hf * np.array(inputs["attention_mask"])[:, :, np.newaxis],
            embeddings * np.array(inputs["attention_mask"])[:, :, np.newaxis],
            atol=1e-4,
        ),
        "computed embeddings don't match the HuggingFace ones",
    )
