import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest
from enformer_pytorch import Enformer, str_to_one_hot

from trix.pretrained.enformer import get_pretrained_enformer_model


@pytest.mark.parametrize("seed", [0, 123, 42])
def test_consistency(seed: int) -> None:
    """
    Check that Trix and HuggingFace implementations of the Enformer yield the same
    results.
    """
    # Load Enformer from HuggingFace
    torch_enformer = Enformer.from_pretrained("EleutherAI/enformer-official-rough")

    # Get Trix model
    params, state, enformer_fn, tokenizer, config = get_pretrained_enformer_model()
    enformer_fn = hk.transform_with_state(enformer_fn)
    apply_fn = enformer_fn.apply

    # Get a sequence
    sequence_length = 196_608
    rng = np.random.default_rng(seed=seed)
    seq = "".join(rng.choice(list("ATCGN"), size=(sequence_length,)))

    # Convert to torch for pass in Enformer
    torch_one_hot = str_to_one_hot([seq])
    torch_outs, torch_embeddings = torch_enformer(torch_one_hot, return_embeddings=True)

    # Tokenize it for trix model
    tokens_ids = [b[1] for b in tokenizer.batch_tokenize([seq])]
    tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)

    # Pass in Trix model
    random_key = jax.random.PRNGKey(0)

    outs, state = apply_fn(params, state, random_key, tokens, is_training=False)

    # Compare outputs
    torch_embeddings = jnp.asarray(torch_embeddings.detach().numpy())
    torch_human_outs = jnp.asarray(torch_outs["human"].detach().numpy())
    torch_mouse_outs = jnp.asarray(torch_outs["mouse"].detach().numpy())

    trix_embeddings = outs["embedding"]
    trix_human_outs = outs["human_head"]
    trix_mouse_outs = outs["mouse_head"]

    tolerance = 1e-3
    pytest.assume(
        jnp.max(jnp.abs(torch_embeddings - trix_embeddings)) < tolerance,
        "max error between embeddings is superior to tolerance",
    )

    pytest.assume(
        jnp.max(jnp.abs(torch_human_outs - trix_human_outs)) < tolerance,
        "max error between human predictions is superior to tolerance",
    )

    pytest.assume(
        jnp.max(jnp.abs(torch_mouse_outs - trix_mouse_outs)) < tolerance,
        "max error between mouse predictions is superior to tolerance",
    )
