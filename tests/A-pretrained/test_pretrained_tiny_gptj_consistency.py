from typing import Callable, Tuple

import haiku as hk
import jax
import numpy as np
import pytest
from jax.random import PRNGKey
from transformers import AutoModelForCausalLM, AutoTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.pretrained.gptj import GPTJ_MODEL_NAME, translate_torch_params
from trix.utils.decoding import decode_greedy


def get_pretrained_tiny_gptj() -> Tuple[hk.Params, Callable, AutoTokenizer, GptConfig]:
    """
    Create a Haiku tiny gptj model either by downloading the model directly or by
    downloading transforming a pretrained PyTorch model.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config
    """

    model_params = AutoModelForCausalLM.from_pretrained(
        "hf-internal-testing/tiny-random-gptj"
    ).state_dict()
    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=32,
        ffn_embed_dim=32 * 4,
        num_heads=4,
        num_layers=5,
        rope_dimensions=4,
        max_position_embeddings=512,
        add_bias_ffn=True,
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=True,
        norm_type="layer_norm",
        parallel_attention_ff=True,
        use_gradient_checkpointing=False,
    )

    parameters = translate_torch_params(model_params, config.num_layers)

    gptj_fn = build_gpt_fn(config, name=GPTJ_MODEL_NAME)

    return parameters, gptj_fn, tokenizer, config


def test_pretrained_tiny_gptj_model() -> None:
    """
    Tests a pretrained gptj model :
    - step 1 : Model initialisation sanity : init the model, and test if the shapes of
        the tensors match the ones from the parameters of the pre-trained model
    - step 2 : Compare generated sequences against hugging face implementation ( torch )
    - step 3 : Compare the logits against hugging face implementation ( torch )
    """

    params, gptj_fn, tokenizer, config = get_pretrained_tiny_gptj()

    gptj_fn = hk.transform(gptj_fn)

    # step 1 : Model initialisation sanity : init the model, and test if the shapes of
    # the tensors match the ones from the parameters of the pre-trained model
    init_prompt = "Hello, I"
    input_token_ids = tokenizer([init_prompt], return_tensors="np", padding=True)[
        "input_ids"
    ]

    init_params = gptj_fn.init(PRNGKey(1), input_token_ids)  # type: ignore
    for layer_name in init_params:
        pytest.assume(
            layer_name in params,
            f"the layer name {layer_name} should be in the loaded ones",
        )
        if type(init_params[layer_name]) == dict:
            for weight_name in init_params[layer_name]:
                pytest.assume(
                    params[layer_name][weight_name].shape
                    == init_params[layer_name][weight_name].shape,
                    "parameters at init are inconsistent with the loaded ones",
                )

    # step 2 : Compare generated sequences against hugging face implementation ( torch )
    prompts = ["My dog is", "I'd like to go"]
    greedy_tokens = [
        decode_greedy(
            init_tokens_ids=tokenizer([prompt], return_tensors="np")["input_ids"],
            random_key=PRNGKey(0),
            params=params,
            apply_fn=gptj_fn.apply,
            num_tokens_to_decode=10,
            eos_token_id=tokenizer.eos_token_id,
        )[0]
        for prompt in prompts
    ]

    answers = [tokenizer.batch_decode(tokens)[0] for tokens in greedy_tokens]

    huggingface_model = AutoModelForCausalLM.from_pretrained(
        "hf-internal-testing/tiny-random-gptj"
    )

    for prompt, answer in zip(prompts, answers):
        input_ids = tokenizer(prompt, return_tensors="pt").input_ids
        gen_tokens = huggingface_model.generate(
            input_ids,
            do_sample=False,
            max_length=30,
        )
        answer_hf = tokenizer.batch_decode(gen_tokens)[0]
        pytest.assume(
            answer == answer_hf[: len(answer)],
            "decoded sequences don't match the HuggingFace ones",
        )

    # step 3 : Compare the logits against hugging face implementation ( torch )
    prompts = ["My dog is", "I'd like to go"]
    input_token_ids = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]
    logits_array = gptj_fn.apply(  # type: ignore
        params, jax.random.PRNGKey(42), token_ids=input_token_ids
    )["logits"]

    for prompt, logits in zip(prompts, logits_array):
        inputs = tokenizer(prompt, return_tensors="pt")
        outputs = huggingface_model(**inputs, labels=inputs["input_ids"])
        logits_hf = outputs.logits.detach().numpy()[0]
        pytest.assume(
            np.allclose(logits_hf, logits[: logits_hf.shape[0]], atol=1e-4),
            "computed logits don't match the HuggingFace ones",
        )
