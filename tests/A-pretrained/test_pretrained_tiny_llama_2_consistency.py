from typing import Callable, Tuple

import haiku as hk
import jax
import numpy as np
import pytest
from jax.random import PRNGKey
from transformers import LlamaForCausalLM, LlamaTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.pretrained.llama import (
    LLAMA_MODEL_NAME,
    translate_torch_params_from_huggingface,
)
from trix.utils.decoding import decode_greedy


def get_pretrained_tiny_llama_2() -> Tuple[
    hk.Params, Callable, LlamaTokenizer, GptConfig
]:
    """
    Create a Haiku tiny Llama 2 model by downloading the model directly
    from 'https://huggingface.co/yujiepan/llama-2-tiny-random'.

    Returns:
        Model parameters.
        Haiku function to call the model.
        Tokenizer.
        Model config.
    """

    model_params = LlamaForCausalLM.from_pretrained(
        "yujiepan/llama-2-tiny-random"
    ).state_dict()
    tokenizer = LlamaTokenizer.from_pretrained("yujiepan/llama-2-tiny-random")

    config = GptConfig(
        vocab_size=tokenizer.vocab_size,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=8,
        ffn_embed_dim=32,
        num_heads=2,
        num_layers=1,
        rope_dimensions=None,
        max_position_embeddings=512,
        add_bias_ffn=False,
        norm_type="RMS_norm",
        parallel_attention_ff=False,
        ffn_activation_name="silu",
        use_glu_in_ffn=True,
        add_bias_lm_head=False,
        use_gradient_checkpointing=False,
    )

    parameters = translate_torch_params_from_huggingface(
        model_params,
        num_layers=1,
        num_heads=2,
        embed_dim=8,
    )

    llama_fn = build_gpt_fn(config, name=LLAMA_MODEL_NAME)

    return parameters, llama_fn, tokenizer, config


def test_pretrained_tiny_llama_2_model() -> None:
    """
    Tests a pretrained Llama 2 model:
    - step 1 : Model initialization sanity : init the model, and test if the shapes of
        the tensors match the ones from the parameters of the pre-trained model
    - step 2 : Compare generated sequences against HuggingFace implementation (torch)
    - step 3 : Compare the logits against HuggingFace implementation (torch)
    """

    params, llama_fn, tokenizer, _ = get_pretrained_tiny_llama_2()

    llama_fn = hk.transform(llama_fn)

    # step 1 : Model initialisation sanity : init the model, and test if the shapes of
    # the tensors match the ones from the parameters of the pre-trained model
    init_prompt = "Hello, I"
    input_token_ids = tokenizer([init_prompt], return_tensors="np", padding=False)[
        "input_ids"
    ]

    init_params = llama_fn.init(PRNGKey(1), input_token_ids)  # type: ignore
    for layer_name in init_params:
        assert layer_name in params
        pytest.assume(
            layer_name in params,
            f"the layer name {layer_name} should be in the loaded ones",
        )
        if type(init_params[layer_name]) == dict:
            for weight_name in init_params[layer_name]:
                pytest.assume(
                    params[layer_name][weight_name].shape
                    == init_params[layer_name][weight_name].shape,
                    "parameters at init are inconsistent with the loaded ones",
                )

    # step 2 : Compare generated sequences against hugging face implementation ( torch )
    prompts = ["My dog is", "I'd like to go"]
    greedy_tokens = [
        decode_greedy(
            init_tokens_ids=tokenizer([prompt], return_tensors="np")["input_ids"],
            random_key=PRNGKey(0),
            params=params,
            apply_fn=llama_fn.apply,
            num_tokens_to_decode=10,
            eos_token_id=tokenizer.eos_token_id,
        )[0]
        for prompt in prompts
    ]

    answers = [tokenizer.batch_decode(tokens)[0] for tokens in greedy_tokens]

    hf_model = LlamaForCausalLM.from_pretrained("yujiepan/llama-2-tiny-random")

    for prompt, answer in zip(prompts, answers):
        input_ids = tokenizer(prompt, return_tensors="pt").input_ids
        gen_tokens = hf_model.generate(input_ids, do_sample=False, max_length=30)
        answer_hf = tokenizer.batch_decode(gen_tokens)[0]
        pytest.assume(
            answer == answer_hf[: len(answer)],
            "decoded sequences don't match the HuggingFace ones",
        )

    # step 3: Compare the logits against hugging face implementation (torch)
    prompts = ["My dog is", "I'd like to go"]
    input_token_ids = tokenizer(prompts, return_tensors="np", padding=False)[
        "input_ids"
    ]
    input_token_ids = [input_token.astype(np.int64) for input_token in input_token_ids]

    for prompt, input_token_id in zip(prompts, input_token_ids):
        logits = llama_fn.apply(  # type: ignore
            params, jax.random.PRNGKey(42), token_ids=np.array([input_token_id])
        )["logits"]
        inputs = tokenizer(prompt, return_tensors="pt")
        outputs = hf_model(**inputs, labels=inputs["input_ids"])
        logits_hf = outputs.logits.detach().numpy()[0]
        pytest.assume(
            np.allclose(logits_hf, logits[: logits_hf.shape[0]], atol=5e-4),
            "computed logits don't match the HuggingFace ones",
        )
