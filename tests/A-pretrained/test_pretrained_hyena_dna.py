from typing import Dict

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest
import torch

from tests.test_utils.standalone_torch_hyena_dna import HyenaDNAModel
from trix.pretrained.hyena_dna import get_pretrained_hyena_dna
from trix.pretrained.hyena_dna_utils.character_tokenizer import CharacterTokenizer
from trix.pretrained.hyena_dna_utils.hugging_face import get_hyena_dna_from_hf


@pytest.mark.parametrize("use_gradient_checkpointing", [True, False])
@pytest.mark.parametrize(
    "pretrained_model_name",
    [
        "hyenadna-tiny-1k-seqlen",
        "hyenadna-tiny-1k-seqlen-d256",
        "hyenadna-tiny-16k-seqlen-d128",
    ]
    # larger models tested in K-large_tests
)
def test_get_pretrained_hyena(
    use_gradient_checkpointing: bool, pretrained_model_name: str
) -> None:

    # tolerance to use for tests
    tolerance = 0.0001
    grad_tolerance = 0.01

    # get jax defined model
    params, forward_fn, tokenizer, config = get_pretrained_hyena_dna(
        pretrained_model_name,
        use_gradient_checkpointing_mixer=use_gradient_checkpointing,
        use_gradient_checkpointing_mlp=use_gradient_checkpointing,
    )

    # get torch defined model
    torch_params, torch_config = get_hyena_dna_from_hf(pretrained_model_name)
    torch_model = HyenaDNAModel(**torch_config)
    torch_model.load_state_dict(torch_params)

    max_length = config.hyena_operator_max_seq_len

    # get tokenizer defined in the hyena-dna repo
    character_tokenizer = CharacterTokenizer(
        characters=["A", "C", "G", "T", "N"],  # add DNA characters, N is uncertain
        model_max_length=max_length,  # type:ignore # to account for special tokens EOS
        add_special_tokens=False,  # we handle special tokens elsewhere
        padding_side="left",  # since HyenaDNA is causal, we pad on the left
    )

    # generate fake inputs
    sequences = ["ATACG" * 10, "CGCCC" * 12, "CGCGCGCTTTAfk" * 4]

    # check that tokenized sequences are the same across tokenizers
    token_ids_jax = jnp.array([b[1] for b in tokenizer.batch_tokenize(sequences)])
    token_ids_original = jnp.array(
        character_tokenizer(sequences, padding="max_length")["input_ids"]
    )

    pytest.assume(jnp.allclose(token_ids_jax, token_ids_original, atol=tolerance))

    # check that embeddings/logits are the same
    forward_fn = hk.transform(forward_fn)
    rng = jax.random.PRNGKey(0)

    # jax forward
    jax_outputs = forward_fn.apply(params, rng, token_ids_jax, is_training=False)
    jax_embeddings = jax_outputs["embeddings"]
    jax_logits = jax_outputs["logits"]

    # torch forward
    torch_model.eval()  # since there is dropout
    torch_outputs = torch_model(torch.tensor(np.array(token_ids_original)))
    torch_embeddings = torch_outputs["embeddings"]
    torch_logits = torch_outputs["lm_head_logits"]

    # compare embeddings
    pytest.assume(
        jnp.allclose(
            jax_embeddings, jnp.array(torch_embeddings.detach().numpy()), atol=tolerance
        )
    )

    # compare lm head logits
    pytest.assume(
        jnp.allclose(
            jax_logits, jnp.array(torch_logits.detach().numpy()), atol=tolerance
        )
    )

    # compare gradients for a few different layers

    # jax gradients
    def loss_fn(
        params: Dict[str, jnp.ndarray], rng: jnp.ndarray, tokens: jnp.ndarray
    ) -> jnp.ndarray:

        outs = forward_fn.apply(params, rng, tokens, is_training=False)  # type:ignore
        embeddings = outs["embeddings"]

        loss = jnp.sum(embeddings)
        return loss

    jax_gradients = jax.grad(loss_fn)(params, rng, token_ids_jax)

    # torch gradients
    loss = torch.sum(torch_embeddings)
    loss.backward()

    # compare word embedding gradients
    pytest.assume(
        jnp.allclose(
            jax_gradients["hyena_dna/~/lm_backbone/~/token_embeddings/~/embed"][
                "embeddings"
            ],
            torch_model.backbone.embeddings.word_embeddings.weight.grad.detach().numpy(),  # noqa: E501
            atol=grad_tolerance,
        )
    )

    # compare short conv gradients
    jnp.allclose(
        jax_gradients["hyena_dna/~/lm_backbone/~/block/~/hyena_operator/~/conv1_d"][
            "w"
        ],
        getattr(torch_model.backbone.layers, "0")
        .mixer.short_filter.weight.grad.T.detach()
        .numpy(),
        atol=grad_tolerance,
    )

    pytest.assume(
        jnp.allclose(
            jax_gradients["hyena_dna/~/lm_backbone/~/block_1/~/mlp/~/linear"]["b"],
            getattr(torch_model.backbone.layers, "1")
            .mlp.fc1.bias.grad.detach()
            .numpy(),
            atol=grad_tolerance,
        )
    )
