from typing import Tuple

import haiku as hk
import jax
import numpy as np
import pytest
import torch
from jax.random import PRNGKey
from transformers import AutoModelForCausalLM, AutoTokenizer, GenerationConfig

from trix.pretrained.rita import get_pretrained_rita_model
from trix.utils.decoding import decode_greedy


def get_pretrained_hf_rita() -> Tuple[AutoModelForCausalLM, AutoTokenizer]:
    """Retrieves pretrained RITA_s model from HuggingFace."""
    model = AutoModelForCausalLM.from_pretrained(
        "lightonai/RITA_s", trust_remote_code=True
    )
    tokenizer = AutoTokenizer.from_pretrained("lightonai/RITA_s")

    # The original RITA model was written in HuggingFace v4.18.0, which had a default
    # `prepare_inputs_for_generation`. This method is necessary if we want to call
    # generate() to generate text with our model.
    #
    # Unfortunately, the default `prepare_inputs_for_generation` was removed from
    # HuggingFace by this commit:
    # https://github.com/huggingface/transformers/commit/4bd36f1853501c453dc0ae994f789311468b87bc
    #
    # The line below adds back the default function. Note that this error is known and
    # also appears in this issue:
    # https://huggingface.co/lightonai/RITA_s/discussions/4
    def prepare_inputs_for_generation(self, input_ids, **kwargs):  # type: ignore
        return {"input_ids": input_ids}

    # The line below takes advantage of descriptor protocol in Python to bind a method
    # to an existing object -- see here for more info:
    # https://stackoverflow.com/questions/972/adding-a-method-to-an-existing-object-instance-in-python
    model.prepare_inputs_for_generation = prepare_inputs_for_generation.__get__(model)

    return model, tokenizer


def test_pretrained_rita_model() -> None:
    """
    Tests a pretrained RITA model :
    - step 1 : Model initialisation sanity : init the model, and test if the shapes of
        the tensors match the ones from the parameters of the pre-trained model
    - step 2 : Compare generated sequences against hugging face implementation ( torch )
    - step 3 : Compare the logits against hugging face implementation ( torch )
    """

    original_model, original_tokenizer = get_pretrained_hf_rita()

    params, rita_fn, tokenizer, config = get_pretrained_rita_model(model_name="RITA_s")
    rita_fn = hk.transform(rita_fn)

    # step 1 : Model initialisation sanity : init the model, and test if the shapes of
    # the tensors match the ones from the parameters of the pre-trained model
    init_prompt = "MAB"
    input_token_ids = tokenizer(
        [init_prompt], return_tensors="np", add_special_tokens=False
    )["input_ids"]

    init_params = rita_fn.init(PRNGKey(1), input_token_ids)  # type: ignore
    for layer_name in init_params:
        pytest.assume(
            layer_name in params,
            f"the layer name {layer_name} should be in the loaded ones",
        )
        if isinstance(init_params[layer_name], dict):
            for weight_name in init_params[layer_name]:
                pytest.assume(
                    params[layer_name][weight_name].shape
                    == init_params[layer_name][weight_name].shape,
                    "parameters at init are inconsistent with the loaded ones",
                )

    # step 2 : Compare generated sequences against hugging face implementation ( torch )
    prompts = ["M", "MAB", "MALL"]
    greedy_tokens = [
        decode_greedy(
            init_tokens_ids=tokenizer(
                [prompt], return_tensors="np", add_special_tokens=False
            )["input_ids"],
            random_key=PRNGKey(0),
            params=params,
            apply_fn=rita_fn.apply,
            num_tokens_to_decode=10,
            eos_token_id=tokenizer.eos_token_id,
        )[0]
        for prompt in prompts
    ]

    answers = [tokenizer.batch_decode(tokens)[0] for tokens in greedy_tokens]

    # In older versions of HuggingFace, generate() had arguments such as `eos_token_id`
    # that would default to values found in `model.config`. Later versions of
    # HuggingFace instead take in a GenerationConfig that we define below.
    generation_config = GenerationConfig(
        max_new_tokens=30,
        do_sample=False,
        eos_token_id=original_model.config.eos_token_id,
    )

    for prompt, answer in zip(prompts, answers):
        input_ids = original_tokenizer(
            prompt, return_tensors="pt", add_special_tokens=False
        ).input_ids
        gen_tokens = original_model.generate(input_ids, generation_config)
        answer_hf = original_tokenizer.batch_decode(gen_tokens)[0]
        pytest.assume(
            answer == answer_hf[: len(answer)],
            "decoded sequences don't match the HuggingFace ones",
        )

    # step 3 : Compare the logits against hugging face implementation ( torch )
    # To make the test more robust, the sequences below are different from the ones
    # above, and they are not ordered by length.
    prompts = ["ZLLAAAN", "A", "XMQ"]
    input_token_ids = tokenizer(
        prompts, return_tensors="np", padding=True, add_special_tokens=False
    )["input_ids"]
    logits_array = rita_fn.apply(  # type: ignore
        params, jax.random.PRNGKey(42), token_ids=input_token_ids
    )["logits"]

    for prompt, logits in zip(prompts, logits_array):
        inputs = original_tokenizer(
            prompt, return_tensors="pt", add_special_tokens=False
        )
        outputs = original_model(**inputs, labels=inputs["input_ids"])
        logits_hf = outputs.logits.detach().numpy()[0]
        pytest.assume(
            np.allclose(logits_hf, logits[: logits_hf.shape[0]], atol=1e-4),
            "computed logits don't match the HuggingFace ones",
        )


def test_pretrained_rita_model_soft_prompt() -> None:
    """Tests a pretrained RITA model with soft prompting.

    We compare the logits output from a single pass of the trix implementation and the
    HuggingFace implementation. It is difficult to examine longer sequences since
    neither trix nor HuggingFace have generation pipelines that accept soft prompting by
    default.
    """

    original_model, original_tokenizer = get_pretrained_hf_rita()

    params, rita_fn, tokenizer, config = get_pretrained_rita_model(model_name="RITA_s")
    rita_fn = hk.transform(rita_fn)

    random_key = jax.random.PRNGKey(42)

    prompts = ["MAB", "M", "MALL"]
    for prompt in prompts:
        soft_prompt = jax.random.uniform(random_key, (1, 2, config.embed_dim))

        # Soft prompt the trix model.
        logits = rita_fn.apply(
            params=params,
            token_ids=tokenizer(
                prompt,
                return_tensors="np",
                add_special_tokens=False,
            )["input_ids"],
            rng=random_key,
            soft_prompt=soft_prompt,
        )["logits"]

        # Soft prompt the HuggingFace model - this code is based on the forward()
        # functions here:
        # https://huggingface.co/lightonai/RITA_s/blob/main/rita_modeling.py
        hf_prompt_embeddings = original_model.get_input_embeddings()(
            original_tokenizer(
                prompt, return_tensors="pt", add_special_tokens=False
            ).input_ids
        )
        hf_input_embeddings = torch.cat(  # (1, soft_prompt_len + prompt_len, embed_dim)
            (
                torch.from_numpy(np.asarray(soft_prompt)),
                hf_prompt_embeddings,
            ),
            axis=1,
        )
        hf_transformer_outputs = original_model.transformer(
            # The model needs an array of input_ids to determine shapes of internal
            # tensors, so we pass an arbitrary array with the right shape.
            input_ids=torch.zeros((1, hf_input_embeddings.shape[1]), dtype=int),
            inputs_embeds=hf_input_embeddings,
        )
        hf_logits = original_model.lm_head(hf_transformer_outputs.hidden_states)

        pytest.assume(
            np.allclose(hf_logits.detach().numpy(), logits, atol=1e-4),
            "computed logits don't match the HuggingFace ones",
        )
