import jax
import numpy as np
import pytest

from trix.pretrained.scgpt import (
    download_io_data,
    get_pretrained_scgpt_model,
    reset_dsbn_statistics,
)
from trix.utils.masking import build_padding_attention_mask


@pytest.mark.parametrize("model_name", ["blood", "human", "pancancer"])
def test_pretrained_scgpt_consistency(model_name: str) -> None:
    """
    Test that the pretrained model in PyTorch and Haiku are consistent.
    """

    seed = 0
    random_key = jax.random.PRNGKey(seed)
    pad_token = "<pad>"

    jax_params, forward_fn, vocab, config = get_pretrained_scgpt_model(
        model_name=model_name
    )
    pad_token_id = vocab[pad_token]

    # Load data
    loaded_io = download_io_data(
        model_name=model_name, test_file_name="io_pretrained_params.joblib"
    )
    batch = loaded_io["inputs"]

    gene_indexes = batch["gene_ids"]
    gene_expression_values = batch["values"]

    attention_mask = build_padding_attention_mask(batch["gene_ids"], pad_token_id)

    # Initialize Haiku model (state = running stats for batch norm)
    _, initial_state = forward_fn.init(
        random_key,
        gene_indexes=gene_indexes,
        gene_expression_values=gene_expression_values,
        attention_mask=attention_mask,
        batch_labels=batch["batch_labels"],
        is_training=True,
    )

    state = reset_dsbn_statistics(initial_state)

    # Haiku forward pass
    jax_out, _ = forward_fn.apply(
        jax_params,
        state,
        random_key,
        gene_indexes=gene_indexes,
        gene_expression_values=gene_expression_values,
        attention_mask=attention_mask,
        is_training=False,
        batch_labels=batch["batch_labels"],
    )

    # Results
    torch_out = loaded_io["torch_outputs"]
    jax_out = jax.tree_map(np.array, jax_out)

    pytest.assume(
        np.allclose(
            torch_out["cell_emb"],
            jax_out["cell_embedding"],
            atol=1e-4,
            rtol=1e-3,
        )
    )
    pytest.assume(
        np.allclose(
            torch_out["mlm_output"],
            jax_out["gene_expression_predictions"],
            atol=1e-4,
            rtol=1e-3,
        )
    )

    pytest.assume(
        np.allclose(
            torch_out["mvc_output"],
            jax_out["mvc_gene_expression_predictions"],
            atol=1e-4,
            rtol=1e-3,
        )
    )
