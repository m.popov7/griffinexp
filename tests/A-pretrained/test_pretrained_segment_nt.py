import os

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.pretrained.segment_nt import (
    download_test_inputs_file,
    get_pretrained_segment_nt_model,
)


@pytest.mark.parametrize(
    "model_name",
    ["segment_nt", "segment_nt_multi_species"],
)
def test_pretrained_inference_result(model_name: str) -> None:
    """Test inference output for pretrained models over previously obtained results."""
    # Load pre-computed inputs / outputs
    test_inputs = download_test_inputs_file(model_name=model_name)

    # Load pre-trained model
    embeddings_layers_to_save = tuple(
        [
            int(key.split("embeddings_")[-1])
            for key in test_inputs.keys()
            if "embeddings" in key
        ]
    )
    parameters, forward_fn, tokenizer, config = get_pretrained_segment_nt_model(
        model_name=model_name,
        embeddings_layers_to_save=embeddings_layers_to_save,
        max_positions=test_inputs["tokens"].shape[-1],
    )
    forward_fn = hk.transform(forward_fn)

    # Tokenize and do a forward pass
    key = jax.random.PRNGKey(0)
    batch = tokenizer.batch_tokenize([test_inputs["sequence"]])
    tokens_ids = [b[1] for b in batch]
    tokens = jnp.asarray(tokens_ids, dtype=jnp.int32)
    outs = forward_fn.apply(parameters, key, tokens)

    # Check all inputs / outputs
    tolerance = 0.001
    pytest.assume(float(jnp.max(jnp.abs(test_inputs["tokens"] - tokens))) < tolerance)

    pytest.assume(
        float(jnp.max(jnp.abs(test_inputs["logits"] - outs["logits"]))) < tolerance
    )
    for embedding_num in embeddings_layers_to_save:
        key_name = f"embeddings_{embedding_num}"
        error = float(jnp.max(jnp.abs(test_inputs[key_name] - outs[key_name])))
        pytest.assume(error < tolerance)

    # Purge the cache
    os.system("pip cache purge")
