import os

import esm
import haiku as hk
import jax
import jax.numpy as jnp
import pytest
import torch

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.pretrained.esm import get_pretrained_esm_model
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.masking import build_padding_attention_mask


@pytest.mark.parametrize(
    "model_name",
    [
        "esm1_t6_43M_UR50S",
        "esm2_t12_35M_UR50D",
        "esm1v_t33_650M_UR90S_1",
        "esm1b_t33_650M_UR50S",
    ],
)
def test_consistency(model_name: str) -> None:
    """
    Checks that the jax version of ESM matches the outputs of the pytorch version
    for same inputs.
    This test checks also that both Trix and ESM tokenizer behave the same as well as
    padding is taken into account correctly.
    """
    os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"] = "false"

    max_positions = 64

    # Load Torch Model
    model, alphabet = getattr(esm.pretrained, model_name)()
    batch_converter = alphabet.get_batch_converter()

    # Load Jax Model
    parameters, forward_fn, tokenizer, config = get_pretrained_esm_model(
        model_name=model_name,
        max_positions=max_positions,
        embeddings_layers_to_save=(model.num_layers,),
    )

    # Input sequence
    min_sequence_length = 10
    max_sequence_length = 50
    num_sequences = 8
    sequences = generate_fake_dataset(
        min_sequence_length,
        max_sequence_length,
        num_sequences,
        AMINO_ACIDS,
        seed=0,
    )

    # pad torch tokens
    inputs = [(f"seq_{i}", sequences[i]) for i in range(num_sequences)]
    torch_tokens = batch_converter(inputs)[2]
    torch_tokens = torch.cat(
        (
            torch_tokens,
            torch.ones(num_sequences, max_positions - torch_tokens.shape[1]),
        ),
        axis=1,
    ).long()

    # here trix tokenizer will pad to max_positions (need fixed length in Jax)
    jax_tokens = tokenizer.batch_tokenize([x[1] for x in inputs])
    jax_tokens = jnp.array([j[1] for j in jax_tokens])

    pytest.assume(
        jnp.allclose(torch_tokens.numpy(), jax_tokens),
        "tokenizer gives different results for jax and pytorch",
    )
    # Predict with Torch
    torch_output = model(torch_tokens, repr_layers=(model.num_layers,))
    torch.cuda.empty_cache()

    # Build Jax Forward Fn
    transformed_fn = hk.transform(forward_fn)
    apply_fn = transformed_fn.apply
    random_key = jax.random.PRNGKey(0)

    # Compute Jax Embeddings
    attention_mask = build_padding_attention_mask(
        jax_tokens, pad_token_id=tokenizer.pad_token_id
    )
    jax_output = apply_fn(parameters, random_key, jax_tokens, attention_mask)
    sequence_mask = attention_mask[:, 0, :, 0][:, :, None]

    # compare final layer embeddings
    torch_embeddings = (
        torch_output["representations"][model.num_layers].detach().cpu().numpy()
        * sequence_mask
    )
    jax_embeddings = jax_output[f"embeddings_{model.num_layers}"] * sequence_mask
    pytest.assume(
        jnp.allclose(torch_embeddings, jax_embeddings, atol=5e-3),
        "embeddings differ between torch and jax",
    )

    # compare lm head logits
    torch_logits = torch_output["logits"].detach().cpu().numpy() * sequence_mask
    jax_logits = jax_output["logits"] * sequence_mask
    pytest.assume(
        jnp.allclose(torch_logits, jax_logits, atol=5e-3),
        "logits differ between torch and jax",
    )
