import os
from typing import List

import haiku as hk
import jax
import jax.numpy as jnp
import pytest
import transformers
from transformers import T5EncoderModel

from trix.pretrained.t5 import get_pretrained_t5_model

transformers.logging.set_verbosity_error()


def read_sentences(input_file: str) -> List[str]:
    """
    Reads a .txt file.

    Args:
        input_file: Path to the .txt file.

    Returns:
        List of sentences in the .txt file.
    """
    with open(input_file) as f:
        sentences = [line.rstrip() for line in f]
    return sentences


@pytest.mark.parametrize("model_name", ["t5-small", "flan-t5-small"])
def test_t5_encoder(model_name: str) -> None:
    """
    Checks that the Trix T5 encoder embeddings match the ones from HuggingFace.
    """
    # Get Trix Model
    parameters, encoder_fn, tokenizer, _ = get_pretrained_t5_model(
        model_name=model_name, encoder_only=True, embeddings_layers_to_save=(3,)
    )
    encoder_fn = hk.transform(encoder_fn)

    # Load Data
    test_path = os.path.dirname(os.path.realpath(__file__))

    sentences = read_sentences(f"{test_path}/test_files/input_sentences.txt")
    tokenized_seqs = tokenizer(
        sentences,
        padding="longest",
        truncation=True,
        return_tensors="pt",
    )
    tokens = tokenized_seqs["input_ids"]
    attention_mask = tokenized_seqs["attention_mask"]

    outs = encoder_fn.apply(
        parameters,
        jax.random.PRNGKey(42),
        encoder_token_ids=tokens.cpu().numpy(),
        dropout_rate=0,
    )

    encoder_output = outs["embeddings"]

    # Instantiate Huggingface Model
    if model_name == "flan-t5-small":
        hf_model_name = f"google/{model_name}"
    else:
        hf_model_name = model_name
    t5_model = T5EncoderModel.from_pretrained(
        hf_model_name, output_hidden_states=True  # return intermediate embddings
    )
    hf_outs = t5_model(input_ids=tokens, attention_mask=attention_mask)

    hf_embeds = hf_outs["last_hidden_state"].detach().numpy()
    mask = jnp.expand_dims(tokens.cpu().numpy() != 0, axis=-1)

    pytest.assume(
        jnp.allclose(encoder_output * mask, hf_embeds * mask, atol=1e-5),
        "trix is inconsistent with huggingface at the last embedding",
    )
    trix_intermediate_embed = outs["embeddings_3"]
    hf_intermediate_embed = hf_outs["hidden_states"][3].detach().numpy()

    pytest.assume(
        jnp.allclose(
            trix_intermediate_embed * mask,
            hf_intermediate_embed * mask,
            atol=1e-3,  # atol is higher as the embeddings are not normalized
        ),
        "trix is inconsistent with huggingface at the intermediate embedding",
    )


@pytest.mark.parametrize("model_name", ["t5-small", "flan-t5-small"])
def test_t5_weight_loading(model_name: str) -> None:
    """Temporary test to check that weight loading works. Generation will be brought
    back in a future MR."""
    # Get Trix Model
    parameters, encoder_decoder_fn, tokenizer, _ = get_pretrained_t5_model(
        model_name=model_name, encoder_only=False
    )
    encoder_decoder_fn = hk.transform(encoder_decoder_fn)
    # Load Data
    test_path = os.path.dirname(os.path.realpath(__file__))

    sentences = read_sentences(f"{test_path}/test_files/input_sentences.txt")
    tokenized_seqs = tokenizer(
        sentences,
        padding="longest",
        truncation=True,
        return_tensors="np",
    )["input_ids"]
    random_key = jax.random.PRNGKey(0)

    logits = encoder_decoder_fn.apply(
        parameters,
        random_key,
        encoder_token_ids=tokenized_seqs,
        decoder_token_ids=jnp.array([[tokenizer.pad_token_id]] * len(sentences)),
        dropout_rate=0.0,
    )["logits"]

    pytest.assume(jnp.all(~jnp.isnan(logits)), "NaNs in logits.")


@pytest.mark.parametrize("model_name", ["t5-small", "flan-t5-small"])
def test_t5_generation(model_name: str) -> None:
    """
    Checks that using greedy decoding with the Trix T5 model gives the same decoded
    sentences as HuggingFace T5ForConditionalGeneration model. Uses pre-computed
    output sequences in test_files.
    """
    # Get Trix Model
    parameters, encoder_decoder_fn, tokenizer, _ = get_pretrained_t5_model(
        model_name=model_name, encoder_only=False
    )
    encoder_decoder_fn = hk.transform(encoder_decoder_fn)
    # Load Data
    test_path = os.path.dirname(os.path.realpath(__file__))

    sentences = read_sentences(f"{test_path}/test_files/input_sentences.txt")
    tokenized_seqs = tokenizer(
        sentences,
        padding="longest",
        truncation=True,
        return_tensors="np",
    )["input_ids"]
    random_key = jax.random.PRNGKey(0)

    def generate(
        apply_fn: hk.Transformed,
        parameters: hk.Params,
        random_key: jnp.ndarray,
        encoder_token_ids: jnp.ndarray,
        decoder_token_ids: jnp.ndarray,
        dropout_rate: float = 0,
        max_new_tokens: int = 50,
    ) -> jnp.ndarray:
        """Generate new tokens from a pretrained T5 model."""
        batch_size, _ = encoder_token_ids.shape
        unfinished_sequences = jnp.ones(batch_size, dtype=jnp.bool_)
        decoder_token_ids = jnp.concatenate(
            [
                decoder_token_ids,
                jnp.full(
                    shape=(batch_size, max_new_tokens),
                    fill_value=0,  # tokenizer pad value
                    dtype=jnp.int32,
                ),
            ],
            axis=-1,
        )
        time_step = 0
        out = None
        while jnp.any(unfinished_sequences) and time_step < max_new_tokens:
            out = apply_fn(
                parameters,
                random_key,
                encoder_token_ids=encoder_token_ids,
                encoder_output=out,
                decoder_token_ids=decoder_token_ids[:, : time_step + 1],
                dropout_rate=0,
            )
            out_logits = out["logits"]
            new_token_id = jnp.argmax(out_logits[:, -1, :], axis=-1)
            unfinished_sequences = unfinished_sequences * (
                decoder_token_ids[:, time_step] != tokenizer.eos_token_id
            )
            new_token_id = (
                unfinished_sequences * new_token_id
                + ~unfinished_sequences * tokenizer.pad_token_id
            )
            decoder_token_ids = decoder_token_ids.at[:, time_step + 1].set(new_token_id)
            time_step += 1
        return decoder_token_ids

    generated_tokens = generate(
        encoder_decoder_fn.apply,
        parameters,
        random_key,
        encoder_token_ids=tokenized_seqs,
        decoder_token_ids=jnp.array([[tokenizer.pad_token_id]] * len(sentences)),
        dropout_rate=0.0,
        max_new_tokens=50,
    )

    output_sequences = tokenizer.batch_decode(
        generated_tokens, skip_special_tokens=True
    )
    expected_sequences = read_sentences(
        f"{test_path}/test_files/output_sentences_{model_name}.txt"
    )

    pytest.assume(
        jnp.all(
            jnp.array(
                [
                    out_s == exp_s
                    for (out_s, exp_s) in zip(output_sequences, expected_sequences)
                ]
            )
        ),
        "decoded sequences don't match the HuggingFace ones",
    )
