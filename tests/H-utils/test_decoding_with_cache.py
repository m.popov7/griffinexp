"""
Tests for utils used for autoregressive generation with decoder LMs
( with caching system ).
"""
import functools
from pprint import pprint
from typing import Optional, Tuple

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest
from transformers import AutoTokenizer

from trix.models.gpt.caching.model_caching import build_gpt_fn_with_caching
from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.types import RNGKey
from trix.utils.decoding import update_tokens_ids_greedy
from trix.utils.decoding_with_cache import (
    update_tokens_ids_greedy_with_cache,
    update_tokens_ids_temperature_sampling_with_cache,
)


@pytest.mark.parametrize(
    (
        "rope_dimensions",
        "add_bias_ffn",
        "norm_type",
        "parallel_attention_ff",
        "use_glu_in_ffn",
        "ffn_activation_name",
        "add_bias_lm_head",
    ),
    [
        [
            2,
            True,
            "layer_norm",
            True,
            False,
            "gelu",
            True,
        ],  # GPTJ,
        [
            None,
            False,
            "RMS_norm",
            False,
            True,
            "silu",
            False,
        ],  # LLAMA
    ],
)
def test_greedy_decoding_with_cache(
    rope_dimensions: Optional[int],
    add_bias_ffn: bool,
    norm_type: str,
    parallel_attention_ff: bool,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    add_bias_lm_head: bool,
) -> None:
    """
    Test greedy decoding with cache, and compare the results with decoding without
    """
    seed = 42
    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=4,
        ffn_embed_dim=2,
        num_heads=2,
        num_layers=1,
        rope_dimensions=rope_dimensions,
        max_position_embeddings=512,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=add_bias_lm_head,
        norm_type=norm_type,
        parallel_attention_ff=parallel_attention_ff,
        use_gradient_checkpointing=False,
    )

    inputs = [
        "What is the difference between a gene and a protein?",
    ]
    inputs_tokens_ids = jnp.asarray(tokenizer(inputs, padding=True)["input_ids"])

    num_tokens_to_decode = 5
    cache_length = inputs_tokens_ids.shape[1] + num_tokens_to_decode

    gptj_fn_with_state = build_gpt_fn_with_caching(
        config, cache_length=cache_length, name="gpt_decoder"
    )
    gptj_fn_with_state = hk.transform_with_state(gptj_fn_with_state)

    gptj_fn = build_gpt_fn(config)
    gptj_fn = hk.transform(gptj_fn)

    init_positions = np.array([[0]])
    init_tokens = inputs_tokens_ids[:1, :1]

    random_key = jax.random.PRNGKey(seed)
    params, cache_state = gptj_fn_with_state.init(
        random_key, init_tokens, init_positions
    )
    pprint(jax.tree_map(lambda x: x.shape, params))

    def update_tokens_fn_greedy_with_cache(
        tokens_ids: jnp.ndarray,
        time_step: jnp.ndarray,
        random_key: jnp.ndarray,
        params: hk.Params,
        cache_state: hk.State,
    ) -> Tuple[jnp.ndarray, RNGKey, jnp.ndarray]:
        return update_tokens_ids_greedy_with_cache(  # type: ignore
            tokens_ids=tokens_ids,
            time_step=time_step,
            random_key=random_key,
            params=params,
            cache_state=cache_state,
            apply_fn=gptj_fn_with_state.apply,  # type: ignore
            pad_token_id=tokenizer.eos_token_id,
        )

    # init of the token tensor
    batch_size, inputs_length = inputs_tokens_ids.shape
    fill_tokens = jnp.full(
        shape=(batch_size, num_tokens_to_decode),
        fill_value=tokenizer.eos_token_id,
    )
    init_tokens_ids = jnp.concatenate([inputs_tokens_ids, fill_tokens], axis=-1)
    tokens_ids = init_tokens_ids.copy()

    # decoding with cache
    cache_state = jax.tree_map(lambda x: np.tile(x, (batch_size, 1, 1, 1)), cache_state)

    for time_step in range(init_tokens_ids.shape[1] - 1):
        tokens_ids, random_key, cache_state = update_tokens_fn_greedy_with_cache(
            tokens_ids,
            time_step,
            random_key,
            params,
            cache_state,
        )
    tokens_ids_cache = tokens_ids
    decoded_outputs_cache = tokenizer.batch_decode(tokens_ids)

    # decoding without cache
    update_tokens_fn_greedy = jax.jit(
        functools.partial(update_tokens_ids_greedy, apply_fn=gptj_fn.apply)
    )
    tokens_ids = init_tokens_ids.copy()
    for i in range(num_tokens_to_decode):
        time_step = jnp.asarray(i + inputs_length - 1)

        tokens_ids, random_key = update_tokens_fn_greedy(
            tokens_ids=tokens_ids,
            random_key=random_key,
            params=params,
            time_step=time_step,
        )
    decoded_outputs = tokenizer.batch_decode(tokens_ids)

    pytest.assume(
        np.allclose(tokens_ids_cache, tokens_ids),
        f"\n{tokens_ids_cache}\n" f"{tokens_ids}",
    )
    pytest.assume(
        decoded_outputs_cache == decoded_outputs,
        f"\n{decoded_outputs_cache}\n" f"{decoded_outputs}",
    )


def test_temperature_sampling_decoding_with_cache() -> None:
    """
    Test greedy decoding with cache, and compare the results with decoding without
    """
    seed = 42
    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=4,
        ffn_embed_dim=2,
        num_heads=2,
        num_layers=1,
        rope_dimensions=2,
        max_position_embeddings=512,
        add_bias_ffn=True,
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=True,
        norm_type="layer_norm",
        parallel_attention_ff=True,
        use_gradient_checkpointing=False,
    )

    inputs = [
        "What is the difference between a gene and a protein?",
    ]
    inputs_tokens_ids = jnp.asarray(tokenizer(inputs, padding=True)["input_ids"])

    num_tokens_to_decode = 5
    cache_length = inputs_tokens_ids.shape[1] + num_tokens_to_decode

    gptj_fn_with_state = build_gpt_fn_with_caching(
        config, cache_length=cache_length, name="gpt_decoder"
    )
    gptj_fn_with_state = hk.transform_with_state(gptj_fn_with_state)

    init_positions = np.array([[0]])
    init_tokens = inputs_tokens_ids[:1, :1]

    random_key = jax.random.PRNGKey(seed)
    params, cache_state = gptj_fn_with_state.init(
        random_key, init_tokens, init_positions
    )

    def update_tokens_fn_temperature_sampling_with_cache(
        tokens_ids: jnp.ndarray,
        time_step: jnp.ndarray,
        random_key: jnp.ndarray,
        params: hk.Params,
        cache_state: hk.State,
    ) -> Tuple[jnp.ndarray, RNGKey, jnp.ndarray]:
        return update_tokens_ids_temperature_sampling_with_cache(  # type: ignore
            tokens_ids=tokens_ids,
            time_step=time_step,
            random_key=random_key,
            params=params,
            cache_state=cache_state,
            apply_fn=gptj_fn_with_state.apply,  # type: ignore
            pad_token_id=tokenizer.eos_token_id,
        )

    # init of the token tensor
    batch_size, inputs_length = inputs_tokens_ids.shape
    fill_tokens = jnp.full(
        shape=(batch_size, num_tokens_to_decode),
        fill_value=tokenizer.eos_token_id,
    )
    init_tokens_ids = jnp.concatenate([inputs_tokens_ids, fill_tokens], axis=-1)
    tokens_ids = init_tokens_ids.copy()

    # decoding with cache
    cache_state = jax.tree_map(lambda x: np.tile(x, (batch_size, 1, 1, 1)), cache_state)

    for time_step in range(init_tokens_ids.shape[1] - 1):
        (
            tokens_ids,
            random_key,
            cache_state,
        ) = update_tokens_fn_temperature_sampling_with_cache(
            tokens_ids,
            time_step,
            random_key,
            params,
            cache_state,
        )
    pytest.assume(not jnp.isnan(tokens_ids).any(), "NaN in result")
