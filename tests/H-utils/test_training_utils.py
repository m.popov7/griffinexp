from typing import Dict, List, Optional

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.models.encoder_decoder.model import (
    DecoderConfig,
    EncoderConfig,
    build_encoder_decoder_fn,
)
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.models.t5.decoder import T5DecoderConfig, build_t5_decoder_only
from trix.tokenizers.language_models.standard import (
    FixedSizeStandardTokenizer,
    StandardTokenizer,
)
from trix.training.losses import (
    cross_entropy_loss,
    cross_entropy_loss_decoder_only,
    cross_entropy_loss_encoder_decoder,
    cross_entropy_loss_multilabel_classification,
    cross_entropy_loss_multiregression,
    mse_loss,
)
from trix.training.utils import (
    get_causal_labels,
    mask_sequence_bert_style,
    split_sequences,
    square_decay,
)
from trix.types import TransformerOutput
from trix.utils.constants.bio import AMINO_ACIDS
from trix.utils.masking import build_padding_attention_mask


@pytest.mark.parametrize("sequences", ["seq a", "seq b", "seq c"])
@pytest.mark.parametrize("train_proportion", [0.0, 1.0, 0.3, 0.5, 0.6])
def test_split_sequences(sequences: List[str], train_proportion: int) -> None:
    """Tests if the sequences are split in the right proportions."""

    train_sequences, test_sequences = split_sequences(
        sequences=sequences, train_proportion=train_proportion, seed=0
    )

    pytest.assume(
        len(train_sequences) == int(np.round(len(sequences) * train_proportion))
    )
    pytest.assume(len(test_sequences) == (len(sequences) - len(train_sequences)))


@pytest.mark.parametrize("init_value", [3e-1])
@pytest.mark.parametrize("warmup_end_lr", [1e-1])
@pytest.mark.parametrize("warmup_updates", [0, 10, 1000])
def test_square_decay(
    init_value: float, warmup_end_lr: float, warmup_updates: int
) -> None:
    """Tests if the appropriate decay schedule is applied before and
    after warmup end."""

    if warmup_updates == 0:
        with pytest.raises(ValueError):
            schedule = square_decay(init_value, warmup_end_lr, warmup_updates)

    else:
        schedule = square_decay(init_value, warmup_end_lr, warmup_updates)

        for i in range((warmup_updates + 100) * 2):
            res = schedule(i)
            if i <= warmup_updates:
                expected = (
                    warmup_end_lr - init_value
                ) * i / warmup_updates + init_value

            else:
                expected = warmup_end_lr * (warmup_updates**0.5) * (i**-0.5)
            pytest.assume(np.allclose(expected, res))


@pytest.mark.parametrize(
    ("noising_ratio", "masking_prob", "random_token_prob"),
    [[0.5, 0.6, 0.2]],
)
@pytest.mark.parametrize(
    ("short_length", "long_length"),
    [[128, 256], [512, 1024]],
)
def test_masking(
    noising_ratio: float,
    masking_prob: float,
    random_token_prob: float,
    short_length: int,
    long_length: int,
) -> None:
    """
    Tests that the masking function masks the right number of tokens in a sequence.
    No test is done on the number of randomized tokens as some randomized tokens can be
    equal to the original one so the behaviour is unpredictable (this is also the case
    in common MLM libraries).
    """

    tokenizer_short = FixedSizeStandardTokenizer(
        fixed_length=short_length, standard_tokens=AMINO_ACIDS, prepend_cls_token=True
    )
    tokenizer_long = FixedSizeStandardTokenizer(
        fixed_length=long_length, standard_tokens=AMINO_ACIDS, prepend_cls_token=True
    )

    sequences = generate_fake_dataset(
        min_sequence_length=50,
        max_sequence_length=100,
        num_sequences=16,
        standard_tokens=AMINO_ACIDS,
        seed=0,
    )

    jax_tokens_short = jnp.array(
        [j[1] for j in tokenizer_short.batch_tokenize(sequences)]
    )
    jax_tokens_long = jnp.array(
        [j[1] for j in tokenizer_long.batch_tokenize(sequences)]
    )

    random_key = jax.random.PRNGKey(0)
    random_token_indices = jnp.array([4, 5, 6, 18, 20, 21, 22, 23])
    mask_id = tokenizer_short.mask_token_id
    pad_id = tokenizer_short.pad_token_id

    tokens_masked_short_padding, _target_short_padding = mask_sequence_bert_style(
        tokens=jax_tokens_short,
        random_key=random_key,
        random_token_indices=random_token_indices,
        mask_id=mask_id,
        pad_id=pad_id,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
        random_token_prob=random_token_prob,
    )

    tokens_masked_long_padding, _target_long_padding = mask_sequence_bert_style(
        tokens=jax_tokens_long,
        random_key=random_key,
        random_token_indices=random_token_indices,
        mask_id=mask_id,
        pad_id=pad_id,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
        random_token_prob=random_token_prob,
    )

    for i in range(tokens_masked_short_padding.shape[0]):

        attention_mask_short_padding = jax_tokens_short[i] != pad_id
        attention_mask_long_padding = jax_tokens_long[i] != pad_id

        seq_length_short_padding = jnp.sum(attention_mask_short_padding)
        seq_length_long_padding = jnp.sum(attention_mask_long_padding)

        seq = tokens_masked_short_padding[i, :seq_length_short_padding]
        number_of_masked_tokens_short_padding = jnp.sum(seq == mask_id)
        expected_number_of_masked_tokens = jnp.floor(
            noising_ratio * masking_prob * seq_length_short_padding
        )

        expected_number_of_masked_tokens = jnp.floor(
            noising_ratio * masking_prob * seq_length_short_padding
        )

        seq = tokens_masked_long_padding[i, :seq_length_long_padding]

        number_of_masked_tokens_long_padding = jnp.sum(seq == mask_id)
        pytest.assume(
            jnp.sum(
                jax_tokens_short[i, :seq_length_short_padding]
                != tokens_masked_short_padding[i, :seq_length_short_padding]
            )
            <= int(noising_ratio * seq_length_short_padding) + 1,
            "Masking (including random tokens) differs from expected masking",
        )
        pytest.assume(
            jnp.sum(
                jax_tokens_long[i, :seq_length_long_padding]
                != tokens_masked_long_padding[i, :seq_length_long_padding]
            )
            <= int(noising_ratio * seq_length_short_padding) + 1,
            "Masking (including random tokens) differs from expected masking",
        )
        pytest.assume(
            number_of_masked_tokens_long_padding
            == number_of_masked_tokens_short_padding,
            "True Masking changes with padding size",
        )

        pytest.assume(
            number_of_masked_tokens_long_padding == expected_number_of_masked_tokens,
            "True Masking differs from expected true masking",
        )
        pytest.assume(
            seq_length_long_padding == seq_length_short_padding,
            "Length of sequence differs",
        )

        pytest.assume(
            (
                (jax_tokens_short[i] != pad_id)
                == (tokens_masked_short_padding[i] != pad_id)
            ).all(),
            "Pad tokens have changed",
        )
        pytest.assume(
            (
                (jax_tokens_long[i] != pad_id)
                == (tokens_masked_long_padding[i] != pad_id)
            ).all(),
            "Pad tokens have changed",
        )


@pytest.mark.parametrize(
    "sequences", [["ABCA" * 4, "DADA" * 3], ["ABCAADADACA" * 2, "JAJAJBA" * 3]]
)
def test_cross_entropy_loss(sequences: List[str]) -> None:
    """Tests if the cross entropy loss is correct on a few inputs."""

    max_sequence_length = 256
    prepend_cls_token = True

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length,
        prepend_cls_token=prepend_cls_token,
    )

    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        mask_token_id=tokenizer.mask_token_id,
        max_positions=max_sequence_length * 2,
        attention_heads=2,
        embed_dim=4,
        ffn_embed_dim=8,
        num_layers=1,
    )

    # Get forward function
    forward_fn = build_esm_fn(model_config)
    forward_fn = hk.transform(forward_fn)

    random_key = jax.random.PRNGKey(0)
    random_token_indices = jnp.asarray(
        [tokenizer.token_to_id(tok) for tok in tokenizer.standard_tokens]
    )
    random_key, subkey = jax.random.split(random_key)

    input_tokens = tokenizer.batch_tokenize(sequences=sequences)
    input_tokens = jnp.array([x[1] for x in input_tokens])

    parameters = forward_fn.init(subkey, input_tokens)

    noising_ratio = 0.5
    masking_prob = 0.25

    tokens, targets = mask_sequence_bert_style(
        tokens=input_tokens,
        random_key=subkey,
        random_token_indices=random_token_indices,
        mask_id=tokenizer.mask_token_id,
        pad_id=tokenizer.pad_token_id,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
    )

    sequence_mask = targets != tokenizer.pad_token_id
    attention_mask = build_padding_attention_mask(tokens, tokenizer.pad_token_id)
    loss, metrics = cross_entropy_loss(
        apply_fn=forward_fn.apply,
        params=parameters,
        random_key=subkey,
        tokens=tokens,
        targets=targets,
        sequence_mask=sequence_mask,
        attention_mask=attention_mask,
    )

    padded_tokens = jnp.concatenate(
        (tokens, jnp.ones(tokens.shape) * tokenizer.pad_token_id), axis=1
    ).astype(jnp.int32)
    padded_targets = jnp.concatenate(
        (targets, jnp.ones(targets.shape) * tokenizer.pad_token_id), axis=1
    ).astype(jnp.int32)
    padded_sequence_mask = padded_targets != tokenizer.pad_token_id

    padded_loss, padded_metrics = cross_entropy_loss(
        apply_fn=forward_fn.apply,
        params=parameters,
        random_key=subkey,
        tokens=padded_tokens,
        targets=padded_targets,
        sequence_mask=padded_sequence_mask,
        attention_mask=build_padding_attention_mask(
            padded_tokens, tokenizer.pad_token_id
        ),
    )
    pytest.assume(jnp.allclose(loss, padded_loss), "padding makes losses different")

    test_accuracy = metrics["accuracy"]  # type: ignore
    test_padded_accuracy = padded_metrics["accuracy"]
    pytest.assume(
        jnp.allclose(test_padded_accuracy, test_accuracy),
        "padding makes accuracies different",
    )

    test_loss = loss  # type: ignore

    logits = forward_fn.apply(
        parameters, subkey, tokens, attention_mask=attention_mask
    )[
        "logits"
    ]  # (B,L,V)
    log_probs = jax.nn.log_softmax(logits, axis=-1)
    mask = targets != tokenizer.pad_token_id  # (B,L)
    oh = jax.nn.one_hot(targets, tokenizer.vocabulary_size)
    loss = -(oh * log_probs).sum(axis=-1)
    loss = loss * mask
    loss = loss.sum(axis=-1) / mask.sum(axis=-1)
    loss = loss.mean()

    accuracy = logits.argmax(axis=-1) == targets
    accuracy = jnp.sum(accuracy * mask, axis=-1) / jnp.sum(mask, axis=-1)
    accuracy = accuracy.mean()

    pytest.assume(jnp.allclose(accuracy, test_accuracy), "Wrong accuracy")

    pytest.assume(jnp.allclose(loss, test_loss), "Wrong loss")


@pytest.mark.parametrize(
    "inputs, outputs",
    (
        (
            {
                "logits": jnp.array([[[0.5, -2, 1, -10], [4, -2, 1, 15]]]),
                "targets": jnp.array([[3, 2]]),
                "sequence_mask": None,
                "num_classes": 4,
            },
            {
                "loss": jnp.array(12.752312),
            },
        ),
        (
            {
                "logits": jnp.array([[[0.1, -2, 3], [4, -2, 1]]]),
                "targets": jnp.array([[2, 0]]),
                "sequence_mask": None,
                "num_classes": 3,
            },
            {
                "loss": jnp.array(0.05543735),
            },
        ),
    ),
)
def test_cross_entropy_loss_multilabel_classification(
    inputs: Dict[str, jnp.ndarray], outputs: Dict[str, jnp.ndarray]
) -> None:
    """
    Tests the mse_loss on a few inputs.
    """

    def dummy_network_fn(
        x: jnp.ndarray,
        unused: Optional[jnp.ndarray] = None,
        unused_2: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        return {"logits": x}

    dummy_network = hk.transform(dummy_network_fn)

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    x = inputs["logits"]
    params = dummy_network.init(subkey, x)

    targets = inputs["targets"]
    sequence_mask = inputs["sequence_mask"]
    loss, _ = cross_entropy_loss_multilabel_classification(
        params=params,
        random_key=subkey,
        tokens=x,
        targets=targets,
        forward_fn=dummy_network.apply,
        num_classes=inputs["num_classes"],
        sequence_mask=sequence_mask,
        attention_mask=None,
    )

    pytest.assume(jnp.array_equal(loss, outputs["loss"]))


@pytest.mark.parametrize(
    "inputs, outputs",
    (
        (
            {
                "logits": jnp.array([[[0.1, -2, 1], [0.01, -2, 0.2]]]),
                "targets": jnp.array([[[0, 0.9, 0.1], [0.1, 0.9, 0]]]),
                "sequence_mask": None,
            },
            {
                "loss": jnp.array(0.95607865),
            },
        ),
        (
            {
                "logits": jnp.array([[[0.5, -2, 1, -10], [4, -2, 1, 15]]]),
                "targets": jnp.array([[[0, 0.8, 0.2, 0], [0.4, 0.6, 0, 0]]]),
                "sequence_mask": None,
            },
            {
                "loss": jnp.array(2.188078),
            },
        ),
    ),
)
def test_cross_entropy_loss_multiregression(
    inputs: Dict[str, jnp.ndarray], outputs: Dict[str, jnp.ndarray]
) -> None:
    """
    Tests the mse_loss on a few inputs.
    """

    def dummy_network_fn(
        tokens: jnp.ndarray,
        attention_mask: Optional[jnp.ndarray] = None,
        sequence_mask: Optional[jnp.ndarray] = None,
    ) -> TransformerOutput:
        return {"logits": x}

    dummy_network = hk.transform(dummy_network_fn)

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    x = inputs["logits"]
    params = dummy_network.init(subkey, x)

    targets = inputs["targets"]
    sequence_mask = inputs["sequence_mask"]
    loss, _ = cross_entropy_loss_multiregression(
        params=params,
        random_key=subkey,
        tokens=x,
        targets=targets,
        forward_fn=dummy_network.apply,
        sequence_mask=sequence_mask,
        attention_mask=None,
    )

    pytest.assume(jnp.array_equal(loss, outputs["loss"]))


@pytest.mark.parametrize(
    "encoder_seqs", [["ABCA" * 4, "DADA" * 4], ["ABCAADAA" * 1, "JAJAJBA" * 2]]
)
@pytest.mark.parametrize(
    "decoder_seqs", [["CCAA" * 4, "BBBB" * 4], ["ABCAADDD" * 1, "JJJJJJJ" * 2]]
)
def test_cross_entropy_loss_encoder_decoder(
    encoder_seqs: List[str], decoder_seqs: List[str]
) -> None:
    """
    Tests if the cross entropy loss for encoder decoder models
    is correct on a few inputs.
    """

    max_sequence_length = 40
    prepend_cls_token = True

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length + int(prepend_cls_token),
        prepend_cls_token=prepend_cls_token,
    )

    encoder_config = EncoderConfig(
        alphabet_size=tokenizer.vocabulary_size,
        max_positions=max_sequence_length + int(prepend_cls_token),
        num_attention_heads=2,
        embed_dim=4,
        ffn_embed_dim=4,
        num_layers=1,
    )

    decoder_config = DecoderConfig(
        alphabet_size=tokenizer.vocabulary_size,
        max_positions=max_sequence_length + int(prepend_cls_token),
        num_attention_heads=2,
        embed_dim=4,
        ffn_embed_dim=4,
        num_layers=1,
    )

    # Get forward function
    forward_fn = build_encoder_decoder_fn(
        encoder_config, decoder_config, mixed_precision=False
    )
    forward_fn = hk.transform(forward_fn)

    random_key = jax.random.PRNGKey(0)
    random_token_indices = jnp.asarray(
        [tokenizer.token_to_id(tok) for tok in tokenizer.standard_tokens]
    )
    random_key, subkey = jax.random.split(random_key)

    encoder_tokens = tokenizer.batch_tokenize(sequences=encoder_seqs)
    encoder_tokens = jnp.array([x[1] for x in encoder_tokens])

    decoder_tokens = tokenizer.batch_tokenize(sequences=encoder_seqs)
    decoder_tokens = jnp.array([x[1] for x in decoder_tokens])

    parameters = forward_fn.init(subkey, encoder_tokens, decoder_tokens)

    noising_ratio = 0.15
    masking_prob = 0.8
    targets = get_causal_labels(
        decoder_tokens, tokenizer.eos_token_id, tokenizer.pad_token_id
    )

    sequence_mask = decoder_tokens != tokenizer.pad_token_id

    decoder_tokens, bert_targets = mask_sequence_bert_style(
        tokens=decoder_tokens,
        random_key=random_key,
        random_token_indices=random_token_indices,
        mask_id=tokenizer.mask_token_id,
        pad_id=tokenizer.pad_token_id,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
    )
    denoising_mask = bert_targets != tokenizer.pad_token_id

    loss, metrics = cross_entropy_loss_encoder_decoder(
        forward_fn=forward_fn.apply,
        params=parameters,
        random_key=subkey,
        encoder_tokens=encoder_tokens,
        decoder_tokens=decoder_tokens,
        targets=targets,
        decoder_sequence_mask=sequence_mask,
        denoising_sequence_mask=denoising_mask,
    )

    test_accuracy = metrics["accuracy"]  # type: ignore
    test_bert_accuracy = metrics["bert_accuracy"]  # type: ignore
    test_causal_accuracy = metrics["causal_accuracy"]  # type:ignore
    test_loss = loss

    _, decoder_outs = forward_fn.apply(
        parameters, subkey, encoder_tokens, decoder_tokens
    )
    logits = decoder_outs["logits"]  # (B,L,V)

    log_probs = jax.nn.log_softmax(logits, axis=-1)
    oh = jax.nn.one_hot(targets, tokenizer.vocabulary_size)
    loss = -(oh * log_probs).sum(axis=-1)

    batch_size, sequence_length = decoder_tokens.shape[:2]
    loss = loss.reshape(batch_size, sequence_length)
    loss = jnp.mean(loss * sequence_mask, axis=-1)
    loss = jnp.mean(loss)

    accuracy = logits.argmax(axis=-1) == targets
    accuracy = jnp.sum(accuracy * sequence_mask, axis=-1) / jnp.sum(
        sequence_mask, axis=-1
    )
    accuracy = accuracy.mean()

    bert_accuracy = logits.argmax(axis=-1) == targets
    bert_accuracy = jnp.sum(
        bert_accuracy * sequence_mask * denoising_mask, axis=-1
    ) / jnp.sum(sequence_mask * denoising_mask, axis=-1)
    bert_accuracy = bert_accuracy.mean()

    causal_accuracy = logits.argmax(axis=-1) == targets
    causal_accuracy = jnp.sum(
        causal_accuracy * sequence_mask * ~denoising_mask, axis=-1
    ) / jnp.sum(sequence_mask * ~denoising_mask, axis=-1)
    causal_accuracy = causal_accuracy.mean()

    pytest.assume(jnp.allclose(accuracy, test_accuracy), "Wrong accuracy")
    pytest.assume(jnp.allclose(bert_accuracy, test_bert_accuracy), "Wrong accuracy")
    pytest.assume(jnp.allclose(causal_accuracy, test_causal_accuracy), "Wrong accuracy")
    pytest.assume(jnp.allclose(test_loss, loss), "Wrong loss")


@pytest.mark.parametrize(
    "decoder_seqs", [["CCAA" * 4, "BBBB" * 4], ["ABCAADDD" * 1, "JJJJJJJ" * 2]]
)
@pytest.mark.parametrize("targets", [["A", "B"], ["J", "J" * 4]])
def test_cross_entropy_loss_decoder_only(
    decoder_seqs: List[str],
    targets: List[str],
) -> None:
    """
    Tests if the cross entropy loss for decoder only models
    is correct on a few inputs.
    """
    max_sequence_length = 40
    prepend_cls_token = False

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length,
        prepend_cls_token=prepend_cls_token,
    )

    decoder_config = T5DecoderConfig(
        vocab_size=tokenizer.vocabulary_size,
        num_heads=2,
        embed_dim=4,
        ffn_embed_dim=4,
        num_layers=1,
    )
    forward_fn = build_t5_decoder_only(decoder_config)
    # Get forward function
    forward_fn = hk.transform(forward_fn)

    random_key = jax.random.PRNGKey(0)
    random_token_indices = jnp.asarray(
        [tokenizer.token_to_id(tok) for tok in tokenizer.standard_tokens]
    )
    random_key, subkey = jax.random.split(random_key)

    decoder_tokens = tokenizer.batch_tokenize(sequences=decoder_seqs)
    target_tokens = tokenizer.batch_tokenize(sequences=targets)

    decoder_tokens = jnp.array([x[1] for x in decoder_tokens])
    target_tokens = jnp.array([x[1] for x in target_tokens])
    parameters = forward_fn.init(subkey, decoder_tokens)

    noising_ratio = 0.15
    masking_prob = 0.8

    sequence_mask = decoder_tokens != tokenizer.pad_token_id

    decoder_tokens, bert_targets = mask_sequence_bert_style(
        tokens=decoder_tokens,
        random_key=random_key,
        random_token_indices=random_token_indices,
        mask_id=tokenizer.mask_token_id,
        pad_id=tokenizer.pad_token_id,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
    )
    denoising_mask = bert_targets != tokenizer.pad_token_id

    loss, metrics = cross_entropy_loss_decoder_only(
        forward_fn=forward_fn.apply,
        params=parameters,
        random_key=subkey,
        decoder_tokens=decoder_tokens,
        targets=target_tokens,
        decoder_sequence_mask=sequence_mask,
        denoising_sequence_mask=denoising_mask,
    )

    test_accuracy = metrics["accuracy"]  # type: ignore
    test_bert_accuracy = metrics["bert_accuracy"]  # type: ignore
    test_causal_accuracy = metrics["causal_accuracy"]  # type:ignore
    test_loss = loss

    decoder_outs = forward_fn.apply(parameters, subkey, decoder_tokens)
    logits = decoder_outs["logits"]  # (B,L,V)

    log_probs = jax.nn.log_softmax(logits, axis=-1)
    oh = jax.nn.one_hot(target_tokens, tokenizer.vocabulary_size)
    loss = -(oh * log_probs).sum(axis=-1)

    batch_size, sequence_length = decoder_tokens.shape[:2]
    loss = loss.reshape(batch_size, sequence_length)
    loss = jnp.mean(loss * sequence_mask, axis=-1)
    loss = jnp.mean(loss)

    accuracy = logits.argmax(axis=-1) == target_tokens
    accuracy = jnp.sum(accuracy * sequence_mask, axis=-1) / jnp.sum(
        sequence_mask, axis=-1
    )
    accuracy = accuracy.mean()

    bert_accuracy = logits.argmax(axis=-1) == target_tokens
    bert_accuracy = jnp.sum(
        bert_accuracy * sequence_mask * denoising_mask, axis=-1
    ) / jnp.sum(sequence_mask * denoising_mask, axis=-1)
    bert_accuracy = bert_accuracy.mean()

    causal_accuracy = logits.argmax(axis=-1) == target_tokens
    causal_accuracy = jnp.sum(
        causal_accuracy * sequence_mask * ~denoising_mask, axis=-1
    ) / jnp.sum(sequence_mask * ~denoising_mask, axis=-1)
    causal_accuracy = causal_accuracy.mean()

    pytest.assume(jnp.allclose(accuracy, test_accuracy), "Wrong accuracy")
    pytest.assume(jnp.allclose(bert_accuracy, test_bert_accuracy), "Wrong accuracy")
    pytest.assume(jnp.allclose(causal_accuracy, test_causal_accuracy), "Wrong accuracy")
    pytest.assume(jnp.allclose(test_loss, loss), "Wrong loss")


@pytest.mark.parametrize("prefix", [0, 2])
def test_get_causal_labels(prefix: int) -> None:
    """
    Tests if the correct causal labels are returned given a prefix or not.
    """

    sequences = ["ABDBDBDB", "CDADAD"]

    tokenizer = StandardTokenizer(list("ABCDE"))
    tokens = jnp.array(list(zip(*tokenizer.batch_tokenize(sequences)))[1])

    labels = get_causal_labels(
        tokens, tokenizer.eos_token_id, tokenizer.pad_token_id, prefix=prefix
    )

    batch_size = tokens.shape[0]

    if prefix > 0:
        expected_labels = jnp.concatenate(
            [
                jnp.full((batch_size, prefix), tokenizer.pad_token_id),
                tokens[:, prefix + 1 :],
                jnp.full((batch_size, 1), tokenizer.eos_token_id),
            ],
            axis=1,
        )

    else:
        expected_labels = jnp.concatenate(
            [tokens[:, 1:], jnp.full((batch_size, 1), tokenizer.eos_token_id)], axis=1
        )

    pytest.assume(jnp.allclose(expected_labels, labels))


@pytest.mark.parametrize(
    "inputs, outputs",
    (
        (
            {
                "predictions": jnp.array([[0, 0, 2, 1]]),
                "targets": jnp.array([[0, 0, 1, 0]]),
                "sequence_mask": None,
            },
            {
                "loss": jnp.array(0.25),
            },
        ),
        (
            {
                "predictions": jnp.array([[0, 0, 2, 1], [1, 1, 0, 1]]),
                "targets": jnp.array([[0, 0, 1, 0], [0, 0, 1, 0]]),
                "sequence_mask": None,
            },
            {
                "loss": jnp.array(3 / 8),
            },
        ),
        (
            {
                "predictions": jnp.array([[0, 0, 2, 1]]),
                "targets": jnp.array([[0, 0, 1, 0]]),
                "sequence_mask": jnp.array([[1, 1, 1, 0]]),
            },
            {
                "loss": jnp.array(1 / 6),
            },
        ),
    ),
)
def test_mse_loss(
    inputs: Dict[str, jnp.ndarray], outputs: Dict[str, jnp.ndarray]
) -> None:
    """
    Tests the mse_loss on a few inputs.
    """

    def dummy_network_fn(
        x: jnp.ndarray, unused: Optional[jnp.ndarray] = None
    ) -> TransformerOutput:
        return {"predictions": x}

    dummy_network = hk.transform(dummy_network_fn)

    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    x = inputs["predictions"]
    params = dummy_network.init(subkey, x)

    targets = inputs["targets"]
    sequence_mask = inputs["sequence_mask"]
    loss, metrics = mse_loss(
        params=params,
        random_key=subkey,
        tokens=x,
        targets=targets,
        forward_fn=dummy_network.apply,
        sequence_mask=sequence_mask,
        attention_mask=None,
    )

    pytest.assume(jnp.array_equal(loss, outputs["loss"]))
