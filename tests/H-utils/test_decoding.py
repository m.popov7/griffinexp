"""
Tests for utils used for autoregressive generation with decoder LMs.
"""
import functools

import haiku as hk
import jax
import jax.numpy as jnp
import pytest
from transformers import AutoTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.utils.decoding import (
    decode_greedy,
    decode_temperature_sampling,
    update_tokens_ids_greedy,
    update_tokens_ids_temperature_sampling,
)


@pytest.mark.parametrize("seed", [0, 1])
def test_greedy_decoding(seed: int) -> None:
    """
    Test greedy decoding with small GPT model.
    """
    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=32,
        ffn_embed_dim=32 * 4,
        num_heads=4,
        num_layers=2,
        rope_dimensions=4,
        max_position_embeddings=512,
        add_bias_ffn=True,
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=True,
        norm_type="layer_norm",
        parallel_attention_ff=True,
        use_gradient_checkpointing=False,
    )
    gptj_fn = build_gpt_fn(config)
    gptj_fn = hk.transform(gptj_fn)

    inputs = [
        "What is the difference between a gene and a protein?",
        "How many amino-acids are there?",
    ]
    inputs_tokens_ids = jnp.asarray(tokenizer(inputs, padding=True)["input_ids"])

    random_key = jax.random.PRNGKey(seed)
    params = gptj_fn.init(random_key, inputs_tokens_ids[:1, :1])

    output_length = 32
    update_tokens_fn_greedy = jax.jit(
        functools.partial(update_tokens_ids_greedy, apply_fn=gptj_fn.apply)
    )

    # manual decoding
    batch_size, inputs_length = inputs_tokens_ids.shape
    num_tokens_to_decode = output_length - inputs_length
    fill_tokens = jnp.full(
        shape=(batch_size, num_tokens_to_decode),
        fill_value=tokenizer.eos_token_id,
    )
    init_tokens_ids = jnp.concatenate([inputs_tokens_ids, fill_tokens], axis=-1)

    tokens_ids = init_tokens_ids.copy()
    for i in range(num_tokens_to_decode):
        time_step = jnp.asarray(i + inputs_length - 1)
        tokens_ids, random_key = update_tokens_fn_greedy(
            tokens_ids=tokens_ids,
            random_key=random_key,
            params=params,
            time_step=time_step,
        )
    decoded_outputs = tokenizer.batch_decode(tokens_ids)

    # compare with function using scan
    random_key = jax.random.PRNGKey(seed)
    decode_greedy_fn = jax.jit(
        functools.partial(
            decode_greedy,
            apply_fn=gptj_fn.apply,
            num_tokens_to_decode=num_tokens_to_decode,
            eos_token_id=tokenizer.eos_token_id,
        )
    )
    tokens_ids, random_key = decode_greedy_fn(inputs_tokens_ids, random_key, params)
    decoded_outputs_scan = tokenizer.batch_decode(tokens_ids)
    pytest.assume(
        decoded_outputs_scan == decoded_outputs,
        "scan and manual for loop produce different outputs",
    )


@pytest.mark.parametrize("temperature", [0.25, 1.0])
def test_sampling_decoding(temperature: float) -> None:
    """
    Test decoding using temperature sampling with small GPT model.
    """
    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=32,
        ffn_embed_dim=32 * 4,
        num_heads=4,
        num_layers=2,
        rope_dimensions=4,
        max_position_embeddings=512,
        add_bias_ffn=True,
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=True,
        norm_type="layer_norm",
        parallel_attention_ff=True,
        use_gradient_checkpointing=False,
    )
    gptj_fn = build_gpt_fn(config)
    gptj_fn = hk.transform(gptj_fn)

    inputs = [
        "What is the difference between a gene and a protein?",
        "How many amino-acids are there?",
    ]
    inputs_tokens_ids = jnp.asarray(tokenizer(inputs, padding=True)["input_ids"])

    seed = 0
    random_key = jax.random.PRNGKey(seed)
    params = gptj_fn.init(random_key, inputs_tokens_ids[:1, :1])

    output_length = 32
    update_tokens_fn_sampling = jax.jit(
        functools.partial(
            update_tokens_ids_temperature_sampling,
            apply_fn=gptj_fn.apply,
            temperature=temperature,
        )
    )

    # manual decoding
    batch_size, inputs_length = inputs_tokens_ids.shape
    num_tokens_to_decode = output_length - inputs_length
    fill_tokens = jnp.full(
        shape=(batch_size, num_tokens_to_decode),
        fill_value=tokenizer.eos_token_id,
    )
    init_tokens_ids = jnp.concatenate([inputs_tokens_ids, fill_tokens], axis=-1)

    tokens_ids = init_tokens_ids.copy()
    for i in range(num_tokens_to_decode):
        time_step = jnp.asarray(i + inputs_length - 1)
        tokens_ids, random_key = update_tokens_fn_sampling(
            tokens_ids=tokens_ids,
            random_key=random_key,
            params=params,
            time_step=time_step,
        )
    decoded_outputs = tokenizer.batch_decode(tokens_ids)

    # compare with function using scan
    decode_sampling_fn = jax.jit(
        functools.partial(
            decode_temperature_sampling,
            apply_fn=gptj_fn.apply,
            num_tokens_to_decode=num_tokens_to_decode,
            eos_token_id=tokenizer.eos_token_id,
            temperature=temperature,
        )
    )
    random_key = jax.random.PRNGKey(seed)
    tokens_ids, random_key = decode_sampling_fn(inputs_tokens_ids, random_key, params)
    decoded_outputs_scan = tokenizer.batch_decode(tokens_ids)
    pytest.assume(
        decoded_outputs_scan == decoded_outputs,
        "scan and manual for loop produce different outputs",
    )
