import jax
import jax.numpy as jnp
import pytest

from trix.utils.rl import (
    chunk_trajectory_tokens,
    compute_inverse_mu_law,
    compute_mu_law,
    get_mask_from_dones,
)


@pytest.mark.parametrize(
    "dones, mask",
    [
        (jnp.array([0, 0, 0, 1]), jnp.array([1, 1, 1, 1])),
        (jnp.array([0, 1, 0, 1]), jnp.array([1, 1, 0, 0])),
        (jnp.array([0, 1, 0, 0]), jnp.array([1, 1, 0, 0])),
        (jnp.array([0, 0, 0, 0]), jnp.array([1, 1, 1, 1])),
    ],
)
def test_get_mask_from_dones(dones: jnp.ndarray, mask: jnp.ndarray) -> None:
    """
    Tests on a few input-output pairs if the generated mask is valid.
    """
    mask_from_dones = get_mask_from_dones(dones)
    pytest.assume(jnp.array_equal(mask, mask_from_dones))


@pytest.mark.parametrize("mu, m", [(100, 256), (20, 512)])
def test_mu_law(mu: float, m: float) -> None:
    """
    Tests if the mu-law can be properly reconstructed.
    """
    random_key = jax.random.PRNGKey(0)
    x = jax.random.normal(random_key, shape=(5,)) * 10
    c = compute_mu_law(x)
    x_recons = compute_inverse_mu_law(c)

    pytest.assume(jnp.allclose(x_recons, x))


def test_chunk_trajectory_tokens() -> None:
    """
    Tests if the generated chunks have the proper shape. Tests on dummy inputs if
    the chunks are consistent.
    """

    tokens = jnp.array([[0, 1, 2, 3, 4, 5]])
    positions = jnp.array([[0, 0, 1, 1, 2, 2]])
    tokens = {"tokens": tokens, "positions": positions}
    num_trajectories = tokens["tokens"].shape[0]
    random_key = jax.random.PRNGKey(0)
    random_key, subkey = jax.random.split(random_key)

    horizon_size = 2
    concat_size = 2
    num_chunks_per_trajectory = 5

    chunks = chunk_trajectory_tokens(
        tokens,
        subkey,
        horizon_size=horizon_size,
        concat_size=concat_size,
        num_chunks_per_trajectory=num_chunks_per_trajectory,
    )

    jax.tree_map(
        lambda x: pytest.assume(
            x.shape[0] == (num_chunks_per_trajectory * num_trajectories)
        ),
        chunks,
    )

    for chunk in chunks["tokens"]:

        expected_chunk = jnp.arange(start=chunk[0], stop=chunk[0] + len(chunk))
        pytest.assume(jnp.all(chunk == expected_chunk))

    for chunk in chunks["positions"]:
        expected_chunk = jnp.repeat(
            jnp.arange(start=chunk[0], stop=chunk[0] + horizon_size), concat_size
        )
        pytest.assume(jnp.all(chunk == expected_chunk))
