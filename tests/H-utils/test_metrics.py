import jax.numpy as jnp
import pytest

from trix.utils.metrics import compute_accuracy


def test_compute_accuracy() -> None:
    y_true = jnp.array([0, 1, 2, 1, 2, 1])
    y_pred = jnp.array([0, 2, 1, 1, 2, 1])

    pytest.assume(compute_accuracy(y_true, y_pred) == 4 / 6)
