import jax
import jax.numpy as jnp
import pytest

from trix.utils.supervised import compute_binned_labels


@pytest.mark.parametrize("n_bins", [8])
def test_compute_binned_labels(n_bins: int) -> None:
    random_key = jax.random.PRNGKey(0)
    labels = jax.random.normal(random_key, shape=(100, 8))
    labels_binned, bins = compute_binned_labels(labels=labels, n_bins=n_bins, eps=1)
    pytest.assume(
        labels_binned.shape == (labels.shape + (n_bins + 1,)),
        "binned_labels does not have the right shape",
    )
    reconstructed_labels = jnp.sum(labels_binned * bins, axis=-1)
    pytest.assume(
        jnp.allclose(labels, reconstructed_labels),
        "cannot reconstruct the right labels from binned labels",
    )
