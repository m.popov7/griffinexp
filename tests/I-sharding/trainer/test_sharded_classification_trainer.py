import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.sequence_datasets import LabeledSequenceDataset
from trix.layers.heads.classification_head import SimpleClassificationHead
from trix.models.esm.finetuning import build_esm_with_head_fn
from trix.models.esm.model import ESMTransformerConfig
from trix.models.esm.sharding.finetuning import build_sharded_esm_with_head_fn
from trix.models.esm.sharding.utils.unsharding import (
    rename_params_to_unsharded,
    unshard_esm,
)
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.losses import cross_entropy_loss_classification
from trix.training.supervised_trainers.classification_trainer import (
    ClassificationTrainer,
)
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize("num_cycles", [10])
@pytest.mark.parametrize("num_shards", [2])
def test_training_state(num_shards: int, num_cycles: int) -> None:
    """
    Initializes a sharded esm model with classification head on top of it model.
    Performs num_cycles cycles of gradient accumulations with this sharded model.
    Initializes a non sharded esm model with classification head on top of it with the
    same parameters as the sharded one. Performs num_cycles cycles of gradient
    accumulations with this non sharded model.

    Compares the difference between the two resulting set of parameters.
    """

    # Part common to both sharded and non sharded trainers
    max_sequence_length = 10

    effective_batch_size = 4
    batch_size = 1

    seed = 0

    prepend_cls_token = True
    prepend_bos_token = False

    # Number of times we are going to apply gradients in the test
    learning_rate = 1e-3

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length
        + int(prepend_cls_token)
        + int(prepend_bos_token),
        prepend_cls_token=prepend_cls_token,
        prepend_bos_token=prepend_bos_token,
    )

    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        max_positions=max_sequence_length + int(prepend_cls_token),
        attention_heads=2,
        embed_dim=16,
        ffn_embed_dim=8,
        num_layers=1,
        mask_token_id=tokenizer.mask_token_id,
        lm_head="simple",
    )

    ###########################################
    # Perform training steps with sharded model
    ###########################################
    # Initialize on devices
    devices = jax.local_devices()
    num_devices = len(devices)
    devices_mesh = np.array(devices).reshape((-1, num_shards))

    num_data_parallel_ways = num_devices // num_shards
    num_acc_grads = effective_batch_size // (batch_size * (num_devices // num_shards))

    sequences = ["XYQRTLALSC"] * 100
    labels = [0] * 50 + [1] * 50

    # get datasets
    train_dataset = LabeledSequenceDataset(
        sequences=sequences,
        labels=labels,
        tokenizer=tokenizer,
        batch_size=num_data_parallel_ways * num_acc_grads * batch_size,
        shuffle=False,
        drop_last=True,
    )

    num_classes = 2

    # The classification head is wrapped inside head_fn because
    # haiku modules cannot be instantiated outside hk.transform.
    def head_fn() -> hk.Module:
        return SimpleClassificationHead(num_classes=num_classes)

    # Get fine-tunable model
    finetune_forward_fn = build_sharded_esm_with_head_fn(
        model_config=model_config,
        head_fn=head_fn,
        compute_dtype=jnp.float32,
        num_shards=num_shards,
    )
    finetune_forward_fn = hk.transform(finetune_forward_fn)

    # get optimizer
    optimizer = optax.chain(
        optax.clip(1),
        optax.adam(learning_rate=learning_rate),
        # optax.sgd(learning_rate=learning_rate),
    )

    trainer_sharded = ClassificationTrainer(
        forward_fn=finetune_forward_fn,
        loss_fn=cross_entropy_loss_classification,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=2,
    )  # type: ignore

    random_key = jax.random.PRNGKey(0)
    keys = jnp.tile(random_key, reps=(num_shards, num_data_parallel_ways, 1))
    dummy_tokens, _ = next(train_dataset.get_epoch_batches())

    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        training_state = trainer_sharded.build_init_fn()(keys, dummy_tokens[0][None, :])

    training_state_init_cpu = jax.tree_map(lambda x: x[:, 0], training_state)
    params_init = {
        k: v for k, v in training_state_init_cpu.params.items() if "esm" in k
    }
    params_init = unshard_esm(params_init)
    params_init = rename_params_to_unsharded(params_init)
    params_init["simple_classification_head/~/fc"] = jax.tree_map(
        lambda x: x[0],
        training_state_init_cpu.params["simple_classification_head/~/fc"],
    )

    generator = train_dataset.get_epoch_batches()
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        # Build xmapped update function
        update_fn = trainer_sharded.build_xmapped_update_fn()
        for _ in range(num_cycles):
            tokens, labels = next(generator)
            tokens = jnp.reshape(
                tokens,
                (
                    num_data_parallel_ways,
                    num_acc_grads,
                    batch_size,
                    max_sequence_length + int(prepend_cls_token),
                ),
            )
            labels = jnp.reshape(
                labels,
                (num_data_parallel_ways, num_acc_grads, batch_size),
            )
            # Update neural network and optimizer
            training_state, _ = update_fn(training_state, tokens, labels)

    training_state_cpu = jax.tree_map(lambda x: x[:, 0], training_state)
    params_sharded_after_updates = {
        k: v for k, v in training_state_cpu.params.items() if "esm" in k
    }

    params_unsharded_after_updates = unshard_esm(params_sharded_after_updates)
    params_unsharded_after_updates = rename_params_to_unsharded(
        params_unsharded_after_updates
    )
    params_unsharded_after_updates["simple_classification_head/~/fc"] = jax.tree_map(
        lambda x: x[0], training_state_cpu.params["simple_classification_head/~/fc"]
    )

    ###########################################
    # Perform training steps with non sharded
    # model. The init parameters are taken
    # the same as with the sharded model.
    ###########################################

    # Initialize on devices
    devices = jax.local_devices()[:1]
    num_devices = len(devices)

    num_acc_grads = effective_batch_size // (batch_size * (num_devices))

    # Get fine-tunable model
    finetune_forward_fn = build_esm_with_head_fn(
        model_config=model_config,
        head_fn=head_fn,
        compute_dtype=jnp.float32,
    )
    finetune_forward_fn = hk.transform(finetune_forward_fn)

    # # get optimizer
    optimizer = optax.chain(
        optax.clip(1),
        optax.adam(learning_rate=learning_rate),
        # optax.sgd(learning_rate=learning_rate),
    )

    trainer_non_sharded = ClassificationTrainer(
        forward_fn=finetune_forward_fn,
        loss_fn=cross_entropy_loss_classification,
        tokenizer=tokenizer,
        optimizer=optimizer,
        num_classes=2,
    )  # type: ignore

    keys = jax.device_put_replicated(jax.random.PRNGKey(seed), devices=devices)
    dummy_tokens, dummy_labels = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )
    dummy_tokens = dummy_tokens[:, 0, :, :]

    params_init = jax.tree_map(lambda x: x[None, :], params_init)

    training_state = jax.pmap(
        trainer_non_sharded.init, devices=devices, axis_name="batch"
    )(keys, dummy_tokens, params_init)

    update_fn = jax.pmap(trainer_non_sharded.update, devices=devices, axis_name="batch")

    # run train epoch
    generator = train_dataset.get_epoch_batches()

    for _ in range(num_cycles):
        tokens, labels = next(generator)
        tokens = jnp.reshape(
            tokens,
            (
                num_devices,
                num_acc_grads,
                batch_size,
                max_sequence_length + int(prepend_cls_token),
            ),
        )
        labels = jnp.reshape(
            labels,
            (num_devices, num_acc_grads, batch_size),
        )

        training_state, _ = update_fn(training_state, tokens, labels)

        training_state_cpu = jax.tree_map(lambda x: x[0], training_state)
        params_non_sharded_after_updates = training_state_cpu.params

        # Assert that the parameters obtained after the updates are the same with the
        # sharded and unsharded model.
        pytest.assume(
            all(
                jax.tree_util.tree_leaves(
                    jax.tree_map(
                        lambda x, y: np.allclose(x, y, atol=1e-2),
                        params_unsharded_after_updates,
                        params_non_sharded_after_updates,
                    )
                )
            )
        )
