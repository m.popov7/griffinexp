import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.dataloaders.language_models.sequence_datasets import SequencesDataset
from trix.models.esm.model import ESMTransformerConfig, build_esm_fn
from trix.models.esm.sharding.sharded_esm import (
    ESMTransformerConfig as ESMTransformerConfigSharded,
)
from trix.models.esm.sharding.sharded_esm import build_sharded_esm_fn
from trix.models.esm.sharding.utils.unsharding import (
    rename_params_to_unsharded,
    unshard_esm,
)
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.language_trainers.encoder_lm_trainer import EncoderMLMTrainer
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize("num_cycles", [10])
@pytest.mark.parametrize("num_shards", [2])
def test_training_state(num_shards: int, num_cycles: int) -> None:
    """
    Initializes a sharded ESM model. Performs num_cycles cycles of gradient
    accumulations with this sharded model.
    Initializes a non sharded ESM model with the same parameters as the sharded one.
    Performs num_cycles cycles of gradient accumulations with this non sharded model.

    Compares the difference between the two resulting set of parameters.
    """

    # Part common to both sharded and non sharded trainers
    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 150

    effective_batch_size = 4
    batch_size = 1

    noising_ratio = 0.15
    masking_prob = 0.8

    seed = 0

    prepend_cls_token = True
    prepend_bos_token = False

    # Number of times we are going to apply gradients in the test
    num_cycles = 10
    learning_rate = 1e-3

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length
        + int(prepend_cls_token)
        + int(prepend_bos_token),
        prepend_cls_token=prepend_cls_token,
        prepend_bos_token=prepend_bos_token,
    )

    model_config = ESMTransformerConfigSharded(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        max_positions=max_sequence_length + int(prepend_cls_token),
        attention_heads=2,
        embed_dim=16,
        ffn_embed_dim=8,
        num_layers=1,
        mask_token_id=tokenizer.mask_token_id,
        lm_head="simple",
    )

    ###########################################
    # Perform training steps with sharded model
    ###########################################
    # Initialize on devices
    devices = jax.local_devices()
    num_devices = len(devices)
    devices_mesh = np.array(devices).reshape((-1, num_shards))

    num_data_parallel_ways = num_devices // num_shards
    num_acc_grads = effective_batch_size // (batch_size * (num_devices // num_shards))

    # Get Datasets
    train_dataset = SequencesDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed,
        ),
        tokenizer=tokenizer,
        batch_size=num_data_parallel_ways * num_acc_grads,
        shuffle=False,
        drop_last=True,
    )

    model_config = ESMTransformerConfig(
        alphabet_size=tokenizer.vocabulary_size,
        pad_token_id=tokenizer.pad_token_id,
        max_positions=max_sequence_length + int(prepend_cls_token),
        attention_heads=2,
        embed_dim=16,
        ffn_embed_dim=8,
        num_layers=1,
        mask_token_id=tokenizer.mask_token_id,
        lm_head="simple",
    )

    # Get forward function
    forward_fn = build_sharded_esm_fn(model_config, num_shards=num_shards)
    forward_fn = hk.transform(forward_fn)

    # get optimizer
    optimizer = optax.chain(
        optax.clip(1),
        optax.adam(learning_rate=learning_rate),
    )

    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    trainer_sharded = EncoderMLMTrainer(
        init_fn=forward_fn.init,
        apply_fn=forward_fn.apply,
        tokenizer=tokenizer,
        optimizer=optimizer,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
    )

    random_key = jax.random.PRNGKey(0)
    keys = jnp.tile(random_key, reps=(num_shards, num_data_parallel_ways, 1))
    dummy_tokens = next(train_dataset.get_epoch_batches())
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        training_state = trainer_sharded.build_init_fn()(keys, dummy_tokens[0][None, :])

    training_state_init_cpu = jax.tree_map(lambda x: x[:, 0], training_state)
    params_init = unshard_esm(training_state_init_cpu.params)
    params_init = rename_params_to_unsharded(params_init)

    # save_params(params_init, "params_init.joblib")

    generator = train_dataset.get_epoch_batches()
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        # Build xmapped update function
        update_fn = trainer_sharded.build_xmapped_update_fn()
        for _ in range(num_cycles):
            tokens = next(generator)
            tokens = jnp.reshape(
                tokens,
                (
                    num_data_parallel_ways,
                    num_acc_grads,
                    batch_size,
                    max_sequence_length + int(prepend_cls_token),
                ),
            )
            for i in range(num_acc_grads):
                # Update neural network and optimizer
                training_state, _ = update_fn(training_state, tokens[:, i, :, :])

    training_state_cpu = jax.tree_map(lambda x: x[:, 0], training_state)
    params_sharded_after_updates = unshard_esm(training_state_cpu.params)
    params_sharded_after_updates = rename_params_to_unsharded(
        params_sharded_after_updates
    )

    ###########################################
    # Perform training steps with non sharded
    # model. The init parameters are taken
    # the same as with the sharded model.
    ###########################################
    # Initialize on devices
    devices = jax.local_devices()[:1]
    num_devices = len(devices)

    num_acc_grads = effective_batch_size // (batch_size * (num_devices))

    # Get forward function
    forward_fn = build_esm_fn(model_config)
    forward_fn = hk.transform(forward_fn)

    # # get optimizer
    optimizer = optax.chain(
        optax.clip(1),
        optax.adam(learning_rate=learning_rate),
    )

    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    trainer_non_sharded = EncoderMLMTrainer(
        init_fn=forward_fn.init,
        apply_fn=forward_fn.apply,
        tokenizer=tokenizer,
        optimizer=optimizer,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
    )

    keys = jax.device_put_replicated(jax.random.PRNGKey(seed), devices=devices)
    dummy_tokens = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )
    dummy_tokens = dummy_tokens[:, 0, :, :]

    params_init = jax.tree_map(lambda x: x[None, :], params_init)

    training_state = jax.pmap(
        trainer_non_sharded.init, devices=devices, axis_name="batch"
    )(keys, dummy_tokens, params_init)
    update_fn = jax.pmap(trainer_non_sharded.update, devices=devices, axis_name="batch")

    # run train epoch
    generator = train_dataset.get_epoch_batches()

    for _ in range(num_cycles):
        tokens = next(generator)
        tokens = jnp.reshape(
            tokens,
            (
                num_devices,
                num_acc_grads,
                batch_size,
                max_sequence_length + int(prepend_cls_token),
            ),
        )
        for i in range(num_acc_grads):

            training_state, _ = update_fn(training_state, tokens[:, i, :, :])

    training_state_cpu = jax.tree_map(lambda x: x[0], training_state)
    params_non_sharded_after_updates = training_state_cpu.params

    # Compare the parameters from sharded and unsharded training;
    pytest.assume(
        all(
            jax.tree_util.tree_leaves(
                jax.tree_map(
                    lambda x, y: np.allclose(x, y, atol=1e-2),
                    params_sharded_after_updates,
                    params_non_sharded_after_updates,
                )
            )
        )
    )
