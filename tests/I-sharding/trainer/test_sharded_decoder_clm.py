import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.dataloaders.language_models.sequence_datasets import SequencesDataset
from trix.models.esm.sharding.utils.unsharding import rename_params_to_unsharded

# GPT
from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.models.gpt.sharding.sharded_gpt import build_sharded_gpt_fn
from trix.models.gpt.sharding.utils.unsharding import unshard_gpt
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.language_trainers.decoder_causal_lm_trainer import DecoderCLMTrainer
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize("num_cycles", [10])
@pytest.mark.parametrize("num_shards", [2])
def test_training_state(num_shards: int, num_cycles: int) -> None:
    """
    Initializes a sharded GPT model. Performs num_cycles cycles of gradient
    accumulations with this sharded model.
    Initializes a non sharded GPT model with the same parameters as the sharded one.
    Performs num_cycles cycles of gradient accumulations with this non sharded model.

    Compares the difference between the two resulting set of parameters.
    """

    # Part common to both sharded and non sharded trainers
    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 150

    effective_batch_size = 4
    batch_size = 1
    learning_rate = 1e-3

    seed = 0

    tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length + 1,
        append_eos_token=True,
    )

    # take GPT like model for CLM case
    model_config = GptConfig(
        vocab_size=tokenizer.vocabulary_size,
        eos_token_id=tokenizer.eos_token_id,
        num_heads=2,
        embed_dim=4,
        ffn_embed_dim=8,
        num_layers=1,
        rope_dimensions=2,
        max_position_embeddings=512,
        add_bias_ffn=True,
        ffn_activation_name="gelu",
        use_glu_in_ffn=False,
        add_bias_lm_head=True,
        norm_type="layer_norm",
        parallel_attention_ff=True,
        use_gradient_checkpointing=False,
    )

    ###########################################
    # Perform training steps with sharded model
    ###########################################
    # Initialize on devices
    devices = jax.local_devices()
    num_devices = len(devices)
    devices_mesh = np.array(devices).reshape((-1, num_shards))

    num_data_parallel_ways = num_devices // num_shards
    num_acc_grads = effective_batch_size // (batch_size * (num_devices // num_shards))

    # Get Datasets
    train_dataset = SequencesDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed,
        ),
        tokenizer=tokenizer,
        batch_size=num_data_parallel_ways * num_acc_grads,
        shuffle=False,
        drop_last=True,
    )

    forward_fn = build_sharded_gpt_fn(
        config=model_config, num_shards=num_shards, name="sharded_gpt_decoder"
    )
    forward_fn = hk.transform(forward_fn)

    # get optimizer
    optimizer = optax.chain(
        optax.clip(1),
        optax.adam(learning_rate=learning_rate),
    )

    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    # get trainer
    trainer_sharded = DecoderCLMTrainer(
        apply_fn=forward_fn.apply,  # type: ignore
        init_fn=forward_fn.init,  # type: ignore
        pad_token_id=tokenizer.pad_token_id,
        eos_token_id=tokenizer.eos_token_id,
        bos_token_id=tokenizer.bos_token_id,
        optimizer=optimizer,
    )

    random_key = jax.random.PRNGKey(0)
    keys = jnp.tile(random_key, reps=(num_shards, num_data_parallel_ways, 1))
    dummy_tokens = next(train_dataset.get_epoch_batches())
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        training_state = trainer_sharded.build_init_fn()(keys, dummy_tokens[0][None, :])

    training_state_init_cpu = jax.tree_map(lambda x: x[:, 0], training_state)
    params_init = unshard_gpt(training_state_init_cpu.params)
    params_init = rename_params_to_unsharded(params_init)

    generator = train_dataset.get_epoch_batches()
    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        # Build xmapped update function
        update_fn = trainer_sharded.build_xmapped_update_fn()
        for _ in range(num_cycles):
            tokens = next(generator)
            tokens = jnp.reshape(
                tokens,
                (
                    num_data_parallel_ways,
                    num_acc_grads,
                    batch_size,
                    max_sequence_length + 1,
                ),
            )
            for i in range(num_acc_grads):
                # Update neural network and optimizer
                training_state, _ = update_fn(training_state, tokens[:, i, :, :])

    training_state_cpu = jax.tree_map(lambda x: x[:, 0], training_state)
    params_sharded_after_updates = unshard_gpt(training_state_cpu.params)
    params_sharded_after_updates = rename_params_to_unsharded(
        params_sharded_after_updates
    )

    ###########################################
    # Perform training steps with non sharded
    # model. The init parameters are taken
    # the same as with the sharded model.
    ###########################################
    # Initialize on devices
    devices = jax.local_devices()[:1]
    num_devices = len(devices)

    num_acc_grads = effective_batch_size // (batch_size * (num_devices))

    # Get forward function
    forward_fn = build_gpt_fn(model_config)
    forward_fn = hk.transform(forward_fn)

    # # get optimizer
    optimizer = optax.chain(
        optax.clip(1),
        optax.adam(learning_rate=learning_rate),
    )

    optimizer = optax.MultiSteps(optimizer, every_k_schedule=num_acc_grads)

    trainer_non_sharded = DecoderCLMTrainer(
        apply_fn=forward_fn.apply,  # type: ignore
        init_fn=forward_fn.init,  # type: ignore
        pad_token_id=tokenizer.pad_token_id,
        eos_token_id=tokenizer.eos_token_id,
        bos_token_id=tokenizer.bos_token_id,
        optimizer=optimizer,
    )

    keys = jax.device_put_replicated(jax.random.PRNGKey(seed), devices=devices)
    dummy_tokens = next(train_dataset.get_epoch_batches())
    dummy_tokens = jnp.reshape(
        dummy_tokens, (num_devices, num_acc_grads, batch_size, -1)
    )
    dummy_tokens = dummy_tokens[:, 0, :, :]

    params_init = jax.tree_map(lambda x: x[None, :], params_init)

    training_state = jax.pmap(
        trainer_non_sharded.init, devices=devices, axis_name="batch"
    )(keys, dummy_tokens, params_init)

    update_fn = jax.pmap(trainer_non_sharded.update, devices=devices, axis_name="batch")

    # run train epoch
    generator = train_dataset.get_epoch_batches()

    for _ in range(num_cycles):
        tokens = next(generator)
        tokens = jnp.reshape(
            tokens,
            (
                num_devices,
                num_acc_grads,
                batch_size,
                max_sequence_length + 1,
            ),
        )
        for i in range(num_acc_grads):

            training_state, _ = update_fn(training_state, tokens[:, i, :, :])

    training_state_cpu = jax.tree_map(lambda x: x[0], training_state)
    params_non_sharded_after_updates = training_state_cpu.params

    # Compare the parameters from sharded and unsharded training;
    pytest.assume(
        all(
            jax.tree_util.tree_leaves(
                jax.tree_map(
                    lambda x, y: np.allclose(x, y, atol=1e-3),
                    params_sharded_after_updates,
                    params_non_sharded_after_updates,
                )
            )
        )
    )
