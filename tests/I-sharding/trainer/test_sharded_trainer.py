import os
import shutil

import haiku as hk
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pytest
from jax import tree_util

from trix.dataloaders.language_models.generators import generate_fake_dataset
from trix.dataloaders.language_models.sequence_datasets import PairedSequenceDataset
from trix.layers.sharding.linear.linear_and_p_sum import LinearAndPsum
from trix.models.encoder_decoder.sharding.sharded_encoder_decoder import (
    DecoderConfig,
    EncoderConfig,
    build_sharded_encoder_decoder_fn,
)
from trix.tokenizers.language_models.standard import FixedSizeStandardTokenizer
from trix.training.language_trainers.sharding.sharded_trainer import (
    ShardedEncoderDecoderCausalTrainer,
    ShardedTrainingState,
    run_sharded_test_epoch_enc_dec,
    run_sharded_train_epoch_enc_dec,
)
from trix.training.losses import cross_entropy_loss_encoder_decoder
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize("num_shards", [2])
def test_training_state(num_shards: int) -> None:
    """
    Checks that given a ShardedTraniningState, saving and loading the
    ShardedTraniningState maintains the same values of the NamedTuple.
    """

    input_dim = 8
    batch_size = 16

    def forward_fn(x: jnp.array) -> jnp.array:
        layer = LinearAndPsum(output_size=10)
        return layer(x)

    # build forward fn and params
    forward_fn_trans = hk.without_apply_rng(hk.transform(forward_fn))

    params = jax.pmap(forward_fn_trans.init, axis_name="shard")(
        jnp.array([jax.random.PRNGKey(i) for i in range(num_shards)]),
        jnp.zeros((num_shards, batch_size, input_dim)),
    )

    # initialize optim
    optim = optax.adam(0.1)
    optim_state = jax.pmap(optim.init, axis_name="shard")(params)

    rand_key = jax.random.PRNGKey(1)
    step = 0

    # initialize Training State class
    train_state = ShardedTrainingState(
        step=jnp.array(0).repeat(num_shards, axis=0),
        params=params,
        optimizer_state=optim_state,
        random_key=jnp.tile(rand_key, (num_shards, 1)),
    )

    # save then load
    try:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        save_path = os.path.join(current_dir, "tpm_data")
        train_state.save(save_path)
        train_state_load = train_state.load(save_path, num_shards)

        # fake input and grads
        x = jax.random.normal(jax.random.PRNGKey(2), (batch_size, input_dim))
        x = jnp.tile(x, reps=(num_shards, 1, 1))
        grads = jax.tree_map(lambda x: x + 2, params)

        # check saved then loaded training state is equal to
        # training state as argument to save
        pytest.assume(
            jnp.allclose(
                jax.pmap(forward_fn_trans.apply, axis_name="shard")(
                    train_state_load.params, x
                ),
                jax.pmap(forward_fn_trans.apply, axis_name="shard")(params, x),
            )
        )

        load_updates = tree_util.tree_leaves(
            jax.pmap(optim.update, axis_name="shard")(
                grads, train_state_load.optimizer_state, params
            )[0]
        )
        updates = tree_util.tree_leaves(
            jax.pmap(optim.update, axis_name="shard")(grads, optim_state, params)[0]
        )
        for x, y in zip(load_updates, updates):
            pytest.assume(jnp.allclose(x, y))
        pytest.assume(train_state_load.step[0] == step)
        pytest.assume(jnp.allclose(train_state_load.random_key[0], rand_key))
    finally:
        shutil.rmtree(save_path)


@pytest.mark.parametrize("num_shards", [2])
@pytest.mark.parametrize("batch_size", [1])
def test_sharded_encoder_decoder_trainer(num_shards: int, batch_size: int) -> None:
    """
    Tests if the encoder decoder trainer runs without errors.


    WARNING: this test cannot run on CPUs and requires at least num_shards devices.
    """
    max_sequence_length = 10
    min_sequence_length = 10
    num_sequences = 16

    effective_batch_size = 8

    noising_ratio = 0.15
    masking_prob = 0.8

    num_decoding_steps = 2

    seed = 0

    prefix_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS, fixed_length=max_sequence_length
    )

    suffix_tokenizer = FixedSizeStandardTokenizer(
        standard_tokens=AMINO_ACIDS,
        fixed_length=max_sequence_length,
        prepend_bos_token=True,
    )

    encoder_config = EncoderConfig(
        alphabet_size=prefix_tokenizer.vocabulary_size,
        max_positions=max_sequence_length,
        num_attention_heads=2,
        embed_dim=4,
        ffn_embed_dim=4,
        num_layers=1,
    )

    decoder_config = DecoderConfig(
        alphabet_size=suffix_tokenizer.vocabulary_size,
        max_positions=max_sequence_length,
        num_attention_heads=2,
        embed_dim=4,
        ffn_embed_dim=4,
        num_layers=1,
    )

    # Get Datasets
    train_dataset = PairedSequenceDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed,
        ),
        prefix_tokenizer=prefix_tokenizer,
        suffix_tokenizer=suffix_tokenizer,
        batch_size=effective_batch_size,
        min_sequence_len=4,
        random_key=jax.random.PRNGKey(42),
    )
    test_dataset = PairedSequenceDataset(
        sequences=generate_fake_dataset(
            min_sequence_length,
            max_sequence_length,
            num_sequences,
            AMINO_ACIDS,
            seed=seed + 1,
        ),
        prefix_tokenizer=prefix_tokenizer,
        suffix_tokenizer=suffix_tokenizer,
        batch_size=effective_batch_size,
        min_sequence_len=4,
        random_key=jax.random.PRNGKey(42),
    )

    # Get forward function
    forward_fn = build_sharded_encoder_decoder_fn(
        encoder_config, decoder_config, num_shards=num_shards
    )
    forward_fn = hk.transform(forward_fn)

    optimizer = optax.adam(learning_rate=3e-4)

    trainer = ShardedEncoderDecoderCausalTrainer(
        forward_fn=forward_fn,
        loss_fn=cross_entropy_loss_encoder_decoder,
        tokenizer=prefix_tokenizer,
        optimizer=optimizer,
        noising_ratio=noising_ratio,
        masking_prob=masking_prob,
        num_decoding_steps=num_decoding_steps,
    )

    # Initialize on devices
    devices = jax.local_devices()
    num_devices = len(devices)
    devices_mesh = np.array(devices).reshape((-1, num_shards))

    num_data_parallel_ways = num_devices // num_shards
    num_acc_grads = effective_batch_size // (batch_size * (num_devices // num_shards))

    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):

        random_key = jax.random.PRNGKey(0)
        keys = jax.random.split(random_key, num=num_shards)
        encoder_tokens, decoder_tokens = next(train_dataset.get_epoch_batches())
        training_state = trainer.build_init_fn()(keys, encoder_tokens, decoder_tokens)

    with jax.sharding.Mesh(devices_mesh, ("batch", "shard")):
        training_state, metrics = run_sharded_train_epoch_enc_dec(
            update_fn=trainer.build_xmapped_update_fn(),
            dataset=train_dataset,
            num_acc_grads=num_acc_grads,
            num_data_parallel=num_data_parallel_ways,
            batch_size=batch_size,
            training_state=training_state,
            epoch_num=0,
        )

        val_metrics = run_sharded_test_epoch_enc_dec(
            compute_metrics_fn=trainer.build_xmapped_metrics_fn(),
            dataset=test_dataset,
            batch_size=effective_batch_size // num_data_parallel_ways,
            num_data_parallel=num_data_parallel_ways,
            training_state=training_state,
            epoch_num=0,
        )

    for x in jax.tree_util.tree_leaves(training_state):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training state")

    for x in jax.tree_util.tree_leaves(metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in training metrics")

    for x in jax.tree_util.tree_leaves(val_metrics):
        pytest.assume(not jnp.isnan(x).any(), "NaN in eval metrics")
