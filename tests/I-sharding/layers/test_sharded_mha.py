from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.layers.sharding.attention.multi_head_attention import (
    MultiHeadAttention,
    ShardedMultiHeadAttention,
)


@pytest.mark.parametrize("num_heads", [4])
@pytest.mark.parametrize("key_size", [4])
@pytest.mark.parametrize("batch_size", [4])
@pytest.mark.parametrize(
    "rotary_embedding_config", [RotaryEmbeddingConfig(rescaling_factor=None), None]
)
@pytest.mark.parametrize("num_shards", [2])
def test_sharded_multi_head_attention(
    num_heads: int,
    key_size: int,
    num_shards: int,
    batch_size: int,
    rotary_embedding_config: Optional[RotaryEmbeddingConfig],
) -> None:
    """
    Tests sharded MHA. Tests that the outputs have the right shape and are the same on
    each device.
    """
    random_key = jax.random.PRNGKey(0)
    input_dim = key_size * num_heads
    seq_len = 100
    x = jax.random.normal(random_key, (batch_size, seq_len, input_dim))
    unsharded_x = x
    x = jnp.tile(x, (num_shards, 1, 1, 1))

    def sharded_mha(q: jnp.ndarray, k: jnp.ndarray, v: jnp.ndarray) -> jnp.ndarray:
        layer = ShardedMultiHeadAttention(
            num_heads_per_shard=num_heads // num_shards,
            key_size=key_size,
            num_shards=num_shards,
            rotary_embedding_config=rotary_embedding_config,
            name="sharded_mha",
        )
        return layer(q, k, v)

    sharded_mha_fn = hk.transform(sharded_mha)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(sharded_mha_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x, x, x
    )

    y = jax.pmap(sharded_mha_fn.apply, axis_name="shard")(  # type: ignore
        params, jnp.array(random_keys), q=x, k=x, v=x
    )

    embeddings = y["embeddings"]
    pytest.assume(
        embeddings.shape == (num_shards, batch_size, seq_len, num_heads * key_size)
    )
    pytest.assume(
        jnp.all(
            jnp.array(
                [
                    jnp.allclose(embeddings[0], embeddings[i])
                    for i in range(1, num_shards)
                ]
            )
        )
    )

    # unsharded parameters
    unsharded_params = {
        "mha/key": {
            "w": jnp.concatenate(
                params["sharded_mha/key"]["w"].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(params["sharded_mha/key"]["b"]),
        },
        "mha/query": {
            "w": jnp.concatenate(
                params["sharded_mha/query"]["w"].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(params["sharded_mha/query"]["b"]),
        },
        "mha/value": {
            "w": jnp.concatenate(
                params["sharded_mha/value"]["w"].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(params["sharded_mha/value"]["b"]),
        },
        "mha/mha_output": {
            "b": params["sharded_mha/mha_output"]["bias"][0],
            "w": jnp.concatenate(
                params["sharded_mha/mha_output/~/linear"]["w"], axis=0
            ),
        },
    }

    # unsharded function
    def mha(q: jnp.ndarray, k: jnp.ndarray, v: jnp.ndarray) -> jnp.ndarray:
        layer = MultiHeadAttention(
            num_heads=num_heads,
            key_size=key_size,
            rotary_embedding_config=rotary_embedding_config,
            name="mha",
        )
        return layer(q, k, v)

    mha_fn = hk.transform(mha)

    # unsharded output
    unsharded_y = mha_fn.apply(
        unsharded_params, random_key, q=unsharded_x, k=unsharded_x, v=unsharded_x
    )
    unsharded_embeddings = unsharded_y["embeddings"]
    pytest.assume(jnp.allclose(embeddings[0], unsharded_embeddings, atol=1e-5))
