import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.heads.lm_head import SimpleLMHead
from trix.layers.sharding.heads.lm_head import ShardedLMHead


@pytest.mark.parametrize("embedding_dim", [4])
@pytest.mark.parametrize("output_dim", [10])
@pytest.mark.parametrize("batch_size", [8])
@pytest.mark.parametrize("num_shards", [2])
def test_sharded_lm_head(
    embedding_dim: int, output_dim: int, num_shards: int, batch_size: int
) -> None:
    """
    Test sharded LM head: tests that the parameters are indeed synchronized at
    initialization and that it gives the same output as an unsharded LM head.
    #TODO: add a test for one step in GD to check that the weights are still
    synchronized.
    """
    random_key = jax.random.PRNGKey(0)

    x = jax.random.normal(random_key, (batch_size, embedding_dim))
    unsharded_x = x
    x = jnp.tile(x, (num_shards, 1, 1))

    def sharded_lm_head(x: jnp.ndarray) -> jnp.ndarray:
        layer = ShardedLMHead(
            embed_dim=embedding_dim // num_shards,
            alphabet_size=output_dim,
            name="lm_head",
        )
        return layer(x)

    sharded_lm_head_fn = hk.transform(sharded_lm_head)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(sharded_lm_head_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x
    )

    # Check that the initialization works well
    weights = params["lm_head/~/synchronous_final_fc"]["w"]
    biases = params["lm_head/~/synchronous_final_fc"]["b"]
    pytest.assume(
        jnp.all(
            jnp.array(
                [jnp.allclose(weights[0], weights[i]) for i in range(1, num_shards)]
            )
        )
    )
    pytest.assume(
        jnp.all(
            jnp.array(
                [jnp.allclose(biases[0], biases[i]) for i in range(1, num_shards)]
            )
        )
    )

    # Check that it gives the same result as normal LM head
    def lm_head(x: jnp.ndarray) -> jnp.ndarray:
        layer = SimpleLMHead(
            embed_dim=embedding_dim // num_shards,
            alphabet_size=output_dim,
            name="lm_head",
        )
        return layer(x)

    lm_head_fn = hk.transform(lm_head)

    unsharded_params = {"lm_head/~/lm_final_fc": {"w": weights[0], "b": biases[0]}}

    y = jax.pmap(sharded_lm_head_fn.apply, axis_name="shard")(
        params, jnp.array(random_keys), x
    )
    unsharded_y = lm_head_fn.apply(unsharded_params, random_key, unsharded_x)

    pytest.assume(jnp.allclose(y["logits"][0], unsharded_y["logits"], atol=1e-6))
