import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.sharding.positional.learned import (
    ReplicatedLearnedPositionalEmbeddings,
)


@pytest.mark.parametrize("max_positions", [10])
@pytest.mark.parametrize("embed_dim", [4])
@pytest.mark.parametrize("batch_size", [4])
@pytest.mark.parametrize("num_shards", [2])
def test_replicated_layer_norm(
    max_positions: int, embed_dim: int, num_shards: int, batch_size: int
) -> None:
    """
    Test learned positional embedding: tests that the layer norm parameters are the same
    across devices and that every output is the same on each device.
    """

    random_key = jax.random.PRNGKey(0)

    x = jax.random.randint(
        random_key, (batch_size, embed_dim), minval=0, maxval=max_positions
    )
    x = jnp.tile(x, (num_shards, 1, 1))

    def replicated_learned_positional_embedding(x: jnp.ndarray) -> jnp.ndarray:
        layer = ReplicatedLearnedPositionalEmbeddings(
            max_positions=max_positions,
            embed_dim=embed_dim,
            name="pos_embed",
        )
        return layer(x)

    replicated_learned_positional_embedding_fn = hk.transform(
        replicated_learned_positional_embedding
    )

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(
        replicated_learned_positional_embedding_fn.init,  # type: ignore
        axis_name="shard",
    )(jnp.array(random_keys), x)

    # Check that the initialization works well
    embedding_matrix = params["pos_embed/~/replicated_embed"]["embeddings"]
    pytest.assume(
        jnp.all(
            jnp.array(
                [
                    jnp.allclose(embedding_matrix[0], embedding_matrix[i])
                    for i in range(1, num_shards)
                ]
            )
        )
    )

    # Check that the outputs are synchronized
    y = jax.pmap(replicated_learned_positional_embedding_fn.apply, axis_name="shard")(
        params, jnp.array(random_keys), x
    )

    pytest.assume(
        jnp.all(jnp.array([jnp.allclose(y[0], y[i]) for i in range(1, num_shards)]))
    )
