from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.attention.self_attention import SelfAttentionBlock
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.layers.sharding.attention.self_attention import ShardedSelfAttentionBlock
from trix.models.encoder_decoder.sharding.utils.shard_encoder_decoder import (
    rename_params_to_unsharded,
)
from trix.utils.sharding.unshard import unshard_attention_block


@pytest.mark.parametrize("num_heads", [4])
@pytest.mark.parametrize("key_size", [4])
@pytest.mark.parametrize("batch_size", [8])
@pytest.mark.parametrize(
    "rotary_embedding_config", [RotaryEmbeddingConfig(rescaling_factor=None), None]
)
@pytest.mark.parametrize("num_shards", [2])
def test_sharded_self_attention(
    num_heads: int,
    key_size: int,
    num_shards: int,
    batch_size: int,
    rotary_embedding_config: Optional[RotaryEmbeddingConfig],
) -> None:
    """
    Tests sharded Self Attention. Tests that the outputs have the right shape and are
    the same on each device.
    """
    random_key = jax.random.PRNGKey(0)
    seq_len = 100
    input_dim = num_heads * key_size
    # input random data
    x = jax.random.normal(random_key, (batch_size, seq_len, input_dim))
    unsharded_x = x
    x = jnp.tile(x, (num_shards, 1, 1, 1))

    # sharded function
    def sharded_self_attn(x: jnp.ndarray) -> jnp.ndarray:
        layer = ShardedSelfAttentionBlock(
            num_heads_per_shard=num_heads // num_shards,
            embed_dim=num_heads * key_size,
            ffn_embed_dim=num_heads * key_size * 2,
            num_shards=num_shards,
            rotary_embedding_config=rotary_embedding_config,
            name="self_attention_block",
        )
        return layer(x)

    sharded_self_attn_fn = hk.transform(sharded_self_attn)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)

    # init params
    params = jax.pmap(sharded_self_attn_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x
    )

    # sharded result
    y = jax.pmap(sharded_self_attn_fn.apply, axis_name="shard")(  # type: ignore
        params, jnp.array(random_keys), x
    )

    embeddings = y["embeddings"]
    # check shape
    pytest.assume(
        embeddings.shape == (num_shards, batch_size, seq_len, num_heads * key_size)
    )
    # check all embeddings are the same
    pytest.assume(
        jnp.all(
            jnp.array(
                [
                    jnp.allclose(embeddings[0], embeddings[i])
                    for i in range(1, num_shards)
                ]
            )
        )
    )

    # unsharded parameters
    unsharded_params = unshard_attention_block(params)
    unsharded_params = rename_params_to_unsharded(unsharded_params)

    # unsharded function
    def self_attn(x: jnp.ndarray) -> jnp.ndarray:
        layer = SelfAttentionBlock(
            num_heads=num_heads,
            embed_dim=num_heads * key_size,
            ffn_embed_dim=num_heads * key_size * 2,
            rotary_embedding_config=rotary_embedding_config,
            name="self_attention_block",
        )
        return layer(x)

    self_attn_fn = hk.transform(self_attn)

    # unsharded output
    unsharded_y = self_attn_fn.apply(unsharded_params, random_key, unsharded_x)
    unsharded_embeddings = unsharded_y["embeddings"]
    pytest.assume(jnp.allclose(embeddings[0], unsharded_embeddings, atol=1e-4))
