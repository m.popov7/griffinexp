import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.sharding.linear.synchronous_linear import LinearLayerSynchronousInit


@pytest.mark.parametrize("input_dim", [32])
@pytest.mark.parametrize("output_dim", [100])
@pytest.mark.parametrize("batch_size", [10])
@pytest.mark.parametrize("num_shards", [2])
def test_linear_synchronous_init(
    input_dim: int, output_dim: int, num_shards: int, batch_size: int
) -> None:
    """
    Tests that the Linear layer with synchronous initialization indeed initializes every
    weight and bias with the same values
    """
    random_key = jax.random.PRNGKey(0)
    # Input data for initialization
    x = jax.random.normal(random_key, (batch_size, input_dim))
    x = jnp.tile(x, (num_shards, 1, 1))

    # Layer function
    def linear_synchronous(x: jnp.ndarray) -> jnp.ndarray:
        layer = LinearLayerSynchronousInit(output_size=output_dim, name="linear")
        return layer(x)

    linear_synchronous_fn = hk.transform(linear_synchronous)

    # Parameters initialization
    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(linear_synchronous_fn.init, axis_name="shard")(
        jnp.array(random_keys), x
    )

    # Check that they are equal
    weights = params["linear"]["w"]
    biases = params["linear"]["b"]
    pytest.assume(
        jnp.all(
            jnp.array(
                [jnp.allclose(weights[0], weights[i]) for i in range(1, num_shards)]
            )
        )
    )
    pytest.assume(
        jnp.all(
            jnp.array(
                [jnp.allclose(biases[0], biases[i]) for i in range(1, num_shards)]
            )
        )
    )
