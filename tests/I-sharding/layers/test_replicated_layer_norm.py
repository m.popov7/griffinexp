import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.sharding.regularization.norms import ReplicatedLayerNorm


@pytest.mark.parametrize("embedding_dim", [10])
@pytest.mark.parametrize("batch_size", [4])
@pytest.mark.parametrize("num_shards", [2])
@pytest.mark.parametrize("create_offset", [True, False])
def test_replicated_layer_norm(
    embedding_dim: int, create_offset: bool, num_shards: int, batch_size: int
) -> None:
    """
    Test replicated layer norm: tests that the layer norm parameters are the same across
    devices and that every output is the same on each device.
    """

    random_key = jax.random.PRNGKey(0)

    x = jax.random.normal(random_key, (batch_size, embedding_dim))
    x = jnp.tile(x, (num_shards, 1, 1))

    def replicated_layer_norm(x: jnp.ndarray) -> jnp.ndarray:
        layer = ReplicatedLayerNorm(
            create_offset=create_offset,
            name="layer_norm",
        )
        return layer(x)

    replicated_layer_norm_fn = hk.transform(replicated_layer_norm)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(replicated_layer_norm_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x
    )

    # Check that the initialization works well
    scales = params["layer_norm"]["scale"]
    pytest.assume(
        jnp.all(
            jnp.array(
                [jnp.allclose(scales[0], scales[i]) for i in range(1, num_shards)]
            )
        )
    )
    pytest.assume(("offset" in params["layer_norm"]) == create_offset)
    if create_offset:
        offsets = params["layer_norm"]["offset"]
        pytest.assume(
            jnp.all(
                jnp.array(
                    [jnp.allclose(offsets[0], offsets[i]) for i in range(1, num_shards)]
                )
            )
        )

    y = jax.pmap(replicated_layer_norm_fn.apply, axis_name="shard")(
        params, jnp.array(random_keys), x
    )

    pytest.assume(
        jnp.all(jnp.array([jnp.allclose(y[0], y[i]) for i in range(1, num_shards)]))
    )
