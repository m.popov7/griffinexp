from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.attention.cross_attention import CrossAttentionBlock
from trix.layers.positional.esm_rotary import RotaryEmbeddingConfig
from trix.layers.sharding.attention.cross_attention import ShardedCrossAttentionBlock


@pytest.mark.parametrize("num_heads", [4])
@pytest.mark.parametrize("key_size", [4])
@pytest.mark.parametrize("batch_size", [8])
@pytest.mark.parametrize(
    "rotary_embedding_config", [RotaryEmbeddingConfig(rescaling_factor=None), None]
)
@pytest.mark.parametrize("num_shards", [2])
def test_sharded_cross_attention(
    num_heads: int,
    key_size: int,
    num_shards: int,
    batch_size: int,
    rotary_embedding_config: Optional[RotaryEmbeddingConfig],
) -> None:
    """
    Tests sharded Cross Attention. Tests that the outputs have the right shape and are
    the same on each device. Tests that the results are similar to the one of the
    unsharded version.
    """
    random_key = jax.random.PRNGKey(0)
    seq_len = 100
    input_dim = num_heads * key_size
    # input random data
    x = jax.random.normal(random_key, (batch_size, seq_len, input_dim))
    unsharded_x = x
    x = jnp.tile(x, (num_shards, 1, 1, 1))

    # sharded function
    def sharded_cross_attn(x: jnp.ndarray, e: jnp.ndarray) -> jnp.ndarray:
        layer = ShardedCrossAttentionBlock(
            num_heads_per_shard=num_heads // num_shards,
            embed_dim=num_heads * key_size,
            ffn_embed_dim=num_heads * key_size * 2,
            num_shards=num_shards,
            rotary_embedding_config=rotary_embedding_config,
            name="sharded_cross_atention_block",
        )
        return layer(x, e)

    sharded_cross_attn_fn = hk.transform(sharded_cross_attn)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)

    # init params
    params = jax.pmap(sharded_cross_attn_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x, x
    )

    # sharded result
    y = jax.pmap(sharded_cross_attn_fn.apply, axis_name="shard")(  # type: ignore
        params, jnp.array(random_keys), x, x
    )

    embeddings = y["embeddings"]
    # check shape
    pytest.assume(
        embeddings.shape == (num_shards, batch_size, seq_len, num_heads * key_size)
    )
    # check all embeddings are the same
    pytest.assume(
        jnp.all(
            jnp.array(
                [
                    jnp.allclose(embeddings[0], embeddings[i])
                    for i in range(1, num_shards)
                ]
            )
        )
    )

    # unsharded parameters
    unsharded_params = {
        "cross_attn/~/self_attention/key": {
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/self_attention/key"][
                    "w"
                ].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(
                params["sharded_cross_atention_block/~/self_attention/key"]["b"]
            ),
        },
        "cross_attn/~/self_attention/query": {
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/self_attention/query"][
                    "w"
                ].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(
                params["sharded_cross_atention_block/~/self_attention/query"]["b"]
            ),
        },
        "cross_attn/~/self_attention/value": {
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/self_attention/value"][
                    "w"
                ].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(
                params["sharded_cross_atention_block/~/self_attention/value"]["b"]
            ),
        },
        "cross_attn/~/self_attention/mha_output": {
            "b": params["sharded_cross_atention_block/~/self_attention/mha_output"][
                "bias"
            ][0],
            "w": jnp.concatenate(
                params[
                    "sharded_cross_atention_block/~/"
                    "self_attention/mha_output/~/linear"
                ]["w"],
                axis=0,
            ),
        },
        "cross_attn/~/cross_attention/key": {
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/cross_attention/key"][
                    "w"
                ].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(
                params["sharded_cross_atention_block/~/cross_attention/key"]["b"]
            ),
        },
        "cross_attn/~/cross_attention/query": {
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/cross_attention/query"][
                    "w"
                ].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(
                params["sharded_cross_atention_block/~/cross_attention/query"]["b"]
            ),
        },
        "cross_attn/~/cross_attention/value": {
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/cross_attention/value"][
                    "w"
                ].transpose(0, 2, 1)
            ).transpose(1, 0),
            "b": jnp.concatenate(
                params["sharded_cross_atention_block/~/cross_attention/value"]["b"]
            ),
        },
        "cross_attn/~/cross_attention/mha_output": {
            "b": params["sharded_cross_atention_block/~/cross_attention/mha_output"][
                "bias"
            ][0],
            "w": jnp.concatenate(
                params[
                    "sharded_cross_atention_block/~/cross_attention/"
                    "mha_output/~/linear"
                ]["w"],
                axis=0,
            ),
        },
        "cross_attn/~/fc1": {
            "b": jnp.concatenate(params["sharded_cross_atention_block/~/fc1"]["b"]),
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/fc1"]["w"].transpose(0, 2, 1)
            ).transpose(1, 0),
        },
        "cross_attn/~/fc2": {
            "b": params["sharded_cross_atention_block/~/fc2"]["bias"][0],
            "w": jnp.concatenate(
                params["sharded_cross_atention_block/~/fc2/~/linear"]["w"], axis=0
            ),
        },
        "cross_attn/~/self_attention_layer_norm": {
            "scale": params["sharded_cross_atention_block/~/self_attention_layer_norm"][
                "scale"
            ][0],
            "offset": params[
                "sharded_cross_atention_block/~/self_attention_layer_norm"
            ]["offset"][0],
        },
        "cross_attn/~/cross_attention_layer_norm": {
            "scale": params[
                "sharded_cross_atention_block/~/cross_attention_layer_norm"
            ]["scale"][0],
            "offset": params[
                "sharded_cross_atention_block/~/cross_attention_layer_norm"
            ]["offset"][0],
        },
        "cross_attn/~/final_layer_norm": {
            "scale": params["sharded_cross_atention_block/~/final_layer_norm"]["scale"][
                0
            ],
            "offset": params["sharded_cross_atention_block/~/final_layer_norm"][
                "offset"
            ][0],
        },
    }

    # unsharded function
    def cross_attn(x: jnp.ndarray, e: jnp.ndarray) -> jnp.ndarray:
        layer = CrossAttentionBlock(
            num_heads=num_heads,
            embed_dim=num_heads * key_size,
            ffn_embed_dim=num_heads * key_size * 2,
            rotary_embedding_config=rotary_embedding_config,
            name="cross_attn",
        )
        return layer(x, e)

    cross_attn_fn = hk.transform(cross_attn)

    # unsharded output
    unsharded_y = cross_attn_fn.apply(
        unsharded_params, random_key, unsharded_x, unsharded_x
    )
    unsharded_embeddings = unsharded_y["embeddings"]
    pytest.assume(jnp.allclose(embeddings[0], unsharded_embeddings, atol=1e-4))
