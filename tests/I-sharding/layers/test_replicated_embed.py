import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.sharding.embeddings.embed import ReplicatedEmbed


@pytest.mark.parametrize("vocab_size", [10])
@pytest.mark.parametrize("embed_dim", [10])
@pytest.mark.parametrize("batch_size", [4])
@pytest.mark.parametrize("num_shards", [2])
def test_replicated_embed(
    vocab_size: int, embed_dim: int, num_shards: int, batch_size: int
) -> None:
    """
    Test replicated embedding: tests that the layer norm parameters are the same across
    devices and that every output is the same on each device.
    """

    random_key = jax.random.PRNGKey(0)

    x = jax.random.randint(
        random_key, (batch_size, embed_dim), minval=0, maxval=vocab_size
    )
    x = jnp.tile(x, (num_shards, 1, 1))

    def replicated_embed(x: jnp.ndarray) -> jnp.ndarray:
        layer = ReplicatedEmbed(
            vocab_size=vocab_size,
            embed_dim=embed_dim,
            name="replicated_embed",
        )
        return layer(x)

    replicated_embed_fn = hk.transform(replicated_embed)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(replicated_embed_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x
    )

    # Check that the initialization works well
    embedding_matrix = params["replicated_embed"]["embeddings"]
    pytest.assume(
        jnp.all(
            jnp.array(
                [
                    jnp.allclose(embedding_matrix[0], embedding_matrix[i])
                    for i in range(1, num_shards)
                ]
            )
        )
    )

    # Check that the outputs are synchronized
    y = jax.pmap(replicated_embed_fn.apply, axis_name="shard")(
        params, jnp.array(random_keys), x
    )

    pytest.assume(
        jnp.all(jnp.array([jnp.allclose(y[0], y[i]) for i in range(1, num_shards)]))
    )
