import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.layers.sharding.linear.linear_and_p_sum import LinearAndPsum
from trix.utils.sharding.shard import shard_linear_and_psum
from trix.utils.sharding.unshard import unshard_linear_and_psum


@pytest.mark.parametrize("input_dim", [32])
@pytest.mark.parametrize("output_dim", [100])
@pytest.mark.parametrize("batch_size", [10])
@pytest.mark.parametrize("num_shards", [2])
@pytest.mark.parametrize("add_bias", [True, False])
def test_linear_and_psum(
    input_dim: int, output_dim: int, num_shards: int, batch_size: int, add_bias: bool
) -> None:
    """
    Tests that the linear and psum layer has parameters with right shapes, returns
    equal outputs on every shard and returns outputs equal to the ones of an unsharded
    linear layer. Note that there are small differences (around 1e-6 absolute
    difference) between sharded and unsharded output layers when the number of shards is
    greater than one.
    """
    random_key = jax.random.PRNGKey(0)

    x = jax.random.normal(random_key, (batch_size, input_dim))
    unsharded_x = x
    x = x.reshape((batch_size, num_shards, input_dim // num_shards)).transpose(1, 0, 2)

    @hk.transform
    def sharded_linear_fn(x: jnp.ndarray) -> jnp.ndarray:
        layer = LinearAndPsum(output_size=output_dim, with_bias=add_bias, name="linear")
        return layer(x)

    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    params = jax.pmap(sharded_linear_fn.init, axis_name="shard")(  # type: ignore
        jnp.array(random_keys), x
    )

    y = jax.pmap(sharded_linear_fn.apply, axis_name="shard")(  # type: ignore
        params, jnp.array(random_keys), x
    )

    # Check that the output shape is right
    pytest.assume(y.shape == (num_shards, batch_size, output_dim))

    # Check that output on each shard is equal
    pytest.assume(
        jnp.all(jnp.array([jnp.allclose(y[0], y[i]) for i in range(1, num_shards)]))
    )

    # Check that output matches the one of a linear layer
    @hk.transform
    def linear_fn(x: jnp.ndarray) -> jnp.ndarray:
        layer = hk.Linear(output_size=output_dim, with_bias=add_bias)
        return layer(x)

    unsharded_params = unshard_linear_and_psum(params)

    unsharded_y = linear_fn.apply(  # type: ignore
        unsharded_params, random_key, unsharded_x
    )

    pytest.assume(jnp.allclose(y[0], unsharded_y, atol=1e-6, rtol=1e-5))

    # test that resharding the unsharded params gives consistent results
    resharded_params = shard_linear_and_psum(unsharded_params, num_shards=num_shards)

    y_resharded = jax.pmap(sharded_linear_fn.apply, axis_name="shard")(  # type: ignore
        resharded_params, jnp.array(random_keys), x
    )
    pytest.assume(jnp.allclose(y, y_resharded, atol=1e-6, rtol=1e-5))

    # Check that output matches the one of the same layer sharded on one device only
    x_one_device = jnp.expand_dims(unsharded_x, axis=0)
    params_one_device = shard_linear_and_psum(unsharded_params, num_shards=1)

    y_one_device = jax.pmap(sharded_linear_fn.apply, axis_name="shard")(  # type: ignore
        params_one_device, jnp.array([random_key]), x_one_device
    )
    pytest.assume(jnp.allclose(y_one_device[0], y[0], atol=1e-6))
    pytest.assume(jnp.allclose(y_one_device[0], unsharded_y))
