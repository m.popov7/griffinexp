import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.esm.sharding.sharded_esm import ShardedESMTransformer
from trix.models.esm.sharding.utils.sharding import shard_esm
from trix.models.esm.sharding.utils.unsharding import unshard_esm
from trix.pretrained.esm import get_pretrained_esm_model


@pytest.mark.parametrize("model_name", ["esm1_t6_43M_UR50S", "esm2_t12_35M_UR50D"])
@pytest.mark.parametrize("num_shards", [2])
def test_consistency(model_name: str, num_shards: int) -> None:
    """Tests that sharded version of ESM gives the same results as unsharded."""
    parameters, forward_fn, tokenizer, config = get_pretrained_esm_model(
        model_name=model_name, embeddings_layers_to_save=(6,)
    )
    forward_fn = hk.transform(forward_fn)

    def sharded_transformer_fn(x: jnp.ndarray) -> jnp.ndarray:
        model = ShardedESMTransformer(config, num_shards, name=model_name)
        return model(x)

    sharded_transformer_fn = hk.transform(sharded_transformer_fn)
    random_key = jax.random.PRNGKey(0)
    batch_size = 8
    seq_len = 40
    tokens = jax.random.randint(
        random_key, minval=5, maxval=28, shape=(batch_size, seq_len)
    )

    random_key = jax.random.PRNGKey(0)
    random_key, *sub_keys = jax.random.split(random_key, num=num_shards + 1)
    sub_keys = jnp.array(sub_keys)
    tokens = jnp.tile(tokens, (num_shards, 1, 1))
    sharded_params = shard_esm(parameters, num_shards)
    outs = jax.pmap(sharded_transformer_fn.apply, axis_name="shard")(  # type: ignore
        sharded_params, sub_keys, tokens
    )
    unsharded_outs = forward_fn.apply(parameters, random_key, tokens[0])

    pytest.assume(
        jnp.allclose(
            outs["embeddings_6"][0],
            unsharded_outs["embeddings_6"],
            rtol=1e-4,
            atol=1e-3,
        )
    )
    unsharded_params = unshard_esm(sharded_params)
    unsharded_outs_after_unsharding = forward_fn.apply(
        unsharded_params, random_key, tokens[0]
    )
    pytest.assume(
        jnp.allclose(
            unsharded_outs["embeddings_6"],
            unsharded_outs_after_unsharding["embeddings_6"],
            rtol=1e-4,
            atol=1e-3,
        )
    )
