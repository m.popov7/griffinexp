import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.encoder_decoder.sharding.sharded_decoder import DecoderConfig
from trix.models.encoder_decoder.sharding.sharded_encoder import EncoderConfig
from trix.models.encoder_decoder.sharding.sharded_encoder_decoder import (
    build_sharded_encoder_decoder_fn,
)
from trix.models.encoder_decoder.sharding.utils.shard_encoder_decoder import (
    shard_encoder_decoder,
    unshard_encoder_decoder,
)


@pytest.mark.parametrize("alphabet_size", [10])
@pytest.mark.parametrize("batch_size", [16])
@pytest.mark.parametrize(
    "embed_dim",
    [4],
)
@pytest.mark.parametrize("max_positions", [10])
@pytest.mark.parametrize("num_shards", [2])
def test_build_transformer_fn(
    alphabet_size: int,
    batch_size: int,
    max_positions: int,
    embed_dim: int,
    num_shards: int,
) -> None:
    """Tests that the transformer call function returns the right shapes."""
    encoder_config = EncoderConfig(
        alphabet_size=alphabet_size,
        max_positions=max_positions,
        embed_dim=embed_dim,
        num_attention_heads=2,
        num_layers=1,
    )
    decoder_config = DecoderConfig(
        alphabet_size=alphabet_size,
        max_positions=max_positions,
        embed_dim=embed_dim,
        num_attention_heads=2,
        num_layers=1,
    )

    sharded_encoder_decoder_fn = build_sharded_encoder_decoder_fn(
        encoder_config, decoder_config, num_shards=num_shards
    )
    sharded_encoder_decoder_fn = hk.transform(sharded_encoder_decoder_fn)

    random_key = jax.random.PRNGKey(0)
    random_key, *random_keys = jax.random.split(random_key, num=num_shards + 1)
    random_keys = jnp.array(random_keys)

    tokens = jax.random.choice(
        random_key, alphabet_size, shape=(batch_size, max_positions)
    )

    random_key, _ = jax.random.split(random_key)
    decoder_tokens = jax.random.choice(
        random_key, alphabet_size, shape=(batch_size, max_positions)
    )

    tokens = jnp.tile(tokens, (num_shards, 1, 1))
    decoder_tokens = jnp.tile(decoder_tokens, (num_shards, 1, 1))

    params = jax.pmap(sharded_encoder_decoder_fn.init, axis_name="shard")(
        random_keys, tokens, decoder_tokens
    )
    encoder_outs, decoder_outs = jax.pmap(
        sharded_encoder_decoder_fn.apply, axis_name="shard"
    )(params, random_keys, tokens, decoder_tokens)

    pytest.assume(
        encoder_outs["embeddings"].shape
        == (num_shards, batch_size, max_positions, embed_dim)
    )
    pytest.assume(
        decoder_outs["logits"].shape
        == (num_shards, batch_size, max_positions, alphabet_size)
    )
    pytest.assume(
        decoder_outs["attention_weights"].shape
        == (num_shards, batch_size, max_positions, max_positions)
    )

    half_shard_params = shard_encoder_decoder(
        unshard_encoder_decoder(params), num_shards // 2
    )

    half_shard_fn = build_sharded_encoder_decoder_fn(
        encoder_config, decoder_config, num_shards=num_shards // 2
    )
    half_shard_fn = hk.transform(half_shard_fn)

    random_key = jax.random.PRNGKey(0)
    random_key, *random_keys = jax.random.split(random_key, num=num_shards // 2 + 1)
    random_keys = jnp.array(random_keys)
    half_sharded_tokens = jnp.tile(tokens[0], (num_shards // 2, 1, 1))
    print(list(half_shard_params.keys()))
    half_shard_outs, _ = jax.pmap(half_shard_fn.apply, axis_name="shard")(
        half_shard_params, random_keys, half_sharded_tokens, half_sharded_tokens
    )
    pytest.assume(
        jnp.allclose(
            half_shard_outs["embeddings"][0],
            half_shard_outs["embeddings"][0],
            atol=1e-4,
            rtol=1e-4,
        )
    )
