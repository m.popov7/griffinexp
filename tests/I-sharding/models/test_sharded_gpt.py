from typing import Optional

import haiku as hk
import jax
import numpy as np
import pytest
from transformers import AutoTokenizer

from trix.models.gpt.model import GptConfig, build_gpt_fn
from trix.models.gpt.sharding.sharded_gpt import build_sharded_gpt_fn
from trix.models.gpt.sharding.utils.sharding import shard_gpt
from trix.models.gpt.sharding.utils.unsharding import unshard_gpt


@pytest.mark.parametrize("num_shards", [2])
@pytest.mark.parametrize(
    (
        "rope_dimensions",
        "add_bias_ffn",
        "norm_type",
        "parallel_attention_ff",
        "use_glu_in_ffn",
        "ffn_activation_name",
        "add_bias_lm_head",
    ),
    [
        [
            4,
            True,
            "layer_norm",
            False,
            False,
            "gelu",
            True,
        ],  # GPTJ,
        [
            None,
            False,
            "RMS_norm",
            False,
            True,
            "silu",
            False,
        ],  # LLAMA
    ],
)
def test_sharding(
    num_shards: int,
    rope_dimensions: Optional[int],
    add_bias_ffn: bool,
    norm_type: str,
    parallel_attention_ff: bool,
    use_glu_in_ffn: bool,
    ffn_activation_name: str,
    add_bias_lm_head: bool,
) -> None:
    """
    Instantiate a GPT model, then shard the parameters, then unshard them.

    # test 1 : test the initial parameters vs. unsharded ones
    # test 2 : test that the embeddings and logits for each shard are equal
    # test 3 : test the shape of the outputs
    # test 4 : test equality unsharded ( initial weights ) vs. desharded
    # test 5 : test equality unsharded ( initial weights ) vs. sharded
    """

    tokenizer = AutoTokenizer.from_pretrained("hf-internal-testing/tiny-random-gptj")
    tokenizer.pad_token_id = tokenizer.eos_token_id

    random_key = jax.random.PRNGKey(0)

    prompts = ["My dog is", "I'd like to go"]
    tokens = tokenizer(prompts, return_tensors="np", padding=True)["input_ids"]

    init_prompts = ["My dog is"]

    init_tokens = tokenizer(init_prompts, return_tensors="np", padding=True)[
        "input_ids"
    ]

    config = GptConfig(
        vocab_size=1000,
        eos_token_id=tokenizer.eos_token_id,
        embed_dim=8,
        ffn_embed_dim=8 * 2,
        num_heads=2,
        num_layers=2,
        max_position_embeddings=512,
        rope_dimensions=rope_dimensions,
        add_bias_ffn=add_bias_ffn,
        ffn_activation_name=ffn_activation_name,
        use_glu_in_ffn=use_glu_in_ffn,
        add_bias_lm_head=add_bias_lm_head,
        norm_type=norm_type,
        parallel_attention_ff=parallel_attention_ff,
        use_gradient_checkpointing=False,
    )

    sharded_gpt_fn = build_sharded_gpt_fn(
        config=config, num_shards=num_shards, name="sharded_gpt_decoder"
    )
    sharded_gpt_fn = hk.transform(sharded_gpt_fn)

    gpt_fn = build_gpt_fn(config)
    gpt_fn = hk.transform(gpt_fn)

    params = gpt_fn.init(random_key, init_tokens)  # type: ignore
    # instantiate random layer norm
    for k in params.keys():
        for w in params[k].keys():
            if "norm" in w:
                params[k][w] = params[k][w] + np.random.normal(
                    loc=0, scale=0.1, size=params[k][w].shape
                )

    sharded_params = shard_gpt(params, num_shards=num_shards)

    desharded_params = unshard_gpt(sharded_params)

    # test 1 : test the initial parameters vs. unsharded ones
    for k in params.keys():
        for w in params[k].keys():
            pytest.assume(
                np.allclose(params[k][w], desharded_params[k][w]),
                f"unsharding is inconsistent for {k}:{w}\n",
            )

    outs_unsharded = jax.jit(gpt_fn.apply)(params, random_key, tokens)

    outs_sharded = jax.pmap(
        sharded_gpt_fn.apply, axis_name="shard", in_axes=(0, None, None)
    )(sharded_params, random_key, tokens)

    outs_desharded = jax.jit(gpt_fn.apply)(desharded_params, random_key, tokens)

    # test 2 : test that the embeddings and logits for each shard are equal
    pytest.assume(
        np.allclose(outs_sharded["embeddings"][0], outs_sharded["embeddings"][1]),
        "The results at the end of the model are not synchronised across the shards",
    )
    pytest.assume(
        np.allclose(outs_sharded["logits"][0], outs_sharded["logits"][1]),
        "The results at the end of the model are not synchronised across the shards",
    )

    # test 3 : test the shape of the outputs
    pytest.assume(
        outs_sharded["embeddings"].shape
        == (num_shards, 2, tokens.shape[1], config.embed_dim),
        f"unexpected shape {outs_sharded['embeddings'].shape}",
    )
    pytest.assume(
        outs_sharded["logits"].shape
        == (num_shards, 2, tokens.shape[1], config.vocab_size),
        "unexpected shape {outs_sharded['logits'].shape}",
    )

    # test 4 : test equality unsharded ( initial weights ) vs. desharded
    pytest.assume(
        np.allclose(outs_unsharded["logits"], outs_desharded["logits"]),
        "unsharded and desharded logits are inconsistent",
    )
    pytest.assume(
        np.allclose(outs_unsharded["embeddings"], outs_desharded["embeddings"]),
        "unsharded and desharded embeddings are inconsistent",
    )
    # test 5 : test equality unsharded ( initial weights ) vs. sharded
    pytest.assume(
        np.allclose(
            outs_unsharded["logits"],
            outs_sharded["logits"][0],
            rtol=5 * 1e-3,
            atol=5 * 1e-5,
        ),
        "unsharded and sharded logits are inconsistent \n"
        "max abs diff={}".format(
            np.abs(outs_unsharded["logits"] - outs_sharded["logits"][0])
        ),
    )
    pytest.assume(
        np.allclose(
            outs_unsharded["embeddings"],
            outs_sharded["embeddings"][0],
            rtol=5 * 1e-3,
            atol=5 * 1e-5,
        ),
        "unsharded and sharded embeddings are inconsistent \n"
        "max abs diff={}".format(
            np.abs(outs_unsharded["embeddings"] - outs_sharded["embeddings"][0])
        ),
    )
