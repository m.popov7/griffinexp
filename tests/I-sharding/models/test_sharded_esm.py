from typing import Optional

import haiku as hk
import jax
import jax.numpy as jnp
import pytest

from trix.models.esm.model import ESMTransformerConfig
from trix.models.esm.sharding.sharded_esm import build_sharded_esm_fn


@pytest.mark.parametrize("num_shards", [2])
@pytest.mark.parametrize("add_bias_kv", [True, False])
@pytest.mark.parametrize("use_rotary_embedding", [True, False])
@pytest.mark.parametrize("positional_embedding", ["learned", "sinusoidal"])
@pytest.mark.parametrize("lm_head", ["simple", "roberta"])
def test_sharding(
    num_shards: int,
    add_bias_kv: bool,
    use_rotary_embedding: bool,
    positional_embedding: Optional[str],
    lm_head: str,
) -> None:
    batch_size = 16
    seq_len = 10
    if use_rotary_embedding:
        positional_embedding = None
    if add_bias_kv:
        use_rotary_embedding = False

    random_key = jax.random.PRNGKey(0)
    tokens = jax.random.randint(
        random_key, minval=5, maxval=28, shape=(batch_size, seq_len)
    )

    random_key, *sub_keys = jax.random.split(random_key, num=num_shards + 1)
    sub_keys = jnp.array(sub_keys)
    tokens = jnp.tile(tokens, (num_shards, 1, 1))
    config = ESMTransformerConfig(
        alphabet_size=33,
        pad_token_id=1,
        mask_token_id=0,
        attention_heads=16,
        num_layers=2,
        max_positions=seq_len,
        embeddings_layers_to_save=(2,),
        add_bias_kv=add_bias_kv,
        use_rotary_embedding=use_rotary_embedding,
        positional_embedding=positional_embedding,
        lm_head=lm_head,
        attention_maps_to_save=[
            (2, 1),
        ],
    )

    sharded_transformer_fn = build_sharded_esm_fn(
        model_config=config, num_shards=num_shards
    )
    forward_fn = hk.transform(sharded_transformer_fn)

    params = jax.pmap(forward_fn.init, axis_name="shard")(sub_keys, tokens)

    outs = jax.pmap(forward_fn.apply, axis_name="shard")(params, sub_keys, tokens)

    pytest.assume(
        outs[f"embeddings_{config.num_layers}"].shape
        == (num_shards, batch_size, seq_len, config.embed_dim)
    )
    pytest.assume(
        outs["logits"].shape == (num_shards, batch_size, seq_len, config.alphabet_size)
    )
    pytest.assume(
        outs["attention_map_layer_2_number_1"].shape
        == (num_shards, batch_size, seq_len, seq_len + add_bias_kv)
    )
