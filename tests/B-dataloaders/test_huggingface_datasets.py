import jax.numpy as jnp
import pytest
from transformers import AutoTokenizer

from trix.dataloaders.language_models.huggingface_datasets import (
    HFEncoderDecoderDataset,
    HFInstructionDataset,
    HFTextDataset,
)


@pytest.mark.parametrize(("batch_size", "tokenized_sequence_length"), [[8, 128]])
def test_hf_text_dataset(batch_size: int, tokenized_sequence_length: int) -> None:
    """Tests that the dataset returns batches of the right shape and is seeded
    correctly."""
    dataset = HFTextDataset(
        dataset_name="c4",
        split="train",
        tokenizer=AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B"),
        batch_size=batch_size,
        tokenized_sequence_length=tokenized_sequence_length,
        shuffle=True,
        shuffle_buffer_size=200,
        drop_last=True,
        seed=0,
        streaming=True,
        name="en",
    )

    batch_generator = dataset.get_iterator()
    tokens = next(batch_generator)

    pytest.assume(
        tokens.shape == (batch_size, tokenized_sequence_length),
        "batch do not have the right shape",
    )

    new_dataset = HFTextDataset(
        dataset_name="c4",
        split="train",
        tokenizer=AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B"),
        batch_size=8,
        tokenized_sequence_length=tokenized_sequence_length,
        shuffle=True,
        shuffle_buffer_size=200,
        drop_last=True,
        seed=0,
        streaming=True,
        name="en",
    )

    new_batch_generator = new_dataset.get_iterator()
    new_tokens = next(new_batch_generator)

    pytest.assume(jnp.all(new_tokens == tokens), "Seed does not work properly.")


@pytest.mark.parametrize(("batch_size", "tokenized_sequence_length"), [[16, 256]])
@pytest.mark.filterwarnings("ignore::UserWarning")
def test_hf_instruction_dataset(
    batch_size: int, tokenized_sequence_length: int
) -> None:
    """Tests that the dataset returns batches of the right shape and is seeded
    correctly. Also tests that the number of masks is correct and that the number
    of instruction corresponds to the number of responses."""
    tokenizer = AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B")
    tokenizer.pad_token_id = tokenizer.eos_token_id
    tokenizer.pad_token = tokenizer.eos_token

    dataset = HFInstructionDataset(
        dataset_name="tatsu-lab/alpaca",
        split="train",
        tokenizer=tokenizer,
        batch_size=batch_size,
        tokenized_sequence_length=tokenized_sequence_length,
        shuffle=True,
        shuffle_buffer_size=200,
        drop_last=True,
        seed=0,
        streaming=True,
    )

    batch_generator = dataset.get_iterator()
    tokens, mask = next(batch_generator)
    num_instruction = jnp.sum(tokens == tokenizer.encode(" Instruction")[0], axis=1)
    num_response = jnp.sum(tokens == tokenizer.encode(" Response")[0], axis=1)

    # +3 to account for <Response>, <:> and <\n>
    start_response = jnp.argmax(tokens == tokenizer.encode(" Response")[0], axis=1) + 3
    # +1 to add the first <endoftext>
    end_response = jnp.argmax(tokens == tokenizer.eos_token_id, axis=1) + 1
    num_tokens_response = end_response - start_response
    num_mask = jnp.sum(mask, axis=1)

    pytest.assume(
        tokens.shape == (batch_size, tokenized_sequence_length),
        "tokens do not have the right shape",
    )
    pytest.assume(
        mask.shape == (batch_size, tokenized_sequence_length),
        "masks do not have the right shape",
    )
    pytest.assume(
        jnp.all(num_instruction == num_response),
        "some sequences have more instructions than responses",
    )
    pytest.assume(
        jnp.all(num_tokens_response == num_mask),
        "number of tokens after response does not correspond to the number of masks",
    )

    new_dataset = HFInstructionDataset(
        dataset_name="tatsu-lab/alpaca",
        split="train",
        tokenizer=AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B"),
        batch_size=batch_size,
        tokenized_sequence_length=tokenized_sequence_length,
        shuffle=True,
        shuffle_buffer_size=200,
        drop_last=True,
        seed=0,
        streaming=True,
    )

    new_batch_generator = new_dataset.get_iterator()
    new_tokens, _ = next(new_batch_generator)

    pytest.assume(jnp.all(new_tokens == tokens), "Seed does not work properly.")


@pytest.mark.parametrize(("batch_size", "tokenized_sequence_length"), [[16, 256]])
def test_hf_encoder_decoder_dataset(
    batch_size: int, tokenized_sequence_length: int
) -> None:
    tokenizer = AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B")
    tokenizer.pad_token_id = tokenizer.eos_token_id
    tokenizer.pad_token = tokenizer.eos_token

    dataset = HFEncoderDecoderDataset(
        dataset_name="tatsu-lab/alpaca",
        split="train",
        tokenizer=tokenizer,
        batch_size=batch_size,
        tokenized_sequence_length=tokenized_sequence_length,
        shuffle=False,
        shuffle_buffer_size=200,
        drop_last=True,
        seed=0,
        streaming=True,
        input_key="text",
        output_key="output",
    )

    batch_generator = dataset.get_iterator()
    input_tokens, input_mask, target_tokens, target_masks = next(batch_generator)

    pytest.assume(
        input_tokens.shape == (batch_size, tokenized_sequence_length),
        "input tokens do not have the right shape",
    )
    pytest.assume(
        input_mask.shape == (batch_size, tokenized_sequence_length),
        "masks do not have the right shape",
    )
    pytest.assume(
        target_tokens.shape == (batch_size, tokenized_sequence_length),
        "input tokens do not have the right shape",
    )
    pytest.assume(
        target_masks.shape == (batch_size, tokenized_sequence_length),
        "masks do not have the right shape",
    )

    new_dataset = HFEncoderDecoderDataset(
        dataset_name="tatsu-lab/alpaca",
        split="train",
        tokenizer=tokenizer,
        batch_size=batch_size,
        tokenized_sequence_length=tokenized_sequence_length,
        shuffle=False,
        shuffle_buffer_size=200,
        drop_last=True,
        seed=0,
        streaming=True,
        input_key="text",
        output_key="output",
    )

    new_batch_generator = new_dataset.get_iterator()
    new_input_tokens, _, new_target_tokens, _ = next(new_batch_generator)
    pytest.assume(
        jnp.all(new_input_tokens == input_tokens)
        & jnp.all(new_target_tokens == target_tokens),
        "Seed does not work properly.",
    )


def check_init() -> None:
    """Checks that User warnings are raised with different datasets."""

    with pytest.raises(UserWarning):
        _ = HFInstructionDataset(
            dataset_name="EleutherAI/pile",
            split="train",
            tokenizer=AutoTokenizer.from_pretrained("EleutherAI/gpt_base-j-6B"),
            batch_size=1,
            tokenized_sequence_length=1,
            shuffle=True,
            shuffle_buffer_size=1,
            drop_last=True,
            seed=0,
            streaming=True,
        )
