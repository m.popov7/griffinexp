from typing import Dict

import jax
import jax.numpy as jnp
import pytest

from trix.dataloaders.rl.datasets import TrajectoryDataset
from trix.dataloaders.rl.generators import generate_fake_rl_trajectories
from trix.tokenizers.rl.base import RLTokenizer, Transition
from trix.types import Tokens


@pytest.mark.parametrize("drop_last", [False, True])
@pytest.mark.parametrize("shuffle", [False])
def test_trajectory_dataset(drop_last: bool, shuffle: bool) -> None:
    """
    Tests that the dataset class produces the correct batched data, tests the shape of
    the data, and its consistency with fake inputs.
    """

    episode_length = 5
    batch_size = 2
    observation_size = 2
    action_size = 1
    horizon_size = 3
    num_chunks_per_trajectory = 10
    num_trajectories = 100

    random_key = jax.random.PRNGKey(42)
    random_key, subkey = jax.random.split(random_key)

    class DummyTokenizer(RLTokenizer):
        @property
        def action_size(self) -> int:
            return 0

        @property
        def observation_size(self) -> int:
            return 0

        @property
        def concat_size(self) -> int:
            return 3

        def tokenize(self, transitions: Transition) -> Dict[str, Tokens]:
            episode_length = transitions.obs.shape[0]
            return jnp.arange(episode_length * self.concat_size)  # type: ignore

        def batch_tokenize(self, transitions: Transition) -> Dict[str, Tokens]:
            return jax.vmap(self.tokenize)(transitions)  # type: ignore

        def get_positions(self, episode_length: int) -> jnp.ndarray:
            """
            Returns an array that contains the timesteps number along the tokens in the
            sequence in order to compute the positional encodings.

            Args:
                episode_length: Length of the sequencse to be tokenized.

            Returns:
                Positions of the tokens in the trajectory.
            """
            pass

        def decode(self, tokens: Tokens) -> Dict[str, jnp.ndarray]:  # type: ignore
            """Decodes a token to retrieve the inputted values.

            Args:
                tokens: Tokens.

            Returns:
                Dictionary containing the decoded values.
            """
            pass

    data = generate_fake_rl_trajectories(
        num_trajectories=num_trajectories,
        episode_length=episode_length,
        observation_size=observation_size,
        action_size=action_size,
        random_key=subkey,
    )
    tokenizer = DummyTokenizer()

    random_key, subkey = jax.random.split(random_key)
    dataset = TrajectoryDataset(
        data=data,
        tokenizer=tokenizer,
        batch_size=batch_size,
        random_key=subkey,
        horizon_size=horizon_size,
        num_chunks_per_trajectory=num_chunks_per_trajectory,
        resample_chunks=True,
    )

    # check number of batches in each epoch
    num_batches_per_epoch = (
        num_trajectories // batch_size
        if drop_last
        else num_trajectories // batch_size + 1
    )
    pytest.assume(num_batches_per_epoch, dataset.num_batches_per_epoch)

    for batch in dataset.get_epoch_batches():
        for chunk in batch:  # type: ignore
            expected_chunk = jnp.arange(start=chunk[0], stop=len(chunk) + chunk[0])
            pytest.assume(jnp.all(chunk == expected_chunk))
