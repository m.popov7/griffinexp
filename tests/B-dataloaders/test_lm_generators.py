import numpy as np
import pytest

from trix.dataloaders.language_models.generators import (
    generate_fake_dataset,
    generate_fake_labeled_dataset,
    generate_fake_regression_dataset,
)
from trix.utils.constants.bio import AMINO_ACIDS


@pytest.mark.parametrize(
    ("min_sequence_length", "max_sequence_length", "num_sequences", "seed"),
    [[10, 20, 32, 0], [11, 10, 2, 0]],
)
def test_generate_fake_dataset(
    min_sequence_length: int, max_sequence_length: int, num_sequences: int, seed: int
) -> None:
    """Checks shapes of sequences generated and that the generation is determnisitic."""

    if max_sequence_length < min_sequence_length:
        with pytest.raises(ValueError):
            sequences = generate_fake_dataset(
                min_sequence_length=min_sequence_length,
                max_sequence_length=max_sequence_length,
                num_sequences=num_sequences,
                standard_tokens=AMINO_ACIDS,
                seed=seed,
            )
    else:
        sequences = generate_fake_dataset(
            min_sequence_length=min_sequence_length,
            max_sequence_length=max_sequence_length,
            num_sequences=num_sequences,
            standard_tokens=AMINO_ACIDS,
            seed=seed,
        )
        pytest.assume(
            min([len(s) for s in sequences]) >= min_sequence_length,
            "sequence with minimum length lower than min_sequence_length",
        )
        pytest.assume(
            max([len(s) for s in sequences]) <= max_sequence_length,
            "sequence with maximum length higher than max_sequence_length",
        )
        pytest.assume(
            np.all([np.all([t in AMINO_ACIDS for t in s]) for s in sequences]),
            "one token not in standard_tokens",
        )
        new_sequences = generate_fake_dataset(
            min_sequence_length=min_sequence_length,
            max_sequence_length=max_sequence_length,
            num_sequences=num_sequences,
            standard_tokens=AMINO_ACIDS,
            seed=seed,
        )
        pytest.assume(
            np.all([s == new_s for (s, new_s) in zip(sequences, new_sequences)]),
            "seed does not work properly",
        )


@pytest.mark.parametrize(
    (
        "min_sequence_length",
        "max_sequence_length",
        "num_sequences",
        "num_labels",
        "seed",
    ),
    [[10, 20, 32, 3, 0], [11, 10, 32, 3, 0]],
)
def test_generate_fake_labeled_dataset(
    min_sequence_length: int,
    max_sequence_length: int,
    num_sequences: int,
    num_labels: int,
    seed: int,
) -> None:
    """Checks shapes of sequences and labels generated and that the generation is
    determnisitic."""
    if max_sequence_length < min_sequence_length:
        with pytest.raises(ValueError):
            sequences, labels = generate_fake_labeled_dataset(
                min_sequence_length=min_sequence_length,
                max_sequence_length=max_sequence_length,
                num_sequences=num_sequences,
                standard_tokens=AMINO_ACIDS,
                seed=seed,
            )

    else:
        sequences, labels = generate_fake_labeled_dataset(
            min_sequence_length=min_sequence_length,
            max_sequence_length=max_sequence_length,
            num_sequences=num_sequences,
            standard_tokens=AMINO_ACIDS,
            num_labels=num_labels,
            seed=seed,
        )
        pytest.assume(
            min([len(s) for s in sequences]) >= min_sequence_length,
            "sequence with minimum length lower than min_sequence_length",
        )
        pytest.assume(
            max([len(s) for s in sequences]) <= max_sequence_length,
            "sequence with maximum length higher than max_sequence_length",
        )
        pytest.assume(
            np.all([np.all([t in AMINO_ACIDS for t in s]) for s in sequences]),
            "one token not in standard_tokens",
        )
        pytest.assume(
            np.array(labels).shape == (num_sequences, num_labels),
            "number of labels not correct",
        )

        new_sequences, new_labels = generate_fake_labeled_dataset(
            min_sequence_length=min_sequence_length,
            max_sequence_length=max_sequence_length,
            num_sequences=num_sequences,
            standard_tokens=AMINO_ACIDS,
            num_labels=num_labels,
            seed=seed,
        )
        pytest.assume(
            np.all([s == new_s for (s, new_s) in zip(sequences, new_sequences)]),
            "seed does not work properly",
        )
        pytest.assume(
            np.all([l == new_l for (l, new_l) in zip(labels, new_labels)]),
            "seed does not work properly",
        )


@pytest.mark.parametrize(
    (
        "min_sequence_length",
        "max_sequence_length",
        "num_sequences",
        "num_labels",
        "seed",
    ),
    [[10, 20, 32, 3, 0], [11, 10, 32, 3, 0]],
)
def test_generate_fake_regression_dataset(
    min_sequence_length: int,
    max_sequence_length: int,
    num_sequences: int,
    num_labels: int,
    seed: int,
) -> None:
    """Checks shapes of sequences and labels generated and that the generation is
    determnisitic."""
    if max_sequence_length < min_sequence_length:
        with pytest.raises(ValueError):
            sequences, labels = generate_fake_regression_dataset(
                min_sequence_length=min_sequence_length,
                max_sequence_length=max_sequence_length,
                num_sequences=num_sequences,
                standard_tokens=AMINO_ACIDS,
                num_labels=num_labels,
                seed=seed,
            )
    else:
        sequences, labels = generate_fake_regression_dataset(
            min_sequence_length=min_sequence_length,
            max_sequence_length=max_sequence_length,
            num_sequences=num_sequences,
            standard_tokens=AMINO_ACIDS,
            num_labels=num_labels,
            seed=seed,
        )
        pytest.assume(
            min([len(s) for s in sequences]) >= min_sequence_length,
            "sequence with minimum length lower than min_sequence_length",
        )
        pytest.assume(
            max([len(s) for s in sequences]) <= max_sequence_length,
            "sequence with maximum length higher than max_sequence_length",
        )
        pytest.assume(
            np.all([np.all([t in AMINO_ACIDS for t in s]) for s in sequences]),
            "one token not in standard_tokens",
        )
        pytest.assume(
            np.array(labels).shape == (num_sequences, num_labels),
            "number of labels not correct",
        )

        new_sequences, new_labels = generate_fake_regression_dataset(
            min_sequence_length=min_sequence_length,
            max_sequence_length=max_sequence_length,
            num_sequences=num_sequences,
            standard_tokens=AMINO_ACIDS,
            num_labels=num_labels,
            seed=seed,
        )
        pytest.assume(
            np.all([s == new_s for (s, new_s) in zip(sequences, new_sequences)]),
            "seed does not work properly",
        )
        pytest.assume(
            np.all([l == new_l for (l, new_l) in zip(labels, new_labels)]),
            "seed does not work properly",
        )
