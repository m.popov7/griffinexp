import jax.numpy as jnp
import jax.random
import pytest

from trix.dataloaders.language_models.generators import (
    generate_fake_dataset,
    generate_fake_labeled_dataset,
)
from trix.dataloaders.language_models.sequence_datasets import (
    LabeledSequenceDataset,
    PairedSequenceDataset,
    SequencesDataset,
)
from trix.tokenizers.language_models.standard import StandardTokenizer


@pytest.mark.parametrize("drop_last", [False, True])
@pytest.mark.parametrize("shuffle", [False])
@pytest.mark.parametrize("batch_size", [2, 3])
def test_sequence_dataset(drop_last: bool, batch_size: int, shuffle: bool) -> None:
    """
    Tests that the dataset class produces the correct batched data.
    """

    standard_tokens = list("ABCDE")
    sequences = generate_fake_dataset(
        min_sequence_length=5,
        max_sequence_length=10,
        num_sequences=16,
        standard_tokens=standard_tokens,
        seed=0,
    )
    tokenizer = StandardTokenizer(standard_tokens)

    dataset = SequencesDataset(
        sequences=sequences,
        tokenizer=tokenizer,
        batch_size=batch_size,
        drop_last=drop_last,
        shuffle=shuffle,
    )

    # check number of batches in each epoch
    num_batches_per_epoch = (
        len(sequences) // batch_size if drop_last else len(sequences) // batch_size + 1
    )
    pytest.assume(num_batches_per_epoch, dataset.num_batches_per_epoch)

    # random_key = jax.random.PRNGKey(0) , pass an rng key instead ? of using shuffle
    generator = dataset.get_epoch_batches()

    # check that data is correctly batched
    for i in range(0, num_batches_per_epoch - 1):

        if i == dataset.num_batches_per_epoch - 1 and drop_last:
            tokens = list(zip(*tokenizer.batch_tokenize(sequences[batch_size * i :])))[
                1
            ]
        else:
            tokens = list(
                zip(
                    *tokenizer.batch_tokenize(
                        sequences[batch_size * i : batch_size * (i + 1)]
                    )
                )
            )[1]

        pytest.assume(jnp.allclose(jnp.array(tokens), next(generator)))


@pytest.mark.parametrize("drop_last", [False, True])
@pytest.mark.parametrize("shuffle", [False])
@pytest.mark.parametrize("batch_size", [2, 3])
def test_labeled_sequence_dataset(
    drop_last: bool, batch_size: int, shuffle: bool
) -> None:
    """
    Tests that the dataset class produces the correct batched data.
    """

    standard_tokens = list("ABCDE")
    tokenizer = StandardTokenizer(standard_tokens)

    sequences, labels = generate_fake_labeled_dataset(
        min_sequence_length=5,
        max_sequence_length=10,
        num_sequences=16,
        standard_tokens=standard_tokens,
        seed=0,
    )

    dataset = LabeledSequenceDataset(
        sequences=sequences,
        labels=labels,
        tokenizer=tokenizer,
        batch_size=batch_size,
        drop_last=drop_last,
        shuffle=shuffle,
    )

    # check number of batches in each epoch
    num_batches_per_epoch = (
        len(sequences) // batch_size if drop_last else len(sequences) // batch_size + 1
    )
    pytest.assume(num_batches_per_epoch, dataset.num_batches_per_epoch)

    generator = dataset.get_epoch_batches()

    # check that data is correctly batched
    for i in range(0, num_batches_per_epoch - 1):

        if i == dataset.num_batches_per_epoch - 1 and drop_last:
            tokens = list(zip(*tokenizer.batch_tokenize(sequences[batch_size * i :])))[
                1
            ]
            tok_labels = labels[batch_size * i :]
        else:
            tokens = list(
                zip(
                    *tokenizer.batch_tokenize(
                        sequences[batch_size * i : batch_size * (i + 1)]
                    )
                )
            )[1]
            tok_labels = labels[batch_size * i : batch_size * (i + 1)]

        ground_truth_tokens, ground_truth_labels = next(generator)  # type: ignore

        pytest.assume(jnp.allclose(jnp.array(tokens), ground_truth_tokens))
        pytest.assume(jnp.allclose(jnp.array(tok_labels), ground_truth_labels))


@pytest.mark.parametrize("drop_last", [False, True])
@pytest.mark.parametrize("shuffle", [False])
@pytest.mark.parametrize("batch_size", [2, 3])
@pytest.mark.parametrize("min_seq_len", [2, 4])
def test_paired_sequence_dataset(
    drop_last: bool, batch_size: int, shuffle: bool, min_seq_len: int
) -> None:
    """
    Tests that the paired dataset class produces the correct batched data, and raises
    a ValueError if any of the input sequence lengths divided by 2 are less than the
    minimum sequence length argument.
    """

    sequences = ["AAABBB", "CCCDDD", "AAADD", "BEEEEEEE", "DEDDDDDE"]

    vocab = list("ABCDE")
    prefix_tokenizer = StandardTokenizer(vocab, prepend_cls_token=True)
    suffix_tokenizer = StandardTokenizer(vocab, prepend_bos_token=True)

    # check that value error is raised when minimum sequence length arg is greater than
    # any of the sequence lengths divided by 2
    if (min_seq_len > jnp.array([len(seq) for seq in sequences]) // 2).any():
        with pytest.raises(ValueError):
            _ = PairedSequenceDataset(
                sequences=sequences,
                prefix_tokenizer=prefix_tokenizer,
                suffix_tokenizer=suffix_tokenizer,
                batch_size=batch_size,
                min_sequence_len=min_seq_len,
                drop_last=drop_last,
                shuffle=shuffle,
                random_key=jax.random.PRNGKey(42),
            )
        return

    dataset = PairedSequenceDataset(
        sequences=sequences,
        prefix_tokenizer=prefix_tokenizer,
        suffix_tokenizer=suffix_tokenizer,
        batch_size=batch_size,
        min_sequence_len=min_seq_len,
        drop_last=drop_last,
        shuffle=shuffle,
        random_key=jax.random.PRNGKey(42),
    )

    # check number of batches in each epoch
    num_batches_per_epoch = (
        len(sequences) // batch_size if drop_last else len(sequences) // batch_size + 1
    )
    pytest.assume(num_batches_per_epoch, dataset.num_batches_per_epoch)

    generator = dataset.get_epoch_batches()

    # check that data is correctly batched
    for i in range(0, num_batches_per_epoch - 1):

        if i == dataset.num_batches_per_epoch - 1 and drop_last:
            paired_sequences = dataset._paired_sequences[batch_size * i :]
            encoder_seqs, decoder_seqs = list(zip(*paired_sequences))

        else:
            paired_sequences = dataset._paired_sequences[
                batch_size * i : batch_size * (i + 1)
            ]
            encoder_seqs, decoder_seqs = list(zip(*paired_sequences))

        encoder_tokens = list(
            zip(*prefix_tokenizer.batch_tokenize(list(encoder_seqs)))
        )[1]
        decoder_tokens = list(
            zip(*suffix_tokenizer.batch_tokenize(list(decoder_seqs)))
        )[1]

        ground_truth_encoder_tokens, ground_truth_decoder_tokens = next(generator)
        # type: ignore

        pytest.assume(
            jnp.allclose(jnp.array(encoder_tokens), ground_truth_encoder_tokens)
        )
        pytest.assume(
            jnp.allclose(jnp.array(decoder_tokens), ground_truth_decoder_tokens)
        )
